.class public Lorg/htmlparser/Attribute;
.super Ljava/lang/Object;
.source "Attribute.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field protected mAssignment:Ljava/lang/String;

.field protected mName:Ljava/lang/String;

.field protected mQuote:C

.field protected mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 332
    invoke-direct {p0, v0, v0, v0, v1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 293
    invoke-virtual {p0, v0}, Lorg/htmlparser/Attribute;->setName(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p0, v0}, Lorg/htmlparser/Attribute;->setAssignment(Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 296
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->setQuote(C)V

    return-void

    .line 290
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "non whitespace value"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-nez p2, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    const-string v0, "="

    :goto_0
    const/4 v1, 0x0

    .line 310
    invoke-direct {p0, p1, v0, p2, v1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;C)V
    .locals 1

    if-nez p2, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    const-string v0, "="

    .line 276
    :goto_0
    invoke-direct {p0, p1, v0, p2, p3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 322
    invoke-direct {p0, p1, p2, p3, v0}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V
    .locals 0

    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->setName(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0, p2}, Lorg/htmlparser/Attribute;->setAssignment(Ljava/lang/String;)V

    if-nez p4, :cond_0

    .line 257
    invoke-virtual {p0, p3}, Lorg/htmlparser/Attribute;->setRawValue(Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_0
    invoke-virtual {p0, p3}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0, p4}, Lorg/htmlparser/Attribute;->setQuote(C)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getAssignment()Ljava/lang/String;
    .locals 1

    .line 384
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mAssignment:Ljava/lang/String;

    return-object v0
.end method

.method public getAssignment(Ljava/lang/StringBuffer;)V
    .locals 1

    .line 395
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mAssignment:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public getLength()I
    .locals 2

    .line 695
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 697
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    .line 698
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 700
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    .line 701
    :cond_1
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 703
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    .line 704
    :cond_2
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getQuote()C

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v1, v1, 0x2

    :cond_3
    return v1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 345
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getName(Ljava/lang/StringBuffer;)V
    .locals 1

    .line 356
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public getQuote()C
    .locals 1

    .line 466
    iget-char v0, p0, Lorg/htmlparser/Attribute;->mQuote:C

    return v0
.end method

.method public getQuote(Ljava/lang/StringBuffer;)V
    .locals 1

    .line 477
    iget-char v0, p0, Lorg/htmlparser/Attribute;->mQuote:C

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public getRawValue()Ljava/lang/String;
    .locals 2

    .line 509
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->isValued()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 511
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getQuote()C

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 515
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 516
    invoke-virtual {p0, v1}, Lorg/htmlparser/Attribute;->getValue(Ljava/lang/StringBuffer;)V

    .line 517
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 518
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 521
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getRawValue(Ljava/lang/StringBuffer;)V
    .locals 0

    .line 539
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getQuote(Ljava/lang/StringBuffer;)V

    .line 540
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getValue(Ljava/lang/StringBuffer;)V

    .line 541
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getQuote(Ljava/lang/StringBuffer;)V

    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 427
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue(Ljava/lang/StringBuffer;)V
    .locals 1

    .line 438
    iget-object v0, p0, Lorg/htmlparser/Attribute;->mValue:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 439
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .line 669
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isStandAlone()Z
    .locals 1

    .line 659
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isValued()Z
    .locals 1

    .line 679
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isWhitespace()Z
    .locals 1

    .line 649
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setAssignment(Ljava/lang/String;)V
    .locals 0

    .line 411
    iput-object p1, p0, Lorg/htmlparser/Attribute;->mAssignment:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 372
    iput-object p1, p0, Lorg/htmlparser/Attribute;->mName:Ljava/lang/String;

    return-void
.end method

.method public setQuote(C)V
    .locals 0

    .line 492
    iput-char p1, p0, Lorg/htmlparser/Attribute;->mQuote:C

    return-void
.end method

.method public setRawValue(Ljava/lang/String;)V
    .locals 10

    const/16 v0, 0x27

    const/16 v1, 0x22

    const/4 v2, 0x0

    if-eqz p1, :cond_a

    .line 568
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "\'"

    .line 570
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 571
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v5, v3, :cond_0

    .line 574
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v6

    invoke-virtual {p1, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :goto_0
    const/16 v1, 0x27

    goto/16 :goto_5

    :cond_0
    const-string v3, "\""

    .line 576
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 577
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v5, v3, :cond_1

    .line 580
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v6

    invoke-virtual {p1, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_5

    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x1

    .line 589
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v3, v8, :cond_6

    if-eqz v4, :cond_a

    if-eqz v5, :cond_2

    goto :goto_5

    :cond_2
    if-eqz v7, :cond_3

    goto :goto_0

    .line 623
    :cond_3
    new-instance v0, Ljava/lang/StringBuffer;

    .line 624
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x5

    .line 623
    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 625
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 633
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    .line 627
    :cond_4
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v1, v3, :cond_5

    const-string v3, "&quot;"

    .line 629
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 631
    :cond_5
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 591
    :cond_6
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v0, v8, :cond_7

    const/4 v4, 0x1

    const/4 v7, 0x0

    goto :goto_4

    :cond_7
    if-ne v1, v8, :cond_8

    const/4 v4, 0x1

    const/4 v5, 0x0

    goto :goto_4

    :cond_8
    const/16 v9, 0x2d

    if-eq v9, v8, :cond_9

    const/16 v9, 0x2e

    if-eq v9, v8, :cond_9

    const/16 v9, 0x5f

    if-eq v9, v8, :cond_9

    const/16 v9, 0x3a

    if-eq v9, v8, :cond_9

    .line 603
    invoke-static {v8}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v8

    if-nez v8, :cond_9

    const/4 v4, 0x1

    :cond_9
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_a
    const/4 v1, 0x0

    .line 638
    :goto_5
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0, v1}, Lorg/htmlparser/Attribute;->setQuote(C)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0

    .line 455
    iput-object p1, p0, Lorg/htmlparser/Attribute;->mValue:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 733
    invoke-virtual {p0}, Lorg/htmlparser/Attribute;->getLength()I

    move-result v0

    .line 734
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 735
    invoke-virtual {p0, v1}, Lorg/htmlparser/Attribute;->toString(Ljava/lang/StringBuffer;)V

    .line 737
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/StringBuffer;)V
    .locals 0

    .line 747
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getName(Ljava/lang/StringBuffer;)V

    .line 748
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getAssignment(Ljava/lang/StringBuffer;)V

    .line 749
    invoke-virtual {p0, p1}, Lorg/htmlparser/Attribute;->getRawValue(Ljava/lang/StringBuffer;)V

    return-void
.end method
