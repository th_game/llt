.class public Lorg/htmlparser/lexer/PageIndex;
.super Ljava/lang/Object;
.source "PageIndex.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/htmlparser/util/sort/Sortable;


# static fields
.field protected static final mStartIncrement:I = 0x64


# instance fields
.field protected mCount:I

.field protected mIncrement:I

.field protected mIndices:[I

.field protected mPage:Lorg/htmlparser/lexer/Page;


# direct methods
.method public constructor <init>(Lorg/htmlparser/lexer/Page;)V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mPage:Lorg/htmlparser/lexer/Page;

    .line 77
    iget p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIncrement:I

    new-array p1, p1, [I

    iput-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    const/4 p1, 0x0

    .line 78
    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    const/16 p1, 0xc8

    .line 79
    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIncrement:I

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;I)V
    .locals 1

    .line 89
    invoke-direct {p0, p1}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    .line 90
    iget-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    const/4 v0, 0x0

    aput p2, p1, v0

    const/4 p1, 0x1

    .line 91
    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;[I)V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mPage:Lorg/htmlparser/lexer/Page;

    .line 103
    iput-object p2, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    .line 104
    array-length p1, p2

    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    return-void
.end method


# virtual methods
.method public add(I)I
    .locals 2

    .line 185
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->add(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    return p1
.end method

.method public add(Lorg/htmlparser/lexer/Cursor;)I
    .locals 4

    .line 146
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    .line 147
    iget v1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 150
    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/lexer/PageIndex;->insertElementAt(II)V

    goto :goto_0

    .line 154
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    add-int/lit8 v3, v1, -0x1

    aget v2, v2, v3

    if-ne v0, v2, :cond_1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    if-le v0, v2, :cond_2

    .line 161
    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/lexer/PageIndex;->insertElementAt(II)V

    goto :goto_0

    .line 166
    :cond_2
    invoke-static {p0, p1}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;)I

    move-result v1

    .line 169
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result p1

    if-ge v1, p1, :cond_3

    iget-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget p1, p1, v1

    if-eq v0, p1, :cond_4

    .line 170
    :cond_3
    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/lexer/PageIndex;->insertElementAt(II)V

    :cond_4
    :goto_0
    return v1
.end method

.method protected bsearch(I)I
    .locals 2

    .line 305
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-static {p0, v0}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;)I

    move-result p1

    return p1
.end method

.method protected bsearch(III)I
    .locals 2

    .line 317
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-static {p0, v0, p2, p3}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;II)I

    move-result p1

    return p1
.end method

.method public capacity()I
    .locals 1

    .line 131
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    array-length v0, v0

    return v0
.end method

.method public column(I)I
    .locals 2

    .line 282
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->column(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    return p1
.end method

.method public column(Lorg/htmlparser/lexer/Cursor;)I
    .locals 1

    .line 266
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageIndex;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 268
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->elementAt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 272
    :goto_0
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    sub-int/2addr p1, v0

    return p1
.end method

.method public elementAt(I)I
    .locals 3

    .line 220
    iget v0, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    if-ge p1, v0, :cond_0

    .line 223
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget p1, v0, p1

    return p1

    .line 221
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " beyond current limit"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fetch(ILorg/htmlparser/util/sort/Ordered;)Lorg/htmlparser/util/sort/Ordered;
    .locals 2

    if-eqz p2, :cond_0

    .line 403
    check-cast p2, Lorg/htmlparser/lexer/Cursor;

    .line 404
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget p1, v0, p1

    iput p1, p2, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    .line 405
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object p1

    iput-object p1, p2, Lorg/htmlparser/lexer/Cursor;->mPage:Lorg/htmlparser/lexer/Page;

    goto :goto_0

    .line 408
    :cond_0
    new-instance p2, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    iget-object v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget p1, v1, p1

    invoke-direct {p2, v0, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    :goto_0
    return-object p2
.end method

.method public first()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public get()[I
    .locals 4

    .line 292
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v0

    new-array v0, v0, [I

    .line 293
    iget-object v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public getPage()Lorg/htmlparser/lexer/Page;
    .locals 1

    .line 113
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mPage:Lorg/htmlparser/lexer/Page;

    return-object v0
.end method

.method protected insertElementAt(II)V
    .locals 4

    .line 329
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v0

    if-ge p2, v0, :cond_1

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v0

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 343
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v0

    if-ge p2, v0, :cond_3

    .line 345
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-static {v0, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 331
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v0

    iget v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIncrement:I

    add-int/2addr v0, v1

    add-int/lit8 v1, p2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [I

    .line 332
    iget v2, p0, Lorg/htmlparser/lexer/PageIndex;->mIncrement:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, p0, Lorg/htmlparser/lexer/PageIndex;->mIncrement:I

    .line 333
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v2

    const/4 v3, 0x0

    if-ge p2, v2, :cond_2

    .line 336
    iget-object v2, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    invoke-static {v2, v3, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 337
    iget-object v2, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v3

    sub-int/2addr v3, p2

    invoke-static {v2, p2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 340
    :cond_2
    iget-object v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 341
    :goto_1
    iput-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    .line 346
    :cond_3
    :goto_2
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aput p1, v0, p2

    .line 347
    iget p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    return-void
.end method

.method public last()I
    .locals 1

    .line 383
    iget v0, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove(I)V
    .locals 2

    .line 210
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->remove(Lorg/htmlparser/lexer/Cursor;)V

    return-void
.end method

.method public remove(Lorg/htmlparser/lexer/Cursor;)V
    .locals 2

    .line 197
    invoke-static {p0, p1}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;)I

    move-result v0

    .line 200
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    iget-object v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_0

    .line 201
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->removeElementAt(I)V

    :cond_0
    return-void
.end method

.method protected removeElementAt(I)V
    .locals 3

    .line 357
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-static {v0, v1, v0, p1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 358
    iget-object p1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->capacity()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    aput v1, p1, v0

    .line 359
    iget p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    return-void
.end method

.method public row(I)I
    .locals 2

    .line 253
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageIndex;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageIndex;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    return p1
.end method

.method public row(Lorg/htmlparser/lexer/Cursor;)I
    .locals 2

    .line 235
    invoke-static {p0, p1}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;)I

    move-result v0

    .line 240
    iget v1, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    iget-object v1, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public size()I
    .locals 1

    .line 122
    iget v0, p0, Lorg/htmlparser/lexer/PageIndex;->mCount:I

    return v0
.end method

.method public swap(II)V
    .locals 3

    .line 420
    iget-object v0, p0, Lorg/htmlparser/lexer/PageIndex;->mIndices:[I

    aget v1, v0, p1

    .line 421
    aget v2, v0, p2

    aput v2, v0, p1

    .line 422
    aput v1, v0, p2

    return-void
.end method
