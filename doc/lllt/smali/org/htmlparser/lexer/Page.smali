.class public Lorg/htmlparser/lexer/Page;
.super Ljava/lang/Object;
.source "Page.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT_CHARSET:Ljava/lang/String; = "ISO-8859-1"

.field public static final DEFAULT_CONTENT_TYPE:Ljava/lang/String; = "text/html"

.field public static final EOF:C = '\uffff'

.field protected static mConnectionManager:Lorg/htmlparser/http/ConnectionManager;


# instance fields
.field protected mBaseUrl:Ljava/lang/String;

.field protected transient mConnection:Ljava/net/URLConnection;

.field protected mIndex:Lorg/htmlparser/lexer/PageIndex;

.field protected mSource:Lorg/htmlparser/lexer/Source;

.field protected mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 108
    new-instance v0, Lorg/htmlparser/http/ConnectionManager;

    invoke-direct {v0}, Lorg/htmlparser/http/ConnectionManager;-><init>()V

    .line 107
    sput-object v0, Lorg/htmlparser/lexer/Page;->mConnectionManager:Lorg/htmlparser/http/ConnectionManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    .line 115
    invoke-direct {p0, v0}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    const-string p2, "ISO-8859-1"

    .line 153
    :cond_0
    new-instance v0, Lorg/htmlparser/lexer/InputStreamSource;

    invoke-direct {v0, p1, p2}, Lorg/htmlparser/lexer/InputStreamSource;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    .line 154
    new-instance p1, Lorg/htmlparser/lexer/PageIndex;

    invoke-direct {p1, p0}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    const/4 p1, 0x0

    .line 155
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    .line 156
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    .line 157
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-void

    .line 150
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "stream cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 188
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    const-string p2, "ISO-8859-1"

    .line 173
    :cond_0
    new-instance v0, Lorg/htmlparser/lexer/StringSource;

    invoke-direct {v0, p1, p2}, Lorg/htmlparser/lexer/StringSource;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    .line 174
    new-instance p1, Lorg/htmlparser/lexer/PageIndex;

    invoke-direct {p1, p0}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    const/4 p1, 0x0

    .line 175
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    .line 176
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    .line 177
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-void

    .line 170
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "text cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Ljava/net/URLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 133
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/Page;->setConnection(Ljava/net/URLConnection;)V

    const/4 p1, 0x0

    .line 134
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-void

    .line 132
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "connection cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Source;)V
    .locals 1

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 199
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    .line 200
    new-instance p1, Lorg/htmlparser/lexer/PageIndex;

    invoke-direct {p1, p0}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    const/4 p1, 0x0

    .line 201
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    .line 202
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    .line 203
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-void

    .line 198
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "source cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static findCharset(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    :try_start_0
    const-string v0, "java.nio.charset.Charset"

    .line 330
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "forName"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    .line 331
    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v5

    .line 332
    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "name"

    new-array v3, v5, [Ljava/lang/Class;

    .line 333
    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    .line 334
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 335
    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    goto :goto_0

    .line 358
    :catch_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 359
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to determine cannonical charset name for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 360
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " - using "

    .line 361
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 358
    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object p0, p1

    :catch_1
    :goto_0
    return-object p0
.end method

.method public static getConnectionManager()Lorg/htmlparser/http/ConnectionManager;
    .locals 1

    .line 216
    sget-object v0, Lorg/htmlparser/lexer/Page;->mConnectionManager:Lorg/htmlparser/http/ConnectionManager;

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 435
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 438
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 439
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 440
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 442
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 444
    new-instance p1, Ljava/net/URL;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 447
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/Page;->setConnection(Ljava/net/URLConnection;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 451
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_0
    :goto_0
    new-instance p1, Lorg/htmlparser/lexer/Cursor;

    const/4 v2, 0x0

    invoke-direct {p1, p0, v2}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    :goto_1
    if-lt v2, v0, :cond_1

    .line 464
    invoke-virtual {p0, v1}, Lorg/htmlparser/lexer/Page;->setUrl(Ljava/lang/String;)V

    goto :goto_2

    .line 458
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C
    :try_end_1
    .catch Lorg/htmlparser/util/ParserException; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 462
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_2
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 469
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 470
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Page;->setUrl(Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method public static setConnectionManager(Lorg/htmlparser/http/ConnectionManager;)V
    .locals 0

    .line 225
    sput-object p0, Lorg/htmlparser/lexer/Page;->mConnectionManager:Lorg/htmlparser/http/ConnectionManager;

    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 389
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 391
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 392
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 393
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 394
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 395
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Page;->setUrl(Ljava/lang/String;)V

    .line 396
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    .line 397
    iput-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    .line 398
    iget-object v2, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    .line 399
    iput-object v1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    .line 400
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 401
    iput-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    .line 402
    iput-object v2, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 406
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 407
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 408
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 409
    invoke-virtual {p0, v1}, Lorg/htmlparser/lexer/Page;->setUrl(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 411
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Page;->setUrl(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 489
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->destroy()V

    :cond_0
    return-void
.end method

.method public column(I)I
    .locals 1

    .line 1034
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->column(I)I

    move-result p1

    return p1
.end method

.method public column(Lorg/htmlparser/lexer/Cursor;)I
    .locals 1

    .line 1024
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->column(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    return p1
.end method

.method public constructUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 879
    invoke-virtual {p0, p1, p2, v0}, Lorg/htmlparser/lexer/Page;->constructUrl(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;

    move-result-object p1

    return-object p1
.end method

.method public constructUrl(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-nez p3, :cond_1

    .line 904
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result p3

    const/16 v2, 0x3f

    if-ne v2, p3, :cond_1

    .line 906
    invoke-virtual {p2, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p3

    if-eq v0, p3, :cond_0

    .line 907
    invoke-virtual {p2, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 908
    :cond_0
    new-instance p3, Ljava/net/URL;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v2, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 911
    :cond_1
    new-instance p3, Ljava/net/URL;

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {p3, v2, p1}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 912
    :goto_0
    invoke-virtual {p3}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object p2

    const-string v2, "/"

    .line 914
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x1

    if-nez p1, :cond_5

    const/4 p1, 0x0

    :goto_1
    const-string v3, "/."

    .line 918
    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_3

    :cond_2
    const-string v4, "/../"

    .line 920
    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 p1, 0x3

    .line 922
    invoke-virtual {p2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    :goto_2
    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const-string v4, "/./"

    .line 925
    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    const/4 p1, 0x2

    .line 927
    invoke-virtual {p2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :cond_6
    :goto_3
    const-string v3, "/\\"

    .line 935
    invoke-virtual {p2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v0, v3, :cond_8

    if-eqz p1, :cond_7

    .line 941
    new-instance p1, Ljava/net/URL;

    invoke-direct {p1, p3, p2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move-object p1, p3

    :goto_4
    return-object p1

    .line 937
    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p1, 0x1

    goto :goto_3
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 503
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->close()V

    return-void
.end method

.method public getAbsoluteURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 954
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/lexer/Page;->getAbsoluteURL(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAbsoluteURL(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p1, :cond_3

    .line 973
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 978
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 980
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    goto :goto_1

    .line 985
    :cond_2
    invoke-virtual {p0, p1, v0, p2}, Lorg/htmlparser/lexer/Page;->constructUrl(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;

    move-result-object p2

    .line 986
    invoke-virtual {p2}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_3
    :goto_0
    move-object p1, v0

    :catch_0
    :goto_1
    return-object p1
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .line 632
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getCharacter(Lorg/htmlparser/lexer/Cursor;)C
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 699
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    .line 700
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v1

    const-string v2, "can\'t read a character at position "

    const/4 v3, -0x1

    const-string v4, "problem reading a character at position "

    if-ne v1, v0, :cond_1

    .line 704
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->read()I

    move-result v0

    if-ne v3, v0, :cond_0

    const v0, 0xffff

    goto :goto_0

    :cond_0
    int-to-char v0, v0

    .line 710
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->advance()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 715
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 716
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 717
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 716
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 715
    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    if-le v1, v0, :cond_7

    .line 724
    :try_start_1
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Source;->getCharacter(I)C

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 732
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->advance()V

    :goto_0
    const/16 v1, 0xd

    const/16 v5, 0xa

    if-ne v1, v0, :cond_5

    .line 746
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 749
    :try_start_2
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->read()I

    move-result v0

    if-eq v3, v0, :cond_4

    int-to-char v0, v0

    if-ne v5, v0, :cond_2

    .line 755
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->advance()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 759
    :cond_2
    :try_start_3
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->unread()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 763
    :try_start_4
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 764
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can\'t unread a character at position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 765
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 764
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 763
    invoke-direct {v1, v2, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    .line 770
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 771
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 772
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 771
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 770
    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 777
    :cond_3
    :try_start_5
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Source;->getCharacter(I)C

    move-result v0

    if-ne v5, v0, :cond_4

    .line 778
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->advance()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_1
    const/16 v0, 0xa

    goto :goto_2

    :catch_3
    move-exception v0

    .line 782
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 783
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 784
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 783
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 782
    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    :goto_2
    if-ne v5, v0, :cond_6

    .line 789
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v1, p1}, Lorg/htmlparser/lexer/PageIndex;->add(Lorg/htmlparser/lexer/Cursor;)I

    :cond_6
    return v0

    :catch_4
    move-exception p1

    .line 728
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 729
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 730
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 729
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 728
    invoke-direct {v1, v0, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 736
    :cond_7
    new-instance p1, Lorg/htmlparser/util/ParserException;

    .line 737
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attempt to read future characters from source "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 738
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " > "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 737
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 736
    invoke-direct {p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCharset(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 259
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    if-nez v0, :cond_0

    const-string v0, "ISO-8859-1"

    goto :goto_0

    .line 264
    :cond_0
    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->getEncoding()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz p1, :cond_4

    const-string v1, "charset"

    .line 267
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    add-int/lit8 v1, v1, 0x7

    .line 271
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 272
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v1, "="

    .line 273
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 275
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v3, ";"

    .line 276
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v2, :cond_1

    const/4 v2, 0x0

    .line 278
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_1
    const-string v2, "\""

    .line 281
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 282
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 283
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_2
    const-string v2, "\'"

    .line 286
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 287
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 288
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 290
    :cond_3
    invoke-static {p1, v0}, Lorg/htmlparser/lexer/Page;->findCharset(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    return-object v0
.end method

.method public getConnection()Ljava/net/URLConnection;
    .locals 1

    .line 513
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 2

    .line 664
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Content-Type"

    .line 669
    invoke-virtual {v0, v1}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "text/html"

    :goto_0
    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .line 836
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->getEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLine(I)Ljava/lang/String;
    .locals 1

    .line 1219
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-direct {v0, p0, p1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Page;->getLine(Lorg/htmlparser/lexer/Cursor;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLine(Lorg/htmlparser/lexer/Cursor;)Ljava/lang/String;
    .locals 2

    .line 1190
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/Page;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    .line 1191
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/PageIndex;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1194
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v1, p1}, Lorg/htmlparser/lexer/PageIndex;->elementAt(I)I

    move-result v1

    add-int/lit8 p1, p1, 0x1

    if-gt p1, v0, :cond_0

    .line 1197
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->elementAt(I)I

    move-result p1

    goto :goto_0

    .line 1199
    :cond_0
    iget-object p1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result p1

    goto :goto_0

    .line 1203
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->elementAt(I)I

    move-result v1

    .line 1204
    iget-object p1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result p1

    .line 1208
    :goto_0
    invoke-virtual {p0, v1, p1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSource()Lorg/htmlparser/lexer/Source;
    .locals 1

    .line 650
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .line 1122
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText(II)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 1056
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    sub-int v1, p2, p1

    invoke-virtual {v0, p1, v1}, Lorg/htmlparser/lexer/Source;->getString(II)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v0

    .line 1060
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 1061
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can\'t get the "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-int/2addr p2, p1

    .line 1062
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "characters at position "

    .line 1063
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1064
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - "

    .line 1065
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1066
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1061
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1060
    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getText(Ljava/lang/StringBuffer;)V
    .locals 2

    .line 1132
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    return-void
.end method

.method public getText(Ljava/lang/StringBuffer;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 1088
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    if-lt v0, p2, :cond_1

    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    if-lt v0, p3, :cond_1

    if-ge p3, p2, :cond_0

    goto :goto_0

    :cond_0
    move v3, p3

    move p3, p2

    move p2, v3

    :goto_0
    sub-int/2addr p2, p3

    .line 1101
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0, p1, p3, p2}, Lorg/htmlparser/lexer/Source;->getCharacters(Ljava/lang/StringBuffer;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 1105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 1106
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "can\'t get the "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1107
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "characters at position "

    .line 1108
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1109
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " - "

    .line 1110
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1111
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1105
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1089
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 1090
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "attempt to extract future characters from source"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1091
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "|"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " > "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1090
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1089
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getText([CIII)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 1152
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    if-lt v0, p3, :cond_1

    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    if-lt v0, p4, :cond_1

    if-ge p4, p3, :cond_0

    goto :goto_0

    :cond_0
    move v2, p4

    move p4, p3

    move p3, v2

    :goto_0
    sub-int v0, p3, p4

    .line 1163
    :try_start_0
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v1, p1, p2, p4, p3}, Lorg/htmlparser/lexer/Source;->getCharacters([CIII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 1167
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 1168
    new-instance p3, Ljava/lang/StringBuilder;

    const-string v1, "can\'t get the "

    invoke-direct {p3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1169
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "characters at position "

    .line 1170
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1171
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, " - "

    .line 1172
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1173
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1168
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1167
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 1153
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "attempt to extract future characters from source"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .line 612
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .line 479
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->reset()V

    .line 480
    new-instance v0, Lorg/htmlparser/lexer/PageIndex;

    invoke-direct {v0, p0}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    iput-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    return-void
.end method

.method public row(I)I
    .locals 1

    .line 1014
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->row(I)I

    move-result p1

    return p1
.end method

.method public row(Lorg/htmlparser/lexer/Cursor;)I
    .locals 1

    .line 1004
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/PageIndex;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result p1

    return p1
.end method

.method public setBaseUrl(Ljava/lang/String;)V
    .locals 0

    .line 641
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mBaseUrl:Ljava/lang/String;

    return-void
.end method

.method public setConnection(Ljava/net/URLConnection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const-string v0, ")."

    const-string v1, " ("

    .line 535
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    .line 538
    :try_start_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 551
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getContentType()Ljava/lang/String;

    move-result-object v2

    .line 552
    invoke-virtual {p0, v2}, Lorg/htmlparser/lexer/Page;->getCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 555
    :try_start_1
    invoke-virtual {p1}, Ljava/net/URLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    if-eqz v3, :cond_0

    const-string v5, "gzip"

    .line 557
    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-eq v4, v5, :cond_0

    .line 559
    new-instance v3, Lorg/htmlparser/lexer/Stream;

    new-instance v4, Ljava/util/zip/GZIPInputStream;

    .line 560
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 559
    invoke-direct {v3, v4}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_1

    const-string v5, "deflate"

    .line 563
    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v4, v3, :cond_1

    .line 565
    new-instance v3, Lorg/htmlparser/lexer/Stream;

    new-instance v4, Ljava/util/zip/InflaterInputStream;

    .line 566
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    new-instance v6, Ljava/util/zip/Inflater;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v4, v5, v6}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    .line 565
    invoke-direct {v3, v4}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    .line 570
    :cond_1
    new-instance v3, Lorg/htmlparser/lexer/Stream;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 575
    :goto_0
    :try_start_2
    new-instance v4, Lorg/htmlparser/lexer/InputStreamSource;

    invoke-direct {v4, v3, v2}, Lorg/htmlparser/lexer/InputStreamSource;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v4, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_0
    :try_start_3
    const-string v2, "ISO-8859-1"

    .line 589
    new-instance v4, Lorg/htmlparser/lexer/InputStreamSource;

    invoke-direct {v4, v3, v2}, Lorg/htmlparser/lexer/InputStreamSource;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v4, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 598
    :goto_1
    invoke-virtual {p1}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object p1

    invoke-virtual {p1}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    .line 599
    new-instance p1, Lorg/htmlparser/lexer/PageIndex;

    invoke-direct {p1, p0}, Lorg/htmlparser/lexer/PageIndex;-><init>(Lorg/htmlparser/lexer/Page;)V

    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mIndex:Lorg/htmlparser/lexer/PageIndex;

    return-void

    :catch_1
    move-exception p1

    .line 594
    new-instance v2, Lorg/htmlparser/util/ParserException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception getting input stream from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 595
    iget-object v4, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    invoke-virtual {v4}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 594
    invoke-direct {v2, v0, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception p1

    .line 547
    new-instance v2, Lorg/htmlparser/util/ParserException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception connecting to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 548
    iget-object v4, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    invoke-virtual {v4}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 547
    invoke-direct {v2, v0, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_3
    move-exception p1

    .line 542
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connect to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 543
    iget-object v2, p0, Lorg/htmlparser/lexer/Page;->mConnection:Ljava/net/URLConnection;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 542
    invoke-direct {v0, v1, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 864
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Page;->getSource()Lorg/htmlparser/lexer/Source;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/Source;->setEncoding(Ljava/lang/String;)V

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .line 623
    iput-object p1, p0, Lorg/htmlparser/lexer/Page;->mUrl:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1232
    iget-object v0, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v0

    if-lez v0, :cond_1

    .line 1234
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x2b

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1235
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v1

    add-int/lit8 v1, v1, -0x28

    if-gez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const-string v2, "..."

    .line 1239
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1240
    :goto_0
    iget-object v2, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v2}, Lorg/htmlparser/lexer/Source;->offset()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    .line 1241
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1244
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 810
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->retreat()V

    .line 811
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    .line 814
    :try_start_0
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Source;->getCharacter(I)C

    move-result v1

    const/16 v2, 0xa

    if-ne v2, v1, :cond_0

    if-eqz v0, :cond_0

    .line 817
    iget-object v1, p0, Lorg/htmlparser/lexer/Page;->mSource:Lorg/htmlparser/lexer/Source;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Source;->getCharacter(I)C

    move-result v0

    const/16 v1, 0xd

    if-ne v1, v0, :cond_0

    .line 819
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->retreat()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    .line 824
    new-instance v1, Lorg/htmlparser/util/ParserException;

    .line 825
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can\'t read a character at position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 826
    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 825
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 824
    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
