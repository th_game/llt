.class public Lorg/htmlparser/lexer/PageAttribute;
.super Lorg/htmlparser/Attribute;
.source "PageAttribute.java"


# instance fields
.field protected mNameEnd:I

.field protected mNameStart:I

.field protected mPage:Lorg/htmlparser/lexer/Page;

.field protected mValueEnd:I

.field protected mValueStart:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 190
    invoke-direct {p0}, Lorg/htmlparser/Attribute;-><init>()V

    .line 191
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 152
    invoke-direct {p0, p1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    .line 153
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 166
    invoke-direct {p0, p1, p2}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;C)V
    .locals 0

    .line 138
    invoke-direct {p0, p1, p2, p3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    .line 139
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 179
    invoke-direct {p0, p1, p2, p3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;C)V

    .line 124
    invoke-direct {p0}, Lorg/htmlparser/lexer/PageAttribute;->init()V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;IIIIC)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lorg/htmlparser/Attribute;-><init>()V

    .line 88
    iput-object p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    .line 89
    iput p2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    .line 90
    iput p3, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    .line 91
    iput p4, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    .line 92
    iput p5, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    const/4 p1, 0x0

    .line 93
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setName(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setAssignment(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setValue(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0, p6}, Lorg/htmlparser/lexer/PageAttribute;->setQuote(C)V

    return-void
.end method

.method private init()V
    .locals 1

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, -0x1

    .line 106
    iput v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    .line 107
    iput v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    .line 108
    iput v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    .line 109
    iput v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    return-void
.end method


# virtual methods
.method public getAssignment()Ljava/lang/String;
    .locals 4

    .line 247
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 250
    iget-object v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v1, :cond_2

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    if-ltz v2, :cond_2

    iget v3, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-ltz v3, :cond_2

    .line 252
    invoke-virtual {v1, v2, v3}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    .line 255
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 256
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 257
    :cond_1
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageAttribute;->setAssignment(Ljava/lang/String;)V

    :cond_2
    return-object v0
.end method

.method public getAssignment(Ljava/lang/StringBuffer;)V
    .locals 3

    .line 275
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 278
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_2

    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    if-ltz v1, :cond_2

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-ltz v2, :cond_2

    .line 280
    invoke-virtual {v0, p1, v1, v2}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    .line 283
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 284
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    const/16 v2, 0x27

    if-eq v2, v1, :cond_0

    const/16 v2, 0x22

    if-ne v2, v1, :cond_2

    .line 286
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    :goto_0
    return-void
.end method

.method public getLength()I
    .locals 3

    .line 563
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 565
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_0

    .line 566
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    if-ltz v0, :cond_1

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    if-ltz v2, :cond_1

    sub-int/2addr v2, v0

    add-int/2addr v1, v2

    .line 568
    :cond_1
    :goto_0
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 570
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_1

    .line 571
    :cond_2
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_3

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    if-ltz v0, :cond_3

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-ltz v2, :cond_3

    sub-int/2addr v2, v0

    add-int/2addr v1, v2

    .line 573
    :cond_3
    :goto_1
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 575
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_2

    .line 576
    :cond_4
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_5

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-ltz v0, :cond_5

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-ltz v2, :cond_5

    sub-int/2addr v2, v0

    add-int/2addr v1, v2

    .line 578
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->getQuote()C

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v1, v1, 0x2

    :cond_6
    return v1
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .line 205
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 208
    iget-object v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v1, :cond_0

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    if-ltz v2, :cond_0

    .line 210
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    invoke-virtual {v1, v2, v0}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageAttribute;->setName(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getName(Ljava/lang/StringBuffer;)V
    .locals 3

    .line 227
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_1

    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    if-ltz v1, :cond_1

    .line 231
    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    invoke-virtual {v0, p1, v1, v2}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    goto :goto_0

    .line 234
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    :goto_0
    return-void
.end method

.method public getNameEndPosition()I
    .locals 1

    .line 444
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    return v0
.end method

.method public getNameStartPosition()I
    .locals 1

    .line 425
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    return v0
.end method

.method public getPage()Lorg/htmlparser/lexer/Page;
    .locals 1

    .line 405
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    return-object v0
.end method

.method public getRawValue()Ljava/lang/String;
    .locals 4

    .line 352
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->getQuote()C

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 356
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 357
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 358
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 359
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getRawValue(Ljava/lang/StringBuffer;)V
    .locals 4

    .line 376
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValue:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 378
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-ltz v0, :cond_4

    .line 380
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->getQuote()C

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 382
    :cond_0
    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-eq v1, v2, :cond_1

    .line 383
    iget-object v3, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {v3, p1, v1, v2}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    :cond_1
    if-eqz v0, :cond_4

    .line 385
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 390
    :cond_2
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->getQuote()C

    move-result v0

    if-eqz v0, :cond_3

    .line 391
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 392
    :cond_3
    iget-object v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v0, :cond_4

    .line 394
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_4
    :goto_0
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 3

    .line 307
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 310
    iget-object v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v1, :cond_0

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-ltz v2, :cond_0

    .line 312
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    invoke-virtual {v1, v0, v2}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/PageAttribute;->setValue(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getValue(Ljava/lang/StringBuffer;)V
    .locals 3

    .line 329
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 332
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_1

    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-ltz v1, :cond_1

    .line 333
    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    iget v2, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    invoke-virtual {v0, p1, v1, v2}, Lorg/htmlparser/lexer/Page;->getText(Ljava/lang/StringBuffer;II)V

    goto :goto_0

    .line 336
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    :goto_0
    return-void
.end method

.method public getValueEndPosition()I
    .locals 1

    .line 484
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    return v0
.end method

.method public getValueStartPosition()I
    .locals 1

    .line 464
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 530
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->isWhitespace()Z

    move-result v0

    if-nez v0, :cond_1

    .line 531
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->isStandAlone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 532
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 533
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    .line 535
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isStandAlone()Z
    .locals 1

    .line 515
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->isWhitespace()Z

    move-result v0

    if-nez v0, :cond_1

    .line 516
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getAssignment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 517
    invoke-virtual {p0}, Lorg/htmlparser/lexer/PageAttribute;->isValued()Z

    move-result v0

    if-nez v0, :cond_1

    .line 518
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    .line 520
    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    if-ltz v0, :cond_1

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isValued()Z
    .locals 2

    .line 545
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 547
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    if-ltz v0, :cond_0

    iget v1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    if-ltz v1, :cond_0

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public isWhitespace()Z
    .locals 1

    .line 504
    invoke-super {p0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_1

    .line 505
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v0, :cond_2

    iget v0, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    if-ltz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public setNameEndPosition(I)V
    .locals 0

    .line 453
    iput p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameEnd:I

    const/4 p1, 0x0

    .line 454
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setName(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setAssignment(Ljava/lang/String;)V

    return-void
.end method

.method public setNameStartPosition(I)V
    .locals 0

    .line 434
    iput p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mNameStart:I

    const/4 p1, 0x0

    .line 435
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setName(Ljava/lang/String;)V

    return-void
.end method

.method public setPage(Lorg/htmlparser/lexer/Page;)V
    .locals 0

    .line 416
    iput-object p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mPage:Lorg/htmlparser/lexer/Page;

    return-void
.end method

.method public setValueEndPosition(I)V
    .locals 0

    .line 493
    iput p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueEnd:I

    const/4 p1, 0x0

    .line 494
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setValueStartPosition(I)V
    .locals 0

    .line 473
    iput p1, p0, Lorg/htmlparser/lexer/PageAttribute;->mValueStart:I

    const/4 p1, 0x0

    .line 474
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setAssignment(Ljava/lang/String;)V

    .line 475
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/PageAttribute;->setValue(Ljava/lang/String;)V

    return-void
.end method
