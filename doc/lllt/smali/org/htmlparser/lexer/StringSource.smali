.class public Lorg/htmlparser/lexer/StringSource;
.super Lorg/htmlparser/lexer/Source;
.source "StringSource.java"


# instance fields
.field protected mEncoding:Ljava/lang/String;

.field protected mMark:I

.field protected mOffset:I

.field protected mString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ISO-8859-1"

    .line 66
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/lexer/StringSource;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Lorg/htmlparser/lexer/Source;-><init>()V

    if-nez p1, :cond_0

    const-string p1, ""

    .line 77
    :cond_0
    iput-object p1, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    const/4 p1, 0x0

    .line 78
    iput p1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    .line 79
    iput-object p2, p0, Lorg/htmlparser/lexer/StringSource;->mEncoding:Ljava/lang/String;

    const/4 p1, -0x1

    .line 80
    iput p1, p0, Lorg/htmlparser/lexer/StringSource;->mMark:I

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    .line 430
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 433
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    sub-int/2addr v0, v1

    :goto_0
    return v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public destroy()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 401
    iput-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    return-void
.end method

.method public getCharacter(I)C
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 307
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 309
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-ge p1, v1, :cond_0

    .line 312
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    return p1

    .line 310
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "read beyond current offset"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 308
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "source is closed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCharacters(Ljava/lang/StringBuffer;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 378
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    add-int/2addr p3, p2

    .line 382
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-gt p3, v1, :cond_0

    .line 385
    invoke-virtual {v0, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void

    .line 383
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "read beyond end of string"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 379
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCharacters([CIII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 330
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 334
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-gt p4, v1, :cond_0

    .line 337
    invoke-virtual {v0, p3, p4, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    return-void

    .line 335
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "read beyond current offset"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 331
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getString(II)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    add-int/2addr p2, p1

    .line 358
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-gt p2, v1, :cond_0

    .line 361
    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 359
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "read beyond end of string"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 355
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public mark(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    iget-object p1, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 241
    iget p1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    iput p1, p0, Lorg/htmlparser/lexer/StringSource;->mMark:I

    return-void

    .line 240
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "source is closed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public offset()I
    .locals 1

    .line 413
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 416
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    :goto_0
    return v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 131
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v1, v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 136
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    :goto_0
    return v0

    .line 130
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([C)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 186
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/htmlparser/lexer/StringSource;->read([CII)I

    move-result p1

    return p1
.end method

.method public read([CII)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 160
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 161
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-lt v1, v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    sub-int v2, v0, v1

    if-le p3, v2, :cond_1

    sub-int p3, v0, v1

    .line 167
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    add-int v2, v1, p3

    invoke-virtual {v0, v1, v2, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    .line 168
    iget p1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    add-int/2addr p1, p3

    iput p1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    move p1, p3

    :goto_0
    return p1

    .line 157
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public ready()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 199
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    .line 198
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    .line 214
    iget v1, p0, Lorg/htmlparser/lexer/StringSource;->mMark:I

    if-eq v0, v1, :cond_0

    .line 215
    iput v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 217
    iput v0, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    :goto_0
    return-void

    .line 212
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 102
    iput-object p1, p0, Lorg/htmlparser/lexer/StringSource;->mEncoding:Ljava/lang/String;

    return-void
.end method

.method public skip(J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 260
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-wide/16 v1, 0x0

    cmp-long v3, v1, p1

    if-gtz v3, :cond_2

    .line 266
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 267
    iget v3, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-lt v3, v0, :cond_0

    move-wide p1, v1

    goto :goto_0

    :cond_0
    sub-int v1, v0, v3

    int-to-long v1, v1

    cmp-long v4, p1, v1

    if-lez v4, :cond_1

    sub-int/2addr v0, v3

    int-to-long p1, v0

    .line 271
    :cond_1
    :goto_0
    iget v0, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v1, v0

    iput v1, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    return-wide p1

    .line 263
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "cannot skip backwards"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 261
    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public unread()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 288
    iget-object v0, p0, Lorg/htmlparser/lexer/StringSource;->mString:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 290
    iget v0, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 293
    iput v0, p0, Lorg/htmlparser/lexer/StringSource;->mOffset:I

    return-void

    .line 291
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "can\'t unread no characters"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
