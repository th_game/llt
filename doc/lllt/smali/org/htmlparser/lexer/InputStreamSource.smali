.class public Lorg/htmlparser/lexer/InputStreamSource;
.super Lorg/htmlparser/lexer/Source;
.source "InputStreamSource.java"


# static fields
.field public static BUFFER_SIZE:I = 0x4000


# instance fields
.field protected mBuffer:[C

.field protected mEncoding:Ljava/lang/String;

.field protected mLevel:I

.field protected mMark:I

.field protected mOffset:I

.field protected transient mReader:Ljava/io/InputStreamReader;

.field protected transient mStream:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 98
    sget v0, Lorg/htmlparser/lexer/InputStreamSource;->BUFFER_SIZE:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lorg/htmlparser/lexer/InputStreamSource;-><init>(Ljava/io/InputStream;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 112
    sget v0, Lorg/htmlparser/lexer/InputStreamSource;->BUFFER_SIZE:I

    invoke-direct {p0, p1, p2, v0}, Lorg/htmlparser/lexer/InputStreamSource;-><init>(Ljava/io/InputStream;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 123
    invoke-direct {p0}, Lorg/htmlparser/lexer/Source;-><init>()V

    if-nez p1, :cond_0

    .line 128
    new-instance p1, Lorg/htmlparser/lexer/Stream;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    .line 131
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    new-instance v0, Lorg/htmlparser/lexer/Stream;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;)V

    move-object p1, v0

    .line 144
    :cond_1
    :goto_0
    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-nez p2, :cond_2

    .line 147
    new-instance p2, Ljava/io/InputStreamReader;

    invoke-direct {p2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    .line 148
    iget-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Ljava/io/InputStreamReader;->getEncoding()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mEncoding:Ljava/lang/String;

    goto :goto_1

    .line 152
    :cond_2
    iput-object p2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mEncoding:Ljava/lang/String;

    .line 153
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    .line 155
    :goto_1
    new-array p1, p3, [C

    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    const/4 p1, 0x0

    .line 156
    iput p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    .line 157
    iput p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    const/4 p1, -0x1

    .line 158
    iput p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mMark:I

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 200
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 201
    iget-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    if-eqz p1, :cond_0

    .line 203
    new-instance p1, Ljava/io/ByteArrayInputStream;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-direct {p1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 180
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    const/16 v1, 0x1000

    new-array v1, v1, [C

    :cond_0
    const/4 v2, -0x1

    .line 182
    invoke-virtual {p0, v1}, Lorg/htmlparser/lexer/InputStreamSource;->read([C)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 184
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    .line 187
    :cond_1
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    .line 691
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 694
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    sub-int/2addr v0, v1

    :goto_0
    return v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public destroy()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 655
    iput-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    .line 656
    iget-object v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    if-eqz v1, :cond_0

    .line 657
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    .line 658
    :cond_0
    iput-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    .line 659
    iput-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    const/4 v0, 0x0

    .line 660
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    .line 661
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    const/4 v0, -0x1

    .line 662
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mMark:I

    return-void
.end method

.method protected fill(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    if-eqz v0, :cond_4

    .line 326
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    array-length v1, v0

    iget v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    sub-int/2addr v1, v2

    if-ge v1, p1, :cond_1

    .line 330
    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int v1, v2, p1

    if-ge v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    sub-int p1, v0, v2

    :goto_0
    move v1, p1

    .line 336
    new-array v0, v0, [C

    .line 345
    :cond_1
    iget-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    iget v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/InputStreamReader;->read([CII)I

    move-result p1

    const/4 v1, -0x1

    if-ne v1, p1, :cond_2

    .line 348
    iget-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Ljava/io/InputStreamReader;->close()V

    const/4 p1, 0x0

    .line 349
    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    goto :goto_1

    .line 353
    :cond_2
    iget-object v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    if-eq v1, v0, :cond_3

    .line 355
    iget v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 356
    iput-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    .line 358
    :cond_3
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    :cond_4
    :goto_1
    return-void
.end method

.method public getCharacter(I)C
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 578
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    array-length v1, v0

    if-ge p1, v1, :cond_0

    .line 583
    aget-char p1, v0, p1

    return p1

    .line 581
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "illegal read ahead"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 579
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "source is closed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCharacters(Ljava/lang/StringBuffer;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 638
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    invoke-virtual {p1, v0, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    return-void

    .line 639
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCharacters([CIII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 601
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    sub-int/2addr p4, p3

    invoke-static {v0, p3, p1, p2, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    .line 602
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .line 221
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getStream()Ljava/io/InputStream;
    .locals 1

    .line 212
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getString(II)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 618
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    add-int v0, p1, p2

    .line 620
    iget-object v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    array-length v2, v1

    if-gt v0, v2, :cond_0

    .line 623
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    .line 621
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "illegal read ahead"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 619
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public mark(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 492
    iget-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz p1, :cond_0

    .line 494
    iget p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    iput p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mMark:I

    return-void

    .line 493
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "source is closed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public offset()I
    .locals 1

    .line 674
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 677
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    :goto_0
    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 391
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    sub-int/2addr v0, v1

    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    .line 393
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {p0, v2}, Lorg/htmlparser/lexer/InputStreamSource;->fill(I)V

    .line 396
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    if-lt v0, v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 399
    :cond_0
    iget-object v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    aget-char v0, v1, v0

    goto :goto_0

    .line 394
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_2
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    aget-char v0, v0, v1

    :goto_0
    return v0
.end method

.method public read([C)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 453
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/htmlparser/lexer/InputStreamSource;->read([CII)I

    move-result p1

    return p1
.end method

.method public read([CII)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 422
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_3

    if-ltz p2, :cond_3

    if-gez p3, :cond_0

    goto :goto_1

    .line 428
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    sub-int v2, v0, v1

    if-ge v2, p3, :cond_1

    sub-int/2addr v0, v1

    sub-int v0, p3, v0

    .line 429
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/InputStreamSource;->fill(I)V

    .line 430
    :cond_1
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    if-lt v0, v1, :cond_2

    const/4 p1, -0x1

    goto :goto_0

    :cond_2
    sub-int/2addr v1, v0

    .line 434
    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 435
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 436
    iget p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    add-int/2addr p1, p3

    iput p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    move p1, p3

    :goto_0
    return p1

    .line 425
    :cond_3
    :goto_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal argument read ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_4

    const-string p1, "null"

    goto :goto_2

    :cond_4
    const-string p1, "cbuf"

    .line 426
    :goto_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", "

    .line 427
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 425
    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_5
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public ready()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 506
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 508
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    .line 507
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 465
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    .line 467
    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mMark:I

    if-eq v0, v1, :cond_0

    .line 468
    iput v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 470
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    :goto_0
    return-void

    .line 466
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const-string v0, " [0x"

    .line 255
    invoke-virtual {p0}, Lorg/htmlparser/lexer/InputStreamSource;->getEncoding()Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 258
    invoke-virtual {p0}, Lorg/htmlparser/lexer/InputStreamSource;->getStream()Ljava/io/InputStream;

    move-result-object v2

    .line 261
    :try_start_0
    iget-object v3, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    .line 262
    iget v4, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    .line 263
    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 266
    :try_start_1
    iput-object p1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mEncoding:Ljava/lang/String;

    .line 267
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v5, p0, Lorg/htmlparser/lexer/InputStreamSource;->mReader:Ljava/io/InputStreamReader;

    .line 268
    iget-object v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    array-length v2, v2

    new-array v2, v2, [C

    iput-object v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mBuffer:[C

    const/4 v2, 0x0

    .line 269
    iput v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    .line 270
    iput v2, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    const/4 v5, -0x1

    .line 271
    iput v5, p0, Lorg/htmlparser/lexer/InputStreamSource;->mMark:I

    if-eqz v4, :cond_3

    .line 274
    new-array v5, v4, [C

    .line 275
    invoke-virtual {p0, v5}, Lorg/htmlparser/lexer/InputStreamSource;->read([C)I

    move-result v6

    if-ne v4, v6, :cond_2

    :goto_0
    if-lt v2, v4, :cond_0

    goto/16 :goto_1

    .line 278
    :cond_0
    aget-char v6, v5, v2

    aget-char v7, v3, v2

    if-ne v6, v7, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 279
    :cond_1
    new-instance v4, Lorg/htmlparser/util/EncodingChangeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "character mismatch (new: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 280
    aget-char v7, v5, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 281
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    aget-char v5, v5, v2

    const/16 v7, 0x10

    invoke-static {v5, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "] != old: "

    .line 283
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    aget-char v0, v3, v2

    invoke-static {v0, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    aget-char v0, v3, v2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "]) for encoding change from "

    .line 287
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " to "

    .line 289
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " at character offset "

    .line 291
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 279
    invoke-direct {v4, p1}, Lorg/htmlparser/util/EncodingChangeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 276
    :cond_2
    new-instance p1, Lorg/htmlparser/util/ParserException;

    const-string v0, "reset stream failed"

    invoke-direct {p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 297
    :try_start_2
    new-instance v0, Lorg/htmlparser/util/ParserException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    .line 302
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stream reset failed ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "), try wrapping it with a org.htmlparser.lexer.Stream"

    .line 304
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 302
    invoke-direct {v0, v1, p1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_3
    :goto_1
    return-void
.end method

.method public skip(J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 528
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v2, v0, p1

    if-gtz v2, :cond_2

    .line 534
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    sub-int v2, v0, v1

    int-to-long v2, v2

    cmp-long v4, v2, p1

    if-gez v4, :cond_0

    sub-int/2addr v0, v1

    int-to-long v0, v0

    sub-long v0, p1, v0

    long-to-int v1, v0

    .line 535
    invoke-virtual {p0, v1}, Lorg/htmlparser/lexer/InputStreamSource;->fill(I)V

    .line 536
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    iget v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mLevel:I

    if-lt v0, v1, :cond_1

    const-wide/16 p1, -0x1

    goto :goto_0

    :cond_1
    sub-int/2addr v1, v0

    int-to-long v0, v1

    .line 540
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    .line 541
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v1, v0

    iput v1, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    :goto_0
    return-wide p1

    .line 531
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "cannot skip backwards"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 529
    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string p2, "source is closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public unread()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 559
    iget-object v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 561
    iget v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 562
    iput v0, p0, Lorg/htmlparser/lexer/InputStreamSource;->mOffset:I

    return-void

    .line 564
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "can\'t unread no characters"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 560
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "source is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
