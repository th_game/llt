.class public Lorg/htmlparser/lexer/Stream;
.super Ljava/io/InputStream;
.source "Stream.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field protected static final BUFFER_SIZE:I = 0x1000

.field protected static final EOF:I = -0x1


# instance fields
.field public fills:I

.field public volatile mBuffer:[B

.field protected mContentLength:I

.field protected volatile mIn:Ljava/io/InputStream;

.field public volatile mLevel:I

.field protected mMark:I

.field protected mOffset:I

.field public reallocations:I

.field public synchronous:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/lexer/Stream;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .line 111
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->fills:I

    .line 47
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->reallocations:I

    .line 53
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->synchronous:I

    .line 113
    iput-object p1, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    const/4 p1, 0x0

    .line 114
    iput-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 115
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    .line 116
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    if-gez p2, :cond_0

    const/4 p2, 0x0

    .line 117
    :cond_0
    iput p2, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    const/4 p1, -0x1

    .line 118
    iput p1, p0, Lorg/htmlparser/lexer/Stream;->mMark:I

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 286
    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public declared-synchronized close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    .line 296
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 299
    iput-object v1, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    .line 301
    :cond_0
    iput-object v1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    const/4 v0, 0x0

    .line 302
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    .line 303
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    .line 304
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    const/4 v0, -0x1

    .line 305
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mMark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized fill(Z)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    .line 142
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_9

    if-nez p1, :cond_1

    .line 146
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Stream;->available()I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 147
    monitor-exit p0

    return v2

    .line 148
    :cond_0
    :try_start_1
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->synchronous:I

    add-int/2addr p1, v2

    iput p1, p0, Lorg/htmlparser/lexer/Stream;->synchronous:I

    .line 152
    :cond_1
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    if-nez p1, :cond_4

    .line 154
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    if-nez p1, :cond_2

    const/16 p1, 0x1000

    .line 156
    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 157
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    goto :goto_0

    .line 161
    :cond_2
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    array-length p1, p1

    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    sub-int/2addr p1, v0

    const/16 v0, 0x800

    if-ge p1, v0, :cond_3

    .line 162
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    array-length p1, p1

    mul-int/lit8 p1, p1, 0x2

    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    array-length v0, v0

    iget-object v3, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v3

    add-int/2addr v0, v3

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    new-array p1, p1, [B

    goto :goto_0

    .line 164
    :cond_3
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 166
    :goto_0
    array-length v0, p1

    iget v3, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    sub-int/2addr v0, v3

    goto :goto_1

    .line 170
    :cond_4
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    sub-int v0, p1, v0

    .line 171
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    if-nez p1, :cond_5

    .line 172
    new-array p1, v0, [B

    iput-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 173
    :cond_5
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 177
    :goto_1
    iget-object v3, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    iget v4, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    invoke-virtual {v3, p1, v4, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    const/4 v3, -0x1

    const/4 v4, 0x0

    if-ne v3, v0, :cond_6

    .line 180
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 181
    iput-object v4, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    goto :goto_2

    .line 185
    :cond_6
    iget-object v3, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    if-eq v3, p1, :cond_7

    .line 187
    iget-object v3, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    iget v5, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    invoke-static {v3, v1, p1, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 188
    iput-object p1, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    .line 189
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->reallocations:I

    add-int/2addr p1, v2

    iput p1, p0, Lorg/htmlparser/lexer/Stream;->reallocations:I

    .line 191
    :cond_7
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    add-int/2addr p1, v0

    iput p1, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    .line 192
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    if-eqz p1, :cond_8

    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mContentLength:I

    if-ne p1, v0, :cond_8

    .line 194
    iget-object p1, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 195
    iput-object v4, p0, Lorg/htmlparser/lexer/Stream;->mIn:Ljava/io/InputStream;

    .line 198
    :cond_8
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->fills:I

    add-int/2addr p1, v2

    iput p1, p0, Lorg/htmlparser/lexer/Stream;->fills:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x1

    .line 202
    :cond_9
    :goto_2
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public mark(I)V
    .locals 0

    .line 398
    iget p1, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    iput p1, p0, Lorg/htmlparser/lexer/Stream;->mMark:I

    return-void
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 265
    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 266
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Stream;->fill(Z)Z

    .line 267
    :cond_0
    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mLevel:I

    iget v1, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lorg/htmlparser/lexer/Stream;->mBuffer:[B

    iget v1, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 352
    iget v0, p0, Lorg/htmlparser/lexer/Stream;->mMark:I

    const/4 v1, -0x1

    if-eq v1, v0, :cond_0

    .line 353
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 355
    iput v0, p0, Lorg/htmlparser/lexer/Stream;->mOffset:I

    :goto_0
    return-void
.end method

.method public run()V
    .locals 1

    :cond_0
    const/4 v0, 0x1

    .line 221
    :try_start_0
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Stream;->fill(Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 225
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    return-void
.end method
