.class public Lorg/htmlparser/lexer/Lexer;
.super Ljava/lang/Object;
.source "Lexer.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/htmlparser/NodeFactory;


# static fields
.field public static STRICT_REMARKS:Z = true

.field public static final VERSION_DATE:Ljava/lang/String; = "Sep 17, 2006"

.field public static final VERSION_NUMBER:D = 2.0

.field public static final VERSION_STRING:Ljava/lang/String; = "2.0 (Release Build Sep 17, 2006)"

.field public static final VERSION_TYPE:Ljava/lang/String; = "Release Build"

.field protected static mDebugLineTrigger:I = -0x1


# instance fields
.field protected mCursor:Lorg/htmlparser/lexer/Cursor;

.field protected mFactory:Lorg/htmlparser/NodeFactory;

.field protected mPage:Lorg/htmlparser/lexer/Page;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 153
    new-instance v0, Lorg/htmlparser/lexer/Page;

    const-string v1, ""

    invoke-direct {v0, v1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 173
    new-instance v0, Lorg/htmlparser/lexer/Page;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    return-void
.end method

.method public constructor <init>(Ljava/net/URLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 185
    new-instance v0, Lorg/htmlparser/lexer/Page;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/net/URLConnection;)V

    invoke-direct {p0, v0}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;)V
    .locals 2

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    invoke-virtual {p0, p1}, Lorg/htmlparser/lexer/Lexer;->setPage(Lorg/htmlparser/lexer/Page;)V

    .line 163
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->setCursor(Lorg/htmlparser/lexer/Cursor;)V

    .line 164
    invoke-virtual {p0, p0}, Lorg/htmlparser/lexer/Lexer;->setNodeFactory(Lorg/htmlparser/NodeFactory;)V

    return-void
.end method

.method private double_quote(Ljava/util/Vector;[I)V
    .locals 8

    .line 921
    new-instance v7, Lorg/htmlparser/lexer/PageAttribute;

    .line 922
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, 0x1

    aget v2, p2, v0

    const/4 v3, 0x2

    aget v3, p2, v3

    const/4 v4, 0x5

    aget v4, p2, v4

    add-int/2addr v4, v0

    const/4 v0, 0x6

    .line 923
    aget v5, p2, v0

    const/16 v6, 0x22

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 921
    invoke-virtual {p1, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method private empty(Ljava/util/Vector;[I)V
    .locals 8

    .line 886
    new-instance v7, Lorg/htmlparser/lexer/PageAttribute;

    .line 887
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, 0x1

    aget v2, p2, v0

    const/4 v3, 0x2

    aget v4, p2, v3

    aget p2, p2, v3

    add-int/2addr p2, v0

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, v7

    move v3, v4

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 886
    invoke-virtual {p1, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "2.0 (Release Build Sep 17, 2006)"

    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 1665
    array-length v0, p0

    if-gtz v0, :cond_0

    .line 1667
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTML Lexer v"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lorg/htmlparser/lexer/Lexer;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1668
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    .line 1669
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "usage: java -jar htmllexer.jar <url>"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 1675
    :cond_0
    :try_start_0
    invoke-static {}, Lorg/htmlparser/lexer/Page;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v0

    .line 1676
    new-instance v1, Lorg/htmlparser/lexer/Lexer;

    const/4 v2, 0x0

    aget-object p0, p0, v2

    invoke-virtual {v0, p0}, Lorg/htmlparser/http/ConnectionManager;->openConnection(Ljava/lang/String;)Ljava/net/URLConnection;

    move-result-object p0

    invoke-direct {v1, p0}, Lorg/htmlparser/lexer/Lexer;-><init>(Ljava/net/URLConnection;)V

    .line 1677
    :goto_0
    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Lexer;->nextNode(Z)Lorg/htmlparser/Node;

    move-result-object p0

    if-nez p0, :cond_1

    goto :goto_1

    .line 1678
    :cond_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-interface {p0}, Lorg/htmlparser/Node;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 1682
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1683
    invoke-virtual {p0}, Lorg/htmlparser/util/ParserException;->getThrowable()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1684
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lorg/htmlparser/util/ParserException;->getThrowable()Ljava/lang/Throwable;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private naked(Ljava/util/Vector;[I)V
    .locals 8

    .line 897
    new-instance v7, Lorg/htmlparser/lexer/PageAttribute;

    .line 898
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, 0x1

    aget v2, p2, v0

    const/4 v0, 0x2

    aget v3, p2, v0

    const/4 v0, 0x3

    aget v4, p2, v0

    const/4 v0, 0x4

    .line 899
    aget v5, p2, v0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 897
    invoke-virtual {p1, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method private single_quote(Ljava/util/Vector;[I)V
    .locals 8

    .line 909
    new-instance v7, Lorg/htmlparser/lexer/PageAttribute;

    .line 910
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, 0x1

    aget v2, p2, v0

    const/4 v3, 0x2

    aget v3, p2, v3

    const/4 v4, 0x4

    aget v4, p2, v4

    add-int/2addr v4, v0

    const/4 v0, 0x5

    .line 911
    aget v5, p2, v0

    const/16 v6, 0x27

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 909
    invoke-virtual {p1, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method private standalone(Ljava/util/Vector;[I)V
    .locals 8

    .line 875
    new-instance v7, Lorg/htmlparser/lexer/PageAttribute;

    .line 876
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v0, 0x1

    aget v2, p2, v0

    const/4 v0, 0x2

    aget v3, p2, v0

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 875
    invoke-virtual {p1, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method private whitespace(Ljava/util/Vector;[I)V
    .locals 11

    const/4 v0, 0x1

    .line 863
    aget v1, p2, v0

    const/4 v2, 0x0

    aget v3, p2, v2

    if-le v1, v3, :cond_0

    .line 864
    new-instance v1, Lorg/htmlparser/lexer/PageAttribute;

    .line 865
    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v6, -0x1

    const/4 v7, -0x1

    aget v8, p2, v2

    aget v9, p2, v0

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    .line 864
    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public createRemarkNode(Lorg/htmlparser/lexer/Page;II)Lorg/htmlparser/Remark;
    .locals 1

    .line 646
    new-instance v0, Lorg/htmlparser/nodes/RemarkNode;

    invoke-direct {v0, p1, p2, p3}, Lorg/htmlparser/nodes/RemarkNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    return-object v0
.end method

.method public createStringNode(Lorg/htmlparser/lexer/Page;II)Lorg/htmlparser/Text;
    .locals 1

    .line 634
    new-instance v0, Lorg/htmlparser/nodes/TextNode;

    invoke-direct {v0, p1, p2, p3}, Lorg/htmlparser/nodes/TextNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    return-object v0
.end method

.method public createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;
    .locals 1

    .line 663
    new-instance v0, Lorg/htmlparser/nodes/TagNode;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/htmlparser/nodes/TagNode;-><init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V

    return-object v0
.end method

.method public getCurrentLine()Ljava/lang/String;
    .locals 2

    .line 288
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->getLine(Lorg/htmlparser/lexer/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentLineNumber()I
    .locals 2

    .line 279
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result v0

    return v0
.end method

.method public getCursor()Lorg/htmlparser/lexer/Cursor;
    .locals 1

    .line 219
    iget-object v0, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    return-object v0
.end method

.method public getNodeFactory()Lorg/htmlparser/NodeFactory;
    .locals 1

    .line 240
    iget-object v0, p0, Lorg/htmlparser/lexer/Lexer;->mFactory:Lorg/htmlparser/NodeFactory;

    return-object v0
.end method

.method public getPage()Lorg/htmlparser/lexer/Page;
    .locals 1

    .line 198
    iget-object v0, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .line 260
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    return v0
.end method

.method protected makeRemark(II)Lorg/htmlparser/Node;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    sub-int v0, p2, p1

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    if-le v1, v0, :cond_0

    .line 1331
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    .line 1332
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lorg/htmlparser/NodeFactory;->createRemarkNode(Lorg/htmlparser/lexer/Page;II)Lorg/htmlparser/Remark;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method protected makeString(II)Lorg/htmlparser/Node;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    sub-int v0, p2, p1

    if-eqz v0, :cond_0

    .line 848
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v0

    .line 849
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    .line 848
    invoke-interface {v0, v1, p1, p2}, Lorg/htmlparser/NodeFactory;->createStringNode(Lorg/htmlparser/lexer/Page;II)Lorg/htmlparser/Text;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method protected makeTag(IILjava/util/Vector;)Lorg/htmlparser/Node;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    sub-int v0, p2, p1

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    if-le v1, v0, :cond_0

    .line 1180
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    .line 1181
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lorg/htmlparser/NodeFactory;->createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public nextNode()Lorg/htmlparser/Node;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 317
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->nextNode(Z)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0
.end method

.method public nextNode(Z)Lorg/htmlparser/Node;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 337
    sget v0, Lorg/htmlparser/lexer/Lexer;->mDebugLineTrigger:I

    const/4 v1, -0x1

    if-eq v1, v0, :cond_0

    .line 339
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result v0

    .line 341
    sget v1, Lorg/htmlparser/lexer/Lexer;->mDebugLineTrigger:I

    if-ge v1, v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 342
    sput v0, Lorg/htmlparser/lexer/Lexer;->mDebugLineTrigger:I

    .line 344
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    .line 345
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v1

    const/16 v2, 0x3c

    const v3, 0xffff

    if-eq v1, v2, :cond_2

    if-eq v1, v3, :cond_1

    .line 399
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 400
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    goto/16 :goto_1

    :cond_1
    const/4 p1, 0x0

    goto/16 :goto_1

    .line 352
    :cond_2
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v1

    if-ne v3, v1, :cond_3

    .line 354
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object p1

    goto/16 :goto_1

    :cond_3
    const/16 v2, 0x25

    if-ne v2, v1, :cond_4

    .line 357
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1, v1}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 358
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->parseJsp(I)Lorg/htmlparser/Node;

    move-result-object p1

    goto/16 :goto_1

    :cond_4
    const/16 v4, 0x3f

    if-ne v4, v1, :cond_5

    .line 362
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1, v1}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 363
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->parsePI(I)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    :cond_5
    const/16 v4, 0x2f

    if-eq v4, v1, :cond_b

    if-eq v2, v1, :cond_b

    .line 365
    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_0

    :cond_6
    const/16 v2, 0x21

    if-ne v2, v1, :cond_a

    .line 372
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v1

    if-ne v3, v1, :cond_7

    .line 374
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    :cond_7
    const/16 v2, 0x3e

    if-ne v2, v1, :cond_8

    .line 378
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->makeRemark(II)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    .line 381
    :cond_8
    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v3, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v3}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    const/16 v2, 0x2d

    if-ne v2, v1, :cond_9

    .line 383
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->parseRemark(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    .line 386
    :cond_9
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1, v1}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 387
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->parseTag(I)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    .line 394
    :cond_a
    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 395
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    goto :goto_1

    .line 367
    :cond_b
    :goto_0
    iget-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p1, v1}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 368
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->parseTag(I)Lorg/htmlparser/Node;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public parseCDATA()Lorg/htmlparser/Node;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 428
    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->parseCDATA(Z)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0
.end method

.method public parseCDATA(Z)Lorg/htmlparser/Node;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 454
    iget-object v1, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v4, 0x27

    const/16 v5, 0x22

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x0

    :goto_1
    if-eqz v8, :cond_0

    .line 616
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v2

    .line 618
    invoke-virtual {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    .line 462
    :cond_0
    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v13, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v12, v13}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v12

    const/16 v13, 0x2f

    const v14, 0xffff

    if-eqz v9, :cond_10

    const/16 v15, 0x2d

    if-eq v9, v7, :cond_a

    if-eq v9, v3, :cond_7

    if-ne v9, v2, :cond_6

    if-ne v14, v12, :cond_1

    :goto_2
    goto :goto_3

    :cond_1
    if-ne v15, v12, :cond_24

    .line 592
    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v10, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v10

    if-ne v14, v10, :cond_2

    goto :goto_2

    :cond_2
    if-ne v15, v10, :cond_5

    .line 597
    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v10, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v10

    if-ne v14, v10, :cond_3

    goto :goto_2

    :cond_3
    const/16 v12, 0x3e

    if-ne v12, v10, :cond_4

    goto :goto_4

    .line 604
    :cond_4
    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v10, v12}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 605
    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v10, v12}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_9

    .line 609
    :cond_5
    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v10, v12}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_9

    .line 613
    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "how the fuck did we get in state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    if-ne v14, v12, :cond_8

    :goto_3
    const/4 v8, 0x1

    goto/16 :goto_9

    .line 575
    :cond_8
    invoke-static {v12}, Ljava/lang/Character;->isLetter(C)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 579
    iget-object v8, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8, v10}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 580
    iget-object v8, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8, v10}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 581
    iget-object v8, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8, v10}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_3

    :cond_9
    :goto_4
    const/4 v9, 0x0

    goto/16 :goto_9

    :cond_a
    const/16 v2, 0x21

    if-eq v12, v2, :cond_d

    if-eq v12, v13, :cond_c

    if-eq v12, v14, :cond_11

    :cond_b
    const/4 v2, 0x3

    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v2, 0x3

    const/4 v9, 0x2

    goto/16 :goto_1

    .line 550
    :cond_d
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v2

    if-ne v14, v2, :cond_e

    goto :goto_5

    :cond_e
    if-ne v15, v2, :cond_b

    .line 555
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v2

    if-ne v14, v2, :cond_f

    goto :goto_5

    :cond_f
    if-ne v15, v2, :cond_b

    const/4 v2, 0x3

    const/4 v9, 0x3

    goto/16 :goto_1

    :cond_10
    const/16 v2, 0xa

    if-eq v12, v2, :cond_23

    if-eq v12, v5, :cond_20

    if-eq v12, v4, :cond_1e

    if-eq v12, v13, :cond_16

    const/16 v2, 0x3c

    if-eq v12, v2, :cond_14

    const/16 v2, 0x5c

    if-eq v12, v2, :cond_12

    if-eq v12, v14, :cond_11

    goto/16 :goto_8

    :cond_11
    :goto_5
    const/4 v2, 0x3

    const/4 v8, 0x1

    goto/16 :goto_1

    :cond_12
    if-eqz p1, :cond_22

    if-eqz v11, :cond_22

    .line 489
    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v13, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v12, v13}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v12

    if-ne v14, v12, :cond_13

    goto :goto_5

    :cond_13
    if-eq v12, v2, :cond_22

    if-eq v12, v11, :cond_22

    .line 494
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v12}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_8

    :cond_14
    if-eqz p1, :cond_15

    if-nez v11, :cond_22

    :cond_15
    const/4 v2, 0x3

    const/4 v9, 0x1

    goto/16 :goto_1

    :cond_16
    if-eqz p1, :cond_22

    if-nez v11, :cond_22

    .line 502
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v2

    if-ne v14, v2, :cond_17

    goto :goto_5

    :cond_17
    if-ne v13, v2, :cond_18

    const/4 v2, 0x3

    const/4 v10, 0x1

    goto/16 :goto_1

    :cond_18
    const/16 v12, 0x2a

    if-ne v12, v2, :cond_1d

    .line 512
    :cond_19
    :goto_6
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v15, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v15}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v2

    if-eq v14, v2, :cond_1a

    if-ne v12, v2, :cond_19

    .line 514
    :cond_1a
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v15, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v15}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v2

    if-ne v2, v12, :cond_1b

    .line 516
    iget-object v15, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v15, v3}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    :cond_1b
    if-eq v14, v2, :cond_22

    if-ne v13, v2, :cond_1c

    goto :goto_8

    :cond_1c
    const/4 v3, 0x2

    goto :goto_6

    .line 521
    :cond_1d
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v3}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_8

    :cond_1e
    if-eqz p1, :cond_22

    if-nez v10, :cond_22

    if-nez v11, :cond_1f

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v11, 0x27

    goto/16 :goto_1

    :cond_1f
    if-ne v4, v11, :cond_22

    goto :goto_7

    :cond_20
    if-eqz p1, :cond_22

    if-nez v10, :cond_22

    if-nez v11, :cond_21

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v11, 0x22

    goto/16 :goto_1

    :cond_21
    if-ne v5, v11, :cond_22

    :goto_7
    const/4 v2, 0x3

    const/4 v3, 0x2

    goto/16 :goto_0

    :cond_22
    :goto_8
    const/4 v2, 0x3

    const/4 v3, 0x2

    goto/16 :goto_1

    :cond_23
    const/4 v2, 0x3

    :cond_24
    :goto_9
    const/4 v10, 0x0

    goto/16 :goto_1
.end method

.method protected parseJsp(I)Lorg/htmlparser/Node;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    .line 1361
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v11, 0x0

    :cond_0
    :goto_0
    if-eqz v5, :cond_3

    if-ne v3, v8, :cond_2

    if-eqz v11, :cond_1

    .line 1508
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    sub-int/2addr v3, v6

    .line 1509
    new-instance v4, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/4 v15, 0x0

    move-object v9, v4

    move v12, v3

    invoke-direct/range {v9 .. v15}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1510
    new-instance v4, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v13, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    add-int/lit8 v15, v3, 0x1

    const/16 v16, -0x1

    const/16 v17, -0x1

    const/16 v18, 0x0

    move-object v12, v4

    move v14, v3

    invoke-direct/range {v12 .. v18}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1518
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {v0, v1, v3, v2}, Lorg/htmlparser/lexer/Lexer;->makeTag(IILjava/util/Vector;)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    .line 1513
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "jsp with no code!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1516
    :cond_2
    invoke-virtual {v0, v1, v7}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    .line 1370
    :cond_3
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v10}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v9

    const/16 v10, 0x25

    if-eqz v8, :cond_16

    const/16 v12, 0x3e

    const v13, 0xffff

    if-eq v8, v7, :cond_13

    const/16 v14, 0x27

    const/16 v15, 0x22

    if-eq v8, v6, :cond_8

    if-eq v8, v4, :cond_6

    if-eq v8, v15, :cond_5

    if-ne v8, v14, :cond_4

    if-eq v9, v14, :cond_14

    if-eq v9, v13, :cond_18

    goto :goto_0

    .line 1500
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "how the fuck did we get in state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    if-eq v9, v15, :cond_14

    if-eq v9, v13, :cond_18

    goto/16 :goto_0

    :cond_6
    if-eq v9, v12, :cond_7

    if-eq v9, v13, :cond_18

    goto/16 :goto_2

    :cond_7
    const/4 v5, 0x1

    const/4 v8, 0x4

    goto/16 :goto_0

    :cond_8
    if-eq v9, v15, :cond_12

    if-eq v9, v10, :cond_11

    if-eq v9, v14, :cond_12

    const/16 v10, 0x2f

    if-eq v9, v10, :cond_9

    if-eq v9, v12, :cond_18

    if-eq v9, v13, :cond_18

    goto/16 :goto_0

    .line 1421
    :cond_9
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v12}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v9

    if-ne v9, v10, :cond_c

    .line 1426
    :cond_a
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v10}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v9

    if-ne v9, v13, :cond_b

    goto/16 :goto_3

    :cond_b
    const/16 v10, 0xa

    if-eq v9, v10, :cond_0

    const/16 v10, 0xd

    if-ne v9, v10, :cond_a

    goto/16 :goto_0

    :cond_c
    const/16 v12, 0x2a

    if-ne v9, v12, :cond_10

    .line 1443
    :cond_d
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v14, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v14}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v9

    if-eq v13, v9, :cond_e

    if-ne v12, v9, :cond_d

    .line 1445
    :cond_e
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v14, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v14}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v9

    if-ne v9, v12, :cond_f

    .line 1447
    iget-object v14, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v15, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v14, v15}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    :cond_f
    if-eq v13, v9, :cond_0

    if-ne v10, v9, :cond_d

    goto/16 :goto_0

    .line 1452
    :cond_10
    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v9, v10}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_0

    :cond_11
    const/4 v8, 0x3

    goto/16 :goto_0

    :cond_12
    move v8, v9

    goto/16 :goto_0

    :cond_13
    const/16 v10, 0x3d

    if-eq v9, v10, :cond_15

    if-eq v9, v12, :cond_17

    const/16 v10, 0x40

    if-eq v9, v10, :cond_15

    if-eq v9, v13, :cond_17

    .line 1400
    iget-object v8, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v8

    sub-int/2addr v8, v7

    .line 1401
    new-instance v15, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v10, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    add-int/lit8 v11, v1, 0x1

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/16 v16, 0x0

    move-object v9, v15

    move v12, v8

    move-object v3, v15

    move/from16 v15, v16

    invoke-direct/range {v9 .. v15}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v11, v8

    :goto_1
    const/4 v3, 0x4

    :cond_14
    :goto_2
    const/4 v8, 0x2

    goto/16 :goto_0

    .line 1395
    :cond_15
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    .line 1396
    new-instance v15, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    add-int/lit8 v10, v1, 0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object v8, v15

    move v11, v3

    invoke-direct/range {v8 .. v14}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_16
    if-eq v9, v10, :cond_19

    :cond_17
    const/4 v3, 0x4

    :cond_18
    :goto_3
    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_19
    const/4 v3, 0x4

    const/4 v8, 0x1

    goto/16 :goto_0
.end method

.method protected parsePI(I)Lorg/htmlparser/Node;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    .line 1542
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    :goto_0
    if-eqz v5, :cond_2

    if-ne v3, v7, :cond_1

    if-eqz v10, :cond_0

    .line 1633
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    sub-int/2addr v3, v4

    .line 1634
    new-instance v4, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object v8, v4

    move v11, v3

    invoke-direct/range {v8 .. v14}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1635
    new-instance v4, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v12, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    add-int/lit8 v14, v3, 0x1

    const/4 v15, -0x1

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object v11, v4

    move v13, v3

    invoke-direct/range {v11 .. v17}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1643
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {v0, v1, v3, v2}, Lorg/htmlparser/lexer/Lexer;->makeTag(IILjava/util/Vector;)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    .line 1638
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "processing instruction with no content"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1641
    :cond_1
    invoke-virtual {v0, v1, v6}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    .line 1547
    :cond_2
    iget-object v8, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8, v9}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v8

    const/16 v9, 0x3f

    if-eqz v7, :cond_a

    const/16 v11, 0x3e

    const/16 v12, 0x27

    const/16 v13, 0x22

    const v14, 0xffff

    if-eq v7, v6, :cond_7

    if-eq v7, v4, :cond_5

    if-eq v7, v13, :cond_4

    if-ne v7, v12, :cond_3

    if-eq v8, v12, :cond_d

    if-eq v8, v14, :cond_b

    goto :goto_0

    .line 1625
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "how the fuck did we get in state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    if-eq v8, v13, :cond_d

    if-eq v8, v14, :cond_b

    goto/16 :goto_0

    :cond_5
    if-eq v8, v11, :cond_6

    if-eq v8, v14, :cond_b

    goto :goto_1

    :cond_6
    const/4 v5, 0x1

    const/4 v7, 0x3

    goto/16 :goto_0

    :cond_7
    if-eq v8, v13, :cond_9

    if-eq v8, v12, :cond_9

    if-eq v8, v14, :cond_b

    if-eq v8, v11, :cond_b

    if-eq v8, v9, :cond_8

    goto/16 :goto_0

    :cond_8
    const/4 v7, 0x2

    goto/16 :goto_0

    :cond_9
    move v7, v8

    goto/16 :goto_0

    :cond_a
    if-eq v8, v9, :cond_c

    :cond_b
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1554
    :cond_c
    iget-object v7, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v7}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v7

    .line 1555
    new-instance v15, Lorg/htmlparser/lexer/PageAttribute;

    iget-object v9, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    add-int/lit8 v10, v1, 0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object v8, v15

    move v11, v7

    invoke-direct/range {v8 .. v14}, Lorg/htmlparser/lexer/PageAttribute;-><init>(Lorg/htmlparser/lexer/Page;IIIIC)V

    invoke-virtual {v2, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v10, v7

    :cond_d
    :goto_1
    const/4 v7, 0x1

    goto/16 :goto_0
.end method

.method protected parseRemark(IZ)Lorg/htmlparser/Node;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x4

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 1309
    iget-object p2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->makeRemark(II)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    .line 1243
    :cond_1
    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v7, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v6, v7}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v6

    const v7, 0xffff

    if-ne v7, v6, :cond_2

    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/16 v8, 0x3e

    const/16 v9, 0x2d

    if-eqz v5, :cond_d

    if-eq v5, v4, :cond_9

    if-eq v5, v3, :cond_7

    if-eq v5, v1, :cond_5

    if-ne v5, v0, :cond_4

    if-ne v8, v6, :cond_3

    goto :goto_1

    .line 1290
    :cond_3
    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1295
    sget-boolean v7, Lorg/htmlparser/lexer/Lexer;->STRICT_REMARKS:Z

    if-nez v7, :cond_6

    if-eq v9, v6, :cond_0

    const/16 v7, 0x21

    if-eq v7, v6, :cond_0

    goto :goto_2

    .line 1305
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "how the fuck did we get in state "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    if-ne v9, v6, :cond_6

    const/4 v5, 0x4

    goto :goto_0

    :cond_6
    :goto_2
    const/4 v5, 0x2

    goto :goto_0

    :cond_7
    if-ne v9, v6, :cond_8

    const/4 v5, 0x3

    goto :goto_0

    :cond_8
    if-ne v7, v6, :cond_0

    .line 1279
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    :cond_9
    if-ne v9, v6, :cond_c

    .line 1261
    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v9, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v6, v9}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v6

    if-ne v7, v6, :cond_a

    goto :goto_1

    :cond_a
    if-ne v8, v6, :cond_b

    goto :goto_1

    .line 1268
    :cond_b
    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v5, v6}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_2

    .line 1273
    :cond_c
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    :cond_d
    if-ne v8, v6, :cond_e

    const/4 v2, 0x1

    :cond_e
    if-ne v9, v6, :cond_f

    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1255
    :cond_f
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->parseString(IZ)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1
.end method

.method protected parseString(IZ)Lorg/htmlparser/Node;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 827
    iget-object p2, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/lexer/Lexer;->makeString(II)Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1

    .line 737
    :cond_1
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    const v5, 0xffff

    if-ne v5, v4, :cond_2

    :goto_2
    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/16 v6, 0x1b

    if-ne v6, v4, :cond_7

    .line 742
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v6}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-ne v5, v4, :cond_3

    goto :goto_2

    :cond_3
    const/16 v6, 0x24

    if-ne v6, v4, :cond_6

    .line 747
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v6}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-ne v5, v4, :cond_4

    goto :goto_2

    :cond_4
    const/16 v5, 0x42

    if-ne v5, v4, :cond_5

    .line 751
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0, v4}, Lorg/htmlparser/lexer/Lexer;->scanJIS(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_1

    .line 754
    :cond_5
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 755
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_1

    .line 759
    :cond_6
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_1

    :cond_7
    if-eqz p2, :cond_9

    if-nez v3, :cond_9

    const/16 v6, 0x27

    if-eq v6, v4, :cond_8

    const/16 v6, 0x22

    if-ne v6, v4, :cond_9

    :cond_8
    move v3, v4

    goto :goto_1

    :cond_9
    if-eqz p2, :cond_a

    if-eqz v3, :cond_a

    const/16 v6, 0x5c

    if-ne v6, v4, :cond_a

    .line 767
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v7, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v7}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-eq v5, v4, :cond_0

    if-eq v6, v4, :cond_0

    if-eq v4, v3, :cond_0

    .line 772
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_1

    :cond_a
    if-eqz p2, :cond_b

    if-ne v4, v3, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v6, 0x2f

    if-eqz p2, :cond_13

    if-nez v3, :cond_13

    if-ne v4, v6, :cond_13

    .line 781
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v7, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v7}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-ne v5, v4, :cond_c

    goto/16 :goto_2

    :cond_c
    if-ne v6, v4, :cond_e

    .line 787
    :cond_d
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v6, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v6}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-eq v5, v4, :cond_0

    const/16 v6, 0xa

    if-ne v6, v4, :cond_d

    goto/16 :goto_1

    :cond_e
    const/16 v7, 0x2a

    if-ne v7, v4, :cond_12

    .line 795
    :cond_f
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v8, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v8}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-eq v5, v4, :cond_10

    if-ne v7, v4, :cond_f

    .line 797
    :cond_10
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v8, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v8}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-ne v4, v7, :cond_11

    .line 799
    iget-object v8, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v9, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v8, v9}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    :cond_11
    if-eq v5, v4, :cond_0

    if-ne v6, v4, :cond_f

    goto/16 :goto_1

    .line 804
    :cond_12
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_1

    :cond_13
    if-nez v3, :cond_0

    const/16 v7, 0x3c

    if-ne v7, v4, :cond_0

    .line 808
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v7, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v7}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v4

    if-ne v5, v4, :cond_14

    goto/16 :goto_2

    :cond_14
    if-eq v6, v4, :cond_16

    .line 812
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-nez v5, :cond_16

    const/16 v5, 0x21

    if-eq v5, v4, :cond_16

    const/16 v5, 0x25

    if-eq v5, v4, :cond_16

    const/16 v5, 0x3f

    if-ne v5, v4, :cond_15

    goto :goto_3

    .line 822
    :cond_15
    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v4, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_1

    .line 816
    :cond_16
    :goto_3
    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v4}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 817
    iget-object v2, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v4, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2, v4}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto/16 :goto_2
.end method

.method protected parseTag(I)Lorg/htmlparser/Node;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 1002
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/16 v2, 0x8

    new-array v2, v2, [I

    .line 1005
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    const/4 v4, 0x0

    aput v3, v2, v4

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x6

    const/4 v9, 0x1

    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x0

    :cond_0
    :goto_1
    if-eqz v10, :cond_1

    .line 1157
    iget-object v2, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v2}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v2

    move/from16 v12, p1

    invoke-virtual {v0, v12, v2, v1}, Lorg/htmlparser/lexer/Lexer;->makeTag(IILjava/util/Vector;)Lorg/htmlparser/Node;

    move-result-object v1

    return-object v1

    :cond_1
    move/from16 v12, p1

    add-int/lit8 v13, v11, 0x1

    .line 1008
    iget-object v14, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v14}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v14

    aput v14, v2, v13

    .line 1009
    iget-object v14, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v15, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v14, v15}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v14

    const/16 v15, 0x3d

    const/16 v3, 0x22

    const v5, 0xffff

    packed-switch v11, :pswitch_data_0

    .line 1153
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "how the fuck did we get in state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    if-ne v5, v14, :cond_2

    .line 1124
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->standalone(Ljava/util/Vector;[I)V

    .line 1125
    aget v3, v2, v8

    aput v3, v2, v4

    .line 1126
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_0

    .line 1129
    :cond_2
    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_6

    if-ne v15, v14, :cond_3

    .line 1135
    aget v3, v2, v8

    aput v3, v2, v6

    const/4 v3, 0x7

    .line 1136
    aget v3, v2, v3

    aput v3, v2, v7

    :goto_2
    const/4 v11, 0x2

    goto :goto_1

    .line 1146
    :cond_3
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->standalone(Ljava/util/Vector;[I)V

    .line 1147
    aget v3, v2, v8

    aput v3, v2, v4

    .line 1148
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    goto :goto_0

    :pswitch_1
    if-ne v5, v14, :cond_4

    .line 1106
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->double_quote(Ljava/util/Vector;[I)V

    goto/16 :goto_5

    :cond_4
    if-ne v3, v14, :cond_6

    .line 1111
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->double_quote(Ljava/util/Vector;[I)V

    .line 1112
    aget v3, v2, v8

    add-int/2addr v3, v9

    aput v3, v2, v4

    goto/16 :goto_0

    :pswitch_2
    if-ne v5, v14, :cond_5

    .line 1093
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->single_quote(Ljava/util/Vector;[I)V

    goto :goto_5

    :cond_5
    const/16 v3, 0x27

    if-ne v3, v14, :cond_6

    .line 1098
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->single_quote(Ljava/util/Vector;[I)V

    const/4 v3, 0x5

    .line 1099
    aget v5, v2, v3

    add-int/2addr v5, v9

    aput v5, v2, v4

    goto/16 :goto_0

    :cond_6
    const/16 v16, 0x4

    const/16 v17, 0x5

    goto/16 :goto_1

    :pswitch_3
    if-eq v5, v14, :cond_8

    const/16 v3, 0x3e

    if-ne v3, v14, :cond_7

    goto :goto_3

    .line 1083
    :cond_7
    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1085
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->naked(Ljava/util/Vector;[I)V

    const/4 v3, 0x4

    .line 1086
    aget v5, v2, v3

    aput v5, v2, v4

    goto/16 :goto_0

    .line 1080
    :cond_8
    :goto_3
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->naked(Ljava/util/Vector;[I)V

    goto :goto_5

    :pswitch_4
    if-eq v5, v14, :cond_c

    const/16 v5, 0x3e

    if-ne v5, v14, :cond_9

    goto :goto_4

    :cond_9
    const/16 v5, 0x27

    if-ne v5, v14, :cond_a

    .line 1061
    aget v3, v2, v7

    const/16 v16, 0x4

    aput v3, v2, v16

    const/4 v11, 0x4

    goto/16 :goto_1

    :cond_a
    const/16 v16, 0x4

    if-ne v3, v14, :cond_b

    .line 1066
    aget v3, v2, v7

    const/16 v17, 0x5

    aput v3, v2, v17

    const/4 v11, 0x5

    goto/16 :goto_1

    :cond_b
    const/16 v17, 0x5

    .line 1068
    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v11, 0x3

    goto/16 :goto_1

    :cond_c
    :goto_4
    const/16 v16, 0x4

    const/16 v17, 0x5

    .line 1055
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->empty(Ljava/util/Vector;[I)V

    :goto_5
    const/4 v10, 0x1

    goto/16 :goto_1

    :pswitch_5
    const/16 v16, 0x4

    const/16 v17, 0x5

    if-eq v5, v14, :cond_f

    const/16 v3, 0x3e

    if-eq v3, v14, :cond_f

    const/16 v3, 0x3c

    if-ne v3, v14, :cond_d

    goto :goto_6

    .line 1042
    :cond_d
    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1046
    aget v3, v2, v6

    aput v3, v2, v8

    const/4 v11, 0x6

    goto/16 :goto_1

    :cond_e
    if-ne v15, v14, :cond_0

    goto/16 :goto_2

    :cond_f
    const/16 v3, 0x3c

    :goto_6
    if-ne v3, v14, :cond_10

    .line 1036
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 1037
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    aput v3, v2, v13

    .line 1039
    :cond_10
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->standalone(Ljava/util/Vector;[I)V

    goto :goto_5

    :pswitch_6
    const/16 v16, 0x4

    const/16 v17, 0x5

    if-eq v5, v14, :cond_12

    const/16 v3, 0x3e

    if-eq v3, v14, :cond_12

    const/16 v3, 0x3c

    if-ne v3, v14, :cond_11

    goto :goto_7

    .line 1024
    :cond_11
    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1026
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->whitespace(Ljava/util/Vector;[I)V

    const/4 v11, 0x1

    goto/16 :goto_1

    :cond_12
    const/16 v3, 0x3c

    :goto_7
    if-ne v3, v14, :cond_13

    .line 1018
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    iget-object v5, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3, v5}, Lorg/htmlparser/lexer/Page;->ungetCharacter(Lorg/htmlparser/lexer/Cursor;)V

    .line 1019
    iget-object v3, v0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v3}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v3

    aput v3, v2, v13

    .line 1021
    :cond_13
    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Lexer;->whitespace(Ljava/util/Vector;[I)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public reset()V
    .locals 3

    .line 302
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Page;->reset()V

    .line 303
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/lexer/Lexer;->setCursor(Lorg/htmlparser/lexer/Cursor;)V

    return-void
.end method

.method protected scanJIS(Lorg/htmlparser/lexer/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_0
    const/4 v4, 0x0

    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    return-void

    .line 687
    :cond_2
    iget-object v5, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {v5, p1}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v5

    const v6, 0xffff

    if-ne v6, v5, :cond_3

    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_6

    if-eq v4, v1, :cond_5

    if-ne v4, v0, :cond_4

    const/16 v6, 0x4a

    if-ne v6, v5, :cond_0

    goto :goto_1

    .line 710
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "state "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    const/16 v4, 0x28

    if-ne v4, v5, :cond_0

    const/4 v4, 0x2

    goto :goto_0

    :cond_6
    const/16 v6, 0x1b

    if-ne v6, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public setCursor(Lorg/htmlparser/lexer/Cursor;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 231
    iput-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mCursor:Lorg/htmlparser/lexer/Cursor;

    return-void

    .line 229
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "cursor cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNodeFactory(Lorg/htmlparser/NodeFactory;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 251
    iput-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mFactory:Lorg/htmlparser/NodeFactory;

    return-void

    .line 250
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "node factory cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPage(Lorg/htmlparser/lexer/Page;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 210
    iput-object p1, p0, Lorg/htmlparser/lexer/Lexer;->mPage:Lorg/htmlparser/lexer/Page;

    return-void

    .line 208
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "page cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPosition(I)V
    .locals 1

    .line 270
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/Cursor;->setPosition(I)V

    return-void
.end method
