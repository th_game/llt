.class public Lorg/htmlparser/lexer/Cursor;
.super Ljava/lang/Object;
.source "Cursor.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/htmlparser/util/sort/Ordered;
.implements Ljava/lang/Cloneable;


# instance fields
.field protected mPage:Lorg/htmlparser/lexer/Page;

.field protected mPosition:I


# direct methods
.method public constructor <init>(Lorg/htmlparser/lexer/Page;I)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/htmlparser/lexer/Cursor;->mPage:Lorg/htmlparser/lexer/Page;

    .line 59
    iput p2, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    return-void
.end method


# virtual methods
.method public advance()V
    .locals 1

    .line 94
    iget v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    return-void
.end method

.method public compare(Ljava/lang/Object;)I
    .locals 1

    .line 164
    check-cast p1, Lorg/htmlparser/lexer/Cursor;

    .line 165
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result p1

    sub-int/2addr v0, p1

    return v0
.end method

.method public dup()Lorg/htmlparser/lexer/Cursor;
    .locals 3

    .line 116
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/lexer/Cursor;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 120
    :catch_0
    new-instance v0, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Cursor;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-virtual {p0}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    return-object v0
.end method

.method public getPage()Lorg/htmlparser/lexer/Page;
    .locals 1

    .line 68
    iget-object v0, p0, Lorg/htmlparser/lexer/Cursor;->mPage:Lorg/htmlparser/lexer/Page;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .line 77
    iget v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    return v0
.end method

.method public retreat()V
    .locals 1

    .line 102
    iget v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    .line 103
    iget v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 104
    iput v0, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    :cond_0
    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .line 86
    iput p1, p0, Lorg/htmlparser/lexer/Cursor;->mPosition:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 133
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 134
    invoke-virtual {p0}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "["

    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    iget-object v1, p0, Lorg/htmlparser/lexer/Cursor;->mPage:Lorg/htmlparser/lexer/Page;

    const-string v2, "?"

    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {v1, p0}, Lorg/htmlparser/lexer/Page;->row(Lorg/htmlparser/lexer/Cursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 139
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    const-string v1, ","

    .line 140
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    iget-object v1, p0, Lorg/htmlparser/lexer/Cursor;->mPage:Lorg/htmlparser/lexer/Page;

    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {v1, p0}, Lorg/htmlparser/lexer/Page;->column(Lorg/htmlparser/lexer/Cursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 144
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    const-string v1, "]"

    .line 145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
