.class Lorg/htmlparser/util/CharacterReferenceEx;
.super Lorg/htmlparser/util/CharacterReference;
.source "Translate.java"


# instance fields
.field protected mEnd:I

.field protected mStart:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, v0, v1}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;)I
    .locals 5

    .line 108
    check-cast p1, Lorg/htmlparser/util/CharacterReference;

    .line 109
    invoke-virtual {p1}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 111
    iget v1, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mStart:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    iget v4, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mEnd:I

    if-lt v1, v4, :cond_0

    goto :goto_1

    :cond_0
    if-lt v2, v0, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    .line 118
    :cond_1
    iget-object v3, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mKernel:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    sub-int/2addr v3, v4

    if-eqz v3, :cond_2

    :goto_1
    return v3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getKernel()Ljava/lang/String;
    .locals 3

    .line 89
    iget-object v0, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mKernel:Ljava/lang/String;

    iget v1, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mStart:I

    iget v2, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mEnd:I

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setEnd(I)V
    .locals 0

    .line 80
    iput p1, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mEnd:I

    return-void
.end method

.method public setStart(I)V
    .locals 0

    .line 71
    iput p1, p0, Lorg/htmlparser/util/CharacterReferenceEx;->mStart:I

    return-void
.end method
