.class public Lorg/htmlparser/util/CharacterReference;
.super Ljava/lang/Object;
.source "CharacterReference.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/htmlparser/util/sort/Ordered;


# instance fields
.field protected mCharacter:I

.field protected mKernel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lorg/htmlparser/util/CharacterReference;->mKernel:Ljava/lang/String;

    .line 65
    iput p2, p0, Lorg/htmlparser/util/CharacterReference;->mCharacter:I

    .line 66
    iget-object p1, p0, Lorg/htmlparser/util/CharacterReference;->mKernel:Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, ""

    .line 67
    iput-object p1, p0, Lorg/htmlparser/util/CharacterReference;->mKernel:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;)I
    .locals 1

    .line 142
    check-cast p1, Lorg/htmlparser/util/CharacterReference;

    .line 144
    invoke-virtual {p0}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getCharacter()I
    .locals 1

    .line 95
    iget v0, p0, Lorg/htmlparser/util/CharacterReference;->mCharacter:I

    return v0
.end method

.method public getKernel()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lorg/htmlparser/util/CharacterReference;->mKernel:Ljava/lang/String;

    return-object v0
.end method

.method setCharacter(I)V
    .locals 0

    .line 105
    iput p1, p0, Lorg/htmlparser/util/CharacterReference;->mCharacter:I

    return-void
.end method

.method setKernel(Ljava/lang/String;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lorg/htmlparser/util/CharacterReference;->mKernel:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 118
    invoke-virtual {p0}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\u"

    .line 119
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    const/4 v3, 0x4

    if-lt v2, v3, :cond_0

    .line 122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "["

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    invoke-virtual {p0}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "]"

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v3, "0"

    .line 121
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
