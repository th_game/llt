.class public Lorg/htmlparser/util/Translate;
.super Ljava/lang/Object;
.source "Translate.java"


# static fields
.field protected static final BREAKPOINT:I = 0x100

.field public static DECODE_LINE_BY_LINE:Z = false

.field public static ENCODE_HEXADECIMAL:Z = false

.field protected static final mCharacterList:[Lorg/htmlparser/util/CharacterReference;

.field protected static final mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xfc

    new-array v1, v0, [Lorg/htmlparser/util/CharacterReference;

    .line 169
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v3, 0xa0

    const-string v4, "nbsp"

    invoke-direct {v2, v4, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v4, 0x0

    aput-object v2, v1, v4

    .line 170
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v5, 0xa1

    const-string v6, "iexcl"

    invoke-direct {v2, v6, v5}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v6, 0x1

    aput-object v2, v1, v6

    .line 171
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v6, 0xa2

    const-string v7, "cent"

    invoke-direct {v2, v7, v6}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v7, 0x2

    aput-object v2, v1, v7

    .line 172
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v7, 0xa3

    const-string v8, "pound"

    invoke-direct {v2, v8, v7}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v8, 0x3

    aput-object v2, v1, v8

    .line 173
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v8, 0xa4

    const-string v9, "curren"

    invoke-direct {v2, v9, v8}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v9, 0x4

    aput-object v2, v1, v9

    .line 174
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v9, 0xa5

    const-string v10, "yen"

    invoke-direct {v2, v10, v9}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v10, 0x5

    aput-object v2, v1, v10

    .line 175
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v10, 0xa6

    const-string v11, "brvbar"

    invoke-direct {v2, v11, v10}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v11, 0x6

    aput-object v2, v1, v11

    .line 176
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v11, 0xa7

    const-string v12, "sect"

    invoke-direct {v2, v12, v11}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/4 v12, 0x7

    aput-object v2, v1, v12

    .line 177
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v12, 0xa8

    const-string v13, "uml"

    invoke-direct {v2, v13, v12}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v13, 0x8

    aput-object v2, v1, v13

    .line 178
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v13, 0xa9

    const-string v14, "copy"

    invoke-direct {v2, v14, v13}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v14, 0x9

    aput-object v2, v1, v14

    .line 179
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const/16 v14, 0xaa

    const-string v15, "ordf"

    invoke-direct {v2, v15, v14}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v15, 0xa

    aput-object v2, v1, v15

    .line 180
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v15, "laquo"

    const/16 v4, 0xab

    invoke-direct {v2, v15, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0xb

    aput-object v2, v1, v4

    .line 181
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "not"

    const/16 v15, 0xac

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0xc

    aput-object v2, v1, v4

    .line 182
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "shy"

    const/16 v15, 0xad

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0xd

    aput-object v2, v1, v4

    .line 183
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "reg"

    const/16 v15, 0xae

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0xe

    aput-object v2, v1, v4

    .line 184
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "macr"

    const/16 v15, 0xaf

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0xf

    aput-object v2, v1, v4

    .line 185
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "deg"

    const/16 v15, 0xb0

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x10

    aput-object v2, v1, v4

    .line 186
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "plusmn"

    const/16 v15, 0xb1

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x11

    aput-object v2, v1, v4

    .line 187
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "sup2"

    const/16 v15, 0xb2

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x12

    aput-object v2, v1, v4

    .line 188
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "sup3"

    const/16 v15, 0xb3

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x13

    aput-object v2, v1, v4

    .line 189
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "acute"

    const/16 v15, 0xb4

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x14

    aput-object v2, v1, v4

    .line 190
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "micro"

    const/16 v15, 0xb5

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x15

    aput-object v2, v1, v4

    .line 191
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "para"

    const/16 v15, 0xb6

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x16

    aput-object v2, v1, v4

    .line 192
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "middot"

    const/16 v15, 0xb7

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x17

    aput-object v2, v1, v4

    .line 193
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "cedil"

    const/16 v15, 0xb8

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x18

    aput-object v2, v1, v4

    .line 194
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "sup1"

    const/16 v15, 0xb9

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x19

    aput-object v2, v1, v4

    .line 195
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ordm"

    const/16 v15, 0xba

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1a

    aput-object v2, v1, v4

    .line 196
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "raquo"

    const/16 v15, 0xbb

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1b

    aput-object v2, v1, v4

    .line 197
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "frac14"

    const/16 v15, 0xbc

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1c

    aput-object v2, v1, v4

    .line 198
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "frac12"

    const/16 v15, 0xbd

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1d

    aput-object v2, v1, v4

    .line 199
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "frac34"

    const/16 v15, 0xbe

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1e

    aput-object v2, v1, v4

    .line 200
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "iquest"

    const/16 v15, 0xbf

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x1f

    aput-object v2, v1, v4

    .line 201
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Agrave"

    const/16 v15, 0xc0

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x20

    aput-object v2, v1, v4

    .line 202
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Aacute"

    const/16 v15, 0xc1

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x21

    aput-object v2, v1, v4

    .line 203
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Acirc"

    const/16 v15, 0xc2

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x22

    aput-object v2, v1, v4

    .line 204
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Atilde"

    const/16 v15, 0xc3

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x23

    aput-object v2, v1, v4

    .line 205
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Auml"

    const/16 v15, 0xc4

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x24

    aput-object v2, v1, v4

    .line 206
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Aring"

    const/16 v15, 0xc5

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x25

    aput-object v2, v1, v4

    .line 207
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "AElig"

    const/16 v15, 0xc6

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x26

    aput-object v2, v1, v4

    .line 208
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ccedil"

    const/16 v15, 0xc7

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x27

    aput-object v2, v1, v4

    .line 209
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Egrave"

    const/16 v15, 0xc8

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x28

    aput-object v2, v1, v4

    .line 210
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Eacute"

    const/16 v15, 0xc9

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x29

    aput-object v2, v1, v4

    .line 211
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ecirc"

    const/16 v15, 0xca

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2a

    aput-object v2, v1, v4

    .line 212
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Euml"

    const/16 v15, 0xcb

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2b

    aput-object v2, v1, v4

    .line 213
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Igrave"

    const/16 v15, 0xcc

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2c

    aput-object v2, v1, v4

    .line 214
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Iacute"

    const/16 v15, 0xcd

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2d

    aput-object v2, v1, v4

    .line 215
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Icirc"

    const/16 v15, 0xce

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2e

    aput-object v2, v1, v4

    .line 216
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Iuml"

    const/16 v15, 0xcf

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x2f

    aput-object v2, v1, v4

    .line 217
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ETH"

    const/16 v15, 0xd0

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x30

    aput-object v2, v1, v4

    .line 218
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ntilde"

    const/16 v15, 0xd1

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x31

    aput-object v2, v1, v4

    .line 219
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ograve"

    const/16 v15, 0xd2

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x32

    aput-object v2, v1, v4

    .line 220
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Oacute"

    const/16 v15, 0xd3

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x33

    aput-object v2, v1, v4

    .line 221
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ocirc"

    const/16 v15, 0xd4

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x34

    aput-object v2, v1, v4

    .line 222
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Otilde"

    const/16 v15, 0xd5

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x35

    aput-object v2, v1, v4

    .line 223
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ouml"

    const/16 v15, 0xd6

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x36

    aput-object v2, v1, v4

    .line 224
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "times"

    const/16 v15, 0xd7

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x37

    aput-object v2, v1, v4

    .line 225
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Oslash"

    const/16 v15, 0xd8

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x38

    aput-object v2, v1, v4

    .line 226
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ugrave"

    const/16 v15, 0xd9

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x39

    aput-object v2, v1, v4

    .line 227
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Uacute"

    const/16 v15, 0xda

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3a

    aput-object v2, v1, v4

    .line 228
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Ucirc"

    const/16 v15, 0xdb

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3b

    aput-object v2, v1, v4

    .line 229
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Uuml"

    const/16 v15, 0xdc

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3c

    aput-object v2, v1, v4

    .line 230
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "Yacute"

    const/16 v15, 0xdd

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3d

    aput-object v2, v1, v4

    .line 231
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "THORN"

    const/16 v15, 0xde

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3e

    aput-object v2, v1, v4

    .line 232
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "szlig"

    const/16 v15, 0xdf

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x3f

    aput-object v2, v1, v4

    .line 233
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "agrave"

    const/16 v15, 0xe0

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x40

    aput-object v2, v1, v4

    .line 234
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "aacute"

    const/16 v15, 0xe1

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x41

    aput-object v2, v1, v4

    .line 235
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "acirc"

    const/16 v15, 0xe2

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x42

    aput-object v2, v1, v4

    .line 236
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "atilde"

    const/16 v15, 0xe3

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x43

    aput-object v2, v1, v4

    .line 237
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "auml"

    const/16 v15, 0xe4

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x44

    aput-object v2, v1, v4

    .line 238
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "aring"

    const/16 v15, 0xe5

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x45

    aput-object v2, v1, v4

    .line 239
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "aelig"

    const/16 v15, 0xe6

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x46

    aput-object v2, v1, v4

    .line 240
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ccedil"

    const/16 v15, 0xe7

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x47

    aput-object v2, v1, v4

    .line 241
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "egrave"

    const/16 v15, 0xe8

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x48

    aput-object v2, v1, v4

    .line 242
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "eacute"

    const/16 v15, 0xe9

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x49

    aput-object v2, v1, v4

    .line 243
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ecirc"

    const/16 v15, 0xea

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4a

    aput-object v2, v1, v4

    .line 244
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "euml"

    const/16 v15, 0xeb

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4b

    aput-object v2, v1, v4

    .line 245
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "igrave"

    const/16 v15, 0xec

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4c

    aput-object v2, v1, v4

    .line 246
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "iacute"

    const/16 v15, 0xed

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4d

    aput-object v2, v1, v4

    .line 247
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "icirc"

    const/16 v15, 0xee

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4e

    aput-object v2, v1, v4

    .line 248
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "iuml"

    const/16 v15, 0xef

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x4f

    aput-object v2, v1, v4

    .line 249
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "eth"

    const/16 v15, 0xf0

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x50

    aput-object v2, v1, v4

    .line 250
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ntilde"

    const/16 v15, 0xf1

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x51

    aput-object v2, v1, v4

    .line 251
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ograve"

    const/16 v15, 0xf2

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x52

    aput-object v2, v1, v4

    .line 252
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "oacute"

    const/16 v15, 0xf3

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x53

    aput-object v2, v1, v4

    .line 253
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ocirc"

    const/16 v15, 0xf4

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x54

    aput-object v2, v1, v4

    .line 254
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "otilde"

    const/16 v15, 0xf5

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x55

    aput-object v2, v1, v4

    .line 255
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ouml"

    const/16 v15, 0xf6

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x56

    aput-object v2, v1, v4

    .line 256
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "divide"

    const/16 v15, 0xf7

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x57

    aput-object v2, v1, v4

    .line 257
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "oslash"

    const/16 v15, 0xf8

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x58

    aput-object v2, v1, v4

    .line 258
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ugrave"

    const/16 v15, 0xf9

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x59

    aput-object v2, v1, v4

    .line 259
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "uacute"

    const/16 v15, 0xfa

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x5a

    aput-object v2, v1, v4

    .line 260
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "ucirc"

    const/16 v15, 0xfb

    invoke-direct {v2, v4, v15}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v4, 0x5b

    aput-object v2, v1, v4

    .line 261
    new-instance v2, Lorg/htmlparser/util/CharacterReference;

    const-string v4, "uuml"

    invoke-direct {v2, v4, v0}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v0, 0x5c

    aput-object v2, v1, v0

    .line 262
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "yacute"

    const/16 v4, 0xfd

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x5d

    aput-object v0, v1, v2

    .line 263
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "thorn"

    const/16 v4, 0xfe

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x5e

    aput-object v0, v1, v2

    .line 264
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "yuml"

    const/16 v4, 0xff

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x5f

    aput-object v0, v1, v2

    .line 281
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "fnof"

    const/16 v4, 0x192

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x60

    aput-object v0, v1, v2

    .line 283
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Alpha"

    const/16 v4, 0x391

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x61

    aput-object v0, v1, v2

    .line 284
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Beta"

    const/16 v4, 0x392

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x62

    aput-object v0, v1, v2

    .line 285
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Gamma"

    const/16 v4, 0x393

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x63

    aput-object v0, v1, v2

    .line 286
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Delta"

    const/16 v4, 0x394

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x64

    aput-object v0, v1, v2

    .line 287
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Epsilon"

    const/16 v4, 0x395

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x65

    aput-object v0, v1, v2

    .line 288
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Zeta"

    const/16 v4, 0x396

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x66

    aput-object v0, v1, v2

    .line 289
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Eta"

    const/16 v4, 0x397

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x67

    aput-object v0, v1, v2

    .line 290
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Theta"

    const/16 v4, 0x398

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x68

    aput-object v0, v1, v2

    .line 291
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Iota"

    const/16 v4, 0x399

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x69

    aput-object v0, v1, v2

    .line 292
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Kappa"

    const/16 v4, 0x39a

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6a

    aput-object v0, v1, v2

    .line 293
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Lambda"

    const/16 v4, 0x39b

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6b

    aput-object v0, v1, v2

    .line 294
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Mu"

    const/16 v4, 0x39c

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6c

    aput-object v0, v1, v2

    .line 295
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Nu"

    const/16 v4, 0x39d

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6d

    aput-object v0, v1, v2

    .line 296
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Xi"

    const/16 v4, 0x39e

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6e

    aput-object v0, v1, v2

    .line 297
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Omicron"

    const/16 v4, 0x39f

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x6f

    aput-object v0, v1, v2

    .line 298
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Pi"

    const/16 v4, 0x3a0

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x70

    aput-object v0, v1, v2

    .line 299
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Rho"

    const/16 v4, 0x3a1

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x71

    aput-object v0, v1, v2

    .line 301
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Sigma"

    const/16 v4, 0x3a3

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x72

    aput-object v0, v1, v2

    .line 302
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Tau"

    const/16 v4, 0x3a4

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x73

    aput-object v0, v1, v2

    .line 303
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Upsilon"

    const/16 v4, 0x3a5

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x74

    aput-object v0, v1, v2

    .line 304
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Phi"

    const/16 v4, 0x3a6

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x75

    aput-object v0, v1, v2

    .line 305
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Chi"

    const/16 v4, 0x3a7

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x76

    aput-object v0, v1, v2

    .line 306
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Psi"

    const/16 v4, 0x3a8

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x77

    aput-object v0, v1, v2

    .line 307
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Omega"

    const/16 v4, 0x3a9

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x78

    aput-object v0, v1, v2

    .line 308
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "alpha"

    const/16 v4, 0x3b1

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x79

    aput-object v0, v1, v2

    .line 309
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "beta"

    const/16 v4, 0x3b2

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7a

    aput-object v0, v1, v2

    .line 310
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "gamma"

    const/16 v4, 0x3b3

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7b

    aput-object v0, v1, v2

    .line 311
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "delta"

    const/16 v4, 0x3b4

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7c

    aput-object v0, v1, v2

    .line 312
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "epsilon"

    const/16 v4, 0x3b5

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7d

    aput-object v0, v1, v2

    .line 313
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "zeta"

    const/16 v4, 0x3b6

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7e

    aput-object v0, v1, v2

    .line 314
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "eta"

    const/16 v4, 0x3b7

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7f

    aput-object v0, v1, v2

    .line 315
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "theta"

    const/16 v4, 0x3b8

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x80

    aput-object v0, v1, v2

    .line 316
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "iota"

    const/16 v4, 0x3b9

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x81

    aput-object v0, v1, v2

    .line 317
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "kappa"

    const/16 v4, 0x3ba

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x82

    aput-object v0, v1, v2

    .line 318
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lambda"

    const/16 v4, 0x3bb

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x83

    aput-object v0, v1, v2

    .line 319
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "mu"

    const/16 v4, 0x3bc

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x84

    aput-object v0, v1, v2

    .line 320
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "nu"

    const/16 v4, 0x3bd

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x85

    aput-object v0, v1, v2

    .line 321
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "xi"

    const/16 v4, 0x3be

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x86

    aput-object v0, v1, v2

    .line 322
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "omicron"

    const/16 v4, 0x3bf

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x87

    aput-object v0, v1, v2

    .line 323
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "pi"

    const/16 v4, 0x3c0

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x88

    aput-object v0, v1, v2

    .line 324
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rho"

    const/16 v4, 0x3c1

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x89

    aput-object v0, v1, v2

    .line 325
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sigmaf"

    const/16 v4, 0x3c2

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8a

    aput-object v0, v1, v2

    .line 326
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sigma"

    const/16 v4, 0x3c3

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8b

    aput-object v0, v1, v2

    .line 327
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "tau"

    const/16 v4, 0x3c4

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8c

    aput-object v0, v1, v2

    .line 328
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "upsilon"

    const/16 v4, 0x3c5

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8d

    aput-object v0, v1, v2

    .line 329
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "phi"

    const/16 v4, 0x3c6

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8e

    aput-object v0, v1, v2

    .line 330
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "chi"

    const/16 v4, 0x3c7

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x8f

    aput-object v0, v1, v2

    .line 331
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "psi"

    const/16 v4, 0x3c8

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x90

    aput-object v0, v1, v2

    .line 332
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "omega"

    const/16 v4, 0x3c9

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x91

    aput-object v0, v1, v2

    .line 333
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "thetasym"

    const/16 v4, 0x3d1

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x92

    aput-object v0, v1, v2

    .line 334
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "upsih"

    const/16 v4, 0x3d2

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x93

    aput-object v0, v1, v2

    .line 335
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "piv"

    const/16 v4, 0x3d6

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x94

    aput-object v0, v1, v2

    .line 337
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "bull"

    const/16 v4, 0x2022

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x95

    aput-object v0, v1, v2

    .line 339
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "hellip"

    const/16 v4, 0x2026

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x96

    aput-object v0, v1, v2

    .line 340
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "prime"

    const/16 v4, 0x2032

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x97

    aput-object v0, v1, v2

    .line 341
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Prime"

    const/16 v4, 0x2033

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x98

    aput-object v0, v1, v2

    .line 342
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "oline"

    const/16 v4, 0x203e

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x99

    aput-object v0, v1, v2

    .line 343
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "frasl"

    const/16 v4, 0x2044

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9a

    aput-object v0, v1, v2

    .line 345
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "weierp"

    const/16 v4, 0x2118

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9b

    aput-object v0, v1, v2

    .line 346
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "image"

    const/16 v4, 0x2111

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9c

    aput-object v0, v1, v2

    .line 347
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "real"

    const/16 v4, 0x211c

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9d

    aput-object v0, v1, v2

    .line 348
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "trade"

    const/16 v4, 0x2122

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9e

    aput-object v0, v1, v2

    .line 349
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "alefsym"

    const/16 v4, 0x2135

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x9f

    aput-object v0, v1, v2

    .line 353
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "larr"

    const/16 v4, 0x2190

    invoke-direct {v0, v2, v4}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v3

    .line 354
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "uarr"

    const/16 v3, 0x2191

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v5

    .line 355
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rarr"

    const/16 v3, 0x2192

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v6

    .line 356
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "darr"

    const/16 v3, 0x2193

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v7

    .line 357
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "harr"

    const/16 v3, 0x2194

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v8

    .line 358
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "crarr"

    const/16 v3, 0x21b5

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v9

    .line 359
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lArr"

    const/16 v3, 0x21d0

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v10

    .line 363
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "uArr"

    const/16 v3, 0x21d1

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v11

    .line 364
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rArr"

    const/16 v3, 0x21d2

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v12

    .line 368
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "dArr"

    const/16 v3, 0x21d3

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v13

    .line 369
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "hArr"

    const/16 v3, 0x21d4

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    aput-object v0, v1, v14

    .line 371
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "forall"

    const/16 v3, 0x2200

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xab

    aput-object v0, v1, v2

    .line 372
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "part"

    const/16 v3, 0x2202

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xac

    aput-object v0, v1, v2

    .line 373
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "exist"

    const/16 v3, 0x2203

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xad

    aput-object v0, v1, v2

    .line 374
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "empty"

    const/16 v3, 0x2205

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xae

    aput-object v0, v1, v2

    .line 375
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "nabla"

    const/16 v3, 0x2207

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xaf

    aput-object v0, v1, v2

    .line 376
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "isin"

    const/16 v3, 0x2208

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb0

    aput-object v0, v1, v2

    .line 377
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "notin"

    const/16 v3, 0x2209

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb1

    aput-object v0, v1, v2

    .line 378
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ni"

    const/16 v3, 0x220b

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb2

    aput-object v0, v1, v2

    .line 380
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "prod"

    const/16 v3, 0x220f

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb3

    aput-object v0, v1, v2

    .line 383
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sum"

    const/16 v3, 0x2211

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb4

    aput-object v0, v1, v2

    .line 386
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "minus"

    const/16 v3, 0x2212

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb5

    aput-object v0, v1, v2

    .line 387
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lowast"

    const/16 v3, 0x2217

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb6

    aput-object v0, v1, v2

    .line 388
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "radic"

    const/16 v3, 0x221a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb7

    aput-object v0, v1, v2

    .line 389
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "prop"

    const/16 v3, 0x221d

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb8

    aput-object v0, v1, v2

    .line 390
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "infin"

    const/16 v3, 0x221e

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xb9

    aput-object v0, v1, v2

    .line 391
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ang"

    const/16 v3, 0x2220

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xba

    aput-object v0, v1, v2

    .line 392
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "and"

    const/16 v3, 0x2227

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xbb

    aput-object v0, v1, v2

    .line 393
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "or"

    const/16 v3, 0x2228

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xbc

    aput-object v0, v1, v2

    .line 394
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "cap"

    const/16 v3, 0x2229

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xbd

    aput-object v0, v1, v2

    .line 395
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "cup"

    const/16 v3, 0x222a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xbe

    aput-object v0, v1, v2

    .line 396
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "int"

    const/16 v3, 0x222b

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xbf

    aput-object v0, v1, v2

    .line 397
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "there4"

    const/16 v3, 0x2234

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc0

    aput-object v0, v1, v2

    .line 398
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sim"

    const/16 v3, 0x223c

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc1

    aput-object v0, v1, v2

    .line 401
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "cong"

    const/16 v3, 0x2245

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc2

    aput-object v0, v1, v2

    .line 402
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "asymp"

    const/16 v3, 0x2248

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc3

    aput-object v0, v1, v2

    .line 403
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ne"

    const/16 v3, 0x2260

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc4

    aput-object v0, v1, v2

    .line 404
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "equiv"

    const/16 v3, 0x2261

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc5

    aput-object v0, v1, v2

    .line 405
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "le"

    const/16 v3, 0x2264

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc6

    aput-object v0, v1, v2

    .line 406
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ge"

    const/16 v3, 0x2265

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc7

    aput-object v0, v1, v2

    .line 407
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sub"

    const/16 v3, 0x2282

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc8

    aput-object v0, v1, v2

    .line 408
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sup"

    const/16 v3, 0x2283

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xc9

    aput-object v0, v1, v2

    .line 412
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "nsub"

    const/16 v3, 0x2284

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xca

    aput-object v0, v1, v2

    .line 413
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sube"

    const/16 v3, 0x2286

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xcb

    aput-object v0, v1, v2

    .line 414
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "supe"

    const/16 v3, 0x2287

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xcc

    aput-object v0, v1, v2

    .line 415
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "oplus"

    const/16 v3, 0x2295

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xcd

    aput-object v0, v1, v2

    .line 416
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "otimes"

    const/16 v3, 0x2297

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xce

    aput-object v0, v1, v2

    .line 417
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "perp"

    const/16 v3, 0x22a5

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xcf

    aput-object v0, v1, v2

    .line 418
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sdot"

    const/16 v3, 0x22c5

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd0

    aput-object v0, v1, v2

    .line 421
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lceil"

    const/16 v3, 0x2308

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd1

    aput-object v0, v1, v2

    .line 422
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rceil"

    const/16 v3, 0x2309

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd2

    aput-object v0, v1, v2

    .line 423
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lfloor"

    const/16 v3, 0x230a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd3

    aput-object v0, v1, v2

    .line 424
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rfloor"

    const/16 v3, 0x230b

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd4

    aput-object v0, v1, v2

    .line 425
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lang"

    const/16 v3, 0x2329

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd5

    aput-object v0, v1, v2

    .line 428
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rang"

    const/16 v3, 0x232a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd6

    aput-object v0, v1, v2

    .line 432
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "loz"

    const/16 v3, 0x25ca

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd7

    aput-object v0, v1, v2

    .line 434
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "spades"

    const/16 v3, 0x2660

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd8

    aput-object v0, v1, v2

    .line 436
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "clubs"

    const/16 v3, 0x2663

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xd9

    aput-object v0, v1, v2

    .line 437
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "hearts"

    const/16 v3, 0x2665

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xda

    aput-object v0, v1, v2

    .line 438
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "diams"

    const/16 v3, 0x2666

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xdb

    aput-object v0, v1, v2

    .line 455
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "quot"

    const/16 v3, 0x22

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xdc

    aput-object v0, v1, v2

    .line 456
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "amp"

    const/16 v3, 0x26

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xdd

    aput-object v0, v1, v2

    .line 457
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lt"

    const/16 v3, 0x3c

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xde

    aput-object v0, v1, v2

    .line 458
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "gt"

    const/16 v3, 0x3e

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xdf

    aput-object v0, v1, v2

    .line 460
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "OElig"

    const/16 v3, 0x152

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe0

    aput-object v0, v1, v2

    .line 461
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "oelig"

    const/16 v3, 0x153

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe1

    aput-object v0, v1, v2

    .line 463
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Scaron"

    const/16 v3, 0x160

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe2

    aput-object v0, v1, v2

    .line 464
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "scaron"

    const/16 v3, 0x161

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe3

    aput-object v0, v1, v2

    .line 465
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Yuml"

    const/16 v3, 0x178

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe4

    aput-object v0, v1, v2

    .line 467
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "circ"

    const/16 v3, 0x2c6

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe5

    aput-object v0, v1, v2

    .line 468
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "tilde"

    const/16 v3, 0x2dc

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe6

    aput-object v0, v1, v2

    .line 470
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ensp"

    const/16 v3, 0x2002

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe7

    aput-object v0, v1, v2

    .line 471
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "emsp"

    const/16 v3, 0x2003

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe8

    aput-object v0, v1, v2

    .line 472
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "thinsp"

    const/16 v3, 0x2009

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xe9

    aput-object v0, v1, v2

    .line 473
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "zwnj"

    const/16 v3, 0x200c

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xea

    aput-object v0, v1, v2

    .line 474
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "zwj"

    const/16 v3, 0x200d

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xeb

    aput-object v0, v1, v2

    .line 475
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lrm"

    const/16 v3, 0x200e

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xec

    aput-object v0, v1, v2

    .line 476
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rlm"

    const/16 v3, 0x200f

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xed

    aput-object v0, v1, v2

    .line 477
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ndash"

    const/16 v3, 0x2013

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xee

    aput-object v0, v1, v2

    .line 478
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "mdash"

    const/16 v3, 0x2014

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xef

    aput-object v0, v1, v2

    .line 479
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lsquo"

    const/16 v3, 0x2018

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf0

    aput-object v0, v1, v2

    .line 480
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rsquo"

    const/16 v3, 0x2019

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf1

    aput-object v0, v1, v2

    .line 481
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "sbquo"

    const/16 v3, 0x201a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf2

    aput-object v0, v1, v2

    .line 482
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "ldquo"

    const/16 v3, 0x201c

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf3

    aput-object v0, v1, v2

    .line 483
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rdquo"

    const/16 v3, 0x201d

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf4

    aput-object v0, v1, v2

    .line 484
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "bdquo"

    const/16 v3, 0x201e

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf5

    aput-object v0, v1, v2

    .line 485
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "dagger"

    const/16 v3, 0x2020

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf6

    aput-object v0, v1, v2

    .line 486
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "Dagger"

    const/16 v3, 0x2021

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf7

    aput-object v0, v1, v2

    .line 487
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "permil"

    const/16 v3, 0x2030

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf8

    aput-object v0, v1, v2

    .line 488
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "lsaquo"

    const/16 v3, 0x2039

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xf9

    aput-object v0, v1, v2

    .line 490
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "rsaquo"

    const/16 v3, 0x203a

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xfa

    aput-object v0, v1, v2

    .line 492
    new-instance v0, Lorg/htmlparser/util/CharacterReference;

    const-string v2, "euro"

    const/16 v3, 0x20ac

    invoke-direct {v0, v2, v3}, Lorg/htmlparser/util/CharacterReference;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0xfb

    aput-object v0, v1, v2

    .line 159
    sput-object v1, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 519
    :goto_0
    sget-object v2, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    array-length v3, v2

    const/16 v4, 0x100

    if-lt v0, v3, :cond_5

    .line 523
    array-length v0, v2

    add-int/2addr v0, v4

    sub-int/2addr v0, v1

    new-array v0, v0, [Lorg/htmlparser/util/CharacterReference;

    sput-object v0, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    const/4 v3, 0x0

    const/16 v5, 0x100

    .line 525
    :goto_1
    sget-object v0, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    array-length v1, v0

    if-lt v3, v1, :cond_0

    .line 551
    invoke-static {v0}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Lorg/htmlparser/util/sort/Ordered;)V

    return-void

    .line 527
    :cond_0
    aget-object v6, v0, v3

    .line 528
    aget-object v0, v0, v3

    invoke-virtual {v0}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v7

    if-ge v7, v4, :cond_1

    .line 530
    sget-object v0, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    aput-object v6, v0, v7

    goto :goto_5

    :cond_1
    const/16 v0, 0x100

    :goto_2
    if-lt v0, v5, :cond_2

    goto :goto_3

    .line 536
    :cond_2
    sget-object v1, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v1

    if-le v1, v7, :cond_4

    :goto_3
    add-int/lit8 v1, v5, -0x1

    :goto_4
    if-ge v1, v0, :cond_3

    .line 546
    sget-object v1, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    aput-object v6, v1, v0

    add-int/lit8 v5, v5, 0x1

    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 543
    :cond_3
    sget-object v2, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    add-int/lit8 v7, v1, 0x1

    aget-object v8, v2, v1

    aput-object v8, v2, v7

    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 520
    :cond_5
    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v2

    if-ge v2, v4, :cond_6

    add-int/lit8 v1, v1, 0x1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .line 558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 16

    move-object/from16 v0, p0

    const/16 v1, 0x26

    .line 749
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v3, v2, :cond_0

    goto/16 :goto_c

    :cond_0
    const/4 v4, 0x0

    .line 755
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 756
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    move-object v8, v4

    move v4, v2

    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-lt v2, v4, :cond_14

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v5, :cond_10

    .line 767
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x23

    const/16 v11, 0x3b

    if-ne v10, v9, :cond_9

    add-int/lit8 v2, v2, 0x1

    move v9, v2

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :cond_2
    :goto_1
    if-ge v9, v5, :cond_8

    if-eqz v10, :cond_3

    goto :goto_6

    .line 778
    :cond_3
    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v7, 0x10

    if-eq v15, v11, :cond_7

    const/16 v12, 0x58

    if-eq v15, v12, :cond_6

    const/16 v12, 0x78

    if-eq v15, v12, :cond_6

    const/16 v12, 0xa

    packed-switch v15, :pswitch_data_0

    packed-switch v15, :pswitch_data_1

    packed-switch v15, :pswitch_data_2

    :cond_4
    :goto_2
    const/4 v10, 0x1

    goto :goto_5

    :pswitch_0
    if-ne v7, v14, :cond_4

    mul-int v13, v13, v14

    add-int/lit8 v15, v15, -0x61

    goto :goto_3

    :pswitch_1
    if-ne v7, v14, :cond_4

    mul-int v13, v13, v14

    add-int/lit8 v15, v15, -0x41

    :goto_3
    add-int/2addr v15, v12

    add-int/2addr v13, v15

    goto :goto_5

    :pswitch_2
    if-nez v14, :cond_5

    goto :goto_4

    :cond_5
    move v12, v14

    :goto_4
    mul-int v13, v13, v12

    add-int/lit8 v15, v15, -0x30

    add-int/2addr v13, v15

    move v14, v12

    goto :goto_5

    :cond_6
    if-nez v14, :cond_4

    const/16 v14, 0x10

    goto :goto_5

    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :goto_5
    if-nez v10, :cond_2

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_8
    :goto_6
    if-eqz v13, :cond_10

    int-to-char v2, v13

    .line 837
    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v9

    move v4, v2

    goto :goto_a

    .line 843
    :cond_9
    invoke-static {v9}, Ljava/lang/Character;->isLetter(C)Z

    move-result v7

    if-eqz v7, :cond_10

    add-int/lit8 v7, v2, 0x1

    move v10, v5

    const/4 v9, 0x0

    :goto_7
    if-ge v7, v5, :cond_d

    if-eqz v9, :cond_a

    goto :goto_9

    .line 851
    :cond_a
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v12

    if-ne v11, v12, :cond_b

    add-int/lit8 v9, v7, 0x1

    move v10, v7

    move v7, v9

    :goto_8
    const/4 v9, 0x1

    goto :goto_7

    .line 858
    :cond_b
    invoke-static {v12}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v12

    if-eqz v12, :cond_c

    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_c
    move v10, v7

    goto :goto_8

    :cond_d
    :goto_9
    if-nez v8, :cond_e

    .line 868
    new-instance v8, Lorg/htmlparser/util/CharacterReferenceEx;

    invoke-direct {v8}, Lorg/htmlparser/util/CharacterReferenceEx;-><init>()V

    .line 869
    :cond_e
    invoke-virtual {v8, v0}, Lorg/htmlparser/util/CharacterReferenceEx;->setKernel(Ljava/lang/String;)V

    .line 870
    invoke-virtual {v8, v2}, Lorg/htmlparser/util/CharacterReferenceEx;->setStart(I)V

    .line 871
    invoke-virtual {v8, v10}, Lorg/htmlparser/util/CharacterReferenceEx;->setEnd(I)V

    .line 872
    invoke-static {v8}, Lorg/htmlparser/util/Translate;->lookup(Lorg/htmlparser/util/CharacterReference;)Lorg/htmlparser/util/CharacterReference;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 875
    invoke-virtual {v7}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 876
    invoke-virtual {v7}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v2, v4

    if-ge v2, v5, :cond_f

    .line 877
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v11, v4, :cond_f

    add-int/lit8 v2, v2, 0x1

    :cond_f
    move v4, v2

    move v2, v4

    :cond_10
    :goto_a
    if-lt v4, v2, :cond_13

    if-ge v2, v5, :cond_11

    .line 891
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    if-ne v3, v4, :cond_1

    :cond_11
    :goto_b
    if-lt v2, v5, :cond_12

    .line 896
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_12
    add-int/lit8 v1, v2, 0x1

    .line 895
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v1

    goto :goto_b

    :cond_13
    add-int/lit8 v7, v4, 0x1

    .line 889
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v4, v7

    goto :goto_a

    :cond_14
    add-int/lit8 v7, v2, 0x1

    .line 762
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v7

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x41
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x61
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static decode(Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 0

    .line 911
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/htmlparser/util/Translate;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static decode(Ljava/io/InputStream;Ljava/io/PrintStream;)V
    .locals 6

    .line 938
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    const-string v2, "ISO-8859-1"

    invoke-direct {v1, p0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    goto/16 :goto_7

    :catch_0
    move-exception p0

    goto/16 :goto_5

    .line 943
    :catch_1
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 945
    :goto_0
    new-instance p0, Ljava/lang/StringBuffer;

    const/16 v1, 0x400

    invoke-direct {p0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 947
    sget-boolean v1, Lorg/htmlparser/util/Translate;->DECODE_LINE_BY_LINE:Z

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    .line 948
    :goto_1
    invoke-virtual {v0}, Ljava/io/Reader;->read()I

    move-result v4

    if-ne v2, v4, :cond_0

    goto :goto_4

    :cond_0
    const/16 v5, 0xd

    if-eq v5, v4, :cond_3

    const/16 v5, 0xa

    if-ne v5, v4, :cond_1

    goto :goto_2

    :cond_1
    if-eqz v1, :cond_2

    .line 965
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 966
    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    const/4 v1, 0x0

    :cond_2
    int-to-char v4, v4

    .line 969
    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_3
    :goto_2
    if-nez v1, :cond_4

    .line 954
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/htmlparser/util/Translate;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 955
    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 956
    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    const/4 v1, 0x1

    :cond_4
    int-to-char v4, v4

    .line 959
    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 973
    :cond_5
    :goto_3
    invoke-virtual {v0}, Ljava/io/Reader;->read()I

    move-result v1

    if-ne v2, v1, :cond_7

    const/4 v1, 0x0

    .line 975
    :goto_4
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-eqz v0, :cond_8

    if-eqz v1, :cond_6

    .line 978
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_6

    .line 981
    :cond_6
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/htmlparser/util/Translate;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 982
    invoke-virtual {p1, p0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    int-to-char v1, v1

    .line 974
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 988
    :goto_5
    :try_start_2
    invoke-virtual {p1}, Ljava/io/PrintStream;->println()V

    .line 989
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 993
    :cond_8
    :goto_6
    invoke-virtual {p1}, Ljava/io/PrintStream;->flush()V

    return-void

    :goto_7
    invoke-virtual {p1}, Ljava/io/PrintStream;->flush()V

    .line 994
    goto :goto_9

    :goto_8
    throw p0

    :goto_9
    goto :goto_8
.end method

.method public static encode(I)Ljava/lang/String;
    .locals 2

    .line 1008
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v1, "&#"

    .line 1009
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1010
    sget-boolean v1, Lorg/htmlparser/util/Translate;->ENCODE_HEXADECIMAL:Z

    if-eqz v1, :cond_0

    const-string v1, "x"

    .line 1012
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1013
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1016
    :cond_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :goto_0
    const/16 p0, 0x3b

    .line 1017
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1019
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1036
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x6

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1037
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    .line 1064
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 1040
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 1041
    invoke-static {v3}, Lorg/htmlparser/util/Translate;->lookup(C)Lorg/htmlparser/util/CharacterReference;

    move-result-object v4

    const/16 v5, 0x3b

    if-eqz v4, :cond_1

    const/16 v3, 0x26

    .line 1044
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1045
    invoke-virtual {v4}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1046
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_1
    const/16 v4, 0x7f

    if-lt v3, v4, :cond_3

    const-string v4, "&#"

    .line 1050
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1051
    sget-boolean v4, Lorg/htmlparser/util/Translate;->ENCODE_HEXADECIMAL:Z

    if-eqz v4, :cond_2

    const-string v4, "x"

    .line 1053
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1054
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1057
    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1058
    :goto_1
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1061
    :cond_3
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static encode(Ljava/io/InputStream;Ljava/io/PrintStream;)V
    .locals 5

    const-string v0, "ISO-8859-1"

    .line 1086
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p0, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1087
    new-instance v2, Ljava/io/PrintWriter;

    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, p1, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1092
    :catch_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1093
    new-instance v2, Ljava/io/PrintWriter;

    new-instance p0, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v2, p0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    :goto_0
    const/4 p0, -0x1

    .line 1097
    :try_start_1
    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne p0, p1, :cond_0

    .line 1130
    :goto_1
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    goto :goto_3

    :cond_0
    int-to-char p0, p1

    .line 1100
    :try_start_2
    invoke-static {p0}, Lorg/htmlparser/util/Translate;->lookup(C)Lorg/htmlparser/util/CharacterReference;

    move-result-object p1

    const/16 v0, 0x3b

    if-eqz p1, :cond_1

    const/16 p0, 0x26

    .line 1103
    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->print(C)V

    .line 1104
    invoke-virtual {p1}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1105
    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->print(C)V

    goto :goto_0

    :cond_1
    const/16 p1, 0x7f

    if-lt p0, p1, :cond_3

    const-string p1, "&#"

    .line 1109
    invoke-virtual {v2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1110
    sget-boolean p1, Lorg/htmlparser/util/Translate;->ENCODE_HEXADECIMAL:Z

    if-eqz p1, :cond_2

    const-string p1, "x"

    .line 1112
    invoke-virtual {v2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1113
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_2

    .line 1116
    :cond_2
    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->print(I)V

    .line 1117
    :goto_2
    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->print(C)V

    goto :goto_0

    .line 1120
    :cond_3
    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->print(C)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    goto :goto_4

    :catch_1
    move-exception p0

    .line 1125
    :try_start_3
    invoke-virtual {v2}, Ljava/io/PrintWriter;->println()V

    .line 1126
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :goto_3
    return-void

    .line 1130
    :goto_4
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 1131
    goto :goto_6

    :goto_5
    throw p0

    :goto_6
    goto :goto_5
.end method

.method protected static lookup([Lorg/htmlparser/util/CharacterReference;CII)I
    .locals 7

    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    const/4 v1, -0x1

    move v2, v0

    move v0, p3

    move p3, p2

    const/4 p2, -0x1

    :goto_0
    if-ne v1, p2, :cond_5

    if-le p3, v0, :cond_0

    goto :goto_3

    .line 582
    :cond_0
    div-int/lit8 v3, v2, 0x2

    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v5, v3, -0x1

    :goto_1
    add-int/2addr v5, p3

    .line 584
    aget-object v6, p0, v5

    invoke-virtual {v6}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v6

    sub-int v6, p1, v6

    if-nez v6, :cond_2

    move p2, v5

    goto :goto_0

    :cond_2
    if-gez v6, :cond_4

    add-int/lit8 v0, v5, -0x1

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_4
    add-int/lit8 p3, v5, 0x1

    :goto_2
    move v2, v3

    goto :goto_0

    :cond_5
    :goto_3
    if-ne v1, p2, :cond_6

    move p2, p3

    :cond_6
    return p2
.end method

.method public static lookup(C)Lorg/htmlparser/util/CharacterReference;
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0x100

    if-ge p0, v1, :cond_0

    .line 617
    sget-object v0, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    aget-object v0, v0, p0

    goto :goto_0

    .line 620
    :cond_0
    sget-object v2, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, p0, v1, v3}, Lorg/htmlparser/util/Translate;->lookup([Lorg/htmlparser/util/CharacterReference;CII)I

    move-result v1

    .line 621
    sget-object v2, Lorg/htmlparser/util/Translate;->mCharacterList:[Lorg/htmlparser/util/CharacterReference;

    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 623
    aget-object v1, v2, v1

    .line 624
    invoke-virtual {v1}, Lorg/htmlparser/util/CharacterReference;->getCharacter()I

    move-result v2

    if-eq p0, v2, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :cond_2
    :goto_0
    return-object v0
.end method

.method public static lookup(Ljava/lang/String;II)Lorg/htmlparser/util/CharacterReference;
    .locals 1

    .line 719
    new-instance v0, Lorg/htmlparser/util/CharacterReferenceEx;

    invoke-direct {v0}, Lorg/htmlparser/util/CharacterReferenceEx;-><init>()V

    .line 720
    invoke-virtual {v0, p0}, Lorg/htmlparser/util/CharacterReferenceEx;->setKernel(Ljava/lang/String;)V

    .line 721
    invoke-virtual {v0, p1}, Lorg/htmlparser/util/CharacterReferenceEx;->setStart(I)V

    .line 722
    invoke-virtual {v0, p2}, Lorg/htmlparser/util/CharacterReferenceEx;->setEnd(I)V

    .line 724
    invoke-static {v0}, Lorg/htmlparser/util/Translate;->lookup(Lorg/htmlparser/util/CharacterReference;)Lorg/htmlparser/util/CharacterReference;

    move-result-object p0

    return-object p0
.end method

.method protected static lookup(Lorg/htmlparser/util/CharacterReference;)Lorg/htmlparser/util/CharacterReference;
    .locals 7

    .line 659
    sget-object v0, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    invoke-static {v0, p0}, Lorg/htmlparser/util/sort/Sort;->bsearch([Lorg/htmlparser/util/sort/Ordered;Lorg/htmlparser/util/sort/Ordered;)I

    move-result v0

    .line 660
    invoke-virtual {p0}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object p0

    .line 661
    sget-object v1, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-ge v0, v2, :cond_1

    .line 663
    aget-object v1, v1, v0

    .line 664
    invoke-virtual {v1}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v2

    .line 669
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    .line 665
    invoke-virtual {p0, v4, v2, v4, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v3, v1

    :cond_1
    :goto_0
    if-nez v3, :cond_4

    .line 677
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_3

    goto :goto_1

    .line 680
    :cond_3
    sget-object v2, Lorg/htmlparser/util/Translate;->mCharacterReferences:[Lorg/htmlparser/util/CharacterReference;

    aget-object v2, v2, v0

    .line 681
    invoke-virtual {v2}, Lorg/htmlparser/util/CharacterReference;->getKernel()Ljava/lang/String;

    move-result-object v5

    .line 682
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v1, v6, :cond_4

    .line 688
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    .line 684
    invoke-virtual {p0, v4, v5, v4, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_4
    :goto_1
    move-object v2, v3

    :goto_2
    return-object v2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 2

    .line 1145
    array-length v0, p0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    aget-object p0, p0, v1

    const-string v0, "-encode"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    .line 1150
    sget-object p0, Ljava/lang/System;->in:Ljava/io/InputStream;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {p0, v0}, Lorg/htmlparser/util/Translate;->encode(Ljava/io/InputStream;Ljava/io/PrintStream;)V

    goto :goto_0

    .line 1152
    :cond_1
    sget-object p0, Ljava/lang/System;->in:Ljava/io/InputStream;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {p0, v0}, Lorg/htmlparser/util/Translate;->decode(Ljava/io/InputStream;Ljava/io/PrintStream;)V

    :goto_0
    return-void
.end method
