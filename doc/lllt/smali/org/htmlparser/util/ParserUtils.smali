.class public Lorg/htmlparser/util/ParserUtils;
.super Ljava/lang/Object;
.source "ParserUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createDummyString(CI)Ljava/lang/String;
    .locals 2

    .line 1232
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-lt v1, p1, :cond_0

    .line 1235
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object p0

    .line 1234
    :cond_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static createParserParsingAnInputString(Ljava/lang/String;)Lorg/htmlparser/Parser;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1139
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    .line 1140
    new-instance v1, Lorg/htmlparser/lexer/Lexer;

    invoke-direct {v1}, Lorg/htmlparser/lexer/Lexer;-><init>()V

    .line 1141
    new-instance v2, Lorg/htmlparser/lexer/Page;

    invoke-direct {v2, p0}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    .line 1142
    invoke-virtual {v1, v2}, Lorg/htmlparser/lexer/Lexer;->setPage(Lorg/htmlparser/lexer/Page;)V

    .line 1143
    invoke-virtual {v0, v1}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    return-object v0
.end method

.method public static findTypeInNode(Lorg/htmlparser/Node;Ljava/lang/Class;)[Lorg/htmlparser/Node;
    .locals 2

    .line 85
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 86
    new-instance v1, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v1, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 87
    invoke-interface {p0, v0, v1}, Lorg/htmlparser/Node;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    .line 89
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->toNodeArray()[Lorg/htmlparser/Node;

    move-result-object p0

    return-object p0
.end method

.method private static getLinks(Ljava/lang/String;Ljava/lang/String;Z)Lorg/htmlparser/util/NodeList;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1153
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    .line 1154
    new-instance v0, Lorg/htmlparser/filters/TagNameFilter;

    invoke-direct {v0, p1}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    .line 1155
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 1156
    invoke-static {p0}, Lorg/htmlparser/util/ParserUtils;->createParserParsingAnInputString(Ljava/lang/String;)Lorg/htmlparser/Parser;

    move-result-object p0

    .line 1157
    invoke-virtual {p0, v0}, Lorg/htmlparser/Parser;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object p0

    if-nez p2, :cond_3

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 1164
    :goto_0
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    goto :goto_2

    .line 1166
    :cond_0
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/tags/CompositeTag;

    .line 1167
    invoke-virtual {v0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v1

    .line 1168
    invoke-virtual {v0}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v0

    .line 1169
    invoke-interface {v1}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v1

    move v2, p2

    const/4 p2, 0x0

    .line 1170
    :goto_1
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt p2, v3, :cond_1

    add-int/lit8 p2, v2, 0x1

    goto :goto_0

    .line 1172
    :cond_1
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/CompositeTag;

    .line 1173
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v4

    .line 1174
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v3

    .line 1175
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v4

    if-eq p2, v2, :cond_2

    if-le v3, v0, :cond_2

    if-ge v4, v1, :cond_2

    .line 1178
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    add-int/lit8 p2, p2, -0x1

    add-int/lit8 v2, v2, -0x1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    return-object p0
.end method

.method private static getLinks(Ljava/lang/String;Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1194
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    .line 1195
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 1196
    invoke-static {p0}, Lorg/htmlparser/util/ParserUtils;->createParserParsingAnInputString(Ljava/lang/String;)Lorg/htmlparser/Parser;

    move-result-object p0

    .line 1197
    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object p0

    if-nez p2, :cond_3

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 1204
    :goto_0
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    goto :goto_2

    .line 1206
    :cond_0
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/tags/CompositeTag;

    .line 1207
    invoke-virtual {v0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v1

    .line 1208
    invoke-virtual {v0}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v0

    .line 1209
    invoke-interface {v1}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v1

    move v2, p2

    const/4 p2, 0x0

    .line 1210
    :goto_1
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt p2, v3, :cond_1

    add-int/lit8 p2, v2, 0x1

    goto :goto_0

    .line 1212
    :cond_1
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/CompositeTag;

    .line 1213
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v4

    .line 1214
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v3

    .line 1215
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v4

    if-eq p2, v2, :cond_2

    if-le v3, v0, :cond_2

    if-ge v4, v1, :cond_2

    .line 1218
    invoke-virtual {p0, p2}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    add-int/lit8 p2, p2, -0x1

    add-int/lit8 v2, v2, -0x1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    return-object p0
.end method

.method private static modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;
    .locals 4

    sub-int v0, p2, p1

    const/16 v1, 0x2a

    .line 1240
    invoke-static {v1, v0}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v0

    .line 1241
    new-instance v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {p0, v3, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {p0, p2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static removeChars(Ljava/lang/String;C)Ljava/lang/String;
    .locals 3

    .line 47
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 49
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 50
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, p1, :cond_1

    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static removeEscapeCharacters(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0xd

    .line 58
    invoke-static {p0, v0}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0xa

    .line 59
    invoke-static {p0, v0}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x9

    .line 60
    invoke-static {p0, v0}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static removeTrailingBlanks(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v0, 0x20

    move-object v1, p0

    const/16 p0, 0x20

    :cond_0
    :goto_0
    if-eq p0, v0, :cond_1

    return-object v1

    .line 67
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {v1, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    if-ne p0, v0, :cond_0

    const/4 v2, 0x0

    .line 69
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static splitButChars(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11

    .line 395
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 397
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    move-object v3, v1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 401
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-lt v1, v6, :cond_2

    .line 426
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    if-eqz p0, :cond_0

    add-int/2addr v4, v7

    .line 428
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 429
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 430
    new-instance p0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ljava/lang/StringBuffer;-><init>()V

    .line 435
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 436
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 437
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v8, p0, [Ljava/lang/String;

    .line 438
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lt v2, p0, :cond_1

    return-object v8

    .line 439
    :cond_1
    new-instance p0, Ljava/lang/String;

    aget-object p1, v6, v2

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object p0, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 404
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v6, v9, :cond_7

    if-eqz v8, :cond_3

    .line 409
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    if-nez v5, :cond_4

    const/4 v5, 0x1

    :cond_4
    :goto_3
    if-eqz v5, :cond_6

    .line 416
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-eqz v6, :cond_6

    add-int/lit8 v4, v4, 0x1

    .line 418
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 419
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 420
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, -0x1

    :cond_6
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 405
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v9, v10, :cond_8

    const/4 v8, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method

.method public static splitButDigits(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    move-object v3, v1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 111
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-lt v1, v6, :cond_2

    .line 136
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    if-eqz p0, :cond_0

    add-int/2addr v4, v7

    .line 138
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 139
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 140
    new-instance p0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ljava/lang/StringBuffer;-><init>()V

    .line 145
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 146
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 147
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v8, p0, [Ljava/lang/String;

    .line 148
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lt v2, p0, :cond_1

    return-object v8

    .line 149
    :cond_1
    new-instance p0, Ljava/lang/String;

    aget-object p1, v6, v2

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object p0, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 114
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v6, v9, :cond_8

    .line 117
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_4

    if-eqz v8, :cond_3

    goto :goto_3

    :cond_3
    if-nez v5, :cond_5

    const/4 v5, 0x1

    goto :goto_4

    .line 119
    :cond_4
    :goto_3
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    :cond_5
    :goto_4
    if-eqz v5, :cond_7

    .line 126
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-eqz v6, :cond_7

    add-int/lit8 v4, v4, 0x1

    .line 128
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 129
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 130
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_5

    :cond_6
    add-int/lit8 v4, v4, -0x1

    :cond_7
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 115
    :cond_8
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v9, v10, :cond_9

    const/4 v8, 0x1

    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method

.method public static splitChars(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11

    .line 539
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 541
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    move-object v3, v1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 545
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-lt v1, v6, :cond_2

    .line 570
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    if-eqz p0, :cond_0

    add-int/2addr v4, v7

    .line 572
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 573
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 574
    new-instance p0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ljava/lang/StringBuffer;-><init>()V

    .line 579
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 580
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 581
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v8, p0, [Ljava/lang/String;

    .line 582
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lt v2, p0, :cond_1

    return-object v8

    .line 583
    :cond_1
    new-instance p0, Ljava/lang/String;

    aget-object p1, v6, v2

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object p0, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 548
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v6, v9, :cond_7

    if-nez v8, :cond_3

    .line 553
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    if-nez v5, :cond_4

    const/4 v5, 0x1

    :cond_4
    :goto_3
    if-eqz v5, :cond_6

    .line 560
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-eqz v6, :cond_6

    add-int/lit8 v4, v4, 0x1

    .line 562
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 563
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 564
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, -0x1

    :cond_6
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 549
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v9, v10, :cond_8

    const/4 v8, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method

.method public static splitSpaces(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 252
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    move-object v3, v1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 256
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-lt v1, v6, :cond_2

    .line 281
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    if-eqz p0, :cond_0

    add-int/2addr v4, v7

    .line 283
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 284
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 285
    new-instance p0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ljava/lang/StringBuffer;-><init>()V

    .line 290
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 291
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 292
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v8, p0, [Ljava/lang/String;

    .line 293
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lt v2, p0, :cond_1

    return-object v8

    .line 294
    :cond_1
    new-instance p0, Ljava/lang/String;

    aget-object p1, v6, v2

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object p0, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 259
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v6, v9, :cond_7

    .line 262
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v6

    if-nez v6, :cond_3

    if-nez v8, :cond_3

    .line 264
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    if-nez v5, :cond_4

    const/4 v5, 0x1

    :cond_4
    :goto_3
    if-eqz v5, :cond_6

    .line 271
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-eqz v6, :cond_6

    add-int/lit8 v4, v4, 0x1

    .line 273
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 274
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 275
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, -0x1

    :cond_6
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 260
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v9, v10, :cond_8

    const/4 v8, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method

.method public static splitTags(Ljava/lang/String;Ljava/lang/Class;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 792
    new-instance v0, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v0, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    const/4 p1, 0x1

    invoke-static {p0, v0, p1, p1}, Lorg/htmlparser/util/ParserUtils;->splitTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static splitTags(Ljava/lang/String;Ljava/lang/Class;ZZ)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 805
    new-instance v0, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v0, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    invoke-static {p0, v0, p2, p3}, Lorg/htmlparser/util/ParserUtils;->splitTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static splitTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 818
    invoke-static {p0, p1, v0, v0}, Lorg/htmlparser/util/ParserUtils;->splitTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static splitTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)[Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 832
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 834
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 836
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    invoke-static {v2, v1}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v1

    .line 839
    invoke-static {p0, p1, p2}, Lorg/htmlparser/util/ParserUtils;->getLinks(Ljava/lang/String;Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    const/4 p2, 0x0

    move-object v3, v1

    const/4 v1, 0x0

    .line 840
    :goto_0
    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-lt v1, v4, :cond_6

    .line 861
    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    const/4 p3, 0x0

    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_4

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    goto :goto_4

    :cond_0
    const/16 v4, 0x2a

    .line 863
    invoke-virtual {v3, v4, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    if-eq v4, v1, :cond_2

    .line 866
    invoke-virtual {p0, p1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 867
    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    add-int/lit8 p3, p3, 0x1

    .line 870
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 871
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 872
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1}, Ljava/lang/String;-><init>()V

    goto :goto_2

    :cond_1
    add-int/lit8 p3, p3, -0x1

    :goto_2
    move p1, v1

    goto :goto_1

    .line 878
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    add-int/lit8 p3, p3, 0x1

    .line 882
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 883
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 884
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1}, Ljava/lang/String;-><init>()V

    goto :goto_3

    :cond_3
    add-int/lit8 p3, p3, -0x1

    :goto_3
    move p1, v4

    goto :goto_1

    .line 891
    :cond_4
    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 892
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v4

    .line 893
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v5, p0, [Ljava/lang/String;

    .line 894
    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lt p2, p0, :cond_5

    return-object v5

    .line 895
    :cond_5
    new-instance p0, Ljava/lang/String;

    aget-object p1, v4, p2

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object p0, v5, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_5

    .line 842
    :cond_6
    invoke-virtual {p1, v1}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    check-cast v4, Lorg/htmlparser/tags/CompositeTag;

    .line 843
    invoke-virtual {v4}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v5

    .line 846
    invoke-virtual {v4}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v6

    .line 847
    invoke-virtual {v4}, Lorg/htmlparser/tags/CompositeTag;->getEndPosition()I

    move-result v4

    .line 848
    invoke-interface {v5}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v7

    .line 849
    invoke-interface {v5}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v5

    if-eqz p3, :cond_7

    .line 853
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v6, v5}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    .line 857
    :cond_7
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v6, v4}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    .line 858
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v7, v5}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public static splitTags(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 677
    invoke-static {p0, p1, v0, v0}, Lorg/htmlparser/util/ParserUtils;->splitTags(Ljava/lang/String;[Ljava/lang/String;ZZ)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static splitTags(Ljava/lang/String;[Ljava/lang/String;ZZ)[Ljava/lang/String;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    move-object/from16 v0, p1

    .line 701
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 703
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 704
    new-instance v2, Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/String;

    .line 707
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v6, 0x20

    invoke-static {v6, v3}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v3

    move-object v7, v1

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 710
    :goto_0
    array-length v9, v0

    if-lt v1, v9, :cond_0

    return-object v5

    .line 714
    :cond_0
    aget-object v5, v0, v1

    move/from16 v9, p2

    invoke-static {v2, v5, v9}, Lorg/htmlparser/util/ParserUtils;->getLinks(Ljava/lang/String;Ljava/lang/String;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v5

    move-object v10, v3

    const/4 v3, 0x0

    .line 715
    :goto_1
    invoke-virtual {v5}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v11

    if-lt v3, v11, :cond_7

    .line 736
    invoke-virtual {v10, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    move v11, v8

    :goto_2
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_5

    const/4 v5, -0x1

    if-ne v3, v5, :cond_1

    goto :goto_5

    :cond_1
    const/16 v8, 0x2a

    .line 738
    invoke-virtual {v10, v8, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v8

    if-eq v8, v5, :cond_3

    .line 741
    invoke-virtual {v2, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 742
    invoke-virtual {v10, v6, v8}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    add-int/lit8 v11, v11, 0x1

    .line 745
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 746
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 747
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    goto :goto_3

    :cond_2
    add-int/lit8 v11, v11, -0x1

    :goto_3
    move v3, v5

    goto :goto_2

    .line 753
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v11, v11, 0x1

    .line 757
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 758
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 759
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    goto :goto_4

    :cond_4
    add-int/lit8 v11, v11, -0x1

    :goto_4
    move v3, v8

    goto :goto_2

    .line 764
    :cond_5
    :goto_5
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 765
    invoke-virtual {v7}, Ljava/util/ArrayList;->trimToSize()V

    .line 766
    invoke-virtual {v7}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v13

    .line 767
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v14, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 768
    :goto_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_6

    .line 773
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 774
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 775
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v6, v3}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v1, v1, 0x1

    move v8, v11

    move-object v5, v14

    goto/16 :goto_0

    .line 770
    :cond_6
    new-instance v3, Ljava/lang/String;

    aget-object v5, v13, v2

    check-cast v5, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v14, v2

    .line 771
    aget-object v3, v14, v2

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 717
    :cond_7
    invoke-virtual {v5, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v11

    check-cast v11, Lorg/htmlparser/tags/CompositeTag;

    .line 718
    invoke-virtual {v11}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v12

    .line 721
    invoke-virtual {v11}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v13

    .line 722
    invoke-virtual {v11}, Lorg/htmlparser/tags/CompositeTag;->getEndPosition()I

    move-result v11

    .line 723
    invoke-interface {v12}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v14

    .line 724
    invoke-interface {v12}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v12

    if-eqz p3, :cond_8

    .line 728
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v13, v12}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    .line 732
    :cond_8
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v15, v13, v11}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v10

    .line 733
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v14, v12}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v10

    :goto_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method

.method public static trimAllTags(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7

    .line 913
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/16 v2, 0x3e

    const/16 v3, 0x3c

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    .line 916
    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    const/4 v5, -0x1

    if-eq p1, v5, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p1

    if-eq p1, v5, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p1

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-ge p1, v5, :cond_0

    goto :goto_0

    .line 919
    :cond_0
    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    invoke-virtual {p0, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 920
    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p1

    add-int/2addr p1, v4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 917
    :cond_1
    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    const/4 v5, 0x1

    .line 924
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-lt p1, v6, :cond_3

    .line 935
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 926
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v3, :cond_4

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    :cond_4
    if-eqz v5, :cond_5

    .line 929
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 930
    :cond_5
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v2, :cond_6

    if-nez v5, :cond_6

    const/4 v5, 0x1

    :cond_6
    add-int/lit8 p1, p1, 0x1

    goto :goto_1
.end method

.method public static trimButChars(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 458
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 461
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 471
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 464
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v3, v5, :cond_2

    if-eqz v4, :cond_1

    .line 468
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 465
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_3

    const/4 v4, 0x1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static trimButCharsBeginEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 490
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 493
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 496
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_4

    if-nez v4, :cond_0

    goto :goto_2

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 499
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_2

    if-eqz v7, :cond_1

    move v5, v3

    const/4 v4, 0x0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 500
    :cond_2
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_3

    const/4 v7, 0x1

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    move v4, v0

    const/4 v3, 0x1

    :goto_3
    if-ltz v0, :cond_9

    if-nez v3, :cond_5

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 512
    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_7

    if-eqz v7, :cond_6

    move v4, v0

    const/4 v3, 0x0

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 513
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_8

    const/4 v7, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_9
    :goto_5
    add-int/2addr v4, v1

    .line 521
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimButDigits(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 168
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 171
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 181
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 174
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v3, v5, :cond_3

    .line 177
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v4, :cond_2

    .line 178
    :cond_1
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :cond_3
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_4

    const/4 v4, 0x1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static trimButDigitsBeginEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 200
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 203
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 206
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_5

    if-nez v4, :cond_0

    goto :goto_2

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 209
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_3

    .line 212
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_1

    if-eqz v7, :cond_2

    :cond_1
    move v5, v3

    const/4 v4, 0x0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 210
    :cond_3
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_4

    const/4 v7, 0x1

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    move v4, v0

    const/4 v3, 0x1

    :goto_3
    if-ltz v0, :cond_b

    if-nez v3, :cond_6

    goto :goto_5

    :cond_6
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 222
    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_9

    .line 225
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_7

    if-eqz v7, :cond_8

    :cond_7
    move v4, v0

    const/4 v3, 0x0

    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 223
    :cond_9
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_a

    const/4 v7, 0x1

    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_b
    :goto_5
    add-int/2addr v4, v1

    .line 231
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimChars(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 601
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 604
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 614
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 607
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v3, v5, :cond_2

    if-nez v4, :cond_1

    .line 611
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 608
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_3

    const/4 v4, 0x1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static trimCharsBeginEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 632
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 635
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 638
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_4

    if-nez v4, :cond_0

    goto :goto_2

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 641
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_2

    if-nez v7, :cond_1

    move v5, v3

    const/4 v4, 0x0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 642
    :cond_2
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_3

    const/4 v7, 0x1

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    move v4, v0

    const/4 v3, 0x1

    :goto_3
    if-ltz v0, :cond_9

    if-nez v3, :cond_5

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 654
    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_7

    if-nez v7, :cond_6

    move v4, v0

    const/4 v3, 0x0

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 655
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_8

    const/4 v7, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_9
    :goto_5
    add-int/2addr v4, v1

    .line 663
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimSpaces(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 313
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 316
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 326
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 319
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v3, v5, :cond_2

    .line 322
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 323
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 320
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_3

    const/4 v4, 0x1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static trimSpacesBeginEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 345
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 348
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 351
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_4

    if-nez v4, :cond_0

    goto :goto_2

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 354
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_2

    .line 357
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v6

    if-nez v6, :cond_1

    if-nez v7, :cond_1

    move v5, v3

    const/4 v4, 0x0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 355
    :cond_2
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_3

    const/4 v7, 0x1

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    move v4, v0

    const/4 v3, 0x1

    :goto_3
    if-ltz v0, :cond_9

    if-nez v3, :cond_5

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 367
    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v6, v8, :cond_7

    .line 370
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v6

    if-nez v6, :cond_6

    if-nez v7, :cond_6

    move v4, v0

    const/4 v3, 0x0

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 368
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_8

    const/4 v7, 0x1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_9
    :goto_5
    add-int/2addr v4, v1

    .line 376
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimTags(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1039
    new-instance v0, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v0, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    const/4 p1, 0x1

    invoke-static {p0, v0, p1, p1}, Lorg/htmlparser/util/ParserUtils;->trimTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimTags(Ljava/lang/String;Ljava/lang/Class;ZZ)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1053
    new-instance v0, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v0, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    invoke-static {p0, v0, p2, p3}, Lorg/htmlparser/util/ParserUtils;->trimTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 1067
    invoke-static {p0, p1, v0, v0}, Lorg/htmlparser/util/ParserUtils;->trimTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimTags(Ljava/lang/String;Lorg/htmlparser/NodeFilter;ZZ)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1082
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1084
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    invoke-static {v2, v1}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v1

    .line 1087
    invoke-static {p0, p1, p2}, Lorg/htmlparser/util/ParserUtils;->getLinks(Ljava/lang/String;Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    const/4 p2, 0x0

    .line 1088
    :goto_0
    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt p2, v3, :cond_3

    .line 1109
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p2

    if-ge p1, p2, :cond_2

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    goto :goto_2

    :cond_0
    const/16 p3, 0x2a

    .line 1111
    invoke-virtual {v1, p3, p1}, Ljava/lang/String;->indexOf(II)I

    move-result p3

    if-eq p3, p2, :cond_1

    .line 1114
    invoke-virtual {p0, p1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1115
    invoke-virtual {v1, v2, p3}, Ljava/lang/String;->indexOf(II)I

    move-result p1

    goto :goto_1

    .line 1119
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move p1, p3

    goto :goto_1

    .line 1125
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 1090
    :cond_3
    invoke-virtual {p1, p2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/CompositeTag;

    .line 1091
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v4

    .line 1094
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v5

    .line 1095
    invoke-virtual {v3}, Lorg/htmlparser/tags/CompositeTag;->getEndPosition()I

    move-result v3

    .line 1096
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v6

    .line 1097
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v4

    if-eqz p3, :cond_4

    .line 1101
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v5, v4}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 1105
    :cond_4
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v5, v3}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    .line 1106
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v6, v4}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method public static trimTags(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 948
    invoke-static {p0, p1, v0, v0}, Lorg/htmlparser/util/ParserUtils;->trimTags(Ljava/lang/String;[Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static trimTags(Ljava/lang/String;[Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 973
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 974
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 975
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    const/16 v2, 0x20

    invoke-static {v2, p0}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    move-object v4, p0

    const/4 p0, 0x0

    .line 978
    :goto_0
    array-length v5, p1

    if-lt p0, v5, :cond_0

    .line 1024
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 980
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 983
    aget-object v5, p1, p0

    invoke-static {v1, v5, p2}, Lorg/htmlparser/util/ParserUtils;->getLinks(Ljava/lang/String;Ljava/lang/String;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v5

    move-object v6, v4

    const/4 v4, 0x0

    .line 984
    :goto_1
    invoke-virtual {v5}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v7

    if-lt v4, v7, :cond_4

    .line 1006
    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    goto :goto_3

    :cond_1
    const/16 v7, 0x2a

    .line 1008
    invoke-virtual {v6, v7, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    if-eq v7, v5, :cond_2

    .line 1011
    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1012
    invoke-virtual {v6, v2, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    goto :goto_2

    .line 1016
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v4, v7

    goto :goto_2

    .line 1020
    :cond_3
    :goto_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    .line 1021
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v2, v4}, Lorg/htmlparser/util/ParserUtils;->createDummyString(CI)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 986
    :cond_4
    invoke-virtual {v5, v4}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v7

    check-cast v7, Lorg/htmlparser/tags/CompositeTag;

    .line 987
    invoke-virtual {v7}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v8

    .line 990
    invoke-virtual {v7}, Lorg/htmlparser/tags/CompositeTag;->getStartPosition()I

    move-result v9

    .line 991
    invoke-virtual {v7}, Lorg/htmlparser/tags/CompositeTag;->getEndPosition()I

    move-result v7

    .line 992
    invoke-interface {v8}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v10

    .line 993
    invoke-interface {v8}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v8

    if-eqz p3, :cond_5

    .line 998
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v9, v8}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 1002
    :cond_5
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v9, v7}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    .line 1003
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v10, v8}, Lorg/htmlparser/util/ParserUtils;->modifyDummyString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
