.class public Lorg/htmlparser/util/IteratorImpl;
.super Ljava/lang/Object;
.source "IteratorImpl.java"

# interfaces
.implements Lorg/htmlparser/util/NodeIterator;


# instance fields
.field mCursor:Lorg/htmlparser/lexer/Cursor;

.field mFeedback:Lorg/htmlparser/util/ParserFeedback;

.field mLexer:Lorg/htmlparser/lexer/Lexer;


# direct methods
.method public constructor <init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    .line 45
    iput-object p2, p0, Lorg/htmlparser/util/IteratorImpl;->mFeedback:Lorg/htmlparser/util/ParserFeedback;

    .line 46
    new-instance p1, Lorg/htmlparser/lexer/Cursor;

    iget-object p2, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object p2

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    iput-object p1, p0, Lorg/htmlparser/util/IteratorImpl;->mCursor:Lorg/htmlparser/lexer/Cursor;

    return-void
.end method


# virtual methods
.method public hasMoreNodes()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lorg/htmlparser/util/IteratorImpl;->mCursor:Lorg/htmlparser/lexer/Cursor;

    iget-object v1, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Lexer;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Cursor;->setPosition(I)V

    .line 58
    iget-object v0, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    iget-object v1, p0, Lorg/htmlparser/util/IteratorImpl;->mCursor:Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v0

    const v1, 0xffff

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public nextNode()Lorg/htmlparser/Node;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 77
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->nextNode()Lorg/htmlparser/Node;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    instance-of v1, v0, Lorg/htmlparser/Tag;

    if-eqz v1, :cond_0

    .line 83
    move-object v1, v0

    check-cast v1, Lorg/htmlparser/Tag;

    .line 84
    invoke-interface {v1}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    invoke-interface {v1}, Lorg/htmlparser/Tag;->getThisScanner()Lorg/htmlparser/scanners/Scanner;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 90
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 91
    iget-object v3, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-interface {v2, v1, v3, v0}, Lorg/htmlparser/scanners/Scanner;->scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;

    move-result-object v0
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Unexpected Exception occurred while reading "

    .line 104
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    iget-object v2, p0, Lorg/htmlparser/util/IteratorImpl;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {v2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v2

    invoke-virtual {v2}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, ", in nextNode"

    .line 106
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    new-instance v2, Lorg/htmlparser/util/ParserException;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 109
    iget-object v0, p0, Lorg/htmlparser/util/IteratorImpl;->mFeedback:Lorg/htmlparser/util/ParserFeedback;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lorg/htmlparser/util/ParserFeedback;->error(Ljava/lang/String;Lorg/htmlparser/util/ParserException;)V

    .line 110
    throw v2

    :catch_1
    move-exception v0

    .line 99
    throw v0
.end method
