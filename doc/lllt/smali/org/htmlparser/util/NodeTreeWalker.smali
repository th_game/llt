.class public Lorg/htmlparser/util/NodeTreeWalker;
.super Ljava/lang/Object;
.source "NodeTreeWalker.java"

# interfaces
.implements Lorg/htmlparser/util/NodeIterator;


# instance fields
.field protected mCurrentNode:Lorg/htmlparser/Node;

.field protected mDepthFirst:Z

.field protected mMaxDepth:I

.field protected mNextNode:Lorg/htmlparser/Node;

.field protected mRootNode:Lorg/htmlparser/Node;


# direct methods
.method public constructor <init>(Lorg/htmlparser/Node;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 84
    invoke-direct {p0, p1, v0, v1}, Lorg/htmlparser/util/NodeTreeWalker;-><init>(Lorg/htmlparser/Node;ZI)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/Node;Z)V
    .locals 1

    const/4 v0, -0x1

    .line 95
    invoke-direct {p0, p1, p2, v0}, Lorg/htmlparser/util/NodeTreeWalker;-><init>(Lorg/htmlparser/Node;ZI)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/Node;ZI)V
    .locals 1

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-ge p3, v0, :cond_1

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Paramater maxDepth must be > 0 or equal to -1."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 111
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeTreeWalker;->initRootNode(Lorg/htmlparser/Node;)V

    .line 112
    iput-boolean p2, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    .line 113
    iput p3, p0, Lorg/htmlparser/util/NodeTreeWalker;->mMaxDepth:I

    return-void
.end method


# virtual methods
.method public getCurrentNode()Lorg/htmlparser/Node;
    .locals 1

    .line 168
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    return-object v0
.end method

.method public getCurrentNodeDepth()I
    .locals 3

    .line 240
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 243
    :goto_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    if-ne v0, v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 246
    invoke-interface {v0}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    goto :goto_0

    :cond_1
    :goto_1
    return v1
.end method

.method public getMaxDepth()I
    .locals 1

    .line 142
    iget v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mMaxDepth:I

    return v0
.end method

.method protected getNextNodeBreadthFirst()Lorg/htmlparser/Node;
    .locals 6

    .line 322
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    invoke-interface {v0}, Lorg/htmlparser/Node;->getNextSibling()Lorg/htmlparser/Node;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 326
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getCurrentNodeDepth()I

    move-result v0

    .line 333
    iget-object v1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    invoke-interface {v1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    :goto_0
    if-gtz v2, :cond_1

    goto :goto_3

    .line 340
    :cond_1
    :goto_1
    invoke-interface {v1}, Lorg/htmlparser/Node;->getNextSibling()Lorg/htmlparser/Node;

    move-result-object v3

    if-nez v3, :cond_3

    iget-object v4, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    if-ne v1, v4, :cond_2

    goto :goto_2

    .line 342
    :cond_2
    invoke-interface {v1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v1

    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 347
    :cond_3
    :goto_2
    iget-object v4, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    if-ne v1, v4, :cond_c

    .line 371
    :goto_3
    iget v1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mMaxDepth:I

    const/4 v4, 0x0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    if-lt v0, v1, :cond_4

    return-object v4

    .line 377
    :cond_4
    iget-object v1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    invoke-interface {v1}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v1

    const/4 v2, 0x1

    add-int/lit8 v5, v0, 0x1

    :cond_5
    if-gtz v2, :cond_6

    return-object v4

    .line 383
    :cond_6
    invoke-interface {v1}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_9

    .line 384
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    if-nez v0, :cond_7

    goto :goto_5

    .line 386
    :cond_7
    invoke-interface {v1}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_8

    return-object v1

    .line 391
    :cond_8
    invoke-interface {v1}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    goto :goto_4

    .line 395
    :cond_9
    :goto_5
    invoke-interface {v1}, Lorg/htmlparser/Node;->getNextSibling()Lorg/htmlparser/Node;

    move-result-object v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    if-ne v1, v0, :cond_a

    goto :goto_6

    .line 397
    :cond_a
    invoke-interface {v1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v1

    add-int/lit8 v2, v2, -0x1

    goto :goto_5

    .line 400
    :cond_b
    :goto_6
    invoke-interface {v1}, Lorg/htmlparser/Node;->getNextSibling()Lorg/htmlparser/Node;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v4

    :cond_c
    if-eqz v3, :cond_f

    .line 355
    invoke-interface {v3}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    :goto_7
    if-eqz v1, :cond_f

    .line 356
    invoke-virtual {v1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    if-nez v1, :cond_d

    goto :goto_8

    .line 358
    :cond_d
    invoke-interface {v3}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    return-object v3

    .line 363
    :cond_e
    invoke-interface {v3}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    goto :goto_7

    :cond_f
    :goto_8
    move-object v1, v3

    goto/16 :goto_0
.end method

.method protected getNextNodeDepthFirst()Lorg/htmlparser/Node;
    .locals 3

    .line 295
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getCurrentNodeDepth()I

    move-result v0

    .line 297
    iget v1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mMaxDepth:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-ge v0, v1, :cond_1

    .line 299
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    invoke-interface {v0}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 304
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    const/4 v1, 0x0

    .line 307
    :goto_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    if-eq v0, v2, :cond_3

    invoke-interface {v0}, Lorg/htmlparser/Node;->getNextSibling()Lorg/htmlparser/Node;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_1

    .line 308
    :cond_2
    invoke-interface {v0}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    goto :goto_0

    :cond_3
    :goto_1
    return-object v1
.end method

.method public getRootNode()Lorg/htmlparser/Node;
    .locals 1

    .line 159
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    return-object v0
.end method

.method public hasMoreNodes()Z
    .locals 1

    .line 258
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    if-nez v0, :cond_2

    .line 260
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    invoke-interface {v0}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    goto :goto_0

    .line 264
    :cond_0
    iget-boolean v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    if-eqz v0, :cond_1

    .line 265
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getNextNodeDepthFirst()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    goto :goto_0

    .line 267
    :cond_1
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getNextNodeBreadthFirst()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    .line 270
    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method protected initRootNode(Lorg/htmlparser/Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 283
    iput-object p1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    const/4 p1, 0x0

    .line 284
    iput-object p1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    .line 285
    iput-object p1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    return-void

    .line 282
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Root Node cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isDepthFirst()Z
    .locals 1

    .line 122
    iget-boolean v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    return v0
.end method

.method public nextNode()Lorg/htmlparser/Node;
    .locals 1

    .line 210
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    if-eqz v0, :cond_0

    .line 212
    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    const/4 v0, 0x0

    .line 213
    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    goto :goto_0

    .line 218
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    if-nez v0, :cond_1

    .line 219
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mRootNode:Lorg/htmlparser/Node;

    invoke-interface {v0}, Lorg/htmlparser/Node;->getFirstChild()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    goto :goto_0

    .line 222
    :cond_1
    iget-boolean v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    if-eqz v0, :cond_2

    .line 223
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getNextNodeDepthFirst()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    goto :goto_0

    .line 225
    :cond_2
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeTreeWalker;->getNextNodeBreadthFirst()Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    .line 228
    :goto_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    return-object v0
.end method

.method public removeMaxDepthRestriction()V
    .locals 1

    const/4 v0, -0x1

    .line 150
    iput v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mMaxDepth:I

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 200
    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    .line 201
    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    return-void
.end method

.method public setCurrentNodeAsRootNode()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mCurrentNode:Lorg/htmlparser/Node;

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {p0, v0}, Lorg/htmlparser/util/NodeTreeWalker;->initRootNode(Lorg/htmlparser/Node;)V

    return-void

    .line 179
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Current Node is null, cannot set as root Node."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDepthFirst(Z)V
    .locals 1

    .line 131
    iget-boolean v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    .line 132
    iput-object v0, p0, Lorg/htmlparser/util/NodeTreeWalker;->mNextNode:Lorg/htmlparser/Node;

    .line 133
    :cond_0
    iput-boolean p1, p0, Lorg/htmlparser/util/NodeTreeWalker;->mDepthFirst:Z

    return-void
.end method

.method public setRootNode(Lorg/htmlparser/Node;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .line 191
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeTreeWalker;->initRootNode(Lorg/htmlparser/Node;)V

    return-void
.end method
