.class public Lorg/htmlparser/util/DefaultParserFeedback;
.super Ljava/lang/Object;
.source "DefaultParserFeedback.java"

# interfaces
.implements Lorg/htmlparser/util/ParserFeedback;
.implements Ljava/io/Serializable;


# static fields
.field public static final DEBUG:I = 0x2

.field public static final NORMAL:I = 0x1

.field public static final QUIET:I


# instance fields
.field protected mMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    .line 95
    invoke-direct {p0, v0}, Lorg/htmlparser/util/DefaultParserFeedback;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    .line 87
    iput p1, p0, Lorg/htmlparser/util/DefaultParserFeedback;->mMode:I

    return-void

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal mode ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "), must be one of: QUIET, NORMAL, DEBUG"

    .line 86
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 83
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public error(Ljava/lang/String;Lorg/htmlparser/util/ParserException;)V
    .locals 3

    .line 125
    iget v0, p0, Lorg/htmlparser/util/DefaultParserFeedback;->mMode:I

    if-eqz v0, :cond_0

    .line 127
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 p1, 0x2

    .line 128
    iget v0, p0, Lorg/htmlparser/util/DefaultParserFeedback;->mMode:I

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 129
    invoke-virtual {p2}, Lorg/htmlparser/util/ParserException;->printStackTrace()V

    :cond_0
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 3

    .line 104
    iget v0, p0, Lorg/htmlparser/util/DefaultParserFeedback;->mMode:I

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INFO: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public warning(Ljava/lang/String;)V
    .locals 3

    .line 114
    iget v0, p0, Lorg/htmlparser/util/DefaultParserFeedback;->mMode:I

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WARNING: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
