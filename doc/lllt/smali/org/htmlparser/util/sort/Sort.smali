.class public Lorg/htmlparser/util/sort/Sort;
.super Ljava/lang/Object;
.source "Sort.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static QuickSort(Ljava/util/Vector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    .line 65
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Ljava/util/Vector;II)V

    return-void
.end method

.method public static QuickSort(Ljava/util/Vector;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    if-le p2, p1, :cond_7

    add-int v0, p1, p2

    .line 91
    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/util/sort/Ordered;

    move v1, p1

    move v2, p2

    :cond_0
    :goto_0
    if-le v1, v2, :cond_2

    if-ge p1, v2, :cond_1

    .line 114
    invoke-static {p0, p1, v2}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Ljava/util/Vector;II)V

    :cond_1
    if-ge v1, p2, :cond_7

    .line 119
    invoke-static {p0, v1, p2}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Ljava/util/Vector;II)V

    goto :goto_4

    :cond_2
    :goto_1
    if-ge v1, p2, :cond_4

    .line 98
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/util/sort/Ordered;

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v3

    if-ltz v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-le v2, p1, :cond_6

    .line 103
    invoke-virtual {p0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/util/sort/Ordered;

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v3

    if-gtz v3, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_6
    :goto_3
    if-gt v1, v2, :cond_0

    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v4, v2, -0x1

    .line 108
    invoke-static {p0, v1, v2}, Lorg/htmlparser/util/sort/Sort;->swap(Ljava/util/Vector;II)V

    move v1, v3

    move v2, v4

    goto :goto_0

    :cond_7
    :goto_4
    return-void
.end method

.method public static QuickSort(Lorg/htmlparser/util/sort/Sortable;)V
    .locals 2

    .line 350
    invoke-interface {p0}, Lorg/htmlparser/util/sort/Sortable;->first()I

    move-result v0

    invoke-interface {p0}, Lorg/htmlparser/util/sort/Sortable;->last()I

    move-result v1

    invoke-static {p0, v0, v1}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Lorg/htmlparser/util/sort/Sortable;II)V

    return-void
.end method

.method public static QuickSort(Lorg/htmlparser/util/sort/Sortable;II)V
    .locals 6

    if-le p2, p1, :cond_7

    add-int v0, p1, p2

    .line 304
    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Lorg/htmlparser/util/sort/Sortable;->fetch(ILorg/htmlparser/util/sort/Ordered;)Lorg/htmlparser/util/sort/Ordered;

    move-result-object v0

    move v2, p2

    move-object v3, v1

    move v1, p1

    :cond_0
    :goto_0
    if-le v1, v2, :cond_2

    if-ge p1, v2, :cond_1

    .line 328
    invoke-static {p0, p1, v2}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Lorg/htmlparser/util/sort/Sortable;II)V

    :cond_1
    if-ge v1, p2, :cond_7

    .line 333
    invoke-static {p0, v1, p2}, Lorg/htmlparser/util/sort/Sort;->QuickSort(Lorg/htmlparser/util/sort/Sortable;II)V

    goto :goto_4

    :cond_2
    :goto_1
    if-ge v1, p2, :cond_4

    .line 312
    invoke-interface {p0, v1, v3}, Lorg/htmlparser/util/sort/Sortable;->fetch(ILorg/htmlparser/util/sort/Ordered;)Lorg/htmlparser/util/sort/Ordered;

    move-result-object v3

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v4

    if-ltz v4, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-le v2, p1, :cond_6

    .line 317
    invoke-interface {p0, v2, v3}, Lorg/htmlparser/util/sort/Sortable;->fetch(ILorg/htmlparser/util/sort/Ordered;)Lorg/htmlparser/util/sort/Ordered;

    move-result-object v3

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v4

    if-gtz v4, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_6
    :goto_3
    if-gt v1, v2, :cond_0

    add-int/lit8 v4, v1, 0x1

    add-int/lit8 v5, v2, -0x1

    .line 322
    invoke-interface {p0, v1, v2}, Lorg/htmlparser/util/sort/Sortable;->swap(II)V

    move v1, v4

    move v2, v5

    goto :goto_0

    :cond_7
    :goto_4
    return-void
.end method

.method public static QuickSort([Ljava/lang/String;)V
    .locals 2

    .line 229
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Ljava/lang/String;II)V

    return-void
.end method

.method public static QuickSort([Ljava/lang/String;II)V
    .locals 5

    if-le p2, p1, :cond_7

    add-int v0, p1, p2

    .line 254
    div-int/lit8 v0, v0, 0x2

    aget-object v0, p0, v0

    move v1, p1

    move v2, p2

    :cond_0
    :goto_0
    if-le v1, v2, :cond_2

    if-ge p1, v2, :cond_1

    .line 277
    invoke-static {p0, p1, v2}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Ljava/lang/String;II)V

    :cond_1
    if-ge v1, p2, :cond_7

    .line 282
    invoke-static {p0, v1, p2}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Ljava/lang/String;II)V

    goto :goto_4

    :cond_2
    :goto_1
    if-ge v1, p2, :cond_4

    .line 261
    aget-object v3, p0, v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-le v2, p1, :cond_6

    .line 266
    aget-object v3, p0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_6
    :goto_3
    if-gt v1, v2, :cond_0

    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v4, v2, -0x1

    .line 271
    invoke-static {p0, v1, v2}, Lorg/htmlparser/util/sort/Sort;->swap([Ljava/lang/Object;II)V

    move v1, v3

    move v2, v4

    goto :goto_0

    :cond_7
    :goto_4
    return-void
.end method

.method public static QuickSort([Lorg/htmlparser/util/sort/Ordered;)V
    .locals 2

    .line 145
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Lorg/htmlparser/util/sort/Ordered;II)V

    return-void
.end method

.method public static QuickSort([Lorg/htmlparser/util/sort/Ordered;II)V
    .locals 5

    if-le p2, p1, :cond_7

    add-int v0, p1, p2

    .line 170
    div-int/lit8 v0, v0, 0x2

    aget-object v0, p0, v0

    move v1, p1

    move v2, p2

    :cond_0
    :goto_0
    if-le v1, v2, :cond_2

    if-ge p1, v2, :cond_1

    .line 193
    invoke-static {p0, p1, v2}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Lorg/htmlparser/util/sort/Ordered;II)V

    :cond_1
    if-ge v1, p2, :cond_7

    .line 198
    invoke-static {p0, v1, p2}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Lorg/htmlparser/util/sort/Ordered;II)V

    goto :goto_4

    :cond_2
    :goto_1
    if-ge v1, p2, :cond_4

    .line 177
    aget-object v3, p0, v1

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v3

    if-ltz v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-le v2, p1, :cond_6

    .line 182
    aget-object v3, p0, v2

    invoke-interface {v3, v0}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v3

    if-gtz v3, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_6
    :goto_3
    if-gt v1, v2, :cond_0

    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v4, v2, -0x1

    .line 187
    invoke-static {p0, v1, v2}, Lorg/htmlparser/util/sort/Sort;->swap([Ljava/lang/Object;II)V

    move v1, v3

    move v2, v4

    goto :goto_0

    :cond_7
    :goto_4
    return-void
.end method

.method public static QuickSort(Ljava/util/Hashtable;)[Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    .line 367
    invoke-virtual {p0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v0, v0, [Lorg/htmlparser/util/sort/Ordered;

    .line 368
    invoke-virtual {p0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 370
    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_1

    if-eqz v3, :cond_0

    .line 379
    move-object p0, v0

    check-cast p0, [Ljava/lang/String;

    invoke-static {p0}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Ljava/lang/String;)V

    goto :goto_1

    .line 381
    :cond_0
    move-object p0, v0

    check-cast p0, [Lorg/htmlparser/util/sort/Ordered;

    invoke-static {p0}, Lorg/htmlparser/util/sort/Sort;->QuickSort([Lorg/htmlparser/util/sort/Ordered;)V

    :goto_1
    return-object v0

    .line 372
    :cond_1
    invoke-interface {p0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v0, v2

    if-eqz v3, :cond_2

    .line 373
    aget-object v4, v0, v2

    instance-of v4, v4, Ljava/lang/String;

    if-nez v4, :cond_2

    const/4 v3, 0x0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static bsearch(Ljava/util/Vector;Lorg/htmlparser/util/sort/Ordered;)I
    .locals 2

    .line 492
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Lorg/htmlparser/util/sort/Sort;->bsearch(Ljava/util/Vector;Lorg/htmlparser/util/sort/Ordered;II)I

    move-result p0

    return p0
.end method

.method public static bsearch(Ljava/util/Vector;Lorg/htmlparser/util/sort/Ordered;II)I
    .locals 7

    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    const/4 v1, -0x1

    move v2, v0

    move v0, p3

    move p3, p2

    const/4 p2, -0x1

    :goto_0
    if-ne v1, p2, :cond_5

    if-le p3, v0, :cond_0

    goto :goto_3

    .line 462
    :cond_0
    div-int/lit8 v3, v2, 0x2

    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v5, v3, -0x1

    :goto_1
    add-int/2addr v5, p3

    .line 464
    invoke-virtual {p0, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {p1, v6}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v6

    if-nez v6, :cond_2

    move p2, v5

    goto :goto_0

    :cond_2
    if-gez v6, :cond_4

    add-int/lit8 v0, v5, -0x1

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_4
    add-int/lit8 p3, v5, 0x1

    :goto_2
    move v2, v3

    goto :goto_0

    :cond_5
    :goto_3
    if-ne v1, p2, :cond_6

    move p2, p3

    :cond_6
    return p2
.end method

.method public static bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;)I
    .locals 2

    .line 439
    invoke-interface {p0}, Lorg/htmlparser/util/sort/Sortable;->first()I

    move-result v0

    invoke-interface {p0}, Lorg/htmlparser/util/sort/Sortable;->last()I

    move-result v1

    invoke-static {p0, p1, v0, v1}, Lorg/htmlparser/util/sort/Sort;->bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;II)I

    move-result p0

    return p0
.end method

.method public static bsearch(Lorg/htmlparser/util/sort/Sortable;Lorg/htmlparser/util/sort/Ordered;II)I
    .locals 8

    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    move-object v3, v2

    move v2, v0

    move v0, p3

    move p3, p2

    const/4 p2, -0x1

    :goto_0
    if-ne v1, p2, :cond_5

    if-le p3, v0, :cond_0

    goto :goto_3

    .line 408
    :cond_0
    div-int/lit8 v4, v2, 0x2

    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_1

    move v6, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v6, v4, -0x1

    :goto_1
    add-int/2addr v6, p3

    .line 410
    invoke-interface {p0, v6, v3}, Lorg/htmlparser/util/sort/Sortable;->fetch(ILorg/htmlparser/util/sort/Ordered;)Lorg/htmlparser/util/sort/Ordered;

    move-result-object v3

    .line 411
    invoke-interface {p1, v3}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v7

    if-nez v7, :cond_2

    move p2, v6

    goto :goto_0

    :cond_2
    if-gez v7, :cond_4

    add-int/lit8 v0, v6, -0x1

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    :cond_4
    add-int/lit8 p3, v6, 0x1

    :goto_2
    move v2, v4

    goto :goto_0

    :cond_5
    :goto_3
    if-ne v1, p2, :cond_6

    move p2, p3

    :cond_6
    return p2
.end method

.method public static bsearch([Lorg/htmlparser/util/sort/Ordered;Lorg/htmlparser/util/sort/Ordered;)I
    .locals 2

    .line 545
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Lorg/htmlparser/util/sort/Sort;->bsearch([Lorg/htmlparser/util/sort/Ordered;Lorg/htmlparser/util/sort/Ordered;II)I

    move-result p0

    return p0
.end method

.method public static bsearch([Lorg/htmlparser/util/sort/Ordered;Lorg/htmlparser/util/sort/Ordered;II)I
    .locals 7

    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    const/4 v1, -0x1

    move v2, v0

    move v0, p3

    move p3, p2

    const/4 p2, -0x1

    :goto_0
    if-ne v1, p2, :cond_5

    if-le p3, v0, :cond_0

    goto :goto_3

    .line 515
    :cond_0
    div-int/lit8 v3, v2, 0x2

    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v5, v3, -0x1

    :goto_1
    add-int/2addr v5, p3

    .line 517
    aget-object v6, p0, v5

    invoke-interface {p1, v6}, Lorg/htmlparser/util/sort/Ordered;->compare(Ljava/lang/Object;)I

    move-result v6

    if-nez v6, :cond_2

    move p2, v5

    goto :goto_0

    :cond_2
    if-gez v6, :cond_4

    add-int/lit8 v0, v5, -0x1

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_4
    add-int/lit8 p3, v5, 0x1

    :goto_2
    move v2, v3

    goto :goto_0

    :cond_5
    :goto_3
    if-ne v1, p2, :cond_6

    move p2, p3

    :cond_6
    return p2
.end method

.method private static swap(Ljava/util/Vector;II)V
    .locals 2

    .line 127
    invoke-virtual {p0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 128
    invoke-virtual {p0, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 129
    invoke-virtual {p0, v0, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    return-void
.end method

.method private static swap([Ljava/lang/Object;II)V
    .locals 2

    .line 211
    aget-object v0, p0, p1

    .line 212
    aget-object v1, p0, p2

    aput-object v1, p0, p1

    .line 213
    aput-object v0, p0, p2

    return-void
.end method
