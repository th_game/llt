.class Lorg/htmlparser/util/NodeList$1;
.super Ljava/lang/Object;
.source "NodeList.java"

# interfaces
.implements Lorg/htmlparser/util/SimpleNodeIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lorg/htmlparser/util/NodeList;


# direct methods
.method constructor <init>(Lorg/htmlparser/util/NodeList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lorg/htmlparser/util/NodeList$1;->this$0:Lorg/htmlparser/util/NodeList;

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 117
    iput p1, p0, Lorg/htmlparser/util/NodeList$1;->count:I

    return-void
.end method


# virtual methods
.method public hasMoreNodes()Z
    .locals 2

    .line 121
    iget v0, p0, Lorg/htmlparser/util/NodeList$1;->count:I

    iget-object v1, p0, Lorg/htmlparser/util/NodeList$1;->this$0:Lorg/htmlparser/util/NodeList;

    invoke-static {v1}, Lorg/htmlparser/util/NodeList;->access$0(Lorg/htmlparser/util/NodeList;)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public nextNode()Lorg/htmlparser/Node;
    .locals 4

    .line 126
    iget-object v0, p0, Lorg/htmlparser/util/NodeList$1;->this$0:Lorg/htmlparser/util/NodeList;

    monitor-enter v0

    .line 128
    :try_start_0
    iget v1, p0, Lorg/htmlparser/util/NodeList$1;->count:I

    iget-object v2, p0, Lorg/htmlparser/util/NodeList$1;->this$0:Lorg/htmlparser/util/NodeList;

    invoke-static {v2}, Lorg/htmlparser/util/NodeList;->access$0(Lorg/htmlparser/util/NodeList;)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 130
    iget-object v1, p0, Lorg/htmlparser/util/NodeList$1;->this$0:Lorg/htmlparser/util/NodeList;

    invoke-static {v1}, Lorg/htmlparser/util/NodeList;->access$1(Lorg/htmlparser/util/NodeList;)[Lorg/htmlparser/Node;

    move-result-object v1

    iget v2, p0, Lorg/htmlparser/util/NodeList$1;->count:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/htmlparser/util/NodeList$1;->count:I

    aget-object v1, v1, v2

    monitor-exit v0

    return-object v1

    .line 126
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Vector Enumeration"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v1

    .line 126
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
