.class public Lorg/htmlparser/util/NodeList;
.super Ljava/lang/Object;
.source "NodeList.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final INITIAL_CAPACITY:I = 0xa


# instance fields
.field private capacity:I

.field private capacityIncrement:I

.field private nodeData:[Lorg/htmlparser/Node;

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p0}, Lorg/htmlparser/util/NodeList;->removeAll()V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/Node;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 56
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    return-void
.end method

.method static synthetic access$0(Lorg/htmlparser/util/NodeList;)I
    .locals 0

    .line 40
    iget p0, p0, Lorg/htmlparser/util/NodeList;->size:I

    return p0
.end method

.method static synthetic access$1(Lorg/htmlparser/util/NodeList;)[Lorg/htmlparser/Node;
    .locals 0

    .line 39
    iget-object p0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    return-object p0
.end method

.method private adjustVectorCapacity()V
    .locals 4

    .line 91
    iget v0, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    iget v1, p0, Lorg/htmlparser/util/NodeList;->capacityIncrement:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    mul-int/lit8 v1, v1, 0x2

    .line 92
    iput v1, p0, Lorg/htmlparser/util/NodeList;->capacityIncrement:I

    .line 93
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    .line 94
    iget v1, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    invoke-direct {p0, v1}, Lorg/htmlparser/util/NodeList;->newNodeArrayFor(I)[Lorg/htmlparser/Node;

    move-result-object v1

    iput-object v1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    .line 95
    iget-object v1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method private newNodeArrayFor(I)[Lorg/htmlparser/Node;
    .locals 0

    .line 100
    new-array p1, p1, [Lorg/htmlparser/Node;

    return-object p1
.end method


# virtual methods
.method public add(Lorg/htmlparser/Node;)V
    .locals 3

    .line 61
    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    iget v1, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    if-ne v0, v1, :cond_0

    .line 62
    invoke-direct {p0}, Lorg/htmlparser/util/NodeList;->adjustVectorCapacity()V

    .line 63
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v1, p0, Lorg/htmlparser/util/NodeList;->size:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    aput-object p1, v0, v1

    return-void
.end method

.method public add(Lorg/htmlparser/util/NodeList;)V
    .locals 2

    const/4 v0, 0x0

    .line 72
    :goto_0
    iget v1, p1, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v0, v1, :cond_0

    return-void

    .line 73
    :cond_0
    iget-object v1, p1, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public asString()Ljava/lang/String;
    .locals 3

    .line 152
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 153
    :goto_0
    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v1, v2, :cond_0

    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 154
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v2, v2, v1

    invoke-interface {v2}, Lorg/htmlparser/Node;->toPlainTextString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public contains(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 216
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeList;->indexOf(Lorg/htmlparser/Node;)I

    move-result p1

    const/4 v0, -0x1

    if-eq v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public copyToNodeArray([Lorg/htmlparser/Node;)V
    .locals 3

    .line 147
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v1, p0, Lorg/htmlparser/util/NodeList;->size:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public elementAt(I)Lorg/htmlparser/Node;
    .locals 1

    .line 110
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public elements()Lorg/htmlparser/util/SimpleNodeIterator;
    .locals 1

    .line 115
    new-instance v0, Lorg/htmlparser/util/NodeList$1;

    invoke-direct {v0, p0}, Lorg/htmlparser/util/NodeList$1;-><init>(Lorg/htmlparser/util/NodeList;)V

    return-object v0
.end method

.method public extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;
    .locals 1

    const/4 v0, 0x0

    .line 281
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    return-object p1
.end method

.method public extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;
    .locals 4

    .line 298
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    const/4 v1, 0x0

    .line 299
    :goto_0
    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v1, v2, :cond_0

    return-object v0

    .line 301
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v2, v2, v1

    .line 302
    invoke-interface {p1, v2}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 303
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 306
    invoke-interface {v2}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 308
    invoke-virtual {v2, p1, p2}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/util/NodeList;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public indexOf(Lorg/htmlparser/Node;)I
    .locals 4

    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 229
    :goto_0
    iget v3, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-ge v1, v3, :cond_2

    if-eq v0, v2, :cond_0

    goto :goto_1

    .line 230
    :cond_0
    iget-object v3, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v2
.end method

.method public keepAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)V
    .locals 1

    const/4 v0, 0x0

    .line 321
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/util/NodeList;->keepAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)V

    return-void
.end method

.method public keepAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)V
    .locals 3

    const/4 v0, 0x0

    .line 334
    :goto_0
    iget v1, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v0, v1, :cond_0

    return-void

    .line 336
    :cond_0
    iget-object v1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v1, v1, v0

    .line 337
    invoke-interface {p1, v1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 338
    invoke-virtual {p0, v0}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 343
    invoke-interface {v1}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 345
    invoke-virtual {v1, p1, p2}, Lorg/htmlparser/util/NodeList;->keepAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public prepend(Lorg/htmlparser/Node;)V
    .locals 4

    .line 82
    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    iget v1, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    if-ne v0, v1, :cond_0

    .line 83
    invoke-direct {p0}, Lorg/htmlparser/util/NodeList;->adjustVectorCapacity()V

    .line 84
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v1, p0, Lorg/htmlparser/util/NodeList;->size:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v3, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    add-int/2addr v0, v2

    iput v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    .line 86
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aput-object p1, v0, v3

    return-void
.end method

.method public remove(I)Lorg/htmlparser/Node;
    .locals 4

    .line 193
    iget-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v1, v0, p1

    add-int/lit8 v2, p1, 0x1

    .line 194
    iget v3, p0, Lorg/htmlparser/util/NodeList;->size:I

    sub-int/2addr v3, p1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v0, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    iget-object p1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    add-int/lit8 v2, v0, -0x1

    const/4 v3, 0x0

    aput-object v3, p1, v2

    add-int/lit8 v0, v0, -0x1

    .line 196
    iput v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    return-object v1
.end method

.method public remove(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 247
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeList;->indexOf(Lorg/htmlparser/Node;)I

    move-result p1

    const/4 v0, -0x1

    if-eq v0, p1, :cond_0

    .line 249
    invoke-virtual {p0, p1}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public removeAll()V
    .locals 1

    const/4 v0, 0x0

    .line 203
    iput v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    const/16 v0, 0xa

    .line 204
    iput v0, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    .line 205
    iget v0, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    invoke-direct {p0, v0}, Lorg/htmlparser/util/NodeList;->newNodeArrayFor(I)[Lorg/htmlparser/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    .line 206
    iget v0, p0, Lorg/htmlparser/util/NodeList;->capacity:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/htmlparser/util/NodeList;->capacityIncrement:I

    return-void
.end method

.method public size()I
    .locals 1

    .line 105
    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    return v0
.end method

.method public toHtml()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 181
    invoke-virtual {p0, v0}, Lorg/htmlparser/util/NodeList;->toHtml(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toHtml(Z)Ljava/lang/String;
    .locals 3

    .line 168
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 169
    :goto_0
    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v1, v2, :cond_0

    .line 172
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 170
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v2, v2, v1

    invoke-interface {v2, p1}, Lorg/htmlparser/Node;->toHtml(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toNodeArray()[Lorg/htmlparser/Node;
    .locals 4

    .line 140
    iget v0, p0, Lorg/htmlparser/util/NodeList;->size:I

    invoke-direct {p0, v0}, Lorg/htmlparser/util/NodeList;->newNodeArrayFor(I)[Lorg/htmlparser/Node;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 265
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 266
    :goto_0
    iget v2, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v1, v2, :cond_0

    .line 269
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 267
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 369
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->beginParsing()V

    const/4 v0, 0x0

    .line 370
    :goto_0
    iget v1, p0, Lorg/htmlparser/util/NodeList;->size:I

    if-lt v0, v1, :cond_0

    .line 372
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->finishedParsing()V

    return-void

    .line 371
    :cond_0
    iget-object v1, p0, Lorg/htmlparser/util/NodeList;->nodeData:[Lorg/htmlparser/Node;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, Lorg/htmlparser/Node;->accept(Lorg/htmlparser/visitors/NodeVisitor;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
