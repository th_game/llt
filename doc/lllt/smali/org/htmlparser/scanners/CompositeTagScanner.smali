.class public Lorg/htmlparser/scanners/CompositeTagScanner;
.super Lorg/htmlparser/scanners/TagScanner;
.source "CompositeTagScanner.java"


# static fields
.field private static final mLeaveEnds:Z = false

.field private static final mUseJVMStack:Z = false


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Lorg/htmlparser/scanners/TagScanner;-><init>()V

    return-void
.end method


# virtual methods
.method protected addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V
    .locals 1

    .line 283
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-interface {p1, v0}, Lorg/htmlparser/Tag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    .line 285
    :cond_0
    invoke-interface {p2, p1}, Lorg/htmlparser/Node;->setParent(Lorg/htmlparser/Node;)V

    .line 286
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object p1

    invoke-virtual {p1, p2}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    return-void
.end method

.method protected createVirtualEndTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/lexer/Page;I)Lorg/htmlparser/Tag;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getRawTagName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 327
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 328
    new-instance v1, Lorg/htmlparser/Attribute;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 329
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object p1

    invoke-interface {p1, p3, p4, p4, v0}, Lorg/htmlparser/NodeFactory;->createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;

    move-result-object p1

    return-object p1
.end method

.method protected finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 301
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 302
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/htmlparser/scanners/CompositeTagScanner;->createVirtualEndTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/lexer/Page;I)Lorg/htmlparser/Tag;

    move-result-object p2

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    .line 303
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object p2

    invoke-interface {p2, p1}, Lorg/htmlparser/Tag;->setParent(Lorg/htmlparser/Node;)V

    .line 304
    invoke-interface {p1}, Lorg/htmlparser/Tag;->doSemanticAction()V

    return-void
.end method

.method public final isTagToBeEndedFor(Lorg/htmlparser/Tag;Lorg/htmlparser/Tag;)Z
    .locals 3

    .line 353
    invoke-interface {p2}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v0

    .line 354
    invoke-interface {p2}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 355
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getEndTagEnders()[Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 357
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getEnders()[Ljava/lang/String;

    move-result-object p1

    :goto_0
    const/4 p2, 0x0

    const/4 v1, 0x0

    .line 358
    :goto_1
    array-length v2, p1

    if-lt v1, v2, :cond_1

    goto :goto_2

    .line 359
    :cond_1
    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 p2, 0x1

    :goto_2
    return p2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 105
    invoke-interface {p1}, Lorg/htmlparser/Tag;->isEmptyXmlTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-interface {p1, p1}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    goto/16 :goto_7

    :cond_0
    const/4 v0, 0x0

    .line 110
    invoke-virtual {p2, v0}, Lorg/htmlparser/lexer/Lexer;->nextNode(Z)Lorg/htmlparser/Node;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_f

    .line 113
    instance-of v2, v0, Lorg/htmlparser/Tag;

    if-eqz v2, :cond_e

    .line 115
    move-object v2, v0

    check-cast v2, Lorg/htmlparser/Tag;

    .line 116
    invoke-interface {v2}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-interface {v2}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 120
    invoke-interface {p1, v2}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p0, p1, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->isTagToBeEndedFor(Lorg/htmlparser/Tag;Lorg/htmlparser/Tag;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 126
    invoke-interface {v2}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v0

    invoke-virtual {p2, v0}, Lorg/htmlparser/lexer/Lexer;->setPosition(I)V

    :goto_0
    move-object v0, v1

    goto/16 :goto_5

    .line 129
    :cond_2
    invoke-interface {v2}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v4

    if-nez v4, :cond_6

    .line 132
    invoke-interface {v2}, Lorg/htmlparser/Tag;->getThisScanner()Lorg/htmlparser/scanners/Scanner;

    move-result-object v3

    if-eqz v3, :cond_5

    if-ne v3, p0, :cond_4

    .line 145
    invoke-interface {v2}, Lorg/htmlparser/Tag;->isEmptyXmlTag()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 147
    invoke-interface {v2, v2}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    .line 148
    invoke-virtual {p0, v2, p2}, Lorg/htmlparser/scanners/CompositeTagScanner;->finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V

    .line 149
    invoke-virtual {p0, p1, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    goto/16 :goto_5

    .line 153
    :cond_3
    invoke-virtual {p3, p1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    move-object p1, v2

    goto/16 :goto_5

    .line 159
    :cond_4
    invoke-interface {v3, v2, p2, p3}, Lorg/htmlparser/scanners/Scanner;->scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;

    move-result-object v0

    .line 160
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    goto/16 :goto_5

    .line 165
    :cond_5
    invoke-virtual {p0, p1, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    goto/16 :goto_5

    .line 188
    :cond_6
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 189
    new-instance v5, Lorg/htmlparser/Attribute;

    invoke-direct {v5, v3, v1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v5

    .line 191
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v6

    invoke-interface {v2}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v7

    invoke-interface {v2}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v8

    .line 190
    invoke-interface {v5, v6, v7, v8, v4}, Lorg/htmlparser/NodeFactory;->createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;

    move-result-object v4

    .line 194
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getThisScanner()Lorg/htmlparser/scanners/Scanner;

    move-result-object v4

    if-eqz v4, :cond_d

    if-ne v4, p0, :cond_d

    .line 199
    invoke-virtual {p3}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const/4 v5, -0x1

    move v6, v4

    const/4 v4, -0x1

    :goto_1
    if-ne v5, v4, :cond_a

    if-gez v6, :cond_7

    goto :goto_3

    .line 203
    :cond_7
    invoke-virtual {p3, v6}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v7

    check-cast v7, Lorg/htmlparser/Tag;

    .line 204
    invoke-interface {v7}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    goto :goto_2

    .line 206
    :cond_8
    invoke-virtual {p0, v7, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->isTagToBeEndedFor(Lorg/htmlparser/Tag;Lorg/htmlparser/Tag;)Z

    move-result v7

    if-eqz v7, :cond_9

    :goto_2
    move v4, v6

    :cond_9
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    :cond_a
    :goto_3
    if-eq v5, v4, :cond_c

    .line 212
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/scanners/CompositeTagScanner;->finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V

    .line 213
    invoke-virtual {p3}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p3, v0}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/Tag;

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    .line 214
    invoke-virtual {p3}, Lorg/htmlparser/util/NodeList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_4
    if-gt p1, v4, :cond_b

    .line 220
    invoke-virtual {p3, v4}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    move-result-object p1

    check-cast p1, Lorg/htmlparser/Tag;

    goto/16 :goto_0

    .line 216
    :cond_b
    invoke-virtual {p3, p1}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/Tag;

    .line 217
    invoke-virtual {p0, v0, p2}, Lorg/htmlparser/scanners/CompositeTagScanner;->finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V

    add-int/lit8 v2, p1, -0x1

    .line 218
    invoke-virtual {p3, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v2

    check-cast v2, Lorg/htmlparser/Tag;

    invoke-virtual {p0, v2, v0}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    add-int/lit8 p1, p1, -0x1

    goto :goto_4

    .line 224
    :cond_c
    invoke-virtual {p0, p1, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    goto :goto_5

    .line 227
    :cond_d
    invoke-virtual {p0, p1, v2}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    goto :goto_5

    .line 235
    :cond_e
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    .line 236
    invoke-interface {v0}, Lorg/htmlparser/Node;->doSemanticAction()V

    :cond_f
    :goto_5
    if-nez v0, :cond_11

    .line 245
    invoke-virtual {p3}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v2

    if-eqz v2, :cond_11

    add-int/lit8 v2, v2, -0x1

    .line 248
    invoke-virtual {p3, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    .line 249
    instance-of v3, v0, Lorg/htmlparser/Tag;

    if-eqz v3, :cond_10

    .line 251
    move-object v3, v0

    check-cast v3, Lorg/htmlparser/Tag;

    .line 252
    invoke-interface {v3}, Lorg/htmlparser/Tag;->getThisScanner()Lorg/htmlparser/scanners/Scanner;

    move-result-object v4

    if-ne v4, p0, :cond_10

    .line 255
    invoke-virtual {p3, v2}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    .line 256
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/scanners/CompositeTagScanner;->finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V

    .line 257
    invoke-virtual {p0, v3, p1}, Lorg/htmlparser/scanners/CompositeTagScanner;->addChild(Lorg/htmlparser/Tag;Lorg/htmlparser/Node;)V

    move-object p1, v3

    goto :goto_6

    :cond_10
    move-object v0, v1

    :cond_11
    :goto_6
    if-nez v0, :cond_0

    .line 271
    :goto_7
    invoke-virtual {p0, p1, p2}, Lorg/htmlparser/scanners/CompositeTagScanner;->finishTag(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;)V

    return-object p1
.end method
