.class public Lorg/htmlparser/scanners/ScriptScanner;
.super Lorg/htmlparser/scanners/CompositeTagScanner;
.source "ScriptScanner.java"


# static fields
.field public static STRICT:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lorg/htmlparser/scanners/CompositeTagScanner;-><init>()V

    return-void
.end method


# virtual methods
.method public scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 102
    instance-of p3, p1, Lorg/htmlparser/tags/ScriptTag;

    if-eqz p3, :cond_1

    .line 104
    move-object p3, p1

    check-cast p3, Lorg/htmlparser/tags/ScriptTag;

    invoke-virtual {p3}, Lorg/htmlparser/tags/ScriptTag;->getLanguage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "JScript.Encode"

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VBScript.Encode"

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getCursor()Lorg/htmlparser/lexer/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/htmlparser/scanners/ScriptDecoder;->Decode(Lorg/htmlparser/lexer/Page;Lorg/htmlparser/lexer/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {p3, v0}, Lorg/htmlparser/tags/ScriptTag;->setScriptCode(Ljava/lang/String;)V

    .line 113
    :cond_1
    sget-boolean p3, Lorg/htmlparser/scanners/ScriptScanner;->STRICT:Z

    xor-int/lit8 p3, p3, 0x1

    invoke-virtual {p2, p3}, Lorg/htmlparser/lexer/Lexer;->parseCDATA(Z)Lorg/htmlparser/Node;

    move-result-object p3

    .line 114
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPosition()I

    move-result v0

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p2, v1}, Lorg/htmlparser/lexer/Lexer;->nextNode(Z)Lorg/htmlparser/Node;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 117
    instance-of v4, v2, Lorg/htmlparser/Tag;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/htmlparser/Tag;

    invoke-interface {v4}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 118
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getIds()[Ljava/lang/String;

    move-result-object v5

    aget-object v1, v5, v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 120
    :cond_2
    invoke-virtual {p2, v0}, Lorg/htmlparser/lexer/Lexer;->setPosition(I)V

    move-object v2, v3

    :cond_3
    if-nez v2, :cond_4

    .line 127
    new-instance v1, Lorg/htmlparser/Attribute;

    const-string v2, "/script"

    invoke-direct {v1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 129
    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 130
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v1

    .line 131
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object p2

    .line 130
    invoke-interface {v1, p2, v0, v0, v2}, Lorg/htmlparser/NodeFactory;->createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;

    move-result-object v2

    .line 133
    :cond_4
    move-object p2, v2

    check-cast p2, Lorg/htmlparser/Tag;

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    if-eqz p3, :cond_5

    .line 136
    new-instance p2, Lorg/htmlparser/util/NodeList;

    invoke-direct {p2, p3}, Lorg/htmlparser/util/NodeList;-><init>(Lorg/htmlparser/Node;)V

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    .line 137
    invoke-interface {p3, p1}, Lorg/htmlparser/Node;->setParent(Lorg/htmlparser/Node;)V

    .line 139
    :cond_5
    invoke-interface {v2, p1}, Lorg/htmlparser/Node;->setParent(Lorg/htmlparser/Node;)V

    .line 140
    invoke-interface {p1}, Lorg/htmlparser/Tag;->doSemanticAction()V

    return-object p1
.end method
