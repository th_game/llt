.class public Lorg/htmlparser/scanners/StyleScanner;
.super Lorg/htmlparser/scanners/CompositeTagScanner;
.source "StyleScanner.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lorg/htmlparser/scanners/CompositeTagScanner;-><init>()V

    return-void
.end method


# virtual methods
.method public scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 70
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->parseCDATA()Lorg/htmlparser/Node;

    move-result-object p3

    .line 71
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPosition()I

    move-result v0

    const/4 v1, 0x0

    .line 72
    invoke-virtual {p2, v1}, Lorg/htmlparser/lexer/Lexer;->nextNode(Z)Lorg/htmlparser/Node;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 74
    instance-of v4, v2, Lorg/htmlparser/Tag;

    if-eqz v4, :cond_0

    move-object v4, v2

    check-cast v4, Lorg/htmlparser/Tag;

    invoke-interface {v4}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 75
    invoke-interface {v4}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getIds()[Ljava/lang/String;

    move-result-object v5

    aget-object v1, v5, v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 77
    :cond_0
    invoke-virtual {p2, v0}, Lorg/htmlparser/lexer/Lexer;->setPosition(I)V

    move-object v2, v3

    :cond_1
    if-nez v2, :cond_2

    .line 84
    new-instance v1, Lorg/htmlparser/Attribute;

    const-string v2, "/style"

    invoke-direct {v1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 86
    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 87
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v1

    .line 88
    invoke-virtual {p2}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object p2

    .line 87
    invoke-interface {v1, p2, v0, v0, v2}, Lorg/htmlparser/NodeFactory;->createTagNode(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)Lorg/htmlparser/Tag;

    move-result-object v2

    .line 90
    :cond_2
    move-object p2, v2

    check-cast p2, Lorg/htmlparser/Tag;

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->setEndTag(Lorg/htmlparser/Tag;)V

    if-eqz p3, :cond_3

    .line 93
    new-instance p2, Lorg/htmlparser/util/NodeList;

    invoke-direct {p2, p3}, Lorg/htmlparser/util/NodeList;-><init>(Lorg/htmlparser/Node;)V

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    .line 94
    invoke-interface {p3, p1}, Lorg/htmlparser/Node;->setParent(Lorg/htmlparser/Node;)V

    .line 96
    :cond_3
    invoke-interface {v2, p1}, Lorg/htmlparser/Node;->setParent(Lorg/htmlparser/Node;)V

    .line 97
    invoke-interface {p1}, Lorg/htmlparser/Tag;->doSemanticAction()V

    return-object p1
.end method
