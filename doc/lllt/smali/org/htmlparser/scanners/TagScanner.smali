.class public Lorg/htmlparser/scanners/TagScanner;
.super Ljava/lang/Object;
.source "TagScanner.java"

# interfaces
.implements Lorg/htmlparser/scanners/Scanner;
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public scan(Lorg/htmlparser/Tag;Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/NodeList;)Lorg/htmlparser/Tag;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 68
    invoke-interface {p1}, Lorg/htmlparser/Tag;->doSemanticAction()V

    return-object p1
.end method
