.class public Lorg/htmlparser/scanners/ScriptDecoder;
.super Ljava/lang/Object;
.source "ScriptDecoder.java"


# static fields
.field public static LAST_STATE:I = 0x0

.field protected static final STATE_CHECKSUM:I = 0x6

.field protected static final STATE_DECODE:I = 0x4

.field public static final STATE_DONE:I = 0x0

.field protected static final STATE_ESCAPE:I = 0x5

.field protected static final STATE_FINAL:I = 0x7

.field public static final STATE_INITIAL:I = 0x1

.field protected static final STATE_LENGTH:I = 0x2

.field protected static final STATE_PREFIX:I = 0x3

.field protected static mDigits:[I

.field protected static mEncodingIndex:[B

.field protected static mEscaped:[C

.field protected static mEscapes:[C

.field protected static mLeader:[C

.field protected static mLookupTable:[[C

.field protected static mPrefix:[C

.field protected static mTrailer:[C


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x40

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/4 v3, 0x2

    aput-byte v3, v0, v2

    const/4 v4, 0x3

    aput-byte v2, v0, v4

    const/4 v4, 0x4

    aput-byte v3, v0, v4

    const/4 v4, 0x6

    aput-byte v3, v0, v4

    const/16 v4, 0x9

    aput-byte v3, v0, v4

    const/16 v5, 0xb

    aput-byte v3, v0, v5

    const/16 v5, 0xc

    aput-byte v2, v0, v5

    const/16 v5, 0xe

    aput-byte v3, v0, v5

    const/16 v5, 0x10

    aput-byte v2, v0, v5

    const/16 v5, 0x12

    aput-byte v3, v0, v5

    const/16 v5, 0x14

    aput-byte v2, v0, v5

    const/16 v5, 0x15

    aput-byte v2, v0, v5

    const/16 v5, 0x16

    aput-byte v3, v0, v5

    const/16 v5, 0x19

    aput-byte v3, v0, v5

    const/16 v5, 0x1a

    aput-byte v2, v0, v5

    const/16 v5, 0x1c

    aput-byte v3, v0, v5

    const/16 v5, 0x1f

    aput-byte v3, v0, v5

    const/16 v5, 0x20

    aput-byte v2, v0, v5

    const/16 v6, 0x21

    aput-byte v2, v0, v6

    const/16 v6, 0x23

    aput-byte v3, v0, v6

    const/16 v7, 0x25

    aput-byte v3, v0, v7

    const/16 v8, 0x27

    aput-byte v2, v0, v8

    const/16 v9, 0x29

    aput-byte v2, v0, v9

    const/16 v10, 0x2a

    aput-byte v2, v0, v10

    const/16 v11, 0x2b

    aput-byte v3, v0, v11

    const/16 v12, 0x2d

    aput-byte v2, v0, v12

    const/16 v12, 0x2f

    aput-byte v3, v0, v12

    const/16 v13, 0x30

    aput-byte v2, v0, v13

    const/16 v14, 0x32

    aput-byte v3, v0, v14

    const/16 v14, 0x34

    aput-byte v2, v0, v14

    const/16 v15, 0x35

    aput-byte v2, v0, v15

    const/16 v15, 0x36

    aput-byte v3, v0, v15

    const/16 v15, 0x39

    aput-byte v2, v0, v15

    const/16 v15, 0x3a

    aput-byte v2, v0, v15

    const/16 v15, 0x3b

    aput-byte v3, v0, v15

    const/16 v15, 0x3d

    aput-byte v2, v0, v15

    const/16 v15, 0x3f

    aput-byte v3, v0, v15

    .line 97
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mEncodingIndex:[B

    const/4 v0, 0x3

    new-array v0, v0, [[C

    const/16 v15, 0x61

    new-array v15, v15, [C

    const/16 v17, 0x7b

    aput-char v17, v15, v1

    const/16 v17, 0x32

    aput-char v17, v15, v2

    aput-char v13, v15, v3

    const/16 v17, 0x3

    const/16 v18, 0x21

    aput-char v18, v15, v17

    const/16 v17, 0x4

    aput-char v9, v15, v17

    const/16 v17, 0x5

    const/16 v18, 0x5b

    aput-char v18, v15, v17

    const/16 v17, 0x6

    const/16 v18, 0x38

    aput-char v18, v15, v17

    const/16 v17, 0x7

    const/16 v18, 0x33

    aput-char v18, v15, v17

    const/16 v17, 0x8

    const/16 v18, 0x3d

    aput-char v18, v15, v17

    const/16 v17, 0x58

    aput-char v17, v15, v4

    const/16 v17, 0xa

    const/16 v18, 0x3a

    aput-char v18, v15, v17

    const/16 v17, 0xb

    const/16 v18, 0x35

    aput-char v18, v15, v17

    const/16 v17, 0xc

    const/16 v18, 0x65

    aput-char v18, v15, v17

    const/16 v17, 0xd

    const/16 v18, 0x39

    aput-char v18, v15, v17

    const/16 v17, 0xe

    const/16 v18, 0x5c

    aput-char v18, v15, v17

    const/16 v17, 0xf

    const/16 v18, 0x56

    aput-char v18, v15, v17

    const/16 v17, 0x10

    const/16 v18, 0x73

    aput-char v18, v15, v17

    const/16 v17, 0x11

    const/16 v18, 0x66

    aput-char v18, v15, v17

    const/16 v17, 0x12

    const/16 v18, 0x4e

    aput-char v18, v15, v17

    const/16 v17, 0x13

    const/16 v18, 0x45

    aput-char v18, v15, v17

    const/16 v17, 0x14

    const/16 v18, 0x6b

    aput-char v18, v15, v17

    const/16 v17, 0x15

    const/16 v18, 0x62

    aput-char v18, v15, v17

    const/16 v17, 0x16

    const/16 v18, 0x59

    aput-char v18, v15, v17

    const/16 v17, 0x17

    const/16 v18, 0x78

    aput-char v18, v15, v17

    const/16 v17, 0x18

    const/16 v18, 0x5e

    aput-char v18, v15, v17

    const/16 v17, 0x19

    const/16 v18, 0x7d

    aput-char v18, v15, v17

    const/16 v17, 0x1a

    const/16 v18, 0x4a

    aput-char v18, v15, v17

    const/16 v17, 0x1b

    const/16 v18, 0x6d

    aput-char v18, v15, v17

    const/16 v17, 0x1c

    const/16 v18, 0x71

    aput-char v18, v15, v17

    const/16 v17, 0x1e

    const/16 v18, 0x60

    aput-char v18, v15, v17

    const/16 v17, 0x53

    aput-char v17, v15, v5

    const/16 v17, 0x22

    const/16 v18, 0x42

    aput-char v18, v15, v17

    aput-char v8, v15, v6

    const/16 v17, 0x24

    const/16 v18, 0x48

    aput-char v18, v15, v17

    const/16 v17, 0x72

    aput-char v17, v15, v7

    const/16 v17, 0x26

    const/16 v18, 0x75

    aput-char v18, v15, v17

    const/16 v17, 0x31

    aput-char v17, v15, v8

    const/16 v17, 0x28

    const/16 v18, 0x37

    aput-char v18, v15, v17

    const/16 v17, 0x4d

    aput-char v17, v15, v9

    const/16 v17, 0x52

    aput-char v17, v15, v10

    const/16 v17, 0x22

    aput-char v17, v15, v11

    const/16 v17, 0x2c

    const/16 v18, 0x54

    aput-char v18, v15, v17

    const/16 v17, 0x2d

    const/16 v18, 0x6a

    aput-char v18, v15, v17

    const/16 v17, 0x2e

    const/16 v18, 0x47

    aput-char v18, v15, v17

    const/16 v17, 0x64

    aput-char v17, v15, v12

    const/16 v17, 0x2d

    aput-char v17, v15, v13

    const/16 v17, 0x31

    aput-char v5, v15, v17

    const/16 v17, 0x32

    const/16 v18, 0x7f

    aput-char v18, v15, v17

    const/16 v17, 0x33

    const/16 v18, 0x2e

    aput-char v18, v15, v17

    const/16 v17, 0x4c

    aput-char v17, v15, v14

    const/16 v17, 0x35

    const/16 v18, 0x5d

    aput-char v18, v15, v17

    const/16 v17, 0x36

    const/16 v18, 0x7e

    aput-char v18, v15, v17

    const/16 v17, 0x37

    const/16 v18, 0x6c

    aput-char v18, v15, v17

    const/16 v17, 0x38

    const/16 v18, 0x6f

    aput-char v18, v15, v17

    const/16 v17, 0x39

    const/16 v18, 0x79

    aput-char v18, v15, v17

    const/16 v17, 0x3a

    const/16 v18, 0x74

    aput-char v18, v15, v17

    const/16 v17, 0x3b

    const/16 v18, 0x43

    aput-char v18, v15, v17

    const/16 v17, 0x3c

    const/16 v18, 0x26

    aput-char v18, v15, v17

    const/16 v17, 0x3d

    const/16 v18, 0x76

    aput-char v18, v15, v17

    const/16 v17, 0x3e

    aput-char v7, v15, v17

    const/16 v17, 0x24

    const/16 v16, 0x3f

    aput-char v17, v15, v16

    const/16 v17, 0x40

    aput-char v11, v15, v17

    const/16 v17, 0x41

    const/16 v18, 0x28

    aput-char v18, v15, v17

    const/16 v17, 0x42

    aput-char v6, v15, v17

    const/16 v17, 0x43

    const/16 v18, 0x41

    aput-char v18, v15, v17

    const/16 v17, 0x44

    aput-char v14, v15, v17

    const/16 v17, 0x45

    aput-char v4, v15, v17

    const/16 v17, 0x46

    aput-char v10, v15, v17

    const/16 v17, 0x47

    const/16 v18, 0x44

    aput-char v18, v15, v17

    const/16 v17, 0x48

    const/16 v16, 0x3f

    aput-char v16, v15, v17

    const/16 v17, 0x49

    const/16 v18, 0x77

    aput-char v18, v15, v17

    const/16 v17, 0x4a

    const/16 v18, 0x3b

    aput-char v18, v15, v17

    const/16 v17, 0x4b

    const/16 v18, 0x55

    aput-char v18, v15, v17

    const/16 v17, 0x4c

    const/16 v18, 0x69

    aput-char v18, v15, v17

    const/16 v17, 0x4d

    const/16 v18, 0x61

    aput-char v18, v15, v17

    const/16 v17, 0x4e

    const/16 v18, 0x63

    aput-char v18, v15, v17

    const/16 v17, 0x4f

    const/16 v18, 0x50

    aput-char v18, v15, v17

    const/16 v17, 0x50

    const/16 v18, 0x67

    aput-char v18, v15, v17

    const/16 v17, 0x51

    const/16 v18, 0x51

    aput-char v18, v15, v17

    const/16 v17, 0x52

    const/16 v18, 0x49

    aput-char v18, v15, v17

    const/16 v17, 0x53

    const/16 v18, 0x4f

    aput-char v18, v15, v17

    const/16 v17, 0x54

    const/16 v18, 0x46

    aput-char v18, v15, v17

    const/16 v17, 0x55

    const/16 v18, 0x68

    aput-char v18, v15, v17

    const/16 v17, 0x56

    const/16 v18, 0x7c

    aput-char v18, v15, v17

    const/16 v17, 0x57

    const/16 v18, 0x36

    aput-char v18, v15, v17

    const/16 v17, 0x58

    const/16 v18, 0x70

    aput-char v18, v15, v17

    const/16 v17, 0x59

    const/16 v18, 0x6e

    aput-char v18, v15, v17

    const/16 v17, 0x5a

    const/16 v18, 0x7a

    aput-char v18, v15, v17

    const/16 v17, 0x5b

    aput-char v12, v15, v17

    const/16 v17, 0x5c

    const/16 v18, 0x5f

    aput-char v18, v15, v17

    const/16 v17, 0x5d

    const/16 v18, 0x4b

    aput-char v18, v15, v17

    const/16 v17, 0x5e

    const/16 v18, 0x5a

    aput-char v18, v15, v17

    const/16 v17, 0x5f

    const/16 v18, 0x2c

    aput-char v18, v15, v17

    const/16 v17, 0x60

    const/16 v18, 0x57

    aput-char v18, v15, v17

    aput-object v15, v0, v1

    const/16 v15, 0x61

    new-array v15, v15, [C

    const/16 v17, 0x57

    aput-char v17, v15, v1

    const/16 v17, 0x2e

    aput-char v17, v15, v2

    const/16 v17, 0x47

    aput-char v17, v15, v3

    const/16 v17, 0x3

    const/16 v18, 0x7a

    aput-char v18, v15, v17

    const/16 v17, 0x4

    const/16 v18, 0x56

    aput-char v18, v15, v17

    const/16 v17, 0x5

    const/16 v18, 0x42

    aput-char v18, v15, v17

    const/16 v17, 0x6

    const/16 v18, 0x6a

    aput-char v18, v15, v17

    const/16 v17, 0x7

    aput-char v12, v15, v17

    const/16 v17, 0x8

    const/16 v18, 0x26

    aput-char v18, v15, v17

    const/16 v17, 0x49

    aput-char v17, v15, v4

    const/16 v17, 0xa

    const/16 v18, 0x41

    aput-char v18, v15, v17

    const/16 v17, 0xb

    aput-char v14, v15, v17

    const/16 v17, 0xc

    const/16 v18, 0x32

    aput-char v18, v15, v17

    const/16 v17, 0xd

    const/16 v18, 0x5b

    aput-char v18, v15, v17

    const/16 v17, 0xe

    const/16 v18, 0x76

    aput-char v18, v15, v17

    const/16 v17, 0xf

    const/16 v18, 0x72

    aput-char v18, v15, v17

    const/16 v17, 0x10

    const/16 v18, 0x43

    aput-char v18, v15, v17

    const/16 v17, 0x11

    const/16 v18, 0x38

    aput-char v18, v15, v17

    const/16 v17, 0x12

    const/16 v18, 0x39

    aput-char v18, v15, v17

    const/16 v17, 0x13

    const/16 v18, 0x70

    aput-char v18, v15, v17

    const/16 v17, 0x14

    const/16 v18, 0x45

    aput-char v18, v15, v17

    const/16 v17, 0x15

    const/16 v18, 0x68

    aput-char v18, v15, v17

    const/16 v17, 0x16

    const/16 v18, 0x71

    aput-char v18, v15, v17

    const/16 v17, 0x17

    const/16 v18, 0x4f

    aput-char v18, v15, v17

    const/16 v17, 0x18

    aput-char v4, v15, v17

    const/16 v17, 0x19

    const/16 v18, 0x62

    aput-char v18, v15, v17

    const/16 v17, 0x1a

    const/16 v18, 0x44

    aput-char v18, v15, v17

    const/16 v17, 0x1b

    aput-char v6, v15, v17

    const/16 v17, 0x1c

    const/16 v18, 0x75

    aput-char v18, v15, v17

    const/16 v17, 0x1e

    const/16 v18, 0x7e

    aput-char v18, v15, v17

    const/16 v17, 0x5e

    aput-char v17, v15, v5

    const/16 v17, 0x22

    const/16 v18, 0x77

    aput-char v18, v15, v17

    const/16 v17, 0x4a

    aput-char v17, v15, v6

    const/16 v17, 0x24

    const/16 v18, 0x61

    aput-char v18, v15, v17

    const/16 v17, 0x5d

    aput-char v17, v15, v7

    const/16 v17, 0x26

    const/16 v18, 0x22

    aput-char v18, v15, v17

    const/16 v17, 0x4b

    aput-char v17, v15, v8

    const/16 v17, 0x28

    const/16 v18, 0x6f

    aput-char v18, v15, v17

    const/16 v17, 0x4e

    aput-char v17, v15, v9

    const/16 v17, 0x3b

    aput-char v17, v15, v10

    const/16 v17, 0x4c

    aput-char v17, v15, v11

    const/16 v17, 0x2c

    const/16 v18, 0x50

    aput-char v18, v15, v17

    const/16 v17, 0x2d

    const/16 v18, 0x67

    aput-char v18, v15, v17

    const/16 v17, 0x2e

    aput-char v10, v15, v17

    const/16 v17, 0x7d

    aput-char v17, v15, v12

    const/16 v17, 0x74

    aput-char v17, v15, v13

    const/16 v17, 0x31

    const/16 v18, 0x54

    aput-char v18, v15, v17

    const/16 v17, 0x32

    aput-char v11, v15, v17

    const/16 v17, 0x33

    const/16 v18, 0x2d

    aput-char v18, v15, v17

    const/16 v17, 0x2c

    aput-char v17, v15, v14

    const/16 v17, 0x35

    aput-char v13, v15, v17

    const/16 v17, 0x36

    const/16 v18, 0x6e

    aput-char v18, v15, v17

    const/16 v17, 0x37

    const/16 v18, 0x6b

    aput-char v18, v15, v17

    const/16 v17, 0x38

    const/16 v18, 0x66

    aput-char v18, v15, v17

    const/16 v17, 0x39

    const/16 v18, 0x35

    aput-char v18, v15, v17

    const/16 v17, 0x3a

    aput-char v7, v15, v17

    const/16 v17, 0x3b

    const/16 v18, 0x21

    aput-char v18, v15, v17

    const/16 v17, 0x3c

    const/16 v18, 0x64

    aput-char v18, v15, v17

    const/16 v17, 0x3d

    const/16 v18, 0x4d

    aput-char v18, v15, v17

    const/16 v17, 0x3e

    const/16 v18, 0x52

    aput-char v18, v15, v17

    const/16 v17, 0x63

    const/16 v16, 0x3f

    aput-char v17, v15, v16

    const/16 v17, 0x40

    aput-char v16, v15, v17

    const/16 v17, 0x41

    const/16 v18, 0x7b

    aput-char v18, v15, v17

    const/16 v17, 0x42

    const/16 v18, 0x78

    aput-char v18, v15, v17

    const/16 v17, 0x43

    aput-char v9, v15, v17

    const/16 v17, 0x44

    const/16 v18, 0x28

    aput-char v18, v15, v17

    const/16 v17, 0x45

    const/16 v18, 0x73

    aput-char v18, v15, v17

    const/16 v17, 0x46

    const/16 v18, 0x59

    aput-char v18, v15, v17

    const/16 v17, 0x47

    const/16 v18, 0x33

    aput-char v18, v15, v17

    const/16 v17, 0x48

    const/16 v18, 0x7f

    aput-char v18, v15, v17

    const/16 v17, 0x49

    const/16 v18, 0x6d

    aput-char v18, v15, v17

    const/16 v17, 0x4a

    const/16 v18, 0x55

    aput-char v18, v15, v17

    const/16 v17, 0x4b

    const/16 v18, 0x53

    aput-char v18, v15, v17

    const/16 v17, 0x4c

    const/16 v18, 0x7c

    aput-char v18, v15, v17

    const/16 v17, 0x4d

    const/16 v18, 0x3a

    aput-char v18, v15, v17

    const/16 v17, 0x4e

    const/16 v18, 0x5f

    aput-char v18, v15, v17

    const/16 v17, 0x4f

    const/16 v18, 0x65

    aput-char v18, v15, v17

    const/16 v17, 0x50

    const/16 v18, 0x46

    aput-char v18, v15, v17

    const/16 v17, 0x51

    const/16 v18, 0x58

    aput-char v18, v15, v17

    const/16 v17, 0x52

    const/16 v18, 0x31

    aput-char v18, v15, v17

    const/16 v17, 0x53

    const/16 v18, 0x69

    aput-char v18, v15, v17

    const/16 v17, 0x54

    const/16 v18, 0x6c

    aput-char v18, v15, v17

    const/16 v17, 0x55

    const/16 v18, 0x5a

    aput-char v18, v15, v17

    const/16 v17, 0x56

    const/16 v18, 0x48

    aput-char v18, v15, v17

    const/16 v17, 0x57

    aput-char v8, v15, v17

    const/16 v17, 0x58

    const/16 v18, 0x5c

    aput-char v18, v15, v17

    const/16 v17, 0x59

    const/16 v18, 0x3d

    aput-char v18, v15, v17

    const/16 v17, 0x5a

    const/16 v18, 0x24

    aput-char v18, v15, v17

    const/16 v17, 0x5b

    const/16 v18, 0x79

    aput-char v18, v15, v17

    const/16 v17, 0x5c

    const/16 v18, 0x37

    aput-char v18, v15, v17

    const/16 v17, 0x5d

    const/16 v18, 0x60

    aput-char v18, v15, v17

    const/16 v17, 0x5e

    const/16 v18, 0x51

    aput-char v18, v15, v17

    const/16 v17, 0x5f

    aput-char v5, v15, v17

    const/16 v17, 0x60

    const/16 v18, 0x36

    aput-char v18, v15, v17

    aput-object v15, v0, v2

    const/16 v15, 0x61

    new-array v15, v15, [C

    const/16 v17, 0x6e

    aput-char v17, v15, v1

    const/16 v17, 0x2d

    aput-char v17, v15, v2

    const/16 v2, 0x75

    aput-char v2, v15, v3

    const/4 v2, 0x3

    const/16 v17, 0x52

    aput-char v17, v15, v2

    const/4 v2, 0x4

    const/16 v17, 0x60

    aput-char v17, v15, v2

    const/4 v2, 0x5

    const/16 v17, 0x71

    aput-char v17, v15, v2

    const/4 v2, 0x6

    const/16 v17, 0x5e

    aput-char v17, v15, v2

    const/4 v2, 0x7

    const/16 v17, 0x49

    aput-char v17, v15, v2

    const/16 v2, 0x8

    const/16 v17, 0x5c

    aput-char v17, v15, v2

    const/16 v2, 0x62

    aput-char v2, v15, v4

    const/16 v2, 0xa

    const/16 v17, 0x7d

    aput-char v17, v15, v2

    const/16 v2, 0xb

    aput-char v9, v15, v2

    const/16 v2, 0xc

    const/16 v17, 0x36

    aput-char v17, v15, v2

    const/16 v2, 0xd

    aput-char v5, v15, v2

    const/16 v2, 0xe

    const/16 v17, 0x7c

    aput-char v17, v15, v2

    const/16 v2, 0xf

    const/16 v17, 0x7a

    aput-char v17, v15, v2

    const/16 v2, 0x10

    const/16 v17, 0x7f

    aput-char v17, v15, v2

    const/16 v2, 0x11

    const/16 v17, 0x6b

    aput-char v17, v15, v2

    const/16 v2, 0x12

    const/16 v17, 0x63

    aput-char v17, v15, v2

    const/16 v2, 0x13

    const/16 v17, 0x33

    aput-char v17, v15, v2

    const/16 v2, 0x14

    aput-char v11, v15, v2

    const/16 v2, 0x15

    const/16 v17, 0x68

    aput-char v17, v15, v2

    const/16 v2, 0x16

    const/16 v17, 0x51

    aput-char v17, v15, v2

    const/16 v2, 0x17

    const/16 v17, 0x66

    aput-char v17, v15, v2

    const/16 v2, 0x18

    const/16 v17, 0x76

    aput-char v17, v15, v2

    const/16 v2, 0x19

    const/16 v17, 0x31

    aput-char v17, v15, v2

    const/16 v2, 0x1a

    const/16 v17, 0x64

    aput-char v17, v15, v2

    const/16 v2, 0x1b

    const/16 v17, 0x54

    aput-char v17, v15, v2

    const/16 v2, 0x1c

    const/16 v17, 0x43

    aput-char v17, v15, v2

    const/16 v2, 0x1e

    const/16 v17, 0x3a

    aput-char v17, v15, v2

    const/16 v2, 0x7e

    aput-char v2, v15, v5

    const/16 v2, 0x22

    const/16 v5, 0x45

    aput-char v5, v15, v2

    const/16 v2, 0x2c

    aput-char v2, v15, v6

    const/16 v2, 0x24

    aput-char v10, v15, v2

    const/16 v2, 0x74

    aput-char v2, v15, v7

    const/16 v2, 0x26

    aput-char v8, v15, v2

    const/16 v2, 0x37

    aput-char v2, v15, v8

    const/16 v2, 0x28

    const/16 v5, 0x44

    aput-char v5, v15, v2

    const/16 v2, 0x79

    aput-char v2, v15, v9

    const/16 v2, 0x59

    aput-char v2, v15, v10

    aput-char v12, v15, v11

    const/16 v2, 0x2c

    const/16 v5, 0x6f

    aput-char v5, v15, v2

    const/16 v2, 0x2d

    const/16 v5, 0x26

    aput-char v5, v15, v2

    const/16 v2, 0x2e

    const/16 v5, 0x72

    aput-char v5, v15, v2

    const/16 v2, 0x6a

    aput-char v2, v15, v12

    const/16 v2, 0x39

    aput-char v2, v15, v13

    const/16 v2, 0x31

    const/16 v5, 0x7b

    aput-char v5, v15, v2

    const/16 v2, 0x32

    const/16 v5, 0x3f

    aput-char v5, v15, v2

    const/16 v2, 0x33

    const/16 v5, 0x38

    aput-char v5, v15, v2

    const/16 v2, 0x77

    aput-char v2, v15, v14

    const/16 v2, 0x35

    const/16 v5, 0x67

    aput-char v5, v15, v2

    const/16 v2, 0x36

    const/16 v5, 0x53

    aput-char v5, v15, v2

    const/16 v2, 0x37

    const/16 v5, 0x47

    aput-char v5, v15, v2

    const/16 v2, 0x38

    aput-char v14, v15, v2

    const/16 v2, 0x39

    const/16 v5, 0x78

    aput-char v5, v15, v2

    const/16 v2, 0x3a

    const/16 v5, 0x5d

    aput-char v5, v15, v2

    const/16 v2, 0x3b

    aput-char v13, v15, v2

    const/16 v2, 0x3c

    aput-char v6, v15, v2

    const/16 v2, 0x3d

    const/16 v5, 0x5a

    aput-char v5, v15, v2

    const/16 v2, 0x3e

    const/16 v5, 0x5b

    aput-char v5, v15, v2

    const/16 v2, 0x6c

    const/16 v5, 0x3f

    aput-char v2, v15, v5

    const/16 v2, 0x40

    const/16 v5, 0x48

    aput-char v5, v15, v2

    const/16 v2, 0x41

    const/16 v5, 0x55

    aput-char v5, v15, v2

    const/16 v2, 0x42

    const/16 v5, 0x70

    aput-char v5, v15, v2

    const/16 v2, 0x43

    const/16 v5, 0x69

    aput-char v5, v15, v2

    const/16 v2, 0x44

    const/16 v5, 0x2e

    aput-char v5, v15, v2

    const/16 v2, 0x45

    const/16 v5, 0x4c

    aput-char v5, v15, v2

    const/16 v2, 0x46

    const/16 v5, 0x21

    aput-char v5, v15, v2

    const/16 v2, 0x47

    const/16 v5, 0x24

    aput-char v5, v15, v2

    const/16 v2, 0x48

    const/16 v5, 0x4e

    aput-char v5, v15, v2

    const/16 v2, 0x49

    const/16 v5, 0x50

    aput-char v5, v15, v2

    const/16 v2, 0x4a

    aput-char v4, v15, v2

    const/16 v2, 0x4b

    const/16 v4, 0x56

    aput-char v4, v15, v2

    const/16 v2, 0x4c

    const/16 v4, 0x73

    aput-char v4, v15, v2

    const/16 v2, 0x4d

    const/16 v4, 0x35

    aput-char v4, v15, v2

    const/16 v2, 0x4e

    const/16 v4, 0x61

    aput-char v4, v15, v2

    const/16 v2, 0x4f

    const/16 v4, 0x4b

    aput-char v4, v15, v2

    const/16 v2, 0x50

    const/16 v4, 0x58

    aput-char v4, v15, v2

    const/16 v2, 0x51

    const/16 v4, 0x3b

    aput-char v4, v15, v2

    const/16 v2, 0x52

    const/16 v4, 0x57

    aput-char v4, v15, v2

    const/16 v2, 0x53

    const/16 v4, 0x22

    aput-char v4, v15, v2

    const/16 v2, 0x54

    const/16 v4, 0x6d

    aput-char v4, v15, v2

    const/16 v2, 0x55

    const/16 v4, 0x4d

    aput-char v4, v15, v2

    const/16 v2, 0x56

    aput-char v7, v15, v2

    const/16 v2, 0x57

    const/16 v4, 0x28

    aput-char v4, v15, v2

    const/16 v2, 0x58

    const/16 v4, 0x46

    aput-char v4, v15, v2

    const/16 v2, 0x59

    const/16 v4, 0x4a

    aput-char v4, v15, v2

    const/16 v2, 0x5a

    const/16 v4, 0x32

    aput-char v4, v15, v2

    const/16 v2, 0x5b

    const/16 v4, 0x41

    aput-char v4, v15, v2

    const/16 v2, 0x5c

    const/16 v4, 0x3d

    aput-char v4, v15, v2

    const/16 v2, 0x5d

    const/16 v4, 0x5f

    aput-char v4, v15, v2

    const/16 v2, 0x5e

    const/16 v4, 0x4f

    aput-char v4, v15, v2

    const/16 v2, 0x5f

    const/16 v4, 0x42

    aput-char v4, v15, v2

    const/16 v2, 0x60

    const/16 v4, 0x65

    aput-char v4, v15, v2

    aput-object v15, v0, v3

    .line 110
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mLookupTable:[[C

    const/16 v0, 0x7b

    new-array v0, v0, [I

    .line 166
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mDigits:[I

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x1a

    if-lt v0, v2, :cond_1

    :goto_1
    const/16 v0, 0xa

    if-lt v1, v0, :cond_0

    .line 174
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mDigits:[I

    const/16 v1, 0x3e

    aput v1, v0, v11

    const/16 v2, 0x3f

    .line 175
    aput v2, v0, v12

    const/4 v0, 0x4

    new-array v0, v0, [C

    .line 184
    fill-array-data v0, :array_0

    .line 183
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mLeader:[C

    new-array v0, v3, [C

    .line 196
    fill-array-data v0, :array_1

    .line 195
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mPrefix:[C

    const/4 v0, 0x6

    new-array v0, v0, [C

    .line 207
    fill-array-data v0, :array_2

    .line 206
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mTrailer:[C

    const/4 v0, 0x5

    new-array v0, v0, [C

    .line 220
    fill-array-data v0, :array_3

    .line 219
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mEscapes:[C

    const/4 v0, 0x5

    new-array v0, v0, [C

    .line 232
    fill-array-data v0, :array_4

    .line 231
    sput-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mEscaped:[C

    return-void

    :cond_0
    const/16 v2, 0x3f

    .line 173
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mDigits:[I

    add-int/lit8 v4, v1, 0x30

    add-int/lit8 v5, v1, 0x34

    aput v5, v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/16 v2, 0x3f

    .line 169
    sget-object v4, Lorg/htmlparser/scanners/ScriptDecoder;->mDigits:[I

    add-int/lit8 v5, v0, 0x41

    aput v0, v4, v5

    add-int/lit8 v5, v0, 0x61

    add-int/lit8 v6, v0, 0x1a

    .line 170
    aput v6, v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    nop

    :array_0
    .array-data 2
        0x23s
        0x40s
        0x7es
        0x5es
    .end array-data

    :array_1
    .array-data 2
        0x3ds
        0x3ds
    .end array-data

    :array_2
    .array-data 2
        0x3ds
        0x3ds
        0x5es
        0x23s
        0x7es
        0x40s
    .end array-data

    :array_3
    .array-data 2
        0x23s
        0x26s
        0x21s
        0x2as
        0x24s
    .end array-data

    nop

    :array_4
    .array-data 2
        0xds
        0xas
        0x3cs
        0x3es
        0x40s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Decode(Lorg/htmlparser/lexer/Page;Lorg/htmlparser/lexer/Cursor;)Ljava/lang/String;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x6

    new-array v1, v0, [C

    .line 295
    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v3, 0x400

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v6, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    :goto_0
    if-nez v8, :cond_0

    .line 452
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 308
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v0

    int-to-char v3, v0

    const v7, 0xffff

    if-ne v7, v0, :cond_2

    if-ne v6, v8, :cond_1

    if-nez v9, :cond_1

    if-nez v10, :cond_1

    if-nez v11, :cond_1

    if-nez v12, :cond_1

    if-nez v13, :cond_1

    const/4 v0, 0x6

    const/4 v8, 0x0

    goto :goto_0

    .line 318
    :cond_1
    new-instance v0, Lorg/htmlparser/util/ParserException;

    const-string v1, "illegal state for exit"

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-wide/16 v19, 0x1

    const-string v7, "illegal character encountered: "

    const-string v6, "\')"

    const-string v4, " (\'"

    packed-switch v8, :pswitch_data_0

    .line 448
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :pswitch_0
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mTrailer:[C

    aget-char v5, v0, v13

    if-ne v3, v5, :cond_3

    add-int/lit8 v13, v13, 0x1

    .line 441
    array-length v0, v0

    if-lt v13, v0, :cond_f

    .line 444
    sget v8, Lorg/htmlparser/scanners/ScriptDecoder;->LAST_STATE:I

    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v13, 0x0

    goto :goto_0

    .line 440
    :cond_3
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :pswitch_1
    aput-char v3, v1, v12

    add-int/lit8 v12, v12, 0x1

    .line 425
    array-length v0, v1

    if-lt v12, v0, :cond_f

    .line 427
    invoke-static {v1}, Lorg/htmlparser/scanners/ScriptDecoder;->decodeBase64([C)J

    move-result-wide v3

    cmp-long v0, v3, v14

    if-nez v0, :cond_4

    const/4 v8, 0x7

    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v12, 0x0

    const-wide/16 v14, 0x0

    goto/16 :goto_0

    .line 429
    :cond_4
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "incorrect checksum, expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", calculated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 402
    :goto_1
    sget-object v7, Lorg/htmlparser/scanners/ScriptDecoder;->mEscapes:[C

    array-length v8, v7

    if-lt v0, v8, :cond_7

    if-eqz v5, :cond_6

    .line 410
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    int-to-long v3, v3

    add-long/2addr v14, v3

    add-int/lit8 v16, v16, 0x1

    sub-long v17, v17, v19

    const-wide/16 v3, 0x0

    cmp-long v0, v3, v17

    if-nez v0, :cond_5

    goto/16 :goto_4

    :cond_5
    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v8, 0x4

    goto/16 :goto_0

    .line 409
    :cond_6
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected escape character: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_7
    aget-char v7, v7, v0

    if-ne v3, v7, :cond_8

    .line 406
    sget-object v3, Lorg/htmlparser/scanners/ScriptDecoder;->mEscaped:[C

    aget-char v3, v3, v0

    const/4 v5, 0x1

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_3
    const/16 v5, 0x40

    if-ne v5, v3, :cond_9

    const/4 v0, 0x5

    const/4 v8, 0x5

    goto :goto_3

    :cond_9
    const/16 v5, 0x80

    if-ge v0, v5, :cond_c

    const/16 v5, 0x9

    if-ne v0, v5, :cond_a

    const/4 v7, 0x0

    goto :goto_2

    :cond_a
    const/16 v5, 0x20

    if-lt v0, v5, :cond_b

    add-int/lit8 v7, v0, -0x1f

    .line 384
    :goto_2
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mLookupTable:[[C

    sget-object v3, Lorg/htmlparser/scanners/ScriptDecoder;->mEncodingIndex:[B

    rem-int/lit8 v4, v16, 0x40

    aget-byte v3, v3, v4

    aget-object v0, v0, v3

    aget-char v0, v0, v7

    .line 385
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    int-to-long v3, v0

    add-long/2addr v14, v3

    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 383
    :cond_b
    new-instance v1, Lorg/htmlparser/util/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "illegal encoded character: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 390
    :cond_c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_3
    sub-long v17, v17, v19

    const-wide/16 v3, 0x0

    cmp-long v0, v3, v17

    if-nez v0, :cond_f

    :goto_4
    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v8, 0x6

    const/16 v16, 0x0

    goto/16 :goto_0

    .line 360
    :pswitch_4
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mPrefix:[C

    aget-char v5, v0, v11

    if-ne v3, v5, :cond_d

    add-int/lit8 v11, v11, 0x1

    .line 364
    array-length v0, v0

    if-lt v11, v0, :cond_f

    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v8, 0x4

    const/4 v11, 0x0

    goto/16 :goto_0

    .line 363
    :cond_d
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :pswitch_5
    aput-char v3, v1, v10

    add-int/lit8 v10, v10, 0x1

    .line 349
    array-length v0, v1

    if-lt v10, v0, :cond_f

    .line 351
    invoke-static {v1}, Lorg/htmlparser/scanners/ScriptDecoder;->decodeBase64([C)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v5, v3

    if-gtz v0, :cond_e

    const/4 v8, 0x3

    move-wide/from16 v17, v3

    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v10, 0x0

    goto/16 :goto_0

    .line 353
    :cond_e
    new-instance v0, Lorg/htmlparser/util/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    :goto_5
    const/4 v0, 0x6

    const/4 v6, 0x1

    goto/16 :goto_0

    :pswitch_6
    const-wide/16 v5, 0x0

    .line 325
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mLeader:[C

    aget-char v4, v0, v9

    if-ne v3, v4, :cond_10

    add-int/lit8 v9, v9, 0x1

    .line 328
    array-length v0, v0

    if-ne v9, v0, :cond_f

    const/4 v8, 0x2

    const/4 v0, 0x6

    const/4 v6, 0x1

    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_10
    const/4 v0, 0x0

    :goto_6
    if-gtz v9, :cond_11

    .line 342
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 339
    :cond_11
    sget-object v4, Lorg/htmlparser/scanners/ScriptDecoder;->mLeader:[C

    add-int/lit8 v7, v0, 0x1

    aget-char v0, v4, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v9, v9, -0x1

    const/4 v0, 0x1

    add-int/lit8 v4, v7, 0x1

    move v0, v4

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected static decodeBase64([C)J
    .locals 9

    .line 255
    sget-object v0, Lorg/htmlparser/scanners/ScriptDecoder;->mDigits:[I

    const/4 v1, 0x0

    aget-char v1, p0, v1

    aget v1, v0, v1

    const/4 v2, 0x2

    shl-int/2addr v1, v2

    int-to-long v3, v1

    const-wide/16 v5, 0x0

    add-long/2addr v3, v5

    const/4 v1, 0x1

    .line 256
    aget-char v5, p0, v1

    aget v5, v0, v5

    const/4 v6, 0x4

    shr-int/2addr v5, v6

    int-to-long v7, v5

    add-long/2addr v3, v7

    .line 257
    aget-char v1, p0, v1

    aget v1, v0, v1

    and-int/lit8 v1, v1, 0xf

    shl-int/lit8 v1, v1, 0xc

    int-to-long v7, v1

    add-long/2addr v3, v7

    .line 258
    aget-char v1, p0, v2

    aget v1, v0, v1

    shr-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    int-to-long v7, v1

    add-long/2addr v3, v7

    .line 259
    aget-char v1, p0, v2

    aget v1, v0, v1

    const/4 v5, 0x3

    and-int/2addr v1, v5

    shl-int/lit8 v1, v1, 0x16

    int-to-long v7, v1

    add-long/2addr v3, v7

    .line 260
    aget-char v1, p0, v5

    aget v1, v0, v1

    shl-int/lit8 v1, v1, 0x10

    int-to-long v7, v1

    add-long/2addr v3, v7

    .line 261
    aget-char v1, p0, v6

    aget v1, v0, v1

    shl-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x18

    int-to-long v1, v1

    add-long/2addr v3, v1

    const/4 v1, 0x5

    .line 262
    aget-char p0, p0, v1

    aget p0, v0, p0

    shr-int/2addr p0, v6

    shl-int/lit8 p0, p0, 0x18

    int-to-long v0, p0

    add-long/2addr v3, v0

    return-wide v3
.end method
