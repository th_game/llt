.class public Lorg/htmlparser/http/ConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.java"


# static fields
.field private static final BASE64_CHAR_TABLE:[C

.field private static final FOUR_OH_FOUR:[Ljava/lang/String;

.field protected static mDefaultRequestProperties:Ljava/util/Hashtable;

.field protected static mFormat:Ljava/text/SimpleDateFormat;


# instance fields
.field protected mCookieJar:Ljava/util/Hashtable;

.field protected mMonitor:Lorg/htmlparser/http/ConnectionMonitor;

.field protected mPassword:Ljava/lang/String;

.field protected mProxyHost:Ljava/lang/String;

.field protected mProxyPassword:Ljava/lang/String;

.field protected mProxyPort:I

.field protected mProxyUser:Ljava/lang/String;

.field protected mRedirectionProcessingEnabled:Z

.field protected mRequestProperties:Ljava/util/Hashtable;

.field protected mUser:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 56
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/htmlparser/http/ConnectionManager;->mDefaultRequestProperties:Ljava/util/Hashtable;

    .line 59
    sget-object v0, Lorg/htmlparser/http/ConnectionManager;->mDefaultRequestProperties:Ljava/util/Hashtable;

    const-string v1, "User-Agent"

    const-string v2, "HTMLParser/2.0"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lorg/htmlparser/http/ConnectionManager;->mDefaultRequestProperties:Ljava/util/Hashtable;

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip, deflate"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "The web site you seek cannot be located, but countless more exist"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "You step in the stream, but the water has moved on. This page is not here."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Yesterday the page existed. Today it does not. The internet is like that."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "That page was so big. It might have been very useful. But now it is gone."

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Three things are certain: death, taxes and broken links. Guess which has occured."

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Chaos reigns within. Reflect, repent and enter the correct URL. Order shall return."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Stay the patient course. Of little worth is your ire. The page is not found."

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "A non-existant URL reduces your expensive computer to a simple stone."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Many people have visited that page. Today, you are not one of the lucky ones."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Cutting the wind with a knife. Bookmarking a URL. Both are ephemeral."

    aput-object v2, v0, v1

    .line 67
    sput-object v0, Lorg/htmlparser/http/ConnectionManager;->FOUR_OH_FOUR:[Ljava/lang/String;

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    .line 95
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 93
    sput-object v0, Lorg/htmlparser/http/ConnectionManager;->BASE64_CHAR_TABLE:[C

    .line 152
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd-MMM-yy kk:mm:ss z"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 151
    sput-object v0, Lorg/htmlparser/http/ConnectionManager;->mFormat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 159
    invoke-static {}, Lorg/htmlparser/http/ConnectionManager;->getDefaultRequestProperties()Ljava/util/Hashtable;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/htmlparser/http/ConnectionManager;-><init>(Ljava/util/Hashtable;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 1

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mRequestProperties:Ljava/util/Hashtable;

    const/4 p1, 0x0

    .line 169
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyHost:Ljava/lang/String;

    const/4 v0, 0x0

    .line 170
    iput v0, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPort:I

    .line 171
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyUser:Ljava/lang/String;

    .line 172
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPassword:Ljava/lang/String;

    .line 173
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mUser:Ljava/lang/String;

    .line 174
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mPassword:Ljava/lang/String;

    .line 175
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    .line 176
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mMonitor:Lorg/htmlparser/http/ConnectionMonitor;

    .line 177
    iput-boolean v0, p0, Lorg/htmlparser/http/ConnectionManager;->mRedirectionProcessingEnabled:Z

    return-void
.end method

.method public static final encode([B)Ljava/lang/String;
    .locals 15

    if-eqz p0, :cond_7

    .line 735
    array-length v0, p0

    if-eqz v0, :cond_7

    .line 737
    array-length v0, p0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 738
    div-int/lit8 v2, v0, 0x3

    add-int/2addr v2, v1

    const/4 v3, 0x2

    shl-int/2addr v2, v3

    add-int/lit8 v4, v2, -0x1

    .line 739
    div-int/lit8 v4, v4, 0x4c

    add-int/2addr v2, v4

    .line 741
    new-array v4, v2, [C

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    if-le v6, v0, :cond_0

    .line 775
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v4}, Ljava/lang/String;-><init>([C)V

    goto :goto_4

    :cond_0
    sub-int v9, v0, v6

    if-le v9, v1, :cond_1

    const/4 v10, 0x2

    goto :goto_1

    :cond_1
    move v10, v9

    :goto_1
    const/16 v11, 0x10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x10

    :goto_2
    if-le v11, v10, :cond_5

    add-int/lit8 v10, v7, 0x1

    .line 760
    sget-object v11, Lorg/htmlparser/http/ConnectionManager;->BASE64_CHAR_TABLE:[C

    ushr-int/lit8 v13, v12, 0x12

    and-int/lit8 v13, v13, 0x3f

    aget-char v13, v11, v13

    aput-char v13, v4, v7

    add-int/lit8 v7, v10, 0x1

    ushr-int/lit8 v13, v12, 0xc

    and-int/lit8 v13, v13, 0x3f

    .line 761
    aget-char v13, v11, v13

    aput-char v13, v4, v10

    add-int/lit8 v10, v7, 0x1

    const/16 v13, 0x3d

    if-lez v9, :cond_2

    ushr-int/lit8 v14, v12, 0x6

    and-int/lit8 v14, v14, 0x3f

    .line 763
    aget-char v11, v11, v14

    goto :goto_3

    :cond_2
    const/16 v11, 0x3d

    .line 762
    :goto_3
    aput-char v11, v4, v7

    add-int/lit8 v7, v10, 0x1

    if-le v9, v1, :cond_3

    .line 766
    sget-object v9, Lorg/htmlparser/http/ConnectionManager;->BASE64_CHAR_TABLE:[C

    and-int/lit8 v11, v12, 0x3f

    aget-char v13, v9, v11

    .line 765
    :cond_3
    aput-char v13, v4, v10

    sub-int v9, v7, v8

    .line 769
    rem-int/lit8 v9, v9, 0x4c

    if-nez v9, :cond_4

    if-ge v7, v2, :cond_4

    add-int/lit8 v9, v7, 0x1

    const/16 v10, 0xa

    .line 771
    aput-char v10, v4, v7

    add-int/lit8 v8, v8, 0x1

    move v7, v9

    :cond_4
    add-int/lit8 v6, v6, 0x3

    goto :goto_0

    :cond_5
    add-int v14, v6, v11

    .line 754
    aget-byte v14, p0, v14

    if-gez v14, :cond_6

    add-int/lit16 v14, v14, 0x100

    :cond_6
    shl-int/2addr v14, v13

    add-int/2addr v12, v14

    add-int/lit8 v13, v13, -0x8

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_7
    const-string p0, ""

    :goto_4
    return-object p0
.end method

.method public static getDefaultRequestProperties()Ljava/util/Hashtable;
    .locals 1

    .line 195
    sget-object v0, Lorg/htmlparser/http/ConnectionManager;->mDefaultRequestProperties:Ljava/util/Hashtable;

    return-object v0
.end method

.method public static setDefaultRequestProperties(Ljava/util/Hashtable;)V
    .locals 0

    .line 255
    sput-object p0, Lorg/htmlparser/http/ConnectionManager;->mDefaultRequestProperties:Ljava/util/Hashtable;

    return-void
.end method


# virtual methods
.method protected addCookies(Ljava/util/Vector;Ljava/lang/String;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 4

    if-eqz p1, :cond_4

    .line 943
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    const/4 v1, 0x0

    .line 944
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    goto :goto_2

    .line 946
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/htmlparser/http/Cookie;

    .line 947
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 948
    invoke-virtual {v3, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 950
    invoke-virtual {p1, v1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 954
    :cond_1
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez p3, :cond_2

    .line 957
    new-instance p3, Ljava/util/Vector;

    invoke-direct {p3}, Ljava/util/Vector;-><init>()V

    .line 958
    :cond_2
    invoke-virtual {p3, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    return-object p3
.end method

.method public addCookies(Ljava/net/URLConnection;)V
    .locals 6

    .line 901
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    .line 905
    invoke-virtual {p1}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v1

    .line 906
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 907
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 908
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    const-string v1, "/"

    :cond_0
    if-eqz v2, :cond_2

    .line 912
    iget-object v3, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    invoke-virtual {p0, v3, v1, v0}, Lorg/htmlparser/http/ConnectionManager;->addCookies(Ljava/util/Vector;Ljava/lang/String;Ljava/util/Vector;)Ljava/util/Vector;

    move-result-object v0

    .line 913
    invoke-virtual {p0, v2}, Lorg/htmlparser/http/ConnectionManager;->getDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 915
    iget-object v2, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    invoke-virtual {p0, v2, v1, v0}, Lorg/htmlparser/http/ConnectionManager;->addCookies(Ljava/util/Vector;Ljava/lang/String;Ljava/util/Vector;)Ljava/util/Vector;

    move-result-object v0

    goto :goto_0

    .line 919
    :cond_1
    iget-object v3, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    invoke-virtual {p0, v2, v1, v0}, Lorg/htmlparser/http/ConnectionManager;->addCookies(Ljava/util/Vector;Ljava/lang/String;Ljava/util/Vector;)Ljava/util/Vector;

    move-result-object v0

    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 924
    invoke-virtual {p0, v0}, Lorg/htmlparser/http/ConnectionManager;->generateCookieProperty(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cookie"

    .line 923
    invoke-virtual {p1, v1, v0}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public fixSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/16 v0, 0x20

    .line 797
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v2, v1, :cond_2

    .line 800
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 801
    new-instance v3, Ljava/lang/StringBuffer;

    mul-int/lit8 v4, v2, 0x3

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v4, 0x0

    .line 802
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    if-lt v1, v2, :cond_0

    .line 811
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 805
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v0, :cond_1

    const-string v4, "%20"

    .line 807
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 809
    :cond_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    return-object p1
.end method

.method protected generateCookieProperty(Ljava/util/Vector;)Ljava/lang/String;
    .locals 7

    .line 1026
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1028
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v4

    if-lt v2, v4, :cond_9

    const-string v4, "\""

    if-eqz v3, :cond_0

    const-string v2, "$Version=\""

    .line 1033
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1034
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1035
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1037
    :cond_0
    :goto_1
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 1067
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result p1

    if-eqz p1, :cond_1

    .line 1068
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_1
    const/4 p1, 0x0

    :goto_2
    return-object p1

    .line 1039
    :cond_2
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/htmlparser/http/Cookie;

    .line 1040
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "; "

    .line 1041
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1042
    :cond_3
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1043
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_3

    :cond_4
    const-string v6, "="

    :goto_3
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v3, :cond_5

    .line 1045
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1046
    :cond_5
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v3, :cond_6

    .line 1048
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_6
    if-eqz v3, :cond_8

    .line 1051
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 1052
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "; $Path=\""

    .line 1054
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1055
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1056
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1058
    :cond_7
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 1059
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "; $Domain=\""

    .line 1061
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1062
    invoke-virtual {v2}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1063
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 1030
    :cond_9
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/htmlparser/http/Cookie;

    invoke-virtual {v4}, Lorg/htmlparser/http/Cookie;->getVersion()I

    move-result v4

    .line 1029
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public getCookieProcessingEnabled()Z
    .locals 1

    .line 406
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected getDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 984
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, "."

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v1

    const/4 v2, 0x3

    if-gt v2, v1, :cond_3

    .line 990
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    if-eqz v3, :cond_0

    goto :goto_1

    .line 994
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 995
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_1

    const/16 v5, 0x2e

    if-eq v4, v5, :cond_1

    const/4 v3, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 1001
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 1002
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1003
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method protected getLocation(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 531
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 532
    :cond_0
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, "Location"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public getMonitor()Lorg/htmlparser/http/ConnectionMonitor;
    .locals 1

    .line 482
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mMonitor:Lorg/htmlparser/http/ConnectionMonitor;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .line 388
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyHost()Ljava/lang/String;
    .locals 1

    .line 291
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyHost:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPassword()Ljava/lang/String;
    .locals 1

    .line 349
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPort()I
    .locals 1

    .line 310
    iget v0, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPort:I

    return v0
.end method

.method public getProxyUser()Ljava/lang/String;
    .locals 1

    .line 330
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyUser:Ljava/lang/String;

    return-object v0
.end method

.method public getRedirectionProcessingEnabled()Z
    .locals 1

    .line 501
    iget-boolean v0, p0, Lorg/htmlparser/http/ConnectionManager;->mRedirectionProcessingEnabled:Z

    return v0
.end method

.method public getRequestProperties()Ljava/util/Hashtable;
    .locals 1

    .line 266
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mRequestProperties:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    .line 369
    iget-object v0, p0, Lorg/htmlparser/http/ConnectionManager;->mUser:Ljava/lang/String;

    return-object v0
.end method

.method public openConnection(Ljava/lang/String;)Ljava/net/URLConnection;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const-string v0, "/"

    const-string v1, "Error in opening a connection to "

    .line 839
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-virtual {p0, p1}, Lorg/htmlparser/http/ConnectionManager;->fixSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 840
    invoke-virtual {p0, v2}, Lorg/htmlparser/http/ConnectionManager;->openConnection(Ljava/net/URL;)Ljava/net/URLConnection;

    move-result-object p1
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 846
    :catch_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 847
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    .line 848
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x10

    .line 849
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    .line 848
    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v4, "file://localhost"

    .line 850
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 851
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 852
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 853
    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 854
    new-instance v0, Ljava/net/URL;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/htmlparser/http/ConnectionManager;->fixSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 855
    invoke-virtual {p0, v0}, Lorg/htmlparser/http/ConnectionManager;->openConnection(Ljava/net/URL;)Ljava/net/URLConnection;

    move-result-object p1
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-object p1

    :catch_1
    move-exception v0

    .line 865
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 866
    new-instance v1, Lorg/htmlparser/util/ParserException;

    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 867
    throw v1

    :catch_2
    move-exception v0

    .line 859
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 860
    new-instance v1, Lorg/htmlparser/util/ParserException;

    invoke-direct {v1, p1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 861
    throw v1
.end method

.method public openConnection(Ljava/net/URL;)Ljava/net/URLConnection;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v2, "http.proxyPort"

    const-string v3, "http.proxyHost"

    const-string v4, "proxyPort"

    const-string v5, "proxyHost"

    const-string v6, "proxySet"

    const/4 v0, 0x0

    move-object/from16 v8, p1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 576
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyHost()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPort()I

    move-result v15

    if-eqz v15, :cond_0

    .line 578
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v15

    const-string v7, "true"

    .line 579
    invoke-virtual {v15, v6, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_c

    .line 580
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v15, v5, v9}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 582
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPort()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    .line 581
    invoke-virtual {v15, v4, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 585
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyHost()Ljava/lang/String;

    move-result-object v12

    .line 584
    invoke-virtual {v15, v3, v12}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 587
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPort()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 586
    invoke-virtual {v15, v2, v13}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 588
    :try_start_5
    invoke-static {v15}, Ljava/lang/System;->setProperties(Ljava/util/Properties;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v9

    move-object v9, v7

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v15, v2

    move-object/from16 p1, v8

    move-object v14, v13

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v15, v2

    move-object/from16 p1, v8

    :goto_1
    move-object v13, v12

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v15, v2

    move-object/from16 p1, v8

    :goto_2
    move-object v12, v11

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v15, v2

    move-object/from16 p1, v8

    :goto_3
    move-object v11, v9

    move-object v9, v7

    goto/16 :goto_13

    :catchall_4
    move-exception v0

    move-object v15, v2

    move-object v9, v7

    goto/16 :goto_12

    .line 593
    :cond_0
    :goto_4
    :try_start_6
    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    .line 594
    instance-of v15, v7, Ljava/net/HttpURLConnection;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_c

    if-eqz v15, :cond_7

    .line 596
    :try_start_7
    move-object v15, v7

    check-cast v15, Ljava/net/HttpURLConnection;

    .line 598
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getRedirectionProcessingEnabled()Z

    move-result v16
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_9

    if-eqz v16, :cond_1

    .line 599
    :try_start_8
    invoke-virtual {v15, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_c

    .line 602
    :cond_1
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getRequestProperties()Ljava/util/Hashtable;

    move-result-object v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    if-eqz v0, :cond_3

    .line 604
    :try_start_a
    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v17

    .line 605
    :goto_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v18

    if-nez v18, :cond_2

    goto :goto_6

    .line 607
    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v18
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    move-object/from16 p1, v8

    :try_start_b
    move-object/from16 v8, v18

    check-cast v8, Ljava/lang/String;

    .line 608
    invoke-virtual {v0, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/String;

    .line 609
    invoke-virtual {v7, v8, v0}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    move-object/from16 v8, p1

    move-object/from16 v0, v19

    goto :goto_5

    :catchall_5
    move-exception v0

    goto/16 :goto_9

    :catchall_6
    move-exception v0

    move-object/from16 p1, v8

    goto/16 :goto_9

    :cond_3
    :goto_6
    move-object/from16 p1, v8

    .line 613
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyUser()Ljava/lang/String;

    move-result-object v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    const-string v8, "ISO-8859-1"

    move-object/from16 v17, v2

    const-string v2, ":"

    if-eqz v0, :cond_4

    .line 614
    :try_start_d
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPassword()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyUser()Ljava/lang/String;

    move-result-object v18
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    move-object/from16 v19, v14

    :try_start_e
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v0, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPassword()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 617
    invoke-virtual {v0, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lorg/htmlparser/http/ConnectionManager;->encode([B)Ljava/lang/String;

    move-result-object v0

    const-string v14, "Proxy-Authorization"

    .line 618
    invoke-virtual {v7, v14, v0}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :catchall_7
    move-exception v0

    move-object/from16 v19, v14

    move-object/from16 v15, v17

    goto/16 :goto_13

    :cond_4
    move-object/from16 v19, v14

    .line 622
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getUser()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getPassword()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getUser()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v0, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 625
    invoke-virtual {v0, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lorg/htmlparser/http/ConnectionManager;->encode([B)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Authorization"

    .line 627
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v14, "Basic "

    invoke-direct {v8, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 626
    invoke-virtual {v7, v2, v0}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getCookieProcessingEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 632
    invoke-virtual {v1, v7}, Lorg/htmlparser/http/ConnectionManager;->addCookies(Ljava/net/URLConnection;)V

    .line 634
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getMonitor()Lorg/htmlparser/http/ConnectionMonitor;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 635
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getMonitor()Lorg/htmlparser/http/ConnectionMonitor;

    move-result-object v0

    invoke-interface {v0, v15}, Lorg/htmlparser/http/ConnectionMonitor;->preConnect(Ljava/net/HttpURLConnection;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    goto :goto_a

    :catchall_8
    move-exception v0

    goto :goto_8

    :catchall_9
    move-exception v0

    move-object/from16 p1, v8

    :goto_8
    move-object/from16 v19, v14

    :goto_9
    move-object v15, v2

    goto/16 :goto_13

    :cond_7
    move-object/from16 v17, v2

    move-object/from16 p1, v8

    move-object/from16 v19, v14

    const/4 v15, 0x0

    .line 642
    :cond_8
    :goto_a
    :try_start_f
    invoke-virtual {v7}, Ljava/net/URLConnection;->connect()V

    if-eqz v15, :cond_b

    .line 646
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getMonitor()Lorg/htmlparser/http/ConnectionMonitor;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 647
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getMonitor()Lorg/htmlparser/http/ConnectionMonitor;

    move-result-object v0

    invoke-interface {v0, v15}, Lorg/htmlparser/http/ConnectionMonitor;->postConnect(Ljava/net/HttpURLConnection;)V

    .line 649
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getCookieProcessingEnabled()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 650
    invoke-virtual {v1, v7}, Lorg/htmlparser/http/ConnectionManager;->parseCookies(Ljava/net/URLConnection;)V

    .line 652
    :cond_a
    invoke-virtual {v15}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/4 v2, 0x3

    .line 653
    div-int/lit8 v0, v0, 0x64

    if-ne v2, v0, :cond_b

    const/16 v0, 0x14

    if-ge v10, v0, :cond_b

    .line 654
    invoke-virtual {v1, v15}, Lorg/htmlparser/http/ConnectionManager;->getLocation(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 656
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/net/UnknownHostException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_a

    add-int/lit8 v10, v10, 0x1

    const/4 v0, 0x1

    move-object v8, v2

    goto :goto_b

    :cond_b
    move-object/from16 v8, p1

    const/4 v0, 0x0

    .line 674
    :goto_b
    :try_start_10
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyHost()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPort()I

    move-result v2

    if-eqz v2, :cond_11

    .line 676
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v2

    if-eqz v9, :cond_c

    .line 678
    invoke-virtual {v2, v6, v9}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 680
    :cond_c
    invoke-virtual {v2, v6}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_c
    if-eqz v11, :cond_d

    .line 682
    invoke-virtual {v2, v5, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 684
    :cond_d
    invoke-virtual {v2, v5}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_d
    if-eqz v12, :cond_e

    .line 686
    invoke-virtual {v2, v4, v12}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 688
    :cond_e
    invoke-virtual {v2, v4}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_e
    if-eqz v13, :cond_f

    .line 690
    invoke-virtual {v2, v3, v13}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 692
    :cond_f
    invoke-virtual {v2, v3}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_f
    if-eqz v19, :cond_10

    move-object/from16 v15, v17

    move-object/from16 v14, v19

    .line 694
    invoke-virtual {v2, v15, v14}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    :cond_10
    move-object/from16 v15, v17

    move-object/from16 v14, v19

    .line 696
    invoke-virtual {v2, v15}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    :goto_10
    invoke-static {v2}, Ljava/lang/System;->setProperties(Ljava/util/Properties;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0

    goto :goto_11

    :cond_11
    move-object/from16 v15, v17

    move-object/from16 v14, v19

    :goto_11
    if-nez v0, :cond_12

    return-object v7

    :cond_12
    move-object v2, v15

    const/4 v0, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_19

    :catchall_a
    move-exception v0

    move-object/from16 v15, v17

    move-object/from16 v14, v19

    goto :goto_13

    :catch_1
    move-exception v0

    move-object/from16 v15, v17

    move-object/from16 v14, v19

    .line 669
    :try_start_11
    new-instance v2, Lorg/htmlparser/util/ParserException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v0

    move-object/from16 v15, v17

    move-object/from16 v14, v19

    .line 664
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    sget-object v2, Lorg/htmlparser/http/ConnectionManager;->FOUR_OH_FOUR:[Ljava/lang/String;

    array-length v2, v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_b

    int-to-double v1, v2

    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v7, v7, v1

    double-to-int v1, v7

    .line 665
    :try_start_12
    new-instance v2, Lorg/htmlparser/util/ParserException;

    sget-object v7, Lorg/htmlparser/http/ConnectionManager;->FOUR_OH_FOUR:[Ljava/lang/String;

    aget-object v1, v7, v1

    invoke-direct {v2, v1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_b

    :catchall_b
    move-exception v0

    goto :goto_13

    :catchall_c
    move-exception v0

    move-object v15, v2

    :goto_12
    move-object/from16 p1, v8

    .line 674
    :goto_13
    :try_start_13
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/http/ConnectionManager;->getProxyPort()I

    move-result v1

    if-eqz v1, :cond_18

    .line 676
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v1

    if-eqz v9, :cond_13

    .line 678
    invoke-virtual {v1, v6, v9}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_14

    .line 680
    :cond_13
    invoke-virtual {v1, v6}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_14
    if-eqz v11, :cond_14

    .line 682
    invoke-virtual {v1, v5, v11}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_15

    .line 684
    :cond_14
    invoke-virtual {v1, v5}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_15
    if-eqz v12, :cond_15

    .line 686
    invoke-virtual {v1, v4, v12}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_16

    .line 688
    :cond_15
    invoke-virtual {v1, v4}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_16
    if-eqz v13, :cond_16

    .line 690
    invoke-virtual {v1, v3, v13}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_17

    .line 692
    :cond_16
    invoke-virtual {v1, v3}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_17
    if-eqz v14, :cond_17

    .line 694
    invoke-virtual {v1, v15, v14}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18

    .line 696
    :cond_17
    invoke-virtual {v1, v15}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    :goto_18
    invoke-static {v1}, Ljava/lang/System;->setProperties(Ljava/util/Properties;)V

    .line 699
    :cond_18
    throw v0
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3

    :catch_3
    move-exception v0

    move-object/from16 v8, p1

    .line 703
    :goto_19
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error in opening a connection to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v8}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 705
    new-instance v2, Lorg/htmlparser/util/ParserException;

    invoke-direct {v2, v1, v0}, Lorg/htmlparser/util/ParserException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 706
    goto :goto_1b

    :goto_1a
    throw v2

    :goto_1b
    goto :goto_1a
.end method

.method public parseCookies(Ljava/net/URLConnection;)V
    .locals 11

    const-string v0, "Set-Cookie"

    .line 1089
    invoke-virtual {p1, v0}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1103
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 1104
    new-instance v2, Ljava/util/StringTokenizer;

    const/4 v3, 0x1

    const-string v4, ";,"

    invoke-direct {v2, v0, v4, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    .line 1106
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-nez v5, :cond_0

    goto/16 :goto_3

    .line 1108
    :cond_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, ";"

    .line 1109
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const-string v6, ","

    .line 1111
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0

    :cond_2
    const/16 v6, 0x3d

    .line 1117
    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-ne v7, v6, :cond_4

    if-nez v4, :cond_3

    const-string v6, ""

    move-object v7, v5

    move-object v5, v6

    goto :goto_2

    .line 1130
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    move-object v7, v0

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    .line 1135
    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    .line 1136
    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1137
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    move-object v10, v7

    move-object v7, v5

    move-object v5, v10

    :goto_2
    if-nez v4, :cond_5

    .line 1144
    :try_start_0
    new-instance v4, Lorg/htmlparser/http/Cookie;

    invoke-direct {v4, v5, v7}, Lorg/htmlparser/http/Cookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    invoke-virtual {v1, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    goto/16 :goto_3

    :cond_5
    const-string v8, "expires"

    .line 1156
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1158
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 1159
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 1162
    :try_start_1
    sget-object v8, Lorg/htmlparser/http/ConnectionManager;->mFormat:Ljava/text/SimpleDateFormat;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    .line 1163
    invoke-virtual {v4, v5}, Lorg/htmlparser/http/Cookie;->setExpiryDate(Ljava/util/Date;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 1168
    :catch_1
    invoke-virtual {v4, v0}, Lorg/htmlparser/http/Cookie;->setExpiryDate(Ljava/util/Date;)V

    goto/16 :goto_1

    :cond_6
    const-string v8, "domain"

    .line 1172
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1173
    invoke-virtual {v4, v7}, Lorg/htmlparser/http/Cookie;->setDomain(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    const-string v8, "path"

    .line 1175
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1176
    invoke-virtual {v4, v7}, Lorg/htmlparser/http/Cookie;->setPath(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    const-string v8, "secure"

    .line 1178
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1179
    invoke-virtual {v4, v3}, Lorg/htmlparser/http/Cookie;->setSecure(Z)V

    goto/16 :goto_1

    :cond_9
    const-string v8, "comment"

    .line 1181
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1182
    invoke-virtual {v4, v7}, Lorg/htmlparser/http/Cookie;->setComment(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    const-string v8, "version"

    .line 1184
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1186
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1185
    invoke-virtual {v4, v5}, Lorg/htmlparser/http/Cookie;->setVersion(I)V

    goto/16 :goto_1

    :cond_b
    const-string v8, "max-age"

    .line 1188
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1190
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 1191
    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 1192
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long/2addr v8, v6

    .line 1194
    invoke-virtual {v5, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 1195
    invoke-virtual {v4, v5}, Lorg/htmlparser/http/Cookie;->setExpiryDate(Ljava/util/Date;)V

    goto/16 :goto_1

    .line 1203
    :cond_c
    :try_start_2
    new-instance v4, Lorg/htmlparser/http/Cookie;

    invoke-direct {v4, v5, v7}, Lorg/htmlparser/http/Cookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    invoke-virtual {v1, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 1215
    :goto_3
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    if-eqz v0, :cond_d

    .line 1216
    invoke-virtual {p0, v1, p1}, Lorg/htmlparser/http/ConnectionManager;->saveCookies(Ljava/util/Vector;Ljava/net/URLConnection;)V

    :cond_d
    return-void
.end method

.method protected saveCookies(Ljava/util/Vector;Ljava/net/URLConnection;)V
    .locals 3

    const/4 v0, 0x0

    .line 1230
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    .line 1232
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/htmlparser/http/Cookie;

    .line 1233
    invoke-virtual {v1}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1235
    invoke-virtual {p2}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 1236
    :cond_1
    invoke-virtual {p0, v1, v2}, Lorg/htmlparser/http/ConnectionManager;->setCookie(Lorg/htmlparser/http/Cookie;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setCookie(Lorg/htmlparser/http/Cookie;Ljava/lang/String;)V
    .locals 7

    .line 434
    invoke-virtual {p1}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 435
    invoke-virtual {p1}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object p2

    .line 436
    :cond_0
    invoke-virtual {p1}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 437
    iget-object v1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    if-nez v1, :cond_1

    .line 438
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    .line 439
    :cond_1
    iget-object v1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    invoke-virtual {v1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    if-eqz v1, :cond_5

    const/4 v2, 0x0

    const/4 p2, 0x0

    .line 443
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    const/4 v4, 0x1

    if-lt p2, v3, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    .line 445
    :cond_2
    invoke-virtual {v1, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/http/Cookie;

    .line 446
    invoke-virtual {v3}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 449
    invoke-virtual {v3}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 451
    invoke-virtual {v1, p1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_1

    .line 455
    :cond_3
    invoke-virtual {v3}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 457
    invoke-virtual {v1, p1, p2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :goto_1
    if-nez v4, :cond_6

    .line 466
    invoke-virtual {v1, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 470
    :cond_5
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 471
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 472
    iget-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    invoke-virtual {p1, p2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    :goto_2
    return-void
.end method

.method public setCookieProcessingEnabled(Z)V
    .locals 0

    if-eqz p1, :cond_1

    .line 417
    iget-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/Hashtable;

    invoke-direct {p1}, Ljava/util/Hashtable;-><init>()V

    :cond_0
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 419
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mCookieJar:Ljava/util/Hashtable;

    :goto_0
    return-void
.end method

.method public setMonitor(Lorg/htmlparser/http/ConnectionMonitor;)V
    .locals 0

    .line 491
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mMonitor:Lorg/htmlparser/http/ConnectionMonitor;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    .line 397
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mPassword:Ljava/lang/String;

    return-void
.end method

.method public setProxyHost(Ljava/lang/String;)V
    .locals 0

    .line 301
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyHost:Ljava/lang/String;

    return-void
.end method

.method public setProxyPassword(Ljava/lang/String;)V
    .locals 0

    .line 359
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPassword:Ljava/lang/String;

    return-void
.end method

.method public setProxyPort(I)V
    .locals 0

    .line 320
    iput p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyPort:I

    return-void
.end method

.method public setProxyUser(Ljava/lang/String;)V
    .locals 0

    .line 340
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mProxyUser:Ljava/lang/String;

    return-void
.end method

.method public setRedirectionProcessingEnabled(Z)V
    .locals 0

    .line 516
    iput-boolean p1, p0, Lorg/htmlparser/http/ConnectionManager;->mRedirectionProcessingEnabled:Z

    return-void
.end method

.method public setRequestProperties(Ljava/util/Hashtable;)V
    .locals 0

    .line 282
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mRequestProperties:Ljava/util/Hashtable;

    return-void
.end method

.method public setUser(Ljava/lang/String;)V
    .locals 0

    .line 379
    iput-object p1, p0, Lorg/htmlparser/http/ConnectionManager;->mUser:Ljava/lang/String;

    return-void
.end method
