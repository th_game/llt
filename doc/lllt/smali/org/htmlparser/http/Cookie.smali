.class public Lorg/htmlparser/http/Cookie;
.super Ljava/lang/Object;
.source "Cookie.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/io/Serializable;


# static fields
.field private static final SPECIALS:Ljava/lang/String; = "()<>@,;:\\\"/[]?={} \t"


# instance fields
.field protected mComment:Ljava/lang/String;

.field protected mDomain:Ljava/lang/String;

.field protected mExpiry:Ljava/util/Date;

.field protected mName:Ljava/lang/String;

.field protected mPath:Ljava/lang/String;

.field protected mSecure:Z

.field protected mValue:Ljava/lang/String;

.field protected mVersion:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-direct {p0, p1}, Lorg/htmlparser/http/Cookie;->isToken(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Comment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Discard"

    .line 130
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Domain"

    .line 131
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Expires"

    .line 132
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Max-Age"

    .line 133
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Path"

    .line 134
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Secure"

    .line 135
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Version"

    .line 136
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mName:Ljava/lang/String;

    .line 139
    iput-object p2, p0, Lorg/htmlparser/http/Cookie;->mValue:Ljava/lang/String;

    const/4 p1, 0x0

    .line 140
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mComment:Ljava/lang/String;

    .line 141
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mDomain:Ljava/lang/String;

    .line 142
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mExpiry:Ljava/util/Date;

    const-string p1, "/"

    .line 143
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mPath:Ljava/lang/String;

    const/4 p1, 0x0

    .line 144
    iput-boolean p1, p0, Lorg/htmlparser/http/Cookie;->mSecure:Z

    .line 145
    iput p1, p0, Lorg/htmlparser/http/Cookie;->mVersion:I

    return-void

    .line 137
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "invalid cookie name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private isToken(Ljava/lang/String;)Z
    .locals 6

    .line 348
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :goto_0
    if-ge v2, v0, :cond_3

    if-nez v3, :cond_0

    goto :goto_1

    .line 351
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-lt v4, v5, :cond_1

    const/16 v5, 0x7e

    if-gt v4, v5, :cond_1

    const-string v5, "()<>@,;:\\\"/[]?={} \t"

    .line 352
    invoke-virtual {v5, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    :cond_1
    const/4 v3, 0x0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return v3
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .line 367
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 371
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .line 168
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mComment:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .line 194
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryDate()Ljava/util/Date;
    .locals 1

    .line 220
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mExpiry:Ljava/util/Date;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 278
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 244
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSecure()Z
    .locals 1

    .line 268
    iget-boolean v0, p0, Lorg/htmlparser/http/Cookie;->mSecure:Z

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 304
    iget-object v0, p0, Lorg/htmlparser/http/Cookie;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .line 318
    iget v0, p0, Lorg/htmlparser/http/Cookie;->mVersion:I

    return v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mComment:Ljava/lang/String;

    return-void
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 0

    .line 184
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mDomain:Ljava/lang/String;

    return-void
.end method

.method public setExpiryDate(Ljava/util/Date;)V
    .locals 0

    .line 208
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mExpiry:Ljava/util/Date;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mPath:Ljava/lang/String;

    return-void
.end method

.method public setSecure(Z)V
    .locals 0

    .line 257
    iput-boolean p1, p0, Lorg/htmlparser/http/Cookie;->mSecure:Z

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lorg/htmlparser/http/Cookie;->mValue:Ljava/lang/String;

    return-void
.end method

.method public setVersion(I)V
    .locals 0

    .line 330
    iput p1, p0, Lorg/htmlparser/http/Cookie;->mVersion:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 383
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 384
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "secure "

    .line 385
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 386
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getVersion()I

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "version "

    .line 388
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 389
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, " "

    .line 390
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v1, "cookie"

    .line 392
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, " for "

    .line 395
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 396
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 398
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 399
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 403
    :cond_2
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, " (path "

    .line 405
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ")"

    .line 407
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    :goto_0
    const-string v1, ": "

    .line 410
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 411
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 412
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    const-string v2, "="

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 413
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x28

    if-le v1, v2, :cond_5

    .line 415
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getValue()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "..."

    .line 416
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 419
    :cond_5
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 420
    :goto_2
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getComment()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    const-string v1, " // "

    .line 422
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 423
    invoke-virtual {p0}, Lorg/htmlparser/http/Cookie;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 426
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
