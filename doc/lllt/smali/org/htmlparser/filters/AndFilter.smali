.class public Lorg/htmlparser/filters/AndFilter;
.super Ljava/lang/Object;
.source "AndFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mPredicates:[Lorg/htmlparser/NodeFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0, v0}, Lorg/htmlparser/filters/AndFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V
    .locals 2

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/htmlparser/NodeFilter;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 66
    invoke-virtual {p0, v0}, Lorg/htmlparser/filters/AndFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>([Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/AndFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 114
    iget-object v3, p0, Lorg/htmlparser/filters/AndFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    array-length v4, v3

    if-lt v2, v4, :cond_0

    goto :goto_1

    .line 115
    :cond_0
    aget-object v3, v3, v2

    invoke-interface {v3, p1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method public getPredicates()[Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 84
    iget-object v0, p0, Lorg/htmlparser/filters/AndFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public setPredicates([Lorg/htmlparser/NodeFilter;)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Lorg/htmlparser/NodeFilter;

    .line 95
    :cond_0
    iput-object p1, p0, Lorg/htmlparser/filters/AndFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    return-void
.end method
