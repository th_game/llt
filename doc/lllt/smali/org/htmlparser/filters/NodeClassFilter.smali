.class public Lorg/htmlparser/filters/NodeClassFilter;
.super Ljava/lang/Object;
.source "NodeClassFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mClass:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 46
    const-class v0, Lorg/htmlparser/tags/Html;

    invoke-direct {p0, v0}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/htmlparser/filters/NodeClassFilter;->mClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 85
    iget-object v0, p0, Lorg/htmlparser/filters/NodeClassFilter;->mClass:Ljava/lang/Class;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getMatchClass()Ljava/lang/Class;
    .locals 1

    .line 64
    iget-object v0, p0, Lorg/htmlparser/filters/NodeClassFilter;->mClass:Ljava/lang/Class;

    return-object v0
.end method

.method public setMatchClass(Ljava/lang/Class;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lorg/htmlparser/filters/NodeClassFilter;->mClass:Ljava/lang/Class;

    return-void
.end method
