.class public Lorg/htmlparser/filters/HasChildFilter;
.super Ljava/lang/Object;
.source "HasChildFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mChildFilter:Lorg/htmlparser/NodeFilter;

.field protected mRecursive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0}, Lorg/htmlparser/filters/HasChildFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;)V
    .locals 1

    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/filters/HasChildFilter;-><init>(Lorg/htmlparser/NodeFilter;Z)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;Z)V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/HasChildFilter;->setChildFilter(Lorg/htmlparser/NodeFilter;)V

    .line 87
    invoke-virtual {p0, p2}, Lorg/htmlparser/filters/HasChildFilter;->setRecursive(Z)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 6

    .line 139
    instance-of v0, p1, Lorg/htmlparser/tags/CompositeTag;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 141
    check-cast p1, Lorg/htmlparser/tags/CompositeTag;

    .line 142
    invoke-virtual {p1}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object p1

    if-eqz p1, :cond_6

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-nez v0, :cond_2

    .line 145
    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-lt v2, v4, :cond_0

    goto :goto_1

    .line 146
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasChildFilter;->getChildFilter()Lorg/htmlparser/NodeFilter;

    move-result-object v4

    invoke-virtual {p1, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-nez v0, :cond_5

    .line 150
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasChildFilter;->getRecursive()Z

    move-result v2

    if-eqz v2, :cond_5

    move v1, v0

    const/4 v0, 0x0

    :goto_2
    if-nez v1, :cond_6

    .line 151
    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    goto :goto_3

    .line 152
    :cond_3
    invoke-virtual {p1, v0}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/htmlparser/filters/HasChildFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v1, v0

    :cond_6
    :goto_3
    return v1
.end method

.method public getChildFilter()Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 95
    iget-object v0, p0, Lorg/htmlparser/filters/HasChildFilter;->mChildFilter:Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public getRecursive()Z
    .locals 1

    .line 114
    iget-boolean v0, p0, Lorg/htmlparser/filters/HasChildFilter;->mRecursive:Z

    return v0
.end method

.method public setChildFilter(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lorg/htmlparser/filters/HasChildFilter;->mChildFilter:Lorg/htmlparser/NodeFilter;

    return-void
.end method

.method public setRecursive(Z)V
    .locals 0

    .line 123
    iput-boolean p1, p0, Lorg/htmlparser/filters/HasChildFilter;->mRecursive:Z

    return-void
.end method
