.class Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;
.super Ljava/lang/Object;
.source "CssSelectorNodeFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/htmlparser/filters/CssSelectorNodeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AttribMatchFilter"
.end annotation


# instance fields
.field private attrib:Ljava/lang/String;

.field private rel:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p2

    iput-object p2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;->rel:Ljava/util/regex/Pattern;

    .line 397
    iput-object p1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;->attrib:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 3

    .line 402
    instance-of v0, p1, Lorg/htmlparser/Tag;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    check-cast p1, Lorg/htmlparser/Tag;

    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;->attrib:Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 403
    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;->rel:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    .line 404
    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;->attrib:Ljava/lang/String;

    invoke-interface {p1, v2}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result p1

    if-nez p1, :cond_0

    return v1

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method
