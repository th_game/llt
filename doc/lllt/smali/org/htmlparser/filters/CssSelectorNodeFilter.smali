.class public Lorg/htmlparser/filters/CssSelectorNodeFilter;
.super Ljava/lang/Object;
.source "CssSelectorNodeFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;,
        Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;,
        Lorg/htmlparser/filters/CssSelectorNodeFilter$HasAncestorFilter;,
        Lorg/htmlparser/filters/CssSelectorNodeFilter$YesFilter;
    }
.end annotation


# static fields
.field private static final COMBINATOR:I = 0x5

.field private static final COMMA:I = 0x7

.field private static final COMMENT:I = 0x1

.field private static final DELIM:I = 0x6

.field private static final NAME:I = 0x4

.field private static final QUOTEDSTRING:I = 0x2

.field private static final RELATION:I = 0x3

.field private static tokens:Ljava/util/regex/Pattern;


# instance fields
.field private m:Ljava/util/regex/Matcher;

.field private therule:Lorg/htmlparser/NodeFilter;

.field private token:Ljava/lang/String;

.field private tokentype:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "(/\\*.*?\\*/) | (   \".*?[^\"]\" | \'.*?[^\']\' | \"\" | \'\' ) | ( [\\~\\*\\$\\^\\|]? = ) | ( [a-zA-Z_\\*](?:[a-zA-Z0-9_-]|\\\\.)* ) | \\s*( [+>~\\s] )\\s* | ( [\\.\\[\\]\\#\\:)(] ) | ( [\\,] ) | ( . )"

    const/16 v1, 0x26

    .line 54
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 53
    sput-object v0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokens:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 113
    iput-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    const/4 v1, 0x0

    .line 114
    iput v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    .line 115
    iput-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    .line 123
    sget-object v0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokens:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    .line 124
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 125
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parse()Lorg/htmlparser/NodeFilter;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->therule:Lorg/htmlparser/NodeFilter;

    :cond_0
    return-void
.end method

.method private nextToken()Z
    .locals 3

    .line 141
    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    const/4 v1, 0x1

    .line 142
    :goto_0
    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    goto :goto_1

    .line 143
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 145
    iput v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    .line 146
    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->m:Ljava/util/regex/Matcher;

    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 149
    iput v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v1, 0x0

    .line 150
    iput-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    return v0
.end method

.method private parse()Lorg/htmlparser/NodeFilter;
    .locals 3

    const/4 v0, 0x0

    .line 162
    :cond_0
    iget v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    const/4 v2, 0x6

    if-eq v1, v2, :cond_5

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto :goto_2

    .line 187
    :cond_1
    new-instance v1, Lorg/htmlparser/filters/OrFilter;

    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parse()Lorg/htmlparser/NodeFilter;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/htmlparser/filters/OrFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 188
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    :goto_0
    move-object v0, v1

    goto :goto_2

    .line 173
    :cond_2
    iget-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2b

    if-eq v1, v2, :cond_4

    const/16 v2, 0x3e

    if-eq v1, v2, :cond_3

    .line 182
    new-instance v1, Lorg/htmlparser/filters/CssSelectorNodeFilter$HasAncestorFilter;

    invoke-direct {v1, v0}, Lorg/htmlparser/filters/CssSelectorNodeFilter$HasAncestorFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    goto :goto_1

    .line 179
    :cond_3
    new-instance v1, Lorg/htmlparser/filters/HasParentFilter;

    invoke-direct {v1, v0}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    goto :goto_1

    .line 176
    :cond_4
    new-instance v1, Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;

    invoke-direct {v1, v0}, Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 184
    :goto_1
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    goto :goto_0

    :cond_5
    if-nez v0, :cond_6

    .line 168
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parseSimple()Lorg/htmlparser/NodeFilter;

    move-result-object v0

    goto :goto_2

    .line 170
    :cond_6
    new-instance v1, Lorg/htmlparser/filters/AndFilter;

    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parseSimple()Lorg/htmlparser/NodeFilter;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    goto :goto_0

    .line 192
    :goto_2
    iget-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    if-nez v1, :cond_0

    return-object v0
.end method

.method private parseAttributeExp()Lorg/htmlparser/NodeFilter;
    .locals 7

    .line 280
    iget v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-ne v0, v1, :cond_5

    .line 282
    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    .line 283
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    .line 284
    iget-object v3, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    const-string v4, "]"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 285
    new-instance v2, Lorg/htmlparser/filters/HasAttributeFilter;

    invoke-static {v0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 286
    :cond_0
    iget v3, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 288
    iget-object v3, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    .line 289
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    .line 290
    iget v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 291
    iget-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-ne v4, v1, :cond_2

    .line 293
    iget-object v1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v1}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    :goto_0
    const-string v4, "~="

    .line 294
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v5, "\\\\$1"

    const-string v6, "([^a-zA-Z0-9])"

    if-eqz v4, :cond_3

    if-eqz v1, :cond_3

    .line 295
    new-instance v2, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;

    invoke-static {v0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\\b"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 295
    invoke-direct {v2, v0, v1}, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v4, "|="

    .line 298
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    .line 299
    new-instance v2, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;

    invoke-static {v0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\\-[^a-zA-Z0-9]*"

    .line 300
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 299
    invoke-direct {v2, v0, v1}, Lorg/htmlparser/filters/CssSelectorNodeFilter$AttribMatchFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v4, "="

    .line 301
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v1, :cond_5

    .line 302
    new-instance v2, Lorg/htmlparser/filters/HasAttributeFilter;

    invoke-direct {v2, v0, v1}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    .line 309
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    return-object v2

    .line 306
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Syntax error at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    iget v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private parsePseudoClass()Lorg/htmlparser/NodeFilter;
    .locals 2

    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pseudoclasses not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private parseSimple()Lorg/htmlparser/NodeFilter;
    .locals 8

    .line 202
    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_11

    const/4 v0, 0x0

    move-object v2, v1

    const/4 v3, 0x0

    .line 205
    :cond_0
    iget v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_f

    const/4 v6, 0x4

    if-eq v4, v6, :cond_c

    const/4 v7, 0x6

    if-eq v4, v7, :cond_1

    const/4 v3, 0x1

    goto/16 :goto_4

    .line 221
    :cond_1
    iget-object v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x23

    const-string v7, "Syntax error at "

    if-eq v4, v5, :cond_9

    const/16 v5, 0x2e

    if-eq v4, v5, :cond_6

    const/16 v5, 0x3a

    if-eq v4, v5, :cond_4

    const/16 v5, 0x5b

    if-eq v4, v5, :cond_2

    goto/16 :goto_2

    .line 254
    :cond_2
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    if-nez v2, :cond_3

    .line 256
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parseAttributeExp()Lorg/htmlparser/NodeFilter;

    move-result-object v2

    goto/16 :goto_2

    .line 258
    :cond_3
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parseAttributeExp()Lorg/htmlparser/NodeFilter;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    goto :goto_0

    .line 247
    :cond_4
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    if-nez v2, :cond_5

    .line 249
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parsePseudoClass()Lorg/htmlparser/NodeFilter;

    move-result-object v2

    goto/16 :goto_2

    .line 251
    :cond_5
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->parsePseudoClass()Lorg/htmlparser/NodeFilter;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    :goto_0
    move-object v2, v4

    goto :goto_2

    .line 224
    :cond_6
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    .line 225
    iget v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    if-ne v4, v6, :cond_8

    const-string v4, "class"

    if-nez v2, :cond_7

    .line 229
    new-instance v2, Lorg/htmlparser/filters/HasAttributeFilter;

    .line 230
    iget-object v5, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v5}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 229
    invoke-direct {v2, v4, v5}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 232
    :cond_7
    new-instance v5, Lorg/htmlparser/filters/AndFilter;

    new-instance v6, Lorg/htmlparser/filters/HasAttributeFilter;

    .line 233
    iget-object v7, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v7}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-direct {v5, v2, v6}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    goto :goto_1

    .line 226
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 226
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_9
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    .line 237
    iget v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->tokentype:I

    if-ne v4, v6, :cond_b

    const-string v4, "id"

    if-nez v2, :cond_a

    .line 241
    new-instance v2, Lorg/htmlparser/filters/HasAttributeFilter;

    iget-object v5, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v5}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 243
    :cond_a
    new-instance v5, Lorg/htmlparser/filters/AndFilter;

    new-instance v6, Lorg/htmlparser/filters/HasAttributeFilter;

    .line 244
    iget-object v7, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v7}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-direct {v5, v2, v6}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    :goto_1
    move-object v2, v5

    .line 261
    :goto_2
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    goto :goto_4

    .line 238
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_c
    iget-object v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    const-string v5, "*"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 212
    new-instance v2, Lorg/htmlparser/filters/CssSelectorNodeFilter$YesFilter;

    invoke-direct {v2, v1}, Lorg/htmlparser/filters/CssSelectorNodeFilter$YesFilter;-><init>(Lorg/htmlparser/filters/CssSelectorNodeFilter$YesFilter;)V

    goto :goto_3

    :cond_d
    if-nez v2, :cond_e

    .line 214
    new-instance v2, Lorg/htmlparser/filters/TagNameFilter;

    iget-object v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v4}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 216
    :cond_e
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    new-instance v5, Lorg/htmlparser/filters/TagNameFilter;

    .line 217
    iget-object v6, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    invoke-static {v6}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    .line 216
    invoke-direct {v4, v2, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    move-object v2, v4

    .line 218
    :goto_3
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    goto :goto_4

    .line 208
    :cond_f
    invoke-direct {p0}, Lorg/htmlparser/filters/CssSelectorNodeFilter;->nextToken()Z

    :goto_4
    if-nez v3, :cond_10

    .line 267
    iget-object v4, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->token:Ljava/lang/String;

    if-nez v4, :cond_0

    :cond_10
    move-object v1, v2

    :cond_11
    return-object v1
.end method

.method public static unescape(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 322
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v1, "\\\\(?:([a-fA-F0-9]{2,6})|(.))"

    .line 323
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 325
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_1

    .line 333
    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 335
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 v1, 0x1

    .line 327
    invoke-virtual {p0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 329
    invoke-virtual {p0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    .line 328
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    .line 330
    invoke-virtual {p0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 331
    invoke-virtual {p0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 136
    iget-object v0, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter;->therule:Lorg/htmlparser/NodeFilter;

    invoke-interface {v0, p1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result p1

    return p1
.end method
