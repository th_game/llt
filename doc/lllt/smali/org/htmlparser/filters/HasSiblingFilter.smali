.class public Lorg/htmlparser/filters/HasSiblingFilter;
.super Ljava/lang/Object;
.source "HasSiblingFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mSiblingFilter:Lorg/htmlparser/NodeFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, v0}, Lorg/htmlparser/filters/HasSiblingFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/HasSiblingFilter;->setSiblingFilter(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 5

    .line 98
    instance-of v0, p1, Lorg/htmlparser/Tag;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 103
    invoke-interface {p1}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 106
    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-nez v1, :cond_3

    if-lt v2, v0, :cond_1

    goto :goto_1

    .line 108
    :cond_1
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasSiblingFilter;->getSiblingFilter()Lorg/htmlparser/NodeFilter;

    move-result-object v3

    invoke-virtual {p1, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return v1
.end method

.method public getSiblingFilter()Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 72
    iget-object v0, p0, Lorg/htmlparser/filters/HasSiblingFilter;->mSiblingFilter:Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public setSiblingFilter(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lorg/htmlparser/filters/HasSiblingFilter;->mSiblingFilter:Lorg/htmlparser/NodeFilter;

    return-void
.end method
