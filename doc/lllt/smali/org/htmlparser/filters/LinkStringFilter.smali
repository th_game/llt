.class public Lorg/htmlparser/filters/LinkStringFilter;
.super Ljava/lang/Object;
.source "LinkStringFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mCaseSensitive:Z

.field protected mPattern:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/filters/LinkStringFilter;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lorg/htmlparser/filters/LinkStringFilter;->mPattern:Ljava/lang/String;

    .line 69
    iput-boolean p2, p0, Lorg/htmlparser/filters/LinkStringFilter;->mCaseSensitive:Z

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 3

    .line 83
    const-class v0, Lorg/htmlparser/tags/LinkTag;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 85
    check-cast p1, Lorg/htmlparser/tags/LinkTag;

    invoke-virtual {p1}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object p1

    .line 86
    iget-boolean v0, p0, Lorg/htmlparser/filters/LinkStringFilter;->mCaseSensitive:Z

    const/4 v2, -0x1

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lorg/htmlparser/filters/LinkStringFilter;->mPattern:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-le p1, v2, :cond_1

    goto :goto_0

    .line 93
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/htmlparser/filters/LinkStringFilter;->mPattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-le p1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
