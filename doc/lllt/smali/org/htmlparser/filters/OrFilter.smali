.class public Lorg/htmlparser/filters/OrFilter;
.super Ljava/lang/Object;
.source "OrFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mPredicates:[Lorg/htmlparser/NodeFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, v0}, Lorg/htmlparser/filters/OrFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V
    .locals 2

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/htmlparser/NodeFilter;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 64
    invoke-virtual {p0, v0}, Lorg/htmlparser/filters/OrFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>([Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/OrFilter;->setPredicates([Lorg/htmlparser/NodeFilter;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 112
    iget-object v2, p0, Lorg/htmlparser/filters/OrFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    array-length v3, v2

    if-lt v1, v3, :cond_0

    goto :goto_1

    .line 113
    :cond_0
    aget-object v2, v2, v1

    invoke-interface {v2, p1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method

.method public getPredicates()[Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 82
    iget-object v0, p0, Lorg/htmlparser/filters/OrFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public setPredicates([Lorg/htmlparser/NodeFilter;)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Lorg/htmlparser/NodeFilter;

    .line 93
    :cond_0
    iput-object p1, p0, Lorg/htmlparser/filters/OrFilter;->mPredicates:[Lorg/htmlparser/NodeFilter;

    return-void
.end method
