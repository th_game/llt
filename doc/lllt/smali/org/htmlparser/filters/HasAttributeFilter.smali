.class public Lorg/htmlparser/filters/HasAttributeFilter;
.super Ljava/lang/Object;
.source "HasAttributeFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mAttribute:Ljava/lang/String;

.field protected mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, v0, v1}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/filters/HasAttributeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mAttribute:Ljava/lang/String;

    .line 81
    iput-object p2, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 2

    .line 135
    instance-of v0, p1, Lorg/htmlparser/Tag;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 137
    check-cast p1, Lorg/htmlparser/Tag;

    .line 138
    iget-object v0, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mAttribute:Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/htmlparser/Tag;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    .line 140
    iget-object v0, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mValue:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_1
    return v1
.end method

.method public getAttributeName()Ljava/lang/String;
    .locals 1

    .line 90
    iget-object v0, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mAttribute:Ljava/lang/String;

    return-object v0
.end method

.method public getAttributeValue()Ljava/lang/String;
    .locals 1

    .line 108
    iget-object v0, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setAttributeName(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mAttribute:Ljava/lang/String;

    return-void
.end method

.method public setAttributeValue(Ljava/lang/String;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lorg/htmlparser/filters/HasAttributeFilter;->mValue:Ljava/lang/String;

    return-void
.end method
