.class Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;
.super Ljava/lang/Object;
.source "CssSelectorNodeFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/htmlparser/filters/CssSelectorNodeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdjacentFilter"
.end annotation


# instance fields
.field private sibtest:Lorg/htmlparser/NodeFilter;


# direct methods
.method public constructor <init>(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iput-object p1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;->sibtest:Lorg/htmlparser/NodeFilter;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 4

    .line 370
    invoke-interface {p1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 372
    invoke-interface {p1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    const/4 v2, 0x0

    .line 373
    :goto_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto :goto_1

    .line 374
    :cond_0
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    if-ne v3, p1, :cond_1

    if-lez v2, :cond_1

    .line 375
    iget-object p1, p0, Lorg/htmlparser/filters/CssSelectorNodeFilter$AdjacentFilter;->sibtest:Lorg/htmlparser/NodeFilter;

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result p1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method
