.class public Lorg/htmlparser/filters/HasParentFilter;
.super Ljava/lang/Object;
.source "HasParentFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mParentFilter:Lorg/htmlparser/NodeFilter;

.field protected mRecursive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, v0}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;)V
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;Z)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;Z)V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/HasParentFilter;->setParentFilter(Lorg/htmlparser/NodeFilter;)V

    .line 84
    invoke-virtual {p0, p2}, Lorg/htmlparser/filters/HasParentFilter;->setRecursive(Z)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 2

    .line 140
    instance-of v0, p1, Lorg/htmlparser/Tag;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 143
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasParentFilter;->getParentFilter()Lorg/htmlparser/NodeFilter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasParentFilter;->getParentFilter()Lorg/htmlparser/NodeFilter;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 146
    invoke-virtual {p0}, Lorg/htmlparser/filters/HasParentFilter;->getRecursive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 147
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/HasParentFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0
.end method

.method public getParentFilter()Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 93
    iget-object v0, p0, Lorg/htmlparser/filters/HasParentFilter;->mParentFilter:Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public getRecursive()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lorg/htmlparser/filters/HasParentFilter;->mRecursive:Z

    return v0
.end method

.method public setParentFilter(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lorg/htmlparser/filters/HasParentFilter;->mParentFilter:Lorg/htmlparser/NodeFilter;

    return-void
.end method

.method public setRecursive(Z)V
    .locals 0

    .line 121
    iput-boolean p1, p0, Lorg/htmlparser/filters/HasParentFilter;->mRecursive:Z

    return-void
.end method
