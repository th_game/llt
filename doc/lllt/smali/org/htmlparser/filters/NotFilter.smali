.class public Lorg/htmlparser/filters/NotFilter;
.super Ljava/lang/Object;
.source "NotFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mPredicate:Lorg/htmlparser/NodeFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, v0}, Lorg/htmlparser/filters/NotFilter;->setPredicate(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0, p1}, Lorg/htmlparser/filters/NotFilter;->setPredicate(Lorg/htmlparser/NodeFilter;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 91
    iget-object v0, p0, Lorg/htmlparser/filters/NotFilter;->mPredicate:Lorg/htmlparser/NodeFilter;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getPredicate()Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 67
    iget-object v0, p0, Lorg/htmlparser/filters/NotFilter;->mPredicate:Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public setPredicate(Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lorg/htmlparser/filters/NotFilter;->mPredicate:Lorg/htmlparser/NodeFilter;

    return-void
.end method
