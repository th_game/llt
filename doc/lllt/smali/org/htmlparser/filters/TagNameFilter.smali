.class public Lorg/htmlparser/filters/TagNameFilter;
.super Ljava/lang/Object;
.source "TagNameFilter.java"

# interfaces
.implements Lorg/htmlparser/NodeFilter;


# instance fields
.field protected mName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    .line 53
    invoke-direct {p0, v0}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/filters/TagNameFilter;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/Node;)Z
    .locals 1

    .line 93
    instance-of v0, p1, Lorg/htmlparser/Tag;

    if-eqz v0, :cond_0

    .line 94
    check-cast p1, Lorg/htmlparser/Tag;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->isEndTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/htmlparser/filters/TagNameFilter;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lorg/htmlparser/filters/TagNameFilter;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lorg/htmlparser/filters/TagNameFilter;->mName:Ljava/lang/String;

    return-void
.end method
