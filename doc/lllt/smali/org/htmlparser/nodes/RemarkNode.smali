.class public Lorg/htmlparser/nodes/RemarkNode;
.super Lorg/htmlparser/nodes/AbstractNode;
.source "RemarkNode.java"

# interfaces
.implements Lorg/htmlparser/Remark;


# instance fields
.field protected mText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, v1, v0, v0}, Lorg/htmlparser/nodes/AbstractNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    .line 56
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/RemarkNode;->setText(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;II)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lorg/htmlparser/nodes/AbstractNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    const/4 p1, 0x0

    .line 68
    iput-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 0

    .line 247
    invoke-virtual {p1, p0}, Lorg/htmlparser/visitors/NodeVisitor;->visitRemarkNode(Lorg/htmlparser/Remark;)V

    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .line 81
    iget-object v0, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lorg/htmlparser/nodes/RemarkNode;->getStartPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    .line 84
    invoke-virtual {p0}, Lorg/htmlparser/nodes/RemarkNode;->getEndPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    if-lt v0, v1, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 88
    :cond_0
    iget-object v2, p0, Lorg/htmlparser/nodes/RemarkNode;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {v2, v0, v1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2

    .line 103
    iput-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    const-string v0, "<!--"

    .line 104
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "-->"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    .line 105
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    :cond_0
    const/4 p1, 0x0

    .line 106
    iput p1, p0, Lorg/htmlparser/nodes/RemarkNode;->nodeBegin:I

    .line 107
    iget-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iput p1, p0, Lorg/htmlparser/nodes/RemarkNode;->nodeEnd:I

    return-void
.end method

.method public toHtml(Z)Ljava/lang/String;
    .locals 2

    .line 130
    iget-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    if-nez p1, :cond_0

    .line 131
    iget-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {p0}, Lorg/htmlparser/nodes/RemarkNode;->getStartPosition()I

    move-result v0

    invoke-virtual {p0}, Lorg/htmlparser/nodes/RemarkNode;->getEndPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 134
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, 0x7

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string p1, "<!--"

    .line 135
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    iget-object p1, p0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "-->"

    .line 137
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public toPlainTextString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 16

    move-object/from16 v0, p0

    .line 160
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/RemarkNode;->getStartPosition()I

    move-result v1

    .line 161
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/RemarkNode;->getEndPosition()I

    move-result v2

    .line 162
    new-instance v3, Ljava/lang/StringBuffer;

    sub-int v4, v2, v1

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 163
    iget-object v4, v0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    const-string v5, "..."

    const-string v7, "\\r"

    const-string v8, "\\n"

    const-string v9, "\\t"

    const/16 v11, 0xa

    const/16 v12, 0x9

    const-string v13, "): "

    const-string v14, ","

    const-string v15, "Rem ("

    if-nez v4, :cond_5

    .line 165
    new-instance v4, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/RemarkNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v6

    invoke-direct {v4, v6, v1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 166
    new-instance v6, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/RemarkNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v10

    invoke-direct {v6, v10, v2}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 167
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 169
    invoke-virtual {v3, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 171
    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x4

    .line 172
    invoke-virtual {v4, v1}, Lorg/htmlparser/lexer/Cursor;->setPosition(I)V

    add-int/lit8 v6, v2, -0x3

    .line 174
    :cond_0
    invoke-virtual {v4}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    if-lt v1, v6, :cond_1

    goto/16 :goto_3

    .line 178
    :cond_1
    :try_start_0
    iget-object v1, v0, Lorg/htmlparser/nodes/RemarkNode;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {v1, v4}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v1

    if-eq v1, v12, :cond_4

    if-eq v1, v11, :cond_3

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2

    .line 191
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 185
    :cond_3
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 182
    :cond_4
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 198
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/16 v2, 0x4d

    if-gt v2, v1, :cond_0

    .line 200
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 207
    :cond_5
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 208
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 209
    invoke-virtual {v3, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 211
    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    .line 212
    :goto_1
    iget-object v2, v0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_6

    goto :goto_3

    .line 214
    :cond_6
    iget-object v2, v0, Lorg/htmlparser/nodes/RemarkNode;->mText:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_9

    if-eq v2, v11, :cond_8

    const/16 v4, 0xd

    if-eq v2, v4, :cond_7

    .line 227
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 224
    :cond_7
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_8
    const/16 v4, 0xd

    .line 221
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_9
    const/16 v4, 0xd

    .line 218
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    const/16 v6, 0x4d

    if-gt v6, v2, :cond_a

    .line 231
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 237
    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
