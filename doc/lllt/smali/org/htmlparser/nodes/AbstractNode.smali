.class public abstract Lorg/htmlparser/nodes/AbstractNode;
.super Ljava/lang/Object;
.source "AbstractNode.java"

# interfaces
.implements Lorg/htmlparser/Node;
.implements Ljava/io/Serializable;


# instance fields
.field protected children:Lorg/htmlparser/util/NodeList;

.field protected mPage:Lorg/htmlparser/lexer/Page;

.field protected nodeBegin:I

.field protected nodeEnd:I

.field protected parent:Lorg/htmlparser/Node;


# direct methods
.method public constructor <init>(Lorg/htmlparser/lexer/Page;II)V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->mPage:Lorg/htmlparser/lexer/Page;

    .line 80
    iput p2, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeBegin:I

    .line 81
    iput p3, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeEnd:I

    const/4 p1, 0x0

    .line 82
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->parent:Lorg/htmlparser/Node;

    .line 83
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    return-void
.end method


# virtual methods
.method public abstract accept(Lorg/htmlparser/visitors/NodeVisitor;)V
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 95
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 191
    invoke-interface {p2, p0}, Lorg/htmlparser/NodeFilter;->accept(Lorg/htmlparser/Node;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 192
    invoke-virtual {p1, p0}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    :cond_0
    return-void
.end method

.method public doSemanticAction()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    return-void
.end method

.method public getChildren()Lorg/htmlparser/util/NodeList;
    .locals 1

    .line 282
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    return-object v0
.end method

.method public getEndPosition()I
    .locals 1

    .line 237
    iget v0, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeEnd:I

    return v0
.end method

.method public getFirstChild()Lorg/htmlparser/Node;
    .locals 2

    .line 301
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 303
    :cond_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    .line 305
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0
.end method

.method public getLastChild()Lorg/htmlparser/Node;
    .locals 2

    .line 315
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 317
    :cond_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    .line 320
    :cond_1
    iget-object v1, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0
.end method

.method public getNextSibling()Lorg/htmlparser/Node;
    .locals 6

    .line 360
    invoke-virtual {p0}, Lorg/htmlparser/nodes/AbstractNode;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 363
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 366
    :cond_1
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    return-object v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    const/4 v4, -0x1

    if-lt v3, v2, :cond_3

    const/4 v3, -0x1

    goto :goto_1

    .line 372
    :cond_3
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v5

    if-ne v5, p0, :cond_6

    :goto_1
    if-ne v3, v4, :cond_4

    return-object v1

    :cond_4
    add-int/lit8 v2, v2, -0x1

    if-ne v3, v2, :cond_5

    return-object v1

    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 382
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getPage()Lorg/htmlparser/lexer/Page;
    .locals 1

    .line 201
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->mPage:Lorg/htmlparser/lexer/Page;

    return-object v0
.end method

.method public getParent()Lorg/htmlparser/Node;
    .locals 1

    .line 264
    iget-object v0, p0, Lorg/htmlparser/nodes/AbstractNode;->parent:Lorg/htmlparser/Node;

    return-object v0
.end method

.method public getPreviousSibling()Lorg/htmlparser/Node;
    .locals 6

    .line 330
    invoke-virtual {p0}, Lorg/htmlparser/nodes/AbstractNode;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 333
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/Node;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 336
    :cond_1
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    return-object v1

    :cond_2
    const/4 v3, -0x1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v2, :cond_3

    goto :goto_1

    .line 342
    :cond_3
    invoke-virtual {v0, v4}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v5

    if-ne v5, p0, :cond_5

    move v3, v4

    :goto_1
    const/4 v2, 0x1

    if-ge v3, v2, :cond_4

    return-object v1

    :cond_4
    sub-int/2addr v3, v2

    .line 350
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v0

    return-object v0

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getStartPosition()I
    .locals 1

    .line 219
    iget v0, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeBegin:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setChildren(Lorg/htmlparser/util/NodeList;)V
    .locals 0

    .line 291
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->children:Lorg/htmlparser/util/NodeList;

    return-void
.end method

.method public setEndPosition(I)V
    .locals 0

    .line 246
    iput p1, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeEnd:I

    return-void
.end method

.method public setPage(Lorg/htmlparser/lexer/Page;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->mPage:Lorg/htmlparser/lexer/Page;

    return-void
.end method

.method public setParent(Lorg/htmlparser/Node;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lorg/htmlparser/nodes/AbstractNode;->parent:Lorg/htmlparser/Node;

    return-void
.end method

.method public setStartPosition(I)V
    .locals 0

    .line 228
    iput p1, p0, Lorg/htmlparser/nodes/AbstractNode;->nodeBegin:I

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public toHtml()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 129
    invoke-virtual {p0, v0}, Lorg/htmlparser/nodes/AbstractNode;->toHtml(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract toHtml(Z)Ljava/lang/String;
.end method

.method public abstract toPlainTextString()Ljava/lang/String;
.end method

.method public abstract toString()Ljava/lang/String;
.end method
