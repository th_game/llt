.class public Lorg/htmlparser/nodes/TextNode;
.super Lorg/htmlparser/nodes/AbstractNode;
.source "TextNode.java"

# interfaces
.implements Lorg/htmlparser/Text;


# instance fields
.field protected mText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, v1, v0, v0}, Lorg/htmlparser/nodes/AbstractNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    .line 56
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TextNode;->setText(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;II)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lorg/htmlparser/nodes/AbstractNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    const/4 p1, 0x0

    .line 68
    iput-object p1, p0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 0

    .line 231
    invoke-virtual {p1, p0}, Lorg/htmlparser/visitors/NodeVisitor;->visitStringNode(Lorg/htmlparser/Text;)V

    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TextNode;->toHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isWhiteSpace()Z
    .locals 2

    .line 219
    iget-object v0, p0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 88
    iput p1, p0, Lorg/htmlparser/nodes/TextNode;->nodeBegin:I

    .line 89
    iget-object p1, p0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iput p1, p0, Lorg/htmlparser/nodes/TextNode;->nodeEnd:I

    return-void
.end method

.method public toHtml(Z)Ljava/lang/String;
    .locals 2

    .line 112
    iget-object p1, p0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    if-nez p1, :cond_0

    .line 114
    iget-object p1, p0, Lorg/htmlparser/nodes/TextNode;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TextNode;->getStartPosition()I

    move-result v0

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TextNode;->getEndPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public toPlainTextString()Ljava/lang/String;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TextNode;->toHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 16

    move-object/from16 v0, p0

    .line 135
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/TextNode;->getStartPosition()I

    move-result v1

    .line 136
    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/TextNode;->getEndPosition()I

    move-result v2

    .line 137
    new-instance v3, Ljava/lang/StringBuffer;

    sub-int v4, v2, v1

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 138
    iget-object v4, v0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    const-string v5, "..."

    const-string v7, "\\r"

    const-string v8, "\\n"

    const-string v9, "\\t"

    const/16 v10, 0xd

    const/16 v11, 0xa

    const/16 v12, 0x9

    const-string v13, "): "

    const-string v14, ","

    const-string v15, "Txt ("

    if-nez v4, :cond_5

    .line 140
    new-instance v4, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/TextNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v6

    invoke-direct {v4, v6, v1}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 141
    new-instance v1, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual/range {p0 .. p0}, Lorg/htmlparser/nodes/TextNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v6

    invoke-direct {v1, v6, v2}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 142
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 144
    invoke-virtual {v3, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 146
    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    :cond_0
    invoke-virtual {v4}, Lorg/htmlparser/lexer/Cursor;->getPosition()I

    move-result v1

    if-lt v1, v2, :cond_1

    goto/16 :goto_3

    .line 151
    :cond_1
    :try_start_0
    iget-object v1, v0, Lorg/htmlparser/nodes/TextNode;->mPage:Lorg/htmlparser/lexer/Page;

    invoke-virtual {v1, v4}, Lorg/htmlparser/lexer/Page;->getCharacter(Lorg/htmlparser/lexer/Cursor;)C

    move-result v1

    if-eq v1, v12, :cond_4

    if-eq v1, v11, :cond_3

    if-eq v1, v10, :cond_2

    .line 164
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 158
    :cond_3
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 155
    :cond_4
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 171
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/16 v6, 0x4d

    if-gt v6, v1, :cond_0

    .line 173
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 180
    :cond_5
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 182
    invoke-virtual {v3, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 184
    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    .line 185
    :goto_1
    iget-object v2, v0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_6

    goto :goto_3

    .line 187
    :cond_6
    iget-object v2, v0, Lorg/htmlparser/nodes/TextNode;->mText:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_9

    if-eq v2, v11, :cond_8

    if-eq v2, v10, :cond_7

    .line 200
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 197
    :cond_7
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 194
    :cond_8
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 191
    :cond_9
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 202
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    const/16 v4, 0x4d

    if-gt v4, v2, :cond_a

    .line 204
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
