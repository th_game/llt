.class public Lorg/htmlparser/nodes/TagNode;
.super Lorg/htmlparser/nodes/AbstractNode;
.source "TagNode.java"

# interfaces
.implements Lorg/htmlparser/Tag;


# static fields
.field private static final NONE:[Ljava/lang/String;

.field protected static breakTags:Ljava/util/Hashtable;

.field protected static final mDefaultScanner:Lorg/htmlparser/scanners/Scanner;


# instance fields
.field protected mAttributes:Ljava/util/Vector;

.field private mScanner:Lorg/htmlparser/scanners/Scanner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 57
    sput-object v0, Lorg/htmlparser/nodes/TagNode;->NONE:[Ljava/lang/String;

    .line 67
    new-instance v0, Lorg/htmlparser/scanners/TagScanner;

    invoke-direct {v0}, Lorg/htmlparser/scanners/TagScanner;-><init>()V

    sput-object v0, Lorg/htmlparser/nodes/TagNode;->mDefaultScanner:Lorg/htmlparser/scanners/Scanner;

    .line 83
    new-instance v0, Ljava/util/Hashtable;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    sput-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    .line 84
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "BLOCKQUOTE"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "BODY"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "BR"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "CENTER"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "DD"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "DIR"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "DIV"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "DL"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "DT"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "FORM"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H1"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H2"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H3"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H4"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H5"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "H6"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "HEAD"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "HR"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "HTML"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "ISINDEX"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "LI"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "MENU"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "NOFRAMES"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "OL"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "P"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "PRE"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "TD"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "TH"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "TITLE"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "UL"

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 121
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1, v1, v0}, Lorg/htmlparser/nodes/TagNode;-><init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V
    .locals 0

    .line 134
    invoke-direct {p0, p1, p2, p3}, Lorg/htmlparser/nodes/AbstractNode;-><init>(Lorg/htmlparser/lexer/Page;II)V

    .line 136
    sget-object p1, Lorg/htmlparser/nodes/TagNode;->mDefaultScanner:Lorg/htmlparser/scanners/Scanner;

    iput-object p1, p0, Lorg/htmlparser/nodes/TagNode;->mScanner:Lorg/htmlparser/scanners/Scanner;

    .line 137
    iput-object p4, p0, Lorg/htmlparser/nodes/TagNode;->mAttributes:Ljava/util/Vector;

    .line 138
    iget-object p1, p0, Lorg/htmlparser/nodes/TagNode;->mAttributes:Ljava/util/Vector;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-nez p1, :cond_2

    .line 142
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getIds()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 143
    array-length p2, p1

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    .line 144
    aget-object p1, p1, p2

    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->setTagName(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string p1, ""

    .line 146
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->setTagName(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/nodes/TagNode;Lorg/htmlparser/scanners/TagScanner;)V
    .locals 3

    .line 157
    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getTagBegin()I

    move-result v1

    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getTagEnd()I

    move-result v2

    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object p1

    invoke-direct {p0, v0, v1, v2, p1}, Lorg/htmlparser/nodes/TagNode;-><init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V

    .line 158
    invoke-virtual {p0, p2}, Lorg/htmlparser/nodes/TagNode;->setThisScanner(Lorg/htmlparser/scanners/Scanner;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 1

    .line 659
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->isEndTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    invoke-virtual {p1, p0}, Lorg/htmlparser/visitors/NodeVisitor;->visitEndTag(Lorg/htmlparser/Tag;)V

    goto :goto_0

    .line 662
    :cond_0
    invoke-virtual {p1, p0}, Lorg/htmlparser/visitors/NodeVisitor;->visitTag(Lorg/htmlparser/Tag;)V

    :goto_0
    return-void
.end method

.method public breaksFlow()Z
    .locals 2

    .line 648
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->breakTags:Ljava/util/Hashtable;

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getTagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 174
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 176
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;
    .locals 6

    .line 295
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 298
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v2, :cond_0

    goto :goto_1

    .line 301
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/htmlparser/Attribute;

    .line 302
    invoke-virtual {v4}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 303
    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v2

    move-object v1, v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v1
.end method

.method public getAttributesEx()Ljava/util/Vector;
    .locals 1

    .line 374
    iget-object v0, p0, Lorg/htmlparser/nodes/TagNode;->mAttributes:Ljava/util/Vector;

    return-object v0
.end method

.method public getEndTag()Lorg/htmlparser/Tag;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 845
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->NONE:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 832
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->NONE:[Ljava/lang/String;

    return-object v0
.end method

.method public getEndingLineNumber()I
    .locals 2

    .line 809
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getEndPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->row(I)I

    move-result v0

    return v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 819
    sget-object v0, Lorg/htmlparser/nodes/TagNode;->NONE:[Ljava/lang/String;

    return-object v0
.end method

.method public getRawTagName()Ljava/lang/String;
    .locals 2

    .line 419
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 420
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 421
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/Attribute;

    invoke-virtual {v0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getStartingLineNumber()I
    .locals 2

    .line 800
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getStartPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->row(I)I

    move-result v0

    return v0
.end method

.method public getTagBegin()I
    .locals 1

    .line 500
    iget v0, p0, Lorg/htmlparser/nodes/TagNode;->nodeBegin:I

    return v0
.end method

.method public getTagEnd()I
    .locals 1

    .line 518
    iget v0, p0, Lorg/htmlparser/nodes/TagNode;->nodeEnd:I

    return v0
.end method

.method public getTagName()Ljava/lang/String;
    .locals 4

    .line 394
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getRawTagName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 397
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    .line 398
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 399
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 400
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 401
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .line 467
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->toHtml()Ljava/lang/String;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThisScanner()Lorg/htmlparser/scanners/Scanner;
    .locals 1

    .line 854
    iget-object v0, p0, Lorg/htmlparser/nodes/TagNode;->mScanner:Lorg/htmlparser/scanners/Scanner;

    return-object v0
.end method

.method public isEmptyXmlTag()Z
    .locals 4

    .line 680
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 681
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v3, 0x1

    sub-int/2addr v1, v3

    .line 684
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlparser/Attribute;

    .line 685
    invoke-virtual {v0}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 688
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v3

    .line 689
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public isEndTag()Z
    .locals 3

    .line 789
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getRawTagName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 791
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v2, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public removeAttribute(Ljava/lang/String;)V
    .locals 1

    .line 262
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 264
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    const/16 v0, 0x27

    const/16 v1, 0x22

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p2, :cond_4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    .line 204
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v4, v8, :cond_0

    move v2, v7

    goto :goto_2

    .line 206
    :cond_0
    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 207
    invoke-static {v8}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    if-ne v0, v8, :cond_2

    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    if-ne v1, v8, :cond_3

    const/4 v7, 0x0

    :cond_3
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    const/4 v6, 0x1

    :goto_2
    if-eqz v5, :cond_9

    if-eqz v2, :cond_5

    goto :goto_5

    :cond_5
    if-eqz v6, :cond_6

    const/16 v1, 0x27

    goto :goto_5

    .line 229
    :cond_6
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 230
    :goto_3
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v3, v2, :cond_7

    .line 238
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_5

    .line 232
    :cond_7
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v1, v2, :cond_8

    const-string v2, "&quot;"

    .line 234
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 236
    :cond_8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    .line 243
    :goto_5
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 246
    invoke-virtual {v0, p2}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    if-eqz v1, :cond_b

    .line 248
    invoke-virtual {v0, v1}, Lorg/htmlparser/Attribute;->setQuote(C)V

    goto :goto_6

    .line 251
    :cond_a
    invoke-virtual {p0, p1, p2, v1}, Lorg/htmlparser/nodes/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;C)V

    :cond_b
    :goto_6
    return-void
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;C)V
    .locals 1

    .line 276
    new-instance v0, Lorg/htmlparser/Attribute;

    invoke-direct {v0, p1, p2, p3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/nodes/TagNode;->setAttribute(Lorg/htmlparser/Attribute;)V

    return-void
.end method

.method public setAttribute(Lorg/htmlparser/Attribute;)V
    .locals 7

    .line 340
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v1, :cond_2

    .line 344
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 345
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v6

    if-lt v3, v6, :cond_0

    move v3, v5

    goto :goto_1

    .line 347
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/htmlparser/Attribute;

    .line 348
    invoke-virtual {v6}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 350
    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 352
    invoke-virtual {v0, p1, v3}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    const/4 v5, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-nez v3, :cond_4

    if-eqz v1, :cond_3

    sub-int/2addr v1, v2

    .line 360
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/htmlparser/Attribute;

    invoke-virtual {v1}, Lorg/htmlparser/Attribute;->isWhitespace()Z

    move-result v1

    if-nez v1, :cond_3

    .line 361
    new-instance v1, Lorg/htmlparser/Attribute;

    const-string v2, " "

    invoke-direct {v1, v2}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 362
    :cond_3
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public setAttributeEx(Lorg/htmlparser/Attribute;)V
    .locals 0

    .line 321
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->setAttribute(Lorg/htmlparser/Attribute;)V

    return-void
.end method

.method public setAttributesEx(Ljava/util/Vector;)V
    .locals 0

    .line 482
    iput-object p1, p0, Lorg/htmlparser/nodes/TagNode;->mAttributes:Ljava/util/Vector;

    return-void
.end method

.method public setEmptyXmlTag(Z)V
    .locals 11

    .line 711
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 712
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const-string v2, "/"

    const/4 v3, 0x0

    if-lez v1, :cond_4

    const/4 v4, 0x1

    sub-int/2addr v1, v4

    .line 715
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/htmlparser/Attribute;

    .line 716
    invoke-virtual {v5}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 719
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 720
    invoke-virtual {v5}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v5

    const-string v8, " "

    if-nez v5, :cond_2

    add-int/lit8 v5, v7, -0x1

    .line 722
    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2f

    if-ne v9, v10, :cond_1

    if-nez p1, :cond_5

    if-ne v4, v7, :cond_0

    .line 727
    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 733
    invoke-virtual {v6, p1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 734
    new-instance v2, Lorg/htmlparser/Attribute;

    invoke-direct {v2, p1, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    .line 736
    invoke-virtual {v0, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_5

    .line 744
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v8}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    .line 745
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 746
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_5

    .line 755
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v8}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    .line 756
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 757
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_5

    .line 767
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    .line 776
    new-instance p1, Lorg/htmlparser/Attribute;

    invoke-direct {p1, v2, v3}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public setEndTag(Lorg/htmlparser/Tag;)V
    .locals 0

    return-void
.end method

.method public setTagBegin(I)V
    .locals 0

    .line 491
    iput p1, p0, Lorg/htmlparser/nodes/TagNode;->nodeBegin:I

    return-void
.end method

.method public setTagEnd(I)V
    .locals 0

    .line 509
    iput p1, p0, Lorg/htmlparser/nodes/TagNode;->nodeEnd:I

    return-void
.end method

.method public setTagName(Ljava/lang/String;)V
    .locals 4

    .line 438
    new-instance v0, Lorg/htmlparser/Attribute;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2, v1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    .line 439
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object p1

    if-nez p1, :cond_0

    .line 442
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 443
    invoke-virtual {p0, p1}, Lorg/htmlparser/nodes/TagNode;->setAttributesEx(Ljava/util/Vector;)V

    .line 445
    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 447
    invoke-virtual {p1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 450
    :cond_1
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/htmlparser/Attribute;

    .line 452
    invoke-virtual {v2}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Lorg/htmlparser/Attribute;->getQuote()C

    move-result v2

    if-nez v2, :cond_2

    .line 453
    invoke-virtual {p1, v0, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    .line 455
    :cond_2
    invoke-virtual {p1, v0, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :goto_0
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .line 530
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Lexer;-><init>(Ljava/lang/String;)V

    .line 533
    :try_start_0
    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->nextNode()Lorg/htmlparser/Node;

    move-result-object p1

    check-cast p1, Lorg/htmlparser/nodes/TagNode;

    .line 534
    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/nodes/TagNode;->mPage:Lorg/htmlparser/lexer/Page;

    .line 535
    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getStartPosition()I

    move-result v0

    iput v0, p0, Lorg/htmlparser/nodes/TagNode;->nodeBegin:I

    .line 536
    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getEndPosition()I

    move-result v0

    iput v0, p0, Lorg/htmlparser/nodes/TagNode;->nodeEnd:I

    .line 537
    invoke-virtual {p1}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/nodes/TagNode;->mAttributes:Ljava/util/Vector;
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setThisScanner(Lorg/htmlparser/scanners/Scanner;)V
    .locals 0

    .line 863
    iput-object p1, p0, Lorg/htmlparser/nodes/TagNode;->mScanner:Lorg/htmlparser/scanners/Scanner;

    return-void
.end method

.method public toHtml(Z)Ljava/lang/String;
    .locals 0

    .line 565
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->toTagHtml()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toPlainTextString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 615
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getText()Ljava/lang/String;

    move-result-object v0

    .line 616
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 617
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->isEndTag()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "End"

    goto :goto_0

    :cond_0
    const-string v2, "Tag"

    .line 621
    :goto_0
    new-instance v3, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v4

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getStartPosition()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 622
    new-instance v4, Lorg/htmlparser/lexer/Cursor;

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v5

    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getEndPosition()I

    move-result v6

    invoke-direct {v4, v5, v6}, Lorg/htmlparser/lexer/Cursor;-><init>(Lorg/htmlparser/lexer/Page;I)V

    .line 623
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, " ("

    .line 624
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 625
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v2, ","

    .line 626
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 627
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v2, "): "

    .line 628
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x50

    .line 629
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_1

    const/4 v2, 0x0

    .line 631
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    rsub-int/lit8 v3, v3, 0x4d

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 632
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "..."

    .line 633
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 636
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 638
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toTagHtml()Ljava/lang/String;
    .locals 6

    .line 584
    invoke-virtual {p0}, Lorg/htmlparser/nodes/TagNode;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 585
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x2

    :goto_0
    if-lt v3, v1, :cond_1

    .line 591
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v3, "<"

    .line 592
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    if-lt v2, v1, :cond_0

    const-string v0, ">"

    .line 598
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 600
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 595
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/Attribute;

    .line 596
    invoke-virtual {v3, v5}, Lorg/htmlparser/Attribute;->toString(Ljava/lang/StringBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 588
    :cond_1
    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/htmlparser/Attribute;

    .line 589
    invoke-virtual {v5}, Lorg/htmlparser/Attribute;->getLength()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
