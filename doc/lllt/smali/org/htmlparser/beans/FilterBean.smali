.class public Lorg/htmlparser/beans/FilterBean;
.super Ljava/lang/Object;
.source "FilterBean.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_CONNECTION_PROPERTY:Ljava/lang/String; = "connection"

.field public static final PROP_NODES_PROPERTY:Ljava/lang/String; = "nodes"

.field public static final PROP_TEXT_PROPERTY:Ljava/lang/String; = "text"

.field public static final PROP_URL_PROPERTY:Ljava/lang/String; = "URL"


# instance fields
.field protected mFilters:[Lorg/htmlparser/NodeFilter;

.field protected mNodes:Lorg/htmlparser/util/NodeList;

.field protected mParser:Lorg/htmlparser/Parser;

.field protected mPropertySupport:Ljava/beans/PropertyChangeSupport;

.field protected mRecursive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Ljava/beans/PropertyChangeSupport;

    invoke-direct {v0, p0}, Ljava/beans/PropertyChangeSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    .line 107
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    iput-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    const/4 v0, 0x0

    .line 108
    iput-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mFilters:[Lorg/htmlparser/NodeFilter;

    .line 109
    iput-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    const/4 v0, 0x1

    .line 110
    iput-boolean v0, p0, Lorg/htmlparser/beans/FilterBean;->mRecursive:Z

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5

    .line 444
    array-length v0, p0

    if-gtz v0, :cond_0

    .line 445
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "Usage: java -classpath htmlparser.jar org.htmlparser.beans.FilterBean <http://whatever_url> [node name]"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 448
    :cond_0
    new-instance v0, Lorg/htmlparser/beans/FilterBean;

    invoke-direct {v0}, Lorg/htmlparser/beans/FilterBean;-><init>()V

    .line 449
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ge v3, v1, :cond_1

    new-array v1, v3, [Lorg/htmlparser/NodeFilter;

    .line 450
    new-instance v4, Lorg/htmlparser/filters/TagNameFilter;

    aget-object v3, p0, v3

    invoke-direct {v4, v3}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lorg/htmlparser/beans/FilterBean;->setFilters([Lorg/htmlparser/NodeFilter;)V

    .line 451
    :cond_1
    aget-object p0, p0, v2

    invoke-virtual {v0, p0}, Lorg/htmlparser/beans/FilterBean;->setURL(Ljava/lang/String;)V

    .line 453
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lorg/htmlparser/beans/FilterBean;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 220
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method protected applyFilters()Lorg/htmlparser/util/NodeList;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/htmlparser/Parser;->parse(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getFilters()[Lorg/htmlparser/NodeFilter;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    .line 170
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_0

    goto :goto_1

    .line 171
    :cond_0
    aget-object v3, v1, v2

    iget-boolean v4, p0, Lorg/htmlparser/beans/FilterBean;->mRecursive:Z

    invoke-virtual {v0, v3, v4}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-object v0
.end method

.method public getConnection()Ljava/net/URLConnection;
    .locals 1

    .line 302
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getFilters()[Lorg/htmlparser/NodeFilter;
    .locals 1

    .line 346
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mFilters:[Lorg/htmlparser/NodeFilter;

    return-object v0
.end method

.method public getNodes()Lorg/htmlparser/util/NodeList;
    .locals 1

    .line 244
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    if-nez v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->setNodes()V

    .line 247
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    return-object v0
.end method

.method public getParser()Lorg/htmlparser/Parser;
    .locals 1

    .line 371
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    return-object v0
.end method

.method public getRecursive()Z
    .locals 1

    .line 423
    iget-boolean v0, p0, Lorg/htmlparser/beans/FilterBean;->mRecursive:Z

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 4

    .line 402
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getNodes()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 405
    new-instance v1, Lorg/htmlparser/beans/StringBean;

    invoke-direct {v1}, Lorg/htmlparser/beans/StringBean;-><init>()V

    const/4 v2, 0x0

    .line 406
    :goto_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 408
    invoke-virtual {v1}, Lorg/htmlparser/beans/StringBean;->getStrings()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 407
    :cond_0
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    invoke-interface {v3, v1}, Lorg/htmlparser/Node;->accept(Lorg/htmlparser/visitors/NodeVisitor;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_1
    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 257
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getURL()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 230
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method public setConnection(Ljava/net/URLConnection;)V
    .locals 4

    .line 316
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 317
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getConnection()Ljava/net/URLConnection;

    move-result-object v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    .line 319
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 323
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    if-nez v2, :cond_2

    .line 324
    new-instance v2, Lorg/htmlparser/Parser;

    invoke-direct {v2, p1}, Lorg/htmlparser/Parser;-><init>(Ljava/net/URLConnection;)V

    iput-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    goto :goto_0

    .line 326
    :cond_2
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2, p1}, Lorg/htmlparser/Parser;->setConnection(Ljava/net/URLConnection;)V

    .line 327
    :goto_0
    iget-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "URL"

    .line 328
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-virtual {p1, v2, v0, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 329
    iget-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v0, "connection"

    .line 330
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 329
    invoke-virtual {p1, v0, v1, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 331
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->setNodes()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 335
    :catch_0
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {p0, p1}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public setFilters([Lorg/htmlparser/NodeFilter;)V
    .locals 0

    .line 357
    iput-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mFilters:[Lorg/htmlparser/NodeFilter;

    .line 358
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getParser()Lorg/htmlparser/Parser;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 360
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getParser()Lorg/htmlparser/Parser;

    move-result-object p1

    invoke-virtual {p1}, Lorg/htmlparser/Parser;->reset()V

    .line 361
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->setNodes()V

    :cond_0
    return-void
.end method

.method protected setNodes()V
    .locals 1

    .line 184
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getURL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    :try_start_0
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->applyFilters()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 188
    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V
    :try_end_0
    .catch Lorg/htmlparser/util/EncodingChangeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 205
    :catch_0
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V

    goto :goto_0

    .line 194
    :catch_1
    :try_start_1
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->reset()V

    .line 195
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->applyFilters()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 196
    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V
    :try_end_1
    .catch Lorg/htmlparser/util/ParserException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 200
    :catch_2
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public setParser(Lorg/htmlparser/Parser;)V
    .locals 0

    .line 382
    iput-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    .line 383
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getFilters()[Lorg/htmlparser/NodeFilter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 384
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->setNodes()V

    :cond_0
    return-void
.end method

.method public setRecursive(Z)V
    .locals 0

    .line 434
    iput-boolean p1, p0, Lorg/htmlparser/beans/FilterBean;->mRecursive:Z

    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 4

    .line 271
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 272
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getConnection()Ljava/net/URLConnection;

    move-result-object v1

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v0, :cond_3

    .line 274
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 278
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    if-nez v2, :cond_2

    .line 279
    new-instance v2, Lorg/htmlparser/Parser;

    invoke-direct {v2, p1}, Lorg/htmlparser/Parser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    goto :goto_0

    .line 281
    :cond_2
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2, p1}, Lorg/htmlparser/Parser;->setURL(Ljava/lang/String;)V

    .line 282
    :goto_0
    iget-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "URL"

    .line 283
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 282
    invoke-virtual {p1, v2, v0, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 284
    iget-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v0, "connection"

    .line 285
    iget-object v2, p0, Lorg/htmlparser/beans/FilterBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 284
    invoke-virtual {p1, v0, v1, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->setNodes()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 290
    :catch_0
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {p0, p1}, Lorg/htmlparser/beans/FilterBean;->updateNodes(Lorg/htmlparser/util/NodeList;)V

    :cond_3
    :goto_1
    return-void
.end method

.method protected updateNodes(Lorg/htmlparser/util/NodeList;)V
    .locals 5

    .line 127
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 129
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    const-string v1, ""

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getText()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    if-nez v2, :cond_2

    move-object v2, v1

    .line 136
    :cond_2
    iput-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    .line 137
    iget-object v3, p0, Lorg/htmlparser/beans/FilterBean;->mNodes:Lorg/htmlparser/util/NodeList;

    if-eqz v3, :cond_3

    .line 138
    invoke-virtual {p0}, Lorg/htmlparser/beans/FilterBean;->getText()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    move-object v3, v1

    :goto_1
    if-nez v3, :cond_4

    goto :goto_2

    :cond_4
    move-object v1, v3

    .line 143
    :goto_2
    iget-object v3, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v4, "nodes"

    invoke-virtual {v3, v4, v0, p1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 145
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 146
    iget-object p1, p0, Lorg/htmlparser/beans/FilterBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v0, "text"

    invoke-virtual {p1, v0, v2, v1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_5
    return-void
.end method
