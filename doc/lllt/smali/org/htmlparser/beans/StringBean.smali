.class public Lorg/htmlparser/beans/StringBean;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "StringBean.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final NEWLINE:Ljava/lang/String;

.field private static final NEWLINE_SIZE:I

.field public static final PROP_COLLAPSE_PROPERTY:Ljava/lang/String; = "collapse"

.field public static final PROP_CONNECTION_PROPERTY:Ljava/lang/String; = "connection"

.field public static final PROP_LINKS_PROPERTY:Ljava/lang/String; = "links"

.field public static final PROP_REPLACE_SPACE_PROPERTY:Ljava/lang/String; = "replaceNonBreakingSpaces"

.field public static final PROP_STRINGS_PROPERTY:Ljava/lang/String; = "strings"

.field public static final PROP_URL_PROPERTY:Ljava/lang/String; = "URL"


# instance fields
.field protected mBuffer:Ljava/lang/StringBuffer;

.field protected mCollapse:Z

.field protected mCollapseState:I

.field protected mIsPre:Z

.field protected mIsScript:Z

.field protected mIsStyle:Z

.field protected mLinks:Z

.field protected mParser:Lorg/htmlparser/Parser;

.field protected mPropertySupport:Ljava/beans/PropertyChangeSupport;

.field protected mReplaceSpace:Z

.field protected mStrings:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "line.separator"

    .line 110
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/htmlparser/beans/StringBean;->NEWLINE:Ljava/lang/String;

    .line 115
    sget-object v0, Lorg/htmlparser/beans/StringBean;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lorg/htmlparser/beans/StringBean;->NEWLINE_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    .line 188
    invoke-direct {p0, v0, v0}, Lorg/htmlparser/visitors/NodeVisitor;-><init>(ZZ)V

    .line 189
    new-instance v1, Ljava/beans/PropertyChangeSupport;

    invoke-direct {v1, p0}, Ljava/beans/PropertyChangeSupport;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    .line 190
    new-instance v1, Lorg/htmlparser/Parser;

    invoke-direct {v1}, Lorg/htmlparser/Parser;-><init>()V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    const/4 v1, 0x0

    .line 191
    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    const/4 v1, 0x0

    .line 192
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mLinks:Z

    .line 193
    iput-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mReplaceSpace:Z

    .line 194
    iput-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapse:Z

    .line 195
    iput v1, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 196
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v2, 0x1000

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    .line 197
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsScript:Z

    .line 198
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsPre:Z

    .line 199
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsStyle:Z

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3

    .line 702
    array-length v0, p0

    if-gtz v0, :cond_0

    .line 703
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "Usage: java -classpath htmlparser.jar org.htmlparser.beans.StringBean <http://whatever_url>"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 707
    :cond_0
    new-instance v0, Lorg/htmlparser/beans/StringBean;

    invoke-direct {v0}, Lorg/htmlparser/beans/StringBean;-><init>()V

    const/4 v1, 0x0

    .line 708
    invoke-virtual {v0, v1}, Lorg/htmlparser/beans/StringBean;->setLinks(Z)V

    const/4 v2, 0x1

    .line 709
    invoke-virtual {v0, v2}, Lorg/htmlparser/beans/StringBean;->setReplaceNonBreakingSpaces(Z)V

    .line 710
    invoke-virtual {v0, v2}, Lorg/htmlparser/beans/StringBean;->setCollapse(Z)V

    .line 711
    aget-object p0, p0, v1

    invoke-virtual {v0, p0}, Lorg/htmlparser/beans/StringBean;->setURL(Ljava/lang/String;)V

    .line 712
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lorg/htmlparser/beans/StringBean;->getStrings()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private resetStrings()V
    .locals 2

    .line 375
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 378
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/Parser;->setURL(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->setStrings()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 383
    invoke-virtual {v0}, Lorg/htmlparser/util/ParserException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 398
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method protected carriageReturn()V
    .locals 3

    .line 214
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget v1, Lorg/htmlparser/beans/StringBean;->NEWLINE_SIZE:I

    if-gt v1, v0, :cond_0

    .line 217
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    sub-int v1, v0, v1

    invoke-virtual {v2, v1, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 218
    sget-object v1, Lorg/htmlparser/beans/StringBean;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lorg/htmlparser/beans/StringBean;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const/4 v0, 0x0

    .line 220
    iput v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    return-void
.end method

.method protected collapse(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 6

    .line 248
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    goto :goto_2

    .line 253
    :cond_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x9

    const/4 v4, 0x1

    if-eq v2, v3, :cond_2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_2

    const/16 v3, 0xd

    if-eq v2, v3, :cond_2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_2

    const/16 v5, 0x200b

    if-eq v2, v5, :cond_2

    .line 268
    iget v5, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    if-ne v4, v5, :cond_1

    .line 269
    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    const/4 v3, 0x2

    .line 270
    iput v3, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 271
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 264
    :cond_2
    iget v2, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    if-eqz v2, :cond_3

    .line 265
    iput v4, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    return-void
.end method

.method protected extractStrings()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 288
    iput v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 289
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0, p0}, Lorg/htmlparser/Parser;->visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V

    .line 290
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v2, 0x1000

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public getCollapse()Z
    .locals 1

    .line 553
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapse:Z

    return v0
.end method

.method public getConnection()Ljava/net/URLConnection;
    .locals 1

    .line 586
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getLinks()Z
    .locals 1

    .line 438
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mLinks:Z

    return v0
.end method

.method public getReplaceNonBreakingSpaces()Z
    .locals 1

    .line 514
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mReplaceSpace:Z

    return v0
.end method

.method public getStrings()Ljava/lang/String;
    .locals 1

    .line 422
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 423
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 424
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->setStrings()V

    goto :goto_0

    .line 426
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V

    .line 428
    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 467
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getURL()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 408
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method public setCollapse(Z)V
    .locals 3

    const/4 v0, 0x0

    .line 568
    iput v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 569
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapse:Z

    if-eq v0, p1, :cond_0

    .line 572
    iput-boolean p1, p0, Lorg/htmlparser/beans/StringBean;->mCollapse:Z

    .line 573
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "collapse"

    invoke-virtual {v1, v2, v0, p1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;ZZ)V

    .line 575
    invoke-direct {p0}, Lorg/htmlparser/beans/StringBean;->resetStrings()V

    :cond_0
    return-void
.end method

.method public setConnection(Ljava/net/URLConnection;)V
    .locals 4

    .line 600
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 601
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getConnection()Ljava/net/URLConnection;

    move-result-object v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    .line 603
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 607
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    if-nez v2, :cond_2

    .line 608
    new-instance v2, Lorg/htmlparser/Parser;

    invoke-direct {v2, p1}, Lorg/htmlparser/Parser;-><init>(Ljava/net/URLConnection;)V

    iput-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    goto :goto_0

    .line 610
    :cond_2
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2, p1}, Lorg/htmlparser/Parser;->setConnection(Ljava/net/URLConnection;)V

    .line 611
    :goto_0
    iget-object p1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "URL"

    .line 612
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 611
    invoke-virtual {p1, v2, v0, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 613
    iget-object p1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v0, "connection"

    .line 614
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 613
    invoke-virtual {p1, v0, v1, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 615
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->setStrings()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 619
    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public setLinks(Z)V
    .locals 3

    .line 450
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mLinks:Z

    if-eq v0, p1, :cond_0

    .line 453
    iput-boolean p1, p0, Lorg/htmlparser/beans/StringBean;->mLinks:Z

    .line 454
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "links"

    invoke-virtual {v1, v2, v0, p1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;ZZ)V

    .line 456
    invoke-direct {p0}, Lorg/htmlparser/beans/StringBean;->resetStrings()V

    :cond_0
    return-void
.end method

.method public setReplaceNonBreakingSpaces(Z)V
    .locals 3

    .line 528
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mReplaceSpace:Z

    if-eq v0, p1, :cond_0

    .line 531
    iput-boolean p1, p0, Lorg/htmlparser/beans/StringBean;->mReplaceSpace:Z

    .line 532
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "replaceNonBreakingSpaces"

    invoke-virtual {v1, v2, v0, p1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;ZZ)V

    .line 534
    invoke-direct {p0}, Lorg/htmlparser/beans/StringBean;->resetStrings()V

    :cond_0
    return-void
.end method

.method protected setStrings()V
    .locals 4

    const/4 v0, 0x0

    .line 319
    iput v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 320
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1000

    if-eqz v1, :cond_0

    .line 325
    :try_start_0
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1, p0}, Lorg/htmlparser/Parser;->visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V

    .line 326
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    :try_start_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    goto :goto_2

    :catchall_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v3, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    .line 331
    throw v1
    :try_end_1
    .catch Lorg/htmlparser/util/EncodingChangeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/htmlparser/util/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 357
    invoke-virtual {v0}, Lorg/htmlparser/util/ParserException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V

    goto :goto_2

    .line 335
    :catch_1
    iput-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsPre:Z

    .line 336
    iput-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsScript:Z

    .line 337
    iput-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsStyle:Z

    .line 340
    :try_start_2
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1}, Lorg/htmlparser/Parser;->reset()V

    .line 341
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    .line 342
    iput v0, p0, Lorg/htmlparser/beans/StringBean;->mCollapseState:I

    .line 343
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0, p0}, Lorg/htmlparser/Parser;->visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V

    .line 344
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/htmlparser/util/ParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 352
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    .line 348
    :try_start_3
    invoke-virtual {v0}, Lorg/htmlparser/util/ParserException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 352
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    :goto_0
    iput-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    goto :goto_2

    :goto_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v1, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    .line 353
    throw v0

    :cond_0
    const/4 v0, 0x0

    .line 363
    iput-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    .line 364
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    :goto_2
    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 4

    .line 481
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 482
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getConnection()Ljava/net/URLConnection;

    move-result-object v1

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v0, :cond_3

    .line 484
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 488
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    if-nez v2, :cond_2

    .line 489
    new-instance v2, Lorg/htmlparser/Parser;

    invoke-direct {v2, p1}, Lorg/htmlparser/Parser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    goto :goto_0

    .line 491
    :cond_2
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2, p1}, Lorg/htmlparser/Parser;->setURL(Ljava/lang/String;)V

    .line 492
    :goto_0
    iget-object p1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "URL"

    .line 493
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 492
    invoke-virtual {p1, v2, v0, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 494
    iget-object p1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v0, "connection"

    .line 495
    iget-object v2, p0, Lorg/htmlparser/beans/StringBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 494
    invoke-virtual {p1, v0, v1, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 496
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->setStrings()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 500
    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/htmlparser/beans/StringBean;->updateStrings(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method protected updateStrings(Ljava/lang/String;)V
    .locals 3

    .line 304
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 306
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    .line 307
    iput-object p1, p0, Lorg/htmlparser/beans/StringBean;->mStrings:Ljava/lang/String;

    .line 308
    iget-object v1, p0, Lorg/htmlparser/beans/StringBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "strings"

    invoke-virtual {v1, v2, v0, p1}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public visitEndTag(Lorg/htmlparser/Tag;)V
    .locals 2

    .line 687
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PRE"

    .line 688
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 689
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsPre:Z

    goto :goto_0

    :cond_0
    const-string v0, "SCRIPT"

    .line 690
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsScript:Z

    goto :goto_0

    :cond_1
    const-string v0, "STYLE"

    .line 692
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 693
    iput-boolean v1, p0, Lorg/htmlparser/beans/StringBean;->mIsStyle:Z

    :cond_2
    :goto_0
    return-void
.end method

.method public visitStringNode(Lorg/htmlparser/Text;)V
    .locals 2

    .line 634
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsScript:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsStyle:Z

    if-nez v0, :cond_3

    .line 636
    invoke-interface {p1}, Lorg/htmlparser/Text;->getText()Ljava/lang/String;

    move-result-object p1

    .line 637
    iget-boolean v0, p0, Lorg/htmlparser/beans/StringBean;->mIsPre:Z

    if-nez v0, :cond_2

    .line 639
    invoke-static {p1}, Lorg/htmlparser/util/Translate;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 640
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getReplaceNonBreakingSpaces()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa0

    const/16 v1, 0x20

    .line 641
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 642
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getCollapse()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/beans/StringBean;->collapse(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    goto :goto_0

    .line 645
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 648
    :cond_2
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    :goto_0
    return-void
.end method

.method public visitTag(Lorg/htmlparser/Tag;)V
    .locals 3

    .line 661
    instance-of v0, p1, Lorg/htmlparser/tags/LinkTag;

    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->getLinks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 665
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    move-object v1, p1

    check-cast v1, Lorg/htmlparser/tags/LinkTag;

    invoke-virtual {v1}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 666
    iget-object v0, p0, Lorg/htmlparser/beans/StringBean;->mBuffer:Ljava/lang/StringBuffer;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 668
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PRE"

    .line 669
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 670
    iput-boolean v2, p0, Lorg/htmlparser/beans/StringBean;->mIsPre:Z

    goto :goto_0

    :cond_1
    const-string v1, "SCRIPT"

    .line 671
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672
    iput-boolean v2, p0, Lorg/htmlparser/beans/StringBean;->mIsScript:Z

    goto :goto_0

    :cond_2
    const-string v1, "STYLE"

    .line 673
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 674
    iput-boolean v2, p0, Lorg/htmlparser/beans/StringBean;->mIsStyle:Z

    .line 675
    :cond_3
    :goto_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->breaksFlow()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 676
    invoke-virtual {p0}, Lorg/htmlparser/beans/StringBean;->carriageReturn()V

    :cond_4
    return-void
.end method
