.class public Lorg/htmlparser/beans/LinkBean;
.super Ljava/lang/Object;
.source "LinkBean.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_LINKS_PROPERTY:Ljava/lang/String; = "links"

.field public static final PROP_URL_PROPERTY:Ljava/lang/String; = "URL"


# instance fields
.field protected mLinks:[Ljava/net/URL;

.field protected mParser:Lorg/htmlparser/Parser;

.field protected mPropertySupport:Ljava/beans/PropertyChangeSupport;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/beans/PropertyChangeSupport;

    invoke-direct {v0, p0}, Ljava/beans/PropertyChangeSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const/4 v0, 0x0

    .line 78
    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 79
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3

    .line 299
    array-length v0, p0

    if-gtz v0, :cond_0

    .line 300
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "Usage: java -classpath htmlparser.jar org.htmlparser.beans.LinkBean <http://whatever_url>"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 304
    :cond_0
    new-instance v0, Lorg/htmlparser/beans/LinkBean;

    invoke-direct {v0}, Lorg/htmlparser/beans/LinkBean;-><init>()V

    const/4 v1, 0x0

    .line 305
    aget-object p0, p0, v1

    invoke-virtual {v0, p0}, Lorg/htmlparser/beans/LinkBean;->setURL(Ljava/lang/String;)V

    .line 306
    invoke-virtual {v0}, Lorg/htmlparser/beans/LinkBean;->getLinks()[Ljava/net/URL;

    move-result-object p0

    .line 307
    :goto_0
    array-length v0, p0

    if-lt v1, v0, :cond_1

    :goto_1
    return-void

    .line 308
    :cond_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v2, p0, v1

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setLinks()V
    .locals 4

    .line 191
    invoke-virtual {p0}, Lorg/htmlparser/beans/LinkBean;->getURL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    :try_start_0
    invoke-virtual {p0}, Lorg/htmlparser/beans/LinkBean;->extractLinks()[Ljava/net/URL;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    invoke-virtual {p0, v1, v0}, Lorg/htmlparser/beans/LinkBean;->equivalent([Ljava/net/URL;[Ljava/net/URL;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 199
    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 200
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "links"

    .line 201
    iget-object v3, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 200
    invoke-virtual {v0, v2, v1, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 206
    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 165
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method protected equivalent([Ljava/net/URL;[Ljava/net/URL;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    goto :goto_2

    :cond_0
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    .line 143
    array-length v2, p1

    array-length v3, p2

    if-ne v2, v3, :cond_4

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 146
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_3

    if-nez v2, :cond_1

    goto :goto_1

    .line 147
    :cond_1
    aget-object v3, p1, v0

    aget-object v4, p2, v0

    if-eq v3, v4, :cond_2

    const/4 v2, 0x0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    move v0, v2

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return v0
.end method

.method protected extractLinks()[Ljava/net/URL;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->reset()V

    .line 100
    new-instance v0, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v1, Lorg/htmlparser/tags/LinkTag;

    invoke-direct {v0, v1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 103
    :try_start_0
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1, v0}, Lorg/htmlparser/Parser;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object v0
    :try_end_0
    .catch Lorg/htmlparser/util/EncodingChangeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1}, Lorg/htmlparser/Parser;->reset()V

    .line 108
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1, v0}, Lorg/htmlparser/Parser;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 110
    :goto_0
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    .line 111
    :goto_1
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 122
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Ljava/net/URL;

    .line 123
    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    return-object v0

    .line 114
    :cond_0
    :try_start_1
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/LinkTag;

    .line 115
    new-instance v4, Ljava/net/URL;

    invoke-virtual {v3}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getConnection()Ljava/net/URLConnection;
    .locals 1

    .line 273
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    return-object v0
.end method

.method public getLinks()[Ljava/net/URL;
    .locals 4

    .line 216
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 219
    :try_start_0
    invoke-virtual {p0}, Lorg/htmlparser/beans/LinkBean;->extractLinks()[Ljava/net/URL;

    move-result-object v1

    iput-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 220
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v2, "links"

    .line 221
    iget-object v3, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 220
    invoke-virtual {v1, v2, v0, v3}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    :catch_0
    iput-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    .line 228
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mLinks:[Ljava/net/URL;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 238
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    .line 175
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method public setConnection(Ljava/net/URLConnection;)V
    .locals 1

    .line 284
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0, p1}, Lorg/htmlparser/Parser;->setConnection(Ljava/net/URLConnection;)V

    .line 285
    invoke-direct {p0}, Lorg/htmlparser/beans/LinkBean;->setLinks()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 3

    .line 249
    invoke-virtual {p0}, Lorg/htmlparser/beans/LinkBean;->getURL()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    .line 251
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 255
    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/htmlparser/beans/LinkBean;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1, p1}, Lorg/htmlparser/Parser;->setURL(Ljava/lang/String;)V

    .line 256
    iget-object p1, p0, Lorg/htmlparser/beans/LinkBean;->mPropertySupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "URL"

    .line 257
    invoke-virtual {p0}, Lorg/htmlparser/beans/LinkBean;->getURL()Ljava/lang/String;

    move-result-object v2

    .line 256
    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 258
    invoke-direct {p0}, Lorg/htmlparser/beans/LinkBean;->setLinks()V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method
