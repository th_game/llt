.class public Lorg/htmlparser/Parser;
.super Ljava/lang/Object;
.source "Parser.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/htmlparser/http/ConnectionMonitor;


# static fields
.field public static final DEVNULL:Lorg/htmlparser/util/ParserFeedback;

.field public static final STDOUT:Lorg/htmlparser/util/ParserFeedback;

.field public static final VERSION_DATE:Ljava/lang/String; = "Sep 17, 2006"

.field public static final VERSION_NUMBER:D = 2.0

.field public static final VERSION_STRING:Ljava/lang/String; = "2.0 (Release Build Sep 17, 2006)"

.field public static final VERSION_TYPE:Ljava/lang/String; = "Release Build"


# instance fields
.field protected mFeedback:Lorg/htmlparser/util/ParserFeedback;

.field protected mLexer:Lorg/htmlparser/lexer/Lexer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 163
    new-instance v0, Lorg/htmlparser/util/DefaultParserFeedback;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/htmlparser/util/DefaultParserFeedback;-><init>(I)V

    .line 162
    sput-object v0, Lorg/htmlparser/Parser;->DEVNULL:Lorg/htmlparser/util/ParserFeedback;

    .line 169
    new-instance v0, Lorg/htmlparser/util/DefaultParserFeedback;

    invoke-direct {v0}, Lorg/htmlparser/util/DefaultParserFeedback;-><init>()V

    sput-object v0, Lorg/htmlparser/Parser;->STDOUT:Lorg/htmlparser/util/ParserFeedback;

    .line 173
    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    invoke-static {}, Lorg/htmlparser/http/ConnectionManager;->getDefaultRequestProperties()Ljava/util/Hashtable;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTMLParser/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lorg/htmlparser/Parser;->getVersionNumber()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "User-Agent"

    .line 173
    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 258
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    new-instance v1, Lorg/htmlparser/lexer/Page;

    const-string v2, ""

    invoke-direct {v1, v2}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    sget-object v1, Lorg/htmlparser/Parser;->DEVNULL:Lorg/htmlparser/util/ParserFeedback;

    invoke-direct {p0, v0, v1}, Lorg/htmlparser/Parser;-><init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 330
    sget-object v0, Lorg/htmlparser/Parser;->STDOUT:Lorg/htmlparser/util/ParserFeedback;

    invoke-direct {p0, p1, v0}, Lorg/htmlparser/Parser;-><init>(Ljava/lang/String;Lorg/htmlparser/util/ParserFeedback;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/htmlparser/util/ParserFeedback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    invoke-virtual {p0, p2}, Lorg/htmlparser/Parser;->setFeedback(Lorg/htmlparser/util/ParserFeedback;)V

    .line 316
    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->setResource(Ljava/lang/String;)V

    .line 317
    new-instance p1, Lorg/htmlparser/PrototypicalNodeFactory;

    invoke-direct {p1}, Lorg/htmlparser/PrototypicalNodeFactory;-><init>()V

    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->setNodeFactory(Lorg/htmlparser/NodeFactory;)V

    return-void
.end method

.method public constructor <init>(Ljava/net/URLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 359
    sget-object v0, Lorg/htmlparser/Parser;->STDOUT:Lorg/htmlparser/util/ParserFeedback;

    invoke-direct {p0, p1, v0}, Lorg/htmlparser/Parser;-><init>(Ljava/net/URLConnection;Lorg/htmlparser/util/ParserFeedback;)V

    return-void
.end method

.method public constructor <init>(Ljava/net/URLConnection;Lorg/htmlparser/util/ParserFeedback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 292
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Lexer;-><init>(Ljava/net/URLConnection;)V

    invoke-direct {p0, v0, p2}, Lorg/htmlparser/Parser;-><init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Lexer;)V
    .locals 1

    .line 342
    sget-object v0, Lorg/htmlparser/Parser;->STDOUT:Lorg/htmlparser/util/ParserFeedback;

    invoke-direct {p0, p1, v0}, Lorg/htmlparser/Parser;-><init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V

    return-void
.end method

.method public constructor <init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V
    .locals 0

    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    invoke-virtual {p0, p2}, Lorg/htmlparser/Parser;->setFeedback(Lorg/htmlparser/util/ParserFeedback;)V

    .line 273
    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    .line 274
    new-instance p1, Lorg/htmlparser/PrototypicalNodeFactory;

    invoke-direct {p1}, Lorg/htmlparser/PrototypicalNodeFactory;-><init>()V

    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->setNodeFactory(Lorg/htmlparser/NodeFactory;)V

    return-void
.end method

.method public static createParser(Ljava/lang/String;Ljava/lang/String;)Lorg/htmlparser/Parser;
    .locals 3

    if-eqz p0, :cond_0

    .line 239
    new-instance v0, Lorg/htmlparser/Parser;

    new-instance v1, Lorg/htmlparser/lexer/Lexer;

    new-instance v2, Lorg/htmlparser/lexer/Page;

    invoke-direct {v2, p0, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    invoke-direct {v0, v1}, Lorg/htmlparser/Parser;-><init>(Lorg/htmlparser/lexer/Lexer;)V

    return-object v0

    .line 238
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "html cannot be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static getConnectionManager()Lorg/htmlparser/http/ConnectionManager;
    .locals 1

    .line 211
    invoke-static {}, Lorg/htmlparser/lexer/Page;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "2.0 (Release Build Sep 17, 2006)"

    return-object v0
.end method

.method public static getVersionNumber()D
    .locals 2

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    return-wide v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5

    .line 816
    array-length v0, p0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    const/4 v0, 0x0

    aget-object v2, p0, v0

    const-string v3, "-help"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 835
    :cond_0
    :try_start_0
    new-instance v2, Lorg/htmlparser/Parser;

    invoke-direct {v2}, Lorg/htmlparser/Parser;-><init>()V

    .line 836
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 837
    new-instance v3, Lorg/htmlparser/filters/TagNameFilter;

    aget-object v4, p0, v1

    invoke-direct {v3, v4}, Lorg/htmlparser/filters/TagNameFilter;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 842
    sget-object v4, Lorg/htmlparser/Parser;->STDOUT:Lorg/htmlparser/util/ParserFeedback;

    invoke-virtual {v2, v4}, Lorg/htmlparser/Parser;->setFeedback(Lorg/htmlparser/util/ParserFeedback;)V

    .line 843
    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lorg/htmlparser/http/ConnectionManager;->setMonitor(Lorg/htmlparser/http/ConnectionMonitor;)V

    .line 845
    :goto_0
    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lorg/htmlparser/http/ConnectionManager;->setRedirectionProcessingEnabled(Z)V

    .line 846
    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lorg/htmlparser/http/ConnectionManager;->setCookieProcessingEnabled(Z)V

    .line 847
    aget-object p0, p0, v0

    invoke-virtual {v2, p0}, Lorg/htmlparser/Parser;->setResource(Ljava/lang/String;)V

    .line 848
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v3}, Lorg/htmlparser/Parser;->parse(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    .line 852
    invoke-virtual {p0}, Lorg/htmlparser/util/ParserException;->printStackTrace()V

    goto :goto_2

    .line 818
    :cond_2
    :goto_1
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTML Parser v"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lorg/htmlparser/Parser;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 819
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    .line 820
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "Syntax : java -jar htmlparser.jar <file/page> [type]"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 822
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "   <file/page> the URL or file to be parsed"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 823
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "   type the node type, for example:"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 824
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "     A - Show only the link tags"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 825
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "     IMG - Show only the image tags"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 826
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "     TITLE - Show only the title tag"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 827
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    .line 828
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "Example : java -jar htmlparser.jar http://www.yahoo.com"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 830
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    :goto_2
    return-void
.end method

.method public static setConnectionManager(Lorg/htmlparser/http/ConnectionManager;)V
    .locals 0

    .line 221
    invoke-static {p0}, Lorg/htmlparser/lexer/Page;->setConnectionManager(Lorg/htmlparser/http/ConnectionManager;)V

    return-void
.end method


# virtual methods
.method public elements()Lorg/htmlparser/util/NodeIterator;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 656
    new-instance v0, Lorg/htmlparser/util/IteratorImpl;

    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getFeedback()Lorg/htmlparser/util/ParserFeedback;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/htmlparser/util/IteratorImpl;-><init>(Lorg/htmlparser/lexer/Lexer;Lorg/htmlparser/util/ParserFeedback;)V

    return-object v0
.end method

.method public extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 765
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 766
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->elements()Lorg/htmlparser/util/NodeIterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_0

    return-object v0

    .line 767
    :cond_0
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    invoke-interface {v2, v0, p1}, Lorg/htmlparser/Node;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    goto :goto_0
.end method

.method public getConnection()Ljava/net/URLConnection;
    .locals 1

    .line 430
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Page;->getConnection()Ljava/net/URLConnection;

    move-result-object v0

    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .line 489
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Page;->getEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFeedback()Lorg/htmlparser/util/ParserFeedback;
    .locals 1

    .line 579
    iget-object v0, p0, Lorg/htmlparser/Parser;->mFeedback:Lorg/htmlparser/util/ParserFeedback;

    return-object v0
.end method

.method public getLexer()Lorg/htmlparser/lexer/Lexer;
    .locals 1

    .line 532
    iget-object v0, p0, Lorg/htmlparser/Parser;->mLexer:Lorg/htmlparser/lexer/Lexer;

    return-object v0
.end method

.method public getNodeFactory()Lorg/htmlparser/NodeFactory;
    .locals 1

    .line 542
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v0

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 461
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parse(Lorg/htmlparser/NodeFilter;)Lorg/htmlparser/util/NodeList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 697
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    .line 698
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->elements()Lorg/htmlparser/util/NodeIterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_0

    return-object v0

    .line 700
    :cond_0
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    if-eqz p1, :cond_1

    .line 702
    invoke-interface {v2, v0, p1}, Lorg/htmlparser/Node;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    goto :goto_0

    .line 704
    :cond_1
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_0
.end method

.method public postConnect(Ljava/net/HttpURLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 803
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getFeedback()Lorg/htmlparser/util/ParserFeedback;

    move-result-object v0

    invoke-static {p1}, Lorg/htmlparser/http/HttpHeader;->getResponseHeader(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/htmlparser/util/ParserFeedback;->info(Ljava/lang/String;)V

    return-void
.end method

.method public preConnect(Ljava/net/HttpURLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 788
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getFeedback()Lorg/htmlparser/util/ParserFeedback;

    move-result-object v0

    invoke-static {p1}, Lorg/htmlparser/http/HttpHeader;->getRequestHeader(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/htmlparser/util/ParserFeedback;->info(Ljava/lang/String;)V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 602
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->reset()V

    return-void
.end method

.method public setConnection(Ljava/net/URLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 419
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Lexer;-><init>(Ljava/net/URLConnection;)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    return-void

    .line 418
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "connection cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 477
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/Page;->setEncoding(Ljava/lang/String;)V

    return-void
.end method

.method public setFeedback(Lorg/htmlparser/util/ParserFeedback;)V
    .locals 0

    if-nez p1, :cond_0

    .line 567
    sget-object p1, Lorg/htmlparser/Parser;->DEVNULL:Lorg/htmlparser/util/ParserFeedback;

    iput-object p1, p0, Lorg/htmlparser/Parser;->mFeedback:Lorg/htmlparser/util/ParserFeedback;

    goto :goto_0

    .line 569
    :cond_0
    iput-object p1, p0, Lorg/htmlparser/Parser;->mFeedback:Lorg/htmlparser/util/ParserFeedback;

    :goto_0
    return-void
.end method

.method public setInputHTML(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    if-eqz p1, :cond_1

    const-string v0, ""

    .line 745
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 746
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    new-instance v1, Lorg/htmlparser/lexer/Page;

    invoke-direct {v1, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    :cond_0
    return-void

    .line 744
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "html cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setLexer(Lorg/htmlparser/lexer/Lexer;)V
    .locals 2

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    .line 511
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 512
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getNodeFactory()Lorg/htmlparser/NodeFactory;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    .line 514
    invoke-virtual {p1, v0}, Lorg/htmlparser/lexer/Lexer;->setNodeFactory(Lorg/htmlparser/NodeFactory;)V

    .line 515
    :cond_1
    iput-object p1, p0, Lorg/htmlparser/Parser;->mLexer:Lorg/htmlparser/lexer/Lexer;

    .line 517
    iget-object p1, p0, Lorg/htmlparser/Parser;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object p1

    invoke-virtual {p1}, Lorg/htmlparser/lexer/Page;->getContentType()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    const-string v0, "text"

    .line 518
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 519
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getFeedback()Lorg/htmlparser/util/ParserFeedback;

    move-result-object p1

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "URL "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 521
    iget-object v1, p0, Lorg/htmlparser/Parser;->mLexer:Lorg/htmlparser/lexer/Lexer;

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-virtual {v1}, Lorg/htmlparser/lexer/Page;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " does not contain text"

    .line 522
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 519
    invoke-interface {p1, v0}, Lorg/htmlparser/util/ParserFeedback;->warning(Ljava/lang/String;)V

    :cond_2
    return-void

    .line 508
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "lexer cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNodeFactory(Lorg/htmlparser/NodeFactory;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 555
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/lexer/Lexer;->setNodeFactory(Lorg/htmlparser/NodeFactory;)V

    return-void

    .line 554
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "node factory cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setResource(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    if-eqz p1, :cond_4

    .line 382
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_0

    goto :goto_1

    .line 386
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 387
    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 v0, 0x3c

    if-ne v0, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 395
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    new-instance v1, Lorg/htmlparser/lexer/Page;

    invoke-direct {v1, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    goto :goto_2

    .line 397
    :cond_2
    new-instance v0, Lorg/htmlparser/lexer/Lexer;

    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/htmlparser/http/ConnectionManager;->openConnection(Ljava/lang/String;)Ljava/net/URLConnection;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/htmlparser/lexer/Lexer;-><init>(Ljava/net/URLConnection;)V

    invoke-virtual {p0, v0}, Lorg/htmlparser/Parser;->setLexer(Lorg/htmlparser/lexer/Lexer;)V

    :goto_2
    return-void

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 381
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "resource cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :goto_3
    throw p1

    :goto_4
    goto :goto_3
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 447
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    invoke-static {}, Lorg/htmlparser/Parser;->getConnectionManager()Lorg/htmlparser/http/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/http/ConnectionManager;->openConnection(Ljava/lang/String;)Ljava/net/URLConnection;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/htmlparser/Parser;->setConnection(Ljava/net/URLConnection;)V

    :cond_0
    return-void
.end method

.method public visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 723
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->beginParsing()V

    .line 724
    invoke-virtual {p0}, Lorg/htmlparser/Parser;->elements()Lorg/htmlparser/util/NodeIterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/NodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_0

    .line 729
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->finishedParsing()V

    return-void

    .line 726
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/util/NodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v1

    .line 727
    invoke-interface {v1, p1}, Lorg/htmlparser/Node;->accept(Lorg/htmlparser/visitors/NodeVisitor;)V

    goto :goto_0
.end method
