.class public Lorg/htmlparser/visitors/UrlModifyingVisitor;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "UrlModifyingVisitor.java"


# instance fields
.field private linkPrefix:Ljava/lang/String;

.field private modifiedResult:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 41
    invoke-direct {p0, v0, v0}, Lorg/htmlparser/visitors/NodeVisitor;-><init>(ZZ)V

    .line 42
    iput-object p1, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->linkPrefix:Ljava/lang/String;

    .line 43
    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object p1, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    return-void
.end method


# virtual methods
.method public getModifiedResult()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public visitEndTag(Lorg/htmlparser/Tag;)V
    .locals 1

    .line 75
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->toHtml()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 81
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/Node;->getParent()Lorg/htmlparser/Node;

    move-result-object p1

    if-nez p1, :cond_1

    .line 83
    iget-object p1, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-interface {v0}, Lorg/htmlparser/Node;->toHtml()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    :goto_0
    return-void
.end method

.method public visitRemarkNode(Lorg/htmlparser/Remark;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-interface {p1}, Lorg/htmlparser/Remark;->toHtml()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method public visitStringNode(Lorg/htmlparser/Text;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-interface {p1}, Lorg/htmlparser/Text;->toHtml()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method public visitTag(Lorg/htmlparser/Tag;)V
    .locals 3

    .line 58
    instance-of v0, p1, Lorg/htmlparser/tags/LinkTag;

    if-eqz v0, :cond_0

    .line 59
    move-object v0, p1

    check-cast v0, Lorg/htmlparser/tags/LinkTag;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->linkPrefix:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/tags/LinkTag;->setLink(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_0
    instance-of v0, p1, Lorg/htmlparser/tags/ImageTag;

    if-eqz v0, :cond_1

    .line 61
    move-object v0, p1

    check-cast v0, Lorg/htmlparser/tags/ImageTag;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->linkPrefix:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/htmlparser/tags/ImageTag;->getImageURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/tags/ImageTag;->setImageURL(Ljava/lang/String;)V

    .line 66
    :cond_1
    :goto_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getParent()Lorg/htmlparser/Node;

    move-result-object v0

    if-nez v0, :cond_3

    .line 67
    instance-of v0, p1, Lorg/htmlparser/tags/CompositeTag;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lorg/htmlparser/tags/CompositeTag;

    invoke-virtual {v0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-nez v0, :cond_3

    .line 68
    :cond_2
    iget-object v0, p0, Lorg/htmlparser/visitors/UrlModifyingVisitor;->modifiedResult:Ljava/lang/StringBuffer;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->toHtml()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    return-void
.end method
