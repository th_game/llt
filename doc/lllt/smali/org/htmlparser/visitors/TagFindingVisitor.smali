.class public Lorg/htmlparser/visitors/TagFindingVisitor;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "TagFindingVisitor.java"


# instance fields
.field private count:[I

.field private endTagCheck:Z

.field private endTagCount:[I

.field private endTags:[Lorg/htmlparser/util/NodeList;

.field private tags:[Lorg/htmlparser/util/NodeList;

.field private tagsToBeFound:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/visitors/TagFindingVisitor;-><init>([Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Z)V
    .locals 3

    .line 44
    invoke-direct {p0}, Lorg/htmlparser/visitors/NodeVisitor;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tagsToBeFound:[Ljava/lang/String;

    .line 46
    array-length v0, p1

    new-array v0, v0, [Lorg/htmlparser/util/NodeList;

    iput-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tags:[Lorg/htmlparser/util/NodeList;

    if-eqz p2, :cond_0

    .line 48
    array-length v0, p1

    new-array v0, v0, [Lorg/htmlparser/util/NodeList;

    iput-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTags:[Lorg/htmlparser/util/NodeList;

    .line 49
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTagCount:[I

    :cond_0
    const/4 v0, 0x0

    .line 51
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 56
    array-length p1, p1

    new-array p1, p1, [I

    iput-object p1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->count:[I

    .line 57
    iput-boolean p2, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTagCheck:Z

    return-void

    .line 52
    :cond_1
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tags:[Lorg/htmlparser/util/NodeList;

    new-instance v2, Lorg/htmlparser/util/NodeList;

    invoke-direct {v2}, Lorg/htmlparser/util/NodeList;-><init>()V

    aput-object v2, v1, v0

    if-eqz p2, :cond_2

    .line 54
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTags:[Lorg/htmlparser/util/NodeList;

    new-instance v2, Lorg/htmlparser/util/NodeList;

    invoke-direct {v2}, Lorg/htmlparser/util/NodeList;-><init>()V

    aput-object v2, v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getEndTagCount(I)I
    .locals 1

    .line 89
    iget-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTagCount:[I

    aget p1, v0, p1

    return p1
.end method

.method public getTagCount(I)I
    .locals 1

    .line 61
    iget-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->count:[I

    aget p1, v0, p1

    return p1
.end method

.method public getTags(I)[Lorg/htmlparser/Node;
    .locals 1

    .line 85
    iget-object v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tags:[Lorg/htmlparser/util/NodeList;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lorg/htmlparser/util/NodeList;->toNodeArray()[Lorg/htmlparser/Node;

    move-result-object p1

    return-object p1
.end method

.method public visitEndTag(Lorg/htmlparser/Tag;)V
    .locals 3

    .line 75
    iget-boolean v0, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTagCheck:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 76
    :goto_0
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tagsToBeFound:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    return-void

    .line 77
    :cond_1
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tagsToBeFound:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTagCount:[I

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    .line 80
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->endTags:[Lorg/htmlparser/util/NodeList;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public visitTag(Lorg/htmlparser/Tag;)V
    .locals 3

    const/4 v0, 0x0

    .line 66
    :goto_0
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tagsToBeFound:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tagsToBeFound:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->count:[I

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    .line 69
    iget-object v1, p0, Lorg/htmlparser/visitors/TagFindingVisitor;->tags:[Lorg/htmlparser/util/NodeList;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
