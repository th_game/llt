.class public Lorg/htmlparser/visitors/StringFindingVisitor;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "StringFindingVisitor.java"


# instance fields
.field private foundCount:I

.field private locale:Ljava/util/Locale;

.field private multipleSearchesWithinStrings:Z

.field private stringToFind:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/visitors/StringFindingVisitor;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lorg/htmlparser/visitors/NodeVisitor;-><init>()V

    if-nez p2, :cond_0

    .line 46
    sget-object p2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    :cond_0
    iput-object p2, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->locale:Ljava/util/Locale;

    .line 47
    iget-object p2, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->locale:Ljava/util/Locale;

    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->stringToFind:Ljava/lang/String;

    const/4 p1, 0x0

    .line 48
    iput p1, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    .line 49
    iput-boolean p1, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->multipleSearchesWithinStrings:Z

    return-void
.end method


# virtual methods
.method public doMultipleSearchesWithinStrings()V
    .locals 1

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->multipleSearchesWithinStrings:Z

    return-void
.end method

.method public stringFoundCount()I
    .locals 1

    .line 80
    iget v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    return v0
.end method

.method public stringWasFound()Z
    .locals 1

    .line 75
    invoke-virtual {p0}, Lorg/htmlparser/visitors/StringFindingVisitor;->stringFoundCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public visitStringNode(Lorg/htmlparser/Text;)V
    .locals 3

    .line 59
    invoke-interface {p1}, Lorg/htmlparser/Text;->getText()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 60
    iget-boolean v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->multipleSearchesWithinStrings:Z

    const/4 v1, -0x1

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->stringToFind:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 62
    iget p1, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    goto :goto_0

    .line 63
    :cond_0
    iget-boolean v0, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->multipleSearchesWithinStrings:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    .line 66
    :cond_1
    iget-object v2, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->stringToFind:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 68
    iget v2, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/htmlparser/visitors/StringFindingVisitor;->foundCount:I

    :cond_2
    if-ne v0, v1, :cond_1

    :cond_3
    :goto_0
    return-void
.end method
