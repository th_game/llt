.class public Lorg/htmlparser/visitors/HtmlPage;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "HtmlPage.java"


# instance fields
.field private nodesInBody:Lorg/htmlparser/util/NodeList;

.field private tables:Lorg/htmlparser/util/NodeList;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/htmlparser/Parser;)V
    .locals 0

    const/4 p1, 0x1

    .line 41
    invoke-direct {p0, p1}, Lorg/htmlparser/visitors/NodeVisitor;-><init>(Z)V

    const-string p1, ""

    .line 42
    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->title:Ljava/lang/String;

    .line 43
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->nodesInBody:Lorg/htmlparser/util/NodeList;

    .line 44
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->tables:Lorg/htmlparser/util/NodeList;

    return-void
.end method

.method private isBodyTag(Lorg/htmlparser/Tag;)Z
    .locals 0

    .line 72
    instance-of p1, p1, Lorg/htmlparser/tags/BodyTag;

    return p1
.end method

.method private isTable(Lorg/htmlparser/Tag;)Z
    .locals 0

    .line 67
    instance-of p1, p1, Lorg/htmlparser/tags/TableTag;

    return p1
.end method

.method private isTitleTag(Lorg/htmlparser/Tag;)Z
    .locals 0

    .line 77
    instance-of p1, p1, Lorg/htmlparser/tags/TitleTag;

    return p1
.end method


# virtual methods
.method public getBody()Lorg/htmlparser/util/NodeList;
    .locals 1

    .line 81
    iget-object v0, p0, Lorg/htmlparser/visitors/HtmlPage;->nodesInBody:Lorg/htmlparser/util/NodeList;

    return-object v0
.end method

.method public getTables()[Lorg/htmlparser/tags/TableTag;
    .locals 2

    .line 86
    iget-object v0, p0, Lorg/htmlparser/visitors/HtmlPage;->tables:Lorg/htmlparser/util/NodeList;

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    new-array v0, v0, [Lorg/htmlparser/tags/TableTag;

    .line 87
    iget-object v1, p0, Lorg/htmlparser/visitors/HtmlPage;->tables:Lorg/htmlparser/util/NodeList;

    invoke-virtual {v1, v0}, Lorg/htmlparser/util/NodeList;->copyToNodeArray([Lorg/htmlparser/Node;)V

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lorg/htmlparser/visitors/HtmlPage;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->title:Ljava/lang/String;

    return-void
.end method

.method public visitTag(Lorg/htmlparser/Tag;)V
    .locals 1

    .line 57
    invoke-direct {p0, p1}, Lorg/htmlparser/visitors/HtmlPage;->isTable(Lorg/htmlparser/Tag;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lorg/htmlparser/visitors/HtmlPage;->tables:Lorg/htmlparser/util/NodeList;

    invoke-virtual {v0, p1}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_0

    .line 59
    :cond_0
    invoke-direct {p0, p1}, Lorg/htmlparser/visitors/HtmlPage;->isBodyTag(Lorg/htmlparser/Tag;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->nodesInBody:Lorg/htmlparser/util/NodeList;

    goto :goto_0

    .line 61
    :cond_1
    invoke-direct {p0, p1}, Lorg/htmlparser/visitors/HtmlPage;->isTitleTag(Lorg/htmlparser/Tag;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    check-cast p1, Lorg/htmlparser/tags/TitleTag;

    invoke-virtual {p1}, Lorg/htmlparser/tags/TitleTag;->getTitle()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/visitors/HtmlPage;->title:Ljava/lang/String;

    :cond_2
    :goto_0
    return-void
.end method
