.class public Lorg/htmlparser/visitors/LinkFindingVisitor;
.super Lorg/htmlparser/visitors/NodeVisitor;
.source "LinkFindingVisitor.java"


# instance fields
.field private count:I

.field private linkTextToFind:Ljava/lang/String;

.field private locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, v0}, Lorg/htmlparser/visitors/LinkFindingVisitor;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1

    .line 44
    invoke-direct {p0}, Lorg/htmlparser/visitors/NodeVisitor;-><init>()V

    const/4 v0, 0x0

    .line 46
    iput v0, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->count:I

    if-nez p2, :cond_0

    .line 47
    sget-object p2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    :cond_0
    iput-object p2, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->locale:Ljava/util/Locale;

    .line 48
    iget-object p2, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->locale:Ljava/util/Locale;

    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->linkTextToFind:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 65
    iget v0, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->count:I

    return v0
.end method

.method public linkTextFound()Z
    .locals 1

    .line 60
    iget v0, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->count:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public visitTag(Lorg/htmlparser/Tag;)V
    .locals 2

    .line 53
    instance-of v0, p1, Lorg/htmlparser/tags/LinkTag;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    .line 54
    check-cast p1, Lorg/htmlparser/tags/LinkTag;

    invoke-virtual {p1}, Lorg/htmlparser/tags/LinkTag;->getLinkText()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->linkTextToFind:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-eq v0, p1, :cond_0

    .line 55
    iget p1, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->count:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lorg/htmlparser/visitors/LinkFindingVisitor;->count:I

    :cond_0
    return-void
.end method
