.class public Lorg/htmlparser/sax/Attributes;
.super Ljava/lang/Object;
.source "Attributes.java"

# interfaces
.implements Lorg/xml/sax/Attributes;


# instance fields
.field protected mParts:[Ljava/lang/String;

.field protected mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

.field protected mTag:Lorg/htmlparser/Tag;


# direct methods
.method public constructor <init>(Lorg/htmlparser/Tag;Lorg/xml/sax/helpers/NamespaceSupport;[Ljava/lang/String;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    .line 66
    iput-object p2, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    .line 67
    iput-object p3, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIndex(Ljava/lang/String;)I
    .locals 3

    .line 261
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    iget-object v1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    .line 262
    iget-object p1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    aget-object p1, p1, v2

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/sax/Attributes;->getIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    .line 227
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, -0x1

    if-eqz v0, :cond_2

    .line 230
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x1

    const/4 v1, 0x1

    const/4 v4, -0x1

    :goto_0
    if-lt v1, v2, :cond_0

    move v1, v4

    goto :goto_1

    .line 233
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/htmlparser/Attribute;

    .line 234
    invoke-virtual {v5}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 237
    iget-object v6, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    iget-object v7, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    invoke-virtual {v6, v5, v7, v3}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    .line 238
    iget-object v5, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 239
    iget-object v6, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {p2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    and-int/2addr v5, v6

    if-eqz v5, :cond_1

    move v4, v1

    move v1, v2

    :cond_1
    add-int/2addr v1, v3

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method public getLength()I
    .locals 1

    .line 91
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getLocalName(I)Ljava/lang/String;
    .locals 3

    .line 122
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-virtual {p0, p1}, Lorg/htmlparser/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    .line 123
    iget-object p1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    aget-object p1, p1, v2

    return-object p1
.end method

.method public getQName(I)Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/htmlparser/Attribute;

    .line 142
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->isWhitespace()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "#text"

    goto :goto_0

    .line 145
    :cond_0
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getType(I)Ljava/lang/String;
    .locals 0

    const-string p1, "CDATA"

    return-object p1
.end method

.method public getType(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getURI(I)Ljava/lang/String;
    .locals 3

    .line 106
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-virtual {p0, p1}, Lorg/htmlparser/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    .line 107
    iget-object p1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    return-object p1
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 1

    .line 194
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/htmlparser/Attribute;

    .line 195
    invoke-virtual {p1}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    return-object p1
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 333
    iget-object v0, p0, Lorg/htmlparser/sax/Attributes;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    iget-object v1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    .line 334
    iget-object p1, p0, Lorg/htmlparser/sax/Attributes;->mParts:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    aget-object p1, p1, v2

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 316
    iget-object p1, p0, Lorg/htmlparser/sax/Attributes;->mTag:Lorg/htmlparser/Tag;

    invoke-interface {p1, p2}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
