.class public Lorg/htmlparser/sax/Feedback;
.super Ljava/lang/Object;
.source "Feedback.java"

# interfaces
.implements Lorg/htmlparser/util/ParserFeedback;


# instance fields
.field protected mErrorHandler:Lorg/xml/sax/ErrorHandler;

.field protected mLocator:Lorg/xml/sax/Locator;


# direct methods
.method public constructor <init>(Lorg/xml/sax/ErrorHandler;Lorg/xml/sax/Locator;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lorg/htmlparser/sax/Feedback;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    .line 61
    iput-object p2, p0, Lorg/htmlparser/sax/Feedback;->mLocator:Lorg/xml/sax/Locator;

    return-void
.end method


# virtual methods
.method public error(Ljava/lang/String;Lorg/htmlparser/util/ParserException;)V
    .locals 3

    .line 102
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/sax/Feedback;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    .line 103
    new-instance v1, Lorg/xml/sax/SAXParseException;

    iget-object v2, p0, Lorg/htmlparser/sax/Feedback;->mLocator:Lorg/xml/sax/Locator;

    invoke-direct {v1, p1, v2, p2}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;Ljava/lang/Exception;)V

    .line 102
    invoke-interface {v0, v1}, Lorg/xml/sax/ErrorHandler;->error(Lorg/xml/sax/SAXParseException;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 107
    invoke-virtual {p1}, Lorg/xml/sax/SAXException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public warning(Ljava/lang/String;)V
    .locals 3

    .line 83
    :try_start_0
    iget-object v0, p0, Lorg/htmlparser/sax/Feedback;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    .line 84
    new-instance v1, Lorg/xml/sax/SAXParseException;

    iget-object v2, p0, Lorg/htmlparser/sax/Feedback;->mLocator:Lorg/xml/sax/Locator;

    invoke-direct {v1, p1, v2}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;)V

    .line 83
    invoke-interface {v0, v1}, Lorg/xml/sax/ErrorHandler;->warning(Lorg/xml/sax/SAXParseException;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 88
    invoke-virtual {p1}, Lorg/xml/sax/SAXException;->printStackTrace()V

    :goto_0
    return-void
.end method
