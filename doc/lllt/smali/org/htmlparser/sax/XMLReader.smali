.class public Lorg/htmlparser/sax/XMLReader;
.super Ljava/lang/Object;
.source "XMLReader.java"

# interfaces
.implements Lorg/xml/sax/XMLReader;


# instance fields
.field protected mContentHandler:Lorg/xml/sax/ContentHandler;

.field protected mDTDHandler:Lorg/xml/sax/DTDHandler;

.field protected mEntityResolver:Lorg/xml/sax/EntityResolver;

.field protected mErrorHandler:Lorg/xml/sax/ErrorHandler;

.field protected mNameSpacePrefixes:Z

.field protected mNameSpaces:Z

.field protected mParser:Lorg/htmlparser/Parser;

.field protected mParts:[Ljava/lang/String;

.field protected mSupport:Lorg/xml/sax/helpers/NamespaceSupport;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 132
    iput-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    const/4 v0, 0x0

    .line 133
    iput-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpacePrefixes:Z

    const/4 v0, 0x0

    .line 135
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mEntityResolver:Lorg/xml/sax/EntityResolver;

    .line 136
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mDTDHandler:Lorg/xml/sax/DTDHandler;

    .line 137
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    .line 138
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    .line 140
    new-instance v0, Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-direct {v0}, Lorg/xml/sax/helpers/NamespaceSupport;-><init>()V

    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    .line 141
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-virtual {v0}, Lorg/xml/sax/helpers/NamespaceSupport;->pushContext()V

    .line 142
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    const-string v1, ""

    const-string v2, "http://www.w3.org/TR/REC-html40"

    invoke-virtual {v0, v1, v2}, Lorg/xml/sax/helpers/NamespaceSupport;->declarePrefix(Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 146
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected doSAX(Lorg/htmlparser/Node;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 633
    instance-of v0, p1, Lorg/htmlparser/Remark;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-interface {p1}, Lorg/htmlparser/Node;->getStartPosition()I

    move-result v2

    invoke-interface {p1}, Lorg/htmlparser/Node;->getEndPosition()I

    move-result p1

    invoke-virtual {v0, v2, p1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object p1

    .line 636
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-interface {v0, v2, v1, p1}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    goto/16 :goto_6

    .line 638
    :cond_0
    instance-of v0, p1, Lorg/htmlparser/Text;

    if-eqz v0, :cond_1

    .line 640
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->getLexer()Lorg/htmlparser/lexer/Lexer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/lexer/Lexer;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-interface {p1}, Lorg/htmlparser/Node;->getStartPosition()I

    move-result v2

    invoke-interface {p1}, Lorg/htmlparser/Node;->getEndPosition()I

    move-result p1

    invoke-virtual {v0, v2, p1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object p1

    .line 641
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-interface {v0, v2, v1, p1}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    goto/16 :goto_6

    .line 643
    :cond_1
    instance-of v0, p1, Lorg/htmlparser/Tag;

    if-eqz v0, :cond_a

    .line 645
    check-cast p1, Lorg/htmlparser/Tag;

    .line 646
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    const/4 v2, 0x1

    const-string v3, ""

    if-eqz v0, :cond_2

    .line 647
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v1}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    goto :goto_0

    .line 650
    :cond_2
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aput-object v3, v0, v1

    .line 651
    aput-object v3, v0, v2

    .line 653
    :goto_0
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpacePrefixes:Z

    const/4 v4, 0x2

    if-eqz v0, :cond_3

    .line 654
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    goto :goto_1

    .line 655
    :cond_3
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    if-eqz v0, :cond_4

    .line 656
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aput-object v3, v0, v4

    goto :goto_1

    .line 658
    :cond_4
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 660
    :goto_1
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    .line 661
    iget-object v5, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aget-object v6, v5, v1

    .line 662
    aget-object v7, v5, v2

    .line 663
    aget-object v8, v5, v4

    .line 664
    new-instance v9, Lorg/htmlparser/sax/Attributes;

    iget-object v10, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-direct {v9, p1, v10, v5}, Lorg/htmlparser/sax/Attributes;-><init>(Lorg/htmlparser/Tag;Lorg/xml/sax/helpers/NamespaceSupport;[Ljava/lang/String;)V

    .line 660
    invoke-interface {v0, v6, v7, v8, v9}, Lorg/xml/sax/ContentHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 665
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_6

    const/4 v5, 0x0

    .line 667
    :goto_2
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v6

    if-lt v5, v6, :cond_5

    goto :goto_3

    .line 668
    :cond_5
    invoke-virtual {v0, v5}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/htmlparser/sax/XMLReader;->doSAX(Lorg/htmlparser/Node;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 669
    :cond_6
    :goto_3
    invoke-interface {p1}, Lorg/htmlparser/Tag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object p1

    if-eqz p1, :cond_a

    .line 672
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    if-eqz v0, :cond_7

    .line 673
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mSupport:Lorg/xml/sax/helpers/NamespaceSupport;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-virtual {v0, v5, v6, v1}, Lorg/xml/sax/helpers/NamespaceSupport;->processName(Ljava/lang/String;[Ljava/lang/String;Z)[Ljava/lang/String;

    goto :goto_4

    .line 676
    :cond_7
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aput-object v3, v0, v1

    .line 677
    aput-object v3, v0, v2

    .line 679
    :goto_4
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpacePrefixes:Z

    if-eqz v0, :cond_8

    .line 680
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v4

    goto :goto_5

    .line 681
    :cond_8
    iget-boolean v0, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    if-eqz v0, :cond_9

    .line 682
    iget-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aput-object v3, p1, v4

    goto :goto_5

    .line 684
    :cond_9
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    invoke-interface {p1}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v4

    .line 685
    :goto_5
    iget-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    .line 686
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParts:[Ljava/lang/String;

    aget-object v1, v0, v1

    .line 687
    aget-object v2, v0, v2

    .line 688
    aget-object v0, v0, v4

    .line 685
    invoke-interface {p1, v1, v2, v0}, Lorg/xml/sax/ContentHandler;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_6
    return-void
.end method

.method public getContentHandler()Lorg/xml/sax/ContentHandler;
    .locals 1

    .line 422
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    return-object v0
.end method

.method public getDTDHandler()Lorg/xml/sax/DTDHandler;
    .locals 1

    .line 389
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mDTDHandler:Lorg/xml/sax/DTDHandler;

    return-object v0
.end method

.method public getEntityResolver()Lorg/xml/sax/EntityResolver;
    .locals 1

    .line 357
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mEntityResolver:Lorg/xml/sax/EntityResolver;

    return-object v0
.end method

.method public getErrorHandler()Lorg/xml/sax/ErrorHandler;
    .locals 1

    .line 457
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    return-object v0
.end method

.method public getFeature(Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    const-string v0, "http://xml.org/sax/features/namespaces"

    .line 215
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-boolean p1, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    goto :goto_0

    :cond_0
    const-string v0, "http://xml.org/sax/features/namespace-prefixes"

    .line 217
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    iget-boolean p1, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpacePrefixes:Z

    :goto_0
    return p1

    .line 220
    :cond_1
    new-instance v0, Lorg/xml/sax/SAXNotSupportedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, " not yet understood"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/xml/sax/SAXNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    .line 289
    new-instance v0, Lorg/xml/sax/SAXNotSupportedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, " not yet understood"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/xml/sax/SAXNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parse(Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 579
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v0, :cond_2

    .line 582
    :try_start_0
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0, p1}, Lorg/htmlparser/Parser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    .line 583
    new-instance v0, Lorg/htmlparser/sax/Locator;

    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-direct {v0, v1}, Lorg/htmlparser/sax/Locator;-><init>(Lorg/htmlparser/Parser;)V

    .line 584
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v1, :cond_0

    .line 585
    new-instance v1, Lorg/htmlparser/sax/Feedback;

    iget-object v2, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    invoke-direct {v1, v2, v0}, Lorg/htmlparser/sax/Feedback;-><init>(Lorg/xml/sax/ErrorHandler;Lorg/xml/sax/Locator;)V

    goto :goto_0

    .line 587
    :cond_0
    new-instance v1, Lorg/htmlparser/util/DefaultParserFeedback;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/htmlparser/util/DefaultParserFeedback;-><init>(I)V

    .line 588
    :goto_0
    iget-object v2, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v2, v1}, Lorg/htmlparser/Parser;->setFeedback(Lorg/htmlparser/util/ParserFeedback;)V

    .line 591
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v1, v0}, Lorg/xml/sax/ContentHandler;->setDocumentLocator(Lorg/xml/sax/Locator;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 594
    :try_start_1
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v1}, Lorg/xml/sax/ContentHandler;->startDocument()V

    .line 595
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1}, Lorg/htmlparser/Parser;->elements()Lorg/htmlparser/util/NodeIterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_1

    .line 597
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v1}, Lorg/xml/sax/ContentHandler;->endDocument()V

    goto :goto_2

    .line 596
    :cond_1
    invoke-interface {v1}, Lorg/htmlparser/util/NodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/htmlparser/sax/XMLReader;->doSAX(Lorg/htmlparser/Node;)V
    :try_end_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 601
    :try_start_2
    iget-object v2, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v2, :cond_2

    .line 602
    iget-object v2, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    .line 603
    new-instance v3, Lorg/xml/sax/SAXParseException;

    const-string v4, "contentHandler threw me"

    invoke-direct {v3, v4, v0, v1}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;Ljava/lang/Exception;)V

    .line 602
    invoke-interface {v2, v3}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V
    :try_end_2
    .catch Lorg/htmlparser/util/ParserException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 608
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v1, :cond_2

    .line 610
    new-instance v8, Lorg/xml/sax/SAXParseException;

    invoke-virtual {v0}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v4, ""

    move-object v2, v8

    move-object v5, p1

    invoke-direct/range {v2 .. v7}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 609
    invoke-interface {v1, v8}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V

    :cond_2
    :goto_2
    return-void
.end method

.method public parse(Lorg/xml/sax/InputSource;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 513
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v0, :cond_2

    .line 516
    :try_start_0
    new-instance v0, Lorg/htmlparser/Parser;

    .line 517
    new-instance v1, Lorg/htmlparser/lexer/Lexer;

    .line 518
    new-instance v2, Lorg/htmlparser/lexer/Page;

    .line 519
    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getByteStream()Ljava/io/InputStream;

    move-result-object v3

    .line 520
    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getEncoding()Ljava/lang/String;

    move-result-object p1

    .line 518
    invoke-direct {v2, v3, p1}, Lorg/htmlparser/lexer/Page;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 517
    invoke-direct {v1, v2}, Lorg/htmlparser/lexer/Lexer;-><init>(Lorg/htmlparser/lexer/Page;)V

    invoke-direct {v0, v1}, Lorg/htmlparser/Parser;-><init>(Lorg/htmlparser/lexer/Lexer;)V

    .line 516
    iput-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    .line 521
    new-instance p1, Lorg/htmlparser/sax/Locator;

    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-direct {p1, v0}, Lorg/htmlparser/sax/Locator;-><init>(Lorg/htmlparser/Parser;)V

    .line 522
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v0, :cond_0

    .line 523
    new-instance v0, Lorg/htmlparser/sax/Feedback;

    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    invoke-direct {v0, v1, p1}, Lorg/htmlparser/sax/Feedback;-><init>(Lorg/xml/sax/ErrorHandler;Lorg/xml/sax/Locator;)V

    goto :goto_0

    .line 525
    :cond_0
    new-instance v0, Lorg/htmlparser/util/DefaultParserFeedback;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/htmlparser/util/DefaultParserFeedback;-><init>(I)V

    .line 526
    :goto_0
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v1, v0}, Lorg/htmlparser/Parser;->setFeedback(Lorg/htmlparser/util/ParserFeedback;)V

    .line 527
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1}, Lorg/xml/sax/ContentHandler;->setDocumentLocator(Lorg/xml/sax/Locator;)V
    :try_end_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 530
    :try_start_1
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->startDocument()V

    .line 531
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mParser:Lorg/htmlparser/Parser;

    invoke-virtual {v0}, Lorg/htmlparser/Parser;->elements()Lorg/htmlparser/util/NodeIterator;

    move-result-object v0

    .line 532
    :goto_1
    invoke-interface {v0}, Lorg/htmlparser/util/NodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_1

    .line 534
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->endDocument()V

    goto :goto_2

    .line 533
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/NodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/htmlparser/sax/XMLReader;->doSAX(Lorg/htmlparser/Node;)V
    :try_end_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/htmlparser/util/ParserException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    .line 538
    :try_start_2
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v1, :cond_2

    .line 539
    iget-object v1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    new-instance v2, Lorg/xml/sax/SAXParseException;

    const-string v3, "contentHandler threw me"

    .line 540
    invoke-direct {v2, v3, p1, v0}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;Ljava/lang/Exception;)V

    .line 539
    invoke-interface {v1, v2}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V
    :try_end_2
    .catch Lorg/htmlparser/util/ParserException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 545
    iget-object v0, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz v0, :cond_2

    .line 546
    new-instance v7, Lorg/xml/sax/SAXParseException;

    .line 547
    invoke-virtual {p1}, Lorg/htmlparser/util/ParserException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v3, ""

    const-string v4, ""

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 546
    invoke-interface {v0, v7}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V

    :cond_2
    :goto_2
    return-void
.end method

.method public setContentHandler(Lorg/xml/sax/ContentHandler;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mContentHandler:Lorg/xml/sax/ContentHandler;

    return-void
.end method

.method public setDTDHandler(Lorg/xml/sax/DTDHandler;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mDTDHandler:Lorg/xml/sax/DTDHandler;

    return-void
.end method

.method public setEntityResolver(Lorg/xml/sax/EntityResolver;)V
    .locals 0

    .line 344
    iput-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mEntityResolver:Lorg/xml/sax/EntityResolver;

    return-void
.end method

.method public setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    .locals 0

    .line 444
    iput-object p1, p0, Lorg/htmlparser/sax/XMLReader;->mErrorHandler:Lorg/xml/sax/ErrorHandler;

    return-void
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    const-string v0, "http://xml.org/sax/features/namespaces"

    .line 252
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iput-boolean p2, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpaces:Z

    goto :goto_0

    :cond_0
    const-string v0, "http://xml.org/sax/features/namespace-prefixes"

    .line 254
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    iput-boolean p2, p0, Lorg/htmlparser/sax/XMLReader;->mNameSpacePrefixes:Z

    :goto_0
    return-void

    .line 257
    :cond_1
    new-instance p2, Lorg/xml/sax/SAXNotSupportedException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, " not yet understood"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lorg/xml/sax/SAXNotSupportedException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    .line 321
    new-instance p2, Lorg/xml/sax/SAXNotSupportedException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, " not yet understood"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lorg/xml/sax/SAXNotSupportedException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
