.class public Lorg/htmlparser/tags/ObjectTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "ObjectTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "OBJECT"

    aput-object v3, v1, v2

    .line 50
    sput-object v1, Lorg/htmlparser/tags/ObjectTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "BODY"

    aput-object v3, v1, v2

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 55
    sput-object v1, Lorg/htmlparser/tags/ObjectTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public createObjectParamsTable()Ljava/util/Hashtable;
    .locals 6

    .line 95
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 96
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 98
    :goto_0
    invoke-virtual {v1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto :goto_1

    .line 100
    :cond_0
    iget-object v3, p0, Lorg/htmlparser/tags/ObjectTag;->children:Lorg/htmlparser/util/NodeList;

    invoke-virtual {v3, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    .line 101
    instance-of v4, v3, Lorg/htmlparser/Tag;

    if-eqz v4, :cond_1

    .line 103
    check-cast v3, Lorg/htmlparser/Tag;

    .line 104
    invoke-interface {v3}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PARAM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "NAME"

    .line 106
    invoke-interface {v3, v4}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 107
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "VALUE"

    .line 109
    invoke-interface {v3, v5}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 110
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/ObjectTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/ObjectTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getObjectClassId()Ljava/lang/String;
    .locals 1

    const-string v0, "CLASSID"

    .line 125
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectCodeBase()Ljava/lang/String;
    .locals 1

    const-string v0, "CODEBASE"

    .line 134
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectCodeType()Ljava/lang/String;
    .locals 1

    const-string v0, "CODETYPE"

    .line 143
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectData()Ljava/lang/String;
    .locals 1

    const-string v0, "DATA"

    .line 152
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectHeight()Ljava/lang/String;
    .locals 1

    const-string v0, "HEIGHT"

    .line 161
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectParams()Ljava/util/Hashtable;
    .locals 1

    .line 197
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->createObjectParamsTable()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public getObjectStandby()Ljava/lang/String;
    .locals 1

    const-string v0, "STANDBY"

    .line 170
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectType()Ljava/lang/String;
    .locals 1

    const-string v0, "TYPE"

    .line 179
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectWidth()Ljava/lang/String;
    .locals 1

    const-string v0, "WIDTH"

    .line 188
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/ObjectTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectParams()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getParameterNames()Ljava/util/Enumeration;
    .locals 1

    .line 216
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectParams()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public setObjectClassId(Ljava/lang/String;)V
    .locals 1

    const-string v0, "CLASSID"

    .line 225
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectCodeBase(Ljava/lang/String;)V
    .locals 1

    const-string v0, "CODEBASE"

    .line 234
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectCodeType(Ljava/lang/String;)V
    .locals 1

    const-string v0, "CODETYPE"

    .line 243
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectData(Ljava/lang/String;)V
    .locals 1

    const-string v0, "DATA"

    .line 252
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectHeight(Ljava/lang/String;)V
    .locals 1

    const-string v0, "HEIGHT"

    .line 261
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectParams(Ljava/util/Hashtable;)V
    .locals 12

    .line 305
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    const-string v1, "PARAM"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 307
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    :goto_0
    move-object v4, v0

    goto :goto_2

    :cond_0
    const/4 v3, 0x0

    .line 310
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-lt v3, v4, :cond_3

    goto :goto_0

    .line 336
    :goto_2
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-nez v0, :cond_2

    .line 351
    invoke-virtual {p0, v4}, Lorg/htmlparser/tags/ObjectTag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    return-void

    .line 338
    :cond_2
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 339
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 340
    invoke-virtual {p1, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 341
    new-instance v7, Lorg/htmlparser/Attribute;

    const/4 v8, 0x0

    invoke-direct {v7, v1, v8}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 342
    new-instance v7, Lorg/htmlparser/Attribute;

    const-string v9, " "

    invoke-direct {v7, v9}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 343
    new-instance v7, Lorg/htmlparser/Attribute;

    const/16 v10, 0x22

    const-string v11, "VALUE"

    invoke-direct {v7, v11, v6, v10}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 344
    new-instance v6, Lorg/htmlparser/Attribute;

    invoke-direct {v6, v9}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 345
    new-instance v6, Lorg/htmlparser/Attribute;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v7, "NAME"

    invoke-direct {v6, v7, v3, v10}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    invoke-virtual {v0, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 346
    new-instance v3, Lorg/htmlparser/nodes/TagNode;

    invoke-direct {v3, v8, v2, v2, v0}, Lorg/htmlparser/nodes/TagNode;-><init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V

    .line 347
    invoke-virtual {v4, v3}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_3

    .line 312
    :cond_3
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    .line 313
    instance-of v5, v4, Lorg/htmlparser/Tag;

    if-eqz v5, :cond_4

    .line 314
    check-cast v4, Lorg/htmlparser/Tag;

    invoke-interface {v4}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 316
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    .line 318
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 320
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    .line 321
    instance-of v5, v4, Lorg/htmlparser/nodes/TextNode;

    if-eqz v5, :cond_1

    .line 323
    check-cast v4, Lorg/htmlparser/nodes/TextNode;

    .line 324
    invoke-virtual {v4}, Lorg/htmlparser/nodes/TextNode;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 325
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method

.method public setObjectStandby(Ljava/lang/String;)V
    .locals 1

    const-string v0, "STANDBY"

    .line 270
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectType(Ljava/lang/String;)V
    .locals 1

    const-string v0, "TYPE"

    .line 279
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setObjectWidth(Ljava/lang/String;)V
    .locals 1

    const-string v0, "WIDTH"

    .line 288
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ObjectTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .line 368
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v1, "Object Tag\n"

    .line 369
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "**********\n"

    .line 370
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "ClassId = "

    .line 371
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 372
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectClassId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    .line 373
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "CodeBase = "

    .line 374
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 375
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectCodeBase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 376
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "CodeType = "

    .line 377
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 378
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectCodeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 379
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Data = "

    .line 380
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 382
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Height = "

    .line 383
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 384
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectHeight()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 385
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Standby = "

    .line 386
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 387
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectStandby()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 388
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Type = "

    .line 389
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 390
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Width = "

    .line 392
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectWidth()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 394
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 395
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->getObjectParams()Ljava/util/Hashtable;

    move-result-object v2

    .line 396
    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v3, :cond_0

    const-string v2, "No Params found.\n"

    .line 398
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    .line 400
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-nez v7, :cond_5

    .line 412
    :goto_1
    invoke-virtual {p0}, Lorg/htmlparser/tags/ObjectTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v5, :cond_1

    .line 426
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v1, "End of Object Tag\n"

    .line 427
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "*****************\n"

    .line 428
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 414
    :cond_2
    invoke-interface {v7}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    .line 415
    instance-of v3, v2, Lorg/htmlparser/Tag;

    if-eqz v3, :cond_3

    .line 416
    move-object v3, v2

    check-cast v3, Lorg/htmlparser/Tag;

    invoke-interface {v3}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "PARAM"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    if-nez v5, :cond_4

    const-string v3, "Miscellaneous items :\n"

    .line 419
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_4
    const-string v3, " "

    .line 421
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 423
    :goto_3
    invoke-interface {v2}, Lorg/htmlparser/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v5, 0x1

    goto :goto_2

    .line 402
    :cond_5
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 403
    invoke-virtual {v2, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 404
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v9, ": Parameter name = "

    .line 405
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v7, ", Parameter value = "

    .line 407
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 408
    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 409
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/2addr v6, v4

    goto :goto_0
.end method
