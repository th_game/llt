.class public Lorg/htmlparser/tags/MetaTag;
.super Lorg/htmlparser/nodes/TagNode;
.source "MetaTag.java"


# static fields
.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "META"

    aput-object v2, v0, v1

    .line 43
    sput-object v0, Lorg/htmlparser/tags/MetaTag;->mIds:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Lorg/htmlparser/nodes/TagNode;-><init>()V

    return-void
.end method


# virtual methods
.method public doSemanticAction()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 145
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getHttpEquiv()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Content-Type"

    .line 146
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    const-string v1, "CONTENT"

    invoke-virtual {p0, v1}, Lorg/htmlparser/tags/MetaTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->getCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Page;->setEncoding(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getHttpEquiv()Ljava/lang/String;
    .locals 1

    const-string v0, "HTTP-EQUIV"

    .line 68
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 58
    sget-object v0, Lorg/htmlparser/tags/MetaTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getMetaContent()Ljava/lang/String;
    .locals 1

    const-string v0, "CONTENT"

    .line 78
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetaTagName()Ljava/lang/String;
    .locals 1

    const-string v0, "NAME"

    .line 88
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setHttpEquiv(Ljava/lang/String;)V
    .locals 3

    const-string v0, "HTTP-EQUIV"

    .line 98
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1, p1}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v1

    new-instance v2, Lorg/htmlparser/Attribute;

    invoke-direct {v2, v0, p1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public setMetaTagContents(Ljava/lang/String;)V
    .locals 3

    const-string v0, "CONTENT"

    .line 112
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {v1, p1}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v1

    new-instance v2, Lorg/htmlparser/Attribute;

    invoke-direct {v2, v0, p1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public setMetaTagName(Ljava/lang/String;)V
    .locals 3

    const-string v0, "NAME"

    .line 126
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/MetaTag;->getAttributeEx(Ljava/lang/String;)Lorg/htmlparser/Attribute;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {v1, p1}, Lorg/htmlparser/Attribute;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/MetaTag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v1

    new-instance v2, Lorg/htmlparser/Attribute;

    invoke-direct {v2, v0, p1}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
