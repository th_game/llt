.class public Lorg/htmlparser/tags/ImageTag;
.super Lorg/htmlparser/nodes/TagNode;
.source "ImageTag.java"


# static fields
.field private static final mIds:[Ljava/lang/String;


# instance fields
.field protected imageURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "IMG"

    aput-object v2, v0, v1

    .line 45
    sput-object v0, Lorg/htmlparser/tags/ImageTag;->mIds:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 57
    invoke-direct {p0}, Lorg/htmlparser/nodes/TagNode;-><init>()V

    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public extractImageLocn()Ljava/lang/String;
    .locals 15

    .line 99
    invoke-virtual {p0}, Lorg/htmlparser/tags/ImageTag;->getAttributesEx()Ljava/util/Vector;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const-string v2, ""

    const/4 v3, 0x0

    move-object v6, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v1, :cond_d

    const/4 v7, 0x3

    if-lt v5, v7, :cond_0

    goto/16 :goto_5

    .line 103
    :cond_0
    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/htmlparser/Attribute;

    .line 104
    invoke-virtual {v8}, Lorg/htmlparser/Attribute;->getName()Ljava/lang/String;

    move-result-object v9

    .line 105
    invoke-virtual {v8}, Lorg/htmlparser/Attribute;->getValue()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x2

    const/4 v11, 0x1

    if-eqz v5, :cond_7

    if-eq v5, v11, :cond_3

    if-ne v5, v10, :cond_2

    if-eqz v9, :cond_c

    if-nez v8, :cond_1

    move-object v6, v9

    :cond_1
    :goto_1
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 173
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "we\'re not supposed to in state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-eqz v9, :cond_c

    const-string v7, "="

    .line 147
    invoke-virtual {v9, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 150
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v11, v5, :cond_5

    .line 152
    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    :cond_4
    :goto_2
    move-object v6, v5

    goto :goto_1

    :cond_5
    if-eqz v8, :cond_6

    .line 157
    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_6
    :goto_3
    const/4 v5, 0x2

    goto :goto_4

    :cond_7
    if-eqz v9, :cond_c

    .line 111
    sget-object v12, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v9, v12}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "SRC"

    .line 112
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    if-eqz v8, :cond_9

    .line 117
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    goto :goto_3

    :cond_8
    move v4, v1

    move-object v6, v8

    :cond_9
    const/4 v5, 0x1

    goto :goto_4

    .line 127
    :cond_a
    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 130
    invoke-virtual {v9, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "\""

    .line 132
    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v11, v6, :cond_b

    .line 133
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v11

    invoke-virtual {v5, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :cond_b
    const-string v6, "\'"

    .line 135
    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v11, v6, :cond_4

    .line 136
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v11

    invoke-virtual {v5, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_c
    :goto_4
    add-int/2addr v4, v11

    goto/16 :goto_0

    :cond_d
    :goto_5
    const/16 v0, 0xa

    .line 176
    invoke-static {v6, v0}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    .line 177
    invoke-static {v0, v1}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 68
    sget-object v0, Lorg/htmlparser/tags/ImageTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getImageURL()Ljava/lang/String;
    .locals 2

    .line 188
    iget-object v0, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lorg/htmlparser/tags/ImageTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lorg/htmlparser/tags/ImageTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlparser/tags/ImageTag;->extractImageLocn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlparser/lexer/Page;->getAbsoluteURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    .line 192
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    return-object v0
.end method

.method public setImageURL(Ljava/lang/String;)V
    .locals 1

    .line 201
    iput-object p1, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    .line 202
    iget-object p1, p0, Lorg/htmlparser/tags/ImageTag;->imageURL:Ljava/lang/String;

    const-string v0, "SRC"

    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/ImageTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
