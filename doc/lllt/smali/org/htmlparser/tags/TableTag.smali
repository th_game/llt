.class public Lorg/htmlparser/tags/TableTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "TableTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "TABLE"

    aput-object v3, v1, v2

    .line 45
    sput-object v1, Lorg/htmlparser/tags/TableTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "BODY"

    aput-object v3, v1, v2

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 50
    sput-object v1, Lorg/htmlparser/tags/TableTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 74
    sget-object v0, Lorg/htmlparser/tags/TableTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 65
    sget-object v0, Lorg/htmlparser/tags/TableTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getRow(I)Lorg/htmlparser/tags/TableRow;
    .locals 2

    .line 137
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableTag;->getRows()[Lorg/htmlparser/tags/TableRow;

    move-result-object v0

    .line 138
    array-length v1, v0

    if-ge p1, v1, :cond_0

    .line 139
    aget-object p1, v0, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getRowCount()I
    .locals 1

    .line 124
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableTag;->getRows()[Lorg/htmlparser/tags/TableRow;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getRows()[Lorg/htmlparser/tags/TableRow;
    .locals 7

    .line 89
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    new-instance v1, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v2, Lorg/htmlparser/tags/TableTag;

    invoke-direct {v1, v2}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 93
    new-instance v2, Lorg/htmlparser/filters/HasParentFilter;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 94
    new-instance v3, Lorg/htmlparser/filters/OrFilter;

    .line 95
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    .line 97
    new-instance v5, Lorg/htmlparser/filters/IsEqualFilter;

    invoke-direct {v5, p0}, Lorg/htmlparser/filters/IsEqualFilter;-><init>(Lorg/htmlparser/Node;)V

    .line 95
    invoke-direct {v4, v1, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 98
    new-instance v5, Lorg/htmlparser/filters/AndFilter;

    .line 99
    new-instance v6, Lorg/htmlparser/filters/NotFilter;

    invoke-direct {v6, v1}, Lorg/htmlparser/filters/NotFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 98
    invoke-direct {v5, v6, v2}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 94
    invoke-direct {v3, v4, v5}, Lorg/htmlparser/filters/OrFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 101
    invoke-virtual {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;->setParentFilter(Lorg/htmlparser/NodeFilter;)V

    .line 104
    new-instance v1, Lorg/htmlparser/filters/AndFilter;

    .line 105
    new-instance v2, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v4, Lorg/htmlparser/tags/TableRow;

    invoke-direct {v2, v4}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 104
    invoke-direct {v1, v2, v3}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    const/4 v2, 0x1

    .line 102
    invoke-virtual {v0, v1, v2}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/htmlparser/tags/TableRow;

    .line 108
    invoke-virtual {v0, v1}, Lorg/htmlparser/util/NodeList;->copyToNodeArray([Lorg/htmlparser/Node;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v1, v0, [Lorg/htmlparser/tags/TableRow;

    :goto_0
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TableTag\n********\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableTag;->toHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
