.class public Lorg/htmlparser/tags/AppletTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "AppletTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "APPLET"

    aput-object v3, v1, v2

    .line 52
    sput-object v1, Lorg/htmlparser/tags/AppletTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "BODY"

    aput-object v3, v1, v2

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 57
    sput-object v1, Lorg/htmlparser/tags/AppletTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public createAppletParamsTable()Ljava/util/Hashtable;
    .locals 6

    .line 97
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 98
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 100
    :goto_0
    invoke-virtual {v1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto :goto_1

    .line 102
    :cond_0
    iget-object v3, p0, Lorg/htmlparser/tags/AppletTag;->children:Lorg/htmlparser/util/NodeList;

    invoke-virtual {v3, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v3

    .line 103
    instance-of v4, v3, Lorg/htmlparser/Tag;

    if-eqz v4, :cond_1

    .line 105
    check-cast v3, Lorg/htmlparser/Tag;

    .line 106
    invoke-interface {v3}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PARAM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "NAME"

    .line 108
    invoke-interface {v3, v4}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 109
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "VALUE"

    .line 111
    invoke-interface {v3, v5}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-virtual {v0, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public getAppletClass()Ljava/lang/String;
    .locals 1

    const-string v0, "CODE"

    .line 127
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/AppletTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppletParams()Ljava/util/Hashtable;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->createAppletParamsTable()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public getArchive()Ljava/lang/String;
    .locals 1

    const-string v0, "ARCHIVE"

    .line 145
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/AppletTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCodeBase()Ljava/lang/String;
    .locals 1

    const-string v0, "CODEBASE"

    .line 154
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/AppletTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 81
    sget-object v0, Lorg/htmlparser/tags/AppletTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 72
    sget-object v0, Lorg/htmlparser/tags/AppletTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 165
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getAppletParams()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getParameterNames()Ljava/util/Enumeration;
    .locals 1

    .line 174
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getAppletParams()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public setAppletClass(Ljava/lang/String;)V
    .locals 1

    const-string v0, "CODE"

    .line 183
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/AppletTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAppletParams(Ljava/util/Hashtable;)V
    .locals 12

    .line 200
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    const-string v1, "PARAM"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    :goto_0
    move-object v4, v0

    goto :goto_2

    :cond_0
    const/4 v3, 0x0

    .line 205
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-lt v3, v4, :cond_3

    goto :goto_0

    .line 231
    :goto_2
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    invoke-virtual {p0, v4}, Lorg/htmlparser/tags/AppletTag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    return-void

    .line 233
    :cond_2
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 234
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 235
    invoke-virtual {p1, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 236
    new-instance v7, Lorg/htmlparser/Attribute;

    const/4 v8, 0x0

    invoke-direct {v7, v1, v8}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 237
    new-instance v7, Lorg/htmlparser/Attribute;

    const-string v9, " "

    invoke-direct {v7, v9}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 238
    new-instance v7, Lorg/htmlparser/Attribute;

    const/16 v10, 0x22

    const-string v11, "VALUE"

    invoke-direct {v7, v11, v6, v10}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    invoke-virtual {v0, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 239
    new-instance v6, Lorg/htmlparser/Attribute;

    invoke-direct {v6, v9}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 240
    new-instance v6, Lorg/htmlparser/Attribute;

    const-string v7, "NAME"

    invoke-direct {v6, v7, v3, v10}, Lorg/htmlparser/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;C)V

    invoke-virtual {v0, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 241
    new-instance v3, Lorg/htmlparser/nodes/TagNode;

    invoke-direct {v3, v8, v2, v2, v0}, Lorg/htmlparser/nodes/TagNode;-><init>(Lorg/htmlparser/lexer/Page;IILjava/util/Vector;)V

    .line 242
    invoke-virtual {v4, v3}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_3

    .line 207
    :cond_3
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    .line 208
    instance-of v5, v4, Lorg/htmlparser/Tag;

    if-eqz v5, :cond_4

    .line 209
    check-cast v4, Lorg/htmlparser/Tag;

    invoke-interface {v4}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 211
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    .line 213
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 215
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    .line 216
    instance-of v5, v4, Lorg/htmlparser/Text;

    if-eqz v5, :cond_1

    .line 218
    check-cast v4, Lorg/htmlparser/Text;

    .line 219
    invoke-interface {v4}, Lorg/htmlparser/Text;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 220
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method

.method public setArchive(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ARCHIVE"

    .line 255
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/AppletTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCodeBase(Ljava/lang/String;)V
    .locals 1

    const-string v0, "CODEBASE"

    .line 264
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/AppletTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .line 281
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v1, "Applet Tag\n"

    .line 282
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "**********\n"

    .line 283
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "Class Name = "

    .line 284
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 285
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getAppletClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    .line 286
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Archive = "

    .line 287
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 288
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getArchive()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 289
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "Codebase = "

    .line 290
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getCodeBase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 292
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 293
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->getAppletParams()Ljava/util/Hashtable;

    move-result-object v2

    .line 294
    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v3, :cond_0

    const-string v2, "No Params found.\n"

    .line 296
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    .line 298
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-nez v7, :cond_5

    .line 310
    :goto_1
    invoke-virtual {p0}, Lorg/htmlparser/tags/AppletTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v5, :cond_1

    .line 324
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v1, "End of Applet Tag\n"

    .line 325
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "*****************\n"

    .line 326
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 312
    :cond_2
    invoke-interface {v7}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    .line 313
    instance-of v3, v2, Lorg/htmlparser/Tag;

    if-eqz v3, :cond_3

    .line 314
    move-object v3, v2

    check-cast v3, Lorg/htmlparser/Tag;

    invoke-interface {v3}, Lorg/htmlparser/Tag;->getTagName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "PARAM"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    if-nez v5, :cond_4

    const-string v3, "Miscellaneous items :\n"

    .line 317
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_4
    const-string v3, " "

    .line 319
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    :goto_3
    invoke-interface {v2}, Lorg/htmlparser/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v5, 0x1

    goto :goto_2

    .line 300
    :cond_5
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 301
    invoke-virtual {v2, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 302
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v9, ": Parameter name = "

    .line 303
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 304
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v7, ", Parameter value = "

    .line 305
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 306
    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 307
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/2addr v6, v4

    goto :goto_0
.end method
