.class public Lorg/htmlparser/tags/TextareaTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "TextareaTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "TEXTAREA"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sput-object v1, Lorg/htmlparser/tags/TextareaTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "INPUT"

    aput-object v4, v1, v3

    aput-object v2, v1, v0

    const/4 v2, 0x2

    const-string v4, "SELECT"

    aput-object v4, v1, v2

    const/4 v4, 0x3

    const-string v5, "OPTION"

    aput-object v5, v1, v4

    .line 41
    sput-object v1, Lorg/htmlparser/tags/TextareaTag;->mEnders:[Ljava/lang/String;

    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "FORM"

    aput-object v4, v1, v3

    const-string v3, "BODY"

    aput-object v3, v1, v0

    const-string v0, "HTML"

    aput-object v0, v1, v2

    .line 46
    sput-object v1, Lorg/htmlparser/tags/TextareaTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/TextareaTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/TextareaTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 61
    sget-object v0, Lorg/htmlparser/tags/TextareaTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lorg/htmlparser/tags/TextareaTag;->toPlainTextString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
