.class public Lorg/htmlparser/tags/FrameSetTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "FrameSetTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "FRAMESET"

    aput-object v3, v1, v2

    .line 42
    sput-object v1, Lorg/htmlparser/tags/FrameSetTag;->mIds:[Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "HTML"

    aput-object v1, v0, v2

    .line 47
    sput-object v0, Lorg/htmlparser/tags/FrameSetTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 71
    sget-object v0, Lorg/htmlparser/tags/FrameSetTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getFrame(Ljava/lang/String;)Lorg/htmlparser/tags/FrameTag;
    .locals 1

    .line 101
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/tags/FrameSetTag;->getFrame(Ljava/lang/String;Ljava/util/Locale;)Lorg/htmlparser/tags/FrameTag;

    move-result-object p1

    return-object p1
.end method

.method public getFrame(Ljava/lang/String;Ljava/util/Locale;)Lorg/htmlparser/tags/FrameTag;
    .locals 5

    .line 119
    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 120
    invoke-virtual {p0}, Lorg/htmlparser/tags/FrameSetTag;->getFrames()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    move-object v2, v1

    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 122
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v3

    .line 123
    instance-of v4, v3, Lorg/htmlparser/tags/FrameTag;

    if-eqz v4, :cond_0

    .line 125
    move-object v2, v3

    check-cast v2, Lorg/htmlparser/tags/FrameTag;

    .line 126
    invoke-virtual {v2}, Lorg/htmlparser/tags/FrameTag;->getFrameName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_2
    :goto_1
    return-object v2
.end method

.method public getFrames()Lorg/htmlparser/util/NodeList;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lorg/htmlparser/tags/FrameSetTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 62
    sget-object v0, Lorg/htmlparser/tags/FrameSetTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public setFrames(Lorg/htmlparser/util/NodeList;)V
    .locals 0

    .line 140
    invoke-virtual {p0, p1}, Lorg/htmlparser/tags/FrameSetTag;->setChildren(Lorg/htmlparser/util/NodeList;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FRAMESET TAG : begins at : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/htmlparser/tags/FrameSetTag;->getStartPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "; ends at : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/FrameSetTag;->getEndPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
