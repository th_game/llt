.class public Lorg/htmlparser/tags/BulletList;
.super Lorg/htmlparser/tags/CompositeTag;
.source "BulletList.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "UL"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "OL"

    aput-object v4, v1, v3

    .line 37
    sput-object v1, Lorg/htmlparser/tags/BulletList;->mIds:[Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "BODY"

    aput-object v1, v0, v2

    const-string v1, "HTML"

    aput-object v1, v0, v3

    .line 42
    sput-object v0, Lorg/htmlparser/tags/BulletList;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 66
    sget-object v0, Lorg/htmlparser/tags/BulletList;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 57
    sget-object v0, Lorg/htmlparser/tags/BulletList;->mIds:[Ljava/lang/String;

    return-object v0
.end method
