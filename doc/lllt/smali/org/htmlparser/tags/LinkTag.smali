.class public Lorg/htmlparser/tags/LinkTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "LinkTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# instance fields
.field private javascriptLink:Z

.field protected mLink:Ljava/lang/String;

.field private mailLink:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "A"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 40
    sput-object v1, Lorg/htmlparser/tags/LinkTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x7

    new-array v4, v1, [Ljava/lang/String;

    aput-object v2, v4, v3

    const-string v2, "P"

    aput-object v2, v4, v0

    const-string v5, "DIV"

    const/4 v6, 0x2

    aput-object v5, v4, v6

    const-string v7, "TD"

    const/4 v8, 0x3

    aput-object v7, v4, v8

    const-string v9, "TR"

    const/4 v10, 0x4

    aput-object v9, v4, v10

    const-string v11, "FORM"

    const/4 v12, 0x5

    aput-object v11, v4, v12

    const-string v13, "LI"

    const/4 v14, 0x6

    aput-object v13, v4, v14

    .line 45
    sput-object v4, Lorg/htmlparser/tags/LinkTag;->mEnders:[Ljava/lang/String;

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    aput-object v2, v4, v3

    aput-object v5, v4, v0

    aput-object v7, v4, v6

    aput-object v9, v4, v8

    aput-object v11, v4, v10

    aput-object v13, v4, v12

    const-string v0, "BODY"

    aput-object v0, v4, v14

    const-string v0, "HTML"

    aput-object v0, v4, v1

    .line 50
    sput-object v4, Lorg/htmlparser/tags/LinkTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public extractLink()Ljava/lang/String;
    .locals 2

    const-string v0, "HREF"

    .line 308
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/LinkTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0xa

    .line 311
    invoke-static {v0, v1}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    .line 312
    invoke-static {v0, v1}, Lorg/htmlparser/util/ParserUtils;->removeChars(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    .line 314
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 315
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Page;->getAbsoluteURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getAccessKey()Ljava/lang/String;
    .locals 1

    const-string v0, "ACCESSKEY"

    .line 125
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/LinkTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 115
    sget-object v0, Lorg/htmlparser/tags/LinkTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 106
    sget-object v0, Lorg/htmlparser/tags/LinkTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 97
    sget-object v0, Lorg/htmlparser/tags/LinkTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getLink()Ljava/lang/String;
    .locals 3

    .line 137
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 139
    iput-boolean v0, p0, Lorg/htmlparser/tags/LinkTag;->mailLink:Z

    .line 140
    iput-boolean v0, p0, Lorg/htmlparser/tags/LinkTag;->javascriptLink:Z

    .line 141
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->extractLink()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    const-string v1, "mailto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 148
    iget-object v2, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    add-int/2addr v0, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    .line 149
    iput-boolean v1, p0, Lorg/htmlparser/tags/LinkTag;->mailLink:Z

    .line 151
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    const-string v2, "javascript:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    .line 155
    iput-boolean v1, p0, Lorg/htmlparser/tags/LinkTag;->javascriptLink:Z

    .line 158
    :cond_1
    iget-object v0, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkText()Ljava/lang/String;
    .locals 1

    .line 169
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public isFTPLink()Z
    .locals 2

    .line 203
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ftp://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isHTTPLikeLink()Z
    .locals 1

    .line 239
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isHTTPLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isHTTPSLink()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public isHTTPLink()Z
    .locals 1

    .line 221
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isFTPLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isHTTPSLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isJavascriptLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isMailLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->isIRCLink()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isHTTPSLink()Z
    .locals 2

    .line 230
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isIRCLink()Z
    .locals 2

    .line 211
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v0

    const-string v1, "irc://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isJavascriptLink()Z
    .locals 1

    .line 193
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    .line 194
    iget-boolean v0, p0, Lorg/htmlparser/tags/LinkTag;->javascriptLink:Z

    return v0
.end method

.method public isMailLink()Z
    .locals 1

    .line 183
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    .line 184
    iget-boolean v0, p0, Lorg/htmlparser/tags/LinkTag;->mailLink:Z

    return v0
.end method

.method public setJavascriptLink(Z)V
    .locals 0

    .line 260
    iput-boolean p1, p0, Lorg/htmlparser/tags/LinkTag;->javascriptLink:Z

    return-void
.end method

.method public setLink(Ljava/lang/String;)V
    .locals 1

    .line 295
    iput-object p1, p0, Lorg/htmlparser/tags/LinkTag;->mLink:Ljava/lang/String;

    const-string v0, "HREF"

    .line 296
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/LinkTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setMailLink(Z)V
    .locals 0

    .line 250
    iput-boolean p1, p0, Lorg/htmlparser/tags/LinkTag;->mailLink:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 269
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Link to : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "; titled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getLinkText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "; begins at : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getStartPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "; ends at : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getEndPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", AccessKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 271
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getAccessKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    if-nez v1, :cond_0

    const-string v1, "null\n"

    .line 272
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 274
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getAccessKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 275
    :goto_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 279
    invoke-virtual {p0}, Lorg/htmlparser/tags/LinkTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    .line 281
    :cond_1
    invoke-interface {v3}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v4

    .line 282
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "   "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-interface {v4}, Lorg/htmlparser/Node;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v1, v6

    goto :goto_1

    .line 286
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
