.class public Lorg/htmlparser/tags/FormTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "FormTag.java"


# static fields
.field public static final GET:Ljava/lang/String; = "GET"

.field public static final POST:Ljava/lang/String; = "POST"

.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# instance fields
.field protected mFormLocation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "FORM"

    aput-object v3, v1, v2

    .line 57
    sput-object v1, Lorg/htmlparser/tags/FormTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "HTML"

    aput-object v3, v1, v2

    const-string v2, "BODY"

    aput-object v2, v1, v0

    const/4 v0, 0x2

    const-string v2, "TABLE"

    aput-object v2, v1, v0

    .line 62
    sput-object v1, Lorg/htmlparser/tags/FormTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 67
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lorg/htmlparser/tags/FormTag;->mFormLocation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public extractFormLocn()Ljava/lang/String;
    .locals 2

    const-string v0, "ACTION"

    .line 230
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/FormTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 233
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 234
    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/htmlparser/lexer/Page;->getAbsoluteURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 96
    sget-object v0, Lorg/htmlparser/tags/FormTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 87
    sget-object v0, Lorg/htmlparser/tags/FormTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getFormInputs()Lorg/htmlparser/util/NodeList;
    .locals 2

    .line 105
    const-class v0, Lorg/htmlparser/tags/InputTag;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/tags/FormTag;->searchFor(Ljava/lang/Class;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    return-object v0
.end method

.method public getFormLocation()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lorg/htmlparser/tags/FormTag;->mFormLocation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->extractFormLocn()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlparser/tags/FormTag;->mFormLocation:Ljava/lang/String;

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/htmlparser/tags/FormTag;->mFormLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getFormMethod()Ljava/lang/String;
    .locals 1

    const-string v0, "METHOD"

    .line 149
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/FormTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GET"

    :cond_0
    return-object v0
.end method

.method public getFormName()Ljava/lang/String;
    .locals 1

    const-string v0, "NAME"

    .line 188
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/FormTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormTextareas()Lorg/htmlparser/util/NodeList;
    .locals 2

    .line 114
    const-class v0, Lorg/htmlparser/tags/TextareaTag;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/tags/FormTag;->searchFor(Ljava/lang/Class;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 78
    sget-object v0, Lorg/htmlparser/tags/FormTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getInputTag(Ljava/lang/String;)Lorg/htmlparser/tags/InputTag;
    .locals 5

    .line 169
    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getFormInputs()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 171
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/InputTag;

    const-string v4, "NAME"

    .line 172
    invoke-virtual {v3, v4}, Lorg/htmlparser/tags/InputTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 173
    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    return-object v3

    :cond_3
    return-object v1
.end method

.method public getTextAreaTag(Ljava/lang/String;)Lorg/htmlparser/tags/TextareaTag;
    .locals 5

    .line 200
    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getFormTextareas()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 202
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v3

    check-cast v3, Lorg/htmlparser/tags/TextareaTag;

    const-string v4, "NAME"

    .line 203
    invoke-virtual {v3, v4}, Lorg/htmlparser/tags/TextareaTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 204
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    return-object v3

    :cond_3
    return-object v1
.end method

.method public setFormLocation(Ljava/lang/String;)V
    .locals 1

    .line 137
    iput-object p1, p0, Lorg/htmlparser/tags/FormTag;->mFormLocation:Ljava/lang/String;

    const-string v0, "ACTION"

    .line 138
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/FormTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FORM TAG : Form at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getFormLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; begins at : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getStartPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "; ends at : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/FormTag;->getEndPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
