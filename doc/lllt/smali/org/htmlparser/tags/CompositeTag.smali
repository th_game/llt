.class public Lorg/htmlparser/tags/CompositeTag;
.super Lorg/htmlparser/nodes/TagNode;
.source "CompositeTag.java"


# static fields
.field protected static final mDefaultCompositeScanner:Lorg/htmlparser/scanners/CompositeTagScanner;


# instance fields
.field protected mEndTag:Lorg/htmlparser/Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lorg/htmlparser/scanners/CompositeTagScanner;

    invoke-direct {v0}, Lorg/htmlparser/scanners/CompositeTagScanner;-><init>()V

    sput-object v0, Lorg/htmlparser/tags/CompositeTag;->mDefaultCompositeScanner:Lorg/htmlparser/scanners/CompositeTagScanner;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 64
    invoke-direct {p0}, Lorg/htmlparser/nodes/TagNode;-><init>()V

    .line 66
    sget-object v0, Lorg/htmlparser/tags/CompositeTag;->mDefaultCompositeScanner:Lorg/htmlparser/scanners/CompositeTagScanner;

    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/CompositeTag;->setThisScanner(Lorg/htmlparser/scanners/Scanner;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/htmlparser/visitors/NodeVisitor;)V
    .locals 2

    .line 468
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->shouldRecurseSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p1, p0}, Lorg/htmlparser/visitors/NodeVisitor;->visitTag(Lorg/htmlparser/Tag;)V

    .line 470
    :cond_0
    invoke-virtual {p1}, Lorg/htmlparser/visitors/NodeVisitor;->shouldRecurseChildren()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 472
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 474
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    .line 475
    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 477
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v1

    .line 478
    invoke-interface {v1, p1}, Lorg/htmlparser/Node;->accept(Lorg/htmlparser/visitors/NodeVisitor;)V

    goto :goto_0

    .line 481
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-eq p0, v0, :cond_3

    .line 482
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/htmlparser/Tag;->accept(Lorg/htmlparser/visitors/NodeVisitor;)V

    :cond_3
    return-void
.end method

.method public childAt(I)Lorg/htmlparser/Node;
    .locals 1

    .line 394
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 395
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public children()Lorg/htmlparser/util/SimpleNodeIterator;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_0
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V
    .locals 2

    .line 433
    invoke-super {p0, p1, p2}, Lorg/htmlparser/nodes/TagNode;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    .line 434
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_1

    .line 436
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lorg/htmlparser/Tag;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    :cond_0
    return-void

    .line 435
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lorg/htmlparser/Node;->collectInto(Lorg/htmlparser/util/NodeList;Lorg/htmlparser/NodeFilter;)V

    goto :goto_0
.end method

.method public digupStringNode(Ljava/lang/String;)[Lorg/htmlparser/Text;
    .locals 7

    .line 532
    invoke-virtual {p0, p1}, Lorg/htmlparser/tags/CompositeTag;->searchFor(Ljava/lang/String;)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 533
    new-instance v1, Lorg/htmlparser/util/NodeList;

    invoke-direct {v1}, Lorg/htmlparser/util/NodeList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 534
    :goto_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 547
    invoke-virtual {v1}, Lorg/htmlparser/util/NodeList;->size()I

    move-result p1

    new-array v4, p1, [Lorg/htmlparser/Text;

    .line 548
    :goto_1
    array-length p1, v4

    if-lt v2, p1, :cond_0

    return-object v4

    .line 549
    :cond_0
    invoke-virtual {v1, v2}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object p1

    check-cast p1, Lorg/htmlparser/Text;

    aput-object p1, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 535
    :cond_1
    invoke-virtual {v0, v3}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object v4

    .line 536
    instance-of v5, v4, Lorg/htmlparser/Text;

    if-eqz v5, :cond_2

    .line 537
    invoke-virtual {v1, v4}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_3

    .line 539
    :cond_2
    instance-of v5, v4, Lorg/htmlparser/tags/CompositeTag;

    if-eqz v5, :cond_4

    .line 540
    check-cast v4, Lorg/htmlparser/tags/CompositeTag;

    .line 541
    invoke-virtual {v4, p1}, Lorg/htmlparser/tags/CompositeTag;->digupStringNode(Ljava/lang/String;)[Lorg/htmlparser/Text;

    move-result-object v4

    const/4 v5, 0x0

    .line 542
    :goto_2
    array-length v6, v4

    if-lt v5, v6, :cond_3

    goto :goto_3

    .line 543
    :cond_3
    aget-object v6, v4, v5

    invoke-virtual {v1, v6}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public elements()Lorg/htmlparser/util/SimpleNodeIterator;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->elements()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public findPositionOf(Ljava/lang/String;)I
    .locals 1

    .line 333
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/tags/CompositeTag;->findPositionOf(Ljava/lang/String;Ljava/util/Locale;)I

    move-result p1

    return p1
.end method

.method public findPositionOf(Ljava/lang/String;Ljava/util/Locale;)I
    .locals 4

    .line 352
    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 353
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    const/4 v3, -0x1

    if-nez v2, :cond_0

    return v3

    .line 355
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    .line 356
    invoke-interface {v2}, Lorg/htmlparser/Node;->toPlainTextString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v3, v2, :cond_1

    return v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public findPositionOf(Lorg/htmlparser/Node;)I
    .locals 3

    .line 376
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, -0x1

    return p1

    .line 377
    :cond_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    if-ne v2, p1, :cond_1

    return v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getChild(I)Lorg/htmlparser/Node;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/util/NodeList;->elementAt(I)Lorg/htmlparser/Node;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getChildCount()I
    .locals 1

    .line 494
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 496
    :cond_0
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getChildrenAsNodeArray()[Lorg/htmlparser/Node;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lorg/htmlparser/Node;

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->toNodeArray()[Lorg/htmlparser/Node;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getChildrenHTML()Ljava/lang/String;
    .locals 3

    .line 445
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 446
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_0

    .line 450
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 447
    :cond_0
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    check-cast v2, Lorg/htmlparser/nodes/AbstractNode;

    .line 448
    invoke-virtual {v2}, Lorg/htmlparser/nodes/AbstractNode;->toHtml()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public getEndTag()Lorg/htmlparser/Tag;
    .locals 1

    .line 509
    iget-object v0, p0, Lorg/htmlparser/tags/CompositeTag;->mEndTag:Lorg/htmlparser/Tag;

    return-object v0
.end method

.method public getStringText()Ljava/lang/String;
    .locals 3

    .line 589
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndPosition()I

    move-result v0

    .line 590
    iget-object v1, p0, Lorg/htmlparser/tags/CompositeTag;->mEndTag:Lorg/htmlparser/Tag;

    invoke-interface {v1}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result v1

    .line 591
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getPage()Lorg/htmlparser/lexer/Page;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lorg/htmlparser/lexer/Page;->getText(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 576
    invoke-super {p0, v0}, Lorg/htmlparser/nodes/TagNode;->toHtml(Z)Ljava/lang/String;

    move-result-object v1

    .line 577
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected putChildrenInto(Ljava/lang/StringBuffer;Z)V
    .locals 4

    .line 151
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 153
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v1

    if-eqz p2, :cond_2

    .line 155
    invoke-interface {v1}, Lorg/htmlparser/Node;->getStartPosition()I

    move-result v2

    invoke-interface {v1}, Lorg/htmlparser/Node;->getEndPosition()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 156
    :cond_2
    invoke-interface {v1}, Lorg/htmlparser/Node;->toHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method protected putEndTagInto(Ljava/lang/StringBuffer;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 169
    iget-object p2, p0, Lorg/htmlparser/tags/CompositeTag;->mEndTag:Lorg/htmlparser/Tag;

    invoke-interface {p2}, Lorg/htmlparser/Tag;->getStartPosition()I

    move-result p2

    iget-object v0, p0, Lorg/htmlparser/tags/CompositeTag;->mEndTag:Lorg/htmlparser/Tag;

    invoke-interface {v0}, Lorg/htmlparser/Tag;->getEndPosition()I

    move-result v0

    if-eq p2, v0, :cond_1

    .line 170
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object p2

    invoke-interface {p2}, Lorg/htmlparser/Tag;->toHtml()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    return-void
.end method

.method public removeChild(I)V
    .locals 1

    .line 114
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlparser/util/NodeList;->remove(I)Lorg/htmlparser/Node;

    :cond_0
    return-void
.end method

.method public searchByName(Ljava/lang/String;)Lorg/htmlparser/Tag;
    .locals 6

    .line 204
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 205
    :cond_1
    invoke-interface {v0}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v4

    .line 206
    instance-of v5, v4, Lorg/htmlparser/Tag;

    if-eqz v5, :cond_0

    .line 208
    move-object v3, v4

    check-cast v3, Lorg/htmlparser/Tag;

    const-string v4, "NAME"

    .line 209
    invoke-interface {v3, v4}, Lorg/htmlparser/Tag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 210
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    return-object v3

    :cond_3
    return-object v1
.end method

.method public searchFor(Ljava/lang/Class;Z)Lorg/htmlparser/util/NodeList;
    .locals 2

    .line 311
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 313
    new-instance p1, Lorg/htmlparser/util/NodeList;

    invoke-direct {p1}, Lorg/htmlparser/util/NodeList;-><init>()V

    goto :goto_0

    .line 316
    :cond_0
    new-instance v1, Lorg/htmlparser/filters/NodeClassFilter;

    invoke-direct {v1, p1}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 315
    invoke-virtual {v0, v1, p2}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public searchFor(Ljava/lang/String;)Lorg/htmlparser/util/NodeList;
    .locals 1

    const/4 v0, 0x0

    .line 236
    invoke-virtual {p0, p1, v0}, Lorg/htmlparser/tags/CompositeTag;->searchFor(Ljava/lang/String;Z)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    return-object p1
.end method

.method public searchFor(Ljava/lang/String;Z)Lorg/htmlparser/util/NodeList;
    .locals 1

    .line 256
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, p1, p2, v0}, Lorg/htmlparser/tags/CompositeTag;->searchFor(Ljava/lang/String;ZLjava/util/Locale;)Lorg/htmlparser/util/NodeList;

    move-result-object p1

    return-object p1
.end method

.method public searchFor(Ljava/lang/String;ZLjava/util/Locale;)Lorg/htmlparser/util/NodeList;
    .locals 5

    .line 281
    new-instance v0, Lorg/htmlparser/util/NodeList;

    invoke-direct {v0}, Lorg/htmlparser/util/NodeList;-><init>()V

    if-nez p2, :cond_0

    .line 284
    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 285
    :cond_0
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_2

    return-object v0

    .line 287
    :cond_2
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    .line 288
    invoke-interface {v2}, Lorg/htmlparser/Node;->toPlainTextString()Ljava/lang/String;

    move-result-object v3

    if-nez p2, :cond_3

    .line 290
    invoke-virtual {v3, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    const/4 v4, -0x1

    .line 291
    invoke-virtual {v3, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v4, v3, :cond_1

    .line 292
    invoke-virtual {v0, v2}, Lorg/htmlparser/util/NodeList;->add(Lorg/htmlparser/Node;)V

    goto :goto_0
.end method

.method public setEndTag(Lorg/htmlparser/Tag;)V
    .locals 0

    .line 521
    iput-object p1, p0, Lorg/htmlparser/tags/CompositeTag;->mEndTag:Lorg/htmlparser/Tag;

    return-void
.end method

.method public toHtml(Z)Ljava/lang/String;
    .locals 2

    .line 184
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 185
    invoke-super {p0, p1}, Lorg/htmlparser/nodes/TagNode;->toHtml(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 186
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->isEmptyXmlTag()Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/CompositeTag;->putChildrenInto(Ljava/lang/StringBuffer;Z)V

    .line 189
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/CompositeTag;->putEndTagInto(Ljava/lang/StringBuffer;Z)V

    .line 192
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toPlainTextString()Ljava/lang/String;
    .locals 3

    .line 135
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 136
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v2

    if-nez v2, :cond_0

    .line 139
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 137
    :cond_0
    invoke-interface {v1}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/htmlparser/Node;->toPlainTextString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 562
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v1, 0x0

    .line 563
    invoke-virtual {p0, v1, v0}, Lorg/htmlparser/tags/CompositeTag;->toString(ILjava/lang/StringBuffer;)V

    .line 565
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(ILjava/lang/StringBuffer;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const-string v2, "  "

    if-lt v1, p1, :cond_5

    .line 607
    invoke-super {p0}, Lorg/htmlparser/nodes/TagNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "line.separator"

    .line 608
    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 609
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->children()Lorg/htmlparser/util/SimpleNodeIterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Lorg/htmlparser/util/SimpleNodeIterator;->hasMoreNodes()Z

    move-result v1

    if-nez v1, :cond_2

    .line 623
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object v1

    if-eq p0, v1, :cond_1

    :goto_2
    if-le v0, p1, :cond_0

    .line 629
    invoke-virtual {p0}, Lorg/htmlparser/tags/CompositeTag;->getEndTag()Lorg/htmlparser/Tag;

    move-result-object p1

    invoke-interface {p1}, Lorg/htmlparser/Tag;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 630
    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 628
    :cond_0
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    return-void

    .line 611
    :cond_2
    invoke-interface {v4}, Lorg/htmlparser/util/SimpleNodeIterator;->nextNode()Lorg/htmlparser/Node;

    move-result-object v5

    .line 612
    instance-of v1, v5, Lorg/htmlparser/tags/CompositeTag;

    if-eqz v1, :cond_3

    .line 613
    check-cast v5, Lorg/htmlparser/tags/CompositeTag;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v5, v1, p2}, Lorg/htmlparser/tags/CompositeTag;->toString(ILjava/lang/StringBuffer;)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_4
    if-le v1, p1, :cond_4

    .line 618
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 619
    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 617
    :cond_4
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 606
    :cond_5
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
