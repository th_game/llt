.class public Lorg/htmlparser/tags/HeadingTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "HeadingTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "H1"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v4, "H2"

    const/4 v5, 0x1

    aput-object v4, v1, v5

    const-string v6, "H3"

    const/4 v7, 0x2

    aput-object v6, v1, v7

    const-string v8, "H4"

    const/4 v9, 0x3

    aput-object v8, v1, v9

    const-string v10, "H5"

    const/4 v11, 0x4

    aput-object v10, v1, v11

    const-string v12, "H6"

    const/4 v13, 0x5

    aput-object v12, v1, v13

    .line 36
    sput-object v1, Lorg/htmlparser/tags/HeadingTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    aput-object v2, v1, v3

    aput-object v4, v1, v5

    aput-object v6, v1, v7

    aput-object v8, v1, v9

    aput-object v10, v1, v11

    aput-object v12, v1, v13

    const-string v2, "PARAM"

    aput-object v2, v1, v0

    .line 41
    sput-object v1, Lorg/htmlparser/tags/HeadingTag;->mEnders:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "BODY"

    aput-object v1, v0, v3

    const-string v1, "HTML"

    aput-object v1, v0, v5

    .line 46
    sput-object v0, Lorg/htmlparser/tags/HeadingTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/HeadingTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/HeadingTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 61
    sget-object v0, Lorg/htmlparser/tags/HeadingTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method
