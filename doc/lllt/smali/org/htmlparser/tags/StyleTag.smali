.class public Lorg/htmlparser/tags/StyleTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "StyleTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "STYLE"

    aput-object v3, v1, v2

    .line 38
    sput-object v1, Lorg/htmlparser/tags/StyleTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "BODY"

    aput-object v3, v1, v2

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 43
    sput-object v1, Lorg/htmlparser/tags/StyleTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    .line 50
    new-instance v0, Lorg/htmlparser/scanners/StyleScanner;

    invoke-direct {v0}, Lorg/htmlparser/scanners/StyleScanner;-><init>()V

    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/StyleTag;->setThisScanner(Lorg/htmlparser/scanners/Scanner;)V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 68
    sget-object v0, Lorg/htmlparser/tags/StyleTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 59
    sget-object v0, Lorg/htmlparser/tags/StyleTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getStyleCode()Ljava/lang/String;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lorg/htmlparser/tags/StyleTag;->getChildrenHTML()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 89
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    invoke-virtual {p0}, Lorg/htmlparser/tags/StyleTag;->toHtml()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Style node :\n"

    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
