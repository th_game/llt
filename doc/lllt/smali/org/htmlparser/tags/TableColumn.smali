.class public Lorg/htmlparser/tags/TableColumn;
.super Lorg/htmlparser/tags/CompositeTag;
.source "TableColumn.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "TD"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sput-object v1, Lorg/htmlparser/tags/TableColumn;->mIds:[Ljava/lang/String;

    const/4 v1, 0x5

    new-array v4, v1, [Ljava/lang/String;

    aput-object v2, v4, v3

    const-string v2, "TR"

    aput-object v2, v4, v0

    const-string v5, "TBODY"

    const/4 v6, 0x2

    aput-object v5, v4, v6

    const-string v7, "TFOOT"

    const/4 v8, 0x3

    aput-object v7, v4, v8

    const-string v9, "THEAD"

    const/4 v10, 0x4

    aput-object v9, v4, v10

    .line 41
    sput-object v4, Lorg/htmlparser/tags/TableColumn;->mEnders:[Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/String;

    aput-object v2, v1, v3

    aput-object v5, v1, v0

    aput-object v7, v1, v6

    aput-object v9, v1, v8

    const-string v0, "TABLE"

    aput-object v0, v1, v10

    .line 46
    sput-object v1, Lorg/htmlparser/tags/TableColumn;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/TableColumn;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/TableColumn;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 61
    sget-object v0, Lorg/htmlparser/tags/TableColumn;->mIds:[Ljava/lang/String;

    return-object v0
.end method
