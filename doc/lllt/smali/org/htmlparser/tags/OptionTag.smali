.class public Lorg/htmlparser/tags/OptionTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "OptionTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "OPTION"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sput-object v1, Lorg/htmlparser/tags/OptionTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x4

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "INPUT"

    aput-object v5, v4, v3

    const-string v5, "TEXTAREA"

    aput-object v5, v4, v0

    const-string v5, "SELECT"

    const/4 v6, 0x2

    aput-object v5, v4, v6

    const/4 v7, 0x3

    aput-object v2, v4, v7

    .line 41
    sput-object v4, Lorg/htmlparser/tags/OptionTag;->mEnders:[Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/String;

    aput-object v5, v1, v3

    const-string v2, "FORM"

    aput-object v2, v1, v0

    const-string v0, "BODY"

    aput-object v0, v1, v6

    const-string v0, "HTML"

    aput-object v0, v1, v7

    .line 46
    sput-object v1, Lorg/htmlparser/tags/OptionTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/OptionTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/OptionTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 61
    sget-object v0, Lorg/htmlparser/tags/OptionTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getOptionText()Ljava/lang/String;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lorg/htmlparser/tags/OptionTag;->toPlainTextString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    const-string v0, "VALUE"

    .line 89
    invoke-virtual {p0, v0}, Lorg/htmlparser/tags/OptionTag;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    const-string v0, "VALUE"

    .line 98
    invoke-virtual {p0, v0, p1}, Lorg/htmlparser/tags/OptionTag;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OPTION VALUE: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/htmlparser/tags/OptionTag;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " TEXT: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/htmlparser/tags/OptionTag;->getOptionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
