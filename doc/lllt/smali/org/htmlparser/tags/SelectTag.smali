.class public Lorg/htmlparser/tags/SelectTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "SelectTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "SELECT"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 38
    sput-object v1, Lorg/htmlparser/tags/SelectTag;->mIds:[Ljava/lang/String;

    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "INPUT"

    aput-object v5, v4, v3

    const-string v5, "TEXTAREA"

    aput-object v5, v4, v0

    const/4 v5, 0x2

    aput-object v2, v4, v5

    .line 43
    sput-object v4, Lorg/htmlparser/tags/SelectTag;->mEnders:[Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "FORM"

    aput-object v2, v1, v3

    const-string v2, "BODY"

    aput-object v2, v1, v0

    const-string v0, "HTML"

    aput-object v0, v1, v5

    .line 48
    sput-object v1, Lorg/htmlparser/tags/SelectTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 81
    sget-object v0, Lorg/htmlparser/tags/SelectTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 72
    sget-object v0, Lorg/htmlparser/tags/SelectTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 63
    sget-object v0, Lorg/htmlparser/tags/SelectTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getOptionTags()[Lorg/htmlparser/tags/OptionTag;
    .locals 2

    .line 93
    const-class v0, Lorg/htmlparser/tags/OptionTag;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/htmlparser/tags/SelectTag;->searchFor(Ljava/lang/Class;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/htmlparser/tags/OptionTag;

    .line 95
    invoke-virtual {v0, v1}, Lorg/htmlparser/util/NodeList;->copyToNodeArray([Lorg/htmlparser/Node;)V

    return-object v1
.end method
