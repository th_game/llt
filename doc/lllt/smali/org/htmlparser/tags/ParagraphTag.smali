.class public Lorg/htmlparser/tags/ParagraphTag;
.super Lorg/htmlparser/tags/CompositeTag;
.source "ParagraphTag.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "P"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sput-object v1, Lorg/htmlparser/tags/ParagraphTag;->mIds:[Ljava/lang/String;

    const/16 v1, 0x1a

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "ADDRESS"

    aput-object v4, v1, v3

    const-string v4, "BLOCKQUOTE"

    aput-object v4, v1, v0

    const/4 v4, 0x2

    const-string v5, "CENTER"

    aput-object v5, v1, v4

    const/4 v5, 0x3

    const-string v6, "DD"

    aput-object v6, v1, v5

    const/4 v5, 0x4

    const-string v6, "DIR"

    aput-object v6, v1, v5

    const/4 v5, 0x5

    const-string v6, "DIV"

    aput-object v6, v1, v5

    const/4 v5, 0x6

    const-string v6, "DL"

    aput-object v6, v1, v5

    const/4 v5, 0x7

    const-string v6, "DT"

    aput-object v6, v1, v5

    const/16 v5, 0x8

    const-string v6, "FIELDSET"

    aput-object v6, v1, v5

    const/16 v5, 0x9

    const-string v6, "FORM"

    aput-object v6, v1, v5

    const/16 v5, 0xa

    const-string v6, "H1"

    aput-object v6, v1, v5

    const/16 v5, 0xb

    const-string v6, "H2"

    aput-object v6, v1, v5

    const/16 v5, 0xc

    const-string v6, "H3"

    aput-object v6, v1, v5

    const/16 v5, 0xd

    const-string v6, "H4"

    aput-object v6, v1, v5

    const/16 v5, 0xe

    const-string v6, "H5"

    aput-object v6, v1, v5

    const/16 v5, 0xf

    const-string v6, "H6"

    aput-object v6, v1, v5

    const/16 v5, 0x10

    const-string v6, "HR"

    aput-object v6, v1, v5

    const/16 v5, 0x11

    const-string v6, "ISINDEX"

    aput-object v6, v1, v5

    const/16 v5, 0x12

    const-string v6, "LI"

    aput-object v6, v1, v5

    const/16 v5, 0x13

    const-string v6, "MENU"

    aput-object v6, v1, v5

    const/16 v5, 0x14

    const-string v6, "NOFRAMES"

    aput-object v6, v1, v5

    const/16 v5, 0x15

    const-string v6, "OL"

    aput-object v6, v1, v5

    const/16 v5, 0x16

    aput-object v2, v1, v5

    const/16 v2, 0x17

    const-string v5, "PARAM"

    aput-object v5, v1, v2

    const/16 v2, 0x18

    const-string v5, "PRE"

    aput-object v5, v1, v2

    const/16 v2, 0x19

    const-string v5, "UL"

    aput-object v5, v1, v2

    .line 41
    sput-object v1, Lorg/htmlparser/tags/ParagraphTag;->mEnders:[Ljava/lang/String;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "BODY"

    aput-object v2, v1, v3

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 46
    sput-object v1, Lorg/htmlparser/tags/ParagraphTag;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/ParagraphTag;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/ParagraphTag;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 61
    sget-object v0, Lorg/htmlparser/tags/ParagraphTag;->mIds:[Ljava/lang/String;

    return-object v0
.end method
