.class public Lorg/htmlparser/tags/TableRow;
.super Lorg/htmlparser/tags/CompositeTag;
.source "TableRow.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "TR"

    aput-object v3, v1, v2

    .line 45
    sput-object v1, Lorg/htmlparser/tags/TableRow;->mIds:[Ljava/lang/String;

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "TBODY"

    aput-object v4, v3, v2

    const-string v5, "TFOOT"

    aput-object v5, v3, v0

    const-string v6, "THEAD"

    const/4 v7, 0x2

    aput-object v6, v3, v7

    .line 50
    sput-object v3, Lorg/htmlparser/tags/TableRow;->mEnders:[Ljava/lang/String;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    aput-object v4, v3, v2

    aput-object v5, v3, v0

    aput-object v6, v3, v7

    const-string v0, "TABLE"

    aput-object v0, v3, v1

    .line 55
    sput-object v3, Lorg/htmlparser/tags/TableRow;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .line 138
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableRow;->getColumns()[Lorg/htmlparser/tags/TableColumn;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getColumns()[Lorg/htmlparser/tags/TableColumn;
    .locals 7

    .line 103
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableRow;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    new-instance v1, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v2, Lorg/htmlparser/tags/TableRow;

    invoke-direct {v1, v2}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 107
    new-instance v2, Lorg/htmlparser/filters/HasParentFilter;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 108
    new-instance v3, Lorg/htmlparser/filters/OrFilter;

    .line 109
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    .line 111
    new-instance v5, Lorg/htmlparser/filters/IsEqualFilter;

    invoke-direct {v5, p0}, Lorg/htmlparser/filters/IsEqualFilter;-><init>(Lorg/htmlparser/Node;)V

    .line 109
    invoke-direct {v4, v1, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 112
    new-instance v5, Lorg/htmlparser/filters/AndFilter;

    .line 113
    new-instance v6, Lorg/htmlparser/filters/NotFilter;

    invoke-direct {v6, v1}, Lorg/htmlparser/filters/NotFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 112
    invoke-direct {v5, v6, v2}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 108
    invoke-direct {v3, v4, v5}, Lorg/htmlparser/filters/OrFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 115
    invoke-virtual {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;->setParentFilter(Lorg/htmlparser/NodeFilter;)V

    .line 118
    new-instance v1, Lorg/htmlparser/filters/AndFilter;

    .line 119
    new-instance v2, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v4, Lorg/htmlparser/tags/TableColumn;

    invoke-direct {v2, v4}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 118
    invoke-direct {v1, v2, v3}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    const/4 v2, 0x1

    .line 116
    invoke-virtual {v0, v1, v2}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/htmlparser/tags/TableColumn;

    .line 122
    invoke-virtual {v0, v1}, Lorg/htmlparser/util/NodeList;->copyToNodeArray([Lorg/htmlparser/Node;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v1, v0, [Lorg/htmlparser/tags/TableColumn;

    :goto_0
    return-object v1
.end method

.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 88
    sget-object v0, Lorg/htmlparser/tags/TableRow;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lorg/htmlparser/tags/TableRow;->mEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderCount()I
    .locals 1

    .line 186
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableRow;->getHeaders()[Lorg/htmlparser/tags/TableHeader;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getHeaders()[Lorg/htmlparser/tags/TableHeader;
    .locals 7

    .line 153
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableRow;->getChildren()Lorg/htmlparser/util/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    new-instance v1, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v2, Lorg/htmlparser/tags/TableRow;

    invoke-direct {v1, v2}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 157
    new-instance v2, Lorg/htmlparser/filters/HasParentFilter;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 158
    new-instance v3, Lorg/htmlparser/filters/OrFilter;

    .line 159
    new-instance v4, Lorg/htmlparser/filters/AndFilter;

    .line 161
    new-instance v5, Lorg/htmlparser/filters/IsEqualFilter;

    invoke-direct {v5, p0}, Lorg/htmlparser/filters/IsEqualFilter;-><init>(Lorg/htmlparser/Node;)V

    .line 159
    invoke-direct {v4, v1, v5}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 162
    new-instance v5, Lorg/htmlparser/filters/AndFilter;

    .line 163
    new-instance v6, Lorg/htmlparser/filters/NotFilter;

    invoke-direct {v6, v1}, Lorg/htmlparser/filters/NotFilter;-><init>(Lorg/htmlparser/NodeFilter;)V

    .line 162
    invoke-direct {v5, v6, v2}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 158
    invoke-direct {v3, v4, v5}, Lorg/htmlparser/filters/OrFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    .line 165
    invoke-virtual {v2, v3}, Lorg/htmlparser/filters/HasParentFilter;->setParentFilter(Lorg/htmlparser/NodeFilter;)V

    .line 168
    new-instance v1, Lorg/htmlparser/filters/AndFilter;

    .line 169
    new-instance v2, Lorg/htmlparser/filters/NodeClassFilter;

    const-class v4, Lorg/htmlparser/tags/TableHeader;

    invoke-direct {v2, v4}, Lorg/htmlparser/filters/NodeClassFilter;-><init>(Ljava/lang/Class;)V

    .line 168
    invoke-direct {v1, v2, v3}, Lorg/htmlparser/filters/AndFilter;-><init>(Lorg/htmlparser/NodeFilter;Lorg/htmlparser/NodeFilter;)V

    const/4 v2, 0x1

    .line 166
    invoke-virtual {v0, v1, v2}, Lorg/htmlparser/util/NodeList;->extractAllNodesThatMatch(Lorg/htmlparser/NodeFilter;Z)Lorg/htmlparser/util/NodeList;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lorg/htmlparser/util/NodeList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/htmlparser/tags/TableHeader;

    .line 172
    invoke-virtual {v0, v1}, Lorg/htmlparser/util/NodeList;->copyToNodeArray([Lorg/htmlparser/Node;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v1, v0, [Lorg/htmlparser/tags/TableHeader;

    :goto_0
    return-object v1
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lorg/htmlparser/tags/TableRow;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public hasHeader()Z
    .locals 1

    .line 195
    invoke-virtual {p0}, Lorg/htmlparser/tags/TableRow;->getHeaderCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
