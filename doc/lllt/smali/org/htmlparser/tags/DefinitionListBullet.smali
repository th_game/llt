.class public Lorg/htmlparser/tags/DefinitionListBullet;
.super Lorg/htmlparser/tags/CompositeTag;
.source "DefinitionListBullet.java"


# static fields
.field private static final mEndTagEnders:[Ljava/lang/String;

.field private static final mIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "DD"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "DT"

    aput-object v4, v1, v3

    .line 36
    sput-object v1, Lorg/htmlparser/tags/DefinitionListBullet;->mIds:[Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "DL"

    aput-object v4, v1, v2

    const-string v2, "BODY"

    aput-object v2, v1, v3

    const-string v2, "HTML"

    aput-object v2, v1, v0

    .line 41
    sput-object v1, Lorg/htmlparser/tags/DefinitionListBullet;->mEndTagEnders:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Lorg/htmlparser/tags/CompositeTag;-><init>()V

    return-void
.end method


# virtual methods
.method public getEndTagEnders()[Ljava/lang/String;
    .locals 1

    .line 74
    sget-object v0, Lorg/htmlparser/tags/DefinitionListBullet;->mEndTagEnders:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnders()[Ljava/lang/String;
    .locals 1

    .line 65
    sget-object v0, Lorg/htmlparser/tags/DefinitionListBullet;->mIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .line 56
    sget-object v0, Lorg/htmlparser/tags/DefinitionListBullet;->mIds:[Ljava/lang/String;

    return-object v0
.end method
