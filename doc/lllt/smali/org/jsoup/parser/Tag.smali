.class public Lorg/jsoup/parser/Tag;
.super Ljava/lang/Object;
.source "Tag.java"


# static fields
.field private static final blockTags:[Ljava/lang/String;

.field private static final emptyTags:[Ljava/lang/String;

.field private static final formatAsInlineTags:[Ljava/lang/String;

.field private static final inlineTags:[Ljava/lang/String;

.field private static final preserveWhitespaceTags:[Ljava/lang/String;

.field private static final tags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/jsoup/parser/Tag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private canContainBlock:Z

.field private canContainInline:Z

.field private empty:Z

.field private formatAsBlock:Z

.field private isBlock:Z

.field private preserveWhitespace:Z

.field private selfClosing:Z

.field private tagName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    const/16 v0, 0x3a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "html"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "head"

    aput-object v3, v0, v2

    const/4 v3, 0x2

    const-string v4, "body"

    aput-object v4, v0, v3

    const/4 v4, 0x3

    const-string v5, "frameset"

    aput-object v5, v0, v4

    const/4 v5, 0x4

    const-string v6, "script"

    aput-object v6, v0, v5

    const/4 v6, 0x5

    const-string v7, "noscript"

    aput-object v7, v0, v6

    const/4 v7, 0x6

    const-string v8, "style"

    aput-object v8, v0, v7

    const/4 v8, 0x7

    const-string v9, "meta"

    aput-object v9, v0, v8

    const/16 v9, 0x8

    const-string v10, "link"

    aput-object v10, v0, v9

    const/16 v10, 0x9

    const-string v11, "title"

    aput-object v11, v0, v10

    const/16 v11, 0xa

    const-string v12, "frame"

    aput-object v12, v0, v11

    const/16 v12, 0xb

    const-string v13, "noframes"

    aput-object v13, v0, v12

    const/16 v13, 0xc

    const-string v14, "section"

    aput-object v14, v0, v13

    const/16 v14, 0xd

    const-string v15, "nav"

    aput-object v15, v0, v14

    const/16 v15, 0xe

    const-string v16, "aside"

    aput-object v16, v0, v15

    const/16 v16, 0xf

    const-string v17, "hgroup"

    aput-object v17, v0, v16

    const/16 v16, 0x10

    const-string v17, "header"

    aput-object v17, v0, v16

    const/16 v16, 0x11

    const-string v17, "footer"

    aput-object v17, v0, v16

    const/16 v16, 0x12

    const-string v17, "p"

    aput-object v17, v0, v16

    const/16 v16, 0x13

    const-string v17, "h1"

    aput-object v17, v0, v16

    const/16 v16, 0x14

    const-string v17, "h2"

    aput-object v17, v0, v16

    const/16 v16, 0x15

    const-string v17, "h3"

    aput-object v17, v0, v16

    const/16 v16, 0x16

    const-string v17, "h4"

    aput-object v17, v0, v16

    const/16 v16, 0x17

    const-string v17, "h5"

    aput-object v17, v0, v16

    const/16 v16, 0x18

    const-string v17, "h6"

    aput-object v17, v0, v16

    const/16 v16, 0x19

    const-string v17, "ul"

    aput-object v17, v0, v16

    const/16 v16, 0x1a

    const-string v17, "ol"

    aput-object v17, v0, v16

    const/16 v16, 0x1b

    const-string v17, "pre"

    aput-object v17, v0, v16

    const/16 v16, 0x1c

    const-string v17, "div"

    aput-object v17, v0, v16

    const/16 v16, 0x1d

    const-string v17, "blockquote"

    aput-object v17, v0, v16

    const/16 v16, 0x1e

    const-string v17, "hr"

    aput-object v17, v0, v16

    const/16 v16, 0x1f

    const-string v17, "address"

    aput-object v17, v0, v16

    const/16 v16, 0x20

    const-string v17, "figure"

    aput-object v17, v0, v16

    const/16 v16, 0x21

    const-string v17, "figcaption"

    aput-object v17, v0, v16

    const/16 v16, 0x22

    const-string v17, "form"

    aput-object v17, v0, v16

    const/16 v16, 0x23

    const-string v17, "fieldset"

    aput-object v17, v0, v16

    const/16 v16, 0x24

    const-string v17, "ins"

    aput-object v17, v0, v16

    const/16 v16, 0x25

    const-string v17, "del"

    aput-object v17, v0, v16

    const/16 v16, 0x26

    const-string v17, "dl"

    aput-object v17, v0, v16

    const/16 v16, 0x27

    const-string v17, "dt"

    aput-object v17, v0, v16

    const/16 v16, 0x28

    const-string v17, "dd"

    aput-object v17, v0, v16

    const/16 v16, 0x29

    const-string v17, "li"

    aput-object v17, v0, v16

    const/16 v16, 0x2a

    const-string v17, "table"

    aput-object v17, v0, v16

    const/16 v16, 0x2b

    const-string v17, "caption"

    aput-object v17, v0, v16

    const/16 v16, 0x2c

    const-string v17, "thead"

    aput-object v17, v0, v16

    const/16 v16, 0x2d

    const-string v17, "tfoot"

    aput-object v17, v0, v16

    const/16 v16, 0x2e

    const-string v17, "tbody"

    aput-object v17, v0, v16

    const/16 v16, 0x2f

    const-string v17, "colgroup"

    aput-object v17, v0, v16

    const/16 v16, 0x30

    const-string v17, "col"

    aput-object v17, v0, v16

    const/16 v16, 0x31

    const-string v17, "tr"

    aput-object v17, v0, v16

    const/16 v16, 0x32

    const-string v17, "th"

    aput-object v17, v0, v16

    const/16 v16, 0x33

    const-string v17, "td"

    aput-object v17, v0, v16

    const/16 v16, 0x34

    const-string v17, "video"

    aput-object v17, v0, v16

    const/16 v16, 0x35

    const-string v17, "audio"

    aput-object v17, v0, v16

    const/16 v16, 0x36

    const-string v17, "canvas"

    aput-object v17, v0, v16

    const/16 v16, 0x37

    const-string v17, "details"

    aput-object v17, v0, v16

    const/16 v16, 0x38

    const-string v17, "menu"

    aput-object v17, v0, v16

    const/16 v16, 0x39

    const-string v17, "plaintext"

    aput-object v17, v0, v16

    .line 199
    sput-object v0, Lorg/jsoup/parser/Tag;->blockTags:[Ljava/lang/String;

    const/16 v0, 0x38

    new-array v0, v0, [Ljava/lang/String;

    const-string v16, "object"

    aput-object v16, v0, v1

    const-string v16, "base"

    aput-object v16, v0, v2

    const-string v16, "font"

    aput-object v16, v0, v3

    const-string v16, "tt"

    aput-object v16, v0, v4

    const-string v16, "i"

    aput-object v16, v0, v5

    const-string v16, "b"

    aput-object v16, v0, v6

    const-string v16, "u"

    aput-object v16, v0, v7

    const-string v16, "big"

    aput-object v16, v0, v8

    const-string v16, "small"

    aput-object v16, v0, v9

    const-string v16, "em"

    aput-object v16, v0, v10

    const-string v16, "strong"

    aput-object v16, v0, v11

    const-string v16, "dfn"

    aput-object v16, v0, v12

    const-string v16, "code"

    aput-object v16, v0, v13

    const-string v16, "samp"

    aput-object v16, v0, v14

    const-string v16, "kbd"

    aput-object v16, v0, v15

    const/16 v16, 0xf

    const-string v17, "var"

    aput-object v17, v0, v16

    const/16 v16, 0x10

    const-string v17, "cite"

    aput-object v17, v0, v16

    const/16 v16, 0x11

    const-string v17, "abbr"

    aput-object v17, v0, v16

    const/16 v16, 0x12

    const-string v17, "time"

    aput-object v17, v0, v16

    const/16 v16, 0x13

    const-string v17, "acronym"

    aput-object v17, v0, v16

    const/16 v16, 0x14

    const-string v17, "mark"

    aput-object v17, v0, v16

    const/16 v16, 0x15

    const-string v17, "ruby"

    aput-object v17, v0, v16

    const/16 v16, 0x16

    const-string v17, "rt"

    aput-object v17, v0, v16

    const/16 v16, 0x17

    const-string v17, "rp"

    aput-object v17, v0, v16

    const/16 v16, 0x18

    const-string v17, "a"

    aput-object v17, v0, v16

    const/16 v16, 0x19

    const-string v17, "img"

    aput-object v17, v0, v16

    const/16 v16, 0x1a

    const-string v17, "br"

    aput-object v17, v0, v16

    const/16 v16, 0x1b

    const-string v17, "wbr"

    aput-object v17, v0, v16

    const/16 v16, 0x1c

    const-string v17, "map"

    aput-object v17, v0, v16

    const/16 v16, 0x1d

    const-string v17, "q"

    aput-object v17, v0, v16

    const/16 v16, 0x1e

    const-string v17, "sub"

    aput-object v17, v0, v16

    const/16 v16, 0x1f

    const-string v17, "sup"

    aput-object v17, v0, v16

    const/16 v16, 0x20

    const-string v17, "bdo"

    aput-object v17, v0, v16

    const/16 v16, 0x21

    const-string v17, "iframe"

    aput-object v17, v0, v16

    const/16 v16, 0x22

    const-string v17, "embed"

    aput-object v17, v0, v16

    const/16 v16, 0x23

    const-string v17, "span"

    aput-object v17, v0, v16

    const/16 v16, 0x24

    const-string v17, "input"

    aput-object v17, v0, v16

    const/16 v16, 0x25

    const-string v17, "select"

    aput-object v17, v0, v16

    const/16 v16, 0x26

    const-string v17, "textarea"

    aput-object v17, v0, v16

    const/16 v16, 0x27

    const-string v17, "label"

    aput-object v17, v0, v16

    const/16 v16, 0x28

    const-string v17, "button"

    aput-object v17, v0, v16

    const/16 v16, 0x29

    const-string v17, "optgroup"

    aput-object v17, v0, v16

    const/16 v16, 0x2a

    const-string v17, "option"

    aput-object v17, v0, v16

    const/16 v16, 0x2b

    const-string v17, "legend"

    aput-object v17, v0, v16

    const/16 v16, 0x2c

    const-string v17, "datalist"

    aput-object v17, v0, v16

    const/16 v16, 0x2d

    const-string v17, "keygen"

    aput-object v17, v0, v16

    const/16 v16, 0x2e

    const-string v17, "output"

    aput-object v17, v0, v16

    const/16 v16, 0x2f

    const-string v17, "progress"

    aput-object v17, v0, v16

    const/16 v16, 0x30

    const-string v17, "meter"

    aput-object v17, v0, v16

    const/16 v16, 0x31

    const-string v17, "area"

    aput-object v17, v0, v16

    const/16 v16, 0x32

    const-string v17, "param"

    aput-object v17, v0, v16

    const/16 v16, 0x33

    const-string v17, "source"

    aput-object v17, v0, v16

    const/16 v16, 0x34

    const-string v17, "track"

    aput-object v17, v0, v16

    const/16 v16, 0x35

    const-string v17, "summary"

    aput-object v17, v0, v16

    const/16 v16, 0x36

    const-string v17, "command"

    aput-object v17, v0, v16

    const/16 v16, 0x37

    const-string v17, "device"

    aput-object v17, v0, v16

    .line 206
    sput-object v0, Lorg/jsoup/parser/Tag;->inlineTags:[Ljava/lang/String;

    new-array v0, v15, [Ljava/lang/String;

    const-string v16, "meta"

    aput-object v16, v0, v1

    const-string v16, "link"

    aput-object v16, v0, v2

    const-string v16, "base"

    aput-object v16, v0, v3

    const-string v16, "frame"

    aput-object v16, v0, v4

    const-string v16, "img"

    aput-object v16, v0, v5

    const-string v16, "br"

    aput-object v16, v0, v6

    const-string v16, "wbr"

    aput-object v16, v0, v7

    const-string v16, "embed"

    aput-object v16, v0, v8

    const-string v16, "hr"

    aput-object v16, v0, v9

    const-string v16, "input"

    aput-object v16, v0, v10

    const-string v16, "keygen"

    aput-object v16, v0, v11

    const-string v16, "col"

    aput-object v16, v0, v12

    const-string v16, "command"

    aput-object v16, v0, v13

    const-string v16, "device"

    aput-object v16, v0, v14

    .line 213
    sput-object v0, Lorg/jsoup/parser/Tag;->emptyTags:[Ljava/lang/String;

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v16, "title"

    aput-object v16, v0, v1

    const-string v16, "a"

    aput-object v16, v0, v2

    const-string v16, "p"

    aput-object v16, v0, v3

    const-string v16, "h1"

    aput-object v16, v0, v4

    const-string v16, "h2"

    aput-object v16, v0, v5

    const-string v16, "h3"

    aput-object v16, v0, v6

    const-string v6, "h4"

    aput-object v6, v0, v7

    const-string v6, "h5"

    aput-object v6, v0, v8

    const-string v6, "h6"

    aput-object v6, v0, v9

    const-string v6, "pre"

    aput-object v6, v0, v10

    const-string v6, "address"

    aput-object v6, v0, v11

    const-string v6, "li"

    aput-object v6, v0, v12

    const-string v6, "th"

    aput-object v6, v0, v13

    const-string v6, "td"

    aput-object v6, v0, v14

    const-string v6, "script"

    aput-object v6, v0, v15

    const/16 v6, 0xf

    const-string v7, "style"

    aput-object v7, v0, v6

    .line 217
    sput-object v0, Lorg/jsoup/parser/Tag;->formatAsInlineTags:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v5, "pre"

    aput-object v5, v0, v1

    const-string v5, "plaintext"

    aput-object v5, v0, v2

    const-string v5, "title"

    aput-object v5, v0, v3

    const-string v3, "textarea"

    aput-object v3, v0, v4

    .line 220
    sput-object v0, Lorg/jsoup/parser/Tag;->preserveWhitespaceTags:[Ljava/lang/String;

    .line 224
    sget-object v0, Lorg/jsoup/parser/Tag;->blockTags:[Ljava/lang/String;

    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v0, v4

    .line 225
    new-instance v6, Lorg/jsoup/parser/Tag;

    invoke-direct {v6, v5}, Lorg/jsoup/parser/Tag;-><init>(Ljava/lang/String;)V

    .line 226
    invoke-static {v6}, Lorg/jsoup/parser/Tag;->register(Lorg/jsoup/parser/Tag;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 228
    :cond_0
    sget-object v0, Lorg/jsoup/parser/Tag;->inlineTags:[Ljava/lang/String;

    array-length v3, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_1

    aget-object v5, v0, v4

    .line 229
    new-instance v6, Lorg/jsoup/parser/Tag;

    invoke-direct {v6, v5}, Lorg/jsoup/parser/Tag;-><init>(Ljava/lang/String;)V

    .line 230
    iput-boolean v1, v6, Lorg/jsoup/parser/Tag;->isBlock:Z

    .line 231
    iput-boolean v1, v6, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    .line 232
    iput-boolean v1, v6, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    .line 233
    invoke-static {v6}, Lorg/jsoup/parser/Tag;->register(Lorg/jsoup/parser/Tag;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 237
    :cond_1
    sget-object v0, Lorg/jsoup/parser/Tag;->emptyTags:[Ljava/lang/String;

    array-length v3, v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_2

    aget-object v5, v0, v4

    .line 238
    sget-object v6, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/jsoup/parser/Tag;

    .line 239
    invoke-static {v5}, Lorg/jsoup/helper/Validate;->notNull(Ljava/lang/Object;)V

    .line 240
    iput-boolean v1, v5, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    .line 241
    iput-boolean v1, v5, Lorg/jsoup/parser/Tag;->canContainInline:Z

    .line 242
    iput-boolean v2, v5, Lorg/jsoup/parser/Tag;->empty:Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 245
    :cond_2
    sget-object v0, Lorg/jsoup/parser/Tag;->formatAsInlineTags:[Ljava/lang/String;

    array-length v3, v0

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v3, :cond_3

    aget-object v5, v0, v4

    .line 246
    sget-object v6, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/jsoup/parser/Tag;

    .line 247
    invoke-static {v5}, Lorg/jsoup/helper/Validate;->notNull(Ljava/lang/Object;)V

    .line 248
    iput-boolean v1, v5, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 251
    :cond_3
    sget-object v0, Lorg/jsoup/parser/Tag;->preserveWhitespaceTags:[Ljava/lang/String;

    array-length v3, v0

    :goto_4
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    .line 252
    sget-object v5, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/jsoup/parser/Tag;

    .line 253
    invoke-static {v4}, Lorg/jsoup/helper/Validate;->notNull(Ljava/lang/Object;)V

    .line 254
    iput-boolean v2, v4, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->isBlock:Z

    .line 18
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    .line 19
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    .line 20
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->canContainInline:Z

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->empty:Z

    .line 22
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->selfClosing:Z

    .line 23
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    .line 26
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    return-void
.end method

.method public static isKnownTag(Ljava/lang/String;)Z
    .locals 1

    .line 144
    sget-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private static register(Lorg/jsoup/parser/Tag;)V
    .locals 2

    .line 259
    sget-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    iget-object v1, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/jsoup/parser/Tag;
    .locals 1

    .line 47
    invoke-static {p0}, Lorg/jsoup/helper/Validate;->notNull(Ljava/lang/Object;)V

    .line 48
    sget-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/jsoup/parser/Tag;

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 52
    invoke-static {p0}, Lorg/jsoup/helper/Validate;->notEmpty(Ljava/lang/String;)V

    .line 53
    sget-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/jsoup/parser/Tag;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lorg/jsoup/parser/Tag;

    invoke-direct {v0, p0}, Lorg/jsoup/parser/Tag;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 58
    iput-boolean p0, v0, Lorg/jsoup/parser/Tag;->isBlock:Z

    const/4 p0, 0x1

    .line 59
    iput-boolean p0, v0, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    :cond_0
    return-object v0
.end method


# virtual methods
.method public canContainBlock()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 164
    :cond_0
    instance-of v1, p1, Lorg/jsoup/parser/Tag;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 166
    :cond_1
    check-cast p1, Lorg/jsoup/parser/Tag;

    .line 168
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    if-eq v1, v3, :cond_2

    return v2

    .line 169
    :cond_2
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->canContainInline:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->canContainInline:Z

    if-eq v1, v3, :cond_3

    return v2

    .line 170
    :cond_3
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->empty:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->empty:Z

    if-eq v1, v3, :cond_4

    return v2

    .line 171
    :cond_4
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    if-eq v1, v3, :cond_5

    return v2

    .line 172
    :cond_5
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->isBlock:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->isBlock:Z

    if-eq v1, v3, :cond_6

    return v2

    .line 173
    :cond_6
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    if-eq v1, v3, :cond_7

    return v2

    .line 174
    :cond_7
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->selfClosing:Z

    iget-boolean v3, p1, Lorg/jsoup/parser/Tag;->selfClosing:Z

    if-eq v1, v3, :cond_8

    return v2

    .line 175
    :cond_8
    iget-object v1, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    iget-object p1, p1, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    return v2

    :cond_9
    return v0
.end method

.method public formatAsBlock()Z
    .locals 1

    .line 80
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 182
    iget-object v0, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 183
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->isBlock:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 184
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->formatAsBlock:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 185
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->canContainBlock:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 186
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->canContainInline:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 187
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->empty:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 188
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->selfClosing:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 189
    iget-boolean v1, p0, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public isBlock()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->isBlock:Z

    return v0
.end method

.method public isData()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->canContainInline:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/jsoup/parser/Tag;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 116
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->empty:Z

    return v0
.end method

.method public isInline()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->isBlock:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isKnownTag()Z
    .locals 2

    .line 134
    sget-object v0, Lorg/jsoup/parser/Tag;->tags:Ljava/util/Map;

    iget-object v1, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSelfClosing()Z
    .locals 1

    .line 125
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->empty:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->selfClosing:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public preserveWhitespace()Z
    .locals 1

    .line 153
    iget-boolean v0, p0, Lorg/jsoup/parser/Tag;->preserveWhitespace:Z

    return v0
.end method

.method setSelfClosing()Lorg/jsoup/parser/Tag;
    .locals 1

    const/4 v0, 0x1

    .line 157
    iput-boolean v0, p0, Lorg/jsoup/parser/Tag;->selfClosing:Z

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 194
    iget-object v0, p0, Lorg/jsoup/parser/Tag;->tagName:Ljava/lang/String;

    return-object v0
.end method
