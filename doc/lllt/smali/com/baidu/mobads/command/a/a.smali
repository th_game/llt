.class public Lcom/baidu/mobads/command/a/a;
.super Lcom/baidu/mobads/command/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/command/b;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;)V

    return-void
.end method

.method public static a(Lcom/baidu/mobads/command/a;)Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 293
    invoke-static {}, Lcom/baidu/mobads/production/b;->f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v0

    .line 297
    :cond_0
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->createAppInfo()Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    move-result-object v0

    .line 298
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setAdId(Ljava/lang/String;)V

    .line 299
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->e()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setAppSize(J)V

    .line 300
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->c()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setClickTime(J)V

    .line 301
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setPackageName(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setQk(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setProd(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0}, Lcom/baidu/mobads/command/a;->f()Z

    move-result p0

    invoke-interface {v0, p0}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->setTooLarge(Z)V

    :cond_1
    return-object v0
.end method

.method private b(Lcom/baidu/mobads/command/a;)V
    .locals 3

    .line 280
    invoke-static {}, Lcom/baidu/mobads/production/b;->f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 281
    invoke-static {p1}, Lcom/baidu/mobads/command/a/a;->a(Lcom/baidu/mobads/command/a;)Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 283
    invoke-static {}, Lcom/baidu/mobads/production/b;->f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 284
    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getXMonitorActivation(Landroid/content/Context;Lcom/baidu/mobads/interfaces/utils/IXAdLogger;)Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;->addAppInfoForMonitor(Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;)V

    goto :goto_0

    .line 286
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "addAppInfoForMonitor error, appInfo is null"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private b()Z
    .locals 3

    .line 227
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 228
    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/utils/o;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 22

    move-object/from16 v1, p0

    const-string v2, "XAdDownloadAPKCommand"

    .line 49
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-nez v0, :cond_0

    return-void

    .line 52
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v3

    .line 53
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 54
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v4

    .line 57
    :try_start_0
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v5

    .line 58
    iget-object v6, v1, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "download pkg = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\uff0c DownloadURL= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v2, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, ""

    if-eqz v5, :cond_1

    .line 59
    :try_start_1
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    iget-object v7, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 60
    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginClickUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 62
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v7, "start to download but package is empty"

    invoke-interface {v5, v2, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginClickUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/baidu/mobads/utils/f;->getMD5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_2
    move-object v8, v5

    .line 66
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/baidu/mobads/openad/b/d;->getAdsApkDownloader(Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object v15

    .line 68
    invoke-static {v8}, Lcom/baidu/mobads/openad/b/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;

    move-result-object v5

    const/4 v14, 0x0

    if-eqz v5, :cond_a

    if-eqz v15, :cond_a

    .line 70
    invoke-virtual {v5}, Lcom/baidu/mobads/openad/b/b;->a()Lcom/baidu/mobads/command/a;

    move-result-object v5

    .line 71
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object v7

    .line 72
    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "startDownload>> downloader exist: state="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v2, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    sget-object v9, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v7, v9, :cond_9

    sget-object v9, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v7, v9, :cond_9

    sget-object v9, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v7, v9, :cond_3

    goto/16 :goto_1

    .line 79
    :cond_3
    sget-object v9, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v7, v9, :cond_5

    .line 80
    iget-object v7, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v7, v5}, Lcom/baidu/mobads/command/a/a;->a(Landroid/content/Context;Lcom/baidu/mobads/command/a;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 81
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->pintHttpInNewThread(Ljava/lang/String;)V

    .line 82
    invoke-direct {v1, v5}, Lcom/baidu/mobads/command/a/a;->b(Lcom/baidu/mobads/command/a;)V

    return-void

    .line 86
    :cond_4
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->cancel()V

    .line 87
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->removeObservers()V

    .line 88
    invoke-static {v8}, Lcom/baidu/mobads/openad/b/b;->b(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;

    .line 89
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/baidu/mobads/openad/b/d;->removeAdsApkDownloader(Ljava/lang/String;)Ljava/lang/Boolean;

    goto/16 :goto_2

    .line 90
    :cond_5
    sget-object v5, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v7, v5, :cond_6

    sget-object v5, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v7, v5, :cond_c

    .line 91
    :cond_6
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v5, 0x211

    const-string v7, "downloading"

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v9, :cond_7

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 92
    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v9

    .line 93
    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_7
    move-object v9, v6

    :goto_0
    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 94
    invoke-virtual {v3, v10}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v11, :cond_8

    iget-object v6, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 95
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    .line 96
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_8
    move-object v11, v6

    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v16, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v17, Landroid/os/Build$VERSION;->SDK_INT:I

    move-object v4, v0

    move-object v6, v7

    move-object v7, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object/from16 v13, v16

    const/4 v0, 0x0

    move/from16 v14, v17

    .line 91
    invoke-virtual/range {v3 .. v14}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 102
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 103
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isPopNotif()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 102
    invoke-virtual {v1, v3, v4, v0, v5}, Lcom/baidu/mobads/command/a/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/Boolean;)V

    return-void

    .line 76
    :cond_9
    :goto_1
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->resume()V

    .line 77
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->pintHttpInNewThread(Ljava/lang/String;)V

    return-void

    :cond_a
    if-eqz v15, :cond_b

    .line 109
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->cancel()V

    .line 110
    invoke-interface {v15}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->removeObservers()V

    .line 112
    :cond_b
    invoke-static {v8}, Lcom/baidu/mobads/openad/b/b;->b(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;

    .line 113
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/baidu/mobads/openad/b/d;->removeAdsApkDownloader(Ljava/lang/String;)Ljava/lang/Boolean;

    .line 117
    :cond_c
    :goto_2
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/baidu/mobads/command/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/command/a;

    move-result-object v5

    const/4 v7, 0x1

    if-eqz v5, :cond_e

    .line 121
    iget-object v9, v5, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v10, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v9, v10, :cond_d

    .line 123
    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v9, v5}, Lcom/baidu/mobads/command/a/a;->a(Landroid/content/Context;Lcom/baidu/mobads/command/a;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 124
    invoke-direct {v1, v5}, Lcom/baidu/mobads/command/a/a;->b(Lcom/baidu/mobads/command/a;)V

    return-void

    .line 129
    :cond_d
    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->pintHttpInNewThread(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 130
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/baidu/mobads/command/a/a;->b()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 131
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v7, 0x211

    const-string v9, "alreadyinstalled1"

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v10, :cond_f

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 132
    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v10

    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    :cond_f
    move-object v10, v6

    :goto_3
    iget-object v11, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 133
    invoke-virtual {v3, v11}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v12, :cond_10

    iget-object v6, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 134
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_10
    move-object v12, v6

    .line 135
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v15, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    move-object v4, v5

    move v5, v7

    move-object v6, v9

    move-object v7, v10

    move-object v9, v11

    move-object v10, v12

    move-object v11, v13

    move-object v12, v14

    move-object v13, v15

    move/from16 v14, v16

    .line 131
    invoke-virtual/range {v3 .. v14}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 137
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    iget-object v4, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 138
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/baidu/mobads/utils/o;->openApp(Landroid/content/Context;Ljava/lang/String;)V

    .line 139
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->pintHttpInNewThread(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/baidu/mobads/production/b;->f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    iget-object v4, v1, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 147
    invoke-interface {v0, v3, v4}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getXMonitorActivation(Landroid/content/Context;Lcom/baidu/mobads/interfaces/utils/IXAdLogger;)Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;->startMonitor()V

    return-void

    .line 151
    :cond_11
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 152
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 153
    :cond_12
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 154
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    :cond_13
    const-string v0, "\u60a8\u70b9\u51fb\u7684\u5e94\u7528"

    .line 158
    :cond_14
    new-instance v5, Lcom/baidu/mobads/command/a;

    invoke-direct {v5, v8, v0}, Lcom/baidu/mobads/command/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v0

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 160
    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v11}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isAutoOpen()Z

    move-result v11

    .line 159
    invoke-virtual {v5, v0, v9, v10, v11}, Lcom/baidu/mobads/command/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 161
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isPopNotif()Z

    move-result v0

    iput-boolean v0, v5, Lcom/baidu/mobads/command/a;->m:Z

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v5, Lcom/baidu/mobads/command/a;->j:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/baidu/mobads/utils/f;->getMD5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".apk"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 163
    invoke-static {v9}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 162
    invoke-virtual {v5, v0, v9}, Lcom/baidu/mobads/command/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v0, :cond_15

    .line 165
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v0

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v9

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v0, v9}, Lcom/baidu/mobads/command/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_15
    invoke-static {v8}, Lcom/baidu/mobads/openad/b/b;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, v5, Lcom/baidu/mobads/command/a;->f:I

    .line 168
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isActionOnlyWifi()Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    goto :goto_4

    :cond_16
    const/4 v0, 0x0

    :goto_4
    iput-boolean v0, v5, Lcom/baidu/mobads/command/a;->s:Z

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v5, v9, v10}, Lcom/baidu/mobads/command/a;->a(J)V

    .line 171
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppSize()J

    move-result-wide v9

    invoke-virtual {v5, v9, v10}, Lcom/baidu/mobads/command/a;->b(J)V

    .line 172
    iget-object v0, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isTooLarge()Z

    move-result v0

    invoke-virtual {v5, v0}, Lcom/baidu/mobads/command/a;->a(Z)V

    .line 174
    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iput-wide v9, v5, Lcom/baidu/mobads/command/a;->t:J

    .line 176
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v9}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;

    move-result-object v15

    new-instance v0, Ljava/net/URL;

    iget-object v9, v5, Lcom/baidu/mobads/command/a;->j:Ljava/lang/String;

    invoke-direct {v0, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v9, v5, Lcom/baidu/mobads/command/a;->c:Ljava/lang/String;

    iget-object v10, v5, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    const/16 v19, 0x3

    iget-object v11, v5, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    iget-object v12, v5, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v17, v9

    move-object/from16 v18, v10

    move-object/from16 v20, v11

    move-object/from16 v21, v12

    invoke-interface/range {v15 .. v21}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;->createAdsApkDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object v0

    .line 179
    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAPOOpen()Z

    move-result v9

    if-ne v9, v7, :cond_17

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 180
    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getPage()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_17

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 181
    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getPage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_17

    .line 182
    iput-boolean v7, v5, Lcom/baidu/mobads/command/a;->w:Z

    .line 183
    iget-object v7, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getPage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/baidu/mobads/command/a;->x:Ljava/lang/String;

    .line 185
    :cond_17
    new-instance v7, Lcom/baidu/mobads/openad/b/b;

    iget-object v9, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-direct {v7, v9, v5}, Lcom/baidu/mobads/openad/b/b;-><init>(Landroid/content/Context;Lcom/baidu/mobads/command/a;)V

    .line 186
    invoke-interface {v0, v7}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->addObserver(Ljava/util/Observer;)V

    .line 189
    iget-boolean v5, v5, Lcom/baidu/mobads/command/a;->s:Z

    if-nez v5, :cond_1a

    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    invoke-interface {v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->is3GConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 190
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v7, 0x211

    const-string v9, "waitwifi"

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v10, :cond_18

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 191
    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v10

    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v10

    goto :goto_6

    :cond_18
    move-object v10, v6

    :goto_6
    iget-object v11, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 192
    invoke-virtual {v3, v11}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v12, :cond_19

    iget-object v6, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 193
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_19
    move-object v12, v6

    .line 194
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v16, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v17, Landroid/os/Build$VERSION;->SDK_INT:I

    move-object v4, v5

    move v5, v7

    move-object v6, v9

    move-object v7, v10

    move-object v9, v11

    move-object v10, v12

    move-object v11, v13

    move-object v12, v15

    move-object/from16 v13, v16

    const/4 v15, 0x0

    move/from16 v14, v17

    .line 190
    invoke-virtual/range {v3 .. v14}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 197
    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->pause()V

    .line 198
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " \u5c06\u5728\u8fde\u5165Wifi\u540e\u5f00\u59cb\u4e0b\u8f7d"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, v1, Lcom/baidu/mobads/command/a/a;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 199
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isPopNotif()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 198
    invoke-virtual {v1, v3, v0, v15, v4}, Lcom/baidu/mobads/command/a/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/Boolean;)V

    goto/16 :goto_8

    .line 204
    :cond_1a
    iget-object v5, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v7, 0x20f

    const-string v9, "realstart"

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v10, :cond_1b

    iget-object v10, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 205
    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v10

    invoke-interface {v10}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    :cond_1b
    move-object v10, v6

    :goto_7
    iget-object v11, v1, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 206
    invoke-virtual {v3, v11}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v12, :cond_1c

    iget-object v6, v1, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 207
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_1c
    move-object v12, v6

    .line 208
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v15, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    move-object v4, v5

    move v5, v7

    move-object v6, v9

    move-object v7, v10

    move-object v9, v11

    move-object v10, v12

    move-object v11, v13

    move-object v12, v14

    move-object v13, v15

    move/from16 v14, v16

    .line 204
    invoke-virtual/range {v3 .. v14}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 210
    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8

    :catch_0
    move-exception v0

    .line 214
    iget-object v3, v1, Lcom/baidu/mobads/command/a/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v3, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ad app download failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :goto_8
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/Boolean;)V
    .locals 0

    .line 221
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    if-eqz p4, :cond_0

    .line 222
    invoke-static {p1, p2, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/baidu/mobads/command/a;)Z
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 240
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    iget-object v4, v2, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    .line 241
    invoke-virtual {v3, v1, v4}, Lcom/baidu/mobads/utils/o;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 242
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v5

    .line 243
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v4

    const/16 v17, 0x1

    const-string v6, ""

    if-eqz v3, :cond_2

    .line 245
    iget-object v3, v0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v7, 0x211

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v8, :cond_0

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 249
    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v8

    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    goto :goto_0

    :cond_0
    move-object v9, v6

    :goto_0
    iget-object v10, v2, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 251
    invoke-virtual {v5, v8}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v8, :cond_1

    iget-object v6, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 252
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_1
    move-object v12, v6

    .line 253
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v15, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v8, "alreadyinstalled"

    move-object v6, v3

    .line 245
    invoke-virtual/range {v5 .. v16}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 255
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/baidu/mobads/utils/o;->openApp(Landroid/content/Context;Ljava/lang/String;)V

    return v17

    .line 258
    :cond_2
    iget-object v3, v0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    const/16 v7, 0x211

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v8, :cond_3

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 262
    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v8

    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    goto :goto_1

    :cond_3
    move-object v9, v6

    :goto_1
    iget-object v10, v2, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->a:Landroid/content/Context;

    .line 264
    invoke-virtual {v5, v8}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v8, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    if-eqz v8, :cond_4

    iget-object v6, v0, Lcom/baidu/mobads/command/a/a;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 265
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdRequestInfo;->getApid()Ljava/lang/String;

    move-result-object v6

    :cond_4
    move-object v12, v6

    .line 266
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v15, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v8, "alreadydownloaded"

    move-object v6, v3

    .line 258
    invoke-virtual/range {v5 .. v16}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 268
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/baidu/mobads/command/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 269
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_5

    .line 271
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/baidu/mobads/utils/o;->b(Landroid/content/Context;Ljava/lang/String;)V

    return v17

    :cond_5
    const/4 v1, 0x0

    return v1
.end method
