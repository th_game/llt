.class Lcom/baidu/mobads/f/i;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/f/g;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/f/g;Landroid/os/Looper;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .line 143
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "APK_INFO"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/f/e;

    const-string v1, "OK"

    .line 146
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "XAdApkLoader"

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_3

    .line 147
    new-instance v0, Lcom/baidu/mobads/f/b;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {v6}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v1, v6, p1}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/baidu/mobads/f/e;)V

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    iget-object v1, v1, Lcom/baidu/mobads/f/g;->g:Landroid/os/Handler;

    sget-object v6, Lcom/baidu/mobads/f/g;->f:Landroid/os/Handler;

    if-ne v1, v6, :cond_1

    .line 158
    invoke-virtual {v0}, Lcom/baidu/mobads/f/b;->a()V

    .line 160
    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 161
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/b;->a(Ljava/lang/String;)V

    .line 162
    sget-object v1, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    if-eqz v1, :cond_0

    .line 163
    sget-object v1, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->b()D

    move-result-wide v6

    iput-wide v6, v1, Lcom/baidu/mobads/f/a;->a:D

    .line 166
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g;)V

    .line 167
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/g;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 169
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v5}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Z)Z

    .line 170
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    iget-object v1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {v1}, Lcom/baidu/mobads/f/g;->d(Lcom/baidu/mobads/f/g;)Z

    move-result v1

    const-string v6, "load remote file just downloaded"

    invoke-static {p1, v1, v6}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;ZLjava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/b;)V

    .line 175
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/f/b;->a(Ljava/lang/String;)V

    .line 176
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v4}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g;Z)V
    :try_end_0
    .catch Lcom/baidu/mobads/f/g$a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lcom/baidu/mobads/f/b;->delete()Z

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 179
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download apk file failed: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/g$a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 180
    iget-object v1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {v1, v5}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g;Z)V

    .line 181
    iget-object v1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {v1}, Lcom/baidu/mobads/f/g;->e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v5

    aput-object p1, v3, v4

    invoke-interface {v1, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 183
    :goto_1
    invoke-virtual {v0}, Lcom/baidu/mobads/f/b;->delete()Z

    throw p1

    .line 186
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    new-array v1, v3, [Ljava/lang/Object;

    aput-object v2, v1, v5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mOnApkDownloadCompleted: download failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-interface {p1, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 187
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v5}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g;Z)V

    .line 188
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 189
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v5}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Z)Z

    .line 190
    iget-object p1, p0, Lcom/baidu/mobads/f/i;->a:Lcom/baidu/mobads/f/g;

    const-string v0, "Refused to download remote for version..."

    invoke-static {p1, v5, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;ZLjava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method
