.class Lcom/baidu/mobads/f/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# instance fields
.field final synthetic a:D

.field final synthetic b:Lcom/baidu/mobads/f/l;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/f/l;D)V
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iput-wide p2, p0, Lcom/baidu/mobads/f/m;->a:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 8

    .line 469
    iget-object v0, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v0, v0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->f(Lcom/baidu/mobads/f/g;)V

    .line 470
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "URLLoader.Load.Complete"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v0, v0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    new-instance v2, Lcom/baidu/mobads/f/e;

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v3, "message"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v2, p1}, Lcom/baidu/mobads/f/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/e;)Lcom/baidu/mobads/f/e;

    .line 472
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v2

    .line 473
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    .line 474
    invoke-static {p1}, Lcom/baidu/mobads/f/g;->g(Lcom/baidu/mobads/f/g;)Landroid/content/SharedPreferences;

    move-result-object p1

    const/4 v0, 0x0

    const-string v4, "__badApkVersion__8.8085"

    .line 475
    invoke-interface {p1, v4, v0}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result p1

    .line 476
    iget-object v0, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v0, v0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/f/e;->b()D

    move-result-wide v4

    double-to-float v0, v4

    const/4 v4, 0x1

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 488
    iget-object v5, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v5, v5, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v5, v5, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v5}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/f/e;->b()D

    move-result-wide v5

    cmpg-double v7, v2, v5

    if-gtz v7, :cond_1

    .line 489
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    iget-object v5, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v5, v5, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v5, v5, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v5}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object v5

    .line 490
    invoke-virtual {v5}, Lcom/baidu/mobads/f/e;->b()D

    move-result-wide v5

    .line 489
    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    cmpl-double v7, v2, v5

    if-nez v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 488
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 492
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "try to download apk badVer="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p1, ", isBad="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", compatible="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v3, "XAdApkLoader"

    invoke-static {v3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iget-wide v3, p0, Lcom/baidu/mobads/f/m;->a:D

    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->b()D

    move-result-wide v5

    cmpg-double p1, v3, v5

    if-gez p1, :cond_2

    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    .line 499
    invoke-static {p1}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    .line 500
    invoke-static {p1}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->a()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 501
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 502
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    iget-object v0, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object v0, v0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/e;)V

    goto :goto_2

    .line 504
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 505
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v1}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Z)Z

    .line 506
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    const-string v0, "Refused to download remote for version..."

    invoke-static {p1, v1, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;ZLjava/lang/String;)V

    goto :goto_2

    .line 510
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {p1}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 511
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {p1, v1}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Z)Z

    .line 512
    iget-object p1, p0, Lcom/baidu/mobads/f/m;->b:Lcom/baidu/mobads/f/l;

    iget-object p1, p1, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object p1, p1, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    const-string v0, "remote update Network access failed"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;ZLjava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method
