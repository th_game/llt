.class public Lcom/baidu/mobads/f/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/f/o$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/net/URL;

.field private c:Ljava/lang/String;

.field private final d:Lcom/baidu/mobads/f/e;

.field private e:Lcom/baidu/mobads/f/o$a;

.field private f:Landroid/content/SharedPreferences;

.field private g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/f/e;Lcom/baidu/mobads/f/o$a;)V
    .locals 1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/baidu/mobads/f/o;->b:Ljava/net/URL;

    .line 36
    iput-object v0, p0, Lcom/baidu/mobads/f/o;->c:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/baidu/mobads/f/p;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/f/p;-><init>(Lcom/baidu/mobads/f/o;)V

    iput-object v0, p0, Lcom/baidu/mobads/f/o;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 69
    iput-object p2, p0, Lcom/baidu/mobads/f/o;->c:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lcom/baidu/mobads/f/o;->d:Lcom/baidu/mobads/f/e;

    .line 71
    invoke-direct {p0, p1, p4}, Lcom/baidu/mobads/f/o;->a(Landroid/content/Context;Lcom/baidu/mobads/f/o$a;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;Lcom/baidu/mobads/f/e;Lcom/baidu/mobads/f/o$a;)V
    .locals 1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/baidu/mobads/f/o;->b:Ljava/net/URL;

    .line 36
    iput-object v0, p0, Lcom/baidu/mobads/f/o;->c:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/baidu/mobads/f/p;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/f/p;-><init>(Lcom/baidu/mobads/f/o;)V

    iput-object v0, p0, Lcom/baidu/mobads/f/o;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 63
    iput-object p2, p0, Lcom/baidu/mobads/f/o;->b:Ljava/net/URL;

    .line 64
    iput-object p3, p0, Lcom/baidu/mobads/f/o;->d:Lcom/baidu/mobads/f/e;

    .line 65
    invoke-direct {p0, p1, p4}, Lcom/baidu/mobads/f/o;->a(Landroid/content/Context;Lcom/baidu/mobads/f/o$a;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/baidu/mobads/f/o$a;)V
    .locals 1

    .line 75
    iput-object p1, p0, Lcom/baidu/mobads/f/o;->a:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/baidu/mobads/f/o;->e:Lcom/baidu/mobads/f/o$a;

    .line 78
    iget-object p1, p0, Lcom/baidu/mobads/f/o;->a:Landroid/content/Context;

    const-string p2, "__xadsdk_downloaded__version__"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/f/o;->f:Landroid/content/SharedPreferences;

    .line 80
    iget-object p1, p0, Lcom/baidu/mobads/f/o;->f:Landroid/content/SharedPreferences;

    iget-object p2, p0, Lcom/baidu/mobads/f/o;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {p1, p2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 93
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/f/o;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/baidu/mobads/f/o;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/baidu/mobads/f/o;->b:Ljava/net/URL;

    :goto_0
    const/4 v1, 0x0

    .line 95
    invoke-interface {v0, v2, p1, p2, v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;->createSimpleFileDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object p1

    .line 96
    invoke-interface {p1, p0}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->addObserver(Ljava/util/Observer;)V

    .line 97
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->start()V

    .line 100
    iget-object p1, p0, Lcom/baidu/mobads/f/o;->f:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 101
    iget-object p2, p0, Lcom/baidu/mobads/f/o;->d:Lcom/baidu/mobads/f/e;

    invoke-virtual {p2}, Lcom/baidu/mobads/f/e;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "version"

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 106
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x9

    if-lt p2, v0, :cond_1

    .line 107
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 109
    :cond_1
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4

    .line 115
    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 117
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p2, v0, :cond_0

    .line 118
    iget-object p2, p0, Lcom/baidu/mobads/f/o;->e:Lcom/baidu/mobads/f/o$a;

    new-instance v0, Lcom/baidu/mobads/f/e;

    iget-object v1, p0, Lcom/baidu/mobads/f/o;->d:Lcom/baidu/mobads/f/e;

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/baidu/mobads/f/e;-><init>(Lcom/baidu/mobads/f/e;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-interface {p2, v0}, Lcom/baidu/mobads/f/o$a;->a(Lcom/baidu/mobads/f/e;)V

    .line 120
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p2, v0, :cond_1

    .line 121
    iget-object p2, p0, Lcom/baidu/mobads/f/o;->e:Lcom/baidu/mobads/f/o$a;

    new-instance v0, Lcom/baidu/mobads/f/e;

    iget-object v1, p0, Lcom/baidu/mobads/f/o;->d:Lcom/baidu/mobads/f/e;

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/baidu/mobads/f/e;-><init>(Lcom/baidu/mobads/f/e;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-interface {p2, v0}, Lcom/baidu/mobads/f/o$a;->b(Lcom/baidu/mobads/f/e;)V

    :cond_1
    return-void
.end method
