.class Lcom/baidu/mobads/f/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/f/g$c;

.field final synthetic b:Landroid/os/Handler;

.field final synthetic c:Lcom/baidu/mobads/f/g;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V
    .locals 0

    .line 714
    iput-object p1, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    iput-object p2, p0, Lcom/baidu/mobads/f/n;->a:Lcom/baidu/mobads/f/g$c;

    iput-object p3, p0, Lcom/baidu/mobads/f/n;->b:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/16 v0, 0x9

    .line 727
    :try_start_0
    const-class v1, Lcom/baidu/mobads/f/g;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 728
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    iget-object v3, p0, Lcom/baidu/mobads/f/n;->a:Lcom/baidu/mobads/f/g$c;

    iget-object v4, p0, Lcom/baidu/mobads/f/n;->b:Landroid/os/Handler;

    invoke-static {v2, v3, v4}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V

    .line 729
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 751
    :try_start_2
    iget-object v1, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v1}, Lcom/baidu/mobads/f/g;->g(Lcom/baidu/mobads/f/g;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "previousProxyVersion"

    .line 752
    iget-object v3, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-virtual {v3}, Lcom/baidu/mobads/f/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 757
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v0, :cond_0

    .line 758
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 760
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 763
    iget-object v1, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v1}, Lcom/baidu/mobads/f/g;->e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "XAdApkLoader"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_1
    move-exception v2

    .line 729
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v1

    .line 731
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Load APK Failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 732
    iget-object v2, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v2}, Lcom/baidu/mobads/f/g;->e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "XAdApkLoader"

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 733
    iget-object v1, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v1, v5}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/g;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 751
    :try_start_6
    iget-object v1, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v1}, Lcom/baidu/mobads/f/g;->g(Lcom/baidu/mobads/f/g;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "previousProxyVersion"

    .line 752
    iget-object v3, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-virtual {v3}, Lcom/baidu/mobads/f/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 757
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v0, :cond_1

    .line 758
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 760
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_0
    return-void

    :catchall_3
    move-exception v1

    .line 751
    :try_start_7
    iget-object v2, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v2}, Lcom/baidu/mobads/f/g;->g(Lcom/baidu/mobads/f/g;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "previousProxyVersion"

    .line 752
    iget-object v4, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-virtual {v4}, Lcom/baidu/mobads/f/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 757
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v0, :cond_2

    .line 758
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 760
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception v0

    .line 763
    iget-object v2, p0, Lcom/baidu/mobads/f/n;->c:Lcom/baidu/mobads/f/g;

    invoke-static {v2}, Lcom/baidu/mobads/f/g;->e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "XAdApkLoader"

    invoke-interface {v2, v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 764
    :goto_1
    throw v1
.end method
