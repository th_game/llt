.class public Lcom/baidu/mobads/f/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/f/q$a;
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String; = ""

.field private static b:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private static volatile c:Lcom/baidu/mobads/f/q;


# instance fields
.field private d:Landroid/content/Context;

.field private e:Lcom/baidu/mobads/f/q$a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/f/q;->d:Landroid/content/Context;

    .line 42
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/f/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 43
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/baidu/mobads/f/r;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/f/r;-><init>(Lcom/baidu/mobads/f/q;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 59
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;
    .locals 2

    .line 30
    sget-object v0, Lcom/baidu/mobads/f/q;->c:Lcom/baidu/mobads/f/q;

    if-nez v0, :cond_1

    .line 31
    const-class v0, Lcom/baidu/mobads/f/q;

    monitor-enter v0

    .line 32
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/f/q;->c:Lcom/baidu/mobads/f/q;

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Lcom/baidu/mobads/f/q;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/f/q;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/f/q;->c:Lcom/baidu/mobads/f/q;

    .line 35
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 37
    :cond_1
    :goto_0
    sget-object p0, Lcom/baidu/mobads/f/q;->c:Lcom/baidu/mobads/f/q;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/f/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 168
    invoke-direct {p0}, Lcom/baidu/mobads/f/q;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .line 110
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 114
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    .line 115
    array-length v1, p1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    .line 116
    :goto_1
    array-length v2, p1

    if-ge v1, v2, :cond_6

    .line 117
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "junit.framework"

    .line 118
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    const-string v3, "com.baidu.mobads.container"

    .line 122
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "remote"

    goto :goto_2

    :cond_2
    const-string v3, "com.baidu.mobads.loader"

    .line 125
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "loader"

    goto :goto_2

    :cond_3
    const-string v3, "com.baidu.mobads_vr"

    .line 128
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v0, "vr"

    goto :goto_2

    :cond_4
    const-string v3, "com.baidu.mobads"

    .line 131
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "proxy"

    goto :goto_2

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    :goto_2
    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobads/f/q;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/baidu/mobads/f/q;->e()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 155
    invoke-direct {p0}, Lcom/baidu/mobads/f/q;->d()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_crash_source"

    .line 156
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "key_crash_trace"

    .line 157
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 159
    sget-object p1, Lcom/baidu/mobads/f/q;->a:Ljava/lang/String;

    const-string p2, "key_crash_ad"

    invoke-interface {v0, p2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 160
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x9

    if-lt p1, p2, :cond_0

    .line 161
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 163
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method

.method private c()Landroid/content/SharedPreferences;
    .locals 3

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/f/q;->d:Landroid/content/Context;

    const-string v1, "baidu_mobads_crash"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private d()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .line 147
    invoke-direct {p0}, Lcom/baidu/mobads/f/q;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 3

    .line 172
    invoke-direct {p0}, Lcom/baidu/mobads/f/q;->d()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 174
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 175
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 177
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 63
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/f/q;

    if-nez v0, :cond_0

    .line 64
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/f/q$a;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/baidu/mobads/f/q;->e:Lcom/baidu/mobads/f/q$a;

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/baidu/mobads/f/q;->e:Lcom/baidu/mobads/f/q$a;

    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .line 71
    :try_start_0
    invoke-direct {p0, p2}, Lcom/baidu/mobads/f/q;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 74
    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/f/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 77
    iget-object v1, p0, Lcom/baidu/mobads/f/q;->e:Lcom/baidu/mobads/f/q$a;

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/baidu/mobads/f/q;->e:Lcom/baidu/mobads/f/q$a;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/f/q$a;->a(Ljava/lang/String;)V

    .line 81
    :cond_0
    sget-object v0, Lcom/baidu/mobads/f/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 82
    sget-object v0, Lcom/baidu/mobads/f/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 85
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method
