.class public Lcom/baidu/mobads/f/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/f/g$b;,
        Lcom/baidu/mobads/f/g$a;,
        Lcom/baidu/mobads/f/g$c;
    }
.end annotation


# static fields
.field protected static a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field protected static volatile b:Lcom/baidu/mobads/f/a;

.field protected static volatile c:Lcom/baidu/mobads/f/a;

.field protected static volatile d:Ljava/lang/Class;

.field protected static e:Ljava/lang/String;

.field protected static final f:Landroid/os/Handler;

.field private static i:Ljava/lang/String;


# instance fields
.field protected g:Landroid/os/Handler;

.field protected final h:Landroid/os/Handler;

.field private j:Lcom/baidu/mobads/openad/d/a;

.field private k:Lcom/baidu/mobads/f/e;

.field private final l:Landroid/content/Context;

.field private m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private n:Z

.field private o:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/baidu/mobads/f/g$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 114
    new-instance v0, Lcom/baidu/mobads/f/h;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/baidu/mobads/f/h;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/baidu/mobads/f/g;->f:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v0, 0x0

    .line 112
    iput-boolean v0, p0, Lcom/baidu/mobads/f/g;->n:Z

    .line 120
    sget-object v0, Lcom/baidu/mobads/f/g;->f:Landroid/os/Handler;

    iput-object v0, p0, Lcom/baidu/mobads/f/g;->g:Landroid/os/Handler;

    .line 121
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 139
    new-instance v0, Lcom/baidu/mobads/f/i;

    .line 140
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/f/i;-><init>(Lcom/baidu/mobads/f/g;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobads/f/g;->h:Landroid/os/Handler;

    .line 209
    sget-object v0, Lcom/baidu/mobads/f/g;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 210
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://mobads.baidu.com/ads/pa/"

    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getMajorVersionNumber()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "/__pasys_remote_banner.php"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/f/g;->i:Ljava/lang/String;

    .line 215
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    .line 216
    invoke-static {p1}, Lcom/baidu/mobads/f/g;->c(Landroid/content/Context;)V

    .line 218
    sget-object v0, Lcom/baidu/mobads/f/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-nez v0, :cond_1

    .line 219
    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/f/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 220
    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p1

    new-instance v0, Lcom/baidu/mobads/f/j;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/f/j;-><init>(Lcom/baidu/mobads/f/g;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/f/q;->a(Lcom/baidu/mobads/f/q$a;)V

    .line 236
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobads/f/q;

    if-nez p1, :cond_2

    .line 237
    sget-object p1, Lcom/baidu/mobads/f/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {p1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)D
    .locals 4

    const-wide/16 v0, 0x0

    .line 885
    :try_start_0
    new-instance p0, Ljava/io/File;

    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 886
    invoke-static {p0}, Lcom/baidu/mobads/utils/m;->a(Ljava/io/File;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 887
    new-instance p1, Ljava/util/jar/JarFile;

    invoke-direct {p1, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 888
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object p0

    const-string v2, "Implementation-Version"

    .line 889
    invoke-virtual {p0, v2}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 888
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 890
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double p0, v2, v0

    if-lez p0, :cond_0

    return-wide v2

    :catch_0
    :cond_0
    return-wide v0
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;)Landroid/content/Context;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/e;)Lcom/baidu/mobads/f/e;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/baidu/mobads/f/g;->k:Lcom/baidu/mobads/f/e;

    return-object p1
.end method

.method private a(Lcom/baidu/mobads/f/a;)Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 802
    :try_start_0
    invoke-virtual {p1}, Lcom/baidu/mobads/f/a;->a()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/openad/d/a;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 593
    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const-string v1, "baidu_ad_sdk"

    .line 594
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    .line 595
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    .line 597
    :cond_0
    sget-object p0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    const-string p0, ""

    return-object p0

    .line 600
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "__xadsdk__remote__final__running__.jar"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/baidu/mobads/f/b;)V
    .locals 6

    .line 366
    invoke-virtual {p1}, Lcom/baidu/mobads/f/b;->b()Ljava/lang/Class;

    move-result-object v1

    .line 367
    monitor-enter p0

    .line 368
    :try_start_0
    new-instance p1, Lcom/baidu/mobads/f/a;

    iget-object v2, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    .line 369
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v3

    sget-object v5, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->DEBUG:Ljava/lang/Boolean;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/f/a;-><init>(Ljava/lang/Class;Landroid/content/Context;DLjava/lang/Boolean;)V

    sput-object p1, Lcom/baidu/mobads/f/g;->c:Lcom/baidu/mobads/f/a;

    .line 371
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/baidu/mobads/f/e;)V
    .locals 4

    .line 679
    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 680
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    sget-object v1, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/f/g;->h:Landroid/os/Handler;

    .line 681
    invoke-static {v0, p1, v1, v2}, Lcom/baidu/mobads/f/c;->a(Landroid/content/Context;Lcom/baidu/mobads/f/e;Ljava/lang/String;Landroid/os/Handler;)Lcom/baidu/mobads/f/c;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Lcom/baidu/mobads/f/c;->isAlive()Z

    move-result v1

    const-string v2, "XAdApkLoader"

    if-nez v1, :cond_0

    .line 684
    iget-object p1, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XApkDownloadThread starting ..."

    invoke-interface {p1, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    invoke-virtual {v0}, Lcom/baidu/mobads/f/c;->start()V

    goto :goto_0

    .line 687
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v3, "XApkDownloadThread already started"

    invoke-interface {v1, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    invoke-virtual {p1}, Lcom/baidu/mobads/f/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/f/c;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/b;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/b;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;ZLjava/lang/String;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/f/g;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .line 286
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 287
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "success"

    .line 288
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 290
    iput p1, v0, Landroid/os/Message;->what:I

    .line 291
    iget-object p1, p0, Lcom/baidu/mobads/f/g;->g:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private declared-synchronized a(ZLjava/lang/String;)V
    .locals 2

    monitor-enter p0

    .line 574
    :try_start_0
    iget-object p2, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-static {p2}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/f/q;->b()V

    .line 579
    iget-object p2, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result p2

    if-lez p2, :cond_0

    .line 580
    iget-object p2, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/f/g$c;

    .line 581
    invoke-interface {v0, p1}, Lcom/baidu/mobads/f/g$c;->a(Z)V

    .line 582
    iget-object v1, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 585
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method static synthetic a(Lcom/baidu/mobads/f/g;Z)Z
    .locals 0

    .line 44
    iput-boolean p1, p0, Lcom/baidu/mobads/f/g;->n:Z

    return p1
.end method

.method public static b(Landroid/content/Context;)D
    .locals 8

    .line 846
    :try_start_0
    invoke-static {p0}, Lcom/baidu/mobads/f/g;->c(Landroid/content/Context;)V

    .line 847
    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v0

    .line 848
    invoke-static {}, Lcom/baidu/mobads/f/g;->d()Ljava/lang/String;

    move-result-object v2

    .line 849
    invoke-static {p0, v2}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v3

    const-string v5, "8.8085"

    .line 851
    invoke-static {v5}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpl-double v7, v5, v3

    if-lez v7, :cond_1

    .line 852
    new-instance v3, Lcom/baidu/mobads/f/b;

    invoke-direct {v3, v2, p0}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 853
    invoke-virtual {v3}, Lcom/baidu/mobads/f/b;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 854
    invoke-virtual {v3}, Lcom/baidu/mobads/f/b;->delete()Z

    .line 856
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v3

    const-string v4, "bdxadsdk.jar"

    .line 865
    invoke-interface {v3, p0, v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileFromAssetsTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    :cond_1
    invoke-static {}, Lcom/baidu/mobads/f/g;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v2

    .line 869
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private b(Lcom/baidu/mobads/f/b;)V
    .locals 13

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "len="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/b;->length()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XAdApkLoader"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-nez v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 386
    new-instance v5, Lcom/baidu/mobads/f/b;

    iget-object v6, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-direct {v5, v0, v6}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 387
    invoke-virtual {v5}, Lcom/baidu/mobads/f/b;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 388
    invoke-virtual {v5}, Lcom/baidu/mobads/f/b;->delete()Z

    .line 392
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 393
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v7

    .line 394
    invoke-interface {v7, v6, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileInputStream(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 397
    iget-object v6, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v6, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    .line 400
    :goto_0
    invoke-virtual {v5}, Lcom/baidu/mobads/f/b;->b()Ljava/lang/Class;

    move-result-object v8

    .line 402
    new-instance v0, Lcom/baidu/mobads/f/a;

    iget-object v9, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    .line 403
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v10

    sget-object v12, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->DEBUG:Ljava/lang/Boolean;

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/baidu/mobads/f/a;-><init>(Ljava/lang/Class;Landroid/content/Context;DLjava/lang/Boolean;)V

    sput-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    .line 407
    :try_start_1
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/f/a;->a()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    .line 408
    iget-object v5, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "preloaded apk.version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/baidu/mobads/f/g$a; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 410
    iget-object v5, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preload local apk "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " failed, msg:"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/f/g$a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", v="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    iget-wide v6, p1, Lcom/baidu/mobads/f/a;->a:D

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v2

    invoke-interface {v5, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    .line 413
    invoke-virtual {v0}, Lcom/baidu/mobads/f/g$a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/f/g;->a(Ljava/lang/String;)V

    .line 414
    throw v0

    .line 417
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mApkBuilder already initialized, version: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    iget-wide v3, v3, Lcom/baidu/mobads/f/a;->a:D

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    :goto_1
    return-void
.end method

.method private b(Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V
    .locals 1

    .line 696
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 699
    :cond_0
    iput-object p2, p0, Lcom/baidu/mobads/f/g;->g:Landroid/os/Handler;

    .line 700
    sget-object p1, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    if-nez p1, :cond_1

    .line 702
    invoke-virtual {p0}, Lcom/baidu/mobads/f/g;->g()V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    .line 704
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->b(Z)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/f/g;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->k()V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/e;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/e;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/f/g;Z)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    if-nez p1, :cond_0

    .line 444
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 446
    iput-boolean v0, p0, Lcom/baidu/mobads/f/g;->n:Z

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "apk Successfully Loaded"

    goto :goto_0

    :cond_1
    const-string v0, "apk Load Failed"

    .line 448
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/f/g;->a(ZLjava/lang/String;)V

    .line 450
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/f/k;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/f/k;-><init>(Lcom/baidu/mobads/f/g;Z)V

    iget-boolean p1, p0, Lcom/baidu/mobads/f/g;->n:Z

    if-eqz p1, :cond_2

    const-wide/16 v2, 0x0

    goto :goto_2

    :cond_2
    const-wide/16 v2, 0x1388

    :goto_2
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected static c()Ljava/lang/String;
    .locals 2

    .line 299
    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 302
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__builtin__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 2

    .line 247
    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const-string v1, "baidu_ad_sdk"

    .line 248
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/baidu/mobads/f/g;Z)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->b(Z)V

    return-void
.end method

.method private c(Lcom/baidu/mobads/f/b;)Z
    .locals 4

    .line 422
    monitor-enter p0

    .line 423
    :try_start_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/b;)V

    .line 424
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loaded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/f/b;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    .line 425
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 426
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic c(Lcom/baidu/mobads/f/g;)Z
    .locals 0

    .line 44
    iget-boolean p0, p0, Lcom/baidu/mobads/f/g;->n:Z

    return p0
.end method

.method protected static d()Ljava/lang/String;
    .locals 2

    .line 307
    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 310
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__builtinversion__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized d(Landroid/content/Context;)V
    .locals 7

    const-class v0, Lcom/baidu/mobads/f/g;

    monitor-enter v0

    .line 339
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/f/g;->c()Ljava/lang/String;

    move-result-object v1

    .line 340
    invoke-static {p0, v1}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v2

    const-string v4, "8.8085"

    .line 342
    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v6, v4, v2

    if-lez v6, :cond_1

    .line 343
    new-instance v2, Lcom/baidu/mobads/f/b;

    invoke-direct {v2, v1, p0}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 344
    invoke-virtual {v2}, Lcom/baidu/mobads/f/b;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 345
    invoke-virtual {v2}, Lcom/baidu/mobads/f/b;->delete()Z

    .line 347
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v2

    const-string v3, "bdxadsdk.jar"

    .line 356
    invoke-interface {v2, p0, v3, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileFromAssetsTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 359
    :try_start_1
    new-instance v1, Lcom/baidu/mobads/f/g$b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBuiltInApk failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/baidu/mobads/f/g$b;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit v0

    throw p0
.end method

.method static synthetic d(Lcom/baidu/mobads/f/g;)Z
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->p()Z

    move-result p0

    return p0
.end method

.method static synthetic e(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object p0
.end method

.method protected static f()Ljava/lang/String;
    .locals 2

    .line 610
    sget-object v0, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 613
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/f/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__downloaded__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/baidu/mobads/f/g;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->o()V

    return-void
.end method

.method static synthetic g(Lcom/baidu/mobads/f/g;)Landroid/content/SharedPreferences;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->l()Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/f/e;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/baidu/mobads/f/g;->k:Lcom/baidu/mobads/f/e;

    return-object p0
.end method

.method static synthetic i(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/openad/d/a;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;

    return-object p0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/baidu/mobads/f/g;->i:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 4

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 128
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 129
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "__xadsdk__remote__final__"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v0, v1

    .line 130
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "dex"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 135
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :cond_1
    return-void
.end method

.method private l()Landroid/content/SharedPreferences;
    .locals 3

    .line 254
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    const-string v1, "com.baidu.mobads.loader"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private m()Z
    .locals 3

    .line 280
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->l()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "previousProxyVersion"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    invoke-virtual {p0}, Lcom/baidu/mobads/f/g;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    .line 282
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private n()Z
    .locals 3

    const/4 v0, 0x0

    .line 431
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/f/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/utils/m;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/utils/m;->b(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :catch_0
    move-exception v1

    .line 434
    iget-object v2, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    return v0
.end method

.method private declared-synchronized o()V
    .locals 1

    monitor-enter p0

    .line 546
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/d/a;->removeAllListeners()V

    .line 548
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/d/a;->a()V

    :cond_0
    const/4 v0, 0x0

    .line 550
    iput-object v0, p0, Lcom/baidu/mobads/f/g;->j:Lcom/baidu/mobads/openad/d/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 552
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    :goto_0
    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw v0
.end method

.method private p()Z
    .locals 10

    .line 637
    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object v0

    .line 638
    new-instance v1, Lcom/baidu/mobads/f/b;

    iget-object v2, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 639
    invoke-static {v1}, Lcom/baidu/mobads/utils/m;->a(Ljava/io/File;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 643
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->m()Z

    move-result v3

    if-nez v3, :cond_1

    .line 648
    monitor-enter p0
    :try_end_0
    .catch Lcom/baidu/mobads/f/g$a; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v3, "XAdApkLoader"

    .line 649
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadDownloadedOrBuiltInApk len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->length()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, ", path="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 649
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    invoke-direct {p0, v1}, Lcom/baidu/mobads/f/g;->b(Lcom/baidu/mobads/f/b;)V

    .line 653
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->l()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "__badApkVersion__8.8085"

    const/high16 v5, -0x40800000    # -1.0f

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v3

    float-to-double v3, v3

    .line 654
    iget-object v5, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v6, "XAdApkLoader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downloadedApkFile.getApkVersion(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->c()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v8, ", badApkVersion: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->c()D

    move-result-wide v5

    cmpl-double v7, v5, v3

    if-eqz v7, :cond_0

    .line 660
    iget-object v3, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v4, "XAdApkLoader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loaded: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    monitor-exit p0

    return v0

    .line 658
    :cond_0
    new-instance v3, Lcom/baidu/mobads/f/g$a;

    const-string v4, "downloaded file marked bad, drop it and use built-in"

    invoke-direct {v3, v4}, Lcom/baidu/mobads/f/g$a;-><init>(Ljava/lang/String;)V

    throw v3

    :catchall_0
    move-exception v3

    .line 664
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3

    .line 645
    :cond_1
    new-instance v3, Lcom/baidu/mobads/f/g$a;

    const-string v4, "XAdApkLoader upgraded, drop stale downloaded file, use built-in instead"

    invoke-direct {v3, v4}, Lcom/baidu/mobads/f/g$a;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Lcom/baidu/mobads/f/g$a; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v3

    .line 666
    iget-object v4, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "XAdApkLoader"

    aput-object v6, v5, v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "load downloaded apk failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/baidu/mobads/f/g$a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", fallback to built-in"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-interface {v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 667
    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 668
    invoke-virtual {v1}, Lcom/baidu/mobads/f/b;->delete()Z

    .line 670
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/f/g;->i()V

    :cond_3
    return v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8085"

    return-object v0
.end method

.method public a(Lcom/baidu/mobads/f/g$c;)V
    .locals 1

    .line 782
    sget-object v0, Lcom/baidu/mobads/f/g;->f:Landroid/os/Handler;

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V
    .locals 2

    .line 714
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/mobads/f/n;

    invoke-direct {v1, p0, p1, p2}, Lcom/baidu/mobads/f/n;-><init>(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/f/g$c;Landroid/os/Handler;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 767
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .line 264
    sget-object p1, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    if-eqz p1, :cond_1

    .line 265
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->l()Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 266
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    iget-wide v0, v0, Lcom/baidu/mobads/f/a;->a:D

    double-to-float v0, v0

    const-string v1, "__badApkVersion__8.8085"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 271
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 272
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 274
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    :goto_0
    return-void
.end method

.method protected b()V
    .locals 2

    .line 258
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/baidu/mobads/f/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method protected e()V
    .locals 4

    .line 315
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->d(Landroid/content/Context;)V

    .line 316
    invoke-static {}, Lcom/baidu/mobads/f/g;->c()Ljava/lang/String;

    move-result-object v0

    .line 317
    new-instance v1, Lcom/baidu/mobads/f/b;

    iget-object v2, p0, Lcom/baidu/mobads/f/g;->l:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/f/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 318
    invoke-static {v1}, Lcom/baidu/mobads/utils/m;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 319
    invoke-direct {p0, v1}, Lcom/baidu/mobads/f/g;->c(Lcom/baidu/mobads/f/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 320
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/g;->b(Z)V

    :cond_0
    return-void

    .line 323
    :cond_1
    new-instance v1, Lcom/baidu/mobads/f/g$b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBuiltInApk failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/baidu/mobads/f/g$b;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected g()V
    .locals 6

    .line 622
    invoke-direct {p0}, Lcom/baidu/mobads/f/g;->p()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 624
    invoke-direct {p0, v1}, Lcom/baidu/mobads/f/g;->b(Z)V

    goto :goto_0

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkLoader"

    const-string v3, "no downloaded file yet, use built-in apk file"

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/f/g;->e()V
    :try_end_0
    .catch Lcom/baidu/mobads/f/g$b; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 630
    iget-object v3, p0, Lcom/baidu/mobads/f/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadBuiltInApk failed: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/f/g$b;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-interface {v3, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 631
    new-instance v1, Lcom/baidu/mobads/f/g$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load built-in apk failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/f/g$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/baidu/mobads/f/g$a;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public h()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 791
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/a;)Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 1

    .line 811
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    if-eqz v0, :cond_0

    .line 812
    sget-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/f/a;->b()V

    const/4 v0, 0x0

    .line 813
    sput-object v0, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    :cond_0
    return-void
.end method
