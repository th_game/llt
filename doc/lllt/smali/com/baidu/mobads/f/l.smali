.class Lcom/baidu/mobads/f/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/f/k;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/f/k;)V
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const-string v0, ""

    .line 459
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    .line 460
    iget-object v2, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-boolean v2, v2, Lcom/baidu/mobads/f/k;->a:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/baidu/mobads/f/g;->b:Lcom/baidu/mobads/f/a;

    iget-wide v2, v2, Lcom/baidu/mobads/f/a;->a:D

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    .line 461
    :goto_0
    iget-object v4, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-boolean v4, v4, Lcom/baidu/mobads/f/k;->a:Z

    .line 466
    new-instance v4, Lcom/baidu/mobads/f/m;

    invoke-direct {v4, p0, v2, v3}, Lcom/baidu/mobads/f/m;-><init>(Lcom/baidu/mobads/f/l;D)V

    .line 517
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v6, "v"

    .line 518
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "os"

    const-string v3, "android"

    .line 519
    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "tp"

    .line 520
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    .line 521
    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v3

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/baidu/mobads/utils/f;->getTextEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 520
    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "bdr"

    .line 523
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v3

    sget-object v6, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    .line 524
    invoke-virtual {v3, v6}, Lcom/baidu/mobads/utils/f;->getTextEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 522
    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    invoke-static {}, Lcom/baidu/mobads/f/g;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->addParameters(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    .line 527
    new-instance v2, Lcom/baidu/mobads/openad/d/b;

    invoke-direct {v2, v1, v0}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 528
    iput v0, v2, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 529
    iget-object v0, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    new-instance v1, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {v1}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    invoke-static {v0, v1}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g;Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/openad/d/a;

    .line 530
    iget-object v0, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->i(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/openad/d/a;

    move-result-object v0

    const-string v1, "URLLoader.Load.Complete"

    .line 531
    invoke-virtual {v0, v1, v4}, Lcom/baidu/mobads/openad/d/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 532
    iget-object v0, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->i(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/openad/d/a;

    move-result-object v0

    const-string v1, "URLLoader.Load.Error"

    invoke-virtual {v0, v1, v4}, Lcom/baidu/mobads/openad/d/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 533
    iget-object v0, p0, Lcom/baidu/mobads/f/l;->a:Lcom/baidu/mobads/f/k;

    iget-object v0, v0, Lcom/baidu/mobads/f/k;->b:Lcom/baidu/mobads/f/g;

    invoke-static {v0}, Lcom/baidu/mobads/f/g;->i(Lcom/baidu/mobads/f/g;)Lcom/baidu/mobads/openad/d/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
