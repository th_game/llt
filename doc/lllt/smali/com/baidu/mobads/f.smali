.class Lcom/baidu/mobads/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/e;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/e;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iput-object p2, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 48
    iget-object v0, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdLoaded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->a(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/production/d/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/d/a;->start()V

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStarted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->b(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/BaiduHybridAdViewListener;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/BaiduHybridAdViewListener;->onAdShow(ILjava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AdError"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AdUserClick"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->b(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/BaiduHybridAdViewListener;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/BaiduHybridAdViewListener;->onAdClick(ILjava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdUserClose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
