.class public Lcom/baidu/mobads/i/a;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/i/a$a;
    }
.end annotation


# instance fields
.field public a:Lcom/baidu/mobads/i/e;

.field public b:Lcom/baidu/mobads/i/d;

.field public c:Landroid/content/Context;

.field public d:Lcom/baidu/mobads/i/i;

.field public e:Landroid/view/View;

.field f:Lcom/baidu/mobads/i/h;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/baidu/mobads/i/a$a;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Lcom/baidu/mobads/utils/o;

.field private p:I

.field private q:Z

.field private r:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 59
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    .line 39
    iput-object v0, p0, Lcom/baidu/mobads/i/a;->h:Ljava/lang/String;

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->k:Z

    .line 47
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->l:Z

    .line 49
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->m:Z

    const/4 v0, 0x1

    .line 56
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->q:Z

    .line 384
    new-instance v0, Lcom/baidu/mobads/i/c;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/i/c;-><init>(Lcom/baidu/mobads/i/a;)V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->f:Lcom/baidu/mobads/i/h;

    .line 60
    iput-object p1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    .line 61
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->i()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/i/a;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/baidu/mobads/i/a;->r:Landroid/view/Surface;

    return-object p1
.end method

.method private a(FF)V
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/i/e;->a(FF)V

    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 2

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 242
    :pswitch_0
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->n()V

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->m()V

    goto :goto_0

    :pswitch_2
    const/4 p1, 0x1

    .line 245
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->q:Z

    .line 246
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->n()V

    .line 247
    iget-object p1, p0, Lcom/baidu/mobads/i/a;->b:Lcom/baidu/mobads/i/d;

    if-eqz p1, :cond_2

    .line 248
    invoke-interface {p1}, Lcom/baidu/mobads/i/d;->c()V

    goto :goto_0

    .line 222
    :pswitch_3
    iget p1, p0, Lcom/baidu/mobads/i/a;->p:I

    if-lez p1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/i/e;->a(I)V

    .line 226
    :cond_0
    iget-boolean p1, p0, Lcom/baidu/mobads/i/a;->g:Z

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/i/a;->a(Z)V

    .line 228
    iget-object p1, p0, Lcom/baidu/mobads/i/a;->d:Lcom/baidu/mobads/i/i;

    if-eqz p1, :cond_1

    .line 229
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->d()I

    move-result v0

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v1}, Lcom/baidu/mobads/i/e;->e()I

    move-result v1

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/i/i;->a(II)V

    .line 232
    :cond_1
    iget-boolean p1, p0, Lcom/baidu/mobads/i/a;->j:Z

    if-eqz p1, :cond_2

    .line 233
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->o()V

    goto :goto_0

    .line 217
    :pswitch_4
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->q:Z

    .line 218
    iput v0, p0, Lcom/baidu/mobads/i/a;->p:I

    .line 219
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->h()V

    goto :goto_0

    .line 209
    :pswitch_5
    iput v0, p0, Lcom/baidu/mobads/i/a;->p:I

    .line 210
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->q:Z

    .line 211
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->n()V

    .line 212
    iget-object p1, p0, Lcom/baidu/mobads/i/a;->b:Lcom/baidu/mobads/i/d;

    if-eqz p1, :cond_2

    .line 213
    invoke-interface {p1}, Lcom/baidu/mobads/i/d;->a()V

    :cond_2
    :goto_0
    :pswitch_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/baidu/mobads/i/a;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->r()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/i/a;I)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/a;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/i/a;Z)Z
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/baidu/mobads/i/a;)Lcom/baidu/mobads/utils/o;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/baidu/mobads/i/a;->o:Lcom/baidu/mobads/utils/o;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/i/a;Z)Z
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->j:Z

    return p1
.end method

.method static synthetic c(Lcom/baidu/mobads/i/a;Z)Z
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->l:Z

    return p1
.end method

.method private h()V
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->b:Lcom/baidu/mobads/i/d;

    if-eqz v0, :cond_0

    .line 260
    invoke-interface {v0}, Lcom/baidu/mobads/i/d;->b()V

    .line 263
    :cond_0
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->n()V

    .line 265
    invoke-virtual {p0}, Lcom/baidu/mobads/i/a;->c()V

    return-void
.end method

.method private i()V
    .locals 1

    .line 269
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->p()V

    .line 270
    new-instance v0, Lcom/baidu/mobads/utils/o;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/o;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->o:Lcom/baidu/mobads/utils/o;

    return-void
.end method

.method private j()V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 276
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->n:Z

    .line 277
    new-instance v0, Lcom/baidu/mobads/i/e;

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/i/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    .line 278
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    new-instance v1, Lcom/baidu/mobads/i/b;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/i/b;-><init>(Lcom/baidu/mobads/i/a;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/e;->a(Lcom/baidu/mobads/i/j;)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .line 289
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 290
    new-instance v0, Lcom/baidu/mobads/i/g;

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/i/a;->f:Lcom/baidu/mobads/i/h;

    invoke-direct {v0, v1, v2}, Lcom/baidu/mobads/i/g;-><init>(Landroid/content/Context;Lcom/baidu/mobads/i/h;)V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->d:Lcom/baidu/mobads/i/i;

    goto :goto_0

    .line 292
    :cond_0
    new-instance v0, Lcom/baidu/mobads/i/f;

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/i/a;->f:Lcom/baidu/mobads/i/h;

    invoke-direct {v0, v1, v2}, Lcom/baidu/mobads/i/f;-><init>(Landroid/content/Context;Lcom/baidu/mobads/i/h;)V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->d:Lcom/baidu/mobads/i/i;

    :goto_0
    return-void
.end method

.method private l()V
    .locals 2

    .line 298
    invoke-virtual {p0}, Lcom/baidu/mobads/i/a;->removeAllViews()V

    .line 299
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 302
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const-string v1, "#000000"

    .line 303
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/i/a;->setBackgroundColor(I)V

    .line 304
    iget-object v1, p0, Lcom/baidu/mobads/i/a;->d:Lcom/baidu/mobads/i/i;

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/i/a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private m()V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/i/a;->e:Landroid/view/View;

    .line 311
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 314
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 315
    iget-object v1, p0, Lcom/baidu/mobads/i/a;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->e:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/i/a;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 318
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private n()V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 325
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 331
    iget-object v1, p0, Lcom/baidu/mobads/i/a;->r:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/e;->a(Landroid/view/Surface;)V

    .line 332
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->a()V

    :cond_0
    return-void
.end method

.method private p()V
    .locals 3

    .line 337
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    .line 338
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    .line 339
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 340
    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;

    if-nez v1, :cond_0

    .line 341
    new-instance v1, Lcom/baidu/mobads/i/a$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/baidu/mobads/i/a$a;-><init>(Lcom/baidu/mobads/i/a;Lcom/baidu/mobads/i/b;)V

    iput-object v1, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;

    .line 342
    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private q()V
    .locals 3

    .line 347
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    .line 349
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 350
    iput-object v2, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    iput-object v2, p0, Lcom/baidu/mobads/i/a;->i:Lcom/baidu/mobads/i/a$a;

    :cond_0
    :goto_0
    return-void
.end method

.method private r()V
    .locals 3

    .line 359
    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/baidu/mobads/i/a;->o:Lcom/baidu/mobads/utils/o;

    iget-object v1, p0, Lcom/baidu/mobads/i/a;->c:Landroid/content/Context;

    .line 360
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/utils/o;->isForeground(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->l:Z

    if-nez v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->q:Z

    if-eqz v0, :cond_2

    .line 363
    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->n:Z

    if-nez v0, :cond_1

    .line 364
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->o()V

    goto :goto_0

    .line 365
    :cond_1
    iget-boolean v0, p0, Lcom/baidu/mobads/i/a;->m:Z

    if-nez v0, :cond_2

    .line 366
    invoke-virtual {p0}, Lcom/baidu/mobads/i/a;->c()V

    const/4 v0, 0x1

    .line 367
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->m:Z

    .line 368
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->j()V

    .line 369
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/i/a;->b(Ljava/lang/String;)V

    .line 370
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->o()V

    :cond_2
    :goto_0
    return-void
.end method

.method private s()V
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-nez v0, :cond_0

    return-void

    .line 379
    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->g()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/i/a;->p:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    .line 79
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->j:Z

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->b()V

    .line 82
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->s()V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/i/d;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/baidu/mobads/i/a;->b:Lcom/baidu/mobads/i/d;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 101
    iput v0, p0, Lcom/baidu/mobads/i/a;->p:I

    .line 102
    iput-object p1, p0, Lcom/baidu/mobads/i/a;->h:Ljava/lang/String;

    const/4 p1, 0x1

    .line 103
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->j:Z

    .line 104
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->q:Z

    .line 105
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->r()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 179
    iput-boolean p1, p0, Lcom/baidu/mobads/i/a;->g:Z

    .line 180
    iget-boolean p1, p0, Lcom/baidu/mobads/i/a;->g:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 181
    invoke-direct {p0, p1, p1}, Lcom/baidu/mobads/i/a;->a(FF)V

    goto :goto_0

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    .line 183
    invoke-direct {p0, p1, p1}, Lcom/baidu/mobads/i/a;->a(FF)V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    .line 89
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->j:Z

    .line 90
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->r()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 136
    iput-object p1, p0, Lcom/baidu/mobads/i/a;->h:Ljava/lang/String;

    .line 137
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->h()V

    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_1

    .line 142
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/i/e;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 112
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->s()V

    const/4 v0, 0x1

    .line 113
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->n:Z

    const/4 v0, 0x0

    .line 114
    iput-boolean v0, p0, Lcom/baidu/mobads/i/a;->m:Z

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/e;->a(Lcom/baidu/mobads/i/j;)V

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->c()V

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->j()V

    .line 118
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->i()V

    .line 119
    iput-object v1, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->f()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->g()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public f()I
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/i/a;->a:Lcom/baidu/mobads/i/e;

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {v0}, Lcom/baidu/mobads/i/e;->h()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    .line 195
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->j()V

    .line 196
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->k()V

    .line 197
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->l()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 189
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 190
    invoke-direct {p0}, Lcom/baidu/mobads/i/a;->q()V

    .line 191
    invoke-virtual {p0}, Lcom/baidu/mobads/i/a;->c()V

    return-void
.end method
