.class public Lcom/baidu/mobads/i/f;
.super Landroid/view/SurfaceView;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/baidu/mobads/i/i;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/baidu/mobads/i/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/i/h;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 24
    iput p1, p0, Lcom/baidu/mobads/i/f;->a:I

    .line 32
    iput-object p2, p0, Lcom/baidu/mobads/i/f;->d:Lcom/baidu/mobads/i/h;

    .line 33
    invoke-virtual {p0}, Lcom/baidu/mobads/i/f;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    invoke-interface {p1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method

.method private a()V
    .locals 0

    .line 103
    invoke-virtual {p0}, Lcom/baidu/mobads/i/f;->requestLayout()V

    .line 104
    invoke-virtual {p0}, Lcom/baidu/mobads/i/f;->invalidate()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .line 87
    iput p1, p0, Lcom/baidu/mobads/i/f;->b:I

    .line 88
    iput p2, p0, Lcom/baidu/mobads/i/f;->c:I

    .line 89
    iget p1, p0, Lcom/baidu/mobads/i/f;->b:I

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/baidu/mobads/i/f;->c:I

    if-eqz p1, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/baidu/mobads/i/f;->a()V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 38
    iget v0, p0, Lcom/baidu/mobads/i/f;->b:I

    invoke-static {v0, p1}, Lcom/baidu/mobads/i/f;->getDefaultSize(II)I

    move-result v0

    .line 39
    iget v1, p0, Lcom/baidu/mobads/i/f;->c:I

    invoke-static {v1, p2}, Lcom/baidu/mobads/i/f;->getDefaultSize(II)I

    move-result v1

    .line 40
    iget v2, p0, Lcom/baidu/mobads/i/f;->b:I

    if-lez v2, :cond_3

    iget v3, p0, Lcom/baidu/mobads/i/f;->c:I

    if-lez v3, :cond_3

    .line 41
    iget v4, p0, Lcom/baidu/mobads/i/f;->a:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    mul-int v2, v2, v1

    .line 77
    div-int v0, v2, v3

    goto :goto_0

    :pswitch_1
    mul-int/lit8 p1, v0, 0x3

    mul-int/lit8 p2, v1, 0x4

    if-ge p1, p2, :cond_0

    .line 71
    div-int/lit8 v1, p1, 0x4

    goto :goto_0

    :cond_0
    if-le p1, p2, :cond_3

    .line 73
    div-int/lit8 v0, p2, 0x3

    goto :goto_0

    :pswitch_2
    mul-int/lit8 p1, v0, 0x9

    mul-int/lit8 p2, v1, 0x10

    if-ge p1, p2, :cond_1

    .line 64
    div-int/lit8 v1, p1, 0x10

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_3

    .line 66
    div-int/lit8 v0, p2, 0x9

    goto :goto_0

    :pswitch_3
    move v0, v2

    move v1, v3

    goto :goto_0

    :pswitch_4
    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-le p1, p2, :cond_2

    mul-int v3, v3, v0

    .line 51
    div-int v1, v3, v2

    goto :goto_0

    :cond_2
    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-ge p1, p2, :cond_3

    mul-int v2, v2, v1

    .line 53
    div-int v0, v2, v3

    goto :goto_0

    :pswitch_5
    mul-int p2, p2, v2

    mul-int p1, p1, v3

    if-le p2, p1, :cond_3

    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-le p1, p2, :cond_3

    mul-int v3, v3, v0

    .line 45
    div-int v1, v3, v2

    :cond_3
    :goto_0
    :pswitch_6
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    .line 81
    iget v2, p0, Lcom/baidu/mobads/i/f;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, p2

    const/4 p2, 0x1

    iget v2, p0, Lcom/baidu/mobads/i/f;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, p2

    const-string p2, "onMeasure.  measure size(%sx%s)"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "BaseSurfaceView"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/i/f;->setMeasuredDimension(II)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/baidu/mobads/i/f;->d:Lcom/baidu/mobads/i/h;

    if-eqz v0, :cond_0

    .line 110
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/i/h;->a(Landroid/view/Surface;)V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    .line 121
    iget-object p1, p0, Lcom/baidu/mobads/i/f;->d:Lcom/baidu/mobads/i/h;

    if-eqz p1, :cond_0

    .line 122
    invoke-interface {p1}, Lcom/baidu/mobads/i/h;->a()V

    :cond_0
    return-void
.end method
