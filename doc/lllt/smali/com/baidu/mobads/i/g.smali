.class public Lcom/baidu/mobads/i/g;
.super Landroid/view/TextureView;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/baidu/mobads/i/i;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/baidu/mobads/i/h;

.field private e:Landroid/graphics/SurfaceTexture;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/i/h;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 28
    iput p1, p0, Lcom/baidu/mobads/i/g;->a:I

    .line 36
    iput-object p2, p0, Lcom/baidu/mobads/i/g;->d:Lcom/baidu/mobads/i/h;

    .line 37
    invoke-virtual {p0, p0}, Lcom/baidu/mobads/i/g;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method private a()V
    .locals 0

    .line 111
    invoke-virtual {p0}, Lcom/baidu/mobads/i/g;->requestLayout()V

    .line 112
    invoke-virtual {p0}, Lcom/baidu/mobads/i/g;->invalidate()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .line 95
    iput p1, p0, Lcom/baidu/mobads/i/g;->b:I

    .line 96
    iput p2, p0, Lcom/baidu/mobads/i/g;->c:I

    .line 97
    iget p1, p0, Lcom/baidu/mobads/i/g;->b:I

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/baidu/mobads/i/g;->c:I

    if-eqz p1, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/baidu/mobads/i/g;->a()V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 42
    iget v0, p0, Lcom/baidu/mobads/i/g;->b:I

    invoke-static {v0, p1}, Lcom/baidu/mobads/i/g;->getDefaultSize(II)I

    move-result v0

    .line 43
    iget v1, p0, Lcom/baidu/mobads/i/g;->c:I

    invoke-static {v1, p2}, Lcom/baidu/mobads/i/g;->getDefaultSize(II)I

    move-result v1

    .line 44
    iget v2, p0, Lcom/baidu/mobads/i/g;->b:I

    if-lez v2, :cond_3

    iget v3, p0, Lcom/baidu/mobads/i/g;->c:I

    if-lez v3, :cond_3

    .line 45
    iget v4, p0, Lcom/baidu/mobads/i/g;->a:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    mul-int v2, v2, v1

    .line 81
    div-int v0, v2, v3

    goto :goto_0

    :pswitch_1
    mul-int/lit8 p1, v0, 0x3

    mul-int/lit8 p2, v1, 0x4

    if-ge p1, p2, :cond_0

    .line 75
    div-int/lit8 v1, p1, 0x4

    goto :goto_0

    :cond_0
    if-le p1, p2, :cond_3

    .line 77
    div-int/lit8 v0, p2, 0x3

    goto :goto_0

    :pswitch_2
    mul-int/lit8 p1, v0, 0x9

    mul-int/lit8 p2, v1, 0x10

    if-ge p1, p2, :cond_1

    .line 68
    div-int/lit8 v1, p1, 0x10

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_3

    .line 70
    div-int/lit8 v0, p2, 0x9

    goto :goto_0

    :pswitch_3
    move v0, v2

    move v1, v3

    goto :goto_0

    :pswitch_4
    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-le p1, p2, :cond_2

    mul-int v3, v3, v0

    .line 55
    div-int v1, v3, v2

    goto :goto_0

    :cond_2
    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-ge p1, p2, :cond_3

    mul-int v2, v2, v1

    .line 57
    div-int v0, v2, v3

    goto :goto_0

    :pswitch_5
    mul-int p2, p2, v2

    mul-int p1, p1, v3

    if-le p2, p1, :cond_3

    mul-int p1, v2, v1

    mul-int p2, v0, v3

    if-le p1, p2, :cond_3

    mul-int v3, v3, v0

    .line 49
    div-int v1, v3, v2

    :cond_3
    :goto_0
    :pswitch_6
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    .line 88
    iget v2, p0, Lcom/baidu/mobads/i/g;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, p2

    const/4 p2, 0x1

    iget v2, p0, Lcom/baidu/mobads/i/g;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, p2

    const-string p2, "onMeasure.  measure size(%sx%s)"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "BaseTextureView"

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/i/g;->setMeasuredDimension(II)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .line 117
    iget-object p2, p0, Lcom/baidu/mobads/i/g;->d:Lcom/baidu/mobads/i/h;

    if-eqz p2, :cond_0

    .line 118
    new-instance p3, Landroid/view/Surface;

    invoke-direct {p3, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-interface {p2, p3}, Lcom/baidu/mobads/i/h;->a(Landroid/view/Surface;)V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/i/g;->d:Lcom/baidu/mobads/i/h;

    if-eqz v0, :cond_0

    .line 130
    invoke-interface {v0}, Lcom/baidu/mobads/i/h;->a()V

    .line 132
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/i/g;->e:Landroid/graphics/SurfaceTexture;

    const/4 p1, 0x0

    return p1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method
