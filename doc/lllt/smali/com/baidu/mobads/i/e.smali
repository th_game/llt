.class public Lcom/baidu/mobads/i/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/i/e$a;
    }
.end annotation


# instance fields
.field public a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private b:Landroid/media/MediaPlayer;

.field private c:Landroid/view/Surface;

.field private d:Lcom/baidu/mobads/i/j;

.field private e:Lcom/baidu/mobads/i/e$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobads/i/e;->k()V

    return-void
.end method

.method private b(I)V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->d:Lcom/baidu/mobads/i/j;

    if-eqz v0, :cond_0

    .line 279
    invoke-interface {v0, p1}, Lcom/baidu/mobads/i/j;->a(I)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .line 284
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    .line 285
    sget-object v0, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    .line 286
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 287
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 288
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 289
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 290
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 291
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    return-void
.end method

.method private l()V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 296
    sget-object v0, Lcom/baidu/mobads/i/e$a;->c:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BaseMediaPlayer"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 73
    sget-object v0, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    :cond_1
    return-void
.end method

.method public a(FF)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_0

    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 181
    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    const-string v2, "BaseMediaPlayer"

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "seekto\u4e0d\u5408\u6cd5\uff0cmCurState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 147
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 149
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seekTo\u5f02\u5e38"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .line 48
    iput-object p1, p0, Lcom/baidu/mobads/i/e;->c:Landroid/view/Surface;

    .line 49
    iget-object p1, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz p1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->c:Landroid/view/Surface;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/i/j;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/baidu/mobads/i/e;->d:Lcom/baidu/mobads/i/j;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 57
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 58
    sget-object p1, Lcom/baidu/mobads/i/e$a;->b:Lcom/baidu/mobads/i/e$a;

    iput-object p1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    .line 59
    invoke-direct {p0}, Lcom/baidu/mobads/i/e;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVideoPath\u5f02\u5e38"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "BaseMediaPlayer"

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public b()V
    .locals 3

    .line 82
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BaseMediaPlayer"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 87
    sget-object v0, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 98
    sget-object v0, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    :cond_1
    return-void
.end method

.method public d()I
    .locals 3

    .line 103
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    return v0

    :cond_1
    return v2
.end method

.method public e()I
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    return v0

    :cond_1
    return v2
.end method

.method public f()Z
    .locals 4

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->b:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 137
    iget-object v1, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPlaying\u5f02\u5e38"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "BaseMediaPlayer"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public g()I
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->b:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 169
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public h()I
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    if-ne v0, v1, :cond_1

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 196
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public i()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 205
    sget-object v0, Lcom/baidu/mobads/i/e$a;->i:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    .line 206
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 207
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 208
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    iput-object v0, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/i/e;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .line 229
    iget-object p1, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCompletion"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaseMediaPlayer"

    invoke-interface {p1, v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    sget-object p1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    iput-object p1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    const/16 p1, 0x100

    .line 231
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 0

    .line 236
    iget-object p1, p0, Lcom/baidu/mobads/i/e;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "onError"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "BaseMediaPlayer"

    invoke-interface {p1, p3, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    sget-object p1, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    iput-object p1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    const/16 p1, 0x101

    .line 238
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 0

    const/4 p1, 0x3

    if-eq p2, p1, :cond_2

    const/16 p1, 0x2bd

    if-eq p2, p1, :cond_1

    const/16 p1, 0x2be

    if-eq p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x106

    .line 255
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    goto :goto_0

    :cond_1
    const/16 p1, 0x105

    .line 251
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    goto :goto_0

    :cond_2
    const/16 p1, 0x104

    .line 247
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 0

    .line 223
    sget-object p1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    iput-object p1, p0, Lcom/baidu/mobads/i/e;->e:Lcom/baidu/mobads/i/e$a;

    const/16 p1, 0x102

    .line 224
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1

    const-string p1, "AdVideoView"

    const-string v0, "onSeekComplete"

    .line 266
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p1, 0x103

    .line 267
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/e;->b(I)V

    return-void
.end method
