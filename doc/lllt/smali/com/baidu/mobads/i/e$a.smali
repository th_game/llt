.class public final enum Lcom/baidu/mobads/i/e$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/i/e$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobads/i/e$a;

.field public static final enum b:Lcom/baidu/mobads/i/e$a;

.field public static final enum c:Lcom/baidu/mobads/i/e$a;

.field public static final enum d:Lcom/baidu/mobads/i/e$a;

.field public static final enum e:Lcom/baidu/mobads/i/e$a;

.field public static final enum f:Lcom/baidu/mobads/i/e$a;

.field public static final enum g:Lcom/baidu/mobads/i/e$a;

.field public static final enum h:Lcom/baidu/mobads/i/e$a;

.field public static final enum i:Lcom/baidu/mobads/i/e$a;

.field public static final enum j:Lcom/baidu/mobads/i/e$a;

.field private static final synthetic k:[Lcom/baidu/mobads/i/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 300
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v1, 0x0

    const-string v2, "IDLE"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    .line 301
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v2, 0x1

    const-string v3, "INITIALIZED"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->b:Lcom/baidu/mobads/i/e$a;

    .line 302
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v3, 0x2

    const-string v4, "PREPARING"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->c:Lcom/baidu/mobads/i/e$a;

    .line 303
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v4, 0x3

    const-string v5, "PREPARED"

    invoke-direct {v0, v5, v4}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    .line 304
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v5, 0x4

    const-string v6, "STARTED"

    invoke-direct {v0, v6, v5}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    .line 305
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v6, 0x5

    const-string v7, "PAUSED"

    invoke-direct {v0, v7, v6}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    .line 306
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v7, 0x6

    const-string v8, "STOPPED"

    invoke-direct {v0, v8, v7}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    .line 307
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/4 v8, 0x7

    const-string v9, "PLAYBACKCOMPLETED"

    invoke-direct {v0, v9, v8}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    .line 308
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/16 v9, 0x8

    const-string v10, "END"

    invoke-direct {v0, v10, v9}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->i:Lcom/baidu/mobads/i/e$a;

    .line 309
    new-instance v0, Lcom/baidu/mobads/i/e$a;

    const/16 v10, 0x9

    const-string v11, "ERROR"

    invoke-direct {v0, v11, v10}, Lcom/baidu/mobads/i/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/baidu/mobads/i/e$a;

    .line 299
    sget-object v11, Lcom/baidu/mobads/i/e$a;->a:Lcom/baidu/mobads/i/e$a;

    aput-object v11, v0, v1

    sget-object v1, Lcom/baidu/mobads/i/e$a;->b:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/i/e$a;->c:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/i/e$a;->d:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/i/e$a;->e:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/i/e$a;->f:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/i/e$a;->g:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/i/e$a;->h:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/i/e$a;->i:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/i/e$a;->j:Lcom/baidu/mobads/i/e$a;

    aput-object v1, v0, v10

    sput-object v0, Lcom/baidu/mobads/i/e$a;->k:[Lcom/baidu/mobads/i/e$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
