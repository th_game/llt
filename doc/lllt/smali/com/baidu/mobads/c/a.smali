.class public Lcom/baidu/mobads/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/c/a$a;
    }
.end annotation


# static fields
.field private static final b:Landroid/os/Handler;

.field private static final c:Landroid/os/HandlerThread;

.field private static final d:Landroid/os/Handler;

.field private static e:Lcom/baidu/mobads/c/a;


# instance fields
.field private a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/baidu/mobads/c/a;->b:Landroid/os/Handler;

    .line 37
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "XAdSimpleImageLoader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/c/a;->c:Landroid/os/HandlerThread;

    .line 66
    sget-object v0, Lcom/baidu/mobads/c/a;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 67
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/baidu/mobads/c/a;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/baidu/mobads/c/a;->d:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    long-to-int v1, v0

    .line 54
    div-int/lit8 v1, v1, 0x20

    .line 55
    new-instance v0, Lcom/baidu/mobads/c/b;

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/c/b;-><init>(Lcom/baidu/mobads/c/a;I)V

    iput-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    return-void
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;Landroid/widget/ImageView;)I
    .locals 3

    .line 170
    invoke-static {p1}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;

    move-result-object p1

    .line 171
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 172
    iget p0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 174
    iget v1, p1, Lcom/baidu/mobads/c/a$a;->a:I

    if-gt v0, v1, :cond_1

    iget v1, p1, Lcom/baidu/mobads/c/a$a;->b:I

    if-le p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v0, v0, v1

    .line 176
    iget v2, p1, Lcom/baidu/mobads/c/a$a;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float p0, p0

    mul-float p0, p0, v1

    .line 177
    iget p1, p1, Lcom/baidu/mobads/c/a$a;->b:I

    int-to-float p1, p1

    div-float/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result p0

    .line 178
    invoke-static {v0, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    :goto_1
    return p0
.end method

.method static synthetic a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;
    .locals 2

    .line 153
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 154
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 155
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 156
    invoke-static {p2}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;

    .line 157
    invoke-static {v0, p2}, Lcom/baidu/mobads/c/a;->a(Landroid/graphics/BitmapFactory$Options;Landroid/widget/ImageView;)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v1, 0x0

    .line 158
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 159
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 161
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-object p1
.end method

.method private static a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;
    .locals 5

    .line 187
    new-instance v0, Lcom/baidu/mobads/c/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/baidu/mobads/c/a$a;-><init>(Lcom/baidu/mobads/c/b;)V

    .line 189
    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 190
    invoke-virtual {p0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 192
    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    if-gtz v3, :cond_0

    .line 194
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_0
    if-gtz v3, :cond_1

    .line 197
    invoke-virtual {p0}, Landroid/widget/ImageView;->getMaxWidth()I

    move-result v3

    :cond_1
    if-gtz v3, :cond_2

    .line 200
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 203
    :cond_2
    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    if-gtz v4, :cond_3

    .line 205
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_3
    if-gtz v4, :cond_4

    .line 208
    invoke-virtual {p0}, Landroid/widget/ImageView;->getMaxHeight()I

    move-result v4

    :cond_4
    if-gtz v4, :cond_5

    .line 211
    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 214
    :cond_5
    iput v3, v0, Lcom/baidu/mobads/c/a$a;->a:I

    .line 215
    iput v4, v0, Lcom/baidu/mobads/c/a$a;->b:I

    return-object v0
.end method

.method public static a()Lcom/baidu/mobads/c/a;
    .locals 2

    .line 42
    sget-object v0, Lcom/baidu/mobads/c/a;->e:Lcom/baidu/mobads/c/a;

    if-nez v0, :cond_1

    .line 43
    const-class v0, Lcom/baidu/mobads/c/a;

    monitor-enter v0

    .line 44
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/c/a;->e:Lcom/baidu/mobads/c/a;

    if-nez v1, :cond_0

    .line 45
    new-instance v1, Lcom/baidu/mobads/c/a;

    invoke-direct {v1}, Lcom/baidu/mobads/c/a;-><init>()V

    sput-object v1, Lcom/baidu/mobads/c/a;->e:Lcom/baidu/mobads/c/a;

    .line 47
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 49
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/c/a;->e:Lcom/baidu/mobads/c/a;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 28
    invoke-static {p0}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 4

    .line 132
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 134
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 135
    invoke-static {v0}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object v0

    .line 136
    invoke-static {p1}, Lcom/baidu/mobads/c/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/baidu/mobads/c/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".temp"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/baidu/mobads/openad/b/d;->createImgHttpDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/interfaces/download/IXAdStaticImgDownloader;

    move-result-object p1

    .line 137
    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/download/IXAdStaticImgDownloader;->addObserver(Ljava/util/Observer;)V

    .line 138
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/download/IXAdStaticImgDownloader;->start()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 140
    invoke-virtual {p1}, Ljava/net/MalformedURLException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method static synthetic b()Landroid/os/Handler;
    .locals 1

    .line 28
    sget-object v0, Lcom/baidu/mobads/c/a;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 227
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    .line 228
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->getStoreagePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 229
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/baidu/mobads/utils/f;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ".temp"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 234
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object p0

    .line 235
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->getStoreagePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 240
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/f;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 2

    .line 77
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_4

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 87
    :cond_1
    invoke-static {p2}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 89
    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 91
    :cond_2
    new-instance v0, Lcom/baidu/mobads/c/c;

    invoke-direct {v0, p0, p2, p1}, Lcom/baidu/mobads/c/c;-><init>(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Ljava/util/Observer;)V

    :cond_3
    :goto_0
    return-void

    .line 78
    :cond_4
    new-instance p1, Ljava/lang/IllegalThreadStateException;

    const-string p2, "please invoke in main thread!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
