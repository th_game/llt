.class public final enum Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/interfaces/IXAdConstants4PDK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VideoState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum COMPLETED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

.field public static final enum IDLE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

.field public static final enum PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

.field public static final enum PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

.field private static final synthetic b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 81
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    const/4 v1, 0x0

    const-string v2, "IDLE"

    invoke-direct {v0, v2, v1, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->IDLE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    .line 85
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    const/4 v2, 0x1

    const-string v3, "PLAYING"

    invoke-direct {v0, v3, v2, v3}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    .line 89
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    const/4 v3, 0x2

    const-string v4, "PAUSED"

    invoke-direct {v0, v4, v3, v4}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    .line 93
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    const/4 v4, 0x3

    const-string v5, "COMPLETED"

    invoke-direct {v0, v5, v4, v5}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->COMPLETED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    .line 77
    sget-object v5, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->IDLE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->COMPLETED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 96
    iput-object p3, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->a:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;
    .locals 5

    .line 106
    invoke-static {}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 107
    iget-object v4, v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;
    .locals 1

    .line 77
    const-class v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;
    .locals 1

    .line 77
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    invoke-virtual {v0}, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->a:Ljava/lang/String;

    return-object v0
.end method
