.class public final enum Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CreativeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum HYBRID:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum TEXT:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field public static final enum VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field private static final synthetic b:[Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 22
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v1, 0x0

    const-string v2, "NONE"

    const-string v3, "none"

    invoke-direct {v0, v2, v1, v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 24
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v2, 0x1

    const-string v3, "TEXT"

    const-string v4, "text"

    invoke-direct {v0, v3, v2, v4}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->TEXT:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 26
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v3, 0x2

    const-string v4, "STATIC_IMAGE"

    const-string v5, "static_image"

    invoke-direct {v0, v4, v3, v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 28
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v4, 0x3

    const-string v5, "GIF"

    const-string v6, "gif"

    invoke-direct {v0, v5, v4, v6}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 30
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v5, 0x4

    const-string v6, "RM"

    const-string v7, "rich_media"

    invoke-direct {v0, v6, v5, v7}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 32
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v6, 0x5

    const-string v7, "HTML"

    const-string v8, "html"

    invoke-direct {v0, v7, v6, v8}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 34
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v7, 0x6

    const-string v8, "HYBRID"

    const-string v9, "hybrid"

    invoke-direct {v0, v8, v7, v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HYBRID:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 36
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/4 v8, 0x7

    const-string v9, "VIDEO"

    const-string v10, "video"

    invoke-direct {v0, v9, v8, v10}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 20
    sget-object v9, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->TEXT:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HYBRID:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->b:[Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->a:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
    .locals 5

    .line 49
    invoke-static {}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->values()[Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 50
    iget-object v4, v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
    .locals 1

    .line 20
    const-class v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
    .locals 1

    .line 20
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->b:[Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    invoke-virtual {v0}, [Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->a:Ljava/lang/String;

    return-object v0
.end method
