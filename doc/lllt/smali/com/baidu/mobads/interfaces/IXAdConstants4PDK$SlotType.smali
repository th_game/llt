.class public final enum Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/interfaces/IXAdConstants4PDK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SlotType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field private static final synthetic b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 223
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v1, 0x0

    const-string v2, "SLOT_TYPE_JSSDK"

    const-string v3, "jssdk"

    invoke-direct {v0, v2, v1, v3}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 227
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v2, 0x1

    const-string v3, "SLOT_TYPE_CPU"

    const-string v4, "cpu"

    invoke-direct {v0, v3, v2, v4}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 228
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v3, 0x2

    const-string v4, "SLOT_TYPE_BANNER"

    const-string v5, "banner"

    invoke-direct {v0, v4, v3, v5}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 229
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v4, 0x3

    const-string v5, "SLOT_TYPE_SPLASH"

    const-string v6, "rsplash"

    invoke-direct {v0, v5, v4, v6}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 230
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v5, 0x4

    const-string v6, "SLOT_TYPE_INTERSTITIAL"

    const-string v7, "int"

    invoke-direct {v0, v6, v5, v7}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 231
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v6, 0x5

    const-string v7, "SLOT_TYPE_FEEDS"

    const-string v8, "feed"

    invoke-direct {v0, v7, v6, v8}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 232
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v7, 0x6

    const-string v8, "SLOT_TYPE_REWARD_VIDEO"

    const-string v9, "rvideo"

    invoke-direct {v0, v8, v7, v9}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 236
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v8, 0x7

    const-string v9, "SLOT_TYPE_PREROLL"

    const-string v10, "preroll"

    invoke-direct {v0, v9, v8, v10}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 240
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v9, 0x8

    const-string v10, "SLOT_TYPE_MIDROLL"

    const-string v11, "midroll"

    invoke-direct {v0, v10, v9, v11}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 244
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v10, 0x9

    const-string v11, "SLOT_TYPE_POSTROLL"

    const-string v12, "postroll"

    invoke-direct {v0, v11, v10, v12}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 248
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v11, 0xa

    const-string v12, "SLOT_TYPE_OVERLAY"

    const-string v13, "overlay"

    invoke-direct {v0, v12, v11, v13}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 252
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v12, 0xb

    const-string v13, "SLOT_TYPE_PAUSE_ROLL"

    const-string v14, "pauseroll"

    invoke-direct {v0, v13, v12, v14}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 218
    sget-object v13, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v12

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 255
    iput-object p3, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 5

    .line 265
    invoke-static {}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 266
    iget-object v4, v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 218
    const-class v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 218
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    return-object v0
.end method
