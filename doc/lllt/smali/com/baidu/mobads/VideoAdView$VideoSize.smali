.class public final enum Lcom/baidu/mobads/VideoAdView$VideoSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/VideoAdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VideoSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/VideoAdView$VideoSize;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SIZE_16x9:Lcom/baidu/mobads/VideoAdView$VideoSize;

.field public static final enum SIZE_4x3:Lcom/baidu/mobads/VideoAdView$VideoSize;

.field private static final synthetic c:[Lcom/baidu/mobads/VideoAdView$VideoSize;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 107
    new-instance v0, Lcom/baidu/mobads/VideoAdView$VideoSize;

    const/4 v1, 0x0

    const-string v2, "SIZE_16x9"

    const/16 v3, 0x140

    const/16 v4, 0xb4

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/baidu/mobads/VideoAdView$VideoSize;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/baidu/mobads/VideoAdView$VideoSize;->SIZE_16x9:Lcom/baidu/mobads/VideoAdView$VideoSize;

    .line 111
    new-instance v0, Lcom/baidu/mobads/VideoAdView$VideoSize;

    const/4 v2, 0x1

    const-string v3, "SIZE_4x3"

    const/16 v4, 0x190

    const/16 v5, 0x12c

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/baidu/mobads/VideoAdView$VideoSize;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/baidu/mobads/VideoAdView$VideoSize;->SIZE_4x3:Lcom/baidu/mobads/VideoAdView$VideoSize;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/baidu/mobads/VideoAdView$VideoSize;

    .line 103
    sget-object v3, Lcom/baidu/mobads/VideoAdView$VideoSize;->SIZE_16x9:Lcom/baidu/mobads/VideoAdView$VideoSize;

    aput-object v3, v0, v1

    sget-object v1, Lcom/baidu/mobads/VideoAdView$VideoSize;->SIZE_4x3:Lcom/baidu/mobads/VideoAdView$VideoSize;

    aput-object v1, v0, v2

    sput-object v0, Lcom/baidu/mobads/VideoAdView$VideoSize;->c:[Lcom/baidu/mobads/VideoAdView$VideoSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118
    iput p3, p0, Lcom/baidu/mobads/VideoAdView$VideoSize;->a:I

    .line 119
    iput p4, p0, Lcom/baidu/mobads/VideoAdView$VideoSize;->b:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/VideoAdView$VideoSize;
    .locals 1

    .line 103
    const-class v0, Lcom/baidu/mobads/VideoAdView$VideoSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/VideoAdView$VideoSize;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/VideoAdView$VideoSize;
    .locals 1

    .line 103
    sget-object v0, Lcom/baidu/mobads/VideoAdView$VideoSize;->c:[Lcom/baidu/mobads/VideoAdView$VideoSize;

    invoke-virtual {v0}, [Lcom/baidu/mobads/VideoAdView$VideoSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/VideoAdView$VideoSize;

    return-object v0
.end method


# virtual methods
.method protected getHeight()I
    .locals 1

    .line 127
    iget v0, p0, Lcom/baidu/mobads/VideoAdView$VideoSize;->b:I

    return v0
.end method

.method protected getWidth()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/baidu/mobads/VideoAdView$VideoSize;->a:I

    return v0
.end method
