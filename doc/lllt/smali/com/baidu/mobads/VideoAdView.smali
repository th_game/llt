.class public Lcom/baidu/mobads/VideoAdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/VideoAdView$VideoSize;,
        Lcom/baidu/mobads/VideoAdView$VideoDuration;
    }
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/production/h/b;

.field private b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private c:Lcom/baidu/mobads/VideoAdViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance p1, Lcom/baidu/mobads/w;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/w;-><init>(Lcom/baidu/mobads/VideoAdView;)V

    iput-object p1, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    const/4 p2, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance p1, Lcom/baidu/mobads/w;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/w;-><init>(Lcom/baidu/mobads/VideoAdView;)V

    iput-object p1, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobads/VideoAdView;->c:Lcom/baidu/mobads/VideoAdViewListener;

    return-object p0
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 180
    invoke-static {p0, p1}, Lcom/baidu/mobads/AdView;->setAppSid(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public requestAd(Lcom/baidu/mobads/VideoAdRequest;)V
    .locals 2

    .line 146
    new-instance p1, Lcom/baidu/mobads/production/h/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/VideoAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "TODO"

    invoke-direct {p1, v0, v1}, Lcom/baidu/mobads/production/h/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    .line 147
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/VideoAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/h/b;->setActivity(Landroid/content/Context;)V

    .line 148
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/production/h/b;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 149
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v1, "AdClickThru"

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 150
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v1, "AdLoaded"

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 151
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v1, "AdStarted"

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 152
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v1, "AdStopped"

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 155
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->b:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v1, "AdError"

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 157
    iget-object p1, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/h/b;->request()V

    return-void
.end method

.method public setListener(Lcom/baidu/mobads/VideoAdViewListener;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/baidu/mobads/VideoAdView;->c:Lcom/baidu/mobads/VideoAdViewListener;

    return-void
.end method

.method public startVideo()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/VideoAdView;->a:Lcom/baidu/mobads/production/h/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/h/b;->start()V

    return-void
.end method
