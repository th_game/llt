.class public Lcom/baidu/mobads/SplashAd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/baidu/mobads/production/g/a;

.field private b:I

.field private volatile c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Lcom/baidu/mobads/SplashAdListener;

.field private f:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 139
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 153
    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 8

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    .line 34
    iput v0, p0, Lcom/baidu/mobads/SplashAd;->b:I

    const-string v0, "init"

    .line 55
    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->c:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/baidu/mobads/r;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/r;-><init>(Lcom/baidu/mobads/SplashAd;)V

    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/SplashAdListener;

    .line 89
    new-instance v0, Lcom/baidu/mobads/s;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/s;-><init>(Lcom/baidu/mobads/SplashAd;)V

    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->f:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 159
    :try_start_0
    iput-object p1, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->l:J

    const-wide/16 v0, 0x0

    .line 161
    sput-wide v0, Lcom/baidu/mobads/constants/a;->m:J

    .line 162
    sput-wide v0, Lcom/baidu/mobads/constants/a;->n:J

    .line 163
    sput-wide v0, Lcom/baidu/mobads/constants/a;->o:J

    .line 164
    sput-wide v0, Lcom/baidu/mobads/constants/a;->p:J

    .line 165
    sput-wide v0, Lcom/baidu/mobads/constants/a;->q:J

    .line 166
    sput-wide v0, Lcom/baidu/mobads/constants/a;->r:J

    .line 169
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->isAnti()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-direct {p0, p2, p1}, Lcom/baidu/mobads/SplashAd;->a(Landroid/view/ViewGroup;Landroid/content/Context;)V

    :cond_0
    if-eqz p3, :cond_1

    .line 173
    iput-object p3, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/SplashAdListener;

    .line 176
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 177
    iget-object p1, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/SplashAdListener;

    const-string p2, "\u8bf7\u60a8\u8f93\u5165\u6b63\u786e\u7684\u5e7f\u544a\u4f4dID"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    return-void

    .line 181
    :cond_2
    new-instance p3, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {p3, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 182
    new-instance v7, Lcom/baidu/mobads/u;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/u;-><init>(Lcom/baidu/mobads/SplashAd;Landroid/content/Context;Lcom/baidu/mobads/component/XAdView;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V

    invoke-virtual {p3, v7}, Lcom/baidu/mobads/component/XAdView;->setListener(Lcom/baidu/mobads/component/XAdView$Listener;)V

    .line 250
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p4, -0x1

    invoke-direct {p1, p4, p4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 252
    invoke-virtual {p3, p1}, Lcom/baidu/mobads/component/XAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 253
    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 256
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    .line 258
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p2

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "splash ad create failed: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/SplashAdListener;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/SplashAd;Lcom/baidu/mobads/production/g/a;)Lcom/baidu/mobads/production/g/a;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/g/a;

    return-object p1
.end method

.method private a(Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 2

    .line 337
    :try_start_0
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 338
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {p2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 339
    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 341
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/g/a;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->f:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobads/SplashAd;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/baidu/mobads/SplashAd;->b:I

    return p0
.end method

.method public static setAppSec(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/f;->setAppId(Ljava/lang/String;)V

    return-void
.end method

.method public static setBitmapDisplayMode(I)V
    .locals 0

    .line 297
    invoke-static {p0}, Lcom/baidu/mobads/production/g/a;->b(I)V

    return-void
.end method

.method public static setMaxVideoCacheCapacityMb(I)V
    .locals 2

    const/16 v0, 0xf

    if-lt p0, v0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 313
    invoke-static {p0}, Lcom/baidu/mobads/utils/j;->a(I)V

    goto :goto_0

    :cond_0
    const/16 p0, 0x1e

    .line 315
    invoke-static {p0}, Lcom/baidu/mobads/utils/j;->a(I)V

    .line 316
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p0

    const-string v0, ""

    const-string v1, "\u5f00\u5c4f\u8bbe\u7f6e\u89c6\u9891\u6700\u5927\u7f13\u5b58\u503c\u6709\u6548\u8303\u56f4\u572815~100M,\u9ed8\u8ba430M"

    .line 317
    invoke-interface {p0, v0, v1, v0}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/g/a;

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/a;->p()V

    :cond_0
    return-void
.end method
