.class public final enum Lcom/baidu/mobads/component/NativeVideoView$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/component/NativeVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/component/NativeVideoView$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum END:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum ERROR:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum INITIALIZED:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum PAUSED:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum PREPARING:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum STARTED:Lcom/baidu/mobads/component/NativeVideoView$State;

.field public static final enum STOPPED:Lcom/baidu/mobads/component/NativeVideoView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 71
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v1, 0x0

    const-string v2, "IDLE"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 72
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v2, 0x1

    const-string v3, "INITIALIZED"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->INITIALIZED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 73
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v3, 0x2

    const-string v4, "PREPARED"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 74
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v4, 0x3

    const-string v5, "PREPARING"

    invoke-direct {v0, v5, v4}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARING:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 75
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v5, 0x4

    const-string v6, "STARTED"

    invoke-direct {v0, v6, v5}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->STARTED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 76
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v6, 0x5

    const-string v7, "STOPPED"

    invoke-direct {v0, v7, v6}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->STOPPED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 77
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v7, 0x6

    const-string v8, "PAUSED"

    invoke-direct {v0, v8, v7}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PAUSED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 78
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v8, 0x7

    const-string v9, "PLAYBACKCOMPLETED"

    invoke-direct {v0, v9, v8}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 79
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/16 v9, 0x8

    const-string v10, "ERROR"

    invoke-direct {v0, v10, v9}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->ERROR:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 80
    new-instance v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    const/16 v10, 0x9

    const-string v11, "END"

    invoke-direct {v0, v11, v10}, Lcom/baidu/mobads/component/NativeVideoView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->END:Lcom/baidu/mobads/component/NativeVideoView$State;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 70
    sget-object v11, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v11, v0, v1

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->INITIALIZED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARING:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->STARTED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->STOPPED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->PAUSED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->ERROR:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->END:Lcom/baidu/mobads/component/NativeVideoView$State;

    aput-object v1, v0, v10

    sput-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->$VALUES:[Lcom/baidu/mobads/component/NativeVideoView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/component/NativeVideoView$State;
    .locals 1

    .line 70
    const-class v0, Lcom/baidu/mobads/component/NativeVideoView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/component/NativeVideoView$State;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/component/NativeVideoView$State;
    .locals 1

    .line 70
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->$VALUES:[Lcom/baidu/mobads/component/NativeVideoView$State;

    invoke-virtual {v0}, [Lcom/baidu/mobads/component/NativeVideoView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/component/NativeVideoView$State;

    return-object v0
.end method
