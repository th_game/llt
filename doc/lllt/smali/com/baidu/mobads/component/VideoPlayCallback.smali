.class public interface abstract Lcom/baidu/mobads/component/VideoPlayCallback;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAttach()V
.end method

.method public abstract onClickAd()V
.end method

.method public abstract onCloseVideo(I)V
.end method

.method public abstract onCompletion(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onDetach()V
.end method

.method public abstract onError(Landroid/media/MediaPlayer;II)V
.end method

.method public abstract onFullScreen(I)V
.end method

.method public abstract onPause(I)V
.end method

.method public abstract onPrepared(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onPreparing()V
.end method

.method public abstract onRelease()V
.end method

.method public abstract onSeekComplete(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method
