.class public Lcom/baidu/mobads/component/XNativeView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;
    }
.end annotation


# static fields
.field private static final COVER_PIC_ID:I = 0x11

.field private static final PAUSE_ID:I = 0x10

.field private static final PLAY_ICON_STRING:Ljava/lang/String; = "iVBORw0KGgoAAAANSUhEUgAAAJAAAACQCAYAAADnRuK4AAAABHNCSVQICAgIfAhkiAAADT5JREFU\neJztnXtsleUdxz897YFKL1AQKEUFKsjqAqgILotIzBYQdERilqAsMTAuIoMNGEtmt8G0cUuj/GFM\nDDFk/lGRqBXZisCMiGBAWjsuQS4ttFx6A6QX2kLpoefsj985tLQ97bm873ne9z3PJznhWM/lC8+3\nz+33e55fAvGHC0gHBgND/H8OBNxdHgO6PAfw+B/tXZ57gFtAE9Do//M64I3R38MSJKgWYDJuIBMY\nBWQghklDTGQGXsRETUADUAvUIWZzJE4zUCIwEsgCRgPDMc8soeIFrgLVQA1wGehQqshAnGAgNzAO\nGI/0NIlq5fRLB9IzlQPnsXnvZFcDJSA9zATEPElq5UTMbaASKEN6J59aOeFjNwOlAzmIcQYp1mI0\nrcBZ4BQyj7IFdjHQEOBRZJiyi+ZI8SFGOoKs7iyN1RvjXsQ441QLUUQFcBT4UbWQYFjVQMOBqcAD\nqoVYhItAKbKasxRWM9BAYDoyz9H05BRQjGxgWgKrGCgBeAh4AkhWrMXqtAGHkZWb8lWbFQw0DHgS\n2QDUhE4d8C1Qr1KESgO5kHnOI4p12BkfMskuRVEMTlXDpQC/QOJUmuipA75C9pJiiopt//uBucje\njsYYUpE5ZD0x3oSMpYFcwDRgBvYNPViZJGSHPhGJtcVkgh0rAyUDc5CdZI25ZCLZCBeQWJupxMJA\nqcCvkF1lTWxIBcYgJmo384vMNtBQxDxpJn+PpifJwINAFXDTrC8x00CZwLPojUGVuJFpQx3QYsYX\nmGWgMcAzdOYUa9SRiJioHkm1NfzDjWYMMAv1qaSaTlzIcHYNg01ktIEykZ5Hm8d6JCBpMbUYOJwZ\naaChyJxHD1vWxYWY6CIGTayNMlBgqa4nzNYnERiLJKtFvcQ3wkDJ6KW63XAjIaVzRLnZGK2BXMgO\ns94ktB/JSApNOVGEPaI10DR0eMLOpCKdQHWkHxCNge5HAqMae5MJXCHCKH6ky+0U4OkI36uxHk8j\nbRo2kRjIhSSD6RWXc0hG2jRsP0QyhD2O5J1onEUqstlYE86bwnXcMCSHWeNMHkE2hEMmHAMlIKcn\ndAK8cwm7jcMx0EPoozfxQCbS1iERqoEGIof+NPHBE0ib90uoBpqOXnXFE8lIm/dLKAYajj6rHo/k\nIG3fJ6EYaGr0WjQ2pd+2789A96KvWIlnHkC2boLSn4EeNU6Lxqb06YG+DDSE+L0ZTNNJNn0cQ+/L\nQLr30QQIGn0IZqB0dJ6PppMJBMk4DWagHHTIQtNJAkG2cnozUAIOi7a/+eabk8+ePbuqsLBwVmZm\n5gDVemzKBHrpVHrrZe5D7u9xDM3Nzf9ITU0dCtDa2tpQUFDw+cqVK7/r6HBMyYpYsZNu6a+99UCO\n6n0AAuYBSElJyVi+fPmi6urq19auXeu4v6vJ9AiydjdQoHCJ4xk5cuSYt99++48nT55cPmfOnH63\n7DWAeOOug6PdDWTnwiURkZOT81hRUdHGffv2vZCdnX2Paj0WJwk5lHiH7gaKy6W7y+VKmjlz5qwf\nfvghr6CgYOagQYP02f7g3DXsd/2HSkTqbcUtycnJqQsXLnzp0qVLf9u4ceNPVeuxKHfVZOtqoJFY\nv1hbTBg6dOioDRs2rD537tzvFyxYkKVaj8UIVIUE7jbQ6NhrsTbZ2dkPb9269a+HDx9+acqUKamq\n9ViIO79Urt5+qOkkISHBNX369JklJSV527dvn5WRkRFXi4wg9DCQmxCyz+IZt9t9z/PPP//C+fPn\n/75p06Z4T7IbgX85HzBQJpEfc44r0tPT712zZs2yqqqq9cuWLRujWo8iXPjLVARME9err0gYPXr0\n+M2bN//52LFji5566qkM1XoUMAo6DRSP/wBGkDB58uSf7d279/Xdu3fPy8rKiqdAbQZ0GkgXPomC\nxMTEAbNnz362rKws7/333/+52+2Oh1SYwSAGcqGvpzOElJSUwUuWLHm5qqrqtfXr14d8utOmpAMu\nV+CJYjGOYsSIEQ/k5+evO3369IrnnntuhGo9JuEC0lz4uyKN8UycOPGRHTt2bNy/f/+vJ06cOEi1\nHhMY4kLPf0zF5XIlzpgx45fHjh3L27p169MOC9QO1j1QjBg4cGDKiy++uKCqqmpDXl7eJNV6DGKw\nixBvYdAYQ0ZGRmZubu7vKisr/7Bw4UK7xx+TXejSBEoYO3ZsTkFBwV+Ki4t/M3XqVLuugt0uIJ42\nv6yGa9q0aTMOHTqUt2PHjmeGDRtmt0CtW/dAFsDtdifPmzdvfmVl5evvvPPO46r1hIE2kJVIS0sb\ntmrVqqU1NTV/evXVV+1wuMGdALyMwyfSPp9vs2oNkXD8+PHi+fPnb62oqDCt5mmUtOkeyMJMnjx5\n+p49e36rWkcfDHDSppZGAS7Ao1qEpndOnDhRPHv27C2qdfRBuzaQBamtra1YuXLlPydNmrTFwvMf\nAE8SBpQ91BhDc3Nz/QcffPDZ6tWrS1RrCRFPEroHUo7H42nbvXv37kWLFn157dq1qEpQxhhtIMV4\nS0tLDy5fvvzz0tLSZtViIkAbSBUXLlw4lZub+8mHH34YcblJC9CeBNxSrSKeaGhoqHvvvfcKc3Nz\nj6vWYgC3koAm1SrigVu3brVu3769aOnSpd+0tLQ45Wq0piSgUbUKJ+P1ejsOHjz49ZIlS3aeOXPm\nhmo9BtOkeyATKSsrO7Zu3bpPi4qKrqjWYhKNSUi5Zy/6ZIZhXL169dJbb731SX5+/hnVWkzECzQn\nBZ6gc6OjprW1tWnbtm2fr1ix4pDH4/Gp1mMy1wFvIAOuEW2giOno6Gjfu3fvl4sXL95TVVUVL6va\nRui8ULMBiNebJqLBd/z48ZJVq1Z9tn///gbVYmLMXQaqRZfzDovq6upzb7zxxsebN28+r1qLImqh\n00B16Il0SDQ3N/+4ZcuWz9asWVOqWotCvIhn7hjIA1xFl/UOisfjublz585dixcv/qqhocFOAU8z\nuII/BNb1GEk12kA98Pl83pKSkm9feeWVfx85csSOAU8zqAk8Ser2w8dir8W6VFRUnMzNzf1k27Zt\nNf2/Oq7o1UCXgQ70XdHU19fXvvvuu59u2LDhhGotFqQD8Qpwt4E6kJn1fbFWZBXa2tpaCgsL/7Ns\n2bL9N27c8KrWY1FqEa8APQurnCUODeT1em8fOHDg66VLl+4sLy+3cg6yFSjv+h/dDVQJPNnLzx3L\n6dOnj6xdu7Zw165dV1VrsQG3EY/cobtRPP4XOL4Q2+XLly/m5+d/vGnTpvL+X63xU4GY6A699TRl\nOMxALS0t9V1KXjb6S14e0iUvw6bHL1tvK64W4Cc46NqXtLS0q1lZWYP27dt3YO7cuf/66KOPLvh8\nTg+WG04rcLD7D4PdZ/wEMMVUORq7cRQo7v7DYLGvU4D+FdUE8CGe6EEwA11HlvQaDcjcp9cwTl/R\n9yPmaNHYkKPB/kdfBmpElm2a+KaCPk7u9Jf/E9R5mrihz5GoPwP9CFw0TovGZlwErvX1glAyEOM5\n8y7e+b6/F4SSunEDGISuqRpvnARO9/eiUHOgi4G2qORo7EQbENIlV6Ea6BZwOGI5GrvxHSHe2hLO\nKYwy/Jn4GkdTh7R1SIRjIB/wLTrE4WQCbRwy4eY/3/S/R5cJdyZHgHPhvCGSg4Sl6KHMidQB/wv3\nTZEYyAt8hV6VOYk2pE3DPkgQ6REeD1CPwzIX45gvkahD2ERzBuy6//2ZUXyGRj1HCZLrEwrRXqbw\nPXo+ZGfqCCFc0RfRnkL1AReQu4WSo/wsTWxpBL4gynvCjTjGfBsx0YPo2mN2oRUoQuKcUWHUOfh2\noAoYb+BnasyhHTHPdSM+zMjGvomMqePRF1VZlQ5gFxGuuHrD6N6iBVneP0jwI0MaNfiA/yL3QBmG\nGcNNE5LFNg7dE1mFDsQ8hmeXmjVfaUKuARln4ndoQqMdGbZMqQpkZuO2II4fi16dqSKw2jJsztMd\ns3uHm8ixkPvR+0SxphEDV1vBiMXw0o6kCIwEUmPwfRpZDX+BAfs8/RGr+clt5HisCx07M5ujwD5i\nVIkylhNcHzKRu4IMaXFzC1qMaEOi6jG9GEPFCilwccMI9JBmFHXATkycLAdD1RLbgwxpCciQpjcd\nI8OHpKF+g8w1Y44VGm4ocrGnnhuFRx2SAF+vUoQVDASi4yHkZjS93O+bNuTcVshHb8zEKgYKMBCY\nDuSoFmJRTiInRi1T1M5qBgowHJgKPKBaiEW4iGQOxnyS3B9WNVCAYcCjQLZqIYqoQCbJfV6xohKr\nGyjAEKSi4gTsozlSfMgK9Sh93AxmFezWGGnAw0jSWopiLUbTihjnFEEutLQidjNQgAQgC1m5jcO+\nu9q3kWGqHKnBZbt7B+xqoK64kZSRCciZfavnHwXKapUjdUlsXT7TCQbqSiIS9c/yP0agPivSi8T/\navyPQGE/R+A0A3XHjexwjwIygMFAOuaZyovE+hr9j1pkxzgmkXEVON1AveFCJuNDEEMNRna/3b08\nAgVn2hETdH+0Iem7TYhhmongggI7838jQjJwZ8OEVQAAAABJRU5ErkJggg==\n"

.field private static final TAG:Ljava/lang/String; = "XNativeView"


# instance fields
.field private mAdLogger:Lcom/baidu/mobads/utils/n;

.field private mCoverPic:Landroid/widget/ImageView;

.field private mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

.field private mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

.field private mPauseBtn:Landroid/widget/ImageView;

.field private mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 102
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 98
    new-instance p1, Lcom/baidu/mobads/utils/n;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/n;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/n;

    const-string p1, "#000000"

    .line 103
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 104
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 108
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    new-instance p1, Lcom/baidu/mobads/utils/n;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/n;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/n;

    const-string p1, "#000000"

    .line 109
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 110
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    new-instance p1, Lcom/baidu/mobads/utils/n;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/n;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/n;

    const-string p1, "#000000"

    .line 115
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 116
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method private hideView()V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 217
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private initAdVideoView()V
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 261
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setCanClickVideo(Z)V

    .line 262
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v2, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    .line 263
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setVideoMute(Z)V

    return-void
.end method

.method private isVisible(Landroid/view/View;I)Z
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 307
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 310
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 312
    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_1

    return v0

    .line 317
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-long v2, v2

    .line 318
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v4, v1

    mul-long v2, v2, v4

    .line 319
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-long v6, p1

    mul-long v4, v4, v6

    const-wide/16 v6, 0x0

    cmp-long p1, v4, v6

    if-gtz p1, :cond_2

    return v0

    :cond_2
    const-wide/16 v6, 0x64

    mul-long v2, v2, v6

    int-to-long p1, p2

    mul-long p1, p1, v4

    cmp-long v1, v2, p1

    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_0
    return v0
.end method

.method private play()V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 192
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->resetAllPlayer(Lcom/baidu/mobads/component/XNativeView;)V

    .line 193
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->initAdVideoView()V

    .line 194
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->hideView()V

    .line 195
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->play()V

    :cond_0
    return-void
.end method

.method private renderView()V
    .locals 4

    .line 146
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-nez v1, :cond_1

    .line 151
    new-instance v1, Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    .line 152
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/baidu/mobads/component/XNativeView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    :cond_1
    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne v0, v1, :cond_2

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    goto :goto_0

    .line 157
    :cond_2
    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne v0, v1, :cond_3

    .line 159
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->showView()V

    :cond_3
    :goto_0
    return-void
.end method

.method private shouldAutoPlay()Z
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isAutoPlay()Z

    move-result v0

    .line 206
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v1

    .line 207
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private showView()V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    .line 236
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 238
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/component/XNativeView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 244
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    .line 245
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    .line 246
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    move-result-object v1

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAJAAAACQCAYAAADnRuK4AAAABHNCSVQICAgIfAhkiAAADT5JREFU\neJztnXtsleUdxz897YFKL1AQKEUFKsjqAqgILotIzBYQdERilqAsMTAuIoMNGEtmt8G0cUuj/GFM\nDDFk/lGRqBXZisCMiGBAWjsuQS4ttFx6A6QX2kLpoefsj985tLQ97bm873ne9z3PJznhWM/lC8+3\nz+33e55fAvGHC0gHBgND/H8OBNxdHgO6PAfw+B/tXZ57gFtAE9Do//M64I3R38MSJKgWYDJuIBMY\nBWQghklDTGQGXsRETUADUAvUIWZzJE4zUCIwEsgCRgPDMc8soeIFrgLVQA1wGehQqshAnGAgNzAO\nGI/0NIlq5fRLB9IzlQPnsXnvZFcDJSA9zATEPElq5UTMbaASKEN6J59aOeFjNwOlAzmIcQYp1mI0\nrcBZ4BQyj7IFdjHQEOBRZJiyi+ZI8SFGOoKs7iyN1RvjXsQ441QLUUQFcBT4UbWQYFjVQMOBqcAD\nqoVYhItAKbKasxRWM9BAYDoyz9H05BRQjGxgWgKrGCgBeAh4AkhWrMXqtAGHkZWb8lWbFQw0DHgS\n2QDUhE4d8C1Qr1KESgO5kHnOI4p12BkfMskuRVEMTlXDpQC/QOJUmuipA75C9pJiiopt//uBucje\njsYYUpE5ZD0x3oSMpYFcwDRgBvYNPViZJGSHPhGJtcVkgh0rAyUDc5CdZI25ZCLZCBeQWJupxMJA\nqcCvkF1lTWxIBcYgJmo384vMNtBQxDxpJn+PpifJwINAFXDTrC8x00CZwLPojUGVuJFpQx3QYsYX\nmGWgMcAzdOYUa9SRiJioHkm1NfzDjWYMMAv1qaSaTlzIcHYNg01ktIEykZ5Hm8d6JCBpMbUYOJwZ\naaChyJxHD1vWxYWY6CIGTayNMlBgqa4nzNYnERiLJKtFvcQ3wkDJ6KW63XAjIaVzRLnZGK2BXMgO\ns94ktB/JSApNOVGEPaI10DR0eMLOpCKdQHWkHxCNge5HAqMae5MJXCHCKH6ky+0U4OkI36uxHk8j\nbRo2kRjIhSSD6RWXc0hG2jRsP0QyhD2O5J1onEUqstlYE86bwnXcMCSHWeNMHkE2hEMmHAMlIKcn\ndAK8cwm7jcMx0EPoozfxQCbS1iERqoEGIof+NPHBE0ib90uoBpqOXnXFE8lIm/dLKAYajj6rHo/k\nIG3fJ6EYaGr0WjQ2pd+2789A96KvWIlnHkC2boLSn4EeNU6Lxqb06YG+DDSE+L0ZTNNJNn0cQ+/L\nQLr30QQIGn0IZqB0dJ6PppMJBMk4DWagHHTIQtNJAkG2cnozUAIOi7a/+eabk8+ePbuqsLBwVmZm\n5gDVemzKBHrpVHrrZe5D7u9xDM3Nzf9ITU0dCtDa2tpQUFDw+cqVK7/r6HBMyYpYsZNu6a+99UCO\n6n0AAuYBSElJyVi+fPmi6urq19auXeu4v6vJ9AiydjdQoHCJ4xk5cuSYt99++48nT55cPmfOnH63\n7DWAeOOug6PdDWTnwiURkZOT81hRUdHGffv2vZCdnX2Paj0WJwk5lHiH7gaKy6W7y+VKmjlz5qwf\nfvghr6CgYOagQYP02f7g3DXsd/2HSkTqbcUtycnJqQsXLnzp0qVLf9u4ceNPVeuxKHfVZOtqoJFY\nv1hbTBg6dOioDRs2rD537tzvFyxYkKVaj8UIVIUE7jbQ6NhrsTbZ2dkPb9269a+HDx9+acqUKamq\n9ViIO79Urt5+qOkkISHBNX369JklJSV527dvn5WRkRFXi4wg9DCQmxCyz+IZt9t9z/PPP//C+fPn\n/75p06Z4T7IbgX85HzBQJpEfc44r0tPT712zZs2yqqqq9cuWLRujWo8iXPjLVARME9err0gYPXr0\n+M2bN//52LFji5566qkM1XoUMAo6DRSP/wBGkDB58uSf7d279/Xdu3fPy8rKiqdAbQZ0GkgXPomC\nxMTEAbNnz362rKws7/333/+52+2Oh1SYwSAGcqGvpzOElJSUwUuWLHm5qqrqtfXr14d8utOmpAMu\nV+CJYjGOYsSIEQ/k5+evO3369IrnnntuhGo9JuEC0lz4uyKN8UycOPGRHTt2bNy/f/+vJ06cOEi1\nHhMY4kLPf0zF5XIlzpgx45fHjh3L27p169MOC9QO1j1QjBg4cGDKiy++uKCqqmpDXl7eJNV6DGKw\nixBvYdAYQ0ZGRmZubu7vKisr/7Bw4UK7xx+TXejSBEoYO3ZsTkFBwV+Ki4t/M3XqVLuugt0uIJ42\nv6yGa9q0aTMOHTqUt2PHjmeGDRtmt0CtW/dAFsDtdifPmzdvfmVl5evvvPPO46r1hIE2kJVIS0sb\ntmrVqqU1NTV/evXVV+1wuMGdALyMwyfSPp9vs2oNkXD8+PHi+fPnb62oqDCt5mmUtOkeyMJMnjx5\n+p49e36rWkcfDHDSppZGAS7Ao1qEpndOnDhRPHv27C2qdfRBuzaQBamtra1YuXLlPydNmrTFwvMf\nAE8SBpQ91BhDc3Nz/QcffPDZ6tWrS1RrCRFPEroHUo7H42nbvXv37kWLFn157dq1qEpQxhhtIMV4\nS0tLDy5fvvzz0tLSZtViIkAbSBUXLlw4lZub+8mHH34YcblJC9CeBNxSrSKeaGhoqHvvvfcKc3Nz\nj6vWYgC3koAm1SrigVu3brVu3769aOnSpd+0tLQ45Wq0piSgUbUKJ+P1ejsOHjz49ZIlS3aeOXPm\nhmo9BtOkeyATKSsrO7Zu3bpPi4qKrqjWYhKNSUi5Zy/6ZIZhXL169dJbb731SX5+/hnVWkzECzQn\nBZ6gc6OjprW1tWnbtm2fr1ix4pDH4/Gp1mMy1wFvIAOuEW2giOno6Gjfu3fvl4sXL95TVVUVL6va\nRui8ULMBiNebJqLBd/z48ZJVq1Z9tn///gbVYmLMXQaqRZfzDovq6upzb7zxxsebN28+r1qLImqh\n00B16Il0SDQ3N/+4ZcuWz9asWVOqWotCvIhn7hjIA1xFl/UOisfjublz585dixcv/qqhocFOAU8z\nuII/BNb1GEk12kA98Pl83pKSkm9feeWVfx85csSOAU8zqAk8Ser2w8dir8W6VFRUnMzNzf1k27Zt\nNf2/Oq7o1UCXgQ70XdHU19fXvvvuu59u2LDhhGotFqQD8Qpwt4E6kJn1fbFWZBXa2tpaCgsL/7Ns\n2bL9N27c8KrWY1FqEa8APQurnCUODeT1em8fOHDg66VLl+4sLy+3cg6yFSjv+h/dDVQJPNnLzx3L\n6dOnj6xdu7Zw165dV1VrsQG3EY/cobtRPP4XOL4Q2+XLly/m5+d/vGnTpvL+X63xU4GY6A699TRl\nOMxALS0t9V1KXjb6S14e0iUvw6bHL1tvK64W4Cc46NqXtLS0q1lZWYP27dt3YO7cuf/66KOPLvh8\nTg+WG04rcLD7D4PdZ/wEMMVUORq7cRQo7v7DYLGvU4D+FdUE8CGe6EEwA11HlvQaDcjcp9cwTl/R\n9yPmaNHYkKPB/kdfBmpElm2a+KaCPk7u9Jf/E9R5mrihz5GoPwP9CFw0TovGZlwErvX1glAyEOM5\n8y7e+b6/F4SSunEDGISuqRpvnARO9/eiUHOgi4G2qORo7EQbENIlV6Ea6BZwOGI5GrvxHSHe2hLO\nKYwy/Jn4GkdTh7R1SIRjIB/wLTrE4WQCbRwy4eY/3/S/R5cJdyZHgHPhvCGSg4Sl6KHMidQB/wv3\nTZEYyAt8hV6VOYk2pE3DPkgQ6REeD1CPwzIX45gvkahD2ERzBuy6//2ZUXyGRj1HCZLrEwrRXqbw\nPXo+ZGfqCCFc0RfRnkL1AReQu4WSo/wsTWxpBL4gynvCjTjGfBsx0YPo2mN2oRUoQuKcUWHUOfh2\noAoYb+BnasyhHTHPdSM+zMjGvomMqePRF1VZlQ5gFxGuuHrD6N6iBVneP0jwI0MaNfiA/yL3QBmG\nGcNNE5LFNg7dE1mFDsQ8hmeXmjVfaUKuARln4ndoQqMdGbZMqQpkZuO2II4fi16dqSKw2jJsztMd\ns3uHm8ixkPvR+0SxphEDV1vBiMXw0o6kCIwEUmPwfRpZDX+BAfs8/RGr+clt5HisCx07M5ujwD5i\nVIkylhNcHzKRu4IMaXFzC1qMaEOi6jG9GEPFCilwccMI9JBmFHXATkycLAdD1RLbgwxpCciQpjcd\nI8OHpKF+g8w1Y44VGm4ocrGnnhuFRx2SAF+vUoQVDASi4yHkZjS93O+bNuTcVshHb8zEKgYKMBCY\nDuSoFmJRTiInRi1T1M5qBgowHJgKPKBaiEW4iGQOxnyS3B9WNVCAYcCjQLZqIYqoQCbJfV6xohKr\nGyjAEKSi4gTsozlSfMgK9Sh93AxmFezWGGnAw0jSWopiLUbTihjnFEEutLQidjNQgAQgC1m5jcO+\nu9q3kWGqHKnBZbt7B+xqoK64kZSRCciZfavnHwXKapUjdUlsXT7TCQbqSiIS9c/yP0agPivSi8T/\navyPQGE/R+A0A3XHjexwjwIygMFAOuaZyovE+hr9j1pkxzgmkXEVON1AveFCJuNDEEMNRna/3b08\nAgVn2hETdH+0Iem7TYhhmongggI7838jQjJwZ8OEVQAAAABJRU5ErkJggg==\n"

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;->string2bitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 245
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 248
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 249
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x78

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 251
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 252
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/component/XNativeView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public handleCover()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCoverPic:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mPauseBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 227
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/16 v0, 0x11

    if-ne p1, v0, :cond_2

    .line 137
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    if-eqz p1, :cond_1

    .line 138
    invoke-interface {p1, p0}, Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;->onNativeViewClick(Lcom/baidu/mobads/component/XNativeView;)V

    .line 140
    :cond_1
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->play()V

    :cond_2
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 269
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 270
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->removeNativeView(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public onScroll()V
    .locals 1

    const/16 v0, 0x32

    .line 292
    invoke-direct {p0, p0, v0}, Lcom/baidu/mobads/component/XNativeView;->isVisible(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->pause()V

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->render()V

    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->resume()V

    goto :goto_0

    .line 278
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->pause()V

    .line 280
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public pause()V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->pause()V

    :cond_0
    return-void
.end method

.method public render()V
    .locals 1

    .line 177
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->shouldAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->play()V

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->resume()V

    :cond_0
    return-void
.end method

.method public setNativeItem(Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 0

    .line 126
    check-cast p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 127
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->renderView()V

    return-void
.end method

.method public setNativeItem(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 122
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->renderView()V

    return-void
.end method

.method public setNativeViewClickListener(Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    return-void
.end method

.method public stop()V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->stop()V

    :cond_0
    return-void
.end method
