.class public Lcom/baidu/mobads/component/FeedPortraitVideoView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "FeedPortraitVideoView"


# instance fields
.field private mAdActionBtn:Landroid/widget/TextView;

.field public mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

.field private mAdVideoListener:Lcom/baidu/mobads/i/d;

.field private mAdVideoView:Lcom/baidu/mobads/i/a;

.field private mBigPic:Landroid/widget/ImageView;

.field private mCanClickVideo:Z

.field private mContext:Landroid/content/Context;

.field private mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

.field private mFloatView:Landroid/widget/LinearLayout;

.field private mHandleFrontEnd:Z

.field private mReplayBtn:Landroid/widget/TextView;

.field private mSendShowLog:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 65
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    .line 210
    new-instance v0, Lcom/baidu/mobads/component/a;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/component/a;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoListener:Lcom/baidu/mobads/i/d;

    .line 66
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 p2, 0x1

    .line 62
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    .line 210
    new-instance p2, Lcom/baidu/mobads/component/a;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/component/a;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoListener:Lcom/baidu/mobads/i/d;

    .line 71
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 p2, 0x1

    .line 62
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    .line 210
    new-instance p2, Lcom/baidu/mobads/component/a;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/component/a;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoListener:Lcom/baidu/mobads/i/d;

    .line 76
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$002(Lcom/baidu/mobads/component/FeedPortraitVideoView;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    return p1
.end method

.method static synthetic access$100(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showEndFrame()V

    return-void
.end method

.method static synthetic access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

    return-object p0
.end method

.method static synthetic access$300(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$400(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobad/feeds/XAdNativeResponse;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    return-object p0
.end method

.method static synthetic access$500(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->hideEndFrame()V

    return-void
.end method

.method static synthetic access$600(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mSendShowLog:Z

    return p0
.end method

.method static synthetic access$602(Lcom/baidu/mobads/component/FeedPortraitVideoView;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mSendShowLog:Z

    return p1
.end method

.method static synthetic access$700(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/i/a;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    return-object p0
.end method

.method static synthetic access$800(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getVideoPlayUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private dp2px(Landroid/content/Context;F)I
    .locals 0

    .line 403
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float p2, p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    float-to-int p1, p2

    return p1
.end method

.method private getVideoPlayUrl()Ljava/lang/String;
    .locals 4

    .line 378
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 380
    invoke-static {v0}, Lcom/baidu/mobads/utils/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 381
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 382
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 383
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    return-object v0
.end method

.method private hideEndFrame()V
    .locals 3

    .line 390
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideEndFrame"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FeedPortraitVideoView"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 398
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private initAdVideoView()V
    .locals 2

    .line 199
    new-instance v0, Lcom/baidu/mobads/i/a;

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    .line 200
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 203
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 204
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoListener:Lcom/baidu/mobads/i/d;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/a;->a(Lcom/baidu/mobads/i/d;)V

    .line 206
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->c()V

    .line 207
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->g()V

    return-void
.end method

.method private release()V
    .locals 1

    const/4 v0, 0x0

    .line 408
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    .line 409
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 410
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    .line 411
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    .line 412
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    const/4 v0, 0x1

    .line 413
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    const/4 v0, 0x0

    .line 414
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mSendShowLog:Z

    return-void
.end method

.method private showEndFrame()V
    .locals 14

    .line 271
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "FeedPortraitVideoView"

    const-string v2, "showEndFrame,,"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    .line 275
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 276
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    const-string v2, "#73000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 277
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getHeight()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 279
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/baidu/mobads/component/b;

    invoke-direct {v2, p0}, Lcom/baidu/mobads/component/b;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    .line 289
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getWidth()I

    move-result v0

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v4, v4, v2

    double-to-int v0, v4

    const-wide v2, 0x3fd999999999999aL    # 0.4

    .line 290
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getHeight()I

    move-result v4

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v4, v4, v2

    double-to-int v2, v4

    .line 291
    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    const/high16 v4, 0x41a80000    # 21.0f

    invoke-direct {p0, v3, v4}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->dp2px(Landroid/content/Context;F)I

    move-result v3

    const-wide v4, 0x3fc3333333333333L    # 0.15

    .line 293
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getHeight()I

    move-result v6

    int-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v6, v6, v4

    double-to-int v4, v6

    .line 294
    iget-object v5, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    const/high16 v6, 0x420c0000    # 35.0f

    invoke-direct {p0, v5, v6}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->dp2px(Landroid/content/Context;F)I

    move-result v5

    if-le v4, v5, :cond_1

    .line 295
    iget-object v4, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v6}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->dp2px(Landroid/content/Context;F)I

    move-result v4

    :cond_1
    const-wide v5, 0x3fdb851eb851eb85L    # 0.43

    int-to-double v7, v4

    .line 298
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v7, v7, v5

    double-to-int v5, v7

    .line 300
    iget-object v6, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    const/16 v7, 0x11

    const/16 v8, 0x10

    const/4 v9, -0x1

    if-nez v6, :cond_4

    .line 301
    new-instance v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v6, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    .line 302
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 304
    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 305
    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 306
    iput v2, v6, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 307
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    int-to-float v10, v3

    .line 309
    invoke-virtual {v2, v10}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    const-string v10, "#3897F0"

    .line 310
    invoke-static {v10}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v2, v10}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 311
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v10, v8, :cond_2

    .line 312
    iget-object v10, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 314
    :cond_2
    iget-object v10, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 316
    :goto_1
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isDownloadApp()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 317
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    const-string v10, "\u7acb\u5373\u4e0b\u8f7d"

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 319
    :cond_3
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    const-string v10, "\u67e5\u770b\u8be6\u60c5"

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    :goto_2
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 322
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    int-to-float v10, v5

    invoke-virtual {v2, v1, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 323
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 324
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    new-instance v10, Lcom/baidu/mobads/component/c;

    invoke-direct {v10, p0}, Lcom/baidu/mobads/component/c;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdActionBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v10, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 334
    :cond_4
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    :goto_3
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    if-nez v2, :cond_6

    .line 338
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    .line 339
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 341
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 342
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const-wide v10, 0x3f9eb851eb851eb8L    # 0.03

    .line 343
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getHeight()I

    move-result v0

    int-to-double v12, v0

    invoke-static {v12, v13}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v12, v12, v10

    double-to-int v0, v12

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 344
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 345
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    int-to-float v3, v3

    .line 346
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 347
    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    const/high16 v4, 0x3e800000    # 0.25f

    invoke-direct {p0, v3, v4}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->dp2px(Landroid/content/Context;F)I

    move-result v3

    const-string v4, "#FFFFFF"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 348
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v3, v8, :cond_5

    .line 349
    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 351
    :cond_5
    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 353
    :goto_4
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    const-string v3, "\u70b9\u51fb\u91cd\u64ad"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 355
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    int-to-float v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 356
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 357
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    new-instance v1, Lcom/baidu/mobads/component/d;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/component/d;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mReplayBtn:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    .line 373
    :cond_6
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    return-void
.end method


# virtual methods
.method public getCurrentPosition()J
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->e()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->f()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isPlaying()Z
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->d()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isShowEndFrame()Z
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFloatView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 124
    iget-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanClickVideo:Z

    if-nez v0, :cond_0

    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {v0, p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->handleClick(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public pause()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->a()V

    :cond_0
    return-void
.end method

.method public play()V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz v0, :cond_0

    .line 116
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->hideEndFrame()V

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getVideoPlayUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/a;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 3

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "FeedPortraitVideoView"

    const-string v2, "resume"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mHandleFrontEnd:Z

    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->b()V

    :cond_0
    return-void
.end method

.method public setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 2

    if-nez p1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "FeedPortraitVideoView"

    const-string v1, "\u5e7f\u544a\u54cd\u5e94\u5185\u5bb9\u4e3a\u7a7a\uff0c\u65e0\u6cd5\u64ad\u653e"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->stop()V

    .line 95
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 96
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object p1

    .line 97
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne p1, v0, :cond_1

    .line 98
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    .line 99
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mSendShowLog:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 100
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mSendShowLog:Z

    .line 101
    invoke-virtual {p1, p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->recordImpression(Landroid/view/View;)V

    goto :goto_0

    .line 103
    :cond_1
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne p1, v0, :cond_2

    .line 104
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->initAdVideoView()V

    .line 105
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/i/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz p1, :cond_2

    .line 107
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getVideoPlayUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/i/a;->b(Ljava/lang/String;)V

    .line 109
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdResponse:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;

    move-result-object v0

    const-string v1, "play_start\u00f8"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobads/utils/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/component/AdLogInfo;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setCanClickVideo(Z)V
    .locals 0

    .line 190
    iput-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanClickVideo:Z

    return-void
.end method

.method public setFeedPortraitListener(Lcom/baidu/mobads/component/IFeedPortraitListener;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

    return-void
.end method

.method public setVideoMute(Z)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/i/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 257
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    .line 258
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 259
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :cond_1
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_2

    .line 265
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mBigPic:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 1

    .line 149
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->release()V

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->c()V

    .line 152
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->removeAllViews()V

    const/4 v0, 0x0

    .line 153
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdVideoView:Lcom/baidu/mobads/i/a;

    :cond_0
    return-void
.end method
