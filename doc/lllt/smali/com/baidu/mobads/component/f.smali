.class Lcom/baidu/mobads/component/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/component/NativeVideoView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/component/NativeVideoView;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 272
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    invoke-virtual {p1}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 273
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    iget-object p1, p1, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz p1, :cond_1

    .line 274
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    iget-object p1, p1, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {p1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onClickAd()V

    .line 275
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    invoke-virtual {p1}, Lcom/baidu/mobads/component/NativeVideoView;->pause()V

    .line 276
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    iget-object p1, p1, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    iget-object v0, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    goto :goto_0

    .line 279
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/component/f;->a:Lcom/baidu/mobads/component/NativeVideoView;

    invoke-virtual {p1}, Lcom/baidu/mobads/component/NativeVideoView;->tryToPrepare()V

    :cond_1
    :goto_0
    return-void
.end method
