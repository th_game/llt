.class public Lcom/baidu/mobads/component/AdLogInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private mAdPlaceId:Ljava/lang/String;

.field private mQk:Ljava/lang/String;

.field private mVideoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdPlaceId()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/baidu/mobads/component/AdLogInfo;->mAdPlaceId:Ljava/lang/String;

    return-object v0
.end method

.method public getQk()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/baidu/mobads/component/AdLogInfo;->mQk:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/baidu/mobads/component/AdLogInfo;->mVideoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setAdPlaceId(Ljava/lang/String;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/baidu/mobads/component/AdLogInfo;->mAdPlaceId:Ljava/lang/String;

    return-void
.end method

.method public setQk(Ljava/lang/String;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/baidu/mobads/component/AdLogInfo;->mQk:Ljava/lang/String;

    return-void
.end method

.method public setVideoUrl(Ljava/lang/String;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/baidu/mobads/component/AdLogInfo;->mVideoUrl:Ljava/lang/String;

    return-void
.end method
