.class public Lcom/baidu/mobads/component/MonitorLogReplaceManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final CLICK_AREA:Ljava/lang/String; = "hot"

.field private static final PLAY_MODE:Ljava/lang/String; = "0"

.field private static final REGULAR_MATCH_CLICK_AREA:Ljava/lang/String; = "%25%25area%25%25"

.field private static final REGULAR_MATCH_CUR_TIME:Ljava/lang/String; = "%25%25cur_time%25%25"

.field private static final REGULAR_MATCH_PLAY_MODE:Ljava/lang/String; = "%25%25play_mode%25%25"

.field private static final REGULAR_MATCH_START_TIME:Ljava/lang/String; = "%25%25start_time%25%25"

.field private static final REGULAR_MATCH_TIME_STAMP:Ljava/lang/String; = "%25%25origin_time%25%25"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 1

    const/4 v0, 0x0

    .line 140
    invoke-interface {p1, p0, v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->fireAdMetrics(Ljava/lang/String;Ljava/util/HashMap;)V

    return-void
.end method

.method public static regularMatch(Ljava/lang/String;II)Ljava/lang/String;
    .locals 4

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "%25%25origin_time%25%25"

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "%25%25play_mode%25%25"

    const-string v2, "0"

    .line 126
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "%25%25cur_time%25%25"

    .line 127
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "%25%25start_time%25%25"

    .line 128
    invoke-virtual {p0, p2, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "%25%25area%25%25"

    const-string p2, "hot"

    .line 129
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static sendClickLog(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    .line 60
    :cond_0
    invoke-interface {p0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdClickTrackingUrls()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 62
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p0, v2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setThirdClickTrackingUrls(Ljava/util/Set;)V

    const/4 v2, 0x0

    .line 63
    invoke-static {v1, v2, v2}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static sendImpressionLog(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    .line 42
    :cond_0
    invoke-interface {p0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getImpressionUrls()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    invoke-static {v1, v2, v2}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    .line 45
    :cond_1
    invoke-interface {p0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdImpressionTrackingUrls()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    invoke-static {v0, v2, v2}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public static sendVCloseLog(IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 110
    invoke-static {v0, p0, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static sendVSkipLog(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 92
    :cond_0
    invoke-interface {p0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getSkipTrackers()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    .line 93
    invoke-static {v0, v1, v1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static sendVStartLog(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 77
    :cond_0
    invoke-interface {p0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    .line 78
    invoke-static {v0, v1, v1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->regularMatch(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/baidu/mobads/component/MonitorLogReplaceManager;->httpGetRequest(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method
