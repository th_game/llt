.class Lcom/baidu/mobads/component/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/i/d;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/component/FeedPortraitVideoView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 214
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$002(Lcom/baidu/mobads/component/FeedPortraitVideoView;Z)Z

    .line 215
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "FeedPortraitVideoView"

    const-string v2, "playCompletion"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$100(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playCompletion()V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$300(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$400(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobad/feeds/XAdNativeResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;

    move-result-object v1

    const-string v2, "play_completion"

    invoke-static {v0, v2, v1}, Lcom/baidu/mobads/utils/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/component/AdLogInfo;)V

    return-void
.end method

.method public b()V
    .locals 3

    .line 226
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$002(Lcom/baidu/mobads/component/FeedPortraitVideoView;Z)Z

    .line 227
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$100(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    .line 228
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "FeedPortraitVideoView"

    const-string v2, "playFailure"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playError()V

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$300(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$400(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobad/feeds/XAdNativeResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;

    move-result-object v1

    const-string v2, "play_error"

    invoke-static {v0, v2, v1}, Lcom/baidu/mobads/utils/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/component/AdLogInfo;)V

    return-void
.end method

.method public c()V
    .locals 3

    .line 238
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playRenderingStart()V

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$500(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    .line 242
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$400(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobad/feeds/XAdNativeResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$600(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$602(Lcom/baidu/mobads/component/FeedPortraitVideoView;Z)Z

    .line 244
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$400(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobad/feeds/XAdNativeResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->recordImpression(Landroid/view/View;)V

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "FeedPortraitVideoView"

    const-string v2, "renderingStart"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
