.class public Lcom/baidu/mobads/component/NativeVideoView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/component/NativeVideoView$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FSVideoView"


# instance fields
.field protected activity:Landroid/app/Activity;

.field protected context:Landroid/content/Context;

.field protected currentLayoutParams:Landroid/view/ViewGroup$LayoutParams;

.field protected currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

.field protected initialConfigOrientation:I

.field protected initialMovieHeight:I

.field protected initialMovieWidth:I

.field isCompleted:Z

.field protected isFullscreen:Z

.field protected keepState:Z

.field protected lastState:Lcom/baidu/mobads/component/NativeVideoView$State;

.field protected loadingView:Landroid/view/View;

.field protected mHandler:Landroid/os/Handler;

.field protected mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

.field protected mediaPlayer:Landroid/media/MediaPlayer;

.field protected parentView:Landroid/view/ViewGroup;

.field protected shouldAutoplay:Z

.field protected surfaceHolder:Landroid/view/SurfaceHolder;

.field protected surfaceIsReady:Z

.field protected surfaceView:Landroid/view/SurfaceView;

.field protected videoIsReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 84
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 60
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mHandler:Landroid/os/Handler;

    .line 85
    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    .line 86
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 60
    iput-boolean p2, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    .line 64
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/baidu/mobads/component/NativeVideoView;->mHandler:Landroid/os/Handler;

    .line 91
    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    .line 92
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 60
    iput-boolean p2, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    .line 64
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/baidu/mobads/component/NativeVideoView;->mHandler:Landroid/os/Handler;

    .line 97
    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    .line 98
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->init()V

    return-void
.end method


# virtual methods
.method public fullscreen()V
    .locals 6

    .line 459
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_9

    .line 462
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->pause()V

    .line 465
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v1, :cond_0

    .line 466
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    .line 469
    :cond_0
    iget-boolean v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->isFullscreen:Z

    const/4 v2, 0x1

    if-nez v1, :cond_4

    .line 470
    iput-boolean v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->isFullscreen:Z

    .line 471
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getRootView()Landroid/view/View;

    move-result-object v1

    const v3, 0x1020002

    .line 472
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 473
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 474
    instance-of v4, v3, Landroid/view/ViewGroup;

    const-string v5, "FSVideoView"

    if-eqz v4, :cond_2

    .line 475
    iget-object v4, p0, Lcom/baidu/mobads/component/NativeVideoView;->parentView:Landroid/view/ViewGroup;

    if-nez v4, :cond_1

    .line 476
    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/baidu/mobads/component/NativeVideoView;->parentView:Landroid/view/ViewGroup;

    .line 479
    :cond_1
    iput-boolean v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->keepState:Z

    .line 481
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 482
    iget-object v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    const-string v2, "Parent View is not a ViewGroup"

    .line 484
    invoke-static {v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :goto_0
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 487
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    const-string v1, "RootView is not a ViewGroup"

    .line 489
    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    .line 492
    iput-boolean v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->isFullscreen:Z

    .line 493
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 494
    instance-of v4, v3, Landroid/view/ViewGroup;

    if-eqz v4, :cond_6

    .line 497
    iget-object v4, p0, Lcom/baidu/mobads/component/NativeVideoView;->parentView:Landroid/view/ViewGroup;

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 499
    iput-boolean v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->keepState:Z

    const/4 v1, 0x1

    .line 502
    :cond_5
    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    if-eqz v1, :cond_6

    .line 504
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 505
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/component/NativeVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 509
    :cond_6
    :goto_1
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->resize()V

    .line 510
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v1, :cond_7

    .line 511
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/component/VideoPlayCallback;->onFullScreen(I)V

    :cond_7
    if-eqz v0, :cond_8

    .line 513
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_8

    .line 514
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->start()V

    .line 515
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 516
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStart()V

    :cond_8
    return-void

    .line 460
    :cond_9
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCurrentPosition()I
    .locals 2

    .line 526
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    return v0

    .line 531
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized getCurrentState()Lcom/baidu/mobads/component/NativeVideoView$State;
    .locals 1

    monitor-enter p0

    .line 396
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDuration()I
    .locals 2

    .line 540
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0

    .line 543
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVideoHeight()I
    .locals 2

    .line 552
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 553
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    return v0

    .line 555
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVideoWidth()I
    .locals 2

    .line 564
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 565
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    return v0

    .line 567
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected init()V
    .locals 3

    .line 241
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 245
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->shouldAutoplay:Z

    .line 246
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    const/4 v0, 0x0

    .line 247
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->isFullscreen:Z

    const/4 v0, -0x1

    .line 248
    iput v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialConfigOrientation:I

    const/high16 v1, -0x1000000

    .line 249
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/component/NativeVideoView;->setBackgroundColor(I)V

    .line 250
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 251
    new-instance v1, Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    .line 252
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xd

    .line 254
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 255
    iget-object v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v2, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 256
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/component/NativeVideoView;->addView(Landroid/view/View;)V

    .line 258
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 260
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 261
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 263
    new-instance v1, Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->loadingView:Landroid/view/View;

    .line 264
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 265
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 266
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->loadingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->loadingView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/component/NativeVideoView;->addView(Landroid/view/View;)V

    .line 269
    new-instance v0, Lcom/baidu/mobads/component/f;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/component/f;-><init>(Lcom/baidu/mobads/component/NativeVideoView;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/component/NativeVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public isLooping()Z
    .locals 2

    .line 576
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isLooping()Z

    move-result v0

    return v0

    .line 579
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isPlaying()Z
    .locals 2

    .line 588
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 589
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0

    .line 591
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isShouldAutoplay()Z
    .locals 1

    .line 437
    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->shouldAutoplay:Z

    return v0
.end method

.method protected keepState(Z)V
    .locals 0

    .line 287
    iput-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->keepState:Z

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 103
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 105
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onAttach()V

    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    const/4 v0, 0x1

    .line 211
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    .line 212
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isLooping()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    .line 214
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    .line 215
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 216
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_1

    .line 217
    invoke-interface {v0, p1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onCompletion(Landroid/media/MediaPlayer;)V

    goto :goto_0

    .line 220
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->start()V

    .line 221
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 222
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {p1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStart()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 111
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 113
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onDetach()V

    .line 115
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->keepState:Z

    if-nez v0, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->release()V

    :cond_1
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->stopLoading()V

    .line 230
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->ERROR:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 231
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 232
    invoke-interface {v0, p1, p2, p3}, Lcom/baidu/mobads/component/VideoPlayCallback;->onError(Landroid/media/MediaPlayer;II)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public declared-synchronized onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    monitor-enter p0

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_0
    const/4 p1, 0x1

    .line 164
    iput-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->videoIsReady:Z

    .line 165
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->tryToPrepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 2

    .line 175
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->stopLoading()V

    .line 176
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->lastState:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eqz v0, :cond_4

    .line 177
    sget-object v0, Lcom/baidu/mobads/component/g;->a:[I

    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->lastState:Lcom/baidu/mobads/component/NativeVideoView$State;

    invoke-virtual {v1}, Lcom/baidu/mobads/component/NativeVideoView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 197
    :cond_0
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    goto :goto_0

    .line 193
    :cond_1
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    goto :goto_0

    .line 186
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->pause()V

    .line 187
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_4

    .line 188
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    goto :goto_0

    .line 179
    :cond_3
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->start()V

    .line 180
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 181
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStart()V

    .line 204
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_5

    .line 205
    invoke-interface {v0, p1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onSeekComplete(Landroid/media/MediaPlayer;)V

    :cond_5
    return-void
.end method

.method public pause()V
    .locals 2

    .line 600
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 601
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PAUSED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 602
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 603
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 604
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    :cond_0
    return-void

    .line 607
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected prepare()V
    .locals 2

    .line 342
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->startLoading()V

    const/4 v0, 0x0

    .line 343
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->videoIsReady:Z

    const/4 v0, -0x1

    .line 344
    iput v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieHeight:I

    .line 345
    iput v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieWidth:I

    .line 346
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 347
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 348
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 349
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 350
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 351
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARING:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 352
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 353
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 354
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPreparing()V

    :cond_0
    return-void
.end method

.method protected release()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    .line 311
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v1, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq v0, v1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onCloseVideo(I)V

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 317
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 318
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 319
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 321
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 323
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_1

    .line 324
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStop()V

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 328
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_2

    .line 329
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onRelease()V

    .line 331
    :cond_2
    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    :cond_3
    const/4 v0, 0x0

    .line 333
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->videoIsReady:Z

    .line 334
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceIsReady:Z

    .line 335
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->END:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    return-void
.end method

.method protected releasePlayer()V
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 292
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 293
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 294
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 295
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 296
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 300
    iput-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_2

    .line 303
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onRelease()V

    :cond_2
    const/4 v0, 0x0

    .line 305
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->videoIsReady:Z

    .line 306
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->END:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    return-void
.end method

.method public reset()V
    .locals 2

    .line 616
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 617
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->IDLE:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 618
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 619
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 620
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onRelease()V

    :cond_0
    return-void

    .line 623
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public resize()V
    .locals 6

    .line 405
    iget v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieWidth:I

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 409
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_3

    .line 411
    iget v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 413
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 414
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v3, v2

    int-to-float v4, v0

    div-float v5, v3, v4

    cmpl-float v5, v1, v5

    if-lez v5, :cond_1

    div-float/2addr v3, v1

    float-to-int v0, v3

    goto :goto_0

    :cond_1
    mul-float v1, v1, v4

    float-to-int v2, v1

    .line 426
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 427
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v3, v2, :cond_2

    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v0, :cond_3

    .line 428
    :cond_2
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 429
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 430
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public seekTo(I)V
    .locals 2

    .line 675
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 676
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 677
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->lastState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 678
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->pause()V

    .line 679
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 680
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 683
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->startLoading()V

    :cond_1
    return-void

    .line 686
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Media Player is not initialized"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->activity:Landroid/app/Activity;

    .line 401
    invoke-virtual {p1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialConfigOrientation:I

    return-void
.end method

.method public setLooping(Z)V
    .locals 1

    .line 715
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 716
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    return-void

    .line 718
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Media Player is not initialized"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 1

    .line 691
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 692
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    return-void

    .line 694
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Media Player is not initialized"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 1

    .line 699
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 700
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    return-void

    .line 702
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Media Player is not initialized"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 708
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    return-void

    .line 710
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Media Player is not initialized"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setShouldAutoplay(Z)V
    .locals 0

    .line 447
    iput-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->shouldAutoplay:Z

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 2

    .line 734
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 735
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 738
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 739
    sget-object p1, Lcom/baidu/mobads/component/NativeVideoView$State;->INITIALIZED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 740
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "FSVideoView"

    const-string v1, "set data execption."

    .line 742
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public setVideoPlayCallback(Lcom/baidu/mobads/component/VideoPlayCallback;)V
    .locals 0

    .line 765
    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 2

    .line 751
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 752
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 755
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->context:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 756
    sget-object p1, Lcom/baidu/mobads/component/NativeVideoView$State;->INITIALIZED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 757
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "FSVideoView"

    const-string v1, "set data execption."

    .line 759
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public setVolume(FF)V
    .locals 1

    .line 723
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 724
    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    return-void

    .line 726
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Media Player is not initialized"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public start()V
    .locals 2

    .line 632
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 633
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->STARTED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 634
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 635
    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 636
    iput-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->isCompleted:Z

    .line 637
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 640
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStart()V

    :cond_1
    return-void

    .line 644
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected startLoading()V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->loadingView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public stop()V
    .locals 2

    .line 653
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 654
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->STOPPED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 655
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 656
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_0

    .line 657
    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStop()V

    :cond_0
    return-void

    .line 660
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media Player is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected stopLoading()V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .line 140
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mHandler:Landroid/os/Handler;

    new-instance p2, Lcom/baidu/mobads/component/e;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/component/e;-><init>(Lcom/baidu/mobads/component/NativeVideoView;)V

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public declared-synchronized surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    monitor-enter p0

    .line 122
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez p1, :cond_0

    .line 123
    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 125
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 127
    iget-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceIsReady:Z

    if-nez p1, :cond_1

    const/4 p1, 0x1

    .line 128
    iput-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceIsReady:Z

    .line 129
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PAUSED:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->STARTED:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PLAYBACKCOMPLETED:Lcom/baidu/mobads/component/NativeVideoView$State;

    if-eq p1, v0, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->tryToPrepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 150
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->pause()V

    .line 152
    iget-object p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->getCurrentPosition()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onPause(I)V

    :cond_0
    const/4 p1, 0x0

    .line 156
    iput-boolean p1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceIsReady:Z

    return-void
.end method

.method protected tryToPrepare()V
    .locals 2

    .line 364
    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceIsReady:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->videoIsReady:Z

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 366
    iget-object v1, p0, Lcom/baidu/mobads/component/NativeVideoView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 367
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieWidth:I

    .line 368
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->initialMovieHeight:I

    .line 370
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->resize()V

    .line 371
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->stopLoading()V

    .line 372
    sget-object v0, Lcom/baidu/mobads/component/NativeVideoView$State;->PREPARED:Lcom/baidu/mobads/component/NativeVideoView$State;

    iput-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->currentState:Lcom/baidu/mobads/component/NativeVideoView$State;

    .line 373
    iget-boolean v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->shouldAutoplay:Z

    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->start()V

    .line 375
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/baidu/mobads/component/NativeVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/baidu/mobads/component/NativeVideoView;->mVideoPlayCallback:Lcom/baidu/mobads/component/VideoPlayCallback;

    invoke-interface {v0}, Lcom/baidu/mobads/component/VideoPlayCallback;->onStart()V

    :cond_1
    return-void
.end method
