.class public Lcom/baidu/mobads/a/b;
.super Lcom/baidu/mobads/openad/a/d;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private d:Lcom/baidu/mobads/a/a;

.field private e:Ljava/lang/String;

.field private f:Ljava/io/File;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Z)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/baidu/mobads/openad/a/d;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/a/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 42
    iput-object p2, p0, Lcom/baidu/mobads/a/b;->e:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/baidu/mobads/a/b;->f:Ljava/io/File;

    .line 44
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/a/b;->g:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/a/b;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "package"

    .line 56
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/baidu/mobads/a/b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/a/b;->d:Lcom/baidu/mobads/a/a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 59
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/a/b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/a/b;->f:Ljava/io/File;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/utils/o;->b(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    iget-object v0, p0, Lcom/baidu/mobads/a/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "XAdInstallController"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :goto_0
    return-void
.end method

.method public a(Landroid/content/BroadcastReceiver;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/baidu/mobads/a/a;

    iput-object p1, p0, Lcom/baidu/mobads/a/b;->d:Lcom/baidu/mobads/a/a;

    return-void
.end method
