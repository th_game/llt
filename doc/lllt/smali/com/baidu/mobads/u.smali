.class Lcom/baidu/mobads/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/XAdView$Listener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/baidu/mobads/component/XAdView;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Lcom/baidu/mobad/feeds/RequestParameters;

.field final synthetic f:Lcom/baidu/mobads/SplashAd;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/SplashAd;Landroid/content/Context;Lcom/baidu/mobads/component/XAdView;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    iput-object p2, p0, Lcom/baidu/mobads/u;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/component/XAdView;

    iput-object p4, p0, Lcom/baidu/mobads/u;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/baidu/mobads/u;->d:Z

    iput-object p6, p0, Lcom/baidu/mobads/u;->e:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/a;->n()V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/a;->o()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onLayoutComplete(II)V
    .locals 10

    .line 200
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    .line 203
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/f;->getScreenDensity(Landroid/content/Context;)F

    move-result v0

    int-to-float v1, p1

    const/high16 v2, 0x43480000    # 200.0f

    mul-float v2, v2, v0

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    int-to-float v1, p2

    const/high16 v2, 0x43160000    # 150.0f

    mul-float v0, v0, v2

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    goto/16 :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    new-instance v9, Lcom/baidu/mobads/production/g/a;

    iget-object v2, p0, Lcom/baidu/mobads/u;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/component/XAdView;

    iget-object v4, p0, Lcom/baidu/mobads/u;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/baidu/mobads/u;->d:Z

    .line 214
    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)I

    move-result v8

    move-object v1, v9

    move v6, p1

    move v7, p2

    invoke-direct/range {v1 .. v8}, Lcom/baidu/mobads/production/g/a;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZIII)V

    .line 213
    invoke-static {v0, v9}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;Lcom/baidu/mobads/production/g/a;)Lcom/baidu/mobads/production/g/a;

    .line 215
    iget-object p1, p0, Lcom/baidu/mobads/u;->e:Lcom/baidu/mobad/feeds/RequestParameters;

    if-eqz p1, :cond_2

    .line 216
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->e:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/g/a;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 218
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object p2

    const-string v0, "AdUserClick"

    invoke-virtual {p1, v0, p2}, Lcom/baidu/mobads/production/g/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 219
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object p2

    const-string v0, "AdLoaded"

    invoke-virtual {p1, v0, p2}, Lcom/baidu/mobads/production/g/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 220
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object p2

    const-string v0, "AdStarted"

    invoke-virtual {p1, v0, p2}, Lcom/baidu/mobads/production/g/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 221
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object p2

    const-string v0, "AdStopped"

    invoke-virtual {p1, v0, p2}, Lcom/baidu/mobads/production/g/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 224
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object p2

    const-string v0, "AdError"

    invoke-virtual {p1, v0, p2}, Lcom/baidu/mobads/production/g/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 225
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/production/g/a;->request()V

    return-void

    .line 206
    :cond_3
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p1

    .line 207
    sget-object p2, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->SHOW_STANDARD_UNFIT:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    const-string v0, "\u5f00\u5c4f\u663e\u793a\u533a\u57df\u592a\u5c0f,\u5bbd\u5ea6\u81f3\u5c11200dp,\u9ad8\u5ea6\u81f3\u5c11150dp"

    invoke-interface {p1, p2, v0}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->genCompleteErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 209
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    .line 210
    iget-object p1, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/SplashAdListener;->onAdDismissed()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/g/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/baidu/mobads/u;->f:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/g/a;->a(I)V

    :cond_0
    return-void
.end method
