.class public Lcom/baidu/mobads/AppActivityImp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final EXTRA_AD_INSTANCE_INFO:Ljava/lang/String; = "EXTRA_DATA_STRING_AD"

.field public static final EXTRA_COMMAND_EXTRA_INFO:Ljava/lang/String; = "EXTRA_DATA_STRING_COM"

.field public static final EXTRA_DATA:Ljava/lang/String; = "EXTRA_DATA"

.field public static final EXTRA_LANDINGPAGE_EXTRA_INFO:Ljava/lang/String; = "EXTRA_DATA_STRING"

.field public static final EXTRA_LP_THEME:Ljava/lang/String; = "theme"

.field private static b:Z = false

.field private static d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;


# instance fields
.field private a:Landroid/app/Activity;

.field private c:Ljava/lang/Object;

.field private e:[Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->e:[Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->e:[Ljava/lang/reflect/Method;

    .line 54
    iput-object p1, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 6

    .line 82
    iget-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->e:[Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 86
    :cond_0
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 88
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 p1, 0x1

    .line 89
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 512
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 513
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object p0

    .line 514
    array-length p2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_1

    aget-object v2, p0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 517
    :try_start_1
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 518
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    .line 519
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    .line 520
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v4}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 521
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 524
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/String;)I

    .line 525
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_1
    move-exception p0

    .line 529
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :cond_1
    return-void
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 120
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object p2, v1, v3

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/n;->d([Ljava/lang/Object;)I

    .line 121
    invoke-direct {p0, p1}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 123
    array-length v0, p2

    if-nez v0, :cond_1

    goto :goto_1

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 124
    :cond_2
    :goto_1
    iget-object p2, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 130
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->d(Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    return-void
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 545
    :cond_0
    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Ljava/lang/Boolean;

    .line 546
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Ljava/lang/Double;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    .line 547
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Ljava/lang/Long;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Lorg/json/JSONArray;

    .line 548
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Lorg/json/JSONObject;

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method private varargs b(Ljava/lang/String;[Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    .line 139
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/n;->d([Ljava/lang/Object;)I

    .line 140
    invoke-direct {p0, p1}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 142
    array-length v1, p2

    if-nez v1, :cond_1

    goto :goto_1

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    invoke-virtual {p1, v1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    .line 143
    :cond_2
    :goto_1
    iget-object p2, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 149
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->d(Ljava/lang/Throwable;)I

    :cond_3
    return v0
.end method

.method private varargs c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .line 159
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object p2, v1, v3

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/n;->d([Ljava/lang/Object;)I

    .line 160
    invoke-direct {p0, p1}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 162
    array-length v0, p2

    if-nez v0, :cond_1

    goto :goto_1

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 163
    :cond_2
    :goto_1
    iget-object p2, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 169
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->d(Ljava/lang/Throwable;)I

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public static canLpShowWhenLocked(Z)V
    .locals 0

    .line 72
    sput-boolean p0, Lcom/baidu/mobads/AppActivityImp;->b:Z

    return-void
.end method

.method public static classToString(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, ""

    .line 485
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 487
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object p0

    .line 488
    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, p0, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 491
    :try_start_1
    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 492
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    .line 493
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    .line 494
    invoke-static {v6}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 496
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    .line 499
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/String;)I

    .line 500
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 503
    :cond_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    .line 505
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    return-object v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "dispatchKeyEvent"

    .line 175
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "dispatchTouchEvent"

    .line 179
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "dispatchTrackballEvent"

    .line 183
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public varargs invokeRemoteStatic(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 101
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object p2, v1, v3

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/n;->d([Ljava/lang/Object;)I

    .line 102
    invoke-direct {p0, p1}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 104
    array-length v1, p2

    if-nez v1, :cond_1

    goto :goto_1

    .line 107
    :cond_1
    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    :goto_1
    new-array p2, v2, [Ljava/lang/Object;

    .line 105
    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 111
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->d(Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    return-void
.end method

.method public loadLocalApk(Ljava/lang/String;)Ljava/lang/Class;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 278
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    .line 279
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x0

    .line 282
    :try_start_0
    iget-object v3, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-static {v3}, Lcom/baidu/mobads/f/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 283
    iget-object v4, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 284
    new-instance v5, Ldalvik/system/DexClassLoader;

    invoke-direct {v5, v3, v4, v2, v1}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const/4 v1, 0x1

    .line 285
    invoke-static {p1, v1, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 287
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    .line 290
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "jar.path=, clz="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;)I

    return-object v2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 191
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const-string p1, "onActivityResult"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 195
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    const-string p1, "onApplyThemeResource"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onChildTitleChanged"

    .line 199
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onConfigurationChanged"

    .line 203
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onContentChanged()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onContentChanged"

    .line 208
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onContextItemSelected"

    .line 212
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onContextMenuClosed"

    .line 216
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const-string v0, "EXTRA_DATA"

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 223
    :try_start_0
    iget-object v3, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 225
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 227
    iget-object v4, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "theme"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 228
    const-class v5, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    sget-object v6, Lcom/baidu/mobads/AppActivityImp;->f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    invoke-static {v5, v6, v4}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "showWhenLocked"

    .line 229
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/baidu/mobads/AppActivityImp;->b:Z

    .line 231
    :cond_0
    new-instance v4, Lcom/baidu/mobads/utils/d;

    invoke-direct {v4}, Lcom/baidu/mobads/utils/d;-><init>()V

    const-string v5, "multiProcess"

    .line 232
    iget-object v6, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v4, v6}, Lcom/baidu/mobads/utils/d;->webviewMultiProcess(Landroid/app/Activity;)Z

    move-result v4

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 234
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->isAnti()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    if-nez v4, :cond_1

    .line 235
    new-instance v4, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;

    const/4 v5, 0x0

    new-instance v6, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    invoke-direct {v6, v7}, Lcom/baidu/mobads/vo/XAdInstanceInfo;-><init>(Lorg/json/JSONObject;)V

    invoke-direct {v4, v5, v6}, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    .line 237
    iget-object v5, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EXTRA_DATA_STRING"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 238
    const-class v6, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;

    invoke-static {v6, v4, v5}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    iget-object v5, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EXTRA_DATA_STRING_COM"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 241
    const-class v6, Lcom/baidu/mobads/command/XAdCommandExtraInfo;

    invoke-static {v6, v4, v5}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object v5, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EXTRA_DATA_STRING_AD"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 244
    const-class v6, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {v4}, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v7

    invoke-static {v6, v7, v5}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    const-string v0, "com.baidu.mobads.container.landingpage.App2Activity"

    .line 251
    invoke-static {}, Lcom/baidu/mobads/f/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v3

    if-nez v3, :cond_2

    .line 253
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/AppActivityImp;->loadLocalApk(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/baidu/mobads/AppActivityImp;->d:Ljava/lang/Class;

    goto :goto_0

    .line 255
    :cond_2
    invoke-static {v0, v2, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/baidu/mobads/AppActivityImp;->d:Ljava/lang/Class;

    .line 257
    :goto_0
    sget-object v3, Lcom/baidu/mobads/AppActivityImp;->d:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/baidu/mobads/AppActivityImp;->e:[Ljava/lang/reflect/Method;

    .line 260
    sget-object v3, Lcom/baidu/mobads/AppActivityImp;->d:Ljava/lang/Class;

    new-array v4, v2, [Ljava/lang/Class;

    const-class v5, Landroid/app/Activity;

    aput-object v5, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    .line 261
    iget-object v5, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    aput-object v5, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    iput-object v3, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    const-string v3, "canLpShowWhenLocked"

    new-array v4, v2, [Ljava/lang/Object;

    .line 263
    sget-boolean v5, Lcom/baidu/mobads/AppActivityImp;->b:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0, v3, v4}, Lcom/baidu/mobads/AppActivityImp;->invokeRemoteStatic(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "setActionBarColor"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    .line 264
    sget-object v5, Lcom/baidu/mobads/AppActivityImp;->f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    iget v5, v5, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    .line 265
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    sget-object v5, Lcom/baidu/mobads/AppActivityImp;->f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    iget v5, v5, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    .line 266
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    sget-object v5, Lcom/baidu/mobads/AppActivityImp;->f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    iget v5, v5, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    .line 267
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x2

    aput-object v5, v4, v6

    sget-object v5, Lcom/baidu/mobads/AppActivityImp;->f:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    iget v5, v5, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    .line 268
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v7, 0x3

    aput-object v5, v4, v7

    .line 264
    invoke-virtual {p0, v3, v4}, Lcom/baidu/mobads/AppActivityImp;->invokeRemoteStatic(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v1

    sget-object v0, Lcom/baidu/mobads/AppActivityImp;->d:Ljava/lang/Class;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->c:Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/baidu/mobads/utils/n;->d([Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 272
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :goto_1
    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string p1, "onCreate"

    .line 274
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const-string p1, "onCreateContextMenu"

    .line 296
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onCreateDescription"

    .line 300
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 309
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onCreateDialog"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Dialog;

    if-eqz p1, :cond_0

    :cond_0
    return-object p1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onCreateOptionsMenu"

    .line 317
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 321
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onCreatePanelMenu"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 3

    :try_start_0
    const-string v0, "onCreatePanelView"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 327
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/AppActivityImp;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 329
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onCreateThumbnail"

    .line 335
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 3

    :try_start_0
    const-string v0, "onCreateView"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    aput-object p3, v1, p1

    .line 340
    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/AppActivityImp;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 342
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 350
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "lp_close"

    .line 351
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    iget-object v1, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onDestroy"

    .line 354
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 358
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onKeyDown"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 362
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const-string p1, "onKeyMultiple"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 366
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onKeyUp"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onLowMemory()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onLowMemory"

    .line 370
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 375
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onMenuItemSelected"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 379
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onMenuOpened"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onNewIntent"

    .line 383
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onOptionsItemSelected"

    .line 387
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onOptionsMenuClosed"

    .line 391
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 395
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onPanelClosed"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onPause"

    .line 399
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onPostCreate"

    .line 403
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onPostResume()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onPostResume"

    .line 407
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 411
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "onPrepareDialog"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onPrepareOptionsMenu"

    .line 415
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 419
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const-string p1, "onPreparePanel"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected onRestart()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onRestart"

    .line 423
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onRestoreInstanceState"

    .line 427
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onResume"

    .line 431
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onRetainNonConfigurationInstance"

    .line 435
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onSaveInstanceState"

    .line 441
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onSearchRequested"

    .line 445
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onStart"

    .line 449
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onStop"

    .line 453
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 457
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const-string p1, "onTitleChanged"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onTouchEvent"

    .line 461
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onTrackballEvent"

    .line 465
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onUserInteraction()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onUserInteraction"

    .line 469
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onUserLeaveHint"

    .line 473
    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onWindowAttributesChanged"

    .line 477
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 481
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onWindowFocusChanged"

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/AppActivityImp;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/baidu/mobads/AppActivityImp;->a:Landroid/app/Activity;

    return-void
.end method
