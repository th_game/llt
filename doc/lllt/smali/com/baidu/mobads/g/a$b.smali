.class public Lcom/baidu/mobads/g/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/g/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/baidu/mobads/g/a;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/g/a;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation

    .line 387
    iput-object p1, p0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 384
    iput-object p1, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    const-wide/16 v0, 0x0

    .line 385
    iput-wide v0, p0, Lcom/baidu/mobads/g/a$b;->c:J

    .line 388
    iput-object p2, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    .line 389
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/baidu/mobads/g/a$b;->c:J

    .line 390
    invoke-direct {p0}, Lcom/baidu/mobads/g/a$b;->b()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/a$b;)Z
    .locals 0

    .line 383
    invoke-direct {p0}, Lcom/baidu/mobads/g/a$b;->c()Z

    move-result p0

    return p0
.end method

.method private b()V
    .locals 7

    .line 454
    invoke-virtual {p0}, Lcom/baidu/mobads/g/a$b;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    const/4 v2, 0x1

    :goto_0
    if-lt v0, v1, :cond_3

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    .line 463
    iget-object v4, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/ScanResult;

    iget v4, v4, Landroid/net/wifi/ScanResult;->level:I

    .line 464
    iget-object v5, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    add-int/lit8 v6, v2, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/ScanResult;

    iget v5, v5, Landroid/net/wifi/ScanResult;->level:I

    if-ge v4, v5, :cond_1

    .line 466
    iget-object v3, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/ScanResult;

    .line 467
    iget-object v4, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 468
    iget-object v4, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v4, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    :cond_1
    move v2, v6

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    move v2, v3

    goto :goto_0

    :cond_3
    return-void
.end method

.method private c()Z
    .locals 5

    .line 476
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobads/g/a$b;->c:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    const-wide/16 v2, 0x1f4

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 397
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 16

    move-object/from16 v0, p0

    .line 402
    invoke-virtual/range {p0 .. p0}, Lcom/baidu/mobads/g/a$b;->a()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ge v1, v3, :cond_0

    return-object v2

    .line 406
    :cond_0
    iget-object v1, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v1}, Lcom/baidu/mobads/g/a;->a(Lcom/baidu/mobads/g/a;)Z

    move-result v1

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    add-int/lit8 v5, p1, -0x1

    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p1

    const/4 v6, 0x1

    .line 411
    :goto_0
    new-instance v7, Ljava/lang/StringBuffer;

    const/16 v8, 0x200

    invoke-direct {v7, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 412
    iget-object v8, v0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    move v10, v6

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x1

    :goto_1
    const-string v12, "h"

    if-ge v6, v8, :cond_6

    .line 416
    iget-object v13, v0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/wifi/ScanResult;

    iget v13, v13, Landroid/net/wifi/ScanResult;->level:I

    if-nez v13, :cond_2

    goto :goto_2

    .line 419
    :cond_2
    iget-object v13, v0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/wifi/ScanResult;

    iget-object v13, v13, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    .line 420
    iget-object v14, v0, Lcom/baidu/mobads/g/a$b;->a:Ljava/util/List;

    invoke-interface {v14, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/wifi/ScanResult;

    iget v14, v14, Landroid/net/wifi/ScanResult;->level:I

    const-string v15, ":"

    const-string v2, ""

    .line 421
    invoke-virtual {v13, v15, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 422
    iget-object v13, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v13}, Lcom/baidu/mobads/g/a;->b(Lcom/baidu/mobads/g/a;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    iget-object v13, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v13}, Lcom/baidu/mobads/g/a;->b(Lcom/baidu/mobads/g/a;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 423
    iget-object v2, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v14}, Ljava/lang/StrictMath;->abs(I)I

    move-result v10

    invoke-static {v2, v10}, Lcom/baidu/mobads/g/a;->a(Lcom/baidu/mobads/g/a;I)I

    const/4 v10, 0x1

    goto :goto_2

    :cond_3
    if-ge v9, v5, :cond_4

    .line 428
    invoke-virtual {v7, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 429
    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "m"

    .line 430
    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    invoke-static {v14}, Ljava/lang/StrictMath;->abs(I)I

    move-result v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    add-int/lit8 v9, v9, 0x1

    const/4 v11, 0x0

    :cond_4
    if-le v9, v5, :cond_5

    if-eqz v10, :cond_5

    goto :goto_3

    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    :goto_3
    if-eqz v1, :cond_7

    .line 443
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v2}, Lcom/baidu/mobads/g/a;->b(Lcom/baidu/mobads/g/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/baidu/mobads/g/a$b;->b:Lcom/baidu/mobads/g/a;

    invoke-static {v2}, Lcom/baidu/mobads/g/a;->c(Lcom/baidu/mobads/g/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    :goto_4
    if-nez v11, :cond_8

    .line 447
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_8
    return-object v2
.end method
