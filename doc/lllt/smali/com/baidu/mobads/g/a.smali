.class public Lcom/baidu/mobads/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/g/a$b;,
        Lcom/baidu/mobads/g/a$a;
    }
.end annotation


# static fields
.field private static d:Ljava/lang/reflect/Method;

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/reflect/Method;

.field private static g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static n:[C


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/telephony/TelephonyManager;

.field private c:Lcom/baidu/mobads/g/a$a;

.field private h:Landroid/net/wifi/WifiManager;

.field private i:Lcom/baidu/mobads/g/a$b;

.field private j:J

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."

    .line 48
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/g/a;->n:[C

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->a:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    .line 36
    new-instance v1, Lcom/baidu/mobads/g/a$a;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/g/a$a;-><init>(Lcom/baidu/mobads/g/a;Lcom/baidu/mobads/g/b;)V

    iput-object v1, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    .line 42
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->h:Landroid/net/wifi/WifiManager;

    .line 43
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->i:Lcom/baidu/mobads/g/a$b;

    const-wide/16 v1, 0x0

    .line 44
    iput-wide v1, p0, Lcom/baidu/mobads/g/a;->j:J

    .line 45
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->k:Ljava/lang/String;

    const/4 v1, 0x0

    .line 46
    iput v1, p0, Lcom/baidu/mobads/g/a;->l:I

    .line 47
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->m:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/g/a;->a:Landroid/content/Context;

    .line 52
    iget-object p1, p0, Lcom/baidu/mobads/g/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    .line 55
    :try_start_0
    iget-object v2, p0, Lcom/baidu/mobads/g/a;->a:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iput-object v2, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    .line 57
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v2

    const-string v3, "uvNYwANvpyP-iyfb"

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IBase64;->decodeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 58
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v3

    .line 59
    iget-object v4, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2, v1}, Lcom/baidu/mobads/utils/f;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 66
    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/g/a;->m:Ljava/lang/String;

    .line 68
    iget-object p1, p0, Lcom/baidu/mobads/g/a;->a:Landroid/content/Context;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/baidu/mobads/g/a;->h:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/a;I)I
    .locals 0

    .line 30
    iput p1, p0, Lcom/baidu/mobads/g/a;->l:I

    return p1
.end method

.method private a(Landroid/telephony/CellInfo;)Lcom/baidu/mobads/g/a$a;
    .locals 6

    .line 265
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x11

    if-ge v0, v2, :cond_0

    return-object v1

    .line 270
    :cond_0
    new-instance v2, Lcom/baidu/mobads/g/a$a;

    invoke-direct {v2, p0, v1}, Lcom/baidu/mobads/g/a$a;-><init>(Lcom/baidu/mobads/g/a;Lcom/baidu/mobads/g/b;)V

    const/4 v1, 0x0

    .line 272
    instance-of v3, p1, Landroid/telephony/CellInfoGsm;

    const/4 v4, 0x1

    const/16 v5, 0x67

    if-eqz v3, :cond_1

    .line 274
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->c:I

    .line 280
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->d:I

    .line 281
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->a:I

    .line 282
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/g/a$a;->b:I

    .line 283
    iput-char v5, v2, Lcom/baidu/mobads/g/a$a;->e:C

    goto :goto_0

    .line 285
    :cond_1
    instance-of v3, p1, Landroid/telephony/CellInfoCdma;

    if-eqz v3, :cond_2

    .line 287
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v1

    .line 294
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->d:I

    .line 295
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->a:I

    .line 296
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/g/a$a;->b:I

    const/16 v1, 0x77

    .line 297
    iput-char v1, v2, Lcom/baidu/mobads/g/a$a;->e:C

    goto :goto_0

    .line 300
    :cond_2
    instance-of v3, p1, Landroid/telephony/CellInfoLte;

    if-eqz v3, :cond_3

    .line 302
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoLte;

    invoke-virtual {v1}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v1

    .line 307
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->c:I

    .line 308
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->d:I

    .line 309
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/g/a$a;->a:I

    .line 310
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/g/a$a;->b:I

    .line 311
    iput-char v5, v2, Lcom/baidu/mobads/g/a$a;->e:C

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_4

    if-nez v4, :cond_4

    .line 317
    :try_start_0
    instance-of v0, p1, Landroid/telephony/CellInfoWcdma;

    if-eqz v0, :cond_4

    .line 320
    check-cast p1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {p1}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object p1

    .line 325
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/g/a$a;->c:I

    .line 326
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/g/a$a;->d:I

    .line 327
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/g/a$a;->a:I

    .line 328
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/a;->b(I)I

    move-result p1

    iput p1, v2, Lcom/baidu/mobads/g/a$a;->b:I

    .line 329
    iput-char v5, v2, Lcom/baidu/mobads/g/a$a;->e:C
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-object v2
.end method

.method private a(I)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    const/4 p1, 0x3

    :cond_0
    const/4 v0, 0x0

    .line 100
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/g/a;->b()Lcom/baidu/mobads/g/a$a;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 101
    invoke-static {v1}, Lcom/baidu/mobads/g/a$a;->a(Lcom/baidu/mobads/g/a$a;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 107
    :cond_1
    iput-object v1, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    goto :goto_1

    .line 102
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/a;->a(Landroid/telephony/CellLocation;)V

    .line 109
    :goto_1
    iget-object v1, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/g/a$a;->a()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-object v1, v0

    :goto_2
    const-string v2, "Z"

    if-nez v1, :cond_3

    move-object v1, v2

    .line 123
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/baidu/mobads/g/a;->i:Lcom/baidu/mobads/g/a$b;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/baidu/mobads/g/a;->i:Lcom/baidu/mobads/g/a$b;

    invoke-static {v3}, Lcom/baidu/mobads/g/a$b;->a(Lcom/baidu/mobads/g/a$b;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 124
    :cond_4
    new-instance v3, Lcom/baidu/mobads/g/a$b;

    iget-object v4, p0, Lcom/baidu/mobads/g/a;->h:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/baidu/mobads/g/a$b;-><init>(Lcom/baidu/mobads/g/a;Ljava/util/List;)V

    iput-object v3, p0, Lcom/baidu/mobads/g/a;->i:Lcom/baidu/mobads/g/a$b;

    .line 126
    :cond_5
    iget-object v3, p0, Lcom/baidu/mobads/g/a;->i:Lcom/baidu/mobads/g/a$b;

    invoke-virtual {v3, p1}, Lcom/baidu/mobads/g/a$b;->a(I)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-object p1, v0

    :goto_3
    if-eqz p1, :cond_6

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    :cond_6
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    return-object v0

    .line 142
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "t"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/g/a;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/g/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 556
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    .line 558
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    int-to-byte v0, v0

    .line 559
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-byte v1, v1

    .line 560
    array-length v2, p0

    add-int/lit8 v2, v2, 0x2

    new-array v2, v2, [B

    .line 562
    array-length v3, p0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-byte v6, p0, v4

    add-int/lit8 v7, v5, 0x1

    xor-int/2addr v6, v0

    int-to-byte v6, v6

    .line 563
    aput-byte v6, v2, v5

    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto :goto_0

    :cond_1
    add-int/lit8 p0, v5, 0x1

    .line 565
    aput-byte v0, v2, v5

    .line 566
    aput-byte v1, v2, p0

    .line 567
    invoke-static {v2}, Lcom/baidu/mobads/g/a;->a([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 10

    .line 571
    array-length v0, p0

    add-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x4

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 572
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_4

    .line 575
    aget-byte v4, p0, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v2, 0x1

    .line 577
    array-length v6, p0

    const/4 v7, 0x1

    if-ge v5, v6, :cond_0

    .line 578
    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v6, v2, 0x2

    .line 582
    array-length v8, p0

    if-ge v6, v8, :cond_1

    .line 583
    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v4, v6

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    :goto_2
    add-int/lit8 v6, v3, 0x3

    .line 586
    sget-object v8, Lcom/baidu/mobads/g/a;->n:[C

    const/16 v9, 0x40

    if-eqz v7, :cond_2

    and-int/lit8 v7, v4, 0x3f

    rsub-int/lit8 v7, v7, 0x3f

    goto :goto_3

    :cond_2
    const/16 v7, 0x40

    :goto_3
    aget-char v7, v8, v7

    aput-char v7, v0, v6

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v6, v3, 0x2

    .line 588
    sget-object v7, Lcom/baidu/mobads/g/a;->n:[C

    if-eqz v5, :cond_3

    and-int/lit8 v5, v4, 0x3f

    rsub-int/lit8 v9, v5, 0x3f

    :cond_3
    aget-char v5, v7, v9

    aput-char v5, v0, v6

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v3, 0x1

    .line 590
    sget-object v6, Lcom/baidu/mobads/g/a;->n:[C

    and-int/lit8 v7, v4, 0x3f

    rsub-int/lit8 v7, v7, 0x3f

    aget-char v7, v6, v7

    aput-char v7, v0, v5

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v3, 0x0

    and-int/lit8 v4, v4, 0x3f

    rsub-int/lit8 v4, v4, 0x3f

    .line 592
    aget-char v4, v6, v4

    aput-char v4, v0, v5

    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v3, v3, 0x4

    goto :goto_0

    .line 594
    :cond_4
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method private a(Landroid/telephony/CellLocation;)V
    .locals 7

    if-eqz p1, :cond_c

    .line 146
    iget-object v0, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    goto/16 :goto_5

    .line 149
    :cond_0
    new-instance v0, Lcom/baidu/mobads/g/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/g/a$a;-><init>(Lcom/baidu/mobads/g/a;Lcom/baidu/mobads/g/b;)V

    .line 150
    iget-object v2, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_7

    .line 151
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_7

    .line 156
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_2

    .line 157
    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gez v4, :cond_1

    .line 158
    iget-object v4, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    iget v4, v4, Lcom/baidu/mobads/g/a$a;->c:I

    :cond_1
    iput v4, v0, Lcom/baidu/mobads/g/a$a;->c:I

    .line 160
    :cond_2
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 162
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    const/4 v5, 0x0

    .line 163
    :goto_0
    array-length v6, v4

    if-ge v5, v6, :cond_5

    .line 164
    aget-char v6, v4, v5

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    .line 169
    :cond_5
    :goto_1
    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_6

    .line 170
    iget-object v2, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    iget v2, v2, Lcom/baidu/mobads/g/a$a;->d:I

    :cond_6
    iput v2, v0, Lcom/baidu/mobads/g/a$a;->d:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    nop

    .line 178
    :cond_7
    :goto_2
    instance-of v2, p1, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_8

    .line 179
    check-cast p1, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v1

    iput v1, v0, Lcom/baidu/mobads/g/a$a;->a:I

    .line 180
    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result p1

    iput p1, v0, Lcom/baidu/mobads/g/a$a;->b:I

    const/16 p1, 0x67

    .line 181
    iput-char p1, v0, Lcom/baidu/mobads/g/a$a;->e:C

    goto/16 :goto_4

    .line 182
    :cond_8
    instance-of v2, p1, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_b

    const/16 v2, 0x77

    .line 183
    iput-char v2, v0, Lcom/baidu/mobads/g/a$a;->e:C

    .line 184
    sget-object v2, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    if-nez v2, :cond_9

    :try_start_1
    const-string v2, "android.telephony.cdma.CdmaCellLocation"

    .line 186
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    .line 187
    sget-object v2, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    const-string v4, "getBaseStationId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/g/a;->d:Ljava/lang/reflect/Method;

    .line 188
    sget-object v2, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    const-string v4, "getNetworkId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/g/a;->e:Ljava/lang/reflect/Method;

    .line 189
    sget-object v2, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    const-string v4, "getSystemId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/g/a;->f:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 191
    :catch_1
    sput-object v1, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    return-void

    .line 195
    :cond_9
    :goto_3
    sget-object v1, Lcom/baidu/mobads/g/a;->g:Ljava/lang/Class;

    if-eqz v1, :cond_b

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 198
    :try_start_2
    sget-object v1, Lcom/baidu/mobads/g/a;->f:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 199
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_a

    .line 200
    iget-object v1, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    iget v1, v1, Lcom/baidu/mobads/g/a$a;->d:I

    :cond_a
    iput v1, v0, Lcom/baidu/mobads/g/a$a;->d:I

    .line 201
    sget-object v1, Lcom/baidu/mobads/g/a;->d:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 202
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/baidu/mobads/g/a$a;->b:I

    .line 203
    sget-object v1, Lcom/baidu/mobads/g/a;->e:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 204
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, v0, Lcom/baidu/mobads/g/a$a;->a:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    :catch_2
    return-void

    .line 211
    :cond_b
    :goto_4
    invoke-static {v0}, Lcom/baidu/mobads/g/a$a;->a(Lcom/baidu/mobads/g/a$a;)Z

    move-result p1

    if-eqz p1, :cond_c

    .line 212
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->c:Lcom/baidu/mobads/g/a$a;

    :cond_c
    :goto_5
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/a;)Z
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/baidu/mobads/g/a;->c()Z

    move-result p0

    return p0
.end method

.method private b(I)I
    .locals 1

    const v0, 0x7fffffff

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    :cond_0
    return p1
.end method

.method private b()Lcom/baidu/mobads/g/a$a;
    .locals 5

    .line 218
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x11

    if-ge v0, v2, :cond_0

    return-object v1

    .line 223
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/g/a;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 224
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 226
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-object v2, v1

    :cond_1
    :goto_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/CellInfo;

    .line 227
    invoke-virtual {v3}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 228
    invoke-direct {p0, v3}, Lcom/baidu/mobads/g/a;->a(Landroid/telephony/CellInfo;)Lcom/baidu/mobads/g/a$a;

    move-result-object v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 232
    :cond_2
    invoke-static {v2}, Lcom/baidu/mobads/g/a$a;->a(Lcom/baidu/mobads/g/a$a;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    move-object v1, v2

    :goto_1
    return-object v1

    :catch_0
    :cond_4
    move-object v1, v2

    :catch_1
    :cond_5
    return-object v1
.end method

.method static synthetic b(Lcom/baidu/mobads/g/a;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/baidu/mobads/g/a;->k:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/g/a;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/baidu/mobads/g/a;->l:I

    return p0
.end method

.method private c()Z
    .locals 4

    const/4 v0, 0x0

    .line 527
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->k:Ljava/lang/String;

    const/4 v1, 0x0

    .line 528
    iput v1, p0, Lcom/baidu/mobads/g/a;->l:I

    .line 529
    iget-object v2, p0, Lcom/baidu/mobads/g/a;->h:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-nez v2, :cond_0

    return v1

    .line 534
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v0, ":"

    const-string v3, ""

    .line 537
    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 539
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_2

    return v1

    .line 542
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/baidu/mobads/g/a;->k:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    return v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const/16 v0, 0xa

    .line 74
    :try_start_0
    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/a;->a(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method
