.class Lcom/baidu/mobads/production/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/f/g$c;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/b;)V
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/baidu/mobads/production/h;->a:Lcom/baidu/mobads/production/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 6

    const-string v0, "AdError"

    const-string v1, "error_message"

    if-eqz p1, :cond_0

    .line 464
    :try_start_0
    sget-object v2, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    if-eqz v2, :cond_0

    .line 465
    sget-object v2, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    invoke-virtual {v2}, Lcom/baidu/mobads/f/g;->h()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    .line 466
    sget-object v2, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    if-eqz v2, :cond_0

    .line 467
    iget-object p1, p0, Lcom/baidu/mobads/production/h;->a:Lcom/baidu/mobads/production/b;

    invoke-static {p1}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/production/b;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 473
    sput-object v2, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    .line 474
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 475
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u56de\u8c03onLoad,succcess="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    iget-object p1, p0, Lcom/baidu/mobads/production/h;->a:Lcom/baidu/mobads/production/b;

    new-instance v3, Lcom/baidu/mobads/e/a;

    invoke-direct {v3, v0, v2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, v3}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 478
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 479
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "async apk on load exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    iget-object v1, p0, Lcom/baidu/mobads/production/h;->a:Lcom/baidu/mobads/production/b;

    new-instance v3, Lcom/baidu/mobads/e/a;

    invoke-direct {v3, v0, v2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {v1, v3}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 481
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    .line 482
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
