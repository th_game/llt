.class Lcom/baidu/mobads/production/g/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/g/a;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/g/a;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2

    .line 380
    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 382
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    const-string v1, "XAbstractAdProdTemplate"

    if-ne p2, v0, :cond_1

    const-string p1, "download complete"

    .line 383
    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    if-ne p1, p2, :cond_0

    .line 385
    iget-object p1, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    invoke-static {v0}, Lcom/baidu/mobads/production/g/a;->a(Lcom/baidu/mobads/production/g/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    invoke-static {v0}, Lcom/baidu/mobads/production/g/a;->b(Lcom/baidu/mobads/production/g/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/baidu/mobads/production/g/a;->a(Lcom/baidu/mobads/production/g/a;Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :cond_0
    new-instance p1, Landroid/os/Handler;

    iget-object p2, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    invoke-static {p2}, Lcom/baidu/mobads/production/g/a;->c(Lcom/baidu/mobads/production/g/a;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p2, Lcom/baidu/mobads/production/g/c;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/production/g/c;-><init>(Lcom/baidu/mobads/production/g/b;)V

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 394
    :cond_1
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p1, p2, :cond_2

    const-string p1, "download error"

    .line 395
    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    iget-object p1, p0, Lcom/baidu/mobads/production/g/b;->a:Lcom/baidu/mobads/production/g/a;

    const-string p2, "\u5f00\u5c4f\u56e0\u4e3a3d\u80cc\u666f\u56fe\u7247\u4e0b\u8f7d\u5931\u8d25\u8df3\u8fc7"

    invoke-static {p1, p2}, Lcom/baidu/mobads/production/g/a;->b(Lcom/baidu/mobads/production/g/a;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method
