.class public Lcom/baidu/mobads/production/g/a;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"


# static fields
.field private static E:Z = false

.field private static F:I


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private G:Ljava/util/Observer;

.field protected final x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private y:Lcom/baidu/mobads/production/g/d;

.field private z:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZII)V
    .locals 2

    .line 79
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 59
    iput-boolean v0, p0, Lcom/baidu/mobads/production/g/a;->A:Z

    .line 70
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/g/a;->x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 376
    new-instance v0, Lcom/baidu/mobads/production/g/b;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/production/g/b;-><init>(Lcom/baidu/mobads/production/g/a;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/g/a;->G:Ljava/util/Observer;

    .line 81
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/g/a;->setId(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->setActivity(Landroid/content/Context;)V

    .line 84
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/g/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 86
    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p2, p0, Lcom/baidu/mobads/production/g/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 88
    iput-object p1, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    .line 89
    new-instance p2, Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p2, v0, v1}, Lcom/baidu/mobads/production/g/d;-><init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p2, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 90
    iget-object p2, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p2, p4}, Lcom/baidu/mobads/production/g/d;->a(Z)V

    .line 92
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p2

    if-eqz p4, :cond_1

    .line 96
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 97
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    invoke-static {p1}, Lcom/baidu/mobads/utils/o;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 101
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/utils/f;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object p1

    .line 108
    :goto_0
    iget-object p2, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/production/g/d;->b(Ljava/lang/String;)V

    .line 109
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p1, p5}, Lcom/baidu/mobads/production/g/d;->d(I)V

    .line 110
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p1, p6}, Lcom/baidu/mobads/production/g/d;->e(I)V

    .line 112
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/g/d;->d(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/g/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZIII)V
    .locals 0

    .line 74
    invoke-direct/range {p0 .. p6}, Lcom/baidu/mobads/production/g/a;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZII)V

    .line 75
    iput p7, p0, Lcom/baidu/mobads/production/g/a;->D:I

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/g/a;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/baidu/mobads/production/g/a;->B:Ljava/lang/String;

    return-object p0
.end method

.method private a(Landroid/os/Handler;)V
    .locals 0

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/g/a;Ljava/lang/String;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/g/a;->f(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/io/File;Ljava/net/URL;)Z
    .locals 5

    .line 403
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    :try_start_0
    invoke-virtual {p2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p2

    check-cast p2, Ljava/net/HttpURLConnection;

    const-string v0, "HEAD"

    .line 407
    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v0, "content-length"

    .line 408
    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 409
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->disconnect()V

    if-lez v0, :cond_0

    .line 410
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v1

    int-to-long v3, v0

    cmp-long p1, v1, v3

    if-nez p1, :cond_0

    .line 411
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 415
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic b(Lcom/baidu/mobads/production/g/a;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/baidu/mobads/production/g/a;->C:Ljava/lang/String;

    return-object p0
.end method

.method public static b(I)V
    .locals 0

    .line 430
    sput p0, Lcom/baidu/mobads/production/g/a;->F:I

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/production/g/a;Ljava/lang/String;)V
    .locals 0

    .line 51
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/baidu/mobads/production/g/a;)Landroid/content/Context;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    return-object p0
.end method

.method private f(Ljava/lang/String;)V
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setSplash3DLocalUrl(Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast p1, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {p1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getLocalCreativeURL()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 370
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "zip pic no download"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string p1, "splash back pic ready"

    .line 372
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->b(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static r()Z
    .locals 1

    .line 422
    sget-boolean v0, Lcom/baidu/mobads/production/g/a;->E:Z

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 342
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 345
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 346
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    .line 347
    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->getStoreagePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/production/g/a;->B:Ljava/lang/String;

    .line 348
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v1

    const-string v2, "http://mobads.baidu.com/ads/img/3d_bg.jpg"

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/f;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/production/g/a;->C:Ljava/lang/String;

    .line 349
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/mobads/production/g/a;->B:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/baidu/mobads/production/g/a;->C:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 351
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 353
    invoke-direct {p0, v1, v6}, Lcom/baidu/mobads/production/g/a;->a(Ljava/io/File;Ljava/net/URL;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setSplash3DLocalUrl(Ljava/lang/String;)V

    .line 355
    new-instance v0, Lcom/baidu/mobads/openad/b/f;

    iget-object v5, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    iget-object v7, p0, Lcom/baidu/mobads/production/g/a;->B:Ljava/lang/String;

    iget-object v8, p0, Lcom/baidu/mobads/production/g/a;->C:Ljava/lang/String;

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/baidu/mobads/openad/b/f;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 357
    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->G:Ljava/util/Observer;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->addObserver(Ljava/util/Observer;)V

    .line 358
    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->start()V

    goto :goto_0

    .line 360
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/g/a;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 363
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/b;

    .line 140
    invoke-virtual {v0}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string v2, "needRequestVR"

    .line 145
    sget-boolean v3, Lcom/baidu/mobads/production/g/a;->E:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "bitmapDisplayMode"

    .line 146
    sget v3, Lcom/baidu/mobads/production/g/a;->F:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "countDownNew"

    const/4 v3, 0x1

    .line 148
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 150
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 152
    :goto_0
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    int-to-double v0, p3

    .line 153
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/u;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method public a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 7

    .line 290
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/g/a;->d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 293
    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file_exist_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const-string v3, "383"

    move-object v4, p2

    .line 291
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    if-nez p1, :cond_0

    const-string p1, "\u5f00\u5c4f\u56e0\u4e3a\u8bf7\u6c42\u5230\u672a\u5728wifi\u4e0b\u7f13\u5b58\u7684\u89c6\u9891\u5e7f\u544a\u8df3\u8fc7"

    .line 295
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    .line 302
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p1

    return p1
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;
    .locals 1

    .line 324
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 327
    :cond_0
    invoke-super {p0, p1}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 7

    const/4 v0, 0x1

    .line 239
    iput-boolean v0, p0, Lcom/baidu/mobads/production/g/a;->A:Z

    .line 241
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 244
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    :cond_0
    move-object v4, v1

    .line 246
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    const-string v3, "382"

    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 247
    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v5

    const/4 v6, 0x0

    .line 246
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 249
    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->r:J

    .line 160
    :try_start_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    const-string p1, "m_new_rsplash"

    .line 161
    sget-wide v0, Lcom/baidu/mobads/constants/a;->l:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_request"

    .line 162
    sget-wide v0, Lcom/baidu/mobads/constants/a;->m:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_request"

    .line 163
    sget-wide v0, Lcom/baidu/mobads/constants/a;->n:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_dex"

    .line 164
    sget-wide v0, Lcom/baidu/mobads/constants/a;->o:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_dex"

    .line 165
    sget-wide v0, Lcom/baidu/mobads/constants/a;->p:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_load"

    .line 166
    sget-wide v0, Lcom/baidu/mobads/constants/a;->q:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_load"

    .line 167
    sget-wide v0, Lcom/baidu/mobads/constants/a;->r:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "isRequestAndLoadAdTimeout"

    .line 168
    iget-boolean v0, p0, Lcom/baidu/mobads/production/g/a;->A:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 173
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    move-object v5, p1

    goto :goto_0

    :cond_0
    move-object v5, v0

    .line 175
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getAttribute()Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_1

    .line 177
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :try_start_1
    const-string v0, "splashTipStyle"

    .line 180
    iget v1, p0, Lcom/baidu/mobads/production/g/a;->D:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    .line 182
    :try_start_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 184
    :goto_1
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/b;

    .line 185
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 186
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    const-string v4, "386"

    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 187
    invoke-virtual {p1}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v6

    .line 186
    invoke-virtual/range {v2 .. v7}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 189
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    .line 191
    :goto_2
    iget-boolean p1, p0, Lcom/baidu/mobads/production/g/a;->A:Z

    if-eqz p1, :cond_2

    return-void

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->start()V

    .line 201
    new-instance p1, Landroid/os/Handler;

    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 202
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdLoaded"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/g/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    if-nez p2, :cond_3

    .line 206
    :try_start_3
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/g/a;->a(Landroid/os/Handler;)V

    goto :goto_3

    :cond_3
    const-string v0, "AdInstance"

    .line 208
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v3, p2

    check-cast v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 209
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p2

    .line 210
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p2, v0, :cond_4

    .line 211
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/g/a;->a(Landroid/os/Handler;)V

    goto :goto_3

    .line 213
    :cond_4
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    const-string v2, "383"

    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 215
    invoke-virtual {p1}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p1, 0x1

    new-array v5, p1, [Ljava/lang/Object;

    const/4 p1, 0x0

    const-string p2, "processAdLoaded"

    aput-object p2, v5, p1

    .line 213
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 219
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_3
    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_1

    const-string p1, "AdInstance"

    .line 256
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 257
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    .line 258
    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p1, p2, :cond_0

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, p2, :cond_1

    .line 259
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 261
    invoke-virtual {p1}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p1, 0x1

    new-array v5, p1, [Ljava/lang/Object;

    const/4 p1, 0x0

    const-string p2, "processAdStart"

    aput-object p2, v5, p1

    const-string v2, "383"

    .line 259
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 307
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    if-eqz p2, :cond_0

    :try_start_0
    const-string p1, "video_close_reason"

    .line 310
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 311
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 312
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->z:Landroid/content/Context;

    const-string v2, "383"

    const/4 v3, 0x0

    iget-object p2, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    .line 314
    invoke-virtual {p2}, Lcom/baidu/mobads/production/g/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p2, 0x2

    new-array v5, p2, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string v6, "closead"

    aput-object v6, v5, p2

    const/4 p2, 0x1

    aput-object p1, v5, p2

    .line 312
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 318
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    .line 333
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/a;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public g()V
    .locals 2

    .line 273
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->q:J

    .line 274
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    goto :goto_0

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->x:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "container is null"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->q()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x1068

    .line 118
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public q()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    return-object v0
.end method

.method public request()V
    .locals 7

    .line 123
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->m:J

    .line 124
    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->m()V

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/production/g/a;->y:Lcom/baidu/mobads/production/g/d;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/g/a;->a(Lcom/baidu/mobads/vo/d;)Z

    .line 130
    :try_start_0
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    const-string v3, ""

    const-string v4, "text/html"

    const-string v5, "UTF-8"

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 132
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method
