.class public Lcom/baidu/mobads/production/g/d;
.super Lcom/baidu/mobads/vo/d;
.source "SourceFile"


# instance fields
.field a:Lcom/baidu/mobads/utils/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, v0, p2}, Lcom/baidu/mobads/vo/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    .line 18
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/g/d;->a:Lcom/baidu/mobads/utils/i;

    .line 23
    iget-object p1, p0, Lcom/baidu/mobads/production/g/d;->i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    const-string p2, "http://mobads.baidu.com/cpro/ui/mads.php"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/g/d;->b:Ljava/lang/String;

    const/4 p1, 0x1

    .line 25
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/d;->g(I)V

    .line 28
    iget-object p1, p0, Lcom/baidu/mobads/production/g/d;->a:Lcom/baidu/mobads/utils/i;

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeImage()I

    move-result p1

    iget-object p2, p0, Lcom/baidu/mobads/production/g/d;->a:Lcom/baidu/mobads/utils/i;

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeVideo()I

    move-result p2

    add-int/2addr p1, p2

    iget-object p2, p0, Lcom/baidu/mobads/production/g/d;->a:Lcom/baidu/mobads/utils/i;

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeRichmedia()I

    move-result p2

    add-int/2addr p1, p2

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/d;->i(I)V

    const/16 p1, 0x8

    .line 33
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/d;->f(I)V

    const/4 p1, 0x0

    .line 35
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/g/d;->h(I)V

    return-void
.end method

.method private c()Z
    .locals 3

    .line 59
    invoke-static {}, Lcom/baidu/mobads/production/g/a;->r()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 63
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/g/d;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/production/a;->a(Landroid/content/Context;)Ldalvik/system/DexClassLoader;

    move-result-object v0

    const-string v2, "com.baidu.mobads_vr.vrplayer.VrImageView"

    .line 64
    invoke-static {v2, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    const-string v2, "com.baidu.mobads_vr.vrplayer.VrImageView$OnGestureListener"

    .line 66
    invoke-static {v2, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    const-string v2, "com.baidu.mobads_vr.vrplayer.VrImageView$OnBitmapLoadedListener"

    .line 68
    invoke-static {v2, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    :catch_0
    move-exception v0

    .line 73
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/baidu/mobads/utils/n;->d(Ljava/lang/Throwable;)I

    return v1
.end method


# virtual methods
.method protected a()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "fet"

    const-string v2, "ANTI,HTML,MSSP,VIDEO,RSPLASHHTML"

    .line 44
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-direct {p0}, Lcom/baidu/mobads/production/g/d;->c()Z

    move-result v1

    const-string v2, "mimetype"

    if-eqz v1, :cond_0

    const-string v1, "video/mp4,image/jpg,image/gif,image/png,rm/3d,rm/vr"

    .line 46
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v1, "video/mp4,image/jpg,image/gif,image/png,rm/3d"

    .line 48
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 55
    invoke-super {p0}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
