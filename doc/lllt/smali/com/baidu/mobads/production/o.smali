.class public Lcom/baidu/mobads/production/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lcom/baidu/mobads/production/b;

.field private c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field private h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private i:Lcom/baidu/mobads/utils/f;

.field private j:Lcom/baidu/mobads/utils/q;

.field private k:Lcom/baidu/mobads/interfaces/IXAdResource;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Lcom/baidu/mobads/utils/b$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/production/b;)V
    .locals 3

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 72
    iput v0, p0, Lcom/baidu/mobads/production/o;->l:I

    const/4 v1, 0x2

    .line 74
    iput v1, p0, Lcom/baidu/mobads/production/o;->m:I

    const/16 v2, 0xf

    .line 75
    iput v2, p0, Lcom/baidu/mobads/production/o;->n:I

    .line 106
    iput v0, p0, Lcom/baidu/mobads/production/o;->o:I

    .line 108
    iput v1, p0, Lcom/baidu/mobads/production/o;->p:I

    .line 109
    iput v2, p0, Lcom/baidu/mobads/production/o;->q:I

    .line 587
    new-instance v1, Lcom/baidu/mobads/production/t;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/t;-><init>(Lcom/baidu/mobads/production/o;)V

    iput-object v1, p0, Lcom/baidu/mobads/production/o;->r:Lcom/baidu/mobads/utils/b$a;

    .line 62
    iput-object p1, p0, Lcom/baidu/mobads/production/o;->a:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    .line 66
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/o;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 67
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/o;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 68
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/o;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->l:I

    return p0
.end method

.method private a()V
    .locals 14

    .line 572
    iget-object v0, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/baidu/mobads/production/o;->j:Lcom/baidu/mobads/utils/q;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/baidu/mobads/production/o;->i:Lcom/baidu/mobads/utils/f;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/baidu/mobads/production/o;->h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    if-nez v1, :cond_0

    goto :goto_0

    .line 577
    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x20d

    iget-object v0, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    .line 578
    invoke-virtual {v0}, Lcom/baidu/mobads/production/b;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/baidu/mobads/production/o;->h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 579
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/baidu/mobads/production/o;->i:Lcom/baidu/mobads/utils/f;

    iget-object v1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    .line 580
    invoke-virtual {v1}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 579
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    .line 581
    invoke-virtual {v0}, Lcom/baidu/mobads/production/b;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getAdPlacementId()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/baidu/mobads/production/o;->j:Lcom/baidu/mobads/utils/q;

    .line 582
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/q;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v12, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v5, "click"

    .line 577
    invoke-virtual/range {v2 .. v13}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 584
    new-instance v0, Lcom/baidu/mobads/command/a/a;

    iget-object v1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    iget-object v2, p0, Lcom/baidu/mobads/production/o;->h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v3, p0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v0, v1, v2, v3}, Lcom/baidu/mobads/command/a/a;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;)V

    invoke-virtual {v0}, Lcom/baidu/mobads/command/a/a;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    const/4 v0, 0x0

    .line 112
    :try_start_0
    iput v0, p0, Lcom/baidu/mobads/production/o;->o:I

    .line 113
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 114
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    .line 115
    new-instance v7, Lcom/baidu/mobads/production/q;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/production/q;-><init>(Lcom/baidu/mobads/production/o;Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;Landroid/content/Context;Ljava/lang/String;Ljava/util/Timer;)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x3e8

    move-object v1, v0

    move-object v2, v7

    .line 135
    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v0, 0x0

    .line 78
    :try_start_0
    iput v0, p0, Lcom/baidu/mobads/production/o;->l:I

    .line 79
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 80
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v3

    .line 81
    new-instance v8, Lcom/baidu/mobads/production/p;

    move-object v1, v8

    move-object v2, p0

    move-object v4, p1

    move-object v5, p3

    move-object v6, v0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/baidu/mobads/production/p;-><init>(Lcom/baidu/mobads/production/o;Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;Landroid/content/Context;Ljava/lang/String;Ljava/util/Timer;Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x3e8

    move-object v1, v0

    move-object v2, v8

    .line 99
    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 563
    new-instance v0, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {v0}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 564
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 565
    new-instance v2, Lcom/baidu/mobads/openad/d/b;

    const-string v3, ""

    invoke-direct {v2, v1, v3}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 566
    iput v1, v2, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 567
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->m:I

    return p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->n:I

    return p0
.end method

.method static synthetic d(Lcom/baidu/mobads/production/o;)I
    .locals 2

    .line 45
    iget v0, p0, Lcom/baidu/mobads/production/o;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/baidu/mobads/production/o;->l:I

    return v0
.end method

.method static synthetic e(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->o:I

    return p0
.end method

.method static synthetic f(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->p:I

    return p0
.end method

.method static synthetic g(Lcom/baidu/mobads/production/o;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/baidu/mobads/production/o;->q:I

    return p0
.end method

.method static synthetic h(Lcom/baidu/mobads/production/o;)I
    .locals 2

    .line 45
    iget v0, p0, Lcom/baidu/mobads/production/o;->o:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/baidu/mobads/production/o;->o:I

    return v0
.end method

.method static synthetic i(Lcom/baidu/mobads/production/o;)Lcom/baidu/mobads/production/b;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;",
            "Lcom/baidu/mobads/interfaces/IXAdResource;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ")Z"
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v0, "lpShoubaiStyle"

    .line 299
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "video_and_web"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 301
    new-instance v1, Lcom/baidu/mobads/command/c/c;

    .line 302
    invoke-interface {p4}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getWebUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, p4, p3, v2}, Lcom/baidu/mobads/command/c/c;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, v1, Lcom/baidu/mobads/command/c/c;->f:Ljava/lang/String;

    const-string p2, "lpMurlStyle"

    .line 304
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, v1, Lcom/baidu/mobads/command/c/c;->g:Ljava/lang/String;

    .line 305
    invoke-virtual {v1}, Lcom/baidu/mobads/command/c/c;->a()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onAdClicked(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    const-string v5, ""

    .line 147
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v6

    iput-object v6, v0, Lcom/baidu/mobads/production/o;->i:Lcom/baidu/mobads/utils/f;

    .line 148
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v6

    check-cast v6, Lcom/baidu/mobads/utils/q;

    iput-object v6, v0, Lcom/baidu/mobads/production/o;->j:Lcom/baidu/mobads/utils/q;

    .line 149
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v6

    .line 150
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v8

    .line 152
    iget-object v7, v0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    .line 153
    iput-object v1, v0, Lcom/baidu/mobads/production/o;->g:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 154
    iput-object v2, v0, Lcom/baidu/mobads/production/o;->h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 155
    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v9

    invoke-interface {v9}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResource()Lcom/baidu/mobads/interfaces/IXAdResource;

    move-result-object v9

    iput-object v9, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    const/4 v15, 0x0

    .line 157
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 159
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v10

    .line 161
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v11

    .line 163
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdClickTrackingUrls()Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    .line 165
    :goto_0
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v15

    if-ge v14, v15, :cond_0

    .line 167
    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 169
    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getPlayheadTime()D

    move-result-wide v3

    double-to-int v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\$\\{PROGRESS\\}"

    .line 167
    invoke-virtual {v15, v4, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 170
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    goto :goto_0

    .line 172
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 173
    invoke-interface {v3, v12}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 174
    invoke-direct {v0, v3}, Lcom/baidu/mobads/production/o;->a(Ljava/util/Set;)V

    .line 177
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeOpenExternalApp()I

    move-result v3

    const/4 v15, 0x1

    if-ne v11, v3, :cond_4

    .line 178
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 181
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 184
    :catch_0
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppOpenStrs()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 185
    new-instance v9, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {v9}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 186
    new-instance v11, Lcom/baidu/mobads/openad/d/b;

    invoke-direct {v11, v10, v5}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iput v15, v11, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 188
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v9, "page"

    .line 193
    invoke-virtual {v4, v9, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 194
    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 195
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x16e

    const-string v13, "fb_act"

    const/4 v10, 0x0

    invoke-virtual {v4, v13, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v16

    const-string v15, "version"

    invoke-virtual {v4, v15, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    move-object v10, v14

    move-object/from16 v17, v13

    move/from16 v13, v16

    move-object/from16 v16, v3

    move-object v3, v14

    move v14, v15

    .line 194
    invoke-interface/range {v8 .. v14}, Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;->sendAPOInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 196
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 197
    new-instance v1, Lcom/baidu/mobads/command/b/a;

    iget-object v4, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v1, v7, v2, v4, v3}, Lcom/baidu/mobads/command/b/a;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/baidu/mobads/command/b/a;->a()V

    goto :goto_2

    :cond_1
    move-object/from16 v8, v17

    const/4 v15, 0x0

    .line 200
    invoke-virtual {v4, v8, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    .line 201
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v9

    .line 202
    new-instance v10, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-direct {v10, v9}, Lcom/baidu/mobads/vo/XAdInstanceInfo;-><init>(Lorg/json/JSONObject;)V

    .line 203
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeLandingPage()I

    move-result v9

    const-string v11, "fallback"

    if-ne v8, v9, :cond_2

    .line 204
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeLandingPage()I

    move-result v6

    invoke-virtual {v10, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setActionType(I)V

    .line 205
    invoke-virtual {v4, v11, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setClickThroughUrl(Ljava/lang/String;)V

    .line 206
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setTitle(Ljava/lang/String;)V

    const/4 v4, 0x1

    .line 207
    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setInapp(Z)V

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    .line 209
    invoke-virtual {v0, v1, v10, v12, v13}, Lcom/baidu/mobads/production/o;->onAdClicked(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V

    goto :goto_2

    :cond_2
    move-object/from16 v12, p3

    move-object/from16 v13, p4

    .line 210
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v9

    if-ne v8, v9, :cond_3

    .line 211
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v6

    invoke-virtual {v10, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setActionType(I)V

    .line 212
    invoke-virtual {v4, v11, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setClickThroughUrl(Ljava/lang/String;)V

    .line 213
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setTitle(Ljava/lang/String;)V

    .line 214
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setQueryKey(Ljava/lang/String;)V

    const/4 v4, 0x1

    .line 215
    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setInapp(Z)V

    .line 216
    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setAPOOpen(Z)V

    .line 217
    invoke-virtual {v10, v3}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setPage(Ljava/lang/String;)V

    .line 218
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setAppPackageName(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v0, v1, v10, v12, v13}, Lcom/baidu/mobads/production/o;->onAdClicked(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V

    .line 224
    :cond_3
    :goto_2
    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/baidu/mobads/production/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v9, v16

    goto/16 :goto_7

    :catch_1
    return-void

    :cond_4
    move-object/from16 v12, p3

    move-object/from16 v13, p4

    const/4 v15, 0x0

    .line 226
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v3

    if-ne v11, v3, :cond_5

    .line 227
    iget-object v1, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-virtual {v0, v13, v7, v1, v2}, Lcom/baidu/mobads/production/o;->a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 229
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 230
    invoke-direct/range {p0 .. p0}, Lcom/baidu/mobads/production/o;->a()V

    goto/16 :goto_7

    .line 233
    :cond_5
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeLandingPage()I

    move-result v3

    if-eq v11, v3, :cond_d

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeOpenMap()I

    move-result v3

    if-ne v11, v3, :cond_6

    goto/16 :goto_6

    .line 252
    :cond_6
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeMakeCall()I

    move-result v1

    if-eq v11, v1, :cond_9

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeSendSMS()I

    move-result v1

    if-eq v11, v1, :cond_9

    .line 253
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeSendMail()I

    move-result v1

    if-ne v11, v1, :cond_7

    goto :goto_3

    .line 282
    :cond_7
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeNothing2Do()I

    move-result v1

    if-ne v11, v1, :cond_8

    goto/16 :goto_7

    .line 284
    :cond_8
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeRichMedia()I

    goto/16 :goto_7

    :cond_9
    :goto_3
    const/4 v1, 0x1

    .line 255
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 256
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 257
    new-instance v1, Lcom/baidu/mobads/command/b/a;

    iget-object v3, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v1, v7, v2, v3, v10}, Lcom/baidu/mobads/command/b/a;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/baidu/mobads/command/b/a;->a()V

    .line 260
    :cond_a
    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeMakeCall()I

    move-result v1

    if-ne v11, v1, :cond_11

    .line 261
    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 262
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/16 v3, 0x40

    .line 264
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 265
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_11

    const/4 v2, 0x0

    move-object v3, v2

    const/4 v2, 0x0

    .line 268
    :goto_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_c

    const/4 v4, 0x1

    if-lt v2, v4, :cond_b

    .line 270
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    goto :goto_5

    .line 275
    :cond_b
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_c
    const/4 v15, 0x1

    :goto_5
    if-eqz v15, :cond_11

    .line 278
    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/baidu/mobads/production/o;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_7

    .line 234
    :cond_d
    :goto_6
    iget-object v3, v0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    invoke-virtual {v3}, Lcom/baidu/mobads/production/b;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v3

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getProductionTypeSplash()Ljava/lang/String;

    move-result-object v4

    if-eq v3, v4, :cond_e

    const/4 v3, 0x1

    .line 235
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 237
    :cond_e
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 238
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isInapp()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 239
    iget-object v1, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-virtual {v0, v13, v7, v1, v2}, Lcom/baidu/mobads/production/o;->a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 240
    new-instance v1, Lcom/baidu/mobads/command/c/c;

    iget-object v3, v0, Lcom/baidu/mobads/production/o;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v1, v7, v2, v3, v10}, Lcom/baidu/mobads/command/c/c;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    if-eqz v13, :cond_f

    const-string v2, "lpShoubaiStyle"

    .line 243
    invoke-virtual {v13, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 244
    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v1, Lcom/baidu/mobads/command/c/c;->f:Ljava/lang/String;

    .line 246
    :cond_f
    invoke-virtual {v1}, Lcom/baidu/mobads/command/c/c;->a()V

    goto :goto_7

    .line 249
    :cond_10
    iget-object v2, v0, Lcom/baidu/mobads/production/o;->i:Lcom/baidu/mobads/utils/f;

    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2, v1, v10}, Lcom/baidu/mobads/utils/f;->browserOutside(Landroid/content/Context;Ljava/lang/String;)V

    .line 290
    :cond_11
    :goto_7
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 291
    iget-object v1, v0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance v2, Lcom/baidu/mobads/e/a;

    const-string v3, "AdClickThru"

    invoke-direct {v2, v3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 294
    :cond_12
    iget-object v1, v0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance v2, Lcom/baidu/mobads/e/a;

    const-string v3, "AdUserClick"

    invoke-direct {v2, v3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdCustomEvent(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p2, "onrvideocachesucc"

    .line 551
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 552
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdRvdieoCacheSucc"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_0
    const-string p2, "onrvideocachefailed"

    .line 553
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 554
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdRvdieoCacheFailed"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_1
    const-string p2, "playCompletion"

    .line 555
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 556
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "PlayCompletion"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_2
    const-string p2, "AdRvdieoPlayError"

    .line 557
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 558
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p3, Lcom/baidu/mobads/e/a;

    invoke-direct {p3, p2, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onAdDurationChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 439
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdError(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 383
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_1

    if-eqz p4, :cond_0

    .line 385
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p1

    .line 386
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 387
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getInfoKeyErrorCode()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ","

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getInfoKeyErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getInfoKeyErrorModule()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 390
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    .line 392
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 393
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdError"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_1
    return-void
.end method

.method public onAdExpandedChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 424
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdImpression(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 350
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getImpressionUrls()Ljava/util/Set;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/o;->a(Ljava/util/Set;)V

    .line 351
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdImpression"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdInteraction(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 487
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdLinearChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 417
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdLoaded(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 315
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    if-ne p2, p3, :cond_0

    .line 316
    iget-object p2, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    invoke-virtual {p2, p1, p4}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    goto :goto_0

    .line 318
    :cond_0
    new-instance p2, Landroid/os/Handler;

    iget-object p3, p0, Lcom/baidu/mobads/production/o;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p3, Lcom/baidu/mobads/production/r;

    invoke-direct {p3, p0, p1, p4}, Lcom/baidu/mobads/production/r;-><init>(Lcom/baidu/mobads/production/o;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public onAdPaused(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 412
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdPaused"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdPlaying(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 403
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdPlaying"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdRemainingTimeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 447
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdSizeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 463
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdSkippableStateChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 471
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdSkipped(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 479
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdStarted(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 330
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    if-ne p2, p3, :cond_0

    .line 331
    iget-object p2, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    invoke-virtual {p2, p1, p4}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    goto :goto_0

    .line 333
    :cond_0
    new-instance p2, Landroid/os/Handler;

    iget-object p3, p0, Lcom/baidu/mobads/production/o;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p3, Lcom/baidu/mobads/production/s;

    invoke-direct {p3, p0, p1, p4}, Lcom/baidu/mobads/production/s;-><init>(Lcom/baidu/mobads/production/o;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public onAdStoped(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 363
    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    .line 364
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 365
    invoke-direct {p0, p3}, Lcom/baidu/mobads/production/o;->a(Ljava/util/Set;)V

    .line 368
    :cond_0
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 369
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    .line 370
    iget-object p2, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p3

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    goto :goto_0

    .line 372
    :cond_1
    iget-object p2, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    invoke-virtual {p2, p1, p5}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 373
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdStopped"

    invoke-direct {p2, p3, p5}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method

.method public onAdUserAcceptInvitation(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 495
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdUserClosed(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 434
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->b:Lcom/baidu/mobads/production/b;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdUserClose"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdUserMinimize(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 503
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVideoComplete(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 543
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVideoFirstQuartile(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 519
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVideoMidpoint(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 527
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVideoStart(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 511
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVideoThirdQuartile(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 535
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method

.method public onAdVolumeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 455
    iget-object p1, p0, Lcom/baidu/mobads/production/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_0
    return-void
.end method
