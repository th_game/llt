.class public Lcom/baidu/mobads/production/h/b;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXLinearAdSlot;
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;


# instance fields
.field private x:Lcom/baidu/mobads/production/h/a;

.field private y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/h/b;->setId(Ljava/lang/String;)V

    .line 48
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/h/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 p1, 0x1

    .line 49
    iput-boolean p1, p0, Lcom/baidu/mobads/production/h/b;->y:Z

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 148
    new-instance v0, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {v0}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 149
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 150
    new-instance v2, Lcom/baidu/mobads/openad/d/b;

    const-string v3, ""

    invoke-direct {v2, v1, v3}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 151
    iput v1, v2, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 152
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 2

    .line 103
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getParameter()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "AD_REQUESTING_TIMEOUT"

    .line 104
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 107
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    int-to-double v0, p3

    .line 112
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/u;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 117
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p2

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p2

    .line 118
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p2, v0, :cond_0

    .line 119
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    .line 120
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, p2, :cond_2

    .line 123
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/h/a;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/vo/b;

    .line 124
    invoke-virtual {p1}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object p2

    if-nez p2, :cond_1

    .line 126
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :cond_1
    :try_start_0
    const-string v0, "supportTipView"

    .line 129
    iget-boolean v1, p0, Lcom/baidu/mobads/production/h/b;->y:Z

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 131
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 133
    :goto_0
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 135
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->start()V

    :cond_2
    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 141
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 142
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 143
    invoke-direct {p0, p2}, Lcom/baidu/mobads/production/h/b;->a(Ljava/util/Set;)V

    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 158
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 160
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->COMPLETED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object p1, p0, Lcom/baidu/mobads/production/h/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    return-void
.end method

.method public g()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XPrerollAdSlot"

    const-string v2, "afterAdContainerInit()"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Lcom/baidu/mobads/openad/c/b;

    const-string v1, "complete"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/openad/c/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/h/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->q()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-nez v0, :cond_0

    .line 216
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->getDuration()I

    move-result v0

    return v0

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getDuration()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getPlayheadTime()I
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-nez v0, :cond_0

    .line 225
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->getPlayheadTime()I

    move-result v0

    return v0

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getPlayheadTime()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x1f40

    .line 56
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public load()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 234
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->load()V

    return-void
.end method

.method public notifyVisitorAction(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VisitorAction;)V
    .locals 0

    return-void
.end method

.method public pause()V
    .locals 3

    .line 255
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XPrerollAdSlot"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    if-ne v0, v1, :cond_0

    .line 257
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->pause()V

    :cond_0
    return-void
.end method

.method public q()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    return-object v0
.end method

.method public request()V
    .locals 4

    .line 73
    new-instance v0, Lcom/baidu/mobads/production/h/a;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/h/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/baidu/mobads/production/h/a;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/h/a;->d(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getParameter()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "BASE_WIDTH"

    .line 79
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "BASE_HEIGHT"

    .line 80
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 83
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 90
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 95
    :catch_1
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/h/a;->d(I)V

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/production/h/a;->e(I)V

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->x:Lcom/baidu/mobads/production/h/a;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method

.method public resume()V
    .locals 3

    .line 263
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XPrerollAdSlot"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    if-ne v0, v1, :cond_0

    .line 265
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->resume()V

    :cond_0
    return-void
.end method

.method public setActivityState(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V
    .locals 0

    return-void
.end method

.method public setContentVideoAssetCurrentTimePosition(D)V
    .locals 0

    return-void
.end method

.method public setMaxAdNum(I)V
    .locals 0

    return-void
.end method

.method public setMaxDuration(I)V
    .locals 0

    return-void
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public setSupportTipView(Z)V
    .locals 0

    .line 270
    iput-boolean p1, p0, Lcom/baidu/mobads/production/h/b;->y:Z

    return-void
.end method

.method public setVideoDisplayBase(Landroid/widget/RelativeLayout;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/baidu/mobads/production/h/b;->e:Landroid/widget/RelativeLayout;

    return-void
.end method

.method public setVideoState(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;)V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->start()V

    goto :goto_0

    .line 242
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->load()V

    :goto_0
    return-void
.end method

.method public stop()V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/baidu/mobads/production/h/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/b;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XPrerollAdSlot"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->stop()V

    return-void
.end method
