.class public Lcom/baidu/mobads/production/b/b;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private x:Lcom/baidu/mobads/production/b/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 31
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/b;->setActivity(Landroid/content/Context;)V

    .line 34
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b/b;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 36
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/b/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 37
    new-instance p1, Lcom/baidu/mobads/production/b/d;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/b/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/b/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/b/b;->x:Lcom/baidu/mobads/production/b/d;

    return-void
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 0

    .line 73
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'html\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/b/b;->x:Lcom/baidu/mobads/production/b/d;

    .line 74
    invoke-virtual {p2}, Lcom/baidu/mobads/production/b/d;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 75
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 77
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b/b;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 79
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 83
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/b;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 65
    iput-object p1, p0, Lcom/baidu/mobads/production/b/b;->k:Lcom/baidu/mobads/vo/d;

    .line 66
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 67
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/b/b;->a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V

    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->start()V

    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 101
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->p()V

    .line 102
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public g()V
    .locals 0

    .line 43
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->load()V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/b;->q()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x2710

    .line 48
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public q()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/production/b/b;->x:Lcom/baidu/mobads/production/b/d;

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/baidu/mobads/production/b/b;->x:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b/b;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
