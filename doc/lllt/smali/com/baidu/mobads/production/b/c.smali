.class public Lcom/baidu/mobads/production/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/webkit/CookieManager;

.field private e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

.field private f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

.field private g:Landroid/content/Context;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 45
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    .line 46
    iput-object p1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    .line 47
    iput p2, p0, Lcom/baidu/mobads/production/b/c;->h:I

    .line 48
    iput-object p3, p0, Lcom/baidu/mobads/production/b/c;->i:Ljava/lang/String;

    const/4 p1, 0x0

    .line 49
    iput-object p1, p0, Lcom/baidu/mobads/production/b/c;->j:Ljava/lang/String;

    .line 50
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->b()V

    .line 51
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->c()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 56
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    .line 57
    iput-object p1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lcom/baidu/mobads/production/b/c;->j:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lcom/baidu/mobads/production/b/c;->i:Ljava/lang/String;

    const/4 p1, -0x1

    .line 60
    iput p1, p0, Lcom/baidu/mobads/production/b/c;->h:I

    .line 61
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->b()V

    .line 62
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const-string v0, ":"

    const-string v1, "-"

    .line 266
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .line 173
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 174
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "="

    .line 175
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string p1, ";"

    .line 177
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->d:Landroid/webkit/CookieManager;

    const-string p2, "https://cpu.baidu.com/"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 181
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 115
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 118
    :goto_0
    :try_start_1
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->d:Landroid/webkit/CookieManager;

    .line 119
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->d:Landroid/webkit/CookieManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    .line 121
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private c()V
    .locals 2

    .line 134
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->a:Ljava/util/Set;

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->a:Ljava/util/Set;

    const-string v1, "46000"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->a:Ljava/util/Set;

    const-string v1, "46002"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->a:Ljava/util/Set;

    const-string v1, "46007"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->b:Ljava/util/Set;

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->b:Ljava/util/Set;

    const-string v1, "46001"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->b:Ljava/util/Set;

    const-string v1, "46006"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->c:Ljava/util/Set;

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->c:Ljava/util/Set;

    const-string v1, "46003"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->c:Ljava/util/Set;

    const-string v1, "46005"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private d()V
    .locals 9

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 149
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 150
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->e()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 151
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v3

    :goto_0
    if-eqz v2, :cond_1

    .line 152
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->f()I

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v2, :cond_2

    .line 153
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->g()Ljava/lang/String;

    move-result-object v3

    .line 155
    :cond_2
    iget-object v6, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v7, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v6, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 156
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->i()Ljava/lang/String;

    move-result-object v7

    const-string v8, "v"

    invoke-direct {p0, v8, v7}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 157
    iget-object v7, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v8, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "im"

    invoke-direct {p0, v8, v7}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    iget-object v7, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v8, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "aid"

    invoke-direct {p0, v8, v7}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    iget-object v7, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v8, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "m"

    invoke-direct {p0, v8, v7}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v7, "cuid"

    .line 160
    invoke-direct {p0, v7, v6}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    iget-object v6, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-static {v6}, Lcom/baidu/mobads/production/b/a;->a(Landroid/content/Context;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "ct"

    invoke-direct {p0, v7, v6}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 162
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->j()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "oi"

    invoke-direct {p0, v7, v6}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v6, 0x1

    .line 163
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "src"

    invoke-direct {p0, v7, v6}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v6, "h"

    invoke-direct {p0, v6, v1}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 165
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "w"

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "apm"

    .line 166
    invoke-direct {p0, v0, v4}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 167
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rssi"

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "apn"

    .line 168
    invoke-direct {p0, v0, v3}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 169
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "isc"

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private e()Z
    .locals 3

    const/4 v0, 0x0

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 188
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 189
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    .line 192
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    return v0
.end method

.method private f()I
    .locals 3

    const/4 v0, 0x0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 200
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    .line 204
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    return v0
.end method

.method private g()Ljava/lang/String;
    .locals 4

    const-string v0, "\""

    const-string v1, ""

    .line 211
    :try_start_0
    iget-object v2, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 212
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v2, v1

    goto :goto_0

    .line 213
    :cond_0
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    .line 214
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x1

    sub-int/2addr v0, v3

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 219
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v1
.end method

.method private h()Ljava/lang/String;
    .locals 2

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 227
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 228
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 230
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, ""

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 237
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 238
    :cond_0
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    :goto_0
    if-eqz v1, :cond_1

    const-string v2, "."

    const-string v3, "-"

    .line 240
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    .line 243
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :cond_1
    return-object v0
.end method

.method private j()I
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->g:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    return v0

    .line 256
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    return v0

    .line 259
    :cond_2
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    return v0

    :cond_3
    const/16 v0, 0x63

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .line 125
    invoke-direct {p0}, Lcom/baidu/mobads/production/b/c;->d()V

    .line 126
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->j:Ljava/lang/String;

    const-string v1, "/"

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://cpu.baidu.com/block/app/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/production/b/c;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 129
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://cpu.baidu.com/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/baidu/mobads/production/b/c;->h:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
