.class public Lcom/baidu/mobads/production/a/a;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private x:Lcom/baidu/mobads/production/a/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Z)V
    .locals 2

    .line 39
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/a/a;->setId(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->setActivity(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 45
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 47
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    .line 48
    new-instance p1, Lcom/baidu/mobads/production/a/c;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/a/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, p2, v0, v1}, Lcom/baidu/mobads/production/a/c;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    .line 50
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    sget-object p2, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a/c;->f(I)V

    .line 51
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/a/c;->d(Ljava/lang/String;)V

    .line 53
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/a/c;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/vo/b;

    .line 54
    invoke-virtual {p1, p4}, Lcom/baidu/mobads/vo/b;->a(Z)V

    .line 55
    invoke-virtual {p1}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object p2

    if-nez p2, :cond_0

    .line 57
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string p4, "ABILITY"

    const-string v0, "BANNER_CLOSE,PAUSE,UNLIMITED_BANNER_SIZE,"

    .line 60
    invoke-virtual {p2, p4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p4

    .line 62
    invoke-virtual {p4}, Lorg/json/JSONException;->printStackTrace()V

    .line 64
    :goto_0
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 65
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/a/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic f(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic g(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobads/production/a/a;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/baidu/mobads/production/a/a;->f:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 0

    .line 107
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    .line 108
    invoke-virtual {p2}, Lcom/baidu/mobads/production/a/c;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 109
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 112
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 118
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 122
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 99
    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->k:Lcom/baidu/mobads/vo/d;

    .line 100
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 101
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/a/a;->a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V

    return-void
.end method

.method protected c()V
    .locals 2

    .line 142
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/mobads/production/a/b;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/a/b;-><init>(Lcom/baidu/mobads/production/a/a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 158
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 128
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->start()V

    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 162
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->p()V

    .line 163
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public g()V
    .locals 0

    .line 70
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->load()V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->q()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x2710

    .line 75
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public q()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    return-object v0
.end method

.method public request()V
    .locals 4

    const-string v0, "AdError"

    const-string v1, "error_message"

    .line 80
    iget-object v2, p0, Lcom/baidu/mobads/production/a/a;->x:Lcom/baidu/mobads/production/a/c;

    invoke-virtual {p0, v2}, Lcom/baidu/mobads/production/a/a;->a(Lcom/baidu/mobads/vo/d;)Z

    .line 83
    :try_start_0
    new-instance v2, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 90
    invoke-virtual {v2}, Ljava/lang/Error;->printStackTrace()V

    .line 91
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "init webview,error"

    .line 92
    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    new-instance v1, Lcom/baidu/mobads/e/a;

    invoke-direct {v1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :catch_1
    move-exception v2

    .line 85
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 86
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "init webview,exception"

    .line 87
    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v1, Lcom/baidu/mobads/e/a;

    invoke-direct {v1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method
