.class public Lcom/baidu/mobads/production/c/c;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"


# instance fields
.field private x:Lcom/baidu/mobads/production/c/d;

.field private y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .line 47
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/c/c;->setId(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/c/c;->setActivity(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/c/c;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 52
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object v0, p0, Lcom/baidu/mobads/production/c/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 53
    new-instance v0, Lcom/baidu/mobads/production/c/d;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/c;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/c/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {v0, v1, v2, v3}, Lcom/baidu/mobads/production/c/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    .line 57
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v0

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    invoke-static {p1}, Lcom/baidu/mobads/utils/o;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/baidu/mobads/utils/f;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    .line 68
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/c/d;->b(Ljava/lang/String;)V

    .line 69
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    const/16 v0, 0x258

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/c/d;->d(I)V

    .line 70
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    const/16 v0, 0x1f4

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/c/d;->e(I)V

    .line 71
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/c/d;->h(I)V

    .line 72
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/c/d;->d(Ljava/lang/String;)V

    .line 73
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    sget-object p2, Lcom/baidu/mobads/AdSize;->FeedNative:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/c/d;->f(I)V

    .line 74
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/c/d;->g(I)V

    .line 75
    iget-object p1, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p2

    .line 76
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeImage()I

    move-result p2

    .line 75
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/c/d;->i(I)V

    return-void
.end method

.method private a(ILjava/util/List;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 245
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/i;->feedsTrackerParameterKeyProgress()Ljava/lang/String;

    move-result-object v1

    .line 246
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 245
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/i;->feedsTrackerParameterKeyList()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 209
    :try_start_0
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object p2

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v0, p1, p3, p4, p2}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onClose(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 212
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 7

    .line 171
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/c;->r()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->setClickView(Landroid/view/View;)V

    .line 172
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdClickTrackingUrls()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p3, v0}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v6

    .line 173
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-object v1, v0

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 175
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, -0x1

    .line 131
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdImpressionTrackingUrls()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 134
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 3

    .line 80
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v0

    .line 81
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 83
    iget-object v2, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-virtual {v2, v0}, Lcom/baidu/mobads/production/c/d;->d(I)V

    .line 84
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/c/d;->e(I)V

    .line 86
    :cond_0
    invoke-super {p0, p1}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 2

    int-to-double v0, p3

    .line 101
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/u;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z
    .locals 3

    const-string v0, "_&_"

    .line 140
    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 141
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 146
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 148
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 149
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 152
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/f/q;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v0, p1, p2, p3}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->isAdAvailable(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return p1

    :catch_1
    const/4 p1, 0x0

    return p1
.end method

.method public b(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 7

    .line 228
    :try_start_0
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getFullScreenTrackers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v6

    .line 229
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-object v1, v0

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onFullScreen(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 232
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, 0x0

    .line 181
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onStart(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 184
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public b(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    const/4 v0, -0x1

    .line 164
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/baidu/mobads/production/c/c;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public c(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onComplete(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 202
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 106
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->start()V

    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public d(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, 0x0

    .line 218
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCstartcardTrackers()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/c/c;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onCstartcard(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 221
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 111
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/c/c;->y:Ljava/util/ArrayList;

    return-void
.end method

.method public g()V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/c;->r()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x1f40

    .line 91
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public q()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->y:Ljava/util/ArrayList;

    return-object v0
.end method

.method public r()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/production/c/c;->x:Lcom/baidu/mobads/production/c/d;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
