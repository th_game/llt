.class Lcom/baidu/mobads/production/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/b;)V
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/baidu/mobads/production/c;->a:Lcom/baidu/mobads/production/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/baidu/mobads/production/c;->a:Lcom/baidu/mobads/production/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/b;->k()V

    .line 235
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "URLLoader.Load.Complete"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/baidu/mobads/production/c;->a:Lcom/baidu/mobads/production/b;

    invoke-virtual {v0, p1, v1}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p1, "request ad-server error, io_err/timeout"

    .line 239
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    invoke-interface {v0, v1, p1, v1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/baidu/mobads/production/c;->a:Lcom/baidu/mobads/production/b;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b;->d(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
