.class public Lcom/baidu/mobads/production/rewardvideo/a;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"


# instance fields
.field private A:Z

.field private x:Lcom/baidu/mobads/production/rewardvideo/b;

.field private y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4

    .line 45
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Z

    .line 46
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/rewardvideo/a;->setId(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/rewardvideo/a;->setActivity(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 48
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 49
    iput-boolean p3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Z

    .line 50
    sget-object p3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 51
    new-instance p3, Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p3, v1, v2, v3}, Lcom/baidu/mobads/production/rewardvideo/b;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    .line 55
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p3

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p3

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    invoke-static {p1}, Lcom/baidu/mobads/utils/o;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/utils/f;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 66
    iget-object v2, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/production/rewardvideo/b;->b(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobads/utils/f;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object p1

    .line 69
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/rewardvideo/b;->d(I)V

    .line 70
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/baidu/mobads/production/rewardvideo/b;->e(I)V

    .line 71
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/rewardvideo/b;->h(I)V

    .line 72
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->d(Ljava/lang/String;)V

    .line 73
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->g(I)V

    .line 74
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    sget-object p2, Lcom/baidu/mobads/AdSize;->RewardVideo:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->f(I)V

    .line 75
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeImage()I

    move-result p2

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeVideo()I

    move-result p3

    add-int/2addr p2, p3

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->i(I)V

    return-void
.end method

.method private w()V
    .locals 3

    .line 186
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 187
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    .line 188
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-static {}, Lcom/baidu/mobads/MobRewardVideoImpl;->getActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 189
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->x()Ljava/lang/String;

    move-result-object v1

    const-string v2, "orientation"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    iget-boolean v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Z

    const-string v2, "useSurfaceView"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private x()Ljava/lang/String;
    .locals 3

    .line 198
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    const-string v1, "portrait"

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 201
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const-string v1, "landscape"

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :cond_1
    :goto_0
    return-object v1
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 2

    int-to-double v0, p3

    .line 142
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/u;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 174
    iput-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Z

    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 147
    iget-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 148
    iput-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Z

    .line 149
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->w()V

    :cond_0
    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public g()V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->v()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x1f40

    .line 132
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public o()V
    .locals 1

    .line 179
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->o()V

    const/4 v0, 0x0

    .line 180
    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 181
    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    return-void
.end method

.method public q()Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 88
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 89
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->isExpired()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 97
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->isVideoDownloaded()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public request()V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method

.method public s()Z
    .locals 4

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdHasDisplayed()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :catch_0
    move-exception v0

    .line 109
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notPlayedBefore-exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "XAbstractAdProdTemplate"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 117
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 118
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->y:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 120
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public u()V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->w()V

    return-void
.end method

.method public v()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->x:Lcom/baidu/mobads/production/rewardvideo/b;

    return-object v0
.end method
