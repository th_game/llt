.class public Lcom/baidu/mobads/production/d/a;
.super Lcom/baidu/mobads/production/b;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private x:Lcom/baidu/mobads/production/d/c;

.field private y:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

.field private z:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;)V
    .locals 3

    .line 52
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->z:Landroid/webkit/WebView;

    .line 55
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/d/a;->setActivity(Landroid/content/Context;)V

    .line 57
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 58
    new-instance p1, Lcom/baidu/mobads/production/d/c;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/d/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, v0, v1, v2}, Lcom/baidu/mobads/production/d/c;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->x:Lcom/baidu/mobads/production/d/c;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic f(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic g(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobads/production/d/a;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/d/a;->f:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
    .locals 0

    .line 86
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/d/a;->x:Lcom/baidu/mobads/production/d/c;

    .line 87
    invoke-virtual {p2}, Lcom/baidu/mobads/production/d/c;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HYBRID:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 88
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 91
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/d/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 93
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 96
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/d/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/baidu/mobads/production/d/a;->y:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 78
    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->k:Lcom/baidu/mobads/vo/d;

    .line 79
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 80
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/d/a;->a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V

    return-void
.end method

.method protected c()V
    .locals 2

    .line 123
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/mobads/production/d/b;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/d/b;-><init>(Lcom/baidu/mobads/production/d/a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 138
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 102
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast p1, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->y:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    .line 103
    iget-object p1, p0, Lcom/baidu/mobads/production/d/a;->y:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    iget-object p2, p0, Lcom/baidu/mobads/production/d/a;->z:Landroid/webkit/WebView;

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;->setCustomerWebView(Landroid/webkit/WebView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 105
    iput-object p1, p0, Lcom/baidu/mobads/production/d/a;->y:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    .line 109
    :goto_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->start()V

    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 142
    invoke-super {p0}, Lcom/baidu/mobads/production/b;->p()V

    .line 143
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/d/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public g()V
    .locals 0

    .line 63
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->load()V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/a;->q()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    const/16 v0, 0x2710

    .line 68
    iput v0, p0, Lcom/baidu/mobads/production/b;->m:I

    return-void
.end method

.method public q()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/baidu/mobads/production/d/a;->x:Lcom/baidu/mobads/production/d/c;

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/baidu/mobads/production/d/a;->x:Lcom/baidu/mobads/production/d/c;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/d/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
