.class Lcom/baidu/mobads/production/e/e;
.super Landroid/os/CountDownTimer;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/e/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/e/b;JJ)V
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/baidu/mobads/production/e/e;->a:Lcom/baidu/mobads/production/e/b;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/baidu/mobads/production/e/e;->a:Lcom/baidu/mobads/production/e/b;

    iget-object v0, v0, Lcom/baidu/mobads/production/e/b;->y:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "CountDownTimer finished"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, Lcom/baidu/mobads/production/e/e;->a:Lcom/baidu/mobads/production/e/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/e/b;->u()V

    .line 311
    iget-object v0, p0, Lcom/baidu/mobads/production/e/e;->a:Lcom/baidu/mobads/production/e/b;

    iget-object v0, v0, Lcom/baidu/mobads/production/e/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->stop()V

    return-void
.end method

.method public onTick(J)V
    .locals 2

    const-wide/16 v0, 0x3e8

    .line 298
    div-long/2addr p1, v0

    long-to-int p2, p1

    const/4 p1, 0x5

    if-le p2, p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, p2

    .line 303
    :goto_0
    iget-object p2, p0, Lcom/baidu/mobads/production/e/e;->a:Lcom/baidu/mobads/production/e/b;

    invoke-static {p2}, Lcom/baidu/mobads/production/e/b;->k(Lcom/baidu/mobads/production/e/b;)Landroid/widget/TextView;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
