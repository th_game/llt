.class public abstract Lcom/baidu/mobads/production/b;
.super Lcom/baidu/mobads/openad/c/c;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/production/b$a;
    }
.end annotation


# static fields
.field public static a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

.field private static final x:[Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Landroid/os/Handler;

.field private C:Ljava/lang/Runnable;

.field private D:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field protected b:Ljava/lang/Boolean;

.field public d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field protected e:Landroid/widget/RelativeLayout;

.field protected f:Landroid/content/Context;

.field protected g:I

.field public h:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field protected i:Ljava/lang/String;

.field protected j:Lcom/baidu/mobads/production/u;

.field protected k:Lcom/baidu/mobads/vo/d;

.field protected l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

.field protected m:I

.field protected n:I

.field protected o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field protected p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected q:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected r:Ljava/lang/String;

.field protected s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

.field protected final t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field protected u:J

.field protected v:J

.field protected w:J

.field private y:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    .line 83
    sput-object v0, Lcom/baidu/mobads/production/b;->x:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 286
    invoke-direct {p0}, Lcom/baidu/mobads/openad/c/c;-><init>()V

    const/4 p1, 0x0

    .line 96
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 107
    iput p1, p0, Lcom/baidu/mobads/production/b;->g:I

    .line 118
    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->IDEL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v1, p0, Lcom/baidu/mobads/production/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    const/16 v1, 0x1388

    .line 121
    iput v1, p0, Lcom/baidu/mobads/production/b;->m:I

    .line 123
    iput p1, p0, Lcom/baidu/mobads/production/b;->n:I

    .line 129
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    .line 131
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 135
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->B:Landroid/os/Handler;

    .line 138
    iput-object v0, p0, Lcom/baidu/mobads/production/b;->C:Ljava/lang/Runnable;

    const-string p1, ""

    .line 139
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    .line 140
    iput-object v0, p0, Lcom/baidu/mobads/production/b;->s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 230
    new-instance p1, Lcom/baidu/mobads/production/c;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/production/c;-><init>(Lcom/baidu/mobads/production/b;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->D:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 278
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-void
.end method

.method private a(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 5

    .line 725
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "caching_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "download the splash picture successfully"

    if-eqz v0, :cond_4

    .line 727
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "local_creative_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 728
    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 729
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v2

    sget-object v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    const-string v4, "XAbstractAdProdTemplate"

    if-ne v2, v3, :cond_1

    .line 730
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 731
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processDlResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 733
    invoke-static {v0, v2}, Lcom/baidu/mobads/utils/w;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    .line 736
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 735
    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/production/b;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 738
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 740
    :cond_0
    iget-object v2, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    const-string v3, "vr"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 741
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 742
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v2

    .line 743
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/utils/f;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 744
    invoke-direct {p0, v2, v0}, Lcom/baidu/mobads/production/b;->a(Ljava/lang/String;Landroid/net/Uri;)V

    .line 747
    :cond_1
    :goto_0
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 748
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    .line 749
    invoke-virtual {v0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getSplash3DLocalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "\u80cc\u666f\u56fe\u7247\u6ca1\u6709\u7f13\u5b58\u5b8c\u6210"

    invoke-interface {v0, v4, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 752
    :cond_2
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    .line 755
    :cond_3
    :goto_1
    invoke-direct {p0, p2}, Lcom/baidu/mobads/production/b;->f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 756
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "vdieoCacheSucc"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_2

    .line 759
    :cond_4
    invoke-direct {p0, p2}, Lcom/baidu/mobads/production/b;->f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 760
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v2, "vdieoCacheFailed"

    invoke-direct {v0, v2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_5
    const/4 v0, 0x0

    .line 762
    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 763
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 764
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    .line 767
    :cond_6
    :goto_2
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/b;->b(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/b;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/baidu/mobads/production/b;->r()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/b;Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/b;->a(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .line 793
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/mobads/production/j;

    invoke-direct {v1, p0, p1, p2}, Lcom/baidu/mobads/production/j;-><init>(Lcom/baidu/mobads/production/b;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 810
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 623
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 624
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 625
    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;->f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 626
    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)Z
    .locals 1

    .line 602
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object p2

    .line 603
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p2

    .line 602
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 604
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;->f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method private b(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)Lcom/baidu/mobads/interfaces/IXAdContainer;
    .locals 5

    .line 1034
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "createAdContainer"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    sget-object v0, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 1038
    invoke-interface {v0, p1, v2}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->createXAdContainer(Lcom/baidu/mobads/interfaces/IXAdContainerContext;Ljava/util/HashMap;)Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1040
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createAdContainer() apk.version="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v2
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    .line 320
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/e;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/production/e;-><init>(Lcom/baidu/mobads/production/b;Landroid/content/Context;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private b(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 9

    .line 774
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b;->d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 777
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-string v4, "caching_time_consume"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 778
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v1

    .line 779
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "caching_result"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "success"

    goto :goto_0

    :cond_1
    const-string p1, "failed"

    .line 781
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    iget-object v4, p0, Lcom/baidu/mobads/production/b;->k:Lcom/baidu/mobads/vo/d;

    .line 783
    invoke-virtual {v4}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v6

    const/4 v4, 0x3

    new-array v7, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file_dl_"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v7, v4

    const/4 p1, 0x1

    aput-object v1, v7, p1

    const/4 p1, 0x2

    aput-object v0, v7, p1

    const-string v4, "383"

    move-object v5, p2

    .line 781
    invoke-virtual/range {v2 .. v7}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    return-void
.end method

.method private b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V
    .locals 7

    .line 633
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    iget-object v0, p0, Lcom/baidu/mobads/production/b;->k:Lcom/baidu/mobads/vo/d;

    .line 637
    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v5

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v3, "file_dl_failed_not_wifi"

    aput-object v3, v6, v0

    const/4 v0, 0x1

    aput-object p2, v6, v0

    const-string v3, "383"

    move-object v4, p1

    .line 635
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V
    .locals 9

    .line 699
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 700
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "jpg"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "png"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "jpeg"

    aput-object v3, v0, v2

    if-eqz p2, :cond_4

    .line 702
    array-length v2, p2

    if-lez v2, :cond_4

    const/4 v2, 0x0

    .line 703
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_4

    .line 704
    aget-object v3, p2, v2

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 705
    array-length v4, v3

    if-lez v4, :cond_3

    const/4 v4, 0x0

    .line 706
    :goto_1
    array-length v5, v3

    if-ge v4, v5, :cond_3

    .line 707
    aget-object v5, v3, v4

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".mp4"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 708
    aget-object p2, p2, v2

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    return-void

    .line 711
    :cond_0
    array-length v5, v0

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v5, :cond_2

    aget-object v7, v0, v6

    .line 712
    aget-object v8, v3, v4

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 713
    aget-object p2, p2, v2

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    return-void

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public static f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 973
    sget-object v0, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method private f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 613
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 614
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v1}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 9

    .line 660
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "cacheCreativeAsset"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v5

    .line 663
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 666
    :cond_0
    invoke-direct {p0, p1, v5}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 667
    invoke-direct {p0, p1, v5}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 670
    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 671
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 672
    invoke-static {v5}, Lcom/baidu/mobads/utils/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 673
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    .line 674
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdCreativeCacheManager()Lcom/baidu/mobads/utils/j;

    move-result-object v3

    .line 675
    invoke-virtual {v3, v6}, Lcom/baidu/mobads/utils/j;->a(Ljava/lang/String;)V

    .line 676
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    new-instance v8, Lcom/baidu/mobads/production/i;

    .line 677
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v8, p0, v0, p1}, Lcom/baidu/mobads/production/i;-><init>(Lcom/baidu/mobads/production/b;Landroid/os/Looper;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    .line 676
    invoke-virtual/range {v3 .. v8}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    return-void
.end method

.method private q()V
    .locals 4

    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->n:J

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 146
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;->a(Ljava/util/ArrayList;)V

    .line 147
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->d()Z

    move-result v0

    const-string v1, "XAdMouldeLoader ad-server requesting success"

    if-nez v0, :cond_0

    .line 149
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    .line 154
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v2

    .line 155
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 157
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    return-void

    .line 161
    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/baidu/mobads/production/b;->a(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    .line 162
    invoke-virtual {p0, v2, v0}, Lcom/baidu/mobads/production/b;->a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    if-eqz v2, :cond_3

    .line 165
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getSplash3DLocalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "\u80cc\u666f\u56fe\u7247\u6ca1\u6709\u7f13\u5b58\u5b8c\u6210"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "download the splash picture successfully"

    .line 168
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    .line 173
    :cond_3
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 175
    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void

    .line 179
    :cond_4
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 181
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->b(Ljava/lang/String;)V

    .line 184
    :cond_5
    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/b;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method private r()V
    .locals 1

    const/4 v0, 0x1

    .line 494
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    const-string v0, "XAdMouldeLoader load success"

    .line 495
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 1

    .line 1197
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1198
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onWindowVisibilityChanged(I)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 2

    .line 441
    sget-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    if-nez v0, :cond_1

    .line 442
    const-class v0, Lcom/baidu/mobads/f/g;

    monitor-enter v0

    .line 443
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    if-nez v1, :cond_0

    .line 444
    new-instance v1, Lcom/baidu/mobads/f/g;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/baidu/mobads/f/g;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    .line 446
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 453
    :cond_1
    :goto_0
    sget-object p1, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    if-eqz p1, :cond_2

    .line 454
    invoke-direct {p0}, Lcom/baidu/mobads/production/b;->r()V

    goto :goto_1

    .line 458
    :cond_2
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    if-eqz p1, :cond_3

    .line 459
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "BaiduXAdSDKContext.mApkLoader != null,load apk"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/f/g;

    new-instance v0, Lcom/baidu/mobads/production/h;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/production/h;-><init>(Lcom/baidu/mobads/production/b;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/f/g;->a(Lcom/baidu/mobads/f/g$c;)V

    goto :goto_1

    .line 487
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "BaiduXAdSDKContext.mApkLoader == null,not load apk"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 0

    .line 1328
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 873
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->l()V

    .line 874
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobads/production/b;->c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 875
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdLoaded"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 7

    const-string v0, "AdError"

    const-string v1, ""

    const-string v2, "XAbstractAdProdTemplate"

    .line 982
    :try_start_0
    iget-object v3, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v4, "processAllReadyOnUIThread()"

    invoke-interface {v3, v2, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/baidu/mobads/production/b;->v:J

    .line 985
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 987
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/baidu/mobads/production/b;->w:J

    .line 989
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-nez p1, :cond_0

    .line 990
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, "processAllReadyOnUIThread(), mAdContainer is null"

    aput-object v3, v1, v2

    invoke-interface {p1, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 991
    new-instance p1, Lcom/baidu/mobads/e/a;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto/16 :goto_0

    .line 993
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v3, "processAllReadyOnUIThread(), mAdContainer be created"

    invoke-interface {p1, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    const-string v3, "start"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/baidu/mobads/production/b;->u:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 996
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    const-string v3, "container_before_created"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/baidu/mobads/production/b;->v:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    const-string v3, "container_after_created"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/baidu/mobads/production/b;->w:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 998
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    iget-object v1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    invoke-interface {p1, v1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->setParameters(Ljava/util/HashMap;)V

    .line 1000
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getRemoteVersion()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    .line 1001
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processAllReadyOnUIThread(), mAdContainer be created, hasCalledLoadAtAppSide="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1003
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1001
    invoke-interface {p1, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1007
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    .line 1009
    :cond_1
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->g()V

    .line 1012
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1015
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1016
    iget-object v1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v2

    sget-object v3, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->PERMISSION_PROBLEM:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    .line 1017
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->genCompleteErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1016
    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    .line 1018
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "process all ready on UI Thread exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    .line 1020
    new-instance p1, Lcom/baidu/mobads/e/a;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    .line 895
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 942
    iput-object p2, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)V
    .locals 3

    .line 422
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "msg"

    .line 423
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    new-instance v1, Lcom/baidu/mobads/e/a;

    const-string v2, "AdError"

    invoke-direct {v1, v2, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 425
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V
.end method

.method protected a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;Ljava/lang/String;)V
    .locals 2

    const-string p2, ""

    .line 249
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v0, "message"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 251
    :try_start_0
    new-instance v0, Lcom/baidu/mobads/vo/c;

    invoke-direct {v0, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 252
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 253
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 254
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "mimetype"

    .line 255
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    .line 256
    invoke-direct {p0}, Lcom/baidu/mobads/production/b;->q()V

    .line 257
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->a()V

    goto :goto_0

    .line 260
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    .line 261
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    .line 262
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1, p2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "response json parsing error"

    .line 267
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    invoke-interface {v0, p2, p1, p2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->d(Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    .line 434
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error_message"

    .line 435
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    new-instance v1, Lcom/baidu/mobads/e/a;

    const-string v2, "AdError"

    invoke-direct {v1, v2, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 437
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1, v1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 1203
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1204
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1209
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1210
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/IXAdContainer;->processKeyEvent(ILandroid/view/KeyEvent;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected a(Lcom/baidu/mobads/vo/d;)Z
    .locals 3

    .line 824
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "doRequest()"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->a(Landroid/content/Context;)V

    .line 845
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/vo/d;)V

    const/4 p1, 0x1

    return p1
.end method

.method a(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 3

    .line 197
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 201
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/baidu/mobads/utils/j;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 202
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 205
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdCreativeCacheManager()Lcom/baidu/mobads/utils/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/j;->c(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->e()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 208
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {p1, v0}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 209
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdCreativeCacheManager()Lcom/baidu/mobads/utils/j;

    move-result-object v0

    .line 212
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/j;->c(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 215
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {p1, v0}, Lcom/baidu/mobads/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 213
    invoke-direct {p0, p2, p1}, Lcom/baidu/mobads/production/b;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V

    return v2

    .line 220
    :cond_1
    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception p1

    .line 225
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :cond_2
    return v1
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 576
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v2, :cond_1

    .line 577
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 578
    :cond_1
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v2, :cond_2

    .line 579
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    return-object v0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 880
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 882
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobads/production/b;->d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 883
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdStarted"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 9

    .line 912
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "handleAllReady"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    iget v0, p0, Lcom/baidu/mobads/production/b;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/baidu/mobads/production/b;->g:I

    .line 915
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 917
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 918
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 919
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    .line 920
    new-instance v6, Lcom/baidu/mobads/production/o;

    invoke-direct {v6, v2, p0}, Lcom/baidu/mobads/production/o;-><init>(Landroid/content/Context;Lcom/baidu/mobads/production/b;)V

    .line 921
    new-instance v0, Lcom/baidu/mobads/production/n;

    iget-object v5, p0, Lcom/baidu/mobads/production/b;->e:Landroid/widget/RelativeLayout;

    const/4 v8, 0x0

    move-object v1, v0

    move-object v7, p1

    invoke-direct/range {v1 .. v8}, Lcom/baidu/mobads/production/n;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Landroid/view/View;)V

    .line 922
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 923
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    .line 925
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/k;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/production/k;-><init>(Lcom/baidu/mobads/production/b;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 3

    .line 851
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->k:Lcom/baidu/mobads/vo/d;

    .line 852
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->k()V

    const/4 v0, 0x0

    .line 854
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    .line 856
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object v0

    .line 858
    :cond_0
    new-instance p1, Lcom/baidu/mobads/production/u;

    invoke-direct {p1}, Lcom/baidu/mobads/production/u;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    .line 860
    sput-object v0, Lcom/baidu/mobads/b/a;->b:Ljava/lang/String;

    .line 862
    new-instance p1, Lcom/baidu/mobads/openad/d/b;

    const-string v1, ""

    invoke-direct {p1, v0, v1}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 863
    iput v0, p1, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 864
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    iget-object v1, p0, Lcom/baidu/mobads/production/b;->D:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v2, "URLLoader.Load.Complete"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/u;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 865
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    iget-object v1, p0, Lcom/baidu/mobads/production/b;->D:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const-string v2, "URLLoader.Load.Error"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/u;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 866
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    iget v1, p0, Lcom/baidu/mobads/production/b;->m:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobads/production/b;->a(Lcom/baidu/mobads/openad/d/b;Lcom/baidu/mobads/production/u;I)V

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 499
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    .line 500
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 501
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->c(Ljava/lang/String;)V

    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected abstract c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
.end method

.method protected declared-synchronized c(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    .line 505
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doubleCheck:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", bfp="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", apk="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p1, :cond_0

    .line 508
    monitor-exit p0

    return-void

    .line 510
    :cond_0
    :try_start_1
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 512
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeAdContainerFactory(Lcom/baidu/mobads/interfaces/IXAdContainerFactory;)V

    .line 521
    :cond_1
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    .line 523
    :try_start_2
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 525
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    goto :goto_0

    .line 528
    :cond_2
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 529
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "doubleCheck IXAdResponseInfo is null, but isBFP4APPRequestSuccess is true"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 536
    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 532
    :try_start_4
    iget-object v1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAbstractAdProdTemplate"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 534
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 536
    :try_start_5
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto :goto_1

    :goto_2
    iget-object v1, p0, Lcom/baidu/mobads/production/b;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 539
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    monitor-exit p0

    goto :goto_5

    :goto_4
    throw p1

    :goto_5
    goto :goto_4
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    .line 585
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq v0, v1, :cond_1

    .line 586
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected abstract d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method protected d(Ljava/lang/String;)V
    .locals 2

    .line 899
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    .line 900
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "error_message"

    .line 901
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 902
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {p1, v1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 903
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    .line 597
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    const-string v1, "vr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 598
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected e(Ljava/lang/String;)V
    .locals 1

    .line 1221
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1222
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->t:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "\u4ee3\u7801\u4f4did(adPlaceId)\u4e0d\u53ef\u4ee5\u4e3a\u7a7a"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public e()Z
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->r:Ljava/lang/String;

    const-string v1, "3d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public abstract g()V
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 2

    .line 956
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 957
    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    .line 958
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->e:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 959
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 969
    sget-object v0, Lcom/baidu/mobads/production/b;->a:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method public getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;
    .locals 1

    .line 1279
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 950
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 951
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getCurrentAdInstance()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 1

    .line 1082
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object v0
.end method

.method public getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;
    .locals 1

    .line 965
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->A:Ljava/lang/String;

    return-object v0
.end method

.method public getParameter()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1070
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPlayheadTime()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getProdBase()Landroid/view/ViewGroup;
    .locals 1

    .line 1086
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->e:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 1058
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    return-object v0
.end method

.method public getRequestParameters()Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;
    .locals 1

    .line 1333
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    if-nez v0, :cond_0

    .line 1334
    new-instance v0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 1336
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->s:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-object v0
.end method

.method public getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    return-object v0
.end method

.method public getType()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object v0
.end method

.method protected abstract h()V
.end method

.method protected i()V
    .locals 2

    .line 1113
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1115
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 1117
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/m;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/m;-><init>(Lcom/baidu/mobads/production/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public isAdServerRequestingSuccess()Ljava/lang/Boolean;
    .locals 1

    .line 1287
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected j()V
    .locals 2

    .line 1139
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1141
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/b;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 1143
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/d;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/d;-><init>(Lcom/baidu/mobads/production/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .line 1164
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    if-eqz v0, :cond_0

    .line 1165
    invoke-virtual {v0}, Lcom/baidu/mobads/production/u;->removeAllListeners()V

    .line 1166
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->j:Lcom/baidu/mobads/production/u;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/u;->a()V

    :cond_0
    return-void
.end method

.method protected l()V
    .locals 2

    .line 1171
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->C:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1172
    iget-object v1, p0, Lcom/baidu/mobads/production/b;->B:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    .line 1174
    iput-object v0, p0, Lcom/baidu/mobads/production/b;->C:Ljava/lang/Runnable;

    return-void
.end method

.method public load()V
    .locals 2

    .line 1090
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1091
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    goto :goto_0

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method

.method protected m()V
    .locals 4

    .line 1178
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->C:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1179
    iget-object v1, p0, Lcom/baidu/mobads/production/b;->B:Landroid/os/Handler;

    iget v2, p0, Lcom/baidu/mobads/production/b;->m:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public n()V
    .locals 1

    .line 1184
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1185
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onAttachedToWindow()V

    :cond_0
    return-void
.end method

.method public o()V
    .locals 1

    .line 1191
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1192
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onDetachedFromWindow()V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    .line 1272
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1273
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->destroy()V

    .line 1275
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->exit()V

    return-void
.end method

.method public pause()V
    .locals 0

    .line 1109
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->i()V

    return-void
.end method

.method public resize()V
    .locals 2

    .line 1098
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1099
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/l;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/l;-><init>(Lcom/baidu/mobads/production/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 0

    .line 1135
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->j()V

    return-void
.end method

.method public setActivity(Landroid/content/Context;)V
    .locals 1

    .line 359
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    .line 360
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->c()V

    .line 361
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 363
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->h()V

    .line 366
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;)V

    .line 367
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 369
    new-instance p1, Lcom/baidu/mobads/production/g;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/production/g;-><init>(Lcom/baidu/mobads/production/b;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/b;->C:Ljava/lang/Runnable;

    .line 384
    iget-object p1, p0, Lcom/baidu/mobads/production/b;->f:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/q;->a()V

    return-void
.end method

.method public setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    .line 1283
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->z:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-void
.end method

.method public setAdSlotBase(Landroid/widget/RelativeLayout;)V
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->e:Landroid/widget/RelativeLayout;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->A:Ljava/lang/String;

    return-void
.end method

.method public setParameter(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1066
    iput-object p1, p0, Lcom/baidu/mobads/production/b;->p:Ljava/util/HashMap;

    return-void
.end method

.method public start()V
    .locals 1

    .line 1129
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1130
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->start()V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .line 1155
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "stop"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    iget-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1157
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->stop()V

    const/4 v0, 0x0

    .line 1158
    iput-object v0, p0, Lcom/baidu/mobads/production/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    :cond_0
    return-void
.end method
