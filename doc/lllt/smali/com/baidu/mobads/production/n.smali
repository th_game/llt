.class public Lcom/baidu/mobads/production/n;
.super Lcom/baidu/mobads/openad/c/c;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdContainerContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/production/n$a;
    }
.end annotation


# instance fields
.field public a:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

.field private b:Landroid/content/Context;

.field private d:Landroid/app/Activity;

.field private e:Landroid/widget/RelativeLayout;

.field private f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

.field private g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private h:Landroid/view/View;

.field private i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

.field private j:J

.field private k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Landroid/view/View;)V
    .locals 2

    .line 101
    invoke-direct {p0}, Lcom/baidu/mobads/openad/c/c;-><init>()V

    const-wide/16 v0, 0x0

    .line 157
    iput-wide v0, p0, Lcom/baidu/mobads/production/n;->j:J

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/n;->k:Ljava/util/HashMap;

    .line 102
    iput-object p1, p0, Lcom/baidu/mobads/production/n;->b:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/baidu/mobads/production/n;->d:Landroid/app/Activity;

    .line 105
    iput-object p3, p0, Lcom/baidu/mobads/production/n;->i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    .line 107
    iput-object p4, p0, Lcom/baidu/mobads/production/n;->e:Landroid/widget/RelativeLayout;

    .line 109
    iput-object p5, p0, Lcom/baidu/mobads/production/n;->a:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

    .line 111
    iput-object p6, p0, Lcom/baidu/mobads/production/n;->f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    .line 113
    invoke-interface {p6}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/n;->g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 115
    iput-object p7, p0, Lcom/baidu/mobads/production/n;->h:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public createOAdTimer(I)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer;
    .locals 1

    .line 283
    new-instance v0, Lcom/baidu/mobads/openad/e/a;

    invoke-direct {v0, p1}, Lcom/baidu/mobads/openad/e/a;-><init>(I)V

    return-object v0
.end method

.method public createOAdTimer(II)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer;
    .locals 1

    .line 288
    new-instance v0, Lcom/baidu/mobads/openad/e/a;

    invoke-direct {v0, p1, p2}, Lcom/baidu/mobads/openad/e/a;-><init>(II)V

    return-object v0
.end method

.method public fireAdMetrics(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 190
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 191
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->addParameters(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object p1

    .line 194
    new-instance p2, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {p2}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 196
    new-instance v0, Lcom/baidu/mobads/openad/d/b;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 197
    iput p1, v0, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 198
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->d:Landroid/app/Activity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/n;->e:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/baidu/mobads/production/n;->d:Landroid/app/Activity;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->d:Landroid/app/Activity;

    return-object v0
.end method

.method public getAdConstants()Lcom/baidu/mobads/interfaces/utils/IXAdConstants;
    .locals 1

    .line 203
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v0

    return-object v0
.end method

.method public getAdContainerListener()Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->a:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

    return-object v0
.end method

.method public getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object v0
.end method

.method public getAdLeadingView()Landroid/view/View;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->h:Landroid/view/View;

    return-object v0
.end method

.method public getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 1

    .line 258
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    return-object v0
.end method

.method public getAdProdBase()Landroid/widget/RelativeLayout;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->e:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getAdProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    return-object v0
.end method

.method public getAdResource()Lcom/baidu/mobads/interfaces/IXAdResource;
    .locals 1

    .line 263
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdResource()Lcom/baidu/mobads/utils/p;

    move-result-object v0

    return-object v0
.end method

.method public getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-object v0
.end method

.method public getAdUitls4URI()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 1

    .line 208
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Activity()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;
    .locals 1

    .line 233
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Bitmap()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;
    .locals 1

    .line 213
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Common()Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;
    .locals 1

    .line 238
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4IO()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;
    .locals 1

    .line 223
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Package()Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;
    .locals 1

    .line 228
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4System()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;
    .locals 1

    .line 243
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4View()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;
    .locals 1

    .line 218
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getViewUtils()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->b:Landroid/content/Context;

    return-object v0
.end method

.method public getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;
    .locals 1

    .line 253
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v0

    return-object v0
.end method

.method public getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 1

    .line 248
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;

    move-result-object p1

    return-object p1
.end method

.method public getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;
    .locals 1

    .line 268
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    return-object v0
.end method

.method public getProxyVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8085"

    return-object v0
.end method

.method public processCommand(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobads/production/n;->j:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/production/n;->j:J

    .line 163
    new-instance v0, Lcom/baidu/mobads/production/n$a;

    const-string v1, "process_command"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/baidu/mobads/production/n$a;-><init>(Lcom/baidu/mobads/production/n;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/n;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method public registerAdService(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->k:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    new-instance v0, Lcom/baidu/mobads/production/n$a;

    const-string v1, "regsiter_adservice"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/baidu/mobads/production/n$a;-><init>(Lcom/baidu/mobads/production/n;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/n;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method public unregisterAdService(Ljava/lang/String;)V
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/baidu/mobads/production/n;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    new-instance v0, Lcom/baidu/mobads/production/n$a;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "unregsiter_adservice"

    invoke-direct {v0, p0, v2, p1, v1}, Lcom/baidu/mobads/production/n$a;-><init>(Lcom/baidu/mobads/production/n;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/n;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method
