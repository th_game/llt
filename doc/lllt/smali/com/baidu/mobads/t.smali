.class Lcom/baidu/mobads/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/s;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/s;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iput-object p2, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdLoaded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 98
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i([Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AdStarted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdPresent()V

    goto/16 :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AdUserClick"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "AdLpClosed"

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdClick()V

    .line 103
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/SplashLpCloseListener;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    .line 104
    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    iget-object v0, v0, Lcom/baidu/mobads/production/g/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/command/c/a;->b()V

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v1, v1, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/command/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto/16 :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStopped"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/a;->removeAllListeners()V

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdDismissed()V

    goto/16 :goto_0

    .line 112
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdError"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/g/a;->removeAllListeners()V

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    .line 116
    invoke-interface {v2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-interface {v0, v1}, Lcom/baidu/mobads/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    .line 118
    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/SplashLpCloseListener;

    if-eqz v0, :cond_5

    .line 119
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/command/c/a;->removeEventListeners(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/command/c/a;->c()V

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/t;->b:Lcom/baidu/mobads/s;

    iget-object v0, v0, Lcom/baidu/mobads/s;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/SplashLpCloseListener;

    invoke-interface {v0}, Lcom/baidu/mobads/SplashLpCloseListener;->onLpClosed()V

    :cond_5
    :goto_0
    return-void
.end method
