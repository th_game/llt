.class Lcom/baidu/mobads/utils/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/baidu/mobads/utils/j;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/utils/j;Ljava/lang/String;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/baidu/mobads/utils/k;->b:Lcom/baidu/mobads/utils/j;

    iput-object p2, p0, Lcom/baidu/mobads/utils/k;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 63
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/baidu/mobads/utils/k;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 67
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 68
    array-length v1, v0

    if-lez v1, :cond_4

    .line 69
    new-instance v1, Lcom/baidu/mobads/utils/l;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/utils/l;-><init>(Lcom/baidu/mobads/utils/k;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const-wide/16 v1, 0x0

    .line 75
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    :goto_0
    if-ltz v3, :cond_4

    .line 76
    aget-object v4, v0, v3

    .line 77
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide v7, 0x9a7ec800L

    cmp-long v9, v5, v7

    if-lez v9, :cond_1

    .line 79
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 80
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v5, v1

    invoke-static {}, Lcom/baidu/mobads/utils/j;->a()J

    move-result-wide v7

    cmp-long v9, v5, v7

    if-lez v9, :cond_2

    .line 81
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 83
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-long/2addr v1, v4

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_4
    return-void
.end method
