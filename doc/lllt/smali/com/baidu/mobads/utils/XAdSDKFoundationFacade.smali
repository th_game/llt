.class public Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;


# instance fields
.field private a:Lcom/baidu/mobads/utils/j;

.field private b:Lcom/baidu/mobads/utils/p;

.field private c:Lcom/baidu/mobads/interfaces/utils/IBase64;

.field private d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

.field private f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

.field private g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

.field private h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

.field private i:Lcom/baidu/mobads/utils/o;

.field private j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

.field private k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

.field private l:Lcom/baidu/mobads/utils/f;

.field private m:Lcom/baidu/mobads/utils/i;

.field private n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

.field private p:Landroid/content/Context;

.field private q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    new-instance v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;-><init>()V

    sput-object v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lcom/baidu/mobads/utils/c;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/c;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->c:Lcom/baidu/mobads/interfaces/utils/IBase64;

    .line 81
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 82
    new-instance v0, Lcom/baidu/mobads/utils/p;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/p;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->b:Lcom/baidu/mobads/utils/p;

    .line 83
    new-instance v0, Lcom/baidu/mobads/utils/v;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/v;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    .line 84
    new-instance v0, Lcom/baidu/mobads/utils/e;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/e;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    .line 85
    new-instance v0, Lcom/baidu/mobads/utils/u;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/u;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 87
    new-instance v0, Lcom/baidu/mobads/utils/q;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/q;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 89
    new-instance v0, Lcom/baidu/mobads/utils/f;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/f;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->l:Lcom/baidu/mobads/utils/f;

    .line 90
    new-instance v0, Lcom/baidu/mobads/utils/m;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/m;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    .line 91
    new-instance v0, Lcom/baidu/mobads/utils/o;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/o;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->i:Lcom/baidu/mobads/utils/o;

    .line 92
    new-instance v0, Lcom/baidu/mobads/utils/d;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/d;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    .line 94
    new-instance v0, Lcom/baidu/mobads/utils/i;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/i;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->m:Lcom/baidu/mobads/utils/i;

    .line 96
    new-instance v0, Lcom/baidu/mobads/d/b;

    iget-object v1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/d/b;-><init>(Lcom/baidu/mobads/interfaces/utils/IXAdLogger;)V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;
    .locals 1

    .line 74
    sget-object v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    return-object v0
.end method


# virtual methods
.method public downloadApp(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 2

    .line 218
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    .line 220
    new-instance v0, Lcom/baidu/mobads/command/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, v1}, Lcom/baidu/mobads/command/a/a;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;)V

    invoke-virtual {v0}, Lcom/baidu/mobads/command/a/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, ""

    .line 223
    invoke-static {p1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public downloadAppSilence(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 232
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->downloadApp(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method public getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    return-object v0
.end method

.method public getAdConstants()Lcom/baidu/mobads/utils/i;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->m:Lcom/baidu/mobads/utils/i;

    return-object v0
.end method

.method public getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method public getAdCreativeCacheManager()Lcom/baidu/mobads/utils/j;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->a:Lcom/baidu/mobads/utils/j;

    return-object v0
.end method

.method public getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object v0
.end method

.method public getAdResource()Lcom/baidu/mobads/utils/p;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->b:Lcom/baidu/mobads/utils/p;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    return-object v0
.end method

.method public getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->c:Lcom/baidu/mobads/interfaces/utils/IBase64;

    return-object v0
.end method

.method public getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    return-object v0
.end method

.method public getCommonUtils()Lcom/baidu/mobads/utils/f;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->l:Lcom/baidu/mobads/utils/f;

    return-object v0
.end method

.method public getDownloaderManager()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object v0

    return-object v0
.end method

.method public getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 0

    .line 187
    invoke-static {p1}, Lcom/baidu/mobads/openad/b/d;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;

    move-result-object p1

    return-object p1
.end method

.method public getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    return-object v0
.end method

.method public getInstallIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 241
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/utils/o;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method public getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    return-object v0
.end method

.method public getPackageUtils()Lcom/baidu/mobads/utils/o;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->i:Lcom/baidu/mobads/utils/o;

    return-object v0
.end method

.method public getProxyVer()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8085"

    return-object v0
.end method

.method public getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    return-object v0
.end method

.method public getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-object v0
.end method

.method public getViewUtils()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    return-object v0
.end method

.method public initializeAdContainerFactory(Lcom/baidu/mobads/interfaces/IXAdContainerFactory;)V
    .locals 0

    if-nez p1, :cond_0

    .line 118
    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    :cond_0
    return-void
.end method

.method public initializeApplicationContext(Landroid/content/Context;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 106
    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    .line 108
    :cond_0
    new-instance p1, Lcom/baidu/mobads/utils/j;

    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/utils/j;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->a:Lcom/baidu/mobads/utils/j;

    return-void
.end method

.method public makeRequest(Ljava/lang/String;)V
    .locals 2

    .line 205
    new-instance v0, Lcom/baidu/mobads/openad/d/b;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 206
    iput p1, v0, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 207
    new-instance p1, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {p1}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 208
    invoke-virtual {p1, v0}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;)V

    return-void
.end method

.method public sendLog(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 236
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V

    return-void
.end method

.method public setMobileConfirmed(Ljava/lang/String;)V
    .locals 1

    .line 195
    invoke-static {p1}, Lcom/baidu/mobads/openad/b/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 197
    invoke-virtual {p1}, Lcom/baidu/mobads/openad/b/b;->a()Lcom/baidu/mobads/command/a;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 199
    iput-boolean v0, p1, Lcom/baidu/mobads/command/a;->s:Z

    :cond_0
    return-void
.end method
