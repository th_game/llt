.class public Lcom/baidu/mobads/openad/e/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer;


# static fields
.field private static c:Ljava/lang/String; = "OAdTimer"


# instance fields
.field protected a:I

.field private b:Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/util/Timer;

.field private h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private i:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/16 v0, 0x12c

    .line 48
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/openad/e/a;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 3

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12c

    .line 31
    iput v0, p0, Lcom/baidu/mobads/openad/e/a;->a:I

    .line 52
    iput p2, p0, Lcom/baidu/mobads/openad/e/a;->a:I

    .line 53
    iget p2, p0, Lcom/baidu/mobads/openad/e/a;->a:I

    div-int/2addr p1, p2

    .line 54
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RendererTimer(duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput p1, p0, Lcom/baidu/mobads/openad/e/a;->d:I

    .line 56
    iput p1, p0, Lcom/baidu/mobads/openad/e/a;->e:I

    .line 57
    new-instance p1, Ljava/util/Timer;

    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    .line 58
    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, -0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/e/a;I)I
    .locals 0

    .line 16
    iput p1, p0, Lcom/baidu/mobads/openad/e/a;->f:I

    return p1
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/e/a;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/openad/e/a;)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/baidu/mobads/openad/e/a;->b:Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/openad/e/a;)I
    .locals 0

    .line 16
    iget p0, p0, Lcom/baidu/mobads/openad/e/a;->d:I

    return p0
.end method

.method static synthetic d(Lcom/baidu/mobads/openad/e/a;)I
    .locals 0

    .line 16
    iget p0, p0, Lcom/baidu/mobads/openad/e/a;->e:I

    return p0
.end method

.method static synthetic e(Lcom/baidu/mobads/openad/e/a;)I
    .locals 0

    .line 16
    iget p0, p0, Lcom/baidu/mobads/openad/e/a;->f:I

    return p0
.end method

.method static synthetic f(Lcom/baidu/mobads/openad/e/a;)I
    .locals 2

    .line 16
    iget v0, p0, Lcom/baidu/mobads/openad/e/a;->e:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/baidu/mobads/openad/e/a;->e:I

    return v0
.end method


# virtual methods
.method public getCurrentCount()I
    .locals 1

    .line 145
    iget v0, p0, Lcom/baidu/mobads/openad/e/a;->f:I

    return v0
.end method

.method public getRepeatCount()I
    .locals 1

    .line 155
    iget v0, p0, Lcom/baidu/mobads/openad/e/a;->d:I

    return v0
.end method

.method public pause()V
    .locals 3

    .line 128
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    const-string v2, "pause"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public reset()V
    .locals 3

    .line 163
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    const-string v2, "reset"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 165
    iget v0, p0, Lcom/baidu/mobads/openad/e/a;->d:I

    iput v0, p0, Lcom/baidu/mobads/openad/e/a;->e:I

    return-void
.end method

.method public resume()V
    .locals 3

    .line 134
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    const-string v2, "resume"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public setEventHandler(Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/baidu/mobads/openad/e/a;->b:Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    return-void
.end method

.method public start()V
    .locals 7

    .line 71
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    const-string v2, "start"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 73
    new-instance v0, Lcom/baidu/mobads/openad/e/b;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/openad/e/b;-><init>(Lcom/baidu/mobads/openad/e/a;)V

    iput-object v0, p0, Lcom/baidu/mobads/openad/e/a;->i:Ljava/util/TimerTask;

    .line 93
    iget-object v1, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    iget-object v2, p0, Lcom/baidu/mobads/openad/e/a;->i:Ljava/util/TimerTask;

    iget v0, p0, Lcom/baidu/mobads/openad/e/a;->a:I

    int-to-long v5, v0

    const-wide/16 v3, 0x0

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method public stop()V
    .locals 3

    .line 101
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/openad/e/a;->c:Ljava/lang/String;

    const-string v2, "stop"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 110
    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->i:Ljava/util/TimerTask;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->i:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 113
    iput-object v1, p0, Lcom/baidu/mobads/openad/e/a;->i:Ljava/util/TimerTask;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->b:Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    if-eqz v0, :cond_1

    .line 116
    iput-object v1, p0, Lcom/baidu/mobads/openad/e/a;->b:Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 121
    iput-object v1, p0, Lcom/baidu/mobads/openad/e/a;->g:Ljava/util/Timer;

    .line 123
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
