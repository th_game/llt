.class Lcom/baidu/mobads/openad/e/b;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/e/a;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/openad/e/a;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->a(Lcom/baidu/mobads/openad/e/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->b(Lcom/baidu/mobads/openad/e/a;)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->c(Lcom/baidu/mobads/openad/e/a;)I

    move-result v1

    iget-object v2, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v2}, Lcom/baidu/mobads/openad/e/a;->d(Lcom/baidu/mobads/openad/e/a;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/baidu/mobads/openad/e/a;->a(Lcom/baidu/mobads/openad/e/a;I)I

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->b(Lcom/baidu/mobads/openad/e/a;)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v1}, Lcom/baidu/mobads/openad/e/a;->e(Lcom/baidu/mobads/openad/e/a;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;->onTimer(I)V

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->d(Lcom/baidu/mobads/openad/e/a;)I

    move-result v0

    if-lez v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->f(Lcom/baidu/mobads/openad/e/a;)I

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/e/a;->stop()V

    .line 87
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->b(Lcom/baidu/mobads/openad/e/a;)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/baidu/mobads/openad/e/b;->a:Lcom/baidu/mobads/openad/e/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/e/a;->b(Lcom/baidu/mobads/openad/e/a;)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer$EventHandler;->onTimerComplete()V

    :cond_3
    :goto_0
    return-void
.end method
