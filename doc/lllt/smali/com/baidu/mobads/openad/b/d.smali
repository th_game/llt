.class public Lcom/baidu/mobads/openad/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;


# static fields
.field private static b:Lcom/baidu/mobads/openad/b/d;


# instance fields
.field protected a:Landroid/content/Context;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/baidu/mobads/openad/a/c;

.field private e:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/d;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/openad/b/d;
    .locals 1

    .line 54
    sget-object v0, Lcom/baidu/mobads/openad/b/d;->b:Lcom/baidu/mobads/openad/b/d;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/baidu/mobads/openad/b/d;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/openad/b/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/baidu/mobads/openad/b/d;->b:Lcom/baidu/mobads/openad/b/d;

    .line 57
    :cond_0
    sget-object p0, Lcom/baidu/mobads/openad/b/d;->b:Lcom/baidu/mobads/openad/b/d;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public declared-synchronized createAdsApkDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 9

    monitor-enter p0

    .line 112
    :try_start_0
    new-instance v8, Lcom/baidu/mobads/openad/b/a;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    move-object v0, v8

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/openad/b/a;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0, p6, v8}, Lcom/baidu/mobads/openad/b/d;->a(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :try_start_1
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    if-nez p1, :cond_0

    .line 119
    new-instance p1, Lcom/baidu/mobads/openad/a/c;

    iget-object p2, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/baidu/mobads/openad/a/c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    .line 120
    new-instance p1, Lcom/baidu/mobads/openad/a/b;

    iget-object p2, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    invoke-direct {p1, p2}, Lcom/baidu/mobads/openad/a/b;-><init>(Lcom/baidu/mobads/openad/a/c;)V

    .line 121
    iget-object p2, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/openad/a/c;->a(Landroid/content/BroadcastReceiver;)V

    .line 123
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    const-string p2, "network_changed"

    new-instance p3, Lcom/baidu/mobads/openad/b/e;

    invoke-direct {p3, p0}, Lcom/baidu/mobads/openad/b/e;-><init>(Lcom/baidu/mobads/openad/b/d;)V

    invoke-virtual {p1, p2, p3}, Lcom/baidu/mobads/openad/a/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 198
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/d;->d:Lcom/baidu/mobads/openad/a/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/openad/a/c;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 201
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    const-string p3, "OAdDownloadManager"

    invoke-interface {p2, p3, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 204
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v8

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public createImgHttpDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/interfaces/download/IXAdStaticImgDownloader;
    .locals 2

    .line 216
    new-instance v0, Lcom/baidu/mobads/c/e;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/baidu/mobads/c/e;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public createSimpleFileDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 7

    .line 210
    new-instance v6, Lcom/baidu/mobads/openad/b/f;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/openad/b/f;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v6
.end method

.method public getAdsApkDownloader(Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 72
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getAllAdsApkDownloaderes()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;",
            ">;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 99
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 100
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 101
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 102
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 105
    :cond_1
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :goto_1
    throw v1

    :goto_2
    goto :goto_1
.end method

.method public removeAdsApkDownloader(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    .line 65
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 64
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public removeAllAdsApkDownloaderes()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 91
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resumeUndownloadedAfterRestartApp(J)V
    .locals 10

    const-string v0, "OAdDownloadManager"

    .line 222
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/baidu/mobads/command/a;->a(Landroid/content/Context;J)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 226
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-lez p2, :cond_4

    const/4 p2, 0x0

    .line 228
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_4

    .line 229
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 230
    invoke-static {v1}, Lcom/baidu/mobads/openad/b/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "pack["

    if-nez v2, :cond_3

    .line 231
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/d;->getAdsApkDownloader(Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 236
    :cond_1
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    .line 237
    invoke-static {v2, v1}, Lcom/baidu/mobads/command/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/command/a;

    move-result-object v2

    if-nez v2, :cond_2

    .line 239
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] has no local data, continue"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-interface {v2, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 244
    :cond_2
    new-instance v4, Ljava/net/URL;

    iget-object v1, v2, Lcom/baidu/mobads/command/a;->j:Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/baidu/mobads/command/a;->c:Ljava/lang/String;

    iget-object v6, v2, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    const/4 v7, 0x1

    iget-object v8, v2, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    iget-object v9, v2, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    move-object v3, p0

    .line 245
    invoke-virtual/range {v3 .. v9}, Lcom/baidu/mobads/openad/b/d;->createAdsApkDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object v1

    .line 247
    new-instance v3, Lcom/baidu/mobads/openad/b/b;

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/d;->a:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/baidu/mobads/openad/b/b;-><init>(Landroid/content/Context;Lcom/baidu/mobads/command/a;)V

    .line 248
    invoke-interface {v1, v3}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->addObserver(Ljava/util/Observer;)V

    .line 249
    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->start()V

    goto :goto_2

    .line 232
    :cond_3
    :goto_1
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] has been stated before, continue"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-interface {v2, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception p1

    .line 252
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    return-void
.end method
