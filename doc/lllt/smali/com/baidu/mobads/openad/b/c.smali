.class Lcom/baidu/mobads/openad/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

.field final synthetic b:Lcom/baidu/mobads/openad/b/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    iput-object p2, p0, Lcom/baidu/mobads/openad/b/c;->a:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "OAdApkDownloaderObserver"

    .line 208
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v1

    iget-object v1, v1, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_0

    .line 209
    invoke-static {}, Lcom/baidu/mobads/openad/b/b;->b()Landroid/app/NotificationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v2}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v2

    iget v2, v2, Lcom/baidu/mobads/command/a;->f:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 211
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/openad/b/b;->b()Landroid/app/NotificationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v2}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v2

    iget v2, v2, Lcom/baidu/mobads/command/a;->f:I

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/c;->a:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    invoke-static {v3, v4}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 212
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v1

    iget-object v1, v1, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_1

    .line 216
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "status >> error"

    invoke-interface {v1, v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v1

    iget-object v1, v1, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_2

    .line 219
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v1

    iget v1, v1, Lcom/baidu/mobads/command/a;->r:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 220
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u5f00\u59cb\u4e0b\u8f7d "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/c;->b:Lcom/baidu/mobads/openad/b/b;

    invoke-static {v3}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;

    move-result-object v3

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 225
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_0
    return-void
.end method
