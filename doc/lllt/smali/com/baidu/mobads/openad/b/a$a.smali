.class public Lcom/baidu/mobads/openad/b/a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/openad/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field protected a:I

.field protected b:Ljava/net/URL;

.field protected c:Ljava/lang/String;

.field protected d:I

.field protected e:I

.field protected f:I

.field protected g:Z

.field protected h:Ljava/lang/Thread;

.field final synthetic i:Lcom/baidu/mobads/openad/b/a;

.field private volatile j:Z

.field private volatile k:I

.field private l:Ljava/net/HttpURLConnection;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/openad/b/a;ILjava/net/URL;Ljava/lang/String;III)V
    .locals 0

    .line 614
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 607
    iput-boolean p1, p0, Lcom/baidu/mobads/openad/b/a$a;->j:Z

    .line 608
    iput p1, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I

    .line 615
    iput p2, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    .line 616
    iput-object p3, p0, Lcom/baidu/mobads/openad/b/a$a;->b:Ljava/net/URL;

    .line 617
    iput-object p4, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Ljava/lang/String;

    .line 618
    iput p5, p0, Lcom/baidu/mobads/openad/b/a$a;->d:I

    .line 619
    iput p6, p0, Lcom/baidu/mobads/openad/b/a$a;->e:I

    .line 620
    iput p7, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    .line 621
    iput-boolean p1, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/net/HttpURLConnection;)V
    .locals 0

    .line 658
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a$a;->l:Ljava/net/HttpURLConnection;

    return-void
.end method

.method public a()Z
    .locals 1

    .line 628
    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    return v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 635
    :try_start_0
    iput-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->j:Z

    .line 636
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->h:Ljava/lang/Thread;

    .line 637
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->h:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    .line 645
    :try_start_0
    iput-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->j:Z

    .line 646
    iget v1, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 4

    .line 672
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->h:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 673
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    goto :goto_0

    .line 675
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "DownloadThread"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "Warning: mThread in DownloadThread.waitFinish is null"

    aput-object v3, v1, v2

    .line 676
    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    :goto_0
    return-void
.end method

.method public run()V
    .locals 13

    .line 682
    iget v0, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 689
    :try_start_0
    iget v5, p0, Lcom/baidu/mobads/openad/b/a$a;->d:I

    iget v6, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/baidu/mobads/openad/b/a$a;->e:I

    if-lt v5, v6, :cond_0

    .line 690
    iput-boolean v3, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    move-object v5, v1

    move-object v6, v5

    goto/16 :goto_3

    .line 692
    :cond_0
    iget-object v5, p0, Lcom/baidu/mobads/openad/b/a$a;->l:Ljava/net/HttpURLConnection;

    if-nez v5, :cond_7

    .line 694
    iget-object v5, p0, Lcom/baidu/mobads/openad/b/a$a;->b:Ljava/net/URL;

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 696
    :try_start_1
    iget-object v6, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    iget-object v6, v6, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 698
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/baidu/mobads/openad/b/a$a;->d:I

    iget v8, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, p0, Lcom/baidu/mobads/openad/b/a$a;->e:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Range"

    .line 699
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bytes="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v7, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 701
    :cond_1
    iput v4, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    .line 705
    :goto_0
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 707
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v6

    .line 708
    iget v7, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-eq v0, v7, :cond_3

    .line 764
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 765
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] ver("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DownloadThread"

    .line 766
    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_2
    return-void

    .line 711
    :cond_3
    :try_start_2
    div-int/lit8 v6, v6, 0x64

    if-eq v6, v2, :cond_5

    .line 712
    iget-object v6, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v6}, Lcom/baidu/mobads/openad/b/a;->b()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 764
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 765
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] ver("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DownloadThread"

    .line 766
    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_4
    return-void

    .line 715
    :cond_5
    :try_start_3
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "text/html"

    .line 716
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 718
    iget-object v6, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v6}, Lcom/baidu/mobads/openad/b/a;->b()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 764
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 765
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] ver("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DownloadThread"

    .line 766
    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_6
    return-void

    .line 722
    :cond_7
    :try_start_4
    iget-object v5, p0, Lcom/baidu/mobads/openad/b/a$a;->l:Ljava/net/HttpURLConnection;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 723
    :try_start_5
    iput-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->l:Ljava/net/HttpURLConnection;

    .line 726
    :cond_8
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 728
    :try_start_6
    iget v7, p0, Lcom/baidu/mobads/openad/b/a$a;->d:I

    iget v8, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v7, v8

    .line 729
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    const-string v9, "DownloadThread"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tmpStartByte = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    new-instance v8, Ljava/io/RandomAccessFile;

    iget-object v9, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Ljava/lang/String;

    const-string v10, "rw"

    invoke-direct {v8, v9, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    int-to-long v9, v7

    .line 732
    :try_start_7
    invoke-virtual {v8, v9, v10}, Ljava/io/RandomAccessFile;->seek(J)V

    const v1, 0x19000

    new-array v9, v1, [B

    .line 737
    :goto_1
    iget-object v10, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    iget-object v10, v10, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v11, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v10, v11, :cond_b

    invoke-virtual {v6, v9, v4, v1}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_b

    iget v11, p0, Lcom/baidu/mobads/openad/b/a$a;->e:I

    if-ge v7, v11, :cond_b

    .line 739
    iget v11, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I

    if-eq v0, v11, :cond_9

    goto :goto_2

    .line 742
    :cond_9
    invoke-virtual {v8, v9, v4, v10}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 743
    iget v11, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v11, v10

    iput v11, p0, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v7, v10

    .line 745
    iget-object v11, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v11, v10}, Lcom/baidu/mobads/openad/b/a;->a(I)V

    .line 746
    monitor-enter p0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 747
    :try_start_8
    iget-boolean v10, p0, Lcom/baidu/mobads/openad/b/a$a;->j:Z

    if-eqz v10, :cond_a

    .line 748
    monitor-exit p0

    goto :goto_2

    .line 750
    :cond_a
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v1

    .line 753
    :cond_b
    :goto_2
    iget v1, p0, Lcom/baidu/mobads/openad/b/a$a;->e:I

    if-lt v7, v1, :cond_c

    .line 754
    iput-boolean v3, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_c
    move-object v1, v8

    .line 764
    :goto_3
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    .line 765
    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Thread["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v9, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, "] ver("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "DownloadThread"

    .line 766
    invoke-interface {v7, v8, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_d

    .line 771
    :try_start_a
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 773
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v7, v2, [Ljava/lang/Object;

    const-string v8, "DownloadThread"

    aput-object v8, v7, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v3

    invoke-interface {v1, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    :cond_d
    :goto_4
    if-eqz v6, :cond_e

    .line 779
    :try_start_b
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    .line 781
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v6, "DownloadThread"

    aput-object v6, v2, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    .line 784
    :cond_e
    :goto_5
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    goto/16 :goto_9

    :catchall_1
    move-exception v1

    move-object v7, v5

    move-object v5, v6

    move-object v6, v1

    move-object v1, v8

    goto/16 :goto_a

    :catch_2
    move-exception v1

    move-object v7, v5

    move-object v5, v6

    move-object v6, v1

    move-object v1, v8

    goto :goto_6

    :catchall_2
    move-exception v7

    move-object v12, v7

    move-object v7, v5

    move-object v5, v6

    move-object v6, v12

    goto/16 :goto_a

    :catch_3
    move-exception v7

    move-object v12, v7

    move-object v7, v5

    move-object v5, v6

    move-object v6, v12

    goto :goto_6

    :catchall_3
    move-exception v6

    move-object v7, v5

    move-object v5, v1

    goto/16 :goto_a

    :catch_4
    move-exception v6

    move-object v7, v5

    move-object v5, v1

    goto :goto_6

    :catchall_4
    move-exception v5

    move-object v7, v1

    move-object v6, v5

    move-object v5, v7

    goto/16 :goto_a

    :catch_5
    move-exception v5

    move-object v7, v1

    move-object v6, v5

    move-object v5, v7

    .line 758
    :goto_6
    :try_start_c
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    const-string v9, "DownloadThread"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8, v9, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    iget v6, p0, Lcom/baidu/mobads/openad/b/a$a;->k:I

    if-ne v0, v6, :cond_f

    .line 760
    iget-object v6, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v6}, Lcom/baidu/mobads/openad/b/a;->b()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 764
    :cond_f
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    .line 765
    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Thread["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v9, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, "] ver("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "DownloadThread"

    .line 766
    invoke-interface {v6, v8, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_10

    .line 771
    :try_start_d
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_7

    :catch_6
    move-exception v0

    .line 773
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v6, v2, [Ljava/lang/Object;

    const-string v8, "DownloadThread"

    aput-object v8, v6, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-interface {v1, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    :cond_10
    :goto_7
    if-eqz v5, :cond_11

    .line 779
    :try_start_e
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7

    goto :goto_8

    :catch_7
    move-exception v0

    .line 781
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "DownloadThread"

    aput-object v5, v2, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    .line 784
    :cond_11
    :goto_8
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_12
    :goto_9
    return-void

    :catchall_5
    move-exception v6

    .line 764
    :goto_a
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    .line 765
    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Thread["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, p0, Lcom/baidu/mobads/openad/b/a$a;->a:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "] ver("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") executed end; isFinished="

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a$a;->g:Z

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v9, "DownloadThread"

    .line 766
    invoke-interface {v8, v9, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_13

    .line 771
    :try_start_f
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8

    goto :goto_b

    :catch_8
    move-exception v0

    .line 773
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v8, v2, [Ljava/lang/Object;

    const-string v9, "DownloadThread"

    aput-object v9, v8, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v3

    invoke-interface {v1, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    :cond_13
    :goto_b
    if-eqz v5, :cond_14

    .line 779
    :try_start_10
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_9

    goto :goto_c

    :catch_9
    move-exception v0

    .line 781
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "DownloadThread"

    aput-object v5, v2, v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    .line 784
    :cond_14
    :goto_c
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 785
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->i:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_15
    goto :goto_e

    :goto_d
    throw v6

    :goto_e
    goto :goto_d
.end method
