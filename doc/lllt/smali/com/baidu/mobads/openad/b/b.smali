.class public Lcom/baidu/mobads/openad/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static a:Landroid/app/NotificationManager; = null

.field private static b:I = 0x276b

.field private static g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/baidu/mobads/openad/b/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lcom/baidu/mobads/command/a;

.field private d:Landroid/content/Context;

.field private e:Lcom/baidu/mobads/a/b;

.field private f:Ljava/lang/String;

.field private h:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/baidu/mobads/openad/b/b;->g:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/command/a;)V
    .locals 5

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    .line 56
    iput-object v0, p0, Lcom/baidu/mobads/openad/b/b;->e:Lcom/baidu/mobads/a/b;

    const-string v0, ""

    .line 58
    iput-object v0, p0, Lcom/baidu/mobads/openad/b/b;->f:Ljava/lang/String;

    .line 200
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/b;->h:Landroid/os/Handler;

    .line 89
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "OAdApkDownloaderObserver"

    const-string v2, "observer created"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    sget-object v0, Lcom/baidu/mobads/openad/b/b;->a:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    const-string v0, "notification"

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sput-object v0, Lcom/baidu/mobads/openad/b/b;->a:Landroid/app/NotificationManager;

    .line 93
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    .line 94
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v1, 0x2

    .line 98
    new-instance v2, Landroid/app/NotificationChannel;

    const-string v3, "down"

    const-string v4, "\u4e0b\u8f7d\u4fe1\u606f"

    invoke-direct {v2, v3, v4, v1}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 99
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 102
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    .line 104
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object p1, p1, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/baidu/mobads/openad/b/b;->a(Ljava/lang/String;Lcom/baidu/mobads/openad/b/b;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)Landroid/app/Notification;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/baidu/mobads/openad/b/b;->b(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)Landroid/app/Notification;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/b/b;)Lcom/baidu/mobads/command/a;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    return-object p0
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;
    .locals 2

    const-class v0, Lcom/baidu/mobads/openad/b/b;

    monitor-enter v0

    .line 68
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/openad/b/b;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/openad/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/String;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/baidu/mobads/openad/b/b;->d(Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;Lcom/baidu/mobads/openad/b/b;)V
    .locals 2

    const-class v0, Lcom/baidu/mobads/openad/b/b;

    monitor-enter v0

    .line 76
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/openad/b/b;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private b(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)Landroid/app/Notification;
    .locals 12

    .line 244
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v0, v0, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u6b63\u5728\u4e0b\u8f7d "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 255
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    const v4, 0x1080082

    const-string v5, ": "

    const v6, 0x1080081

    if-ne v2, v3, :cond_0

    .line 256
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v3}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, " \u70b9\u51fb\u8fd9\u91cc\u5b89\u88c5\u5e94\u7528"

    goto/16 :goto_2

    .line 259
    :cond_0
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_2

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->isPausedManually()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u76ee\u524d\u4e0d\u5728wifi\u7f51\u7edc\u4e0b\uff0c \u70b9\u51fb\u8fd9\u91cc\u7ee7\u7eed\u4e0b\u8f7d"

    goto :goto_0

    :cond_1
    const-string v1, "\u70b9\u51fb\u8fd9\u91cc\u7ee7\u7eed\u4e0b\u8f7d"

    :goto_0
    move-object v2, v1

    const-string v1, "\u5df2\u4e3a\u60a8\u6682\u505c\u4e0b\u8f7d\uff0c \u70b9\u51fb\u901a\u77e5\u680f\u7ee7\u7eed\u4e0b\u8f7d"

    goto/16 :goto_2

    .line 269
    :cond_2
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_3

    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v3}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, " \u7a0d\u540e\u70b9\u51fb\u8fd9\u91cc\u91cd\u65b0\u4e0b\u8f7d"

    goto :goto_2

    .line 273
    :cond_3
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_4

    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v3}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4e0b\u8f7d\u8fdb\u5ea6: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v3, v3, Lcom/baidu/mobads/command/a;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "%  \u5e94\u7528\u5927\u5c0f: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 276
    :cond_4
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_5

    .line 277
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v3}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getMessage()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_5
    const-string v2, ""

    :goto_1
    const v4, 0x1080081

    .line 281
    :goto_2
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    invoke-static {}, Lcom/baidu/mobads/AppActivity;->getActivityClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v5, 0x1

    const-string v6, "dealWithDownload"

    .line 282
    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 283
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->isPausedManually()Z

    move-result p1

    const-string v6, "pausedManually"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 284
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object p1, p1, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->getCode()I

    move-result p1

    const-string v6, "status"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object p1, p1, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    const-string v6, "pk"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v6, v6, Lcom/baidu/mobads/command/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v6, v6, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v6, "localApkPath"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "title"

    .line 287
    invoke-virtual {v3, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    .line 288
    invoke-virtual {v3, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    iget-object p1, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v6, v6, Lcom/baidu/mobads/command/a;->f:I

    const/high16 v7, 0x8000000

    invoke-static {p1, v6, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 294
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    const/4 v7, 0x0

    if-lt v3, v6, :cond_7

    .line 299
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x1a

    const/16 v8, 0x64

    if-lt v3, v6, :cond_6

    .line 300
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    new-instance v6, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    const-string v10, "down"

    invoke-direct {v6, v9, v10}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v6, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    .line 301
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    check-cast v3, Landroid/app/Notification$Builder;

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 302
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v0, v0, Lcom/baidu/mobads/command/a;->e:I

    .line 303
    invoke-virtual {p1, v8, v0, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    goto/16 :goto_3

    .line 305
    :cond_6
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    new-instance v6, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    invoke-direct {v6, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v6, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    .line 306
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    check-cast v3, Landroid/app/Notification$Builder;

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 307
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v0, v0, Lcom/baidu/mobads/command/a;->e:I

    .line 308
    invoke-virtual {p1, v8, v0, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    goto :goto_3

    .line 317
    :cond_7
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    if-nez v3, :cond_8

    .line 318
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    new-instance v8, Landroid/app/Notification;

    invoke-direct {v8}, Landroid/app/Notification;-><init>()V

    iput-object v8, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    .line 320
    :cond_8
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->h:Ljava/lang/Object;

    check-cast v3, Landroid/app/Notification;

    .line 321
    iput v4, v3, Landroid/app/Notification;->icon:I

    .line 322
    iget v4, v3, Landroid/app/Notification;->flags:I

    or-int/2addr v4, v6

    iput v4, v3, Landroid/app/Notification;->flags:I

    .line 323
    iput-object v1, v3, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 324
    iput-object p1, v3, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 327
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "setLatestEventInfo"

    const/4 v6, 0x4

    new-array v8, v6, [Ljava/lang/Class;

    const-class v9, Landroid/content/Context;

    aput-object v9, v8, v7

    const-class v9, Ljava/lang/CharSequence;

    aput-object v9, v8, v5

    const-class v9, Ljava/lang/CharSequence;

    const/4 v10, 0x2

    aput-object v9, v8, v10

    const-class v9, Landroid/app/PendingIntent;

    const/4 v11, 0x3

    aput-object v9, v8, v11

    invoke-virtual {v1, v4, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    .line 329
    iget-object v6, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    aput-object v6, v4, v7

    aput-object v0, v4, v5

    aput-object v2, v4, v10

    aput-object p1, v4, v11

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-object p1, v3

    :goto_3
    return-object p1
.end method

.method static synthetic b()Landroid/app/NotificationManager;
    .locals 1

    .line 47
    sget-object v0, Lcom/baidu/mobads/openad/b/b;->a:Landroid/app/NotificationManager;

    return-object v0
.end method

.method public static declared-synchronized b(Ljava/lang/String;)Lcom/baidu/mobads/openad/b/b;
    .locals 2

    const-class v0, Lcom/baidu/mobads/openad/b/b;

    monitor-enter v0

    .line 72
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/openad/b/b;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/openad/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized c(Ljava/lang/String;)I
    .locals 2

    const-class v0, Lcom/baidu/mobads/openad/b/b;

    monitor-enter v0

    .line 80
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/openad/b/b;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/openad/b/b;

    if-eqz p0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/b;->a()Lcom/baidu/mobads/command/a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/b;->a()Lcom/baidu/mobads/command/a;

    move-result-object p0

    iget p0, p0, Lcom/baidu/mobads/command/a;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return p0

    .line 84
    :cond_0
    :try_start_1
    sget p0, Lcom/baidu/mobads/openad/b/b;->b:I

    add-int/lit8 v1, p0, 0x1

    sput v1, Lcom/baidu/mobads/openad/b/b;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/command/a;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    return-object v0
.end method

.method public a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/b;->h:Landroid/os/Handler;

    new-instance v1, Lcom/baidu/mobads/openad/b/c;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/openad/b/c;-><init>(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 19

    move-object/from16 v0, p0

    .line 114
    move-object/from16 v1, p1

    check-cast v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 115
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 116
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v2

    .line 117
    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v3, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 119
    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iput-object v2, v3, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    .line 122
    :cond_0
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v6, "OAdApkDownloaderObserver"

    if-ne v2, v3, :cond_2

    .line 123
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-wide v2, v2, Lcom/baidu/mobads/command/a;->d:J

    const-wide/16 v7, 0x0

    cmp-long v9, v2, v7

    if-gez v9, :cond_1

    .line 124
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "download update---mExtraInfo.contentLength < 0"

    .line 125
    invoke-interface {v2, v6, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getFileSize()I

    move-result v3

    int-to-long v6, v3

    iput-wide v6, v2, Lcom/baidu/mobads/command/a;->d:J

    .line 127
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getTargetURL()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/a;->k:Ljava/lang/String;

    .line 128
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/command/a;->a(Landroid/content/Context;)V

    .line 129
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-wide v5, v5, Lcom/baidu/mobads/command/a;->d:J

    long-to-float v5, v5

    const/high16 v6, 0x49800000    # 1048576.0f

    div-float/2addr v5, v6

    .line 130
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "%.1fM"

    .line 129
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/baidu/mobads/openad/b/b;->f:Ljava/lang/String;

    .line 132
    :cond_1
    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getProgress()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    .line 133
    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getProgress()F

    move-result v2

    float-to-int v2, v2

    .line 134
    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v3, v3, Lcom/baidu/mobads/command/a;->e:I

    if-le v2, v3, :cond_9

    .line 136
    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iput v2, v3, Lcom/baidu/mobads/command/a;->e:I

    .line 137
    iget-boolean v2, v3, Lcom/baidu/mobads/command/a;->m:Z

    if-eqz v2, :cond_9

    .line 138
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V

    goto/16 :goto_1

    .line 143
    :cond_2
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_5

    .line 144
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v7

    .line 145
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/utils/q;

    .line 146
    iget-object v8, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    const/16 v9, 0x210

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v11, v3, Lcom/baidu/mobads/command/a;->q:Ljava/lang/String;

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v12, v3, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    .line 148
    invoke-virtual {v7, v3}, Lcom/baidu/mobads/utils/f;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/q;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v15

    sget-object v16, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v17, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget v18, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v10, "complete"

    const-string v14, ""

    .line 146
    invoke-virtual/range {v7 .. v18}, Lcom/baidu/mobads/utils/f;->sendDownloadAdLog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 152
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download success-->>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v6, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-boolean v2, v2, Lcom/baidu/mobads/command/a;->l:Z

    if-eqz v2, :cond_4

    .line 157
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const-string v4, "launch installing ............."

    invoke-interface {v3, v6, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v4, v4, Lcom/baidu/mobads/command/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v4, v4, Lcom/baidu/mobads/command/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 159
    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v4, v4, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 161
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v4

    iget-object v5, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    .line 162
    invoke-virtual {v4, v5, v3}, Lcom/baidu/mobads/utils/o;->getLocalApkFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils$ApkInfo;

    move-result-object v4

    .line 163
    iget-object v5, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v4, v4, Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils$ApkInfo;->packageName:Ljava/lang/String;

    iput-object v4, v5, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    .line 165
    :cond_3
    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->e:Lcom/baidu/mobads/a/b;

    if-nez v4, :cond_4

    .line 166
    new-instance v4, Lcom/baidu/mobads/a/a;

    iget-object v5, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-direct {v4, v5}, Lcom/baidu/mobads/a/a;-><init>(Lcom/baidu/mobads/command/a;)V

    .line 167
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    new-instance v3, Lcom/baidu/mobads/a/b;

    iget-object v6, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    iget-object v7, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v7, v7, Lcom/baidu/mobads/command/a;->i:Ljava/lang/String;

    invoke-direct {v3, v6, v7, v5, v2}, Lcom/baidu/mobads/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Z)V

    iput-object v3, v0, Lcom/baidu/mobads/openad/b/b;->e:Lcom/baidu/mobads/a/b;

    .line 169
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->e:Lcom/baidu/mobads/a/b;

    invoke-virtual {v2, v4}, Lcom/baidu/mobads/a/b;->a(Landroid/content/BroadcastReceiver;)V

    .line 170
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->e:Lcom/baidu/mobads/a/b;

    invoke-virtual {v2}, Lcom/baidu/mobads/a/b;->a()V

    .line 176
    :cond_4
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-virtual {v2, v3, v4}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Lcom/baidu/mobads/command/a;)V

    .line 177
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-static {v2}, Lcom/baidu/mobads/command/a/a;->a(Lcom/baidu/mobads/command/a;)Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 179
    invoke-static {}, Lcom/baidu/mobads/production/b;->f()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v3

    iget-object v4, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    .line 180
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getXMonitorActivation(Landroid/content/Context;Lcom/baidu/mobads/interfaces/utils/IXAdLogger;)Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;

    move-result-object v3

    .line 181
    invoke-interface {v3, v2}, Lcom/baidu/mobads/interfaces/download/activate/IXMonitorActivation;->addAppInfoForMonitor(Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;)V

    goto :goto_0

    .line 183
    :cond_5
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v2, Lcom/baidu/mobads/command/a;->g:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_6

    .line 184
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getTargetURL()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/a;->k:Ljava/lang/String;

    .line 185
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download failed-->>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 187
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/b/a;->a(Lcom/baidu/mobads/command/a;)V

    goto :goto_0

    .line 188
    :cond_6
    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object v2

    sget-object v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v2, v3, :cond_7

    .line 190
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget v3, v2, Lcom/baidu/mobads/command/a;->r:I

    add-int/2addr v3, v5

    iput v3, v2, Lcom/baidu/mobads/command/a;->r:I

    .line 193
    :cond_7
    :goto_0
    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-boolean v2, v2, Lcom/baidu/mobads/command/a;->m:Z

    if-eqz v2, :cond_8

    .line 194
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/openad/b/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;)V

    .line 196
    :cond_8
    iget-object v1, v0, Lcom/baidu/mobads/openad/b/b;->c:Lcom/baidu/mobads/command/a;

    iget-object v2, v0, Lcom/baidu/mobads/openad/b/b;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/command/a;->a(Landroid/content/Context;)V

    :cond_9
    :goto_1
    return-void
.end method
