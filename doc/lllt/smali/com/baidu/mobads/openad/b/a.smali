.class public Lcom/baidu/mobads/openad/b/a;
.super Ljava/util/Observable;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/openad/b/a$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/net/URL;

.field protected c:Ljava/net/URL;

.field protected d:Ljava/lang/String;

.field protected e:I

.field protected f:Ljava/lang/Boolean;

.field protected g:Ljava/lang/String;

.field protected h:I

.field protected i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

.field protected volatile j:I

.field protected k:I

.field protected l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/openad/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/baidu/mobads/openad/b/g;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 100
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    const/4 v0, 0x1

    .line 42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    const/4 v1, 0x0

    .line 64
    iput-object v1, p0, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    const/4 v1, 0x0

    .line 66
    iput-boolean v1, p0, Lcom/baidu/mobads/openad/b/a;->p:Z

    .line 88
    new-instance v2, Lcom/baidu/mobads/utils/u;

    invoke-direct {v2}, Lcom/baidu/mobads/utils/u;-><init>()V

    iput-object v2, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 102
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->a:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/net/URL;

    .line 104
    iput-object p3, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    .line 105
    iput p5, p0, Lcom/baidu/mobads/openad/b/a;->e:I

    if-eqz p4, :cond_0

    .line 108
    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 109
    iput-object p4, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x2f

    .line 112
    invoke-virtual {p1, p2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p2

    add-int/2addr p2, v0

    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    :goto_0
    const/4 p1, -0x1

    .line 114
    iput p1, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    .line 115
    sget-object p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->NONE:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 116
    iput v1, p0, Lcom/baidu/mobads/openad/b/a;->j:I

    .line 117
    iput v1, p0, Lcom/baidu/mobads/openad/b/a;->k:I

    .line 119
    iput-object p6, p0, Lcom/baidu/mobads/openad/b/a;->n:Ljava/lang/String;

    .line 120
    iput-object p7, p0, Lcom/baidu/mobads/openad/b/a;->o:Ljava/lang/String;

    .line 121
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-object p0
.end method

.method private b(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 3

    .line 795
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12d

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "Location"

    .line 797
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 798
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/net/URL;

    .line 799
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 p1, 0x2710

    .line 800
    :try_start_1
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/4 p1, 0x0

    .line 801
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const-string p1, "Range"

    const-string v1, "bytes=0-"

    .line 802
    invoke-virtual {v0, p1, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-exception p1

    move-object v2, v0

    move-object v0, p1

    move-object p1, v2

    goto :goto_1

    :catch_1
    move-exception v0

    .line 809
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object p1
.end method


# virtual methods
.method protected a()V
    .locals 0

    .line 273
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->setChanged()V

    .line 274
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->notifyObservers()V

    return-void
.end method

.method protected declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    .line 261
    :try_start_0
    iget v0, p0, Lcom/baidu/mobads/openad/b/a;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/baidu/mobads/openad/b/a;->j:I

    .line 262
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->getProgress()F

    move-result p1

    float-to-int p1, p1

    .line 263
    iget v0, p0, Lcom/baidu/mobads/openad/b/a;->k:I

    if-ge v0, p1, :cond_0

    .line 264
    iput p1, p0, Lcom/baidu/mobads/openad/b/a;->k:I

    .line 265
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 239
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->a()V

    return-void
.end method

.method protected a(Ljava/net/HttpURLConnection;)V
    .locals 20

    move-object/from16 v9, p0

    .line 305
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".tmp"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 309
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v12, ";complete="

    const-string v13, ";end ="

    const/4 v15, 0x1

    const-string v8, "Downloader"

    if-nez v0, :cond_b

    .line 310
    new-instance v0, Ljava/io/File;

    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    const/4 v1, 0x0

    .line 316
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    iget v0, v9, Lcom/baidu/mobads/openad/b/a;->h:I

    int-to-long v5, v0

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    .line 322
    :try_start_0
    new-instance v0, Lcom/baidu/mobads/openad/b/g;

    iget-object v3, v9, Lcom/baidu/mobads/openad/b/a;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/baidu/mobads/openad/b/g;-><init>(Landroid/content/Context;)V

    iput-object v0, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    .line 323
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    invoke-virtual {v0, v10, v11}, Lcom/baidu/mobads/openad/b/g;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 324
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 325
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 326
    :try_start_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 327
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/baidu/mobads/openad/b/h;

    .line 328
    invoke-virtual {v4}, Lcom/baidu/mobads/openad/b/h;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_0

    .line 331
    :cond_1
    invoke-virtual {v4}, Lcom/baidu/mobads/openad/b/h;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    .line 335
    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resume from db: start="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    invoke-virtual {v4}, Lcom/baidu/mobads/openad/b/h;->d()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/baidu/mobads/openad/b/h;->e()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    invoke-virtual {v4}, Lcom/baidu/mobads/openad/b/h;->a()I

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 336
    invoke-interface {v5, v8, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_2
    move-object v1, v3

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v3

    goto :goto_1

    :catch_1
    move-exception v0

    .line 342
    :goto_1
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    invoke-interface {v3, v8, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    if-eqz v1, :cond_5

    .line 346
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v15, :cond_4

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto/16 :goto_6

    .line 347
    :cond_5
    :goto_3
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 348
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 351
    :cond_6
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 353
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rwd"

    invoke-direct {v0, v2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 354
    iget v1, v9, Lcom/baidu/mobads/openad/b/a;->h:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 355
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 356
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downloader.init():  \u5efa\u7acb\u5b8crandom\u6587\u4ef6 ts:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, -0x1

    .line 368
    iget v2, v9, Lcom/baidu/mobads/openad/b/a;->e:I

    if-le v2, v15, :cond_8

    .line 370
    iget v3, v9, Lcom/baidu/mobads/openad/b/a;->h:I

    int-to-float v3, v3

    int-to-float v2, v2

    div-float/2addr v3, v2

    const/high16 v2, 0x47c80000    # 102400.0f

    div-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    const v3, 0x19000

    mul-int v16, v2, v3

    const/4 v2, 0x0

    .line 371
    :goto_4
    iget v3, v9, Lcom/baidu/mobads/openad/b/a;->h:I

    if-ge v1, v3, :cond_9

    add-int/lit8 v5, v1, 0x1

    add-int v1, v1, v16

    if-ge v1, v3, :cond_7

    move/from16 v17, v1

    goto :goto_5

    :cond_7
    move/from16 v17, v3

    :goto_5
    add-int/lit8 v18, v2, 0x1

    .line 380
    new-instance v7, Lcom/baidu/mobads/openad/b/h;

    const/16 v19, 0x0

    move-object v1, v7

    move/from16 v2, v18

    move-object v3, v10

    move-object v4, v11

    move/from16 v6, v17

    move-object v14, v7

    move/from16 v7, v19

    invoke-direct/range {v1 .. v7}, Lcom/baidu/mobads/openad/b/h;-><init>(ILjava/lang/String;Ljava/lang/String;III)V

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v1, v17

    goto :goto_4

    :cond_8
    const/4 v5, 0x0

    .line 385
    iget v6, v9, Lcom/baidu/mobads/openad/b/a;->h:I

    const/4 v2, 0x1

    .line 387
    new-instance v14, Lcom/baidu/mobads/openad/b/h;

    const/4 v7, 0x0

    move-object v1, v14

    move-object v3, v10

    move-object v4, v11

    invoke-direct/range {v1 .. v7}, Lcom/baidu/mobads/openad/b/h;-><init>(ILjava/lang/String;Ljava/lang/String;III)V

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_9
    :goto_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/baidu/mobads/openad/b/h;

    .line 391
    new-instance v7, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->c()I

    move-result v3

    iget-object v4, v9, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    .line 392
    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->d()I

    move-result v6

    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->e()I

    move-result v16

    .line 393
    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->a()I

    move-result v17

    move-object v1, v7

    move-object/from16 v2, p0

    move-object v15, v7

    move/from16 v7, v16

    move-object/from16 v16, v12

    move-object v12, v8

    move/from16 v8, v17

    invoke-direct/range {v1 .. v8}, Lcom/baidu/mobads/openad/b/a$a;-><init>(Lcom/baidu/mobads/openad/b/a;ILjava/net/URL;Ljava/lang/String;III)V

    .line 394
    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->d()I

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v14}, Lcom/baidu/mobads/openad/b/h;->a()I

    move-result v1

    if-nez v1, :cond_a

    move-object/from16 v1, p1

    .line 395
    invoke-virtual {v15, v1}, Lcom/baidu/mobads/openad/b/a$a;->a(Ljava/net/HttpURLConnection;)V

    goto :goto_8

    :cond_a
    move-object/from16 v1, p1

    .line 397
    :goto_8
    iget-object v2, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v8, v12

    move-object/from16 v12, v16

    const/4 v15, 0x1

    goto :goto_7

    :catch_2
    move-object v12, v8

    .line 359
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, " \u5efa\u7acb\u6587\u4ef6\u5931\u8d25:"

    invoke-interface {v0, v12, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    return-void

    :cond_b
    move-object/from16 v16, v12

    move-object v12, v8

    .line 402
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    const/4 v14, 0x0

    .line 403
    :goto_9
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_d

    .line 404
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    iget v1, v1, Lcom/baidu/mobads/openad/b/a$a;->f:I

    add-int/2addr v14, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    const/4 v14, 0x0

    .line 407
    :cond_d
    iput v14, v9, Lcom/baidu/mobads/openad/b/a;->j:I

    .line 408
    invoke-virtual/range {p0 .. p0}, Lcom/baidu/mobads/openad/b/a;->getProgress()F

    move-result v0

    float-to-int v0, v0

    iput v0, v9, Lcom/baidu/mobads/openad/b/a;->k:I

    .line 411
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    .line 413
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "Downloader starts unfinished threads and waits threads end"

    .line 414
    invoke-interface {v0, v12, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 415
    :goto_a
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_f

    .line 416
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v1

    if-nez v1, :cond_e

    .line 417
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    .line 418
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->b()V

    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    const/4 v0, 0x0

    .line 421
    :goto_b
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_11

    .line 422
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v1

    if-nez v1, :cond_10

    .line 423
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    .line 424
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->d()V

    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 428
    :cond_11
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v0, v1, :cond_15

    const/4 v0, 0x0

    .line 431
    :goto_c
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_13

    .line 432
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v1

    if-nez v1, :cond_12

    const/16 v18, 0x1

    goto :goto_d

    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_13
    const/16 v18, 0x0

    :goto_d
    if-eqz v18, :cond_14

    .line 438
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    goto :goto_e

    .line 440
    :cond_14
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Ljava/util/ArrayList;)V

    .line 441
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    goto :goto_e

    .line 444
    :cond_15
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v0, v1, :cond_16

    .line 445
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v9, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    goto :goto_e

    .line 446
    :cond_16
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v0, v1, :cond_17

    .line 447
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "Downloader is cancelled"

    invoke-interface {v0, v12, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e

    .line 449
    :cond_17
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v0, v1, :cond_18

    .line 450
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "Downloader is paused"

    invoke-interface {v0, v12, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_18
    :goto_e
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v0, v1, :cond_1c

    .line 455
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "save database now"

    invoke-interface {v0, v12, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 458
    :try_start_3
    iget-object v0, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    if-nez v0, :cond_19

    .line 459
    new-instance v0, Lcom/baidu/mobads/openad/b/g;

    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/openad/b/g;-><init>(Landroid/content/Context;)V

    iput-object v0, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    .line 461
    :cond_19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 462
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/baidu/mobads/openad/b/a$a;

    .line 463
    new-instance v15, Lcom/baidu/mobads/openad/b/h;

    iget v2, v14, Lcom/baidu/mobads/openad/b/a$a;->a:I

    iget v5, v14, Lcom/baidu/mobads/openad/b/a$a;->d:I

    iget v6, v14, Lcom/baidu/mobads/openad/b/a$a;->e:I

    iget v7, v14, Lcom/baidu/mobads/openad/b/a$a;->f:I

    move-object v1, v15

    move-object v3, v10

    move-object v4, v11

    invoke-direct/range {v1 .. v7}, Lcom/baidu/mobads/openad/b/h;-><init>(ILjava/lang/String;Ljava/lang/String;III)V

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 467
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "save to db: start="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v14, Lcom/baidu/mobads/openad/b/a$a;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v14, Lcom/baidu/mobads/openad/b/a$a;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object/from16 v3, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v14, Lcom/baidu/mobads/openad/b/a$a;->f:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 468
    invoke-interface {v1, v12, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v16, v3

    goto :goto_f

    .line 472
    :cond_1a
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    invoke-virtual {v1, v10, v11}, Lcom/baidu/mobads/openad/b/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 474
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/openad/b/g;->b(Ljava/util/List;)V

    goto :goto_10

    .line 476
    :cond_1b
    iget-object v1, v9, Lcom/baidu/mobads/openad/b/a;->m:Lcom/baidu/mobads/openad/b/g;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/openad/b/g;->a(Ljava/util/List;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_10

    :catch_3
    move-exception v0

    .line 480
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    invoke-interface {v1, v12, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1c
    :goto_10
    return-void
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/openad/b/a$a;",
            ">;)V"
        }
    .end annotation

    .line 588
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 589
    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method protected declared-synchronized b()V
    .locals 2

    monitor-enter p0

    .line 290
    :try_start_0
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    const/4 v0, 0x0

    .line 292
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/a$a;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    goto :goto_2

    :goto_1
    throw v0

    :goto_2
    goto :goto_1
.end method

.method public cancel()V
    .locals 4

    const-string v0, "Downloader"

    .line 175
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute Cancel; state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_3

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 179
    :goto_0
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 180
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/b/a$a;->c()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    :cond_2
    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 189
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "cancel exception"

    invoke-interface {v2, v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "apk download cancel failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public getFileSize()I
    .locals 1

    .line 206
    iget v0, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    return v0
.end method

.method public getOutputPath()Ljava/lang/String;
    .locals 2

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()F
    .locals 2

    .line 213
    iget v0, p0, Lcom/baidu/mobads/openad/b/a;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    return-object v0
.end method

.method public getTargetURL()Ljava/lang/String;
    .locals 1

    .line 821
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 824
    :cond_0
    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPausedManually()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/baidu/mobads/openad/b/a;->p:Z

    return v0
.end method

.method public pause()V
    .locals 4

    const-string v0, "Downloader"

    .line 130
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute Pause; state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->NONE:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_3

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 134
    :goto_0
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 135
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/b/a$a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 136
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/openad/b/a$a;

    invoke-virtual {v2}, Lcom/baidu/mobads/openad/b/a$a;->c()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    :cond_2
    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 144
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "pause exception"

    invoke-interface {v2, v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "apk download pause failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public removeObservers()V
    .locals 0

    .line 816
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/a;->deleteObservers()V

    return-void
.end method

.method public resume()V
    .locals 4

    const-string v0, "Downloader"

    .line 155
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute Resume; state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v1, v2, :cond_1

    .line 158
    :cond_0
    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    const/4 v1, 0x1

    .line 159
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/a;->setPausedManually(Z)V

    .line 160
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 161
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 164
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "resume exception"

    invoke-interface {v2, v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "apk download resume failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/b/a;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public run()V
    .locals 9

    const-string v0, "Accept-Ranges"

    .line 489
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    if-ge v1, v3, :cond_0

    goto :goto_0

    .line 573
    :cond_0
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/openad/b/a;->a(Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    .line 575
    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    .line 576
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "Downloader"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 493
    :cond_1
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/net/URL;

    .line 494
    invoke-interface {v1, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getHttpURLConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v2

    const-string v1, "Range"

    const-string v4, "bytes=0-"

    .line 495
    invoke-virtual {v2, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x2710

    .line 496
    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 500
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 501
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 502
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    const/16 v4, 0x12e

    const/4 v5, 0x0

    if-eq v1, v4, :cond_2

    const/16 v4, 0x12d

    if-ne v1, v4, :cond_3

    .line 506
    :cond_2
    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 507
    invoke-direct {p0, v2}, Lcom/baidu/mobads/openad/b/a;->b(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v2

    .line 508
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 511
    :cond_3
    div-int/lit8 v1, v1, 0x64

    const/4 v4, 0x2

    if-eq v1, v4, :cond_5

    .line 512
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_4

    .line 568
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_4
    return-void

    .line 515
    :cond_5
    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/html"

    .line 516
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 518
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_6

    .line 568
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_6
    return-void

    .line 522
    :cond_7
    :try_start_3
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    if-ge v1, v3, :cond_9

    .line 524
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_8

    .line 568
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_8
    return-void

    :cond_9
    const v4, 0x4e2000

    if-ge v1, v4, :cond_a

    .line 528
    :try_start_4
    iput v3, p0, Lcom/baidu/mobads/openad/b/a;->e:I

    .line 530
    :cond_a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v4

    iput-object v4, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    const-string v4, "mounted"

    .line 531
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 532
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_b

    .line 568
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_b
    return-void

    .line 535
    :cond_c
    :try_start_5
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 536
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    .line 537
    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v7

    iget-object v8, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/net/URL;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/baidu/mobads/utils/f;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".apk"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 538
    iput-object v4, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/lang/String;

    .line 539
    iput-object v6, p0, Lcom/baidu/mobads/openad/b/a;->g:Ljava/lang/String;

    .line 541
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 542
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 543
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 544
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_d

    .line 568
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_d
    return-void

    :cond_e
    :try_start_6
    const-string v4, "Content-Range"

    .line 549
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    .line 550
    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "none"

    .line 551
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 552
    :cond_f
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->f:Ljava/lang/Boolean;

    .line 553
    iput v3, p0, Lcom/baidu/mobads/openad/b/a;->e:I

    .line 555
    :cond_10
    iget v0, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_11

    .line 556
    iput v1, p0, Lcom/baidu/mobads/openad/b/a;->h:I

    .line 561
    :cond_11
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/openad/b/a;->a(Ljava/net/HttpURLConnection;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_12

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    .line 564
    :catch_1
    :try_start_7
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_12

    .line 568
    :goto_1
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_12
    :goto_2
    return-void

    .line 567
    :goto_3
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a;->q:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v1, :cond_13

    .line 568
    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_13
    throw v0
.end method

.method public setPausedManually(Z)V
    .locals 0

    .line 73
    iput-boolean p1, p0, Lcom/baidu/mobads/openad/b/a;->p:Z

    return-void
.end method

.method public start()V
    .locals 3

    .line 246
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "execute Start; state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Downloader"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a;->i:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->NONE:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v0, v1, :cond_0

    .line 249
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->INITING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    const/4 v0, 0x1

    .line 251
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/a;->setPausedManually(Z)V

    .line 252
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 253
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method
