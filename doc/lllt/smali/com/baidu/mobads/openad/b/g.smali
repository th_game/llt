.class public Lcom/baidu/mobads/openad/b/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/baidu/mobads/openad/b/i;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/baidu/mobads/openad/b/i;

    invoke-direct {v0, p1}, Lcom/baidu/mobads/openad/b/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/g;->a:Lcom/baidu/mobads/openad/b/i;

    .line 27
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/g;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/openad/b/h;",
            ">;)V"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/g;->a:Lcom/baidu/mobads/openad/b/i;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/b/i;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 49
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/h;

    const-string v2, "insert into download_info(thread_id,url,local_file,start_pos,end_pos,compelete_size,process_name) values (?,?,?,?,?,?,?)"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    .line 52
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->f()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x2

    aput-object v4, v3, v7

    const/4 v4, 0x3

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->d()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    const/4 v4, 0x4

    .line 53
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->e()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    const/4 v4, 0x5

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x6

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/g;->b:Ljava/lang/String;

    aput-object v4, v3, v1

    .line 55
    :try_start_0
    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 57
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "OAdSqlLiteAccessObj"

    aput-object v4, v3, v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/g;->a:Lcom/baidu/mobads/openad/b/i;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/b/i;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    .line 37
    iget-object p2, p0, Lcom/baidu/mobads/openad/b/g;->b:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object p2, v1, v3

    const-string p2, "select count(*)  from download_info where url=? and local_file=? and process_name=?"

    invoke-virtual {v0, p2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    .line 38
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 39
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 40
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/openad/b/h;",
            ">;"
        }
    .end annotation

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/g;->a:Lcom/baidu/mobads/openad/b/i;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/i;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    aput-object p2, v3, p1

    .line 70
    iget-object p2, p0, Lcom/baidu/mobads/openad/b/g;->b:Ljava/lang/String;

    const/4 v5, 0x2

    aput-object p2, v3, v5

    const-string p2, "select thread_id, url, local_file, start_pos, end_pos,compelete_size from download_info where url=? and local_file=? and process_name=?"

    invoke-virtual {v1, p2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    .line 71
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    new-instance v1, Lcom/baidu/mobads/openad/b/h;

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {p2, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 73
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v3, 0x4

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v3, 0x5

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/baidu/mobads/openad/b/h;-><init>(ILjava/lang/String;Ljava/lang/String;III)V

    .line 74
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public b(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/openad/b/h;",
            ">;)V"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/g;->a:Lcom/baidu/mobads/openad/b/i;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/b/i;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 85
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/openad/b/h;

    const-string v2, "update download_info set compelete_size=? where thread_id=? and url=? and local_file=? and process_name=?"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    .line 88
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x2

    aput-object v4, v3, v7

    const/4 v4, 0x3

    .line 89
    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/h;->f()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x4

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/g;->b:Ljava/lang/String;

    aput-object v4, v3, v1

    .line 91
    :try_start_0
    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 93
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "OAdSqlLiteAccessObj"

    aput-object v4, v3, v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    return-void
.end method
