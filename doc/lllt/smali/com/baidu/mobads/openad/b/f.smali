.class public Lcom/baidu/mobads/openad/b/f;
.super Ljava/util/Observable;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
.implements Ljava/lang/Runnable;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/net/URL;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:I

.field protected f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

.field protected g:I

.field protected h:I

.field private i:Z

.field private j:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .line 76
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    const/4 v0, 0x0

    .line 59
    iput-boolean v0, p0, Lcom/baidu/mobads/openad/b/f;->i:Z

    .line 61
    new-instance v1, Lcom/baidu/mobads/utils/u;

    invoke-direct {v1}, Lcom/baidu/mobads/utils/u;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/openad/b/f;->j:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 77
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/f;->a:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/baidu/mobads/openad/b/f;->b:Ljava/net/URL;

    .line 79
    iput-object p3, p0, Lcom/baidu/mobads/openad/b/f;->c:Ljava/lang/String;

    .line 80
    iput-boolean p5, p0, Lcom/baidu/mobads/openad/b/f;->i:Z

    if-eqz p4, :cond_0

    .line 82
    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 83
    iput-object p4, p0, Lcom/baidu/mobads/openad/b/f;->d:Ljava/lang/String;

    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x2f

    .line 86
    invoke-virtual {p1, p2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/f;->d:Ljava/lang/String;

    :goto_0
    const/4 p1, -0x1

    .line 88
    iput p1, p0, Lcom/baidu/mobads/openad/b/f;->e:I

    .line 89
    sget-object p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    iput-object p1, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 90
    iput v0, p0, Lcom/baidu/mobads/openad/b/f;->g:I

    .line 91
    iput v0, p0, Lcom/baidu/mobads/openad/b/f;->h:I

    return-void
.end method

.method private d()V
    .locals 1

    .line 189
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/f;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 291
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/openad/b/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/f;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/f;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 292
    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method protected a(IF)V
    .locals 0

    .line 171
    iget p2, p0, Lcom/baidu/mobads/openad/b/f;->g:I

    add-int/2addr p2, p1

    iput p2, p0, Lcom/baidu/mobads/openad/b/f;->g:I

    .line 172
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->c()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 156
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->c()V

    return-void
.end method

.method protected b()V
    .locals 1

    .line 163
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 164
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected c()V
    .locals 0

    .line 179
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->setChanged()V

    .line 180
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->notifyObservers()V

    return-void
.end method

.method public cancel()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public getFileSize()I
    .locals 1

    .line 134
    iget v0, p0, Lcom/baidu/mobads/openad/b/f;->e:I

    return v0
.end method

.method public getOutputPath()Ljava/lang/String;
    .locals 2

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/openad/b/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProgress()F
    .locals 2

    .line 141
    iget v0, p0, Lcom/baidu/mobads/openad/b/f;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/baidu/mobads/openad/b/f;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    return-object v0
.end method

.method public getTargetURL()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/f;->b:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPausedManually()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public removeObservers()V
    .locals 0

    return-void
.end method

.method public resume()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public run()V
    .locals 15

    const-string v0, "OAdSimpleFileDownloader"

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 201
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v5

    iget-object v6, p0, Lcom/baidu/mobads/openad/b/f;->b:Ljava/net/URL;

    invoke-interface {v5, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getHttpURLConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/16 v6, 0x2710

    .line 202
    :try_start_1
    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 203
    invoke-virtual {v5, v2}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 204
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 207
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v6

    .line 208
    div-int/lit8 v6, v6, 0x64

    if-eq v6, v3, :cond_0

    .line 209
    invoke-direct {p0}, Lcom/baidu/mobads/openad/b/f;->d()V

    .line 211
    :cond_0
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v6

    if-lez v6, :cond_1

    .line 213
    iput v6, p0, Lcom/baidu/mobads/openad/b/f;->e:I

    .line 217
    :cond_1
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/baidu/mobads/openad/b/f;->c:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 219
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 221
    :cond_2
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 222
    :try_start_2
    new-instance v7, Ljava/io/BufferedOutputStream;

    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->getOutputPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/16 v8, 0x2800

    :try_start_3
    new-array v9, v8, [B

    .line 224
    iget-boolean v10, p0, Lcom/baidu/mobads/openad/b/f;->i:Z

    if-eqz v10, :cond_3

    .line 225
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v1, v10

    :cond_3
    const/4 v10, 0x0

    .line 231
    :goto_0
    iget-object v11, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v12, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v11, v12, :cond_5

    invoke-virtual {v6, v9, v4, v8}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_5

    .line 232
    invoke-virtual {v7, v9, v4, v11}, Ljava/io/BufferedOutputStream;->write([BII)V

    if-eqz v1, :cond_4

    .line 234
    invoke-virtual {v1, v9, v4, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_4
    add-int/2addr v10, v11

    int-to-float v12, v10

    .line 238
    iget v13, p0, Lcom/baidu/mobads/openad/b/f;->e:I

    int-to-float v13, v13

    div-float/2addr v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/baidu/mobads/openad/b/f;->a(IF)V

    goto :goto_0

    .line 240
    :cond_5
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->flush()V

    if-eqz v1, :cond_6

    .line 243
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 247
    :cond_6
    iget-object v8, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v9, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v8, v9, :cond_7

    .line 249
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->a()V

    .line 251
    sget-object v8, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v8}, Lcom/baidu/mobads/openad/b/f;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    goto :goto_1

    .line 252
    :cond_7
    iget-object v8, p0, Lcom/baidu/mobads/openad/b/f;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v8, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 260
    :goto_1
    :try_start_4
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_0
    move-exception v7

    .line 262
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v2

    invoke-interface {v8, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :goto_2
    if-eqz v1, :cond_8

    .line 267
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    :catch_1
    move-exception v1

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v7

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v0, v8, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v8, v2

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 275
    :cond_8
    :goto_3
    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_4

    :catch_2
    move-exception v1

    .line 277
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-interface {v6, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 280
    :goto_4
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/f;->j:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_c

    .line 281
    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    goto/16 :goto_9

    :catchall_0
    move-exception v8

    move-object v14, v5

    move-object v5, v1

    move-object v1, v7

    move-object v7, v6

    move-object v6, v8

    move-object v8, v14

    goto/16 :goto_a

    :catch_3
    move-exception v8

    move-object v14, v5

    move-object v5, v1

    move-object v1, v7

    move-object v7, v6

    move-object v6, v8

    move-object v8, v14

    goto :goto_5

    :catchall_1
    move-exception v7

    move-object v8, v5

    move-object v5, v1

    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    goto/16 :goto_a

    :catch_4
    move-exception v7

    move-object v8, v5

    move-object v5, v1

    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    goto :goto_5

    :catchall_2
    move-exception v6

    move-object v7, v1

    move-object v8, v5

    move-object v5, v7

    goto/16 :goto_a

    :catch_5
    move-exception v6

    move-object v7, v1

    move-object v8, v5

    move-object v5, v7

    goto :goto_5

    :catchall_3
    move-exception v6

    move-object v5, v1

    move-object v7, v5

    move-object v8, v7

    goto/16 :goto_a

    :catch_6
    move-exception v6

    move-object v5, v1

    move-object v7, v5

    move-object v8, v7

    .line 255
    :goto_5
    :try_start_7
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v9

    invoke-virtual {v9}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v9

    new-array v10, v3, [Ljava/lang/Object;

    aput-object v0, v10, v4

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v2

    invoke-interface {v9, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 256
    invoke-direct {p0}, Lcom/baidu/mobads/openad/b/f;->d()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    if-eqz v1, :cond_9

    .line 260
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_6

    :catch_7
    move-exception v1

    .line 262
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v2

    invoke-interface {v6, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_9
    :goto_6
    if-eqz v5, :cond_a

    .line 267
    :try_start_9
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_7

    :catch_8
    move-exception v1

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    invoke-interface {v5, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_a
    :goto_7
    if-eqz v7, :cond_b

    .line 275
    :try_start_a
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_8

    :catch_9
    move-exception v1

    .line 277
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-interface {v5, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 280
    :cond_b
    :goto_8
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/f;->j:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_c

    .line 281
    invoke-interface {v0, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_c
    :goto_9
    return-void

    :catchall_4
    move-exception v6

    :goto_a
    if-eqz v1, :cond_d

    .line 260
    :try_start_b
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_b

    :catch_a
    move-exception v1

    .line 262
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v9

    invoke-virtual {v9}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v9

    new-array v10, v3, [Ljava/lang/Object;

    aput-object v0, v10, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v2

    invoke-interface {v9, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_d
    :goto_b
    if-eqz v5, :cond_e

    .line 267
    :try_start_c
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b

    goto :goto_c

    :catch_b
    move-exception v1

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v5

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v2

    invoke-interface {v5, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_e
    :goto_c
    if-eqz v7, :cond_f

    .line 275
    :try_start_d
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_c

    goto :goto_d

    :catch_c
    move-exception v1

    .line 277
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-interface {v5, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 280
    :cond_f
    :goto_d
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/f;->j:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_10

    .line 281
    invoke-interface {v0, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_10
    goto :goto_f

    :goto_e
    throw v6

    :goto_f
    goto :goto_e
.end method

.method public setPausedManually(Z)V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 1

    .line 119
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/b/f;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    .line 120
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/b/f;->b()V

    return-void
.end method
