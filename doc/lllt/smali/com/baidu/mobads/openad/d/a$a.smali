.class Lcom/baidu/mobads/openad/d/a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/openad/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/d/a;

.field private b:Lcom/baidu/mobads/openad/d/b;

.field private c:D


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/openad/d/a;Lcom/baidu/mobads/openad/d/b;D)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p2, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    .line 109
    iput-wide p3, p0, Lcom/baidu/mobads/openad/d/a$a;->c:D

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const-string v0, "OAdURLLoader"

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 118
    :try_start_0
    iget-object v5, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-wide v5, v5, Lcom/baidu/mobads/openad/d/b;->c:J

    const-wide/16 v7, 0x0

    cmp-long v9, v5, v7

    if-lez v9, :cond_0

    .line 119
    iget-object v5, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-wide v5, v5, Lcom/baidu/mobads/openad/d/b;->c:J

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    .line 123
    :cond_0
    new-instance v5, Ljava/net/URL;

    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-object v6, v6, Lcom/baidu/mobads/openad/d/b;->a:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v6

    .line 125
    invoke-interface {v6, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getHttpURLConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 126
    :try_start_1
    iget-wide v6, p0, Lcom/baidu/mobads/openad/d/a$a;->c:D

    double-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 129
    invoke-virtual {v5, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 130
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-object v6, v6, Lcom/baidu/mobads/openad/d/b;->b:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-object v6, v6, Lcom/baidu/mobads/openad/d/b;->b:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    const-string v6, "User-Agent"

    .line 131
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-object v7, v7, Lcom/baidu/mobads/openad/d/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v6, "Content-type"

    .line 133
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget-object v7, v7, Lcom/baidu/mobads/openad/d/b;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "Connection"

    const-string v7, "keep-alive"

    .line 134
    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "Cache-Control"

    const-string v7, "no-cache"

    .line 135
    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    sget-object v6, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0x8

    if-ge v6, v7, :cond_2

    const-string v6, "http.keepAlive"

    const-string v7, "false"

    .line 138
    invoke-static {v6, v7}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 141
    :cond_2
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget v6, v6, Lcom/baidu/mobads/openad/d/b;->e:I

    if-ne v6, v4, :cond_4

    const-string v6, "GET"

    .line 142
    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 149
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    .line 150
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v6}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GET connect code :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v6}, Lcom/baidu/mobads/openad/d/a;->b(Lcom/baidu/mobads/openad/d/a;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_3

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 154
    invoke-static {v1}, Lcom/baidu/mobads/openad/d/a;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 155
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    new-instance v8, Lcom/baidu/mobads/openad/c/d;

    const-string v9, "URLLoader.Load.Complete"

    iget-object v10, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    invoke-virtual {v10}, Lcom/baidu/mobads/openad/d/b;->a()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v6, v10}, Lcom/baidu/mobads/openad/c/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/baidu/mobads/openad/d/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    .line 157
    :cond_3
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    goto :goto_0

    .line 159
    :cond_4
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    iget v6, v6, Lcom/baidu/mobads/openad/d/b;->e:I

    if-nez v6, :cond_6

    const-string v6, "POST"

    .line 160
    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v5, v4}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 162
    invoke-virtual {v5, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 164
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    invoke-virtual {v6}, Lcom/baidu/mobads/openad/d/b;->b()Landroid/net/Uri$Builder;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 165
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->b:Lcom/baidu/mobads/openad/d/b;

    invoke-virtual {v6}, Lcom/baidu/mobads/openad/d/b;->b()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v6

    .line 166
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    .line 167
    new-instance v8, Ljava/io/BufferedWriter;

    new-instance v9, Ljava/io/OutputStreamWriter;

    const-string v10, "UTF-8"

    invoke-direct {v9, v7, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v8, v9}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 169
    invoke-virtual {v8, v6}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v8}, Ljava/io/BufferedWriter;->flush()V

    .line 171
    invoke-virtual {v8}, Ljava/io/BufferedWriter;->close()V

    .line 172
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 174
    :cond_5
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 175
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 176
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v6}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Post connect code :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_6
    :goto_0
    if-eqz v1, :cond_7

    .line 189
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    .line 195
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v6}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {v6, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 198
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/d/a;->d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_a

    goto/16 :goto_4

    :catchall_1
    move-exception v6

    goto :goto_2

    :catchall_2
    move-exception v5

    move-object v6, v5

    move-object v5, v1

    .line 179
    :goto_2
    :try_start_3
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v7}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v7

    new-array v8, v2, [Ljava/lang/Object;

    aput-object v0, v8, v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "load throwable :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 180
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v7}, Lcom/baidu/mobads/openad/d/a;->b(Lcom/baidu/mobads/openad/d/a;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v7}, Lcom/baidu/mobads/openad/d/a;->c(Lcom/baidu/mobads/openad/d/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v7

    if-nez v7, :cond_8

    .line 181
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    new-instance v8, Lcom/baidu/mobads/openad/c/a;

    const-string v9, "URLLoader.Load.Error"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "RuntimeError: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v6}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v9, v6}, Lcom/baidu/mobads/openad/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {v7, v8}, Lcom/baidu/mobads/openad/d/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :cond_8
    if-eqz v1, :cond_9

    .line 189
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_3

    :catchall_3
    move-exception v1

    .line 195
    iget-object v6, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v6}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {v6, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 198
    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/d/a;->d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 199
    :goto_4
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/d/a;->d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_a
    return-void

    :catchall_4
    move-exception v6

    if-eqz v1, :cond_b

    .line 189
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto :goto_5

    :catchall_5
    move-exception v1

    .line 195
    iget-object v7, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v7}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v7

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {v7, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 198
    :cond_b
    :goto_5
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/d/a;->d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 199
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a$a;->a:Lcom/baidu/mobads/openad/d/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/d/a;->d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_c
    throw v6
.end method
