.class public Lcom/baidu/mobads/openad/d/a;
.super Lcom/baidu/mobads/openad/c/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/openad/d/a$a;
    }
.end annotation


# static fields
.field public static a:I = 0x400

.field private static final f:Ljava/util/concurrent/TimeUnit;

.field private static g:I

.field private static h:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static i:Ljava/util/concurrent/ThreadPoolExecutor;


# instance fields
.field private b:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private k:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/baidu/mobads/openad/d/a;->f:Ljava/util/concurrent/TimeUnit;

    const/4 v0, 0x5

    .line 57
    sput v0, Lcom/baidu/mobads/openad/d/a;->g:I

    .line 58
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/baidu/mobads/openad/d/a;->h:Ljava/util/concurrent/BlockingQueue;

    .line 66
    :try_start_0
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v2, Lcom/baidu/mobads/openad/d/a;->g:I

    sget v3, Lcom/baidu/mobads/openad/d/a;->g:I

    const-wide/16 v4, 0x1

    sget-object v6, Lcom/baidu/mobads/openad/d/a;->f:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Lcom/baidu/mobads/openad/d/a;->h:Ljava/util/concurrent/BlockingQueue;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    sput-object v0, Lcom/baidu/mobads/openad/d/a;->i:Ljava/util/concurrent/ThreadPoolExecutor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 72
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0, v0}, Lcom/baidu/mobads/openad/d/a;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 77
    invoke-direct {p0}, Lcom/baidu/mobads/openad/c/c;-><init>()V

    const/4 v0, 0x0

    .line 49
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/openad/d/a;->d:Ljava/lang/Boolean;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/d/a;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 60
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/openad/d/a;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 61
    new-instance v0, Lcom/baidu/mobads/utils/u;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/u;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/d/a;->k:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 78
    iput-object p1, p0, Lcom/baidu/mobads/openad/d/a;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/d/a;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object p0
.end method

.method static synthetic a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-static {p0}, Lcom/baidu/mobads/openad/d/a;->b(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/openad/d/a;)Ljava/lang/Boolean;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/d/a;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method private static b(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    .line 209
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 210
    new-instance p0, Ljava/io/BufferedReader;

    invoke-direct {p0, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const-string v0, ""

    .line 212
    :goto_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic c(Lcom/baidu/mobads/openad/d/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/d/a;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/openad/d/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/d/a;->k:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/d/b;)V
    .locals 2

    const-wide v0, 0x40d3880000000000L    # 20000.0

    .line 92
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/d/b;D)V
    .locals 2

    .line 97
    :try_start_0
    sget-object v0, Lcom/baidu/mobads/openad/d/a;->i:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/baidu/mobads/openad/d/a$a;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/baidu/mobads/openad/d/a$a;-><init>(Lcom/baidu/mobads/openad/d/a;Lcom/baidu/mobads/openad/d/b;D)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V
    .locals 2

    .line 87
    iput-object p2, p0, Lcom/baidu/mobads/openad/d/a;->d:Ljava/lang/Boolean;

    const-wide v0, 0x40d3880000000000L    # 20000.0

    .line 88
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;D)V

    return-void
.end method

.method public dispose()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/baidu/mobads/openad/d/a;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 224
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/d/a;->a()V

    .line 225
    invoke-super {p0}, Lcom/baidu/mobads/openad/c/c;->dispose()V

    return-void
.end method
