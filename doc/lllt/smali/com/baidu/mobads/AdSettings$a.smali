.class public final enum Lcom/baidu/mobads/AdSettings$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/AdSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/AdSettings$a;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum b:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum c:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum d:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum e:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum f:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum g:Lcom/baidu/mobads/AdSettings$a;

.field private static final synthetic i:[Lcom/baidu/mobads/AdSettings$a;


# instance fields
.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 69
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v1, 0x0

    const-string v2, "PRIMARY"

    invoke-direct {v0, v2, v1, v1}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->a:Lcom/baidu/mobads/AdSettings$a;

    .line 71
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v2, 0x1

    const-string v3, "JUNIOR"

    invoke-direct {v0, v3, v2, v2}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->b:Lcom/baidu/mobads/AdSettings$a;

    .line 73
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v3, 0x2

    const-string v4, "SENIOR"

    invoke-direct {v0, v4, v3, v3}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->c:Lcom/baidu/mobads/AdSettings$a;

    .line 75
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v4, 0x3

    const-string v5, "SPECIALTY"

    invoke-direct {v0, v5, v4, v4}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->d:Lcom/baidu/mobads/AdSettings$a;

    .line 77
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v5, 0x4

    const-string v6, "BACHELOR"

    invoke-direct {v0, v6, v5, v5}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->e:Lcom/baidu/mobads/AdSettings$a;

    .line 79
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v6, 0x5

    const-string v7, "MASTER"

    invoke-direct {v0, v7, v6, v6}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->f:Lcom/baidu/mobads/AdSettings$a;

    .line 81
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const/4 v7, 0x6

    const-string v8, "DOCTOR"

    invoke-direct {v0, v8, v7, v7}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->g:Lcom/baidu/mobads/AdSettings$a;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/baidu/mobads/AdSettings$a;

    .line 64
    sget-object v8, Lcom/baidu/mobads/AdSettings$a;->a:Lcom/baidu/mobads/AdSettings$a;

    aput-object v8, v0, v1

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->b:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->c:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->d:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->e:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->f:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->g:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v7

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->i:[Lcom/baidu/mobads/AdSettings$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 87
    iput p3, p0, Lcom/baidu/mobads/AdSettings$a;->h:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 91
    iget v0, p0, Lcom/baidu/mobads/AdSettings$a;->h:I

    return v0
.end method
