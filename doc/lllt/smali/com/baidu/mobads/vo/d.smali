.class public abstract Lcom/baidu/mobads/vo/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdRequestInfo;


# instance fields
.field private a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Landroid/content/Context;

.field protected e:Landroid/app/Activity;

.field protected f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field protected g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

.field protected h:Lcom/baidu/mobads/interfaces/utils/IXAdConstants;

.field protected i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

.field protected j:Z

.field protected k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:I

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:I

.field private u:Z

.field private v:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 3

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TODO"

    .line 33
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->c:Ljava/lang/String;

    const-string v0, "android"

    .line 45
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    const-string v0, ""

    .line 448
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    .line 452
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/i;->getAdCreativeTypeImage()I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/vo/d;->o:I

    const-string v1, "LP,DL"

    .line 453
    iput-object v1, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    .line 454
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    const/4 v1, 0x0

    .line 461
    iput v1, p0, Lcom/baidu/mobads/vo/d;->s:I

    const/4 v1, 0x1

    .line 465
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/d;->u:Z

    .line 485
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/baidu/mobads/vo/d;->v:J

    .line 63
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->b:Ljava/lang/String;

    .line 64
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->h:Lcom/baidu/mobads/interfaces/utils/IXAdConstants;

    .line 65
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 66
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    .line 72
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 73
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 75
    :goto_0
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    if-nez v0, :cond_1

    move-object v0, p1

    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 78
    iput-object p2, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    .line 80
    :cond_2
    iget-object p2, p0, Lcom/baidu/mobads/vo/d;->k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;->webviewMultiProcess(Landroid/app/Activity;)Z

    move-result p2

    iput-boolean p2, p0, Lcom/baidu/mobads/vo/d;->j:Z

    .line 81
    iput-object p3, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 105
    new-instance p2, Lcom/baidu/mobads/vo/b;

    iget-object p3, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p2, p0, p3}, Lcom/baidu/mobads/vo/b;-><init>(Lcom/baidu/mobads/vo/d;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p2, p0, Lcom/baidu/mobads/vo/d;->g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    .line 106
    iget-object p2, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/vo/d;->c(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0, p1}, Lcom/baidu/mobads/vo/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .line 587
    sget-object v0, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    const-string v1, "0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    invoke-static {p1}, Lcom/baidu/mobads/f/g;->b(Landroid/content/Context;)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double p1, v0, v2

    if-lez p1, :cond_0

    .line 590
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 474
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/d;->u:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .line 434
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->e()Ljava/util/HashMap;

    move-result-object v0

    .line 435
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 436
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/vo/d;->b:Ljava/lang/String;

    .line 437
    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getRequestAdUrl(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    return-void
.end method

.method public d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .line 492
    iput p1, p0, Lcom/baidu/mobads/vo/d;->m:I

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    return-void
.end method

.method protected e()Ljava/util/HashMap;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "_"

    const-string v1, ""

    .line 129
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v2

    .line 130
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v3

    .line 131
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    const-string v5, "net"

    .line 134
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkCatagory(Landroid/content/Context;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "n"

    .line 137
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getN()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "at"

    .line 149
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAt()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "v"

    .line 153
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "4.1.30"

    .line 155
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "cs"

    .line 160
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "pk"

    .line 166
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "trftp"

    const-string v5, "sdk_8.8085"

    .line 167
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "q"

    .line 171
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "_cpr"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "appid"

    .line 175
    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "tp"

    .line 179
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "brd"

    .line 187
    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    const-string v5, "den"

    .line 191
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "w"

    .line 193
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getW()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "h"

    .line 194
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getH()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v5

    const-string v6, "sw"

    .line 198
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "sh"

    .line 202
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "lw"

    .line 204
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getW()I

    move-result v6

    int-to-float v6, v6

    iget v7, v0, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "lh"

    .line 205
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getH()I

    move-result v6

    int-to-float v6, v6

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v6, v0

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "sn"

    .line 227
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getSn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v0, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 243
    :try_start_1
    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCell(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 244
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 245
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    .line 246
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    if-ge v10, v11, :cond_0

    .line 247
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    const-string v12, "%s_%s_%s|"

    new-array v13, v0, [Ljava/lang/Object;

    .line 248
    aget-object v14, v11, v7

    aput-object v14, v13, v7

    aget-object v14, v11, v6

    aput-object v14, v13, v6

    aget-object v11, v11, v5

    aput-object v11, v13, v5

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 250
    :cond_0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    :cond_1
    move-object v8, v1

    :goto_1
    :try_start_2
    const-string v9, "cid"

    .line 256
    invoke-virtual {v4, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "nop"

    .line 268
    iget-object v9, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "im"

    .line 271
    iget-object v9, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getSubscriberId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 279
    :try_start_3
    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getGPS(Landroid/content/Context;)[D

    move-result-object v8

    if-eqz v8, :cond_2

    .line 282
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    const-string v9, "%s_%s_%s"

    new-array v0, v0, [Ljava/lang/Object;

    .line 283
    aget-wide v10, v8, v7

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v0, v7

    aget-wide v10, v8, v6

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v0, v6

    aget-wide v10, v8, v5

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v0, v5

    invoke-static {v9, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    :cond_2
    move-object v0, v1

    :goto_2
    :try_start_4
    const-string v8, "g"

    .line 288
    invoke-virtual {v4, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 295
    :try_start_5
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getWIFI(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 296
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 297
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    .line 298
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 299
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    const-string v11, "%s_%s|"

    new-array v12, v5, [Ljava/lang/Object;

    .line 300
    aget-object v13, v10, v7

    aput-object v13, v12, v7

    aget-object v10, v10, v6

    aput-object v10, v12, v6

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 302
    :cond_3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sub-int/2addr v0, v6

    invoke-virtual {v8, v7, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_4

    :catch_2
    :cond_4
    move-object v0, v1

    :goto_4
    :try_start_6
    const-string v5, "wi"

    .line 307
    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "swi"

    .line 318
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "wifi"

    iget-object v9, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    :goto_5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "tab"

    .line 322
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isTablet(Landroid/content/Context;)Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    const-string v6, "0"

    if-eqz v5, :cond_6

    :try_start_7
    const-string v5, "1"

    goto :goto_6

    :cond_6
    move-object v5, v6

    :goto_6
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "sdc"

    .line 329
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAppSDC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMem()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "act"

    .line 343
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAct()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "prod"

    .line 350
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getProd()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "os"

    const-string v5, "android"

    .line 352
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "osv"

    .line 353
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "bdr"

    .line 354
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "apinfo"

    .line 357
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v3, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getBaiduMapsInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "apid"

    .line 365
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getApid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "chid"

    .line 367
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getChannelId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "apt"

    .line 371
    invoke-virtual {v4, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "ap"

    .line 372
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAp()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "nt"

    .line 374
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "udid"

    .line 375
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "ses"

    .line 379
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getSes()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "android_id"

    .line 381
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "imei"

    .line 382
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "mac"

    .line 383
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "cuid"

    .line 384
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "snfrom"

    .line 385
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getSnFrom(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "p_ver"

    const-string v5, "8.8085"

    .line 386
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "req_id"

    .line 387
    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getApid()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->createRequestId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "cssid"

    .line 388
    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getWifiConnected(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :cond_7
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    invoke-static {}, Lcom/baidu/mobads/AdSettings;->getSupportHttps()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSettings$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "rpt"

    .line 399
    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSettings$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    :cond_8
    return-object v4
.end method

.method public e(I)V
    .locals 0

    .line 500
    iput p1, p0, Lcom/baidu/mobads/vo/d;->n:I

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 478
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)V
    .locals 0

    .line 524
    iput p1, p0, Lcom/baidu/mobads/vo/d;->r:I

    return-void
.end method

.method public g(I)V
    .locals 0

    .line 536
    iput p1, p0, Lcom/baidu/mobads/vo/d;->t:I

    return-void
.end method

.method public getAct()Ljava/lang/String;
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getAp()I
    .locals 1

    .line 563
    iget v0, p0, Lcom/baidu/mobads/vo/d;->s:I

    return v0
.end method

.method public getApid()Ljava/lang/String;
    .locals 1

    .line 571
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getApt()I
    .locals 1

    .line 520
    iget v0, p0, Lcom/baidu/mobads/vo/d;->r:I

    return v0
.end method

.method public getAt()I
    .locals 1

    .line 579
    iget v0, p0, Lcom/baidu/mobads/vo/d;->o:I

    return v0
.end method

.method public getH()I
    .locals 1

    .line 496
    iget v0, p0, Lcom/baidu/mobads/vo/d;->n:I

    return v0
.end method

.method public getN()I
    .locals 1

    .line 528
    iget v0, p0, Lcom/baidu/mobads/vo/d;->t:I

    return v0
.end method

.method public getProd()Ljava/lang/String;
    .locals 1

    .line 512
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getSes()J
    .locals 2

    .line 552
    iget-wide v0, p0, Lcom/baidu/mobads/vo/d;->v:J

    return-wide v0
.end method

.method public getSex()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getUk()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getW()I
    .locals 1

    .line 488
    iget v0, p0, Lcom/baidu/mobads/vo/d;->m:I

    return v0
.end method

.method public getZip()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public h(I)V
    .locals 0

    .line 567
    iput p1, p0, Lcom/baidu/mobads/vo/d;->s:I

    return-void
.end method

.method public i(I)V
    .locals 0

    .line 583
    iput p1, p0, Lcom/baidu/mobads/vo/d;->o:I

    return-void
.end method

.method public isCanClick()Z
    .locals 1

    .line 470
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/d;->u:Z

    return v0
.end method
