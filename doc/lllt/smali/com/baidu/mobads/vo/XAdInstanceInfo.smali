.class public Lcom/baidu/mobads/vo/XAdInstanceInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/baidu/mobads/vo/XAdInstanceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "XAdInstanceInfo"


# instance fields
.field private A:I

.field private B:I

.field private C:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Q:I

.field private R:I

.field private S:I

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Z

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private a:Ljava/lang/String;

.field private aA:Z

.field private aB:Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:J

.field private ad:I

.field private ae:Ljava/lang/String;

.field private af:I

.field private ag:Z

.field private ah:J

.field private ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field private aj:Ljava/lang/String;

.field private ak:I

.field private al:Z

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Ljava/lang/String;

.field private aw:Lorg/json/JSONArray;

.field private ax:Z

.field private ay:Ljava/lang/String;

.field private az:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private v:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lorg/json/JSONObject;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1222
    new-instance v0, Lcom/baidu/mobads/vo/a;

    invoke-direct {v0}, Lcom/baidu/mobads/vo/a;-><init>()V

    sput-object v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .line 1169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    .line 28
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v0, ""

    .line 58
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 61
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    .line 104
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    .line 105
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    .line 106
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    .line 107
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    .line 108
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    .line 109
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    .line 110
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    .line 112
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    .line 113
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    .line 114
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    .line 115
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    const/4 v1, 0x1

    .line 125
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    .line 149
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 156
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    .line 158
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    .line 159
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    .line 170
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    .line 1249
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:Z

    const/4 v2, 0x0

    .line 1262
    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:Ljava/lang/String;

    .line 1170
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    .line 1171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    .line 1172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    .line 1173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    .line 1174
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    .line 1175
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    .line 1176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    .line 1177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    .line 1178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    .line 1179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    .line 1180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    .line 1181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    .line 1182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    .line 1183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    .line 1184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 1185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    .line 1186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    .line 1187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    .line 1188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    .line 1189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    .line 1190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    .line 1191
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    .line 1192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    .line 1193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    .line 1194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    .line 1195
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    .line 1196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    .line 1197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    .line 1198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 1199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    .line 1200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 1201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 1202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    .line 1203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    .line 1204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    .line 1205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    .line 1206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    .line 1207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1208
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1209
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setStartTrackers(Ljava/util/List;)V

    .line 1210
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1211
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1212
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setCloseTrackers(Ljava/util/List;)V

    .line 1214
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 1216
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "XAdInstanceInfo"

    aput-object v5, v4, v0

    .line 1217
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-interface {v3, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 1219
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/baidu/mobads/vo/a;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 13

    const-string v0, "h"

    const-string v1, "w"

    .line 692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "-1"

    .line 28
    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v3, ""

    .line 58
    iput-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 59
    iput-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    const/4 v4, 0x0

    .line 60
    iput v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 61
    iput v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 100
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    .line 104
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    .line 105
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    .line 106
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    .line 107
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    .line 108
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    .line 109
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    .line 110
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    .line 111
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    .line 112
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    .line 113
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    .line 114
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    .line 115
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    const/4 v5, 0x1

    .line 125
    iput-boolean v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    .line 149
    sget-object v6, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v6, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 156
    iput-boolean v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    .line 158
    iput-boolean v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    .line 159
    iput-boolean v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    .line 170
    iput-boolean v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    .line 1249
    iput-boolean v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:Z

    const/4 v6, 0x0

    .line 1262
    iput-object v6, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:Ljava/lang/String;

    .line 693
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    .line 695
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    const-string v7, "act"

    .line 696
    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    const-string v7, "html"

    .line 697
    invoke-virtual {p1, v7, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    const-string v6, "id"

    .line 701
    invoke-virtual {p1, v6, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v2, "src"

    .line 702
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    const-string v2, "tit"

    .line 703
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    const-string v2, "desc"

    .line 704
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    const-string v2, "surl"

    .line 705
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    const-string v2, "phone"

    .line 706
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    const-string v2, "w_picurl"

    .line 707
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    const-string v2, "icon"

    .line 708
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    const-string v2, "exp2"

    const-string v6, "{}"

    .line 709
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    const-string v2, "anti_tag"

    .line 710
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    const-string v2, "vurl"

    .line 713
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    const-string v2, "duration"

    .line 714
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    const-string v2, "sound"

    .line 715
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    const-string v2, "iv"

    .line 716
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    const-string v2, "dur"

    .line 718
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    const-string v2, "curl"

    .line 721
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    const-string v2, "ori_curl"

    .line 722
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    const-string v2, "closetype"

    .line 724
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    const-string v2, "expiration"

    .line 725
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    const-string v2, "mute"

    .line 726
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    const-string v2, "ad_html"

    .line 727
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 729
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 730
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "banner_snippet"

    .line 731
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 732
    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const-string v8, "int_snippet"

    .line 733
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 734
    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v2, "type"

    .line 739
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    .line 741
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, "video"

    if-eqz v2, :cond_5

    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    .line 742
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 743
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto/16 :goto_4

    .line 744
    :cond_5
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 745
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v7, "text"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 746
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->TEXT:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_4

    .line 747
    :cond_6
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 748
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 749
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 750
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 751
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    .line 750
    invoke-virtual {v2, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const/16 v7, 0x2e

    .line 751
    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ltz v2, :cond_7

    .line 754
    iget-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 755
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    .line 754
    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 755
    invoke-virtual {v7, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_7
    move-object v2, v3

    :goto_3
    const-string v7, ".gif"

    .line 757
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 758
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_4

    .line 760
    :cond_8
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_4

    .line 763
    :cond_9
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v7, "rm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 764
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_4

    .line 765
    :cond_a
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 766
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 769
    :cond_b
    :goto_4
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    .line 770
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    const-string v2, "lb_phone"

    .line 771
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    const-string v2, "nwinurl"

    .line 773
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 774
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lez v7, :cond_c

    const/4 v7, 0x0

    .line 775
    :goto_5
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_d

    .line 776
    iget-object v8, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-virtual {v2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_c
    const-string v2, "winurl"

    .line 779
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 780
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 781
    iget-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_d
    const-string v2, "clklogurl"

    .line 784
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 785
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_e

    .line 786
    iget-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_e
    const-string v2, "mon"

    .line 788
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v7, "c"

    const-string v8, "s"

    if-eqz v2, :cond_f

    .line 789
    :try_start_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lez v9, :cond_f

    const/4 v9, 0x0

    .line 790
    :goto_6
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v9, v10, :cond_f

    .line 791
    invoke-virtual {v2, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 792
    invoke-virtual {v10, v8, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 793
    invoke-virtual {v10, v7, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 794
    invoke-virtual {p0, v11}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a(Ljava/lang/String;)V

    .line 795
    invoke-virtual {p0, v10}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b(Ljava/lang/String;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    :cond_f
    const-string v2, "monitors"

    .line 800
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_1c

    .line 802
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    :cond_10
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 803
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 805
    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 806
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 807
    :goto_7
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 808
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    :cond_11
    const-string v11, "vskip"

    .line 810
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 811
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 812
    :goto_8
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 813
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addSkipMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    :cond_12
    const-string v11, "scard"

    .line 815
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 816
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 817
    :goto_9
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 818
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addScardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    :cond_13
    const-string v11, "ccard"

    .line 820
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 821
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 822
    :goto_a
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 823
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCcardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    :cond_14
    const-string v11, "vstart"

    .line 825
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 826
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 827
    :goto_b
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 828
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addStartMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_b

    :cond_15
    const-string v11, "vfullscreen"

    .line 830
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 831
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 832
    :goto_c
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 833
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addFullScreenMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_c

    :cond_16
    const-string v11, "vclose"

    .line 835
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 836
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 837
    :goto_d
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 838
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCloseMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_d

    :cond_17
    const-string v11, "cstartcard"

    .line 840
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 841
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 842
    :goto_e
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 843
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCstartcardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_e

    .line 845
    :cond_18
    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 846
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 847
    :goto_f
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 848
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_f

    :cond_19
    const-string v11, "vcache_succ"

    .line 850
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1a

    .line 851
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 852
    :goto_10
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 853
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_10

    :cond_1a
    const-string v11, "vcache_fail"

    .line 855
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1b

    .line 856
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 857
    :goto_11
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 858
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_11

    :cond_1b
    const-string v11, "vcache_expire"

    .line 860
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 861
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    .line 862
    :goto_12
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_10

    .line 863
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_12

    .line 871
    :cond_1c
    iput-boolean v5, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    const-string v2, "cf"

    .line 872
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    const-string v2, "qk"

    .line 873
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    .line 875
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "_"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    invoke-virtual {v7}, Ljava/util/Random;->nextLong()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 876
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, "|"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Z:Ljava/lang/String;

    const-string v2, "appname"

    .line 877
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    const-string v2, "pk"

    .line 878
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    const-string v2, "sz"

    const-wide/16 v7, 0x0

    .line 879
    invoke-virtual {p1, v2, v7, v8}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    const-string v2, "sb"

    .line 880
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    const-string v2, "apo"

    .line 881
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    const-string v2, "po"

    .line 882
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    const-string v2, "st"

    .line 883
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1d

    const/4 v2, 0x1

    goto :goto_13

    :cond_1d
    const/4 v2, 0x0

    :goto_13
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    const-string v2, "murl"

    .line 885
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    .line 886
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1e

    .line 887
    invoke-virtual {p1, v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 888
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 889
    iput-object v6, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 891
    iget p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    .line 892
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/i;->getActTypeLandingPage()I

    move-result v0

    if-ne p1, v0, :cond_1e

    .line 893
    iget-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_14

    :catch_0
    move-exception p1

    .line 898
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "XAdInstanceInfo"

    aput-object v2, v1, v4

    .line 899
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v5

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_1e
    :goto_14
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)V
    .locals 1

    .line 904
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 905
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCcardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1037
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCloseMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1069
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCstartcardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1086
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addFullScreenMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1053
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addScardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1022
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1023
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addSkipMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1006
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addStartMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 985
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .line 910
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method final c(Ljava/lang/String;)V
    .locals 1

    .line 916
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1108
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final d(Ljava/lang/String;)V
    .locals 1

    .line 937
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method final e(Ljava/lang/String;)V
    .locals 1

    .line 958
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 959
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public getAPOOpen()Z
    .locals 1

    .line 1253
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:Z

    return v0
.end method

.method public getAction()Ljava/lang/String;
    .locals 1

    .line 1310
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getActionType()I
    .locals 1

    .line 544
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    return v0
.end method

.method public getAdHasDisplayed()Z
    .locals 1

    .line 1381
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->az:Z

    return v0
.end method

.method public getAdId()Ljava/lang/String;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getAdSource()Ljava/lang/String;
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getAntiTag()I
    .locals 1

    .line 383
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 592
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public getAppOpenStrs()Ljava/lang/String;
    .locals 1

    .line 622
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 1

    .line 582
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public getAppSize()J
    .locals 2

    .line 602
    iget-wide v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    return-wide v0
.end method

.method public getBannerHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 1356
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    return-object v0
.end method

.method public getCacheExpireTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 965
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCacheFailTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 944
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCacheSuccTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 923
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCcardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1044
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getClickThroughUrl()Ljava/lang/String;
    .locals 1

    .line 453
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    return-object v0
.end method

.method public getClklogurl()Ljava/lang/String;
    .locals 1

    .line 664
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    return-object v0
.end method

.method public getCloseTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1076
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCloseType()I
    .locals 1

    .line 1326
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    return v0
.end method

.method public getConfirmBorderPercent()Ljava/lang/String;
    .locals 1

    .line 562
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .line 1112
    iget-wide v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    return-wide v0
.end method

.method public getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-object v0
.end method

.method public getCstartcardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1093
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getDlTunnel()I
    .locals 1

    .line 185
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ak:I

    return v0
.end method

.method public getExp2ForSingleAd()Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiration()I
    .locals 1

    .line 1336
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    return v0
.end method

.method public getFullScreenTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1060
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFwt()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getHoursInADayToShowAd()I
    .locals 1

    .line 443
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    return v0
.end method

.method public getHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 473
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getImpressionUrls()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 513
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    return-object v0
.end method

.method public getIntHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 1366
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalCreativeURL()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getMainMaterialHeight()I
    .locals 1

    .line 493
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    return v0
.end method

.method public getMainMaterialWidth()I
    .locals 1

    .line 483
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    return v0
.end method

.method public getMainPictureUrl()Ljava/lang/String;
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getMaterialType()Ljava/lang/String;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getMute()Ljava/lang/String;
    .locals 1

    .line 1346
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    return-object v0
.end method

.method public getNwinurl()Lorg/json/JSONArray;
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aw:Lorg/json/JSONArray;

    return-object v0
.end method

.method public getOriginClickUrl()Ljava/lang/String;
    .locals 1

    .line 463
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginJsonObject()Lorg/json/JSONObject;
    .locals 1

    .line 652
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getPage()Ljava/lang/String;
    .locals 1

    .line 1266
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneForLocalBranding()Ljava/lang/String;
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getPointsForWall()I
    .locals 1

    .line 632
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    return v0
.end method

.method public getQueryKey()Ljava/lang/String;
    .locals 1

    .line 572
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public getScardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1028
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSkipTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1013
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSplash3DLocalUrl()Ljava/lang/String;
    .locals 1

    .line 1280
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ay:Ljava/lang/String;

    return-object v0
.end method

.method public getSponsorUrl()Ljava/lang/String;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 992
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSwitchButton()I
    .locals 1

    .line 612
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    return v0
.end method

.method public getThirdClickTrackingUrls()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 534
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getThirdImpressionTrackingUrls()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 523
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .line 1235
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoDuration()I
    .locals 1

    .line 423
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    return v0
.end method

.method public getVideoHeight()I
    .locals 1

    .line 1295
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    return v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoWidth()I
    .locals 1

    .line 1285
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    return v0
.end method

.method public getVurl()Ljava/lang/String;
    .locals 1

    .line 656
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    return-object v0
.end method

.method public getWebUrl()Ljava/lang/String;
    .locals 1

    .line 1315
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getWinurl()Ljava/lang/String;
    .locals 1

    .line 672
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    return-object v0
.end method

.method public isActionOnlyWifi()Z
    .locals 1

    .line 553
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    return v0
.end method

.method public isAutoOpen()Z
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    return v0
.end method

.method public isCanCancel()Z
    .locals 1

    .line 241
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ar:Z

    return v0
.end method

.method public isCanDelete()Z
    .locals 1

    .line 249
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->as:Z

    return v0
.end method

.method public isClose()Z
    .locals 1

    .line 201
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->am:Z

    return v0
.end method

.method public isIconVisibleForImageType()Z
    .locals 1

    .line 433
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    return v0
.end method

.method public isInapp()Z
    .locals 1

    .line 193
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    return v0
.end method

.method public isPopNotif()Z
    .locals 1

    .line 217
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    return v0
.end method

.method public isSecondConfirmed()Z
    .locals 1

    .line 1240
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    return v0
.end method

.method public isTaskDoneForWall()Z
    .locals 1

    .line 642
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    return v0
.end method

.method public isTooLarge()Z
    .locals 1

    .line 233
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aq:Z

    return v0
.end method

.method public isValid()Ljava/lang/Boolean;
    .locals 2

    .line 288
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isVideoMuted()Z
    .locals 1

    .line 413
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    return v0
.end method

.method public isWifiTargeted()Z
    .locals 1

    .line 225
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ap:Z

    return v0
.end method

.method public setAPOOpen(Z)V
    .locals 0

    .line 1258
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:Z

    return-void
.end method

.method public setAction(Ljava/lang/String;)V
    .locals 0

    .line 1305
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    return-void
.end method

.method public setActionOnlyWifi(Z)V
    .locals 0

    .line 557
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    return-void
.end method

.method public setActionType(I)V
    .locals 0

    .line 549
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    return-void
.end method

.method public setAdHasDisplayed(Z)V
    .locals 0

    .line 1376
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->az:Z

    return-void
.end method

.method public setAdId(Ljava/lang/String;)V
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    return-void
.end method

.method public setAdSource(Ljava/lang/String;)V
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    return-void
.end method

.method public setAntiTag(I)V
    .locals 0

    .line 388
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    return-void
.end method

.method public setAppOpenStrs(Ljava/lang/String;)V
    .locals 0

    .line 627
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    return-void
.end method

.method public setAppPackageName(Ljava/lang/String;)V
    .locals 0

    .line 587
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    return-void
.end method

.method public setAppSize(J)V
    .locals 0

    .line 607
    iput-wide p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    return-void
.end method

.method public setAutoOpen(Z)V
    .locals 0

    .line 213
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    return-void
.end method

.method public setBannerHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 1361
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    return-void
.end method

.method public setCacheExpireTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 971
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 972
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 974
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCacheFailTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 950
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 951
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 953
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCacheSuccTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 929
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 930
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 932
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCanCancel(Z)V
    .locals 0

    .line 245
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ar:Z

    return-void
.end method

.method public setCanDelete(Z)V
    .locals 0

    .line 253
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->as:Z

    return-void
.end method

.method public setCcardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1049
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setClickThroughUrl(Ljava/lang/String;)V
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    return-void
.end method

.method public setClklogurl(Ljava/lang/String;)V
    .locals 0

    .line 668
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    return-void
.end method

.method public setClose(Z)V
    .locals 0

    .line 205
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->am:Z

    return-void
.end method

.method public setCloseTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1099
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1100
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1102
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCloseType(I)V
    .locals 0

    .line 1331
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    return-void
.end method

.method public setConfirmBorderPercent(Ljava/lang/String;)V
    .locals 0

    .line 567
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    return-void
.end method

.method public setCreateTime(J)V
    .locals 0

    .line 1116
    iput-wide p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    return-void
.end method

.method public setCreativeType(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-void
.end method

.method public setCstartcardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1081
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1082
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .line 318
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    return-void
.end method

.method public setDlTunnel(I)V
    .locals 0

    .line 189
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ak:I

    return-void
.end method

.method public setExp2ForSingleAd(Ljava/lang/String;)V
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    return-void
.end method

.method public setExpiration(I)V
    .locals 0

    .line 1341
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    return-void
.end method

.method public setFullScreenTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1065
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setFwt(Ljava/lang/String;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    return-void
.end method

.method public setHoursInADayToShowAd(I)V
    .locals 0

    .line 448
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    return-void
.end method

.method public setHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 478
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    return-void
.end method

.method public setIconUrl(Ljava/lang/String;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    return-void
.end method

.method public setIconVisibleForImageType(Z)V
    .locals 0

    .line 438
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    return-void
.end method

.method public setImpressionUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 518
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    return-void
.end method

.method public setInapp(Z)V
    .locals 0

    .line 197
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    return-void
.end method

.method public setIntHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 1371
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    return-void
.end method

.method public setLocalCreativeURL(Ljava/lang/String;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->m:Ljava/lang/String;

    return-void
.end method

.method public setMainMaterialHeight(I)V
    .locals 0

    .line 498
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    return-void
.end method

.method public setMainMaterialWidth(I)V
    .locals 0

    .line 488
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    return-void
.end method

.method public setMainPictureUrl(Ljava/lang/String;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    return-void
.end method

.method public setMaterialType(Ljava/lang/String;)V
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    return-void
.end method

.method public setMute(Ljava/lang/String;)V
    .locals 0

    .line 1351
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    return-void
.end method

.method public setNwinurl(Lorg/json/JSONArray;)V
    .locals 0

    .line 684
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aw:Lorg/json/JSONArray;

    return-void
.end method

.method public setOriginClickUrl(Ljava/lang/String;)V
    .locals 0

    .line 468
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    return-void
.end method

.method public setPage(Ljava/lang/String;)V
    .locals 0

    .line 1271
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:Ljava/lang/String;

    return-void
.end method

.method public setPhoneForLocalBranding(Ljava/lang/String;)V
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    return-void
.end method

.method public setPointsForWall(I)V
    .locals 0

    .line 637
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    return-void
.end method

.method public setPopNotif(Z)V
    .locals 0

    .line 221
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    return-void
.end method

.method public setQueryKey(Ljava/lang/String;)V
    .locals 0

    .line 577
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    return-void
.end method

.method public setScardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1033
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setSecondConfirmed(Z)V
    .locals 0

    .line 1245
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    return-void
.end method

.method public setSkipTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1018
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setSplash3DLocalUrl(Ljava/lang/String;)V
    .locals 0

    .line 1276
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ay:Ljava/lang/String;

    return-void
.end method

.method public setSponsorUrl(Ljava/lang/String;)V
    .locals 0

    .line 328
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    return-void
.end method

.method public setStartTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 998
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 999
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1001
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setSwitchButton(I)V
    .locals 0

    .line 617
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    return-void
.end method

.method public setTaskDoneForWall(Z)V
    .locals 0

    .line 647
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    return-void
.end method

.method public setThirdClickTrackingUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 539
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    return-void
.end method

.method public setThirdImpressionTrackingUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 529
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    return-void
.end method

.method public setTooLarge(Z)V
    .locals 0

    .line 237
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aq:Z

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    return-void
.end method

.method public setVideoDuration(I)V
    .locals 0

    .line 428
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    return-void
.end method

.method public setVideoHeight(I)V
    .locals 0

    .line 1300
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    return-void
.end method

.method public setVideoMuted(Z)V
    .locals 0

    .line 418
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    return-void
.end method

.method public setVideoUrl(Ljava/lang/String;)V
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    return-void
.end method

.method public setVideoWidth(I)V
    .locals 0

    .line 1290
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    return-void
.end method

.method public setVurl(Ljava/lang/String;)V
    .locals 0

    .line 660
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    return-void
.end method

.method public setWebUrl(Ljava/lang/String;)V
    .locals 0

    .line 1320
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    return-void
.end method

.method public setWifiTargeted(Z)V
    .locals 0

    .line 229
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ap:Z

    return-void
.end method

.method public setWinurl(Ljava/lang/String;)V
    .locals 0

    .line 676
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1126
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1127
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1131
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1132
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1133
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1134
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1135
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1136
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1137
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1138
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1139
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1140
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1141
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1142
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1143
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1144
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1145
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1146
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1147
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1149
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1151
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1154
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1155
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1156
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1157
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1159
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1160
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1162
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1164
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1165
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1166
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
