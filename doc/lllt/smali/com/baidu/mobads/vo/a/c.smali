.class public Lcom/baidu/mobads/vo/a/c;
.super Lcom/baidu/mobads/vo/a/a;
.source "SourceFile"


# instance fields
.field public o:Z

.field private p:Landroid/content/Context;

.field private q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;)V
    .locals 3

    .line 20
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getAdId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getQk()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getProd()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/baidu/mobads/vo/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/a/c;->o:Z

    .line 21
    iput-object p1, p0, Lcom/baidu/mobads/vo/a/c;->p:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    return-void
.end method


# virtual methods
.method protected b()Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "1"

    const-string v1, "0"

    .line 27
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v2

    .line 28
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/baidu/mobads/vo/a/c;->p:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCurrentProcessId(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 29
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    const-string v5, "autoOpen"

    .line 31
    iget-boolean v6, p0, Lcom/baidu/mobads/vo/a/c;->o:Z

    if-eqz v6, :cond_0

    const-string v6, "true"

    goto :goto_0

    :cond_0
    const-string v6, "false"

    :goto_0
    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "pk"

    .line 32
    iget-object v6, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "c"

    .line 33
    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "clickProcId"

    .line 35
    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "clickTime"

    .line 37
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getClickTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "contentLength"

    .line 39
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getAppSize()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "dlCnt"

    .line 41
    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "dlTime"

    .line 43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v8}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getClickTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "dlTunnel"

    const-string v6, "3"

    .line 45
    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "dlWay"

    .line 47
    iget-object v6, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->isTooLarge()Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "exp_id"

    .line 48
    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "exp2"

    .line 49
    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "pkg"

    .line 53
    iget-object v1, p0, Lcom/baidu/mobads/vo/a/c;->q:Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/download/activate/IXAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "typeProcId"

    .line 54
    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "url2"

    .line 55
    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/vo/a/c;->p:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sn"

    .line 58
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/baidu/mobads/interfaces/utils/IBase64;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 61
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :goto_2
    return-object v3
.end method
