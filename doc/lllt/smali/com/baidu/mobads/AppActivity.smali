.class public Lcom/baidu/mobads/AppActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;
    }
.end annotation


# static fields
.field private static a:Z

.field public static activityName:Ljava/lang/String;

.field private static c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;


# instance fields
.field private b:Lcom/baidu/mobads/AppActivityImp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 160
    sget-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_WHITE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    sput-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 63
    new-instance v0, Lcom/baidu/mobads/AppActivityImp;

    invoke-direct {v0}, Lcom/baidu/mobads/AppActivityImp;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    return-void
.end method

.method public static canLpShowWhenLocked(Z)V
    .locals 0

    .line 176
    sput-boolean p0, Lcom/baidu/mobads/AppActivity;->a:Z

    .line 177
    invoke-static {p0}, Lcom/baidu/mobads/AppActivityImp;->canLpShowWhenLocked(Z)V

    return-void
.end method

.method public static getActionBarColorTheme()Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;
    .locals 1

    .line 163
    sget-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-object v0
.end method

.method public static getActivityClass()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 51
    const-class v0, Lcom/baidu/mobads/AppActivity;

    .line 52
    sget-object v1, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 56
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-object v0
.end method

.method public static getLpShowWhenLocked()Z
    .locals 1

    .line 181
    sget-boolean v0, Lcom/baidu/mobads/AppActivity;->a:Z

    return v0
.end method

.method public static isAnti()Z
    .locals 1

    .line 47
    sget-object v0, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static setActionBarColorTheme(Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 168
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;)V

    sput-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    :cond_0
    return-void
.end method

.method public static setActivityName(Ljava/lang/String;)V
    .locals 0

    .line 38
    sput-object p0, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 189
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 197
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 205
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onActivityResult(IILandroid/content/Intent;)V

    .line 210
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 215
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    return-void
.end method

.method protected onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 220
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 225
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContentChanged()V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onContentChanged()V

    .line 230
    invoke-super {p0}, Landroid/app/Activity;->onContentChanged()V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 238
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onContextMenuClosed(Landroid/view/Menu;)V

    .line 243
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 249
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/AppActivityImp;->setActivity(Landroid/app/Activity;)V

    .line 252
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 254
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .line 259
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 260
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    return-void
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 268
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 276
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 284
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 292
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 300
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 308
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z

    move-result p1

    return p1
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 316
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onDestroy()V

    .line 321
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 329
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 337
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 345
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLowMemory()V
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onLowMemory()V

    .line 350
    invoke-super {p0}, Landroid/app/Activity;->onLowMemory()V

    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 358
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 366
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onNewIntent(Landroid/content/Intent;)V

    .line 371
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 379
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 384
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onPanelClosed(ILandroid/view/Menu;)V

    .line 389
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPanelClosed(ILandroid/view/Menu;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onPause()V

    .line 394
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 398
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 399
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .line 403
    invoke-super {p0}, Landroid/app/Activity;->onPostResume()V

    .line 404
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onPostResume()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .line 408
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 409
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onPrepareDialog(ILandroid/app/Dialog;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 417
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 425
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onRestart()V
    .locals 1

    .line 429
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 430
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onRestart()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 434
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 435
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 439
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 440
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onResume()V

    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 448
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 452
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 453
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onSearchRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 461
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 1

    .line 465
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 466
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 470
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onStop()V

    .line 471
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .line 475
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 476
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onTitleChanged(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 484
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 488
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 492
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onUserInteraction()V
    .locals 1

    .line 496
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 497
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onUserInteraction()V

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .line 501
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 502
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onUserLeaveHint()V

    return-void
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 1

    .line 506
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    .line 507
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 511
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 512
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onWindowFocusChanged(Z)V

    return-void
.end method
