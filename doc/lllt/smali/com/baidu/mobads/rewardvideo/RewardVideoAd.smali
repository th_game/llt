.class public Lcom/baidu/mobads/rewardvideo/RewardVideoAd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;,
        Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;
    }
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

.field private final b:Landroid/content/Context;

.field private c:Lcom/baidu/mobads/production/rewardvideo/a;

.field private d:Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V
    .locals 1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->CREATE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    iput-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 79
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    .line 80
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 81
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/q;->a()V

    .line 82
    iput-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->d:Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    .line 83
    new-instance p1, Lcom/baidu/mobads/production/rewardvideo/a;

    iget-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-direct {p1, p3, p2, p4}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 92
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V
    .locals 1

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->CREATE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    iput-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 102
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    .line 103
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 104
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/q;->a()V

    .line 105
    iput-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->d:Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    .line 106
    new-instance p1, Lcom/baidu/mobads/production/rewardvideo/a;

    iget-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->b:Landroid/content/Context;

    invoke-direct {p1, p3, p2, p4}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->d:Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 181
    new-instance v0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;-><init>(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)V

    .line 182
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v1}, Lcom/baidu/mobads/production/rewardvideo/a;->removeAllListeners()V

    .line 183
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdUserClick"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 184
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdLoaded"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 185
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdStarted"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 186
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdStopped"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 187
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdError"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 188
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoCacheSucc"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 189
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoCacheFailed"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 190
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "PlayCompletion"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 191
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoPlayError"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 192
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->request()V

    return-void
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V
    .locals 1

    .line 196
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 198
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_1

    .line 199
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->PAUSE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->pause()V

    .line 202
    :cond_0
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->RESUME:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v0, :cond_1

    .line 203
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/rewardvideo/a;->resume()V

    :cond_1
    return-void
.end method

.method public static setAppSid(Ljava/lang/String;)V
    .locals 1

    .line 177
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/f;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 165
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized load()V
    .locals 2

    monitor-enter p0

    .line 112
    :try_start_0
    sget-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 114
    monitor-exit p0

    return-void

    .line 116
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->b(Z)V

    .line 119
    :cond_1
    invoke-direct {p0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public pause()V
    .locals 1

    .line 151
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->PAUSE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V

    return-void
.end method

.method public resume()V
    .locals 1

    .line 155
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->RESUME:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V

    return-void
.end method

.method public declared-synchronized show()V
    .locals 2

    monitor-enter p0

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_2

    .line 133
    sget-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 135
    monitor-exit p0

    return-void

    .line 137
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 138
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 139
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->q()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 140
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    sput-boolean v1, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->u()V

    goto :goto_0

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a()V

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->c:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->b(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
