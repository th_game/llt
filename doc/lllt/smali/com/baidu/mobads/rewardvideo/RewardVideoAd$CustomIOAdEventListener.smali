.class Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/rewardvideo/RewardVideoAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomIOAdEventListener"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 3

    .line 215
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "AdLoaded"

    .line 217
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_1

    :cond_0
    const-string v2, "AdStarted"

    .line 219
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 222
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onAdShow()V

    goto/16 :goto_1

    :cond_1
    const-string v2, "AdUserClick"

    .line 224
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 226
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 227
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onAdClick()V

    goto/16 :goto_1

    :cond_2
    const-string v2, "AdStopped"

    .line 229
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 p1, 0x0

    .line 231
    sput-boolean p1, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    if-eqz v1, :cond_3

    const-string p1, "play_scale"

    .line 234
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    const-string p1, "0"

    .line 236
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onAdClose(F)V

    goto/16 :goto_1

    :cond_4
    const-string v1, "AdRvdieoCacheSucc"

    .line 239
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 241
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 242
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onVideoDownloadSuccess()V

    goto :goto_1

    :cond_5
    const-string v1, "AdRvdieoCacheFailed"

    .line 244
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 246
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 247
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onVideoDownloadFailed()V

    goto :goto_1

    :cond_6
    const-string v1, "AdError"

    .line 249
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 251
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 252
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object v0

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    .line 253
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    .line 252
    invoke-interface {v0, p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->onAdFailed(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string p1, "PlayCompletion"

    .line 255
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 257
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 258
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$CustomIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-static {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->a(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;)Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;->playCompletion()V

    goto :goto_1

    :cond_8
    const-string p1, "AdRvdieoPlayError"

    .line 260
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_9
    :goto_1
    return-void
.end method
