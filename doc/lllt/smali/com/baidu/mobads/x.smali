.class Lcom/baidu/mobads/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/w;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/w;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iput-object p2, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdLoaded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iget-object v0, v0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/VideoAdView;

    invoke-static {v0}, Lcom/baidu/mobads/VideoAdView;->a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/VideoAdViewListener;->onVideoPrepared()V

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStarted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iget-object v0, v0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/VideoAdView;

    invoke-static {v0}, Lcom/baidu/mobads/VideoAdView;->a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/VideoAdViewListener;->onVideoStart()V

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdClickThru"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    iget-object v0, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iget-object v0, v0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/VideoAdView;

    invoke-static {v0}, Lcom/baidu/mobads/VideoAdView;->a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/VideoAdViewListener;->onVideoClickAd()V

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStopped"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    iget-object v0, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iget-object v0, v0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/VideoAdView;

    invoke-static {v0}, Lcom/baidu/mobads/VideoAdView;->a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/VideoAdViewListener;->onVideoFinish()V

    .line 49
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdError"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    iget-object v0, p0, Lcom/baidu/mobads/x;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    const-string v1, "message"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/baidu/mobads/x;->b:Lcom/baidu/mobads/w;

    iget-object v0, v0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/VideoAdView;

    invoke-static {v0}, Lcom/baidu/mobads/VideoAdView;->a(Lcom/baidu/mobads/VideoAdView;)Lcom/baidu/mobads/VideoAdViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/VideoAdViewListener;->onVideoError()V

    :cond_4
    return-void
.end method
