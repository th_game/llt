.class public Lcom/baidu/mobads/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 11
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    .line 14
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "100000"

    const-string v2, "\u8bf7\u6c42\u683c\u5f0f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101000"

    const-string v2, "\u8bf7\u6c42ID\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101001"

    const-string v2, "\u8bf7\u6c42ID\u4e0d\u7b26\u5408\u7ea6\u5b9a\u683c\u5f0f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101002"

    const-string v2, "\u8bf7\u6c42\u7684trftp\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101003"

    const-string v2, "\u8bf7\u6c42\u7684sdk\u7248\u672c\u4fe1\u606f\u6709\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101004"

    const-string v2, "\u8bf7\u6c42\u7684referer\u4fe1\u606f\u6709\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "101005"

    const-string v2, "\u8bf7\u6c42\u7684appid\u4e0d\u5408\u6cd5"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103000"

    const-string v2, "\u5e94\u7528\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103010"

    const-string v2, "\u5e94\u7528ID\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103011"

    const-string v2, "\u5e94\u7528ID\u4fe1\u606f\u9519\u8bef\uff0cMSSP\u672a\u6536\u5f55"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103012"

    const-string v2, "\u5e94\u7528ID\u65e0\u6548\uff0cMSSP\u4e0a\u672a\u751f\u6548"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103020"

    const-string v2, "\u5e94\u7528ID\u65e0\u6548\uff0c\u6e20\u9053ID\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103030"

    const-string v2, "\u5e94\u7528\u7248\u672c\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103040"

    const-string v2, "\u5e94\u7528\u4e3b\u7248\u672c\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103050"

    const-string v2, "\u5e94\u7528\u64cd\u4f5c\u7cfb\u7edf\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "103060"

    const-string v2, "\u5e94\u7528\u5305\u540d\u4fe1\u606f\u9519\u8bef\uff0c\u8bf7\u4fdd\u8bc1\u6ce8\u518c\u5305\u540d\u548c\u5b9e\u9645\u8bf7\u6c42\u5305\u540d\u4e00\u81f4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104000"

    const-string v2, "\u8bbe\u5907\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104010"

    const-string v2, "\u8bbe\u5907\u7c7b\u578b\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104011"

    const-string v2, "\u8bbe\u5907\u7c7b\u578b\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104020"

    const-string v2, "\u64cd\u4f5c\u7cfb\u7edf\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104021"

    const-string v2, "\u64cd\u4f5c\u7cfb\u7edf\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104030"

    const-string v2, "\u64cd\u4f5c\u7cfb\u7edf\u7248\u672c\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104040"

    const-string v2, "\u64cd\u4f5c\u7cfb\u7edf\u4e3b\u7248\u672c\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104050"

    const-string v2, "\u5382\u5546\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104060"

    const-string v2, "\u8bbe\u5907\u578b\u53f7\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104070"

    const-string v2, "\u8bbe\u5907\u552f\u4e00\u6807\u8bc6\u7b26\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104071"

    const-string v2, "\u8bbe\u5907\u552f\u4e00\u6807\u8bc6\u7b26\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104080"

    const-string v2, "android id \u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104081"

    const-string v2, "android id \u683c\u5f0f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104090"

    const-string v2, "\u8bbe\u5907\u5c4f\u5e55\u5c3a\u5bf8\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104100"

    const-string v2, "\u8bbe\u5907\u5c4f\u5e55\u5c3a\u5bf8\u5bbd\u5ea6\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "104110"

    const-string v2, "\u8bbe\u5907\u5c4f\u5e55\u5c3a\u5bf8\u9ad8\u5ea6\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105000"

    const-string v2, "\u7f51\u7edc\u73af\u5883\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105010"

    const-string v2, "\u7f51\u7edc\u5730\u5740\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105011"

    const-string v2, "\u7f51\u7edc\u5730\u5740\u4fe1\u606f\u683c\u5f0f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105020"

    const-string v2, "\u7f51\u7edc\u8fde\u63a5\u7c7b\u578b\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105021"

    const-string v2, "\u7f51\u7edc\u8fde\u63a5\u7c7b\u578b\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105030"

    const-string v2, "\u7f51\u7edc\u8fd0\u8425\u5546\u7c7b\u578b\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105031"

    const-string v2, "\u7f51\u7edc\u8fd0\u8425\u5546\u7c7b\u578b\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105040"

    const-string v2, "Wi-Fi\u70ed\u70b9\u5730\u5740\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105041"

    const-string v2, "Wi-Fi\u70ed\u70b9\u5730\u5740\u4fe1\u606f\u683c\u5f0f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105050"

    const-string v2, "Wi-Fi\u70ed\u70b9\u4fe1\u53f7\u5f3a\u5ea6\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105060"

    const-string v2, "Wi-Fi\u70ed\u70b9\u540d\u79f0\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "105070"

    const-string v2, "Wi-Fi\u8fde\u63a5\u72b6\u6001\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "106000"

    const-string v2, "\u5750\u6807\u7c7b\u578b\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "106001"

    const-string v2, "\u5750\u6807\u7c7b\u578b\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "106010"

    const-string v2, "\u7ecf\u5ea6\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "106020"

    const-string v2, "\u7eac\u5ea6\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "106030"

    const-string v2, "\u5b9a\u4f4d\u65f6\u95f4\u6233\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107000"

    const-string v2, "\u5e7f\u544a\u4f4dID\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107001"

    const-string v2, "\u5e7f\u544a\u4f4dID\u672a\u6536\u5f55"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107002"

    const-string v2, "\u5e7f\u544a\u4f4dID\u672a\u542f\u7528"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107003"

    const-string v2, "\u5e7f\u544a\u4f4dID\u4e0e\u5e94\u7528ID\u4e0d\u5339\u914d"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107010"

    const-string v2, "\u5e7f\u544a\u4f4d\u5c3a\u5bf8\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107020"

    const-string v2, "\u5e7f\u544a\u4f4d\u5c3a\u5bf8\u5bbd\u5ea6\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107030"

    const-string v2, "\u5e7f\u544a\u4f4d\u5c3a\u5bf8\u9ad8\u5ea6\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107040"

    const-string v2, "\u5e7f\u544a\u4f4d\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107050"

    const-string v2, "\u89c6\u9891\u5e7f\u544a\u7684\u7f51\u7edc\u6761\u4ef6\u65e0\u6cd5\u6ee1\u8db3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107051"

    const-string v2, "\u89c6\u9891\u6807\u9898\u540d\u79f0\u8fc7\u957f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "107052"

    const-string v2, "SDK\u4f20\u9012\u7684\u5e7f\u544a\u4f4d\u6bd4\u4f8b\u4e0eMSSP\u7684\u5e7f\u544a\u4f4d\u6bd4\u4f8b\u4e0d\u4e00\u81f4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "200000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201000"

    const-string v2, "\u65e0\u5e7f\u544a\u6570\u636e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201010"

    const-string v2, "\u5e7f\u544a\u65e0\u7b7e\u540d"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201020"

    const-string v2, "\u5e7f\u544a\u521b\u610f\u7c7b\u578b\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201021"

    const-string v2, "\u5e7f\u544a\u521b\u610f\u7c7b\u578b\u4fe1\u606f\u65e0\u6cd5\u8bc6\u522b"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201030"

    const-string v2, "\u5e7f\u544a\u52a8\u4f5c\u7c7b\u578b\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201031"

    const-string v2, "\u5e7f\u544a\u52a8\u4f5c\u7c7b\u578b\u4fe1\u606f\u65e0\u6cd5\u8bc6\u522b"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201040"

    const-string v2, "\u66dd\u5149\u6c47\u62a5\u5730\u5740\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201050"

    const-string v2, "\u70b9\u51fb\u54cd\u5e94\u5730\u5740\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201060"

    const-string v2, "\u63a8\u5e7f\u6807\u9898\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201070"

    const-string v2, "\u63a8\u5e7f\u63cf\u8ff0\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201080"

    const-string v2, "\u63a8\u5e7f\u5e94\u7528\u5305\u540d\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201090"

    const-string v2, "\u63a8\u5e7f\u5e94\u7528\u5305\u5927\u5c0f\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201100"

    const-string v2, "\u63a8\u5e7f\u56fe\u6807\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "201110"

    const-string v2, "\u63a8\u5e7f\u56fe\u7247\u4e22\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 98
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method
