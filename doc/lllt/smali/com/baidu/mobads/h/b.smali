.class public Lcom/baidu/mobads/h/b;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private b:Landroid/content/Context;

.field private c:Lcom/baidu/mobad/nativevideo/e;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/baidu/mobads/i/a;

.field private f:Z

.field private g:Lcom/baidu/mobads/h/a;

.field private h:Z

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;

.field private k:Z

.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/baidu/mobads/i/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 45
    iput-boolean v0, p0, Lcom/baidu/mobads/h/b;->h:Z

    .line 49
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v0, 0x0

    .line 51
    iput-boolean v0, p0, Lcom/baidu/mobads/h/b;->k:Z

    .line 365
    new-instance v0, Lcom/baidu/mobads/h/d;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/h/d;-><init>(Lcom/baidu/mobads/h/b;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->p:Lcom/baidu/mobads/i/d;

    .line 59
    iput-object p1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const-string p1, "#000000"

    .line 60
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/h/b;->setBackgroundColor(I)V

    return-void
.end method

.method private a(Landroid/content/Context;F)I
    .locals 0

    .line 361
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float p2, p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    float-to-int p1, p2

    return p1
.end method

.method static synthetic a(Lcom/baidu/mobads/h/b;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/baidu/mobads/h/b;->k:Z

    return p0
.end method

.method static synthetic a(Lcom/baidu/mobads/h/b;Z)Z
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/baidu/mobads/h/b;->k:Z

    return p1
.end method

.method private b(Lcom/baidu/mobad/nativevideo/e;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 347
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/h/b;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    .line 348
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 349
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/h/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 352
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    invoke-interface {p1}, Lcom/baidu/mobad/nativevideo/e;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :cond_1
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_2

    .line 355
    iget-object p1, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/h/b;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->g()V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/h/b;Z)Z
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/baidu/mobads/h/b;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/baidu/mobads/h/b;)Lcom/baidu/mobads/h/a;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/h/b;->g:Lcom/baidu/mobads/h/a;

    return-object p0
.end method

.method private c()V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->g:Lcom/baidu/mobads/h/a;

    if-eqz v0, :cond_0

    .line 153
    invoke-interface {v0}, Lcom/baidu/mobads/h/a;->c()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/baidu/mobads/h/b;->f:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 156
    iput-boolean v1, p0, Lcom/baidu/mobads/h/b;->f:Z

    .line 157
    invoke-interface {v0, p0}, Lcom/baidu/mobad/nativevideo/e;->a(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private d()V
    .locals 5

    .line 162
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->j:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->j:Landroid/widget/ImageView;

    .line 164
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v2, 0x41e80000    # 29.0f

    .line 165
    invoke-direct {p0, v1, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v1

    iget-object v3, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    .line 166
    invoke-direct {p0, v3, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const-wide v1, 0x3fa0624dd2f1a9fcL    # 0.032

    .line 167
    invoke-virtual {p0}, Lcom/baidu/mobads/h/b;->getMeasuredWidth()I

    move-result v3

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v3, v3, v1

    double-to-int v1, v3

    const/4 v2, 0x0

    .line 168
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 169
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->j:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v2}, Lcom/baidu/mobad/nativevideo/e;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/baidu/mobads/h/b;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->n()V

    return-void
.end method

.method private e()V
    .locals 9

    .line 175
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v0}, Lcom/baidu/mobad/nativevideo/e;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 177
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-direct {p0, v1, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v1

    .line 178
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v4, 0x8

    new-array v4, v4, [F

    int-to-float v1, v1

    const/4 v5, 0x0

    aput v1, v4, v5

    const/4 v6, 0x1

    aput v1, v4, v6

    const/4 v7, 0x2

    aput v1, v4, v7

    const/4 v8, 0x3

    aput v1, v4, v8

    const/4 v8, 0x4

    aput v1, v4, v8

    const/4 v8, 0x5

    aput v1, v4, v8

    const/4 v8, 0x6

    aput v1, v4, v8

    const/4 v8, 0x7

    aput v1, v4, v8

    const/4 v1, 0x0

    invoke-direct {v3, v4, v1, v1}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 181
    :try_start_0
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const-string v3, "#66000000"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :catch_0
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 186
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    .line 187
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 188
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 189
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 190
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v2}, Lcom/baidu/mobad/nativevideo/e;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 193
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-direct {p0, v2, v3}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    const-wide v1, 0x3fdd70a3d70a3d71L    # 0.46

    .line 194
    invoke-virtual {p0}, Lcom/baidu/mobads/h/b;->getMeasuredWidth()I

    move-result v3

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v3, v3, v1

    double-to-int v1, v3

    .line 195
    iget-object v2, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 197
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v3, 0x41800000    # 16.0f

    .line 198
    invoke-direct {p0, v2, v3}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    const/4 v3, -0x2

    invoke-direct {v1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xf

    .line 199
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 200
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v7, 0x41b00000    # 22.0f

    .line 201
    invoke-direct {p0, v6, v7}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v6

    invoke-direct {v4, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 202
    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 203
    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {p0, v2, v3}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v4, v2, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 204
    iget-object v2, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    iget-object v2, p0, Lcom/baidu/mobads/h/b;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/baidu/mobads/h/b;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->c()V

    return-void
.end method

.method private f()V
    .locals 7

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v3, 0x41e80000    # 29.0f

    .line 213
    invoke-direct {p0, v2, v3}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 214
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    .line 215
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 216
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    const-wide v3, 0x3fa47ae147ae147bL    # 0.04

    .line 217
    invoke-virtual {p0}, Lcom/baidu/mobads/h/b;->getMeasuredWidth()I

    move-result v1

    int-to-double v5, v1

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v5, v5, v3

    double-to-int v1, v5

    const-wide v3, 0x3fa0624dd2f1a9fcL    # 0.032

    .line 218
    invoke-virtual {p0}, Lcom/baidu/mobads/h/b;->getMeasuredWidth()I

    move-result v5

    int-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v5, v5, v3

    double-to-int v3, v5

    const/16 v4, 0xc

    .line 219
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v4, 0x9

    .line 220
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 221
    invoke-virtual {v0, v1, v2, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 222
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/h/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->i:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 225
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->i:Landroid/widget/ImageView;

    .line 226
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/baidu/mobads/h/c;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/h/c;-><init>(Lcom/baidu/mobads/h/b;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->g()V

    .line 234
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v2, 0x41b00000    # 22.0f

    .line 235
    invoke-direct {p0, v1, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v1

    iget-object v3, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {p0, v3, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 236
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->l:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 243
    iget-boolean v1, p0, Lcom/baidu/mobads/h/b;->k:Z

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/a;->a(Z)V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 246
    iget-boolean v1, p0, Lcom/baidu/mobads/h/b;->k:Z

    if-eqz v1, :cond_1

    .line 248
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdResource()Lcom/baidu/mobads/utils/p;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/p;->getRewardVideoVolumeMute()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 247
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 250
    :cond_1
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdResource()Lcom/baidu/mobads/utils/p;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/p;->getRewardVideoVolume()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private h()V
    .locals 6

    .line 257
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    const/16 v1, 0xc

    const/high16 v2, 0x41700000    # 15.0f

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 258
    new-instance v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    .line 259
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    const v4, 0x10001

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setId(I)V

    .line 260
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 261
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v5, 0x41c00000    # 24.0f

    .line 262
    invoke-direct {p0, v4, v5}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v4

    iget-object v5, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    .line 263
    invoke-direct {p0, v5, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v5

    invoke-direct {v0, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0xb

    .line 264
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 265
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 266
    iget-object v4, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    iget-object v4, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    invoke-virtual {p0, v4, v0}, Lcom/baidu/mobads/h/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 268
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v5}, Lcom/baidu/mobad/nativevideo/e;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 275
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    .line 276
    new-instance v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    .line 277
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 278
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    const/high16 v5, 0x41600000    # 14.0f

    invoke-direct {p0, v4, v5}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v4

    iget-object v5, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    .line 279
    invoke-direct {p0, v5, v2}, Lcom/baidu/mobads/h/b;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-direct {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 280
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 281
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->m:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 282
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/h/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v2}, Lcom/baidu/mobad/nativevideo/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_1

    .line 286
    :cond_2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_1
    return-void
.end method

.method private i()V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Lcom/baidu/mobads/i/a;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    .line 295
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 297
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 298
    iget-object v1, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    invoke-virtual {p0, v1, v0}, Lcom/baidu/mobads/h/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->p:Lcom/baidu/mobads/i/d;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/a;->a(Lcom/baidu/mobads/i/d;)V

    .line 300
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->c()V

    .line 301
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->g()V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    if-eqz v1, :cond_0

    .line 307
    invoke-interface {v1}, Lcom/baidu/mobad/nativevideo/e;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/i/a;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/baidu/mobads/h/b;->h:Z

    if-eqz v1, :cond_0

    .line 314
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->a()V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 3

    .line 319
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "PacthAdView"

    const-string v2, "resume"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/baidu/mobads/h/b;->h:Z

    if-eqz v1, :cond_0

    .line 322
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->b()V

    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->c()V

    :cond_0
    return-void
.end method

.method private n()V
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    .line 336
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->e()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Lcom/baidu/mobad/nativevideo/e;)V
    .locals 2

    .line 69
    iput-object p1, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    if-nez v0, :cond_0

    .line 71
    iget-object p1, p0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "PacthAdView"

    const-string v1, "\u5e7f\u544a\u54cd\u5e94\u5185\u5bb9\u4e3a\u7a7a\uff0c\u65e0\u6cd5\u64ad\u653e"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 74
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobad/nativevideo/e;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "video"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 76
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->i()V

    .line 77
    iget-object p1, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    iget-boolean v0, p0, Lcom/baidu/mobads/h/b;->k:Z

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/i/a;->a(Z)V

    .line 78
    iget-object p1, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz p1, :cond_1

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-interface {v0}, Lcom/baidu/mobad/nativevideo/e;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/i/a;->b(Ljava/lang/String;)V

    .line 81
    :cond_1
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->j()V

    goto :goto_0

    .line 83
    :cond_2
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->c()V

    .line 85
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    invoke-direct {p0, p1}, Lcom/baidu/mobads/h/b;->b(Lcom/baidu/mobad/nativevideo/e;)V

    .line 86
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->h()V

    .line 87
    invoke-virtual {p0, p0}, Lcom/baidu/mobads/h/b;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/h/a;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/baidu/mobads/h/b;->g:Lcom/baidu/mobads/h/a;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 97
    iput-boolean p1, p0, Lcom/baidu/mobads/h/b;->k:Z

    .line 98
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->g()V

    return-void
.end method

.method public b()J
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->e:Lcom/baidu/mobads/i/a;

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {v0}, Lcom/baidu/mobads/i/a;->f()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->g:Lcom/baidu/mobads/h/a;

    if-eqz v0, :cond_0

    .line 144
    invoke-interface {v0}, Lcom/baidu/mobads/h/a;->d()V

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/h/b;->c:Lcom/baidu/mobad/nativevideo/e;

    if-eqz v0, :cond_1

    .line 147
    invoke-interface {v0, p1}, Lcom/baidu/mobad/nativevideo/e;->b(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->m()V

    .line 128
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 119
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 120
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->f()V

    .line 121
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->d()V

    .line 122
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->e()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->l()V

    goto :goto_0

    .line 136
    :cond_0
    invoke-direct {p0}, Lcom/baidu/mobads/h/b;->k()V

    .line 138
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    return-void
.end method
