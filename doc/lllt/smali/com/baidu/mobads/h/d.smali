.class Lcom/baidu/mobads/h/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/i/d;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/h/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/h/b;)V
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 369
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/baidu/mobads/h/b;->b(Lcom/baidu/mobads/h/b;Z)Z

    .line 370
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    iget-object v0, v0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "PacthAdView"

    const-string v2, "playCompletion"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->c(Lcom/baidu/mobads/h/b;)Lcom/baidu/mobads/h/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->c(Lcom/baidu/mobads/h/b;)Lcom/baidu/mobads/h/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/h/a;->a()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .line 378
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/baidu/mobads/h/b;->b(Lcom/baidu/mobads/h/b;Z)Z

    .line 379
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    iget-object v0, v0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "PacthAdView"

    const-string v2, "playFailure"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->c(Lcom/baidu/mobads/h/b;)Lcom/baidu/mobads/h/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->c(Lcom/baidu/mobads/h/b;)Lcom/baidu/mobads/h/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/h/a;->b()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .line 387
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->d(Lcom/baidu/mobads/h/b;)V

    .line 388
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    invoke-static {v0}, Lcom/baidu/mobads/h/b;->e(Lcom/baidu/mobads/h/b;)V

    .line 389
    iget-object v0, p0, Lcom/baidu/mobads/h/d;->a:Lcom/baidu/mobads/h/b;

    iget-object v0, v0, Lcom/baidu/mobads/h/b;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "PacthAdView"

    const-string v2, "renderingStart"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
