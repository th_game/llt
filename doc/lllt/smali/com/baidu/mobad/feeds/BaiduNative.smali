.class public Lcom/baidu/mobad/feeds/BaiduNative;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private c:Lcom/baidu/mobads/production/c/c;

.field private d:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

.field private e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;

.field private f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;)V
    .locals 1

    .line 170
    new-instance v0, Lcom/baidu/mobads/production/c/c;

    invoke-direct {v0, p1, p2}, Lcom/baidu/mobads/production/c/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/c/c;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/c/c;)V
    .locals 2

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Landroid/content/Context;

    .line 176
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 177
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->b:Ljava/lang/String;

    .line 178
    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    .line 179
    invoke-static {p1}, Lcom/baidu/mobads/f/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/f/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/f/q;->a()V

    .line 180
    iput-object p4, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    return-object p0
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 266
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/f;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method protected handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/c/c;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->b(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnClickAd(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->d(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnClose(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/c/c;->a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnComplete(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->c(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnError(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/c/c;->a(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method protected handleOnFullScreen(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/c/c;->b(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnStart(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->b(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected isAdAvailable(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1

    return p1
.end method

.method public makeRequest()V
    .locals 1

    const/4 v0, 0x0

    .line 192
    check-cast v0, Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {p0, v0}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 3

    if-nez p1, :cond_0

    .line 200
    new-instance p1, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p1

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->b:Ljava/lang/String;

    iput-object v0, p1, Lcom/baidu/mobad/feeds/RequestParameters;->mPlacementId:Ljava/lang/String;

    .line 206
    new-instance v0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;-><init>(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    iput-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    .line 208
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    const-string v2, "AdStarted"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/c/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 210
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    const-string v2, "AdUserClick"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/c/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 211
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    const-string v2, "AdError"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/c/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 212
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    const-string v2, "vdieoCacheSucc"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/c/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 213
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    const-string v2, "vdieoCacheFailed"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/production/c/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 214
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/c/c;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 215
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/c/c;->request()V

    return-void
.end method

.method protected recordImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Lcom/baidu/mobads/production/c/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/c/c;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public setNativeEventListener(Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 188
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;

    return-void
.end method
