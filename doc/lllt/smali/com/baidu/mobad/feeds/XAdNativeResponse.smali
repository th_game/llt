.class public Lcom/baidu/mobad/feeds/XAdNativeResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/feeds/NativeResponse;


# instance fields
.field private a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private b:Lcom/baidu/mobad/feeds/BaiduNative;

.field private c:Z

.field private d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

.field private e:Lcom/baidu/mobads/interfaces/IXAdContainer;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Lcom/baidu/mobads/interfaces/IXAdContainer;)V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    .line 42
    iput-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 43
    iput-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 44
    iput-object p4, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 45
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object p1

    .line 46
    iget-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result p2

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result p1

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    .line 47
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    .line 49
    :cond_0
    iput-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .line 191
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    .line 192
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->is3GConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 194
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    goto :goto_0

    .line 197
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 4

    .line 204
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 206
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-class v1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x1

    const-string v1, "showConfirmDialog context is null"

    aput-object v1, p2, v0

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/n;->e([Ljava/lang/Object;)I

    return-void

    .line 210
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 211
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u786e\u8ba4\u4e0b\u8f7d\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\"?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "\u63d0\u793a"

    .line 212
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "\u786e\u8ba4"

    .line 213
    new-instance v3, Lcom/baidu/mobad/feeds/XAdNativeResponse$1;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse$1;-><init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;Landroid/view/View;I)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "\u53d6\u6d88"

    .line 221
    new-instance p2, Lcom/baidu/mobad/feeds/XAdNativeResponse$2;

    invoke-direct {p2, p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse$2;-><init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    invoke-virtual {v1, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 231
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception p1

    .line 229
    invoke-static {}, Lcom/baidu/mobads/utils/n;->a()Lcom/baidu/mobads/utils/n;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/n;->e(Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 3

    .line 160
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isDownloadApp()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 165
    invoke-interface {p3, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    .line 166
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobad/feeds/BaiduNative;->handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 169
    invoke-direct {p0, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/content/Context;)V

    .line 170
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobad/feeds/BaiduNative;->handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_0

    .line 171
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 173
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 176
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v1

    .line 177
    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->is3GConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 180
    :cond_3
    invoke-direct {p0, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/content/Context;)V

    .line 181
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobad/feeds/BaiduNative;->handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_0

    .line 186
    :cond_4
    iget-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {p3, p1, v0, p2, v1}, Lcom/baidu/mobad/feeds/BaiduNative;->handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    :cond_5
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/BaiduNative;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    return-object p0
.end method


# virtual methods
.method public getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;
    .locals 2

    .line 350
    new-instance v0, Lcom/baidu/mobads/component/AdLogInfo;

    invoke-direct {v0}, Lcom/baidu/mobads/component/AdLogInfo;-><init>()V

    .line 351
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    if-eqz v1, :cond_0

    .line 352
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAdPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setAdPlaceId(Ljava/lang/String;)V

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v1, :cond_1

    .line 355
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setQk(Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setVideoUrl(Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public getAdLogoUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/mob-adIcon_2x.png"

    return-object v0
.end method

.method public getAdMaterialType()Ljava/lang/String;
    .locals 2

    .line 295
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_0

    .line 296
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_1

    .line 298
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 300
    :cond_1
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPackage()Ljava/lang/String;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppSize()J
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBaiduLogoUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/new-bg-logo.png"

    return-object v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoDuration()I

    move-result v0

    return v0
.end method

.method public getExtras()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    .line 75
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMainPicHeight()I
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainMaterialHeight()I

    move-result v0

    return v0
.end method

.method public getMainPicWidth()I
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainMaterialWidth()I

    move-result v0

    return v0
.end method

.method public getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_0

    .line 274
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_1

    .line 276
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0

    .line 278
    :cond_1
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0
.end method

.method public getMultiPicUrls()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 119
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "morepics"

    .line 120
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 121
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 122
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    .line 123
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 124
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    move-object v0, v2

    :catch_1
    :cond_1
    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    return-object v0
.end method

.method public handleClick(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    .line 146
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->handleClick(Landroid/view/View;I)V

    return-void
.end method

.method public handleClick(Landroid/view/View;I)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    if-eqz p1, :cond_0

    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;

    move-result-object p2

    const-string v0, "video_click"

    invoke-static {p1, v0, p2}, Lcom/baidu/mobads/utils/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/component/AdLogInfo;)V

    :cond_0
    return-void
.end method

.method protected handleClickDownloadDirect(Landroid/view/View;)V
    .locals 2

    .line 334
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->supportDownloadDirect()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 338
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    const-string v1, ""

    .line 339
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setAction(Ljava/lang/String;)V

    const/4 v1, -0x1

    .line 340
    invoke-direct {p0, p1, v1, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 344
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public isAdAvailable(Landroid/content/Context;)Z
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->isAdAvailable(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1

    return p1
.end method

.method public isAutoPlay()Z
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "auto_play"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isDownloadApp()Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    return v0
.end method

.method public onClickAd(Landroid/content/Context;)V
    .locals 3

    .line 306
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnClickAd(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onClose(Landroid/content/Context;I)V
    .locals 3

    .line 253
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnClose(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onComplete(Landroid/content/Context;)V
    .locals 3

    .line 248
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnComplete(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onError(Landroid/content/Context;II)V
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnError(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method public onFullScreen(Landroid/content/Context;I)V
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnFullScreen(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onStart(Landroid/content/Context;)V
    .locals 3

    .line 238
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->handleOnStart(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public recordImpression(Landroid/view/View;)V
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobad/feeds/BaiduNative;->recordImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public setIsDownloadApp(Z)V
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    return-void
.end method

.method protected supportDownloadDirect()Z
    .locals 4

    .line 325
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v0

    .line 326
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    .line 327
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video"

    .line 328
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 329
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/i;->getActTypeDownload()I

    move-result v2

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
