.class Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/BaiduNative;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomIOAdEventListener"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobad/feeds/BaiduNative;

.field private b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;


# direct methods
.method public constructor <init>(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 11

    .line 46
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStarted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    .line 49
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 51
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 53
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/i;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 55
    :goto_0
    iget-object v5, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v5}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/production/c/c;->q()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 56
    iget-object v5, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v5}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/production/c/c;->q()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 57
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v6

    .line 61
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v7

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v8

    if-ne v7, v8, :cond_2

    if-eqz v6, :cond_1

    const-string v7, ""

    .line 62
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "null"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    .line 65
    :cond_0
    invoke-virtual {v0, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/o;

    move-result-object v7

    iget-object v8, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v8}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/baidu/mobads/utils/o;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    const/4 v7, 0x1

    goto :goto_3

    :cond_1
    :goto_1
    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    const/4 v7, 0x0

    :goto_3
    if-nez v6, :cond_4

    .line 72
    new-instance v6, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    iget-object v8, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v9, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 74
    invoke-static {v8}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/baidu/mobads/production/c/c;->getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object v10

    invoke-direct {v6, v5, v8, v9, v10}, Lcom/baidu/mobad/feeds/XAdNativeResponse;-><init>(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Lcom/baidu/mobads/interfaces/IXAdContainer;)V

    if-ne v7, v1, :cond_3

    .line 76
    invoke-virtual {v6, v3}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->setIsDownloadApp(Z)V

    .line 78
    :cond_3
    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 81
    :cond_5
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$1;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$1;-><init>(Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/f;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_4

    .line 89
    :cond_6
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AdError"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 90
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/c/c;->removeAllListeners()V

    .line 91
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v0, "message"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 94
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 95
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object p1

    new-instance v0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$2;

    invoke-direct {v0, p0}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$2;-><init>(Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/f;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_4

    .line 103
    :cond_7
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AdUserClick"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "AdLpClosed"

    if-eqz v0, :cond_9

    .line 104
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz p1, :cond_8

    .line 105
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;->onAdClick()V

    .line 107
    :cond_8
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz p1, :cond_c

    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 108
    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/c/c;

    move-result-object p1

    iget-object p1, p1, Lcom/baidu/mobads/production/c/c;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result p1

    if-ne p1, v1, :cond_c

    .line 109
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/command/c/a;->b()V

    .line 110
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 111
    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/baidu/mobads/command/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto/16 :goto_4

    .line 113
    :cond_9
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 114
    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz v0, :cond_a

    .line 115
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/baidu/mobads/command/c/a;->removeEventListeners(Ljava/lang/String;)V

    .line 116
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/c/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/c/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/command/c/a;->c()V

    .line 117
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;->onLpClosed()V

    goto :goto_4

    .line 118
    :cond_a
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "vdieoCacheSucc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 119
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    if-eqz p1, :cond_c

    .line 120
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;->onVideoDownloadSuccess()V

    goto :goto_4

    .line 122
    :cond_b
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object p1

    const-string v0, "vdieoCacheFailed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    .line 123
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    if-eqz p1, :cond_c

    .line 124
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;->onVideoDownloadFailed()V

    :cond_c
    :goto_4
    return-void
.end method
