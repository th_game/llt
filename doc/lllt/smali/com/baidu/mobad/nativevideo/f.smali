.class public Lcom/baidu/mobad/nativevideo/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/nativevideo/e;


# instance fields
.field a:Lcom/baidu/mobad/feeds/NativeResponse;

.field private b:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field private c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;


# direct methods
.method public constructor <init>(Lcom/baidu/mobad/feeds/NativeResponse;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainer;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    .line 16
    iput-object p3, p0, Lcom/baidu/mobad/nativevideo/f;->b:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 17
    iput-object p2, p0, Lcom/baidu/mobad/nativevideo/f;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_2

    .line 45
    sget-object v0, Lcom/baidu/mobad/nativevideo/g;->a:[I

    iget-object v1, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v1}, Lcom/baidu/mobad/feeds/NativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "gif"

    goto :goto_1

    :cond_1
    const-string v0, "video"

    goto :goto_1

    :cond_2
    :goto_0
    const-string v0, "normal"

    :goto_1
    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 23
    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/NativeResponse;->recordImpression(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getAdLogoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 30
    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/NativeResponse;->handleClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 72
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getBaiduLogoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 80
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 96
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/f;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
