.class public Lcom/baidu/mobad/video/XAdContext;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;
    }
.end annotation


# static fields
.field public static final PARAMETER_KEY_OF_BASE_HEIGHT:Ljava/lang/String; = "BASE_HEIGHT"

.field public static final PARAMETER_KEY_OF_BASE_WIDTH:Ljava/lang/String; = "BASE_WIDTH"

.field public static final TAG:Ljava/lang/String; = "XAdContext"


# instance fields
.field a:I

.field b:I

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

.field private e:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

.field private f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

.field private g:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VisitorAction;

.field private h:D

.field private i:I

.field private j:I

.field private k:Landroid/content/Context;

.field private l:Ljava/lang/String;

.field private m:Landroid/location/Location;

.field protected mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private n:Landroid/app/Activity;

.field private o:Landroid/widget/RelativeLayout;

.field private final p:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

.field private final q:Lcom/baidu/mobad/video/XAdSlotManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)V
    .locals 1

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->c:Ljava/util/HashMap;

    .line 59
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;->FULL_SCREEN:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

    iput-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->d:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

    .line 61
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;->IDLE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    iput-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->e:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    .line 63
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->CREATE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    iput-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    const/4 v0, 0x0

    .line 359
    iput v0, p0, Lcom/baidu/mobad/video/XAdContext;->a:I

    .line 360
    iput v0, p0, Lcom/baidu/mobad/video/XAdContext;->b:I

    .line 96
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->k:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcom/baidu/mobad/video/XAdContext;->l:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/baidu/mobad/video/XAdContext;->m:Landroid/location/Location;

    .line 99
    new-instance p1, Lcom/baidu/mobads/openad/c/c;

    invoke-direct {p1}, Lcom/baidu/mobads/openad/c/c;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->p:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    .line 100
    new-instance p1, Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-direct {p1}, Lcom/baidu/mobad/video/XAdSlotManager;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    .line 102
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/video/XAdContext;)Lcom/baidu/mobad/video/XAdSlotManager;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    return-object p0
.end method

.method private a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 203
    new-instance v0, Lcom/baidu/mobads/openad/d/a;

    invoke-direct {v0}, Lcom/baidu/mobads/openad/d/a;-><init>()V

    .line 204
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 205
    new-instance v2, Lcom/baidu/mobads/openad/d/b;

    const-string v3, ""

    invoke-direct {v2, v1, v3}, Lcom/baidu/mobads/openad/d/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 206
    iput v1, v2, Lcom/baidu/mobads/openad/d/b;->e:I

    .line 207
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/openad/d/a;->a(Lcom/baidu/mobads/openad/d/b;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->p:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    return-void
.end method

.method public dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method public dispose()V
    .locals 0

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    return-object v0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getSlotById(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdProd;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {v0, p1}, Lcom/baidu/mobad/video/XAdSlotManager;->retrieveAdSlotById(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object p1

    return-object p1
.end method

.method public getXAdManager()Lcom/baidu/mobads/interfaces/IXAdManager;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public newPrerollAdSlot(Ljava/lang/String;II)Lcom/baidu/mobads/interfaces/IXLinearAdSlot;
    .locals 1

    .line 306
    iget-object p2, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {p2, p1}, Lcom/baidu/mobad/video/XAdSlotManager;->containsAdSlot(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    .line 307
    new-instance p2, Lcom/baidu/mobads/production/h/b;

    iget-object p3, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    invoke-direct {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 308
    iget-object p3, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    invoke-virtual {p2, p3}, Lcom/baidu/mobads/production/h/b;->setActivity(Landroid/content/Context;)V

    .line 309
    iget-object p3, p0, Lcom/baidu/mobad/video/XAdContext;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {p2, p3}, Lcom/baidu/mobads/production/h/b;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 310
    invoke-virtual {p2, p1}, Lcom/baidu/mobads/production/h/b;->setId(Ljava/lang/String;)V

    .line 312
    new-instance p1, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    iget-object p3, p0, Lcom/baidu/mobad/video/XAdContext;->k:Landroid/content/Context;

    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->p:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    invoke-direct {p1, p3, p2, v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;-><init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdProd;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;)V

    .line 313
    invoke-virtual {p2}, Lcom/baidu/mobads/production/h/b;->removeAllListeners()V

    const-string p3, "complete"

    .line 314
    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    const-string p3, "AdStarted"

    .line 315
    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    const-string p3, "AdStopped"

    .line 316
    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    const-string p3, "AdError"

    .line 317
    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    const-string p3, "AdUserClick"

    .line 318
    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/h/b;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 319
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {p1, p2}, Lcom/baidu/mobad/video/XAdSlotManager;->addAdSlot(Lcom/baidu/mobads/interfaces/IXAdProd;)V

    .line 321
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {p1}, Lcom/baidu/mobad/video/XAdSlotManager;->retrievePrerollAdSlot()Lcom/baidu/mobads/interfaces/IXLinearAdSlot;

    move-result-object p1

    return-object p1
.end method

.method public notifyVisitorAction(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VisitorAction;)V
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->g:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VisitorAction;

    return-void
.end method

.method public removeEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->p:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->removeEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 131
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    .line 132
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->k:Landroid/content/Context;

    if-nez p1, :cond_0

    .line 133
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->n:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->k:Landroid/content/Context;

    :cond_0
    return-void
.end method

.method public setActivityState(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V
    .locals 3

    .line 145
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 146
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->mAdLogger:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-virtual {p1}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XAdContext"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {v0}, Lcom/baidu/mobad/video/XAdSlotManager;->retrievePrerollAdSlot()Lcom/baidu/mobads/interfaces/IXLinearAdSlot;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 150
    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->PAUSE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v1, :cond_0

    .line 151
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->pause()V

    .line 153
    :cond_0
    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->RESUME:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v1, :cond_1

    .line 154
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->resume()V

    :cond_1
    return-void
.end method

.method public setAdCreativeLoadingTimeout(I)V
    .locals 0

    .line 232
    iput p1, p0, Lcom/baidu/mobad/video/XAdContext;->j:I

    return-void
.end method

.method public setAdServerRequestingTimeout(I)V
    .locals 0

    .line 227
    iput p1, p0, Lcom/baidu/mobad/video/XAdContext;->i:I

    return-void
.end method

.method public setContentVideoPlayheadTime(D)V
    .locals 0

    .line 219
    iput-wide p1, p0, Lcom/baidu/mobad/video/XAdContext;->h:D

    return-void
.end method

.method public setContentVideoScreenMode(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;)V
    .locals 8

    .line 171
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->d:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

    .line 175
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {p1}, Lcom/baidu/mobad/video/XAdSlotManager;->retrievePrerollAdSlot()Lcom/baidu/mobads/interfaces/IXLinearAdSlot;

    move-result-object p1

    .line 176
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->d:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;->FULL_SCREEN:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ScreenSizeMode;

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_1

    .line 177
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdProd;->getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    if-ne v0, v1, :cond_1

    .line 178
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdProd;->getCurrentAdInstance()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 180
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdProd;->getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getPlayheadTime()D

    move-result-wide v1

    double-to-int p1, v1

    .line 182
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    .line 183
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 184
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getFullScreenTrackers()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    .line 185
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 186
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "progress"

    invoke-interface {v1, v5, v7, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->addParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 187
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setFullScreenTrackers(Ljava/util/List;)V

    .line 192
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 193
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getFullScreenTrackers()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-direct {p0, p1}, Lcom/baidu/mobad/video/XAdContext;->a(Ljava/util/Set;)V

    :cond_1
    return-void
.end method

.method public setContentVideoState(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->e:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$VideoState;

    return-void
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setSupportTipView(Z)V
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {v0}, Lcom/baidu/mobad/video/XAdSlotManager;->retrievePrerollAdSlot()Lcom/baidu/mobads/interfaces/IXLinearAdSlot;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXLinearAdSlot;->setSupportTipView(Z)V

    return-void
.end method

.method public setVideoDisplayBase(Landroid/widget/RelativeLayout;)V
    .locals 1

    .line 107
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->o:Landroid/widget/RelativeLayout;

    .line 108
    iget-object p1, p0, Lcom/baidu/mobad/video/XAdContext;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/baidu/mobad/video/XAdContext;->setActivity(Landroid/app/Activity;)V

    .line 110
    new-instance p1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobad/video/XAdContext;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/baidu/mobad/video/XAdContext$1;

    invoke-direct {v0, p0}, Lcom/baidu/mobad/video/XAdContext$1;-><init>(Lcom/baidu/mobad/video/XAdContext;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setVideoDisplayBaseHeight(I)V
    .locals 0

    .line 369
    iput p1, p0, Lcom/baidu/mobad/video/XAdContext;->b:I

    return-void
.end method

.method public setVideoDisplayBaseWidth(I)V
    .locals 0

    .line 364
    iput p1, p0, Lcom/baidu/mobad/video/XAdContext;->a:I

    return-void
.end method

.method public submitRequest()V
    .locals 5

    .line 237
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext;->q:Lcom/baidu/mobad/video/XAdSlotManager;

    invoke-virtual {v0}, Lcom/baidu/mobad/video/XAdSlotManager;->retrievePrerollAdSlot()Lcom/baidu/mobads/interfaces/IXLinearAdSlot;

    move-result-object v0

    .line 238
    iget v1, p0, Lcom/baidu/mobad/video/XAdContext;->i:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/baidu/mobad/video/XAdContext;->j:I

    if-lez v1, :cond_0

    .line 239
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getParameter()Ljava/util/HashMap;

    move-result-object v1

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/baidu/mobad/video/XAdContext;->i:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "AD_REQUESTING_TIMEOUT"

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/baidu/mobad/video/XAdContext;->j:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "AD_CREATIVE_LOADING_TIMEOUT"

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/baidu/mobad/video/XAdContext;->a:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "BASE_WIDTH"

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobad/video/XAdContext;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BASE_HEIGHT"

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/IXAdProd;->setParameter(Ljava/util/HashMap;)V

    .line 247
    :cond_0
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->request()V

    return-void
.end method
