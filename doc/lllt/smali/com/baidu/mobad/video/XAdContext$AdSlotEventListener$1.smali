.class Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;


# direct methods
.method constructor <init>(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    iput-object p2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 273
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "complete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/video/XAdEvent4PDK;

    iget-object v2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v2}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v2

    const-string v3, "EVENT_REQUEST_COMPLETE"

    invoke-direct {v1, v3, v2}, Lcom/baidu/mobad/video/XAdEvent4PDK;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStarted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/video/XAdEvent4PDK;

    iget-object v2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v2}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v2

    const-string v3, "EVENT_SLOT_STARTED"

    invoke-direct {v1, v3, v2}, Lcom/baidu/mobad/video/XAdEvent4PDK;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdUserClick"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/video/XAdEvent4PDK;

    iget-object v2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v2}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v2

    const-string v3, "EVENT_SLOT_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/baidu/mobad/video/XAdEvent4PDK;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 285
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdStopped"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_5

    .line 286
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 287
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 289
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    move-result-object v0

    new-instance v2, Lcom/baidu/mobad/video/XAdEvent4PDK;

    iget-object v3, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v3}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v3

    const-string v4, "EVENT_SLOT_ENDED"

    invoke-direct {v2, v4, v3}, Lcom/baidu/mobad/video/XAdEvent4PDK;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    invoke-interface {v0, v2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 291
    :cond_5
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AdError"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 293
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 294
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 297
    :cond_6
    iget-object v0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v0}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/video/XAdEvent4PDK;

    iget-object v2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;->b:Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;

    invoke-static {v2}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;

    move-result-object v2

    const-string v3, "EVENT_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/baidu/mobad/video/XAdEvent4PDK;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    invoke-interface {v0, v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_7
    return-void
.end method
