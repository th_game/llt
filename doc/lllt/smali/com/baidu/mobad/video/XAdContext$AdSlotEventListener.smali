.class public Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/video/XAdContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AdSlotEventListener"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "AdSlotEventListener"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/baidu/mobads/interfaces/IXAdProd;

.field private final c:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdProd;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;)V
    .locals 0

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    iput-object p1, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->a:Landroid/content/Context;

    .line 260
    iput-object p2, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b:Lcom/baidu/mobads/interfaces/IXAdProd;

    .line 261
    iput-object p3, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->c:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/interfaces/IXAdProd;
    .locals 0

    .line 250
    iget-object p0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->b:Lcom/baidu/mobads/interfaces/IXAdProd;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;
    .locals 0

    .line 250
    iget-object p0, p0, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;->c:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventDispatcher;

    return-object p0
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 3

    .line 267
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdSlotEventListener"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/f;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener$1;-><init>(Lcom/baidu/mobad/video/XAdContext$AdSlotEventListener;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/f;->a(Ljava/lang/Runnable;)V

    return-void
.end method
