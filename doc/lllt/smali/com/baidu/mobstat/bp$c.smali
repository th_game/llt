.class Lcom/baidu/mobstat/bp$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/bp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/baidu/mobstat/bp$c;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/baidu/mobstat/bp$c;Landroid/view/View;)V
    .locals 1

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p2, p0, Lcom/baidu/mobstat/bp$c;->d:Lcom/baidu/mobstat/bp$c;

    .line 187
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->l(Landroid/view/View;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobstat/bp$c;->a:Ljava/lang/String;

    .line 189
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobstat/bp$c;->b:Ljava/lang/String;

    .line 191
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;)Ljava/lang/String;

    move-result-object p2

    .line 192
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/baidu/mobstat/bp$c;->c()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 194
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    invoke-static {p1, p3}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Landroid/view/View;)Ljava/lang/String;

    move-result-object p2

    .line 199
    :cond_0
    iput-object p2, p0, Lcom/baidu/mobstat/bp$c;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 4

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x0

    .line 207
    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bp$c;->a(Z)Ljava/lang/String;

    move-result-object v3

    .line 208
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    iget-object v1, v1, Lcom/baidu/mobstat/bp$c;->d:Lcom/baidu/mobstat/bp$c;

    goto :goto_0

    .line 212
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 2

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    .line 243
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    iget-object v1, p0, Lcom/baidu/mobstat/bp$c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_0

    const-string p1, "["

    .line 247
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget-object p1, p0, Lcom/baidu/mobstat/bp$c;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    .line 249
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b()Ljava/lang/String;
    .locals 7

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v2, p0

    :goto_0
    if-eqz v2, :cond_2

    const/4 v4, 0x1

    if-nez v3, :cond_1

    .line 225
    invoke-virtual {v2}, Lcom/baidu/mobstat/bp$c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ListView"

    .line 226
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "RecyclerView"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "GridView"

    .line 227
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 233
    :goto_1
    invoke-virtual {v2, v4}, Lcom/baidu/mobstat/bp$c;->a(Z)Ljava/lang/String;

    move-result-object v4

    .line 234
    invoke-virtual {v0, v1, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    iget-object v2, v2, Lcom/baidu/mobstat/bp$c;->d:Lcom/baidu/mobstat/bp$c;

    goto :goto_0

    .line 238
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/baidu/mobstat/bp$c;->d:Lcom/baidu/mobstat/bp$c;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/baidu/mobstat/bp$c;->b:Ljava/lang/String;

    :goto_0
    return-object v0
.end method
