.class public final enum Lcom/baidu/mobstat/n;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobstat/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobstat/n;

.field public static final enum b:Lcom/baidu/mobstat/n;

.field public static final enum c:Lcom/baidu/mobstat/n;

.field public static final enum d:Lcom/baidu/mobstat/n;

.field public static final enum e:Lcom/baidu/mobstat/n;

.field public static final enum f:Lcom/baidu/mobstat/n;

.field public static final enum g:Lcom/baidu/mobstat/n;

.field public static final enum h:Lcom/baidu/mobstat/n;

.field public static final enum i:Lcom/baidu/mobstat/n;

.field private static final synthetic k:[Lcom/baidu/mobstat/n;


# instance fields
.field public j:J


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 8
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v1, 0x0

    const-string v2, "AP_LIST"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->a:Lcom/baidu/mobstat/n;

    .line 10
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v2, 0x1

    const-string v3, "APP_USER_LIST"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->b:Lcom/baidu/mobstat/n;

    .line 12
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v3, 0x2

    const-string v4, "APP_SYS_LIST"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->c:Lcom/baidu/mobstat/n;

    .line 14
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v4, 0x3

    const-string v5, "APP_TRACE_CURRENT"

    invoke-direct {v0, v5, v4}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->d:Lcom/baidu/mobstat/n;

    .line 16
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v5, 0x4

    const-string v6, "APP_TRACE_HIS"

    invoke-direct {v0, v6, v5}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->e:Lcom/baidu/mobstat/n;

    .line 18
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v6, 0x5

    const-string v7, "APP_CHANGE"

    invoke-direct {v0, v7, v6}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->f:Lcom/baidu/mobstat/n;

    .line 20
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v7, 0x6

    const-string v8, "APP_APK"

    invoke-direct {v0, v8, v7}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->g:Lcom/baidu/mobstat/n;

    .line 22
    new-instance v0, Lcom/baidu/mobstat/n;

    const/4 v8, 0x7

    const-string v9, "LAST_SEND"

    invoke-direct {v0, v9, v8}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->h:Lcom/baidu/mobstat/n;

    .line 24
    new-instance v0, Lcom/baidu/mobstat/n;

    const/16 v9, 0x8

    const-string v10, "LAST_UPDATE"

    invoke-direct {v0, v10, v9}, Lcom/baidu/mobstat/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/n;->i:Lcom/baidu/mobstat/n;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/baidu/mobstat/n;

    .line 6
    sget-object v10, Lcom/baidu/mobstat/n;->a:Lcom/baidu/mobstat/n;

    aput-object v10, v0, v1

    sget-object v1, Lcom/baidu/mobstat/n;->b:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobstat/n;->c:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobstat/n;->d:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobstat/n;->e:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobstat/n;->f:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobstat/n;->g:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobstat/n;->h:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobstat/n;->i:Lcom/baidu/mobstat/n;

    aput-object v1, v0, v9

    sput-object v0, Lcom/baidu/mobstat/n;->k:[Lcom/baidu/mobstat/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobstat/n;
    .locals 1

    .line 6
    const-class v0, Lcom/baidu/mobstat/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobstat/n;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobstat/n;
    .locals 1

    .line 6
    sget-object v0, Lcom/baidu/mobstat/n;->k:[Lcom/baidu/mobstat/n;

    invoke-virtual {v0}, [Lcom/baidu/mobstat/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobstat/n;

    return-object v0
.end method
