.class Lcom/baidu/mobstat/bm$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/bm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private volatile a:Z

.field private b:Z

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/baidu/mobstat/br;

.field private final e:Landroid/os/Handler;

.field private final f:Landroid/os/Handler;

.field private g:Lorg/json/JSONObject;

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/baidu/mobstat/br;Landroid/os/Handler;Landroid/os/Handler;Lorg/json/JSONObject;ZZZ)V
    .locals 1

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 257
    iput-object v0, p0, Lcom/baidu/mobstat/bm$a;->l:Ljava/lang/Runnable;

    .line 147
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/baidu/mobstat/bm$a;->h:Ljava/lang/ref/WeakReference;

    .line 148
    iput-object p6, p0, Lcom/baidu/mobstat/bm$a;->g:Lorg/json/JSONObject;

    .line 149
    iput-object p3, p0, Lcom/baidu/mobstat/bm$a;->d:Lcom/baidu/mobstat/br;

    .line 150
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/baidu/mobstat/bm$a;->c:Ljava/lang/ref/WeakReference;

    .line 151
    iput-object p4, p0, Lcom/baidu/mobstat/bm$a;->e:Landroid/os/Handler;

    .line 152
    iput-object p5, p0, Lcom/baidu/mobstat/bm$a;->f:Landroid/os/Handler;

    const/4 p1, 0x1

    .line 153
    iput-boolean p1, p0, Lcom/baidu/mobstat/bm$a;->b:Z

    const/4 p1, 0x0

    .line 154
    iput-boolean p1, p0, Lcom/baidu/mobstat/bm$a;->a:Z

    .line 155
    iput-boolean p7, p0, Lcom/baidu/mobstat/bm$a;->i:Z

    .line 157
    iput-boolean p8, p0, Lcom/baidu/mobstat/bm$a;->j:Z

    .line 159
    iput-boolean p9, p0, Lcom/baidu/mobstat/bm$a;->k:Z

    .line 161
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 163
    invoke-virtual {p1, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobstat/bm$a;->run()V

    return-void
.end method

.method private a(Lcom/baidu/mobstat/br;Landroid/os/Handler;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-nez p2, :cond_1

    return-void

    .line 318
    :cond_1
    new-instance v0, Lcom/baidu/mobstat/bm$a$2;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobstat/bm$a$2;-><init>(Lcom/baidu/mobstat/bm$a;Lcom/baidu/mobstat/br;)V

    const-wide/16 v1, 0x1f4

    .line 328
    invoke-virtual {p2, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(Ljava/lang/ref/WeakReference;Lorg/json/JSONObject;Lcom/baidu/mobstat/br;Landroid/os/Handler;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;",
            "Lorg/json/JSONObject;",
            "Lcom/baidu/mobstat/br;",
            "Landroid/os/Handler;",
            "Z)V"
        }
    .end annotation

    if-nez p3, :cond_0

    return-void

    :cond_0
    if-nez p4, :cond_1

    return-void

    .line 271
    :cond_1
    new-instance v6, Lcom/baidu/mobstat/bm$a$1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p5

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobstat/bm$a$1;-><init>(Lcom/baidu/mobstat/bm$a;Ljava/lang/ref/WeakReference;ZLcom/baidu/mobstat/br;Lorg/json/JSONObject;)V

    .line 301
    iget-object p1, p0, Lcom/baidu/mobstat/bm$a;->l:Ljava/lang/Runnable;

    if-eqz p1, :cond_2

    .line 302
    invoke-virtual {p4, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 304
    :cond_2
    iput-object v6, p0, Lcom/baidu/mobstat/bm$a;->l:Ljava/lang/Runnable;

    const-wide/16 p1, 0x1f4

    .line 306
    invoke-virtual {p4, v6, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/bm$a;)Z
    .locals 0

    .line 142
    iget-boolean p0, p0, Lcom/baidu/mobstat/bm$a;->k:Z

    return p0
.end method

.method private b()V
    .locals 3

    .line 227
    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->b:Z

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    .line 230
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 231
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 232
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 233
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 235
    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 240
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->d:Lcom/baidu/mobstat/br;

    iget-object v1, p0, Lcom/baidu/mobstat/bm$a;->f:Landroid/os/Handler;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobstat/bm$a;->a(Lcom/baidu/mobstat/br;Landroid/os/Handler;)V

    :cond_2
    const/4 v0, 0x0

    .line 242
    iput-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 219
    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 220
    iput-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->a:Z

    .line 221
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onGlobalLayout()V
    .locals 0

    .line 170
    invoke-virtual {p0}, Lcom/baidu/mobstat/bm$a;->run()V

    return-void
.end method

.method public run()V
    .locals 9

    .line 175
    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_7

    .line 180
    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->a:Z

    if-eqz v0, :cond_1

    goto/16 :goto_1

    .line 185
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    const-string v1, "onGlobalLayout"

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->k:Z

    if-eqz v0, :cond_2

    .line 186
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 188
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 193
    :cond_3
    invoke-static {}, Lcom/baidu/mobstat/am;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 196
    invoke-static {}, Lcom/baidu/mobstat/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 197
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_6

    .line 200
    iget-boolean v1, p0, Lcom/baidu/mobstat/bm$a;->i:Z

    iget-boolean v2, p0, Lcom/baidu/mobstat/bm$a;->k:Z

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/bm;->a(Landroid/app/Activity;ZZ)V

    .line 203
    iget-object v4, p0, Lcom/baidu/mobstat/bm$a;->h:Ljava/lang/ref/WeakReference;

    iget-object v5, p0, Lcom/baidu/mobstat/bm$a;->g:Lorg/json/JSONObject;

    iget-object v6, p0, Lcom/baidu/mobstat/bm$a;->d:Lcom/baidu/mobstat/br;

    iget-object v7, p0, Lcom/baidu/mobstat/bm$a;->f:Landroid/os/Handler;

    iget-boolean v8, p0, Lcom/baidu/mobstat/bm$a;->j:Z

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/baidu/mobstat/bm$a;->a(Ljava/lang/ref/WeakReference;Lorg/json/JSONObject;Lcom/baidu/mobstat/br;Landroid/os/Handler;Z)V

    goto :goto_0

    .line 206
    :cond_4
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    const-string v1, "no touch, skip onGlobalLayout"

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/baidu/mobstat/bm$a;->k:Z

    if-eqz v0, :cond_5

    .line 207
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 209
    :cond_5
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 210
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 215
    :cond_6
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    .line 181
    :cond_7
    :goto_1
    invoke-direct {p0}, Lcom/baidu/mobstat/bm$a;->b()V

    return-void
.end method
