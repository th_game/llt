.class public abstract Lcom/baidu/mobstat/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/ch;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobstat/ce;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 91
    invoke-interface {p1}, Lcom/baidu/mobstat/ce;->a()Ljava/net/InetSocketAddress;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "\" /></cross-domain-policy>\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 93
    :cond_0
    new-instance p1, Lcom/baidu/mobstat/cp;

    const-string v0, "socket not bound"

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/cp;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    return-void
.end method

.method public a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cy;Lcom/baidu/mobstat/df;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V
    .locals 1

    .line 61
    new-instance v0, Lcom/baidu/mobstat/cx;

    invoke-direct {v0, p2}, Lcom/baidu/mobstat/cx;-><init>(Lcom/baidu/mobstat/cw;)V

    .line 62
    sget-object p2, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    invoke-virtual {v0, p2}, Lcom/baidu/mobstat/cx;->a(Lcom/baidu/mobstat/cw$a;)V

    .line 63
    invoke-interface {p1, v0}, Lcom/baidu/mobstat/ce;->a(Lcom/baidu/mobstat/cw;)V

    return-void
.end method

.method public c(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V
    .locals 0

    return-void
.end method
