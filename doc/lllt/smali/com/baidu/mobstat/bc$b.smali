.class public Lcom/baidu/mobstat/bc$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/BaiduStatJSInterface$IWebviewPageLoadCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/bc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 2

    .line 220
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 224
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 228
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    const-string v1, "WebView onPageFinished"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    :cond_2
    const-string v0, "WebViewInterface"

    .line 232
    invoke-virtual {p1, p3, v0}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/baidu/mobstat/bg;->a()Lcom/baidu/mobstat/bg;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobstat/bg;->a(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 1

    .line 198
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result p2

    if-nez p2, :cond_0

    return-void

    .line 202
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobstat/be;->b()Z

    move-result p2

    if-eqz p2, :cond_1

    return-void

    .line 206
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobstat/bo;->b()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 207
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object p2

    const-string v0, "WebView onPageStarted"

    invoke-virtual {p2, v0}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    :cond_2
    const-string p2, "WebViewInterface"

    .line 210
    invoke-virtual {p1, p3, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
