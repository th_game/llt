.class public final enum Lcom/baidu/mobstat/cj$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/cj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobstat/cj$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobstat/cj$a;

.field public static final enum b:Lcom/baidu/mobstat/cj$a;

.field public static final enum c:Lcom/baidu/mobstat/cj$a;

.field private static final synthetic d:[Lcom/baidu/mobstat/cj$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 47
    new-instance v0, Lcom/baidu/mobstat/cj$a;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobstat/cj$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cj$a;->a:Lcom/baidu/mobstat/cj$a;

    new-instance v0, Lcom/baidu/mobstat/cj$a;

    const/4 v2, 0x1

    const-string v3, "ONEWAY"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobstat/cj$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cj$a;->b:Lcom/baidu/mobstat/cj$a;

    new-instance v0, Lcom/baidu/mobstat/cj$a;

    const/4 v3, 0x2

    const-string v4, "TWOWAY"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobstat/cj$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cj$a;->c:Lcom/baidu/mobstat/cj$a;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/baidu/mobstat/cj$a;

    .line 46
    sget-object v4, Lcom/baidu/mobstat/cj$a;->a:Lcom/baidu/mobstat/cj$a;

    aput-object v4, v0, v1

    sget-object v1, Lcom/baidu/mobstat/cj$a;->b:Lcom/baidu/mobstat/cj$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobstat/cj$a;->c:Lcom/baidu/mobstat/cj$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/baidu/mobstat/cj$a;->d:[Lcom/baidu/mobstat/cj$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobstat/cj$a;
    .locals 1

    .line 46
    const-class v0, Lcom/baidu/mobstat/cj$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobstat/cj$a;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobstat/cj$a;
    .locals 1

    .line 46
    sget-object v0, Lcom/baidu/mobstat/cj$a;->d:[Lcom/baidu/mobstat/cj$a;

    invoke-virtual {v0}, [Lcom/baidu/mobstat/cj$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobstat/cj$a;

    return-object v0
.end method
