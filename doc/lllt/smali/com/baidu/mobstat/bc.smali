.class public Lcom/baidu/mobstat/bc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/bc$b;,
        Lcom/baidu/mobstat/bc$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 1

    .line 83
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 87
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 91
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/bd;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;)V
    .locals 1

    .line 48
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 52
    :cond_0
    invoke-static {p0}, Lcom/baidu/mobstat/ay;->a(Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .line 30
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 34
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 38
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bg;->a()Lcom/baidu/mobstat/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/bg;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)V
    .locals 1

    .line 61
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 65
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 69
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/bd;->a(Lorg/json/JSONObject;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .line 105
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 113
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/bd;->b(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .line 249
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 255
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bg;->a()Lcom/baidu/mobstat/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/bg;->a(Ljava/lang/String;)V

    return-void
.end method
