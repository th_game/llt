.class Lcom/baidu/mobstat/y;
.super Lcom/baidu/mobstat/q;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "app_trace3"

    const-string v1, "Create table if not exists app_trace3(_id Integer primary key AUTOINCREMENT,time VARCHAR(50),content TEXT);"

    .line 18
    invoke-direct {p0, v0, v1}, Lcom/baidu/mobstat/q;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/p;",
            ">;"
        }
    .end annotation

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    .line 105
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    :cond_1
    const-string v1, "_id"

    .line 109
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const-string v2, "time"

    .line 110
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v3, "content"

    .line 111
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 113
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 114
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 115
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 116
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 118
    new-instance v8, Lcom/baidu/mobstat/p;

    invoke-direct {v8, v4, v5, v6, v7}, Lcom/baidu/mobstat/p;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    const-string v0, "time"

    const-string v1, "content"

    const/4 v2, 0x1

    .line 62
    invoke-virtual {p0, v1, p2, v0, v2}, Lcom/baidu/mobstat/y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v2

    .line 63
    invoke-direct {p0, v2}, Lcom/baidu/mobstat/y;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v2, :cond_0

    .line 65
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 70
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x0

    .line 71
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobstat/p;

    .line 72
    invoke-virtual {p1}, Lcom/baidu/mobstat/p;->a()J

    move-result-wide p1

    return-wide p1

    .line 76
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 77
    invoke-virtual {v2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0, v2}, Lcom/baidu/mobstat/y;->a(Landroid/content/ContentValues;)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(II)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/p;",
            ">;"
        }
    .end annotation

    const-string v0, "time"

    .line 34
    invoke-virtual {p0, v0, p1, p2}, Lcom/baidu/mobstat/y;->a(Ljava/lang/String;II)Landroid/database/Cursor;

    move-result-object p1

    .line 35
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/y;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 37
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object p2
.end method

.method public b(J)Z
    .locals 0

    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobstat/y;->a(J)Z

    move-result p1

    return p1
.end method
