.class public abstract Lcom/baidu/mobstat/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/cj$a;,
        Lcom/baidu/mobstat/cj$b;
    }
.end annotation


# static fields
.field public static a:I = 0x3e8

.field public static b:I = 0x40

.field public static final c:[B


# instance fields
.field protected d:Lcom/baidu/mobstat/ce$b;

.field protected e:Lcom/baidu/mobstat/cw$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "<policy-file-request/>\u0000"

    .line 53
    invoke-static {v0}, Lcom/baidu/mobstat/di;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/baidu/mobstat/cj;->c:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/baidu/mobstat/cj;->d:Lcom/baidu/mobstat/ce$b;

    .line 58
    iput-object v0, p0, Lcom/baidu/mobstat/cj;->e:Lcom/baidu/mobstat/cw$a;

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;Lcom/baidu/mobstat/ce$b;)Lcom/baidu/mobstat/da;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;,
            Lcom/baidu/mobstat/cm;
        }
    .end annotation

    .line 88
    invoke-static {p0}, Lcom/baidu/mobstat/cj;->b(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v1, 0x3

    const-string v2, " "

    .line 92
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 93
    array-length v2, v0

    if-ne v2, v1, :cond_4

    .line 97
    sget-object v1, Lcom/baidu/mobstat/ce$b;->a:Lcom/baidu/mobstat/ce$b;

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne p1, v1, :cond_0

    .line 99
    new-instance p1, Lcom/baidu/mobstat/dc;

    invoke-direct {p1}, Lcom/baidu/mobstat/dc;-><init>()V

    .line 100
    move-object v1, p1

    check-cast v1, Lcom/baidu/mobstat/dg;

    .line 101
    aget-object v4, v0, v3

    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v4

    invoke-interface {v1, v4}, Lcom/baidu/mobstat/dg;->a(S)V

    .line 102
    aget-object v0, v0, v2

    invoke-interface {v1, v0}, Lcom/baidu/mobstat/dg;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_0
    new-instance p1, Lcom/baidu/mobstat/db;

    invoke-direct {p1}, Lcom/baidu/mobstat/db;-><init>()V

    .line 106
    aget-object v0, v0, v3

    invoke-interface {p1, v0}, Lcom/baidu/mobstat/cz;->a(Ljava/lang/String;)V

    .line 110
    :goto_0
    invoke-static {p0}, Lcom/baidu/mobstat/cj;->b(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    .line 111
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    const-string v1, ":"

    .line 112
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 113
    array-length v1, v0

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    .line 115
    aget-object v1, v0, v1

    aget-object v0, v0, v3

    const-string v4, "^ +"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/baidu/mobstat/da;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-static {p0}, Lcom/baidu/mobstat/cj;->b(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 114
    :cond_1
    new-instance p0, Lcom/baidu/mobstat/cp;

    const-string p1, "not an http header"

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cp;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    if-eqz v0, :cond_3

    return-object p1

    .line 119
    :cond_3
    new-instance p0, Lcom/baidu/mobstat/cm;

    invoke-direct {p0}, Lcom/baidu/mobstat/cm;-><init>()V

    throw p0

    .line 94
    :cond_4
    new-instance p0, Lcom/baidu/mobstat/cp;

    invoke-direct {p0}, Lcom/baidu/mobstat/cp;-><init>()V

    throw p0

    .line 90
    :cond_5
    new-instance p1, Lcom/baidu/mobstat/cm;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result p0

    add-int/lit16 p0, p0, 0x80

    invoke-direct {p1, p0}, Lcom/baidu/mobstat/cm;-><init>(I)V

    goto :goto_3

    :goto_2
    throw p1

    :goto_3
    goto :goto_2
.end method

.method public static a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 4

    .line 61
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/16 v1, 0x30

    .line 64
    :goto_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    .line 67
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v3, 0xd

    if-ne v1, v3, :cond_0

    const/16 v1, 0xa

    if-ne v2, v1, :cond_0

    .line 69
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result p0

    add-int/lit8 p0, p0, -0x2

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 p0, 0x0

    .line 70
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    return-object v0

    :cond_0
    move v1, v2

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int/2addr v1, v0

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    const/4 p0, 0x0

    return-object p0
.end method

.method public static b(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 2

    .line 81
    invoke-static {p0}, Lcom/baidu/mobstat/cj;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result p0

    invoke-static {v0, v1, p0}, Lcom/baidu/mobstat/di;->a([BII)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public a(I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cq;,
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    if-ltz p1, :cond_0

    return p1

    .line 223
    :cond_0
    new-instance p1, Lcom/baidu/mobstat/cn;

    const/16 v0, 0x3ea

    const-string v1, "Negative count"

    invoke-direct {p1, v0, v1}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public abstract a(Lcom/baidu/mobstat/cy;Lcom/baidu/mobstat/df;)Lcom/baidu/mobstat/cj$b;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation
.end method

.method public abstract a(Lcom/baidu/mobstat/cz;)Lcom/baidu/mobstat/cz;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation
.end method

.method public abstract a(Lcom/baidu/mobstat/cw;)Ljava/nio/ByteBuffer;
.end method

.method public a(Lcom/baidu/mobstat/dd;Lcom/baidu/mobstat/ce$b;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobstat/dd;",
            "Lcom/baidu/mobstat/ce$b;",
            ")",
            "Ljava/util/List<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 166
    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/dd;Lcom/baidu/mobstat/ce$b;Z)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/baidu/mobstat/dd;Lcom/baidu/mobstat/ce$b;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobstat/dd;",
            "Lcom/baidu/mobstat/ce$b;",
            "Z)",
            "Ljava/util/List<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .line 170
    new-instance p2, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 171
    instance-of v0, p1, Lcom/baidu/mobstat/cy;

    if-eqz v0, :cond_0

    const-string v0, "GET "

    .line 172
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    move-object v0, p1

    check-cast v0, Lcom/baidu/mobstat/cy;

    invoke-interface {v0}, Lcom/baidu/mobstat/cy;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " HTTP/1.1"

    .line 174
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 175
    :cond_0
    instance-of v0, p1, Lcom/baidu/mobstat/df;

    if-eqz v0, :cond_5

    const-string v0, "HTTP/1.1 101 "

    .line 176
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p1

    check-cast v0, Lcom/baidu/mobstat/df;

    invoke-interface {v0}, Lcom/baidu/mobstat/df;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string v0, "\r\n"

    .line 180
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-interface {p1}, Lcom/baidu/mobstat/dd;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 182
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 184
    invoke-interface {p1, v2}, Lcom/baidu/mobstat/dd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ": "

    .line 186
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 190
    :cond_1
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/baidu/mobstat/di;->b(Ljava/lang/String;)[B

    move-result-object p2

    if-eqz p3, :cond_2

    .line 193
    invoke-interface {p1}, Lcom/baidu/mobstat/dd;->c()[B

    move-result-object p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    if-nez p1, :cond_3

    const/4 p3, 0x0

    goto :goto_3

    .line 194
    :cond_3
    array-length p3, p1

    :goto_3
    array-length v0, p2

    add-int/2addr p3, v0

    invoke-static {p3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p3

    .line 195
    invoke-virtual {p3, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    if-eqz p1, :cond_4

    .line 197
    invoke-virtual {p3, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 198
    :cond_4
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 199
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 178
    :cond_5
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "unknown role"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :goto_4
    throw p1

    :goto_5
    goto :goto_4
.end method

.method public abstract a(Ljava/nio/ByteBuffer;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/cw;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public a(Lcom/baidu/mobstat/ce$b;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/baidu/mobstat/cj;->d:Lcom/baidu/mobstat/ce$b;

    return-void
.end method

.method public abstract b()Lcom/baidu/mobstat/cj$a;
.end method

.method public abstract c()Lcom/baidu/mobstat/cj;
.end method

.method public abstract c(Ljava/nio/ByteBuffer;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/cw;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation
.end method

.method public d(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/dd;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/baidu/mobstat/cj;->d:Lcom/baidu/mobstat/ce$b;

    invoke-static {p1, v0}, Lcom/baidu/mobstat/cj;->a(Ljava/nio/ByteBuffer;Lcom/baidu/mobstat/ce$b;)Lcom/baidu/mobstat/da;

    move-result-object p1

    return-object p1
.end method
