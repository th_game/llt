.class public Lcom/baidu/mobstat/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/app/Activity;Z)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 40
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/az;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 44
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/ba;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    .line 48
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public static a(Landroid/view/KeyEvent;)V
    .locals 1

    .line 140
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/az;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 144
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 148
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/ba;->a(Landroid/view/KeyEvent;)V

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/app/Activity;)V
    .locals 1

    .line 120
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/az;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 124
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 128
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;Landroid/app/Activity;)V

    return-void
.end method

.method public static a(Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;)V
    .locals 1

    .line 21
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/ba;->a(Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .line 166
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 170
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static b(Landroid/app/Activity;Z)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/az;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 71
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/ba;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    .line 75
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobstat/ba;->b(Landroid/app/Activity;)V

    return-void
.end method

.method public static c(Landroid/app/Activity;Z)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/az;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 98
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobstat/ba;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    .line 102
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/ba;->a()Lcom/baidu/mobstat/ba;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobstat/ba;->c(Landroid/app/Activity;)V

    return-void
.end method
