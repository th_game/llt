.class public Lcom/baidu/mobstat/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lorg/json/JSONArray;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:J

.field private i:J

.field private j:J

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIJJJLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    move-object v0, p0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 31
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->a:Ljava/lang/String;

    move-object v1, p2

    .line 32
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->b:Ljava/lang/String;

    move-object v1, p3

    .line 33
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->c:Lorg/json/JSONArray;

    move-object v1, p4

    .line 34
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->d:Ljava/lang/String;

    move-object v1, p5

    .line 35
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->e:Ljava/lang/String;

    move-object v1, p6

    .line 36
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->f:Ljava/lang/String;

    move v1, p7

    .line 37
    iput-boolean v1, v0, Lcom/baidu/mobstat/ax;->g:Z

    move v1, p8

    .line 38
    iput v1, v0, Lcom/baidu/mobstat/ax;->k:I

    move-wide v1, p9

    .line 39
    iput-wide v1, v0, Lcom/baidu/mobstat/ax;->h:J

    move-wide v1, p11

    .line 40
    iput-wide v1, v0, Lcom/baidu/mobstat/ax;->i:J

    move-wide/from16 v1, p13

    .line 41
    iput-wide v1, v0, Lcom/baidu/mobstat/ax;->j:J

    move-object/from16 v1, p15

    .line 42
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->l:Ljava/lang/String;

    move-object/from16 v1, p16

    .line 43
    iput-object v1, v0, Lcom/baidu/mobstat/ax;->m:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    .line 190
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "id"

    .line 191
    invoke-virtual {v1, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "p"

    .line 192
    invoke-virtual {v1, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "path"

    .line 193
    invoke-virtual {v1, p0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "title"

    .line 195
    invoke-virtual {v1, p0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "index"

    .line 196
    invoke-virtual {v1, p0, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "n"

    .line 197
    invoke-virtual {v1, p0, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "user"

    if-eqz p6, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 198
    :goto_0
    invoke-virtual {v1, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 200
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-object p0, v0

    .line 206
    :goto_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 207
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-static {p0}, Lcom/baidu/mobstat/bz$a;->a([B)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 7

    .line 155
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/baidu/mobstat/ax;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobstat/ax;->c:Lorg/json/JSONArray;

    iget-object v3, p0, Lcom/baidu/mobstat/ax;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/baidu/mobstat/ax;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/baidu/mobstat/ax;->f:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/baidu/mobstat/ax;->g:Z

    invoke-static/range {v0 .. v6}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 161
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v3, "id"

    .line 164
    iget-object v4, p0, Lcom/baidu/mobstat/ax;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "d"

    .line 165
    iget-object v4, p0, Lcom/baidu/mobstat/ax;->l:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "p"

    .line 166
    invoke-virtual {v1, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "path"

    .line 167
    invoke-virtual {v1, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "title"

    .line 169
    iget-object p3, p0, Lcom/baidu/mobstat/ax;->d:Ljava/lang/String;

    invoke-virtual {v1, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "index"

    .line 170
    iget-object p3, p0, Lcom/baidu/mobstat/ax;->e:Ljava/lang/String;

    invoke-virtual {v1, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "n"

    .line 171
    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "user"

    .line 172
    iget-boolean p2, p0, Lcom/baidu/mobstat/ax;->g:Z

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "c"

    .line 173
    iget p2, p0, Lcom/baidu/mobstat/ax;->k:I

    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "t"

    .line 174
    iget-wide p2, p0, Lcom/baidu/mobstat/ax;->h:J

    invoke-virtual {v1, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string p1, "ps"

    .line 175
    iget-object p2, p0, Lcom/baidu/mobstat/ax;->m:Ljava/lang/String;

    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "sign"

    .line 177
    invoke-virtual {v1, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-object v1, v2

    :goto_1
    return-object v1
.end method

.method public a(I)V
    .locals 0

    .line 111
    iput p1, p0, Lcom/baidu/mobstat/ax;->k:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 127
    iput-wide p1, p0, Lcom/baidu/mobstat/ax;->i:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/baidu/mobstat/ax;->l:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 0

    .line 131
    iput-wide p1, p0, Lcom/baidu/mobstat/ax;->j:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/baidu/mobstat/ax;->m:Ljava/lang/String;

    return-void
.end method

.method public c()Lorg/json/JSONArray;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->c:Lorg/json/JSONArray;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/baidu/mobstat/ax;->g:Z

    return v0
.end method

.method public h()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/baidu/mobstat/ax;->k:I

    return v0
.end method

.method public i()J
    .locals 2

    .line 79
    iget-wide v0, p0, Lcom/baidu/mobstat/ax;->h:J

    return-wide v0
.end method

.method public j()J
    .locals 2

    .line 119
    iget-wide v0, p0, Lcom/baidu/mobstat/ax;->i:J

    return-wide v0
.end method

.method public k()J
    .locals 2

    .line 123
    iget-wide v0, p0, Lcom/baidu/mobstat/ax;->j:J

    return-wide v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/baidu/mobstat/ax;->l:Ljava/lang/String;

    return-object v0
.end method
