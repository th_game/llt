.class public Lcom/baidu/mobstat/BasicStoreTools;
.super Lcom/baidu/mobstat/bu;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "__Baidu_Stat_SDK_SendRem"

.field private static b:Lcom/baidu/mobstat/BasicStoreTools;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Lcom/baidu/mobstat/BasicStoreTools;

    invoke-direct {v0}, Lcom/baidu/mobstat/BasicStoreTools;-><init>()V

    sput-object v0, Lcom/baidu/mobstat/BasicStoreTools;->b:Lcom/baidu/mobstat/BasicStoreTools;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/baidu/mobstat/bu;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobstat/BasicStoreTools;
    .locals 1

    .line 62
    sget-object v0, Lcom/baidu/mobstat/BasicStoreTools;->b:Lcom/baidu/mobstat/BasicStoreTools;

    return-object v0
.end method


# virtual methods
.method public getAppChannelWithCode(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "setchannelwithcode"

    const/4 v1, 0x0

    .line 221
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getAppChannelWithPreference(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "setchannelwithcodevalue"

    const/4 v1, 0x0

    .line 200
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAppDeviceMac(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "mtjsdkmacss2_1"

    const/4 v1, 0x0

    .line 263
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAppDeviceMacTv(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "mtjsdkmacsstv_1"

    const/4 v1, 0x0

    .line 326
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAppKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "mjsetappkey"

    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAutoTraceConfigFetchTime(Landroid/content/Context;)J
    .locals 3

    const-string v0, "autotrace_config_fetch_time"

    const-wide/16 v1, 0x0

    .line 440
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/baidu/mobstat/BasicStoreTools;->getLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getAutoTraceTrackJsFetchInterval(Landroid/content/Context;)J
    .locals 3

    const-string v0, "autotrace_track_js_fetch_interval"

    const-wide/16 v1, 0x0

    .line 422
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/baidu/mobstat/BasicStoreTools;->getLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getAutoTraceTrackJsFetchTime(Landroid/content/Context;)J
    .locals 3

    const-string v0, "autotrace_track_js_fetch_time"

    const-wide/16 v1, 0x0

    .line 404
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/baidu/mobstat/BasicStoreTools;->getLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getForTV(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "mtjtv"

    const/4 v1, 0x0

    .line 305
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getGenerateDeviceCUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "cuidsec_1"

    const/4 v1, 0x0

    .line 179
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getGenerateDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "device_id_1"

    const/4 v1, 0x0

    .line 154
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getHeaderExt(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "he.ext"

    const/4 v1, 0x0

    .line 347
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLastData(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "lastdata"

    const/4 v1, 0x0

    .line 284
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getMacEnabledTrick(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "mtjsdkmactrick"

    const/4 v1, 0x1

    .line 385
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getOnlyWifiChannel(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "onlywifi"

    const/4 v1, 0x0

    .line 132
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getPushId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "he.push"

    const/4 v1, 0x0

    .line 368
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSendStrategy(Landroid/content/Context;)I
    .locals 2

    const-string v0, "sendLogtype"

    const/4 v1, 0x0

    .line 88
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public getSendStrategyTime(Landroid/content/Context;)I
    .locals 2

    const-string v0, "timeinterval"

    const/4 v1, 0x1

    .line 110
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .line 66
    sget-object v0, Lcom/baidu/mobstat/BasicStoreTools;->a:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    return-object p1
.end method

.method public getUserId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "custom_userid"

    const-string v1, ""

    .line 463
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public setAppChannelWithCode(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "setchannelwithcode"

    .line 210
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public setAppChannelWithPreference(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "setchannelwithcodevalue"

    .line 189
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAppDeviceMac(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "mtjsdkmacss2_1"

    .line 252
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAppDeviceMacTv(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "mtjsdkmacsstv_1"

    .line 315
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAppKey(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "mjsetappkey"

    .line 231
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAutoTraceConfigFetchTime(Landroid/content/Context;J)V
    .locals 1

    const-string v0, "autotrace_config_fetch_time"

    .line 431
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/baidu/mobstat/BasicStoreTools;->putLong(Landroid/content/Context;Ljava/lang/String;J)V

    return-void
.end method

.method public setAutoTraceTrackJsFetchInterval(Landroid/content/Context;J)V
    .locals 1

    const-string v0, "autotrace_track_js_fetch_interval"

    .line 413
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/baidu/mobstat/BasicStoreTools;->putLong(Landroid/content/Context;Ljava/lang/String;J)V

    return-void
.end method

.method public setAutoTraceTrackJsFetchTime(Landroid/content/Context;J)V
    .locals 1

    const-string v0, "autotrace_track_js_fetch_time"

    .line 395
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/baidu/mobstat/BasicStoreTools;->putLong(Landroid/content/Context;Ljava/lang/String;J)V

    return-void
.end method

.method public setForTV(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "mtjtv"

    .line 294
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public setGenerateDeviceCUID(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const-string v0, "cuid"

    const/4 v1, 0x0

    .line 164
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobstat/BasicStoreTools;->removeString(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    const-string v0, "cuidsec_1"

    .line 168
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setGenerateDeviceId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "device_id_1"

    .line 143
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setHeaderExt(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "he.ext"

    .line 336
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLastData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "lastdata"

    .line 273
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setMacEnabledTrick(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "mtjsdkmactrick"

    .line 376
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public setOnlyWifi(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "onlywifi"

    .line 121
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public setPushId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "he.push"

    .line 357
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSendStrategy(Landroid/content/Context;I)V
    .locals 1

    const-string v0, "sendLogtype"

    .line 77
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putInt(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public setSendStrategyTime(Landroid/content/Context;I)V
    .locals 1

    const-string v0, "timeinterval"

    .line 99
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putInt(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public setUserId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 450
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, ""

    :cond_0
    const-string v0, "custom_userid"

    .line 454
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/BasicStoreTools;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
