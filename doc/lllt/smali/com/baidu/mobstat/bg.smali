.class public Lcom/baidu/mobstat/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Lcom/baidu/mobstat/bg;

.field private static volatile l:Z


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/app/Activity;

.field private volatile c:Z

.field private volatile d:Z

.field private volatile e:Ljava/lang/String;

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Landroid/graphics/PointF;

.field private j:Lcom/baidu/mobstat/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Lcom/baidu/mobstat/bg;

    invoke-direct {v0}, Lcom/baidu/mobstat/bg;-><init>()V

    sput-object v0, Lcom/baidu/mobstat/bg;->k:Lcom/baidu/mobstat/bg;

    const/4 v0, 0x1

    .line 66
    sput-boolean v0, Lcom/baidu/mobstat/bg;->l:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/baidu/mobstat/bm;->a()Lcom/baidu/mobstat/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/bg;->j:Lcom/baidu/mobstat/bm;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/bg;)Landroid/graphics/PointF;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobstat/bg;->i:Landroid/graphics/PointF;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobstat/bg;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/baidu/mobstat/bg;->i:Landroid/graphics/PointF;

    return-object p1
.end method

.method private a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
    .locals 1

    :goto_0
    if-eqz p1, :cond_0

    .line 185
    instance-of v0, p1, Lcom/baidu/mobstat/as;

    if-eqz v0, :cond_0

    .line 186
    check-cast p1, Lcom/baidu/mobstat/as;

    .line 187
    invoke-virtual {p1}, Lcom/baidu/mobstat/as;->a()Landroid/view/Window$Callback;

    move-result-object p1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method public static a()Lcom/baidu/mobstat/bg;
    .locals 1

    .line 43
    sget-object v0, Lcom/baidu/mobstat/bg;->k:Lcom/baidu/mobstat/bg;

    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobstat/bg;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Z)V
    .locals 0

    if-eqz p0, :cond_0

    .line 70
    invoke-static {}, Lcom/baidu/mobstat/bm;->b()V

    .line 73
    :cond_0
    sput-boolean p0, Lcom/baidu/mobstat/bg;->l:Z

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/bg;Z)Z
    .locals 0

    .line 23
    iput-boolean p1, p0, Lcom/baidu/mobstat/bg;->c:Z

    return p1
.end method

.method private b(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 6

    if-nez p3, :cond_0

    return-void

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobstat/bg;->b:Landroid/app/Activity;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p3

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobstat/bs;->a(Landroid/app/Activity;Landroid/webkit/WebView;Ljava/lang/String;Lorg/json/JSONObject;Z)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobstat/bg;)Z
    .locals 0

    .line 23
    iget-boolean p0, p0, Lcom/baidu/mobstat/bg;->c:Z

    return p0
.end method

.method static synthetic c(Lcom/baidu/mobstat/bg;)Landroid/content/Context;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    return-object p0
.end method

.method private c(Landroid/app/Activity;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 91
    :cond_1
    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 98
    :cond_2
    new-instance v1, Lcom/baidu/mobstat/as;

    new-instance v2, Lcom/baidu/mobstat/bg$1;

    invoke-direct {v2, p0}, Lcom/baidu/mobstat/bg$1;-><init>(Lcom/baidu/mobstat/bg;)V

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobstat/as;-><init>(Landroid/view/Window$Callback;Lcom/baidu/mobstat/as$a;)V

    invoke-virtual {p1, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public static c()Z
    .locals 1

    .line 77
    sget-boolean v0, Lcom/baidu/mobstat/bg;->l:Z

    return v0
.end method

.method static synthetic d(Lcom/baidu/mobstat/bg;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobstat/bg;->h:Ljava/lang/String;

    return-object p0
.end method

.method private d(Landroid/app/Activity;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 168
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 173
    :cond_1
    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/bg;->a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;

    move-result-object v0

    .line 174
    invoke-virtual {p1, v0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    return-void
.end method

.method private d()Z
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private e()V
    .locals 5

    .line 229
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobstat/cc;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 233
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/bg;->c:Z

    if-eqz v0, :cond_1

    return-void

    .line 238
    :cond_1
    iget-boolean v0, p0, Lcom/baidu/mobstat/bg;->d:Z

    if-nez v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    const-string v1, "mtj_autoTracker.js"

    invoke-static {v0, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    const/4 v0, 0x1

    .line 240
    iput-boolean v0, p0, Lcom/baidu/mobstat/bg;->d:Z

    .line 244
    :cond_2
    iget-wide v0, p0, Lcom/baidu/mobstat/bg;->f:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    .line 245
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getAutoTraceTrackJsFetchTime(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobstat/bg;->f:J

    .line 246
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getAutoTraceTrackJsFetchInterval(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobstat/bg;->g:J

    .line 249
    :cond_3
    iget-boolean v0, p0, Lcom/baidu/mobstat/bg;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 250
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobstat/bg;->f:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/baidu/mobstat/bg;->g:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_6

    .line 251
    :cond_5
    invoke-direct {p0}, Lcom/baidu/mobstat/bg;->f()V

    :cond_6
    return-void
.end method

.method private f()V
    .locals 2

    .line 256
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/mobstat/bg$2;

    invoke-direct {v1, p0}, Lcom/baidu/mobstat/bg$2;-><init>(Lcom/baidu/mobstat/bg;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const-string v1, "downloadThread"

    .line 274
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 3

    .line 138
    invoke-direct {p0}, Lcom/baidu/mobstat/bg;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 143
    invoke-static {v0}, Lcom/baidu/mobstat/bg;->a(Z)V

    .line 145
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    .line 146
    iput-object p1, p0, Lcom/baidu/mobstat/bg;->b:Landroid/app/Activity;

    .line 149
    invoke-direct {p0}, Lcom/baidu/mobstat/bg;->e()V

    .line 152
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/bg;->c(Landroid/app/Activity;)V

    .line 155
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->j:Lcom/baidu/mobstat/bm;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1, v2}, Lcom/baidu/mobstat/bm;->a(Landroid/app/Activity;ZLorg/json/JSONObject;Z)V

    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 1

    .line 212
    iget-object p2, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 213
    iget-object p2, p0, Lcom/baidu/mobstat/bg;->a:Landroid/content/Context;

    const-string v0, "mtj_autoTracker.js"

    invoke-static {p2, v0}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    .line 216
    :cond_0
    iget-object p2, p0, Lcom/baidu/mobstat/bg;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/bg;->b(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 54
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/be;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Landroid/graphics/PointF;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->i:Landroid/graphics/PointF;

    return-object v0
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .line 198
    invoke-direct {p0}, Lcom/baidu/mobstat/bg;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->b:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/bg;->d(Landroid/app/Activity;)V

    const/4 v0, 0x0

    .line 205
    iput-object v0, p0, Lcom/baidu/mobstat/bg;->b:Landroid/app/Activity;

    .line 208
    iget-object v0, p0, Lcom/baidu/mobstat/bg;->j:Lcom/baidu/mobstat/bm;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/baidu/mobstat/bm;->a(Landroid/app/Activity;Z)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/baidu/mobstat/bg;->h:Ljava/lang/String;

    return-void
.end method
