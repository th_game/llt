.class public Lcom/baidu/mobstat/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/baidu/mobstat/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Lcom/baidu/mobstat/ag;

    invoke-direct {v0}, Lcom/baidu/mobstat/ag;-><init>()V

    sput-object v0, Lcom/baidu/mobstat/ag;->a:Lcom/baidu/mobstat/ag;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 1

    .line 114
    new-instance v0, Lcom/baidu/mobstat/aj;

    invoke-direct {v0, p1}, Lcom/baidu/mobstat/aj;-><init>(Lorg/json/JSONObject;)V

    .line 116
    iget-boolean p1, v0, Lcom/baidu/mobstat/aj;->a:Z

    sput-boolean p1, Lcom/baidu/mobstat/ai;->b:Z

    .line 117
    iget-object p1, v0, Lcom/baidu/mobstat/aj;->b:Ljava/lang/String;

    sput-object p1, Lcom/baidu/mobstat/ai;->c:Ljava/lang/String;

    .line 118
    iget-boolean p1, v0, Lcom/baidu/mobstat/aj;->c:Z

    sput-boolean p1, Lcom/baidu/mobstat/ai;->d:Z

    return-void
.end method

.method private a()Z
    .locals 2

    .line 261
    sget-object v0, Lcom/baidu/mobstat/r;->a:Lcom/baidu/mobstat/r;

    invoke-virtual {v0}, Lcom/baidu/mobstat/r;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 266
    :cond_0
    sget-object v0, Lcom/baidu/mobstat/r;->b:Lcom/baidu/mobstat/r;

    invoke-virtual {v0}, Lcom/baidu/mobstat/r;->b()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 271
    :cond_1
    sget-object v0, Lcom/baidu/mobstat/r;->c:Lcom/baidu/mobstat/r;

    invoke-virtual {v0}, Lcom/baidu/mobstat/r;->b()Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 276
    :cond_2
    sget-object v0, Lcom/baidu/mobstat/r;->d:Lcom/baidu/mobstat/r;

    invoke-virtual {v0}, Lcom/baidu/mobstat/r;->b()Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 281
    :cond_3
    sget-object v0, Lcom/baidu/mobstat/r;->e:Lcom/baidu/mobstat/r;

    invoke-virtual {v0}, Lcom/baidu/mobstat/r;->b()Z

    move-result v0

    if-nez v0, :cond_4

    return v1

    :cond_4
    const/4 v0, 0x0

    return v0
.end method

.method private b(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 5

    .line 292
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "he"

    .line 294
    invoke-virtual {v0, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v1, p2

    goto :goto_0

    :catch_0
    move-exception p2

    .line 299
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 305
    :goto_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "APP_MEM"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 306
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p2

    .line 307
    invoke-virtual {p2}, Lcom/baidu/mobstat/af;->b()Z

    move-result p2

    if-nez p2, :cond_0

    .line 309
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->x(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 310
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 311
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 312
    invoke-virtual {v2, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 314
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_0

    :try_start_1
    const-string p2, "app_mem3"

    .line 316
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 318
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    add-int/2addr v1, p2

    goto :goto_1

    :catch_1
    move-exception p2

    .line 321
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 326
    :cond_0
    :goto_1
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "APP_APK"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 327
    sget-object p2, Lcom/baidu/mobstat/r;->e:Lcom/baidu/mobstat/r;

    const/16 v2, 0x5000

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/r;->a(I)Ljava/util/List;

    move-result-object p2

    .line 328
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 329
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 330
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 331
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    .line 334
    :cond_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_2

    :try_start_2
    const-string p2, "app_apk3"

    .line 336
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 338
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    add-int/2addr v1, p2

    goto :goto_3

    :catch_2
    move-exception p2

    .line 341
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 345
    :cond_2
    :goto_3
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "APP_CHANGE"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 346
    sget-object p2, Lcom/baidu/mobstat/r;->d:Lcom/baidu/mobstat/r;

    const/16 v2, 0x2800

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/r;->a(I)Ljava/util/List;

    move-result-object p2

    .line 347
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 348
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 349
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_4

    .line 353
    :cond_3
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_4

    :try_start_3
    const-string p2, "app_change3"

    .line 355
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 357
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    add-int/2addr v1, p2

    goto :goto_5

    :catch_3
    move-exception p2

    .line 360
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 364
    :cond_4
    :goto_5
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "APP_TRACE"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 365
    sget-object p2, Lcom/baidu/mobstat/r;->c:Lcom/baidu/mobstat/r;

    const/16 v2, 0x3c00

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/r;->a(I)Ljava/util/List;

    move-result-object p2

    .line 366
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 367
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_6
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 368
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 369
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_6

    .line 372
    :cond_5
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_6

    :try_start_4
    const-string p2, "app_trace3"

    .line 375
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 377
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    add-int/2addr v1, p2

    goto :goto_7

    :catch_4
    move-exception p2

    .line 380
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 384
    :cond_6
    :goto_7
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "APP_LIST"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 385
    sget-object p2, Lcom/baidu/mobstat/r;->b:Lcom/baidu/mobstat/r;

    const v2, 0xb400

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/r;->a(I)Ljava/util/List;

    move-result-object p2

    .line 386
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 387
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_8
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 388
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_8

    .line 392
    :cond_7
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_8

    :try_start_5
    const-string p2, "app_list3"

    .line 395
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 397
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    add-int/2addr v1, p2

    goto :goto_9

    :catch_5
    move-exception p2

    .line 400
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 404
    :cond_8
    :goto_9
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v2, "AP_LIST"

    invoke-virtual {p2, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const p2, 0x2d000

    sub-int/2addr p2, v1

    .line 406
    sget-object v2, Lcom/baidu/mobstat/r;->a:Lcom/baidu/mobstat/r;

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/r;->a(I)Ljava/util/List;

    move-result-object p2

    .line 407
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 408
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_a
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 409
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 410
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_a

    .line 413
    :cond_9
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-lez p2, :cond_a

    :try_start_6
    const-string p2, "ap_list3"

    .line 415
    invoke-virtual {v0, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    add-int/2addr v1, p2

    goto :goto_b

    :catch_6
    move-exception p2

    .line 420
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    .line 424
    :cond_a
    :goto_b
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "log in bytes is almost :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 426
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    .line 427
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 429
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_7
    const-string v1, "payload"

    .line 431
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 432
    invoke-static {}, Lcom/baidu/mobstat/z;->a()Lcom/baidu/mobstat/z;

    move-result-object p2

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/baidu/mobstat/z;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_c

    :catch_7
    move-exception p1

    .line 434
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    :goto_c
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 9

    .line 128
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPWithStretegy 1"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 129
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/baidu/mobstat/n;->a:Lcom/baidu/mobstat/n;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v1

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 132
    invoke-virtual {v0}, Lcom/baidu/mobstat/af;->e()J

    move-result-wide v5

    .line 134
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, ": last time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, "; time interval: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 135
    invoke-virtual {v0, v7}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const-wide/16 v7, 0x0

    cmp-long v0, v1, v7

    if-eqz v0, :cond_0

    sub-long/2addr v3, v1

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    .line 138
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPWithStretegy 2"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 140
    invoke-static {p1}, Lcom/baidu/mobstat/j;->a(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 14

    .line 151
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPPListWithStretegy 1"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 154
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v2

    .line 156
    sget-object v3, Lcom/baidu/mobstat/n;->b:Lcom/baidu/mobstat/n;

    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v3

    .line 157
    invoke-virtual {v2}, Lcom/baidu/mobstat/af;->f()J

    move-result-wide v5

    .line 158
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "now time: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v10, ": last time: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, "; userInterval : "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 159
    invoke-virtual {v7, v8}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const-wide/16 v7, 0x0

    cmp-long v11, v3, v7

    if-eqz v11, :cond_0

    sub-long v11, v0, v3

    cmp-long v13, v11, v5

    if-gtz v13, :cond_0

    .line 160
    invoke-virtual {v2, v3, v4}, Lcom/baidu/mobstat/af;->a(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 161
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v3

    const-string v4, "collectUserAPPListWithStretegy 2"

    invoke-virtual {v3, v4}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 163
    invoke-static {p1, v3}, Lcom/baidu/mobstat/j;->a(Landroid/content/Context;Z)V

    .line 166
    :cond_1
    sget-object v3, Lcom/baidu/mobstat/n;->c:Lcom/baidu/mobstat/n;

    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v3

    .line 167
    invoke-virtual {v2}, Lcom/baidu/mobstat/af;->g()J

    move-result-wide v5

    .line 168
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v9, "; sysInterval : "

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 169
    invoke-virtual {v2, v9}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    cmp-long v2, v3, v7

    if-eqz v2, :cond_2

    sub-long/2addr v0, v3

    cmp-long v2, v0, v5

    if-lez v2, :cond_3

    .line 171
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectSysAPPListWithStretegy 2"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 173
    invoke-static {p1, v0}, Lcom/baidu/mobstat/j;->a(Landroid/content/Context;Z)V

    :cond_3
    return-void
.end method

.method private e(Landroid/content/Context;)V
    .locals 9

    .line 184
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPPTraceWithStretegy 1"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 187
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v2

    .line 201
    sget-object v3, Lcom/baidu/mobstat/n;->e:Lcom/baidu/mobstat/n;

    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v3

    .line 202
    invoke-virtual {v2}, Lcom/baidu/mobstat/af;->i()J

    move-result-wide v5

    .line 203
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, ": last time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, "; time interval: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 204
    invoke-virtual {v2, v7}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const-wide/16 v7, 0x0

    cmp-long v2, v3, v7

    if-eqz v2, :cond_0

    sub-long/2addr v0, v3

    cmp-long v2, v0, v5

    if-lez v2, :cond_1

    .line 206
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPPTraceWithStretegy 2"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 208
    invoke-static {p1, v0}, Lcom/baidu/mobstat/j;->b(Landroid/content/Context;Z)V

    :cond_1
    return-void
.end method

.method private f(Landroid/content/Context;)V
    .locals 9

    .line 218
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPKWithStretegy 1"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 220
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 221
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v2

    .line 223
    sget-object v3, Lcom/baidu/mobstat/n;->g:Lcom/baidu/mobstat/n;

    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v3

    .line 224
    invoke-virtual {v2}, Lcom/baidu/mobstat/af;->h()J

    move-result-wide v5

    .line 225
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, ": last time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, "; interval : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const-wide/16 v7, 0x0

    cmp-long v2, v3, v7

    if-eqz v2, :cond_0

    sub-long/2addr v0, v3

    cmp-long v2, v0, v5

    if-lez v2, :cond_1

    .line 227
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "collectAPKWithStretegy 2"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 229
    invoke-static {p1}, Lcom/baidu/mobstat/j;->b(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method private g(Landroid/content/Context;)V
    .locals 4

    .line 246
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobstat/n;->h:Lcom/baidu/mobstat/n;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;J)V

    .line 248
    invoke-static {p1}, Lcom/baidu/mobstat/o;->a(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 249
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "header: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 252
    :goto_0
    invoke-direct {p0}, Lcom/baidu/mobstat/ag;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_0

    .line 254
    invoke-static {v0}, Lcom/baidu/mobstat/o;->c(Lorg/json/JSONObject;)V

    .line 256
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobstat/ag;->b(Landroid/content/Context;Lorg/json/JSONObject;)V

    move v1, v2

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;J)V
    .locals 1

    .line 242
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobstat/n;->i:Lcom/baidu/mobstat/n;

    invoke-virtual {p1, v0, p2, p3}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;J)V

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 234
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/baidu/mobstat/af;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 4

    .line 52
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    const-string v1, "startDataAnynalyzed start"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p2}, Lcom/baidu/mobstat/ag;->a(Lorg/json/JSONObject;)V

    .line 56
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p2

    .line 57
    invoke-virtual {p2}, Lcom/baidu/mobstat/af;->a()Z

    move-result v0

    .line 58
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is data collect closed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    if-nez v0, :cond_6

    .line 61
    sget-object v0, Lcom/baidu/mobstat/r;->a:Lcom/baidu/mobstat/r;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/r;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ag;->c(Landroid/content/Context;)V

    .line 66
    :cond_0
    sget-object v0, Lcom/baidu/mobstat/r;->b:Lcom/baidu/mobstat/r;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/r;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ag;->d(Landroid/content/Context;)V

    .line 71
    :cond_1
    sget-object v0, Lcom/baidu/mobstat/r;->c:Lcom/baidu/mobstat/r;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/r;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ag;->e(Landroid/content/Context;)V

    .line 76
    :cond_2
    sget-boolean v0, Lcom/baidu/mobstat/ai;->e:Z

    if-eqz v0, :cond_3

    .line 77
    sget-object v0, Lcom/baidu/mobstat/r;->e:Lcom/baidu/mobstat/r;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/r;->b(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 79
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ag;->f(Landroid/content/Context;)V

    .line 83
    :cond_3
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->q(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 84
    invoke-virtual {p2}, Lcom/baidu/mobstat/af;->l()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 85
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    const-string v0, "sendLog"

    invoke-virtual {p2, v0}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ag;->g(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    if-nez v0, :cond_5

    .line 88
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p1

    const-string p2, "isWifiAvailable = false, will not sendLog"

    invoke-virtual {p1, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_5
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p1

    const-string p2, "can not sendLog due to time stratergy"

    invoke-virtual {p1, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 110
    :cond_6
    :goto_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p1

    const-string p2, "startDataAnynalyzed finished"

    invoke-virtual {p1, p2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 10

    .line 443
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p1

    .line 445
    sget-object v0, Lcom/baidu/mobstat/n;->i:Lcom/baidu/mobstat/n;

    invoke-virtual {p1, v0}, Lcom/baidu/mobstat/af;->a(Lcom/baidu/mobstat/n;)J

    move-result-wide v0

    .line 446
    invoke-virtual {p1}, Lcom/baidu/mobstat/af;->c()J

    move-result-wide v2

    .line 447
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v6, v4, v0

    const-string p1, ";timeInteveral="

    const-string v8, "nowTime="

    cmp-long v9, v6, v2

    if-lez v9, :cond_0

    .line 450
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "need to update, checkWithLastUpdateTime lastUpdateTime ="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 451
    invoke-virtual {v6, p1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    .line 457
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "no need to update, checkWithLastUpdateTime lastUpdateTime ="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 458
    invoke-virtual {v6, p1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 238
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/baidu/mobstat/af;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Landroid/content/Context;)Z
    .locals 1

    .line 466
    invoke-static {p1}, Lcom/baidu/mobstat/af;->a(Landroid/content/Context;)Lcom/baidu/mobstat/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/af;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/ag;->a(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
