.class public abstract Lcom/baidu/mobstat/ci;
.super Lcom/baidu/mobstat/cf;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/ce;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/ci$a;
    }
.end annotation


# static fields
.field static final synthetic c:Z


# instance fields
.field private a:Lcom/baidu/mobstat/cg;

.field protected b:Ljava/net/URI;

.field private d:Ljava/net/Socket;

.field private e:Ljava/io/InputStream;

.field private f:Ljava/io/OutputStream;

.field private g:Ljava/net/Proxy;

.field private h:Ljava/lang/Thread;

.field private i:Lcom/baidu/mobstat/cj;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/concurrent/CountDownLatch;

.field private l:Ljava/util/concurrent/CountDownLatch;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    const-class v0, Lcom/baidu/mobstat/ci;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/baidu/mobstat/ci;->c:Z

    return-void
.end method

.method public constructor <init>(Ljava/net/URI;Lcom/baidu/mobstat/cj;Ljava/util/Map;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Lcom/baidu/mobstat/cj;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 92
    invoke-direct {p0}, Lcom/baidu/mobstat/cf;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-object v0, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    .line 39
    iput-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    .line 41
    iput-object v0, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    .line 47
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    iput-object v0, p0, Lcom/baidu/mobstat/ci;->g:Ljava/net/Proxy;

    .line 55
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/baidu/mobstat/ci;->k:Ljava/util/concurrent/CountDownLatch;

    .line 57
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/baidu/mobstat/ci;->l:Ljava/util/concurrent/CountDownLatch;

    const/4 v0, 0x0

    .line 59
    iput v0, p0, Lcom/baidu/mobstat/ci;->m:I

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 98
    iput-object p1, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    .line 99
    iput-object p2, p0, Lcom/baidu/mobstat/ci;->i:Lcom/baidu/mobstat/cj;

    .line 100
    iput-object p3, p0, Lcom/baidu/mobstat/ci;->j:Ljava/util/Map;

    .line 101
    iput p4, p0, Lcom/baidu/mobstat/ci;->m:I

    .line 102
    new-instance p1, Lcom/baidu/mobstat/cg;

    invoke-direct {p1, p0, p2}, Lcom/baidu/mobstat/cg;-><init>(Lcom/baidu/mobstat/ch;Lcom/baidu/mobstat/cj;)V

    iput-object p1, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    return-void

    .line 96
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "null as draft is permitted for `WebSocketServer` only!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 94
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method static synthetic a(Lcom/baidu/mobstat/ci;)Lcom/baidu/mobstat/cg;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobstat/ci;)Ljava/io/OutputStream;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/baidu/mobstat/ci;->f:Ljava/io/OutputStream;

    return-object p0
.end method

.method private h()I
    .locals 4

    .line 229
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getPort()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 231
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wss"

    .line 232
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x1bb

    return v0

    :cond_0
    const-string v1, "ws"

    .line 234
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x50

    return v0

    .line 237
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown scheme: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    return v0
.end method

.method private i()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getRawPath()Ljava/lang/String;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->getRawQuery()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v0, "/"

    :cond_1
    if-eqz v1, :cond_2

    .line 252
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    :cond_2
    invoke-direct {p0}, Lcom/baidu/mobstat/ci;->h()I

    move-result v1

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x50

    if-eq v1, v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    const-string v1, ""

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 256
    new-instance v2, Lcom/baidu/mobstat/db;

    invoke-direct {v2}, Lcom/baidu/mobstat/db;-><init>()V

    .line 257
    invoke-virtual {v2, v0}, Lcom/baidu/mobstat/db;->a(Ljava/lang/String;)V

    const-string v0, "Host"

    .line 258
    invoke-virtual {v2, v0, v1}, Lcom/baidu/mobstat/db;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->j:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 260
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 261
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/baidu/mobstat/db;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0, v2}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cz;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/net/InetSocketAddress;
    .locals 1

    .line 491
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->a()Ljava/net/InetSocketAddress;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public abstract a(ILjava/lang/String;Z)V
.end method

.method public a(Lcom/baidu/mobstat/ce;ILjava/lang/String;)V
    .locals 0

    .line 334
    invoke-virtual {p0, p2, p3}, Lcom/baidu/mobstat/ci;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/baidu/mobstat/ce;ILjava/lang/String;Z)V
    .locals 0

    .line 306
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    if-eqz p1, :cond_0

    .line 307
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 309
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    if-eqz p1, :cond_1

    .line 310
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 312
    invoke-virtual {p0, p0, p1}, Lcom/baidu/mobstat/ci;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 314
    :cond_1
    :goto_0
    invoke-virtual {p0, p2, p3, p4}, Lcom/baidu/mobstat/ci;->a(ILjava/lang/String;Z)V

    .line 315
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 316
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->l:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V
    .locals 0

    .line 289
    invoke-virtual {p0, p2}, Lcom/baidu/mobstat/ci;->b(Lcom/baidu/mobstat/cw;)V

    return-void
.end method

.method public final a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/dd;)V
    .locals 0

    .line 297
    check-cast p2, Lcom/baidu/mobstat/df;

    invoke-virtual {p0, p2}, Lcom/baidu/mobstat/ci;->a(Lcom/baidu/mobstat/df;)V

    .line 298
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public final a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V
    .locals 0

    .line 324
    invoke-virtual {p0, p2}, Lcom/baidu/mobstat/ci;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Lcom/baidu/mobstat/ce;Ljava/lang/String;)V
    .locals 0

    .line 279
    invoke-virtual {p0, p2}, Lcom/baidu/mobstat/ci;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/baidu/mobstat/ce;Ljava/nio/ByteBuffer;)V
    .locals 0

    .line 284
    invoke-virtual {p0, p2}, Lcom/baidu/mobstat/ci;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method public a(Lcom/baidu/mobstat/cw;)V
    .locals 1

    .line 486
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cw;)V

    return-void
.end method

.method public abstract a(Lcom/baidu/mobstat/df;)V
.end method

.method public abstract a(Ljava/lang/Exception;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public a(Ljava/net/Socket;)V
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    if-nez v0, :cond_0

    .line 426
    iput-object p1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    return-void

    .line 424
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "socket has already been set"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 0

    return-void
.end method

.method public a([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/NotYetConnectedException;
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/cg;->a([B)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    .line 137
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    .line 135
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WebSocketClient objects are not reuseable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(ILjava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/baidu/mobstat/ce;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/baidu/mobstat/ce;ILjava/lang/String;Z)V
    .locals 0

    .line 339
    invoke-virtual {p0, p2, p3, p4}, Lcom/baidu/mobstat/ci;->b(ILjava/lang/String;Z)V

    return-void
.end method

.method public b(Lcom/baidu/mobstat/cw;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/baidu/mobstat/ce;)Ljava/net/InetSocketAddress;
    .locals 0

    .line 370
    iget-object p1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    if-eqz p1, :cond_0

    .line 371
    invoke-virtual {p1}, Ljava/net/Socket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object p1

    check-cast p1, Ljava/net/InetSocketAddress;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public c()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 146
    invoke-virtual {p0}, Lcom/baidu/mobstat/ci;->b()V

    .line 147
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 148
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/cg;->a(I)V

    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->e()Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .line 446
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->f()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->d()Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 5

    const/4 v0, -0x1

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Ljava/net/Socket;

    iget-object v2, p0, Lcom/baidu/mobstat/ci;->g:Ljava/net/Proxy;

    invoke-direct {v1, v2}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    iput-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    goto :goto_0

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isBound()Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/baidu/mobstat/ci;->b:Ljava/net/URI;

    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/baidu/mobstat/ci;->h()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iget v3, p0, Lcom/baidu/mobstat/ci;->m:I

    invoke-virtual {v1, v2, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 198
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/ci;->e:Ljava/io/InputStream;

    .line 199
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/ci;->f:Ljava/io/OutputStream;

    .line 201
    invoke-direct {p0}, Lcom/baidu/mobstat/ci;->i()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 208
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/baidu/mobstat/ci$a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/baidu/mobstat/ci$a;-><init>(Lcom/baidu/mobstat/ci;Lcom/baidu/mobstat/ci$1;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    .line 209
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->h:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 211
    sget v1, Lcom/baidu/mobstat/cg;->b:I

    new-array v1, v1, [B

    .line 215
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/baidu/mobstat/ci;->g()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/baidu/mobstat/ci;->f()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/baidu/mobstat/ci;->e:Ljava/io/InputStream;

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-eq v2, v0, :cond_2

    .line 216
    iget-object v3, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    const/4 v4, 0x0

    invoke-static {v1, v4, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/baidu/mobstat/cg;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 223
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/ci;->a(Ljava/lang/Exception;)V

    .line 224
    iget-object v1, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    const/16 v2, 0x3ee

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;)V

    goto :goto_2

    .line 220
    :catch_1
    iget-object v0, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cg;->b()V

    .line 226
    :goto_2
    sget-boolean v0, Lcom/baidu/mobstat/ci;->c:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/baidu/mobstat/ci;->d:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    :goto_3
    return-void

    .line 194
    :cond_5
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v1

    .line 203
    iget-object v2, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {p0, v2, v1}, Lcom/baidu/mobstat/ci;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 204
    iget-object v2, p0, Lcom/baidu/mobstat/ci;->a:Lcom/baidu/mobstat/cg;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;)V

    return-void
.end method
