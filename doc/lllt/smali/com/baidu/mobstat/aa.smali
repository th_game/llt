.class public abstract enum Lcom/baidu/mobstat/aa;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobstat/aa;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobstat/aa;

.field public static final enum b:Lcom/baidu/mobstat/aa;

.field public static final enum c:Lcom/baidu/mobstat/aa;

.field public static final enum d:Lcom/baidu/mobstat/aa;

.field private static final synthetic f:[Lcom/baidu/mobstat/aa;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 17
    new-instance v0, Lcom/baidu/mobstat/aa$1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SERVICE"

    invoke-direct {v0, v3, v1, v2}, Lcom/baidu/mobstat/aa$1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobstat/aa;->a:Lcom/baidu/mobstat/aa;

    .line 41
    new-instance v0, Lcom/baidu/mobstat/aa$2;

    const/4 v3, 0x2

    const-string v4, "NO_SERVICE"

    invoke-direct {v0, v4, v2, v3}, Lcom/baidu/mobstat/aa$2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobstat/aa;->b:Lcom/baidu/mobstat/aa;

    .line 61
    new-instance v0, Lcom/baidu/mobstat/aa$3;

    const/4 v4, 0x3

    const-string v5, "RECEIVER"

    invoke-direct {v0, v5, v3, v4}, Lcom/baidu/mobstat/aa$3;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobstat/aa;->c:Lcom/baidu/mobstat/aa;

    .line 81
    new-instance v0, Lcom/baidu/mobstat/aa$4;

    const/4 v5, 0x4

    const-string v6, "ERISED"

    invoke-direct {v0, v6, v4, v5}, Lcom/baidu/mobstat/aa$4;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobstat/aa;->d:Lcom/baidu/mobstat/aa;

    new-array v0, v5, [Lcom/baidu/mobstat/aa;

    .line 15
    sget-object v5, Lcom/baidu/mobstat/aa;->a:Lcom/baidu/mobstat/aa;

    aput-object v5, v0, v1

    sget-object v1, Lcom/baidu/mobstat/aa;->b:Lcom/baidu/mobstat/aa;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobstat/aa;->c:Lcom/baidu/mobstat/aa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobstat/aa;->d:Lcom/baidu/mobstat/aa;

    aput-object v1, v0, v4

    sput-object v0, Lcom/baidu/mobstat/aa;->f:[Lcom/baidu/mobstat/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 104
    iput p3, p0, Lcom/baidu/mobstat/aa;->e:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/baidu/mobstat/aa$1;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/aa;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static a(I)Lcom/baidu/mobstat/aa;
    .locals 5

    .line 119
    invoke-static {}, Lcom/baidu/mobstat/aa;->values()[Lcom/baidu/mobstat/aa;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 120
    iget v4, v3, Lcom/baidu/mobstat/aa;->e:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 125
    :cond_1
    sget-object p0, Lcom/baidu/mobstat/aa;->b:Lcom/baidu/mobstat/aa;

    return-object p0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    const-string v0, "activity"

    .line 129
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/ActivityManager;

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    const v1, 0x7fffffff

    .line 133
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object p0

    const/4 v1, 0x0

    :goto_0
    if-eqz p0, :cond_1

    .line 134
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 135
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 136
    iget-object v2, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.baidu.bottom.service.BottomService"

    .line 137
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception p0

    .line 142
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    :cond_1
    return v0
.end method

.method static synthetic c(Landroid/content/Context;)Z
    .locals 0

    .line 15
    invoke-static {p0}, Lcom/baidu/mobstat/aa;->d(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 152
    invoke-static {p0, v0}, Lcom/baidu/mobstat/bv;->e(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobstat/aa;
    .locals 1

    .line 15
    const-class v0, Lcom/baidu/mobstat/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobstat/aa;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobstat/aa;
    .locals 1

    .line 15
    sget-object v0, Lcom/baidu/mobstat/aa;->f:[Lcom/baidu/mobstat/aa;

    invoke-virtual {v0}, [Lcom/baidu/mobstat/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobstat/aa;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 113
    iget v0, p0, Lcom/baidu/mobstat/aa;->e:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
