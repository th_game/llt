.class public Lcom/baidu/mobstat/cu;
.super Lcom/baidu/mobstat/cx;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/ct;


# static fields
.field static final a:Ljava/nio/ByteBuffer;


# instance fields
.field private f:I

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    .line 11
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobstat/cu;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    sget-object v0, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/cx;-><init>(Lcom/baidu/mobstat/cw$a;)V

    const/4 v0, 0x1

    .line 18
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/cu;->a(Z)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 28
    sget-object v0, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/cx;-><init>(Lcom/baidu/mobstat/cw$a;)V

    const/4 v0, 0x1

    .line 29
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/cu;->a(Z)V

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobstat/cu;->a(ILjava/lang/String;)V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    const-string v0, ""

    if-nez p2, :cond_0

    move-object p2, v0

    :cond_0
    const/16 v1, 0x3ed

    const/16 v2, 0x3f7

    if-ne p1, v2, :cond_1

    move-object p2, v0

    const/16 p1, 0x3ed

    :cond_1
    const/16 v0, 0x3ea

    if-ne p1, v1, :cond_3

    .line 43
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-gtz p1, :cond_2

    return-void

    .line 44
    :cond_2
    new-instance p1, Lcom/baidu/mobstat/cn;

    const-string p2, "A close frame must have a closecode if it has a reason"

    invoke-direct {p1, v0, p2}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1

    :cond_3
    const/16 v1, 0x3f3

    if-le p1, v1, :cond_5

    const/16 v1, 0xbb8

    if-ge p1, v1, :cond_5

    if-ne p1, v2, :cond_4

    goto :goto_0

    .line 50
    :cond_4
    new-instance p1, Lcom/baidu/mobstat/cn;

    const-string p2, "Trying to send an illegal close code!"

    invoke-direct {p1, v0, p2}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1

    .line 53
    :cond_5
    :goto_0
    invoke-static {p2}, Lcom/baidu/mobstat/di;->a(Ljava/lang/String;)[B

    move-result-object p2

    const/4 v0, 0x4

    .line 54
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 p1, 0x2

    .line 56
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 57
    array-length v1, p2

    add-int/2addr v1, p1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 58
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 59
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 60
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 61
    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/cu;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method private g()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/co;
        }
    .end annotation

    const/16 v0, 0x3ed

    .line 65
    iput v0, p0, Lcom/baidu/mobstat/cu;->f:I

    .line 66
    invoke-super {p0}, Lcom/baidu/mobstat/cx;->c()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 68
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    const/4 v2, 0x4

    .line 69
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 70
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 71
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    .line 72
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 73
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobstat/cu;->f:I

    .line 75
    iget v2, p0, Lcom/baidu/mobstat/cu;->f:I

    const/16 v3, 0x3ee

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3f7

    if-eq v2, v3, :cond_0

    if-eq v2, v0, :cond_0

    const/16 v0, 0x1387

    if-gt v2, v0, :cond_0

    const/16 v0, 0x3e8

    if-lt v2, v0, :cond_0

    const/16 v0, 0x3ec

    if-eq v2, v0, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    new-instance v0, Lcom/baidu/mobstat/co;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closecode must not be sent over the wire: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/baidu/mobstat/cu;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    return-void
.end method

.method private h()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 88
    iget v0, p0, Lcom/baidu/mobstat/cu;->f:I

    const/16 v1, 0x3ed

    if-ne v0, v1, :cond_0

    .line 89
    invoke-super {p0}, Lcom/baidu/mobstat/cx;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobstat/di;->a(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cu;->g:Ljava/lang/String;

    goto :goto_0

    .line 91
    :cond_0
    invoke-super {p0}, Lcom/baidu/mobstat/cx;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 94
    :try_start_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 95
    invoke-static {v0}, Lcom/baidu/mobstat/di;->a(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobstat/cu;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    goto :goto_1

    :catch_0
    move-exception v2

    .line 97
    :try_start_1
    new-instance v3, Lcom/baidu/mobstat/co;

    invoke-direct {v3, v2}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :goto_1
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    throw v2
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 84
    iget v0, p0, Lcom/baidu/mobstat/cu;->f:I

    return v0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 116
    invoke-super {p0, p1}, Lcom/baidu/mobstat/cx;->a(Ljava/nio/ByteBuffer;)V

    .line 117
    invoke-direct {p0}, Lcom/baidu/mobstat/cu;->g()V

    .line 118
    invoke-direct {p0}, Lcom/baidu/mobstat/cu;->h()V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/baidu/mobstat/cu;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/nio/ByteBuffer;
    .locals 2

    .line 123
    iget v0, p0, Lcom/baidu/mobstat/cu;->f:I

    const/16 v1, 0x3ed

    if-ne v0, v1, :cond_0

    .line 124
    sget-object v0, Lcom/baidu/mobstat/cu;->a:Ljava/nio/ByteBuffer;

    return-object v0

    .line 125
    :cond_0
    invoke-super {p0}, Lcom/baidu/mobstat/cx;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/baidu/mobstat/cx;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/baidu/mobstat/cu;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
