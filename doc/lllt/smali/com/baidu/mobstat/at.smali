.class public Lcom/baidu/mobstat/at;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/at$a;,
        Lcom/baidu/mobstat/at$b;,
        Lcom/baidu/mobstat/at$c;
    }
.end annotation


# static fields
.field private static final A:Lcom/baidu/mobstat/at;


# instance fields
.field private B:Landroid/os/Handler;

.field private C:Lcom/baidu/mobstat/ar$a;

.field private D:Lorg/json/JSONArray;

.field private E:Ljava/lang/Object;

.field private a:Landroid/content/Context;

.field private b:Lcom/baidu/mobstat/ar;

.field private c:Lcom/baidu/mobstat/aq;

.field private d:Landroid/app/Activity;

.field private e:Lcom/baidu/mobstat/au;

.field private f:Landroid/os/Handler;

.field private g:Landroid/os/HandlerThread;

.field private h:Landroid/os/Handler;

.field private i:Landroid/os/HandlerThread;

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Z

.field private volatile o:Z

.field private volatile p:Ljava/lang/String;

.field private volatile q:Ljava/lang/String;

.field private volatile r:Ljava/lang/String;

.field private s:J

.field private t:J

.field private u:J

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Lorg/json/JSONObject;

.field private y:Lorg/json/JSONObject;

.field private z:Lcom/baidu/mobstat/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 101
    new-instance v0, Lcom/baidu/mobstat/at;

    invoke-direct {v0}, Lcom/baidu/mobstat/at;-><init>()V

    sput-object v0, Lcom/baidu/mobstat/at;->A:Lcom/baidu/mobstat/at;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->x:Lorg/json/JSONObject;

    .line 97
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->y:Lorg/json/JSONObject;

    .line 99
    invoke-static {}, Lcom/baidu/mobstat/bm;->a()Lcom/baidu/mobstat/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->z:Lcom/baidu/mobstat/bm;

    .line 119
    new-instance v0, Lcom/baidu/mobstat/at$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobstat/at$1;-><init>(Lcom/baidu/mobstat/at;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    .line 166
    new-instance v0, Lcom/baidu/mobstat/at$2;

    invoke-direct {v0, p0}, Lcom/baidu/mobstat/at$2;-><init>(Lcom/baidu/mobstat/at;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->C:Lcom/baidu/mobstat/ar$a;

    .line 851
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->D:Lorg/json/JSONArray;

    .line 852
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->E:Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/baidu/mobstat/au;

    invoke-direct {v0}, Lcom/baidu/mobstat/au;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->e:Lcom/baidu/mobstat/au;

    .line 110
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "crawlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->i:Landroid/os/HandlerThread;

    .line 111
    iget-object v0, p0, Lcom/baidu/mobstat/at;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 112
    new-instance v0, Lcom/baidu/mobstat/at$c;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobstat/at$c;-><init>(Lcom/baidu/mobstat/at;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    .line 114
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "downloadThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->g:Landroid/os/HandlerThread;

    .line 115
    iget-object v0, p0, Lcom/baidu/mobstat/at;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 116
    new-instance v0, Lcom/baidu/mobstat/at$a;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->g:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobstat/at$a;-><init>(Lcom/baidu/mobstat/at;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    return-void
.end method

.method public static a()Lcom/baidu/mobstat/at;
    .locals 1

    .line 104
    sget-object v0, Lcom/baidu/mobstat/at;->A:Lcom/baidu/mobstat/at;

    return-object v0
.end method

.method private a(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    const-string v0, "UTF-8"

    .line 532
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 533
    new-instance v2, Landroid/util/Pair;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "appKey"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    new-instance v2, Landroid/util/Pair;

    invoke-static {p1}, Lcom/baidu/mobstat/cc;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "appVersion"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 535
    new-instance v2, Landroid/util/Pair;

    invoke-static {p1}, Lcom/baidu/mobstat/cc;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "appName"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 536
    new-instance v2, Landroid/util/Pair;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "packageName"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/baidu/mobstat/StatService;->getSdkVersion()Ljava/lang/String;

    move-result-object v3

    const-string v4, "sdkVersion"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 538
    new-instance v2, Landroid/util/Pair;

    invoke-static {p1}, Lcom/baidu/mobstat/cc;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "deviceName"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    new-instance v2, Landroid/util/Pair;

    const-string v3, "platform"

    const-string v4, "Android"

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540
    new-instance v2, Landroid/util/Pair;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "model"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/baidu/mobstat/CooperService;->getCUID(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    const-string v3, "cuid"

    invoke-direct {v2, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 543
    new-instance p1, Landroid/util/Pair;

    const-string v2, "auto"

    const-string v3, "1"

    invoke-direct {p1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 546
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 548
    :try_start_0
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 549
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 551
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "="

    if-eqz v4, :cond_0

    .line 552
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 554
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    .line 561
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "wss://mtjsocket.baidu.com/app?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3

    .line 640
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "type"

    const-string v2, "upload"

    .line 647
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "data"

    .line 648
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 10

    const-string v0, "data"

    const-string v1, "meta"

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-object v2

    .line 759
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    .line 765
    :cond_1
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    const-string v4, "matchAll"

    .line 766
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 768
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    .line 769
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 770
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_3

    .line 771
    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "page"

    .line 773
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 775
    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 776
    invoke-virtual {v5, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    if-eqz v4, :cond_4

    goto :goto_1

    :cond_4
    if-nez v4, :cond_5

    .line 783
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result p2

    if-eqz p2, :cond_5

    goto :goto_1

    :cond_5
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_6

    .line 788
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :try_start_1
    invoke-virtual {p1, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 790
    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_0
    :cond_6
    move-object p1, v2

    :catch_1
    :goto_2
    return-object p1
.end method

.method private a(Landroid/app/Activity;Z)V
    .locals 2

    .line 598
    instance-of v0, p1, Lcom/baidu/mobstat/IIgnoreAutoTrace;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    if-eqz p2, :cond_1

    .line 603
    invoke-static {}, Lcom/baidu/mobstat/BDStatCore;->instance()Lcom/baidu/mobstat/BDStatCore;

    move-result-object p2

    invoke-virtual {p2, p1, v0}, Lcom/baidu/mobstat/BDStatCore;->onResume(Landroid/app/Activity;Z)V

    goto :goto_0

    .line 605
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/BDStatCore;->instance()Lcom/baidu/mobstat/BDStatCore;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobstat/BDStatCore;->onPause(Landroid/app/Activity;ZLcom/baidu/mobstat/ExtraInfo;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->h()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/at;Ljava/lang/String;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/at;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/at;Z)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/at;->b(Z)V

    return-void
.end method

.method private b(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 0

    if-nez p3, :cond_0

    return-void

    .line 577
    :cond_0
    invoke-virtual {p3, p1, p2}, Lcom/baidu/mobstat/bs;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->o()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .line 483
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 487
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 491
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobstat/BasicStoreTools;->setAutoTraceConfigFetchTime(Landroid/content/Context;J)V

    .line 492
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "mtj_auto.config"

    invoke-static {v0, v2, p1, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 494
    iget-object p1, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 495
    iget-object v0, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private b(Z)V
    .locals 1

    const/4 p1, 0x0

    .line 423
    iput-boolean p1, p0, Lcom/baidu/mobstat/at;->j:Z

    .line 424
    invoke-static {}, Lcom/baidu/mobstat/au;->b()V

    .line 430
    iget-object p1, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 432
    iget-object p1, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 433
    iget-object v0, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private c(Landroid/app/Activity;)V
    .locals 2

    .line 326
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    const-string v1, "installConnectionTracker"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 330
    :cond_0
    new-instance v0, Lcom/baidu/mobstat/ar;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->C:Lcom/baidu/mobstat/ar$a;

    invoke-direct {v0, v1}, Lcom/baidu/mobstat/ar;-><init>(Lcom/baidu/mobstat/ar$a;)V

    iput-object v0, p0, Lcom/baidu/mobstat/at;->b:Lcom/baidu/mobstat/ar;

    .line 331
    iget-object v0, p0, Lcom/baidu/mobstat/at;->b:Lcom/baidu/mobstat/ar;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/ar;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private c(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 8

    if-nez p3, :cond_0

    return-void

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->x:Lorg/json/JSONObject;

    invoke-direct {p0}, Lcom/baidu/mobstat/at;->r()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobstat/at;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 586
    iget-object v3, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    const/4 v7, 0x1

    move-object v2, p3

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v2 .. v7}, Lcom/baidu/mobstat/bs;->a(Landroid/app/Activity;Landroid/webkit/WebView;Ljava/lang/String;Lorg/json/JSONObject;Z)V

    return-void
.end method

.method static synthetic c(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->s()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 9

    const-string v0, "data"

    const-string v1, "meta"

    .line 718
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    .line 723
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    .line 726
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONArray;

    .line 728
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 729
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    const/4 v5, 0x0

    .line 731
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 732
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    const-string v7, "webLayout"

    .line 734
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "url"

    .line 735
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 737
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 738
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 740
    :cond_1
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 744
    :cond_2
    iget-object v2, p0, Lcom/baidu/mobstat/at;->x:Lorg/json/JSONObject;

    invoke-virtual {v2, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 745
    iget-object v2, p0, Lcom/baidu/mobstat/at;->x:Lorg/json/JSONObject;

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 747
    iget-object v2, p0, Lcom/baidu/mobstat/at;->y:Lorg/json/JSONObject;

    invoke-virtual {v2, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 748
    iget-object p1, p0, Lcom/baidu/mobstat/at;->y:Lorg/json/JSONObject;

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic d(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->i()V

    return-void
.end method

.method static synthetic e(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->j()V

    return-void
.end method

.method private f()V
    .locals 2

    .line 335
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    const-string v1, "uninstallConnectionTracker"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->b:Lcom/baidu/mobstat/ar;

    if-eqz v0, :cond_1

    .line 340
    invoke-virtual {v0}, Lcom/baidu/mobstat/ar;->b()V

    const/4 v0, 0x0

    .line 341
    iput-object v0, p0, Lcom/baidu/mobstat/at;->b:Lcom/baidu/mobstat/ar;

    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->k()V

    return-void
.end method

.method static synthetic g(Lcom/baidu/mobstat/at;)Landroid/os/Handler;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    return-object p0
.end method

.method private g()V
    .locals 1

    .line 346
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->j:Z

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/baidu/mobstat/at;->b()V

    goto :goto_0

    .line 349
    :cond_0
    invoke-virtual {p0}, Lcom/baidu/mobstat/at;->c()V

    :goto_0
    return-void
.end method

.method private h()V
    .locals 2

    .line 370
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v1, "mtj_auto.config"

    invoke-static {v0, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->r:Ljava/lang/String;

    .line 371
    iget-object v0, p0, Lcom/baidu/mobstat/at;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/at;->c(Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/baidu/mobstat/at;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobstat/bc;->b(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/baidu/mobstat/at;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobstat/ay;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->l()V

    return-void
.end method

.method private i()V
    .locals 2

    .line 380
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 385
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v0

    const-string v1, "autotrace: gesture success"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bj;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 386
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/at;->a(I)V

    .line 388
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobstat/cc;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 389
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v0

    const-string v1, "autotrace: network invalid, failed to connect to circle server"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bj;->a(Ljava/lang/String;)V

    return-void

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 394
    iget-object v1, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic i(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->m()V

    return-void
.end method

.method private j()V
    .locals 2

    .line 400
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->k:Z

    if-nez v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 402
    iget-object v1, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/baidu/mobstat/at;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->n()V

    return-void
.end method

.method private k()V
    .locals 2

    const/4 v0, 0x1

    .line 408
    iput-boolean v0, p0, Lcom/baidu/mobstat/at;->j:Z

    .line 411
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->j:Z

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 415
    iget-object v0, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 417
    iget-object v1, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method private l()V
    .locals 4

    .line 438
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->k:Z

    if-eqz v0, :cond_0

    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 443
    invoke-static {v0, v1, v2, v3}, Lcom/baidu/mobstat/bn;->a(Landroid/content/Context;Ljava/lang/String;IZ)Z

    move-result v0

    .line 444
    iput-boolean v3, p0, Lcom/baidu/mobstat/at;->k:Z

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v1, "mtj_vizParser.js"

    invoke-static {v0, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->p:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private m()V
    .locals 3

    .line 453
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->l:Z

    if-eqz v0, :cond_0

    return-void

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    const/4 v2, 0x1

    .line 458
    invoke-static {v0, v1, v2, v2}, Lcom/baidu/mobstat/bn;->a(Landroid/content/Context;Ljava/lang/String;IZ)Z

    move-result v0

    .line 459
    iput-boolean v2, p0, Lcom/baidu/mobstat/at;->l:Z

    if-eqz v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v1, "mtj_autoTracker.js"

    invoke-static {v0, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private n()V
    .locals 4

    .line 468
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->m:Z

    if-eqz v0, :cond_0

    return-void

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 473
    invoke-static {v0, v1, v2, v3}, Lcom/baidu/mobstat/bn;->a(Landroid/content/Context;Ljava/lang/String;IZ)Z

    move-result v0

    .line 474
    iput-boolean v3, p0, Lcom/baidu/mobstat/at;->m:Z

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 478
    iget-object v1, p0, Lcom/baidu/mobstat/at;->B:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method

.method private o()V
    .locals 4

    .line 499
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v0

    const-string v1, "autotrace: start to connect"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bj;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 500
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/at;->a(I)V

    .line 503
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v0

    const-string v1, "autotrace: connect established, no need to duplicate connect"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bj;->a(Ljava/lang/String;)V

    return-void

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/at;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 510
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "url:"

    .line 512
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 516
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 520
    :cond_2
    :try_start_0
    new-instance v1, Lcom/baidu/mobstat/aq;

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    new-instance v2, Lcom/baidu/mobstat/at$b;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/baidu/mobstat/at$b;-><init>(Lcom/baidu/mobstat/at;Lcom/baidu/mobstat/at$1;)V

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobstat/aq;-><init>(Ljava/net/URI;Lcom/baidu/mobstat/aq$a;)V

    iput-object v1, p0, Lcom/baidu/mobstat/at;->c:Lcom/baidu/mobstat/aq;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private p()Z
    .locals 1

    .line 527
    iget-object v0, p0, Lcom/baidu/mobstat/at;->c:Lcom/baidu/mobstat/aq;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobstat/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private q()Z
    .locals 1

    .line 566
    iget-object v0, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private r()Ljava/lang/String;
    .locals 1

    .line 591
    iget-object v0, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 592
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private s()V
    .locals 4

    .line 616
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->j:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->e:Lcom/baidu/mobstat/au;

    iget-object v1, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/au;->a(Landroid/app/Activity;)Lorg/json/JSONObject;

    move-result-object v0

    .line 621
    invoke-direct {p0, v0}, Lcom/baidu/mobstat/at;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 623
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 624
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doSendSnapshot:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 628
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobstat/at;->c:Lcom/baidu/mobstat/aq;

    invoke-virtual {v1, v0}, Lcom/baidu/mobstat/aq;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 635
    :catch_0
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 636
    iget-object v1, p0, Lcom/baidu/mobstat/at;->h:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    :goto_0
    return-void
.end method

.method private t()V
    .locals 5

    .line 657
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobstat/cc;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 661
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->m:Z

    if-eqz v0, :cond_1

    return-void

    .line 666
    :cond_1
    iget-wide v0, p0, Lcom/baidu/mobstat/at;->u:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    .line 667
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getAutoTraceConfigFetchTime(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobstat/at;->u:J

    .line 670
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobstat/at;->u:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v4, v0, v2

    if-lez v4, :cond_3

    .line 672
    iget-object v0, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 673
    iget-object v1, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_3
    return-void
.end method

.method private u()V
    .locals 5

    .line 678
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobstat/cc;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 682
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->l:Z

    if-eqz v0, :cond_1

    return-void

    .line 687
    :cond_1
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->n:Z

    if-nez v0, :cond_2

    .line 688
    iget-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v1, "mtj_autoTracker.js"

    invoke-static {v0, v1}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    const/4 v0, 0x1

    .line 689
    iput-boolean v0, p0, Lcom/baidu/mobstat/at;->n:Z

    .line 693
    :cond_2
    iget-wide v0, p0, Lcom/baidu/mobstat/at;->s:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    .line 694
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getAutoTraceTrackJsFetchTime(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobstat/at;->s:J

    .line 695
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/BasicStoreTools;->getAutoTraceTrackJsFetchInterval(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobstat/at;->t:J

    .line 698
    :cond_3
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->n:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 699
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobstat/at;->s:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/baidu/mobstat/at;->t:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_6

    .line 701
    :cond_5
    iget-object v0, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 702
    iget-object v1, p0, Lcom/baidu/mobstat/at;->f:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_6
    return-void
.end method

.method private v()V
    .locals 1

    .line 708
    iget-boolean v0, p0, Lcom/baidu/mobstat/at;->o:Z

    if-nez v0, :cond_1

    .line 709
    iget-object v0, p0, Lcom/baidu/mobstat/at;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->h()V

    :cond_0
    const/4 v0, 0x1

    .line 713
    iput-boolean v0, p0, Lcom/baidu/mobstat/at;->o:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    const-string v0, ""

    .line 800
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobstat/at;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 6

    .line 804
    iget-object v0, p0, Lcom/baidu/mobstat/at;->E:Ljava/lang/Object;

    monitor-enter v0

    .line 805
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 806
    monitor-exit v0

    return-void

    :cond_0
    if-nez p2, :cond_1

    const-string p2, ""

    .line 813
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 814
    iget-object v3, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/baidu/mobstat/cc;->s(Landroid/content/Context;)Z

    move-result v3

    .line 815
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "|"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 816
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "_"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "_"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 817
    iget-object p2, p0, Lcom/baidu/mobstat/at;->D:Lorg/json/JSONArray;

    invoke-virtual {p2, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 819
    iget-object p1, p0, Lcom/baidu/mobstat/at;->D:Lorg/json/JSONArray;

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    .line 820
    iget-object p2, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v1, "trace_circle.data"

    invoke-static {p2, v1, p1, v5}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 821
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    .line 257
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->q()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/baidu/mobstat/at;->c()V

    .line 266
    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    .line 267
    iput-object p1, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    .line 270
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->v()V

    .line 272
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->t()V

    .line 274
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->u()V

    const/4 v0, 0x1

    .line 277
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobstat/at;->a(Landroid/app/Activity;Z)V

    .line 279
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/at;->c(Landroid/app/Activity;)V

    .line 281
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->g()V

    .line 284
    iget-object v1, p0, Lcom/baidu/mobstat/at;->z:Lcom/baidu/mobstat/bm;

    iget-object v2, p0, Lcom/baidu/mobstat/at;->y:Lorg/json/JSONObject;

    iget-boolean v3, p0, Lcom/baidu/mobstat/at;->w:Z

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/baidu/mobstat/bm;->a(Landroid/app/Activity;ZLorg/json/JSONObject;Z)V

    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V
    .locals 1

    .line 310
    iget-object p2, p0, Lcom/baidu/mobstat/at;->p:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 311
    iget-object p2, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v0, "mtj_vizParser.js"

    invoke-static {p2, v0}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobstat/at;->p:Ljava/lang/String;

    .line 314
    :cond_0
    iget-object p2, p0, Lcom/baidu/mobstat/at;->p:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/at;->b(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V

    .line 316
    iget-object p2, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 317
    iget-object p2, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v0, "mtj_autoTracker.js"

    invoke-static {p2, v0}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    .line 320
    :cond_1
    iget-object p2, p0, Lcom/baidu/mobstat/at;->q:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/at;->c(Landroid/webkit/WebView;Ljava/lang/String;Lcom/baidu/mobstat/bs;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/baidu/mobstat/at;->v:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 249
    iput-boolean p1, p0, Lcom/baidu/mobstat/at;->w:Z

    return-void
.end method

.method public b()V
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    .line 358
    :cond_0
    invoke-static {v0}, Lcom/baidu/mobstat/ap;->b(Landroid/app/Activity;)V

    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .line 292
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->q()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 297
    iput-object v0, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    const/4 v0, 0x0

    .line 300
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobstat/at;->a(Landroid/app/Activity;Z)V

    .line 302
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->f()V

    .line 305
    iget-object v0, p0, Lcom/baidu/mobstat/at;->z:Lcom/baidu/mobstat/bm;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/baidu/mobstat/bm;->a(Landroid/app/Activity;Z)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/baidu/mobstat/at;->d:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    .line 366
    :cond_0
    invoke-static {v0}, Lcom/baidu/mobstat/ap;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 610
    invoke-direct {p0}, Lcom/baidu/mobstat/at;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/baidu/mobstat/at;->c:Lcom/baidu/mobstat/aq;

    invoke-virtual {v0}, Lcom/baidu/mobstat/aq;->a()V

    :cond_0
    return-void
.end method

.method public e()Lorg/json/JSONArray;
    .locals 6

    .line 825
    iget-object v0, p0, Lcom/baidu/mobstat/at;->E:Ljava/lang/Object;

    monitor-enter v0

    .line 826
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 827
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    monitor-exit v0

    return-object v1

    .line 830
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v2, "trace_circle.data"

    invoke-static {v1, v2}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    .line 833
    :try_start_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 834
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v3

    goto :goto_0

    :catch_0
    nop

    :cond_1
    :goto_0
    if-nez v2, :cond_2

    .line 841
    :try_start_2
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 844
    :cond_2
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobstat/at;->D:Lorg/json/JSONArray;

    .line 845
    iget-object v1, p0, Lcom/baidu/mobstat/at;->a:Landroid/content/Context;

    const-string v3, "trace_circle.data"

    iget-object v4, p0, Lcom/baidu/mobstat/at;->D:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v3, v4, v5}, Lcom/baidu/mobstat/bv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 847
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception v1

    .line 848
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
