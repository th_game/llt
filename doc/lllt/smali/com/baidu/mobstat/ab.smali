.class public Lcom/baidu/mobstat/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/baidu/mobstat/h;


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;)Lcom/baidu/mobstat/h;
    .locals 4

    const-class v0, Lcom/baidu/mobstat/ab;

    monitor-enter v0

    .line 26
    :try_start_0
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    const-string v2, "getBPStretegyController begin"

    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 28
    sget-object v1, Lcom/baidu/mobstat/ab;->a:Lcom/baidu/mobstat/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    const-string v2, "com.baidu.bottom.remote.BPStretegyController2"

    .line 34
    invoke-static {p0, v2}, Lcom/baidu/mobstat/ae;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 36
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    .line 37
    new-instance v3, Lcom/baidu/mobstat/ad;

    invoke-direct {v3, v2}, Lcom/baidu/mobstat/ad;-><init>(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    :try_start_2
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    const-string v2, "Get BPStretegyController load remote class v2"

    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v3

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v2, v1

    move-object v1, v3

    goto :goto_0

    :catch_1
    move-exception v2

    .line 41
    :goto_0
    :try_start_3
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    if-nez v1, :cond_1

    .line 48
    new-instance v1, Lcom/baidu/mobstat/ac;

    invoke-direct {v1}, Lcom/baidu/mobstat/ac;-><init>()V

    .line 49
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v2

    const-string v3, "Get BPStretegyController load local class"

    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    .line 52
    :cond_1
    sput-object v1, Lcom/baidu/mobstat/ab;->a:Lcom/baidu/mobstat/h;

    .line 55
    invoke-static {p0, v1}, Lcom/baidu/mobstat/ae;->a(Landroid/content/Context;Lcom/baidu/mobstat/h;)V

    .line 58
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p0

    const-string v2, "getBPStretegyController end"

    invoke-virtual {p0, v2}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a()V
    .locals 2

    const-class v0, Lcom/baidu/mobstat/ab;

    monitor-enter v0

    const/4 v1, 0x0

    .line 63
    :try_start_0
    sput-object v1, Lcom/baidu/mobstat/ab;->a:Lcom/baidu/mobstat/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
