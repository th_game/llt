.class public final enum Lcom/baidu/mobstat/cw$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/cw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobstat/cw$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobstat/cw$a;

.field public static final enum b:Lcom/baidu/mobstat/cw$a;

.field public static final enum c:Lcom/baidu/mobstat/cw$a;

.field public static final enum d:Lcom/baidu/mobstat/cw$a;

.field public static final enum e:Lcom/baidu/mobstat/cw$a;

.field public static final enum f:Lcom/baidu/mobstat/cw$a;

.field private static final synthetic g:[Lcom/baidu/mobstat/cw$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 12
    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v1, 0x0

    const-string v2, "CONTINUOUS"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v2, 0x1

    const-string v3, "TEXT"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v3, 0x2

    const-string v4, "BINARY"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;

    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v4, 0x3

    const-string v5, "PING"

    invoke-direct {v0, v5, v4}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v5, 0x4

    const-string v6, "PONG"

    invoke-direct {v0, v6, v5}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    new-instance v0, Lcom/baidu/mobstat/cw$a;

    const/4 v6, 0x5

    const-string v7, "CLOSING"

    invoke-direct {v0, v7, v6}, Lcom/baidu/mobstat/cw$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/baidu/mobstat/cw$a;

    .line 11
    sget-object v7, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    aput-object v7, v0, v1

    sget-object v1, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/baidu/mobstat/cw$a;->g:[Lcom/baidu/mobstat/cw$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobstat/cw$a;
    .locals 1

    .line 11
    const-class v0, Lcom/baidu/mobstat/cw$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobstat/cw$a;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobstat/cw$a;
    .locals 1

    .line 11
    sget-object v0, Lcom/baidu/mobstat/cw$a;->g:[Lcom/baidu/mobstat/cw$a;

    invoke-virtual {v0}, [Lcom/baidu/mobstat/cw$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobstat/cw$a;

    return-object v0
.end method
