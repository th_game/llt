.class Lcom/baidu/mobstat/br$a$a;
.super Landroid/view/View$AccessibilityDelegate;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/br$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobstat/br$a;

.field private b:Landroid/view/View$AccessibilityDelegate;

.field private c:Landroid/view/View;

.field private volatile d:Z


# direct methods
.method public constructor <init>(Lcom/baidu/mobstat/br$a;Ljava/lang/ref/WeakReference;Landroid/view/View;Ljava/lang/String;Landroid/view/View$AccessibilityDelegate;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Landroid/view/View$AccessibilityDelegate;",
            "Z)V"
        }
    .end annotation

    .line 51
    iput-object p1, p0, Lcom/baidu/mobstat/br$a$a;->a:Lcom/baidu/mobstat/br$a;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    .line 52
    iput-object p5, p0, Lcom/baidu/mobstat/br$a$a;->b:Landroid/view/View$AccessibilityDelegate;

    .line 53
    invoke-static {p1, p2}, Lcom/baidu/mobstat/br$a;->a(Lcom/baidu/mobstat/br$a;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    .line 54
    iput-object p3, p0, Lcom/baidu/mobstat/br$a$a;->c:Landroid/view/View;

    .line 55
    iput-boolean p6, p0, Lcom/baidu/mobstat/br$a$a;->d:Z

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/baidu/mobstat/br$a$a;->b:Landroid/view/View$AccessibilityDelegate;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .line 47
    iput-boolean p1, p0, Lcom/baidu/mobstat/br$a$a;->d:Z

    return-void
.end method

.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 4

    .line 64
    iget-object v0, p0, Lcom/baidu/mobstat/br$a$a;->c:Landroid/view/View;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 65
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    const-string v1, "watch view  OnEvent:"

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobstat/br$a$a;->d:Z

    if-eqz v0, :cond_0

    .line 66
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 67
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v2}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 69
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/br$a$a;->a:Lcom/baidu/mobstat/br$a;

    invoke-static {v0}, Lcom/baidu/mobstat/br$a;->a(Lcom/baidu/mobstat/br$a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/baidu/mobstat/br$a$a;->a:Lcom/baidu/mobstat/br$a;

    invoke-static {v0}, Lcom/baidu/mobstat/br$a;->a(Lcom/baidu/mobstat/br$a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 78
    iget-object v1, p0, Lcom/baidu/mobstat/br$a$a;->a:Lcom/baidu/mobstat/br$a;

    invoke-static {v1}, Lcom/baidu/mobstat/br$a;->b(Lcom/baidu/mobstat/br$a;)Lcom/baidu/mobstat/br$b;

    move-result-object v1

    iget-boolean v2, p0, Lcom/baidu/mobstat/br$a$a;->d:Z

    invoke-interface {v1, p1, v2, v0}, Lcom/baidu/mobstat/br$b;->a(Landroid/view/View;ZLandroid/app/Activity;)V

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/br$a$a;->b:Landroid/view/View$AccessibilityDelegate;

    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {v0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    :cond_3
    return-void
.end method
