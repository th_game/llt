.class public Lcom/baidu/mobstat/bc$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/ActivityLifeObserver$IActivityLifeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobstat/bc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    .line 160
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 164
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 168
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    const-string v1, "onActivityPaused"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 172
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bg;->a()Lcom/baidu/mobstat/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/bg;->b(Landroid/app/Activity;)V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    .line 138
    invoke-static {}, Lcom/baidu/mobstat/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 142
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/be;->a()Lcom/baidu/mobstat/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/be;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 146
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    const-string v1, "onActivityResumed"

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 150
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bg;->a()Lcom/baidu/mobstat/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/bg;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
