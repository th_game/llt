.class public Lcom/baidu/mobstat/au;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/au$c;,
        Lcom/baidu/mobstat/au$a;,
        Lcom/baidu/mobstat/au$b;
    }
.end annotation


# static fields
.field private static volatile c:Ljava/lang/String;

.field private static volatile d:I


# instance fields
.field private final a:Lcom/baidu/mobstat/au$b;

.field private final b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobstat/au;->b:Landroid/os/Handler;

    .line 49
    new-instance v0, Lcom/baidu/mobstat/au$b;

    invoke-direct {v0}, Lcom/baidu/mobstat/au$b;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/au;->a:Lcom/baidu/mobstat/au$b;

    return-void
.end method

.method public static a()V
    .locals 1

    const/4 v0, 0x0

    .line 53
    sput v0, Lcom/baidu/mobstat/au;->d:I

    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/view/View;Lorg/json/JSONArray;Ljava/lang/String;Landroid/view/View;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object v6, p1

    move-object v0, p2

    move-object/from16 v1, p4

    if-nez v0, :cond_0

    return-void

    .line 133
    :cond_0
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->e(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    if-nez v2, :cond_1

    return-void

    .line 139
    :cond_1
    invoke-static {p2}, Lcom/baidu/mobstat/ap;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-void

    .line 145
    :cond_2
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->l(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    return-void

    .line 152
    :cond_3
    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->c(Landroid/app/Activity;Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_4

    return-void

    .line 157
    :cond_4
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 158
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 159
    invoke-static {p2, v1}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 160
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    move-object/from16 v7, p5

    .line 161
    invoke-static {p2, v7}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_5
    move-object/from16 v7, p5

    .line 166
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    return-void

    :cond_6
    const-wide/16 v8, -0x1

    .line 172
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    :goto_1
    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-gez v5, :cond_7

    return-void

    .line 180
    :cond_7
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 182
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 183
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    const-string v10, "p"

    .line 184
    invoke-virtual {v9, v10, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "i"

    .line 186
    invoke-virtual {v9, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 188
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "t"

    .line 189
    invoke-virtual {v9, v3, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v3, "path"

    .line 191
    invoke-virtual {v5, v3, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "type"

    .line 193
    invoke-virtual {v5, v3, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "value"

    invoke-virtual {v5, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 196
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 198
    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    invoke-static {p1, v4}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v4

    const-string v8, "x"

    invoke-virtual {v3, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 199
    iget v4, v2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-static {p1, v4}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v4

    const-string v8, "y"

    invoke-virtual {v3, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 200
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    invoke-static {p1, v4}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v4

    const-string v8, "w"

    invoke-virtual {v3, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 201
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    invoke-static {p1, v4}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v4

    const-string v8, "h"

    invoke-virtual {v3, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "frame"

    .line 202
    invoke-virtual {v5, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 204
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->i(Landroid/view/View;)I

    move-result v3

    const-string v4, "alpha"

    invoke-virtual {v5, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 205
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "page"

    invoke-virtual {v5, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 206
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->j(Landroid/view/View;)F

    move-result v3

    float-to-double v3, v3

    const-string v8, "z"

    invoke-virtual {v5, v8, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 208
    instance-of v3, v0, Landroid/webkit/WebView;

    const-string v4, "child"

    if-eqz v3, :cond_b

    const/4 v8, 0x0

    .line 212
    move-object v9, v0

    check-cast v9, Landroid/webkit/WebView;

    .line 213
    invoke-static {p1, v9, v2}, Lcom/baidu/mobstat/bs;->a(Landroid/app/Activity;Landroid/webkit/WebView;Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v2

    .line 214
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    const-string v11, "url"

    if-nez v9, :cond_8

    .line 215
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "objects"

    .line 218
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    goto :goto_2

    :cond_8
    const-string v2, ""

    :goto_2
    if-nez v8, :cond_9

    .line 223
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 225
    :cond_9
    invoke-virtual {v5, v4, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 228
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    const-string v2, "/"

    .line 230
    invoke-virtual {v5, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_3

    .line 232
    :cond_a
    invoke-virtual {v5, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 236
    :cond_b
    :goto_3
    invoke-static {p2, v1}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;Ljava/lang/String;)Z

    move-result v1

    const-string v2, "edit"

    .line 237
    invoke-virtual {v5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-object v1, p3

    .line 239
    invoke-virtual {p3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    if-eqz v3, :cond_c

    return-void

    .line 246
    :cond_c
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_d

    .line 247
    move-object v8, v0

    check-cast v8, Landroid/view/ViewGroup;

    .line 249
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 250
    invoke-virtual {v5, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const/4 v0, 0x0

    const/4 v11, 0x0

    .line 252
    :goto_4
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v11, v0, :cond_e

    .line 253
    invoke-virtual {v8, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, v9

    move-object v4, v10

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobstat/au;->a(Landroid/app/Activity;Landroid/view/View;Lorg/json/JSONArray;Ljava/lang/String;Landroid/view/View;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 256
    :cond_d
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v5, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_e
    return-void
.end method

.method public static b()V
    .locals 1

    const-string v0, ""

    .line 57
    sput-object v0, Lcom/baidu/mobstat/au;->c:Ljava/lang/String;

    return-void
.end method

.method private c(Landroid/app/Activity;)Lorg/json/JSONArray;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 120
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 121
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v5

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, v5

    move-object v3, v6

    .line 122
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobstat/au;->a(Landroid/app/Activity;Landroid/view/View;Lorg/json/JSONArray;Ljava/lang/String;Landroid/view/View;)V

    return-object v6
.end method


# virtual methods
.method public a(Landroid/app/Activity;)Lorg/json/JSONObject;
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 69
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/baidu/mobstat/ar;->a()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    .line 73
    :cond_1
    sget v1, Lcom/baidu/mobstat/au;->d:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/baidu/mobstat/au;->d:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    const/4 v1, 0x0

    .line 74
    invoke-static {v1}, Lcom/baidu/mobstat/ar;->a(Z)V

    .line 78
    :cond_2
    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/au;->b(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 85
    :cond_3
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/au;->c(Landroid/app/Activity;)Lorg/json/JSONArray;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v3}, Lcom/baidu/mobstat/bz$a;->a([B)Ljava/lang/String;

    move-result-object v3

    .line 87
    sget-object v4, Lcom/baidu/mobstat/au;->c:Ljava/lang/String;

    if-eqz v4, :cond_4

    sget-object v4, Lcom/baidu/mobstat/au;->c:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    return-object v0

    .line 91
    :cond_4
    sput-object v3, Lcom/baidu/mobstat/au;->c:Ljava/lang/String;

    .line 93
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :try_start_1
    invoke-static {v1}, Lcom/baidu/mobstat/bq;->a(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "screenshot"

    .line 96
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    invoke-static {v1}, Lcom/baidu/mobstat/bq;->b(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "hash"

    .line 99
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "w"

    .line 106
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "h"

    .line 107
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "screen"

    .line 108
    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "page"

    .line 110
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "objects"

    .line 111
    invoke-virtual {v3, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_0
    move-object v3, v0

    :catchall_1
    :goto_0
    return-object v3
.end method

.method public b(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 4

    .line 261
    iget-object v0, p0, Lcom/baidu/mobstat/au;->a:Lcom/baidu/mobstat/au$b;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/au$b;->a(Landroid/app/Activity;)V

    .line 262
    new-instance p1, Ljava/util/concurrent/FutureTask;

    iget-object v0, p0, Lcom/baidu/mobstat/au;->a:Lcom/baidu/mobstat/au$b;

    invoke-direct {p1, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 263
    iget-object v0, p0, Lcom/baidu/mobstat/au;->b:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 265
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-wide/16 v1, 0x2

    .line 268
    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v2, v3}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 277
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v1

    const-string v2, "autotrace: Exception thrown during screenshot attempt"

    invoke-virtual {v1, v2, p1}, Lcom/baidu/mobstat/bj;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 273
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v1

    const-string v2, "autotrace: Screenshot took more than 2 second to be scheduled and executed. No screenshot will be sent."

    .line 274
    invoke-virtual {v1, v2, p1}, Lcom/baidu/mobstat/bj;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception p1

    .line 271
    invoke-static {}, Lcom/baidu/mobstat/bj;->c()Lcom/baidu/mobstat/bj;

    move-result-object v1

    const-string v2, "autotrace: Screenshot interrupted, no screenshot will be sent."

    invoke-virtual {v1, v2, p1}, Lcom/baidu/mobstat/bj;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    move-object p1, v0

    .line 280
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 285
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobstat/au$c;

    .line 286
    iget-object p1, p1, Lcom/baidu/mobstat/au$c;->c:Lcom/baidu/mobstat/au$a;

    invoke-static {p1}, Lcom/baidu/mobstat/au$a;->a(Lcom/baidu/mobstat/au$a;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
