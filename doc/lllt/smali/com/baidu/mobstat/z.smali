.class public Lcom/baidu/mobstat/z;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/baidu/mobstat/z;


# instance fields
.field private c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    const-string v0, "http://openrcv.baidu.com/1010/bplus.gif"

    goto :goto_0

    :cond_0
    const-string v0, "https://openrcv.baidu.com/1010/bplus.gif"

    :goto_0
    sput-object v0, Lcom/baidu/mobstat/z;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "LogSender"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 43
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/baidu/mobstat/z;->c:Landroid/os/Handler;

    return-void
.end method

.method public static a()Lcom/baidu/mobstat/z;
    .locals 2

    .line 47
    sget-object v0, Lcom/baidu/mobstat/z;->b:Lcom/baidu/mobstat/z;

    if-nez v0, :cond_1

    .line 48
    const-class v0, Lcom/baidu/mobstat/z;

    monitor-enter v0

    .line 49
    :try_start_0
    sget-object v1, Lcom/baidu/mobstat/z;->b:Lcom/baidu/mobstat/z;

    if-nez v1, :cond_0

    .line 50
    new-instance v1, Lcom/baidu/mobstat/z;

    invoke-direct {v1}, Lcom/baidu/mobstat/z;-><init>()V

    sput-object v1, Lcom/baidu/mobstat/z;->b:Lcom/baidu/mobstat/z;

    .line 52
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 54
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobstat/z;->b:Lcom/baidu/mobstat/z;

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p3

    const-string v1, "https:"

    move-object/from16 v2, p2

    .line 194
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v3, 0x1

    xor-int/2addr v1, v3

    .line 198
    invoke-static/range {p1 .. p2}, Lcom/baidu/mobstat/bv;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v2

    .line 201
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v4, 0x0

    .line 202
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 203
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    const-string v5, "Content-Encoding"

    const-string v6, "gzip"

    .line 204
    invoke-virtual {v2, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 209
    :try_start_0
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 210
    new-instance v6, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v6, v5}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v7, 0x4

    new-array v8, v7, [B

    const/16 v9, 0x48

    aput-byte v9, v8, v4

    const/16 v10, 0x4d

    aput-byte v10, v8, v3

    const/16 v11, 0x30

    const/4 v12, 0x2

    aput-byte v11, v8, v12

    const/16 v13, 0x31

    const/4 v14, 0x3

    aput-byte v13, v8, v14

    .line 212
    invoke-virtual {v6, v8}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    new-array v8, v7, [B

    aput-byte v4, v8, v4

    aput-byte v4, v8, v3

    aput-byte v4, v8, v12

    aput-byte v3, v8, v14

    .line 213
    invoke-virtual {v6, v8}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    new-array v8, v7, [B

    aput-byte v4, v8, v4

    aput-byte v4, v8, v3

    aput-byte v14, v8, v12

    const/16 v15, -0xe

    aput-byte v15, v8, v14

    .line 214
    invoke-virtual {v6, v8}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    const/16 v8, 0x8

    new-array v15, v8, [B

    aput-byte v4, v15, v4

    aput-byte v4, v15, v3

    aput-byte v4, v15, v12

    aput-byte v4, v15, v14

    aput-byte v4, v15, v7

    const/16 v16, 0x5

    aput-byte v4, v15, v16

    const/16 v17, 0x6

    aput-byte v4, v15, v17

    const/16 v18, 0x7

    aput-byte v4, v15, v18

    .line 215
    invoke-virtual {v6, v15}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    new-array v15, v12, [B

    aput-byte v4, v15, v4

    aput-byte v12, v15, v3

    .line 216
    invoke-virtual {v6, v15}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    if-eqz v1, :cond_0

    new-array v15, v12, [B

    aput-byte v4, v15, v4

    aput-byte v3, v15, v3

    .line 219
    invoke-virtual {v6, v15}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    goto :goto_0

    :cond_0
    new-array v15, v12, [B

    aput-byte v4, v15, v4

    aput-byte v4, v15, v3

    .line 221
    invoke-virtual {v6, v15}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    :goto_0
    new-array v15, v7, [B

    aput-byte v9, v15, v4

    aput-byte v10, v15, v3

    aput-byte v11, v15, v12

    aput-byte v13, v15, v14

    .line 224
    invoke-virtual {v6, v15}, Ljava/util/zip/GZIPOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v9, "utf-8"

    if-eqz v1, :cond_1

    .line 228
    :try_start_1
    invoke-static {}, Lcom/baidu/mobstat/bt$a;->a()[B

    move-result-object v1

    .line 230
    invoke-static {}, Lcom/baidu/mobstat/bx;->a()[B

    move-result-object v10

    invoke-static {v4, v10, v1}, Lcom/baidu/mobstat/cb;->a(Z[B[B)[B

    move-result-object v10

    .line 232
    array-length v11, v10

    move-object/from16 p2, v9

    int-to-long v8, v11

    invoke-static {v8, v9, v7}, Lcom/baidu/mobstat/z;->a(JI)[B

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 233
    invoke-virtual {v6, v10}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    const/16 v8, 0x10

    new-array v8, v8, [B

    aput-byte v3, v8, v4

    aput-byte v3, v8, v3

    aput-byte v3, v8, v12

    aput-byte v3, v8, v14

    aput-byte v3, v8, v7

    aput-byte v3, v8, v16

    aput-byte v3, v8, v17

    aput-byte v3, v8, v18

    const/16 v4, 0x8

    aput-byte v3, v8, v4

    const/16 v4, 0x9

    aput-byte v3, v8, v4

    const/16 v4, 0xa

    aput-byte v3, v8, v4

    const/16 v4, 0xb

    aput-byte v3, v8, v4

    const/16 v4, 0xc

    aput-byte v3, v8, v4

    const/16 v4, 0xd

    aput-byte v3, v8, v4

    const/16 v4, 0xe

    aput-byte v3, v8, v4

    const/16 v4, 0xf

    aput-byte v3, v8, v4

    move-object/from16 v3, p2

    .line 238
    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v1, v8, v0}, Lcom/baidu/mobstat/bt$a;->a([B[B[B)[B

    move-result-object v0

    .line 239
    array-length v1, v0

    int-to-long v3, v1

    invoke-static {v3, v4, v12}, Lcom/baidu/mobstat/z;->a(JI)[B

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    goto :goto_1

    :cond_1
    move-object v3, v9

    .line 241
    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 244
    :goto_1
    invoke-virtual {v6, v0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 246
    invoke-virtual {v6}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 247
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 249
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 250
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    .line 252
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "; len: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/baidu/mobstat/bi;->c(Ljava/lang/String;)V

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_3

    if-nez v1, :cond_3

    .line 257
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    :goto_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 268
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-object v0

    .line 265
    :cond_2
    :try_start_2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 255
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Response code = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 272
    :try_start_3
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobstat/bi;->b(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 274
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    const-string v0, ""

    return-object v0

    :goto_3
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_5

    :goto_4
    throw v0

    :goto_5
    goto :goto_4
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .line 89
    invoke-static {}, Lcom/baidu/mobstat/bv;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 90
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 95
    new-instance v1, Ljava/io/File;

    const-string v2, "backups/system"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 100
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 101
    array-length v1, v0

    if-nez v1, :cond_2

    goto :goto_3

    .line 106
    :cond_2
    :try_start_0
    new-instance v1, Lcom/baidu/mobstat/z$2;

    invoke-direct {v1, p0}, Lcom/baidu/mobstat/z$2;-><init>(Lcom/baidu/mobstat/z;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 114
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/baidu/mobstat/bi;->b(Ljava/lang/Throwable;)V

    .line 118
    :goto_0
    array-length v1, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    if-ge v4, v1, :cond_8

    aget-object v6, v0, v4

    .line 119
    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v7

    if-nez v7, :cond_3

    goto :goto_2

    .line 123
    :cond_3
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 124
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_2

    :cond_4
    const-string v7, "__send_log_data_"

    .line 128
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    goto :goto_2

    .line 132
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 133
    invoke-static {v6}, Lcom/baidu/mobstat/bv;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 135
    invoke-direct {p0, p1, v7}, Lcom/baidu/mobstat/z;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 137
    invoke-static {v6}, Lcom/baidu/mobstat/bv;->c(Ljava/lang/String;)Z

    const/4 v5, 0x0

    goto :goto_2

    .line 140
    :cond_6
    invoke-direct {p0, v7, v6}, Lcom/baidu/mobstat/z;->a(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x5

    if-lt v5, v6, :cond_7

    goto :goto_3

    :cond_7
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_8
    :goto_3
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/z;Landroid/content/Context;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/z;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/z;Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/z;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "backups/system"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__send_log_data_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 85
    invoke-static {v0, p1, v1}, Lcom/baidu/mobstat/bv;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 155
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 161
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    nop

    .line 165
    :goto_0
    invoke-static {v0}, Lcom/baidu/mobstat/o;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 167
    invoke-static {p1}, Lcom/baidu/mobstat/o;->b(Lorg/json/JSONObject;)V

    .line 168
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p2, p1, v0}, Lcom/baidu/mobstat/bv;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    :goto_1
    return-void
.end method

.method private static a(JI)[B
    .locals 5

    .line 281
    new-array v0, p2, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    sub-int v2, p2, v1

    add-int/lit8 v2, v2, -0x1

    const-wide/16 v3, 0xff

    and-long/2addr v3, p0

    long-to-int v4, v3

    int-to-byte v3, v4

    .line 284
    aput-byte v3, v0, v2

    const/16 v2, 0x8

    shr-long/2addr p0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 173
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 178
    :cond_0
    :try_start_0
    sget-object v1, Lcom/baidu/mobstat/z;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v1, p2}, Lcom/baidu/mobstat/z;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 181
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobstat/bi;->c(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 58
    invoke-static {}, Lcom/baidu/mobstat/bi;->c()Lcom/baidu/mobstat/bi;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bi;->a(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const-string v0, ""

    .line 59
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/z;->c:Landroid/os/Handler;

    new-instance v1, Lcom/baidu/mobstat/z$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/baidu/mobstat/z$1;-><init>(Lcom/baidu/mobstat/z;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method
