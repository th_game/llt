.class public Lcom/baidu/mobstat/ba;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/ba$a;
    }
.end annotation


# static fields
.field private static final u:Lcom/baidu/mobstat/ba;


# instance fields
.field private A:Ljava/lang/Object;

.field private a:Landroid/content/Context;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Handler;

.field private d:Z

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

.field private g:J

.field private h:J

.field private i:J

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private v:Lcom/baidu/mobstat/ba$a;

.field private w:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private x:Ljava/lang/Runnable;

.field private y:F

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 80
    new-instance v0, Lcom/baidu/mobstat/ba;

    invoke-direct {v0}, Lcom/baidu/mobstat/ba;-><init>()V

    sput-object v0, Lcom/baidu/mobstat/ba;->u:Lcom/baidu/mobstat/ba;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 42
    iput-boolean v0, p0, Lcom/baidu/mobstat/ba;->d:Z

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->e:Ljava/util/ArrayList;

    .line 48
    sget-object v0, Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;->TRACK_ALL:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->r:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    .line 404
    new-instance v0, Lcom/baidu/mobstat/ba$11;

    invoke-direct {v0, p0}, Lcom/baidu/mobstat/ba$11;-><init>(Lcom/baidu/mobstat/ba;)V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->w:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    const/4 v0, 0x0

    .line 415
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->x:Ljava/lang/Runnable;

    const/4 v0, 0x0

    .line 1530
    iput v0, p0, Lcom/baidu/mobstat/ba;->y:F

    .line 1531
    iput v0, p0, Lcom/baidu/mobstat/ba;->z:F

    .line 1532
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->A:Ljava/lang/Object;

    .line 87
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "feedViewCrawlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 89
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/ba;F)F
    .locals 0

    .line 35
    iput p1, p0, Lcom/baidu/mobstat/ba;->y:F

    return p1
.end method

.method public static a()Lcom/baidu/mobstat/ba;
    .locals 1

    .line 83
    sget-object v0, Lcom/baidu/mobstat/ba;->u:Lcom/baidu/mobstat/ba;

    return-object v0
.end method

.method private a(Lcom/baidu/mobstat/ax;)Ljava/lang/String;
    .locals 7

    .line 1312
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->a()Ljava/lang/String;

    move-result-object v0

    .line 1313
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->b()Ljava/lang/String;

    move-result-object v1

    .line 1314
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->c()Lorg/json/JSONArray;

    move-result-object v2

    .line 1315
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->d()Ljava/lang/String;

    move-result-object v3

    .line 1316
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->e()Ljava/lang/String;

    move-result-object v4

    .line 1317
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->f()Ljava/lang/String;

    move-result-object v5

    .line 1318
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->g()Z

    move-result v6

    .line 1321
    invoke-static/range {v0 .. v6}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/baidu/mobstat/ba;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/baidu/mobstat/ba;->b:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method private a(Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .line 662
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 663
    new-instance p1, Lcom/baidu/mobstat/ba$13;

    invoke-direct {p1, p0}, Lcom/baidu/mobstat/ba$13;-><init>(Lcom/baidu/mobstat/ba;)V

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 670
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 671
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 672
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method private a(Ljava/util/HashMap;Landroid/view/View;)Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;"
        }
    .end annotation

    .line 814
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 815
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 816
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 818
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    goto :goto_0

    .line 823
    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    if-ne v2, p2, :cond_0

    .line 825
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    .line 827
    new-instance p2, Ljava/util/LinkedHashMap;

    const/4 v0, 0x1

    invoke-direct {p2, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 828
    invoke-virtual {p2, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    return-object p2
.end method

.method private a(Landroid/app/Activity;J)V
    .locals 10

    .line 167
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    .line 168
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/baidu/mobstat/ba;->b:Ljava/lang/ref/WeakReference;

    .line 170
    iput-wide p2, p0, Lcom/baidu/mobstat/ba;->g:J

    .line 173
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    .line 175
    iget-object p2, p0, Lcom/baidu/mobstat/ba;->j:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/baidu/mobstat/bq;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    .line 176
    iput-boolean p2, p0, Lcom/baidu/mobstat/ba;->p:Z

    .line 179
    iget-object v3, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/baidu/mobstat/ba;->j:Ljava/lang/String;

    iget-wide v5, p0, Lcom/baidu/mobstat/ba;->i:J

    iget-wide v7, p0, Lcom/baidu/mobstat/ba;->g:J

    move-object v1, p0

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/app/Activity;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 182
    iput-boolean p1, p0, Lcom/baidu/mobstat/ba;->p:Z

    :cond_0
    return-void
.end method

.method private a(Landroid/app/Activity;JJLjava/util/List;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "JJ",
            "Ljava/util/List<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    if-nez v2, :cond_0

    return-void

    .line 683
    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 685
    invoke-static/range {p1 .. p1}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v3

    .line 686
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v3, 0x0

    .line 690
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    if-nez v4, :cond_2

    goto :goto_0

    .line 695
    :cond_2
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    if-nez v4, :cond_3

    goto :goto_0

    :cond_3
    move-object v3, v4

    .line 704
    :cond_4
    iget-object v2, v0, Lcom/baidu/mobstat/ba;->l:Ljava/lang/String;

    .line 705
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    return-void

    .line 709
    :cond_5
    iget-object v2, v0, Lcom/baidu/mobstat/ba;->m:Ljava/lang/String;

    .line 710
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    return-void

    .line 714
    :cond_6
    invoke-static/range {p1 .. p1}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    .line 715
    invoke-static/range {p1 .. p1}, Lcom/baidu/mobstat/bq;->f(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v6

    .line 717
    invoke-virtual {v0, v1, v3}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v4

    .line 718
    iget-object v7, v0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-float v9, v9

    invoke-static {v7, v9}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v7

    .line 719
    iget-object v9, v0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v9, v4}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v4

    .line 721
    invoke-static {v1, v3}, Lcom/baidu/mobstat/bq;->b(Landroid/app/Activity;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v1

    .line 722
    iget-object v3, v0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v3, v8}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v3

    .line 723
    iget-object v8, v0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v8, v1}, Lcom/baidu/mobstat/ao;->a(Landroid/content/Context;F)I

    move-result v1

    if-le v3, v7, :cond_7

    move v7, v3

    :cond_7
    if-le v1, v4, :cond_8

    move v4, v1

    :cond_8
    if-eqz v7, :cond_a

    if-nez v4, :cond_9

    goto :goto_1

    :cond_9
    sub-long v8, p4, p2

    .line 740
    new-instance v15, Lcom/baidu/mobstat/av;

    iget-object v10, v0, Lcom/baidu/mobstat/ba;->l:Ljava/lang/String;

    int-to-float v12, v3

    int-to-float v13, v1

    int-to-float v14, v7

    int-to-float v1, v4

    iget-boolean v3, v0, Lcom/baidu/mobstat/ba;->n:Z

    iget-object v11, v0, Lcom/baidu/mobstat/ba;->o:Ljava/lang/String;

    move-object v4, v15

    move-object v7, v10

    move-object/from16 v18, v11

    move-wide/from16 v10, p2

    move-object/from16 v19, v15

    move v15, v1

    move-object/from16 v16, v2

    move/from16 v17, v3

    invoke-direct/range {v4 .. v18}, Lcom/baidu/mobstat/av;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJFFFFLjava/lang/String;ZLjava/lang/String;)V

    .line 744
    iget-object v1, v0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/baidu/mobstat/ba;->a(Landroid/content/Context;Lcom/baidu/mobstat/av;)V

    :cond_a
    :goto_1
    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/view/View;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/View;",
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    if-nez p2, :cond_0

    return-void

    .line 631
    :cond_0
    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->c(Landroid/app/Activity;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 635
    :cond_1
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 636
    invoke-direct {p0, p2}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p2}, Lcom/baidu/mobstat/bq;->d(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637
    invoke-virtual {p4, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v0, :cond_3

    .line 641
    invoke-virtual {p5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-nez v0, :cond_4

    .line 645
    instance-of v0, p2, Landroid/webkit/WebView;

    if-nez v0, :cond_4

    instance-of v0, p2, Landroid/widget/ScrollView;

    if-eqz v0, :cond_5

    .line 646
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int v0, v0, v1

    if-eqz v0, :cond_5

    .line 648
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    :cond_5
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    .line 653
    check-cast p2, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 655
    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 656
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    move-object v2, p0

    move-object v3, p1

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;Landroid/view/View;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/webkit/WebView;)V
    .locals 1

    .line 1536
    new-instance v0, Lcom/baidu/mobstat/ba$5;

    invoke-direct {v0, p0, p2}, Lcom/baidu/mobstat/ba$5;-><init>(Lcom/baidu/mobstat/ba;Landroid/webkit/WebView;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/baidu/mobstat/av;)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->v:Lcom/baidu/mobstat/ba$a;

    if-eqz v0, :cond_1

    .line 753
    invoke-interface {v0, p2}, Lcom/baidu/mobstat/ba$a;->a(Lcom/baidu/mobstat/av;)V

    .line 756
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobstat/bd;->a(Landroid/content/Context;Lcom/baidu/mobstat/av;)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/app/Activity;J)V
    .locals 2

    .line 338
    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Landroid/app/Activity;)Landroid/view/View;

    move-result-object p1

    .line 339
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->n(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 344
    iget-boolean p1, p0, Lcom/baidu/mobstat/ba;->p:Z

    if-nez p1, :cond_0

    .line 345
    invoke-direct {p0}, Lcom/baidu/mobstat/ba;->c()V

    :cond_0
    return-void

    .line 350
    :cond_1
    invoke-direct {p0, v0}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 352
    iget-boolean p1, p0, Lcom/baidu/mobstat/ba;->p:Z

    if-nez p1, :cond_2

    .line 353
    invoke-direct {p0}, Lcom/baidu/mobstat/ba;->c()V

    :cond_2
    return-void

    .line 359
    :cond_3
    iput-wide p3, p0, Lcom/baidu/mobstat/ba;->i:J

    .line 360
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    .line 361
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->s(Landroid/view/View;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/baidu/mobstat/ba;->l:Ljava/lang/String;

    .line 362
    iget-object p3, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/baidu/mobstat/ba;->m:Ljava/lang/String;

    .line 363
    invoke-static {v0}, Lcom/baidu/mobstat/bq;->r(Landroid/view/View;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/baidu/mobstat/ba;->n:Z

    .line 365
    invoke-virtual {p0, p2, p1, v0}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobstat/ba;->o:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;Landroid/app/Activity;J)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    if-eqz v1, :cond_2

    if-nez v2, :cond_0

    goto :goto_0

    .line 1043
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobstat/az;->c()F

    move-result v3

    .line 1045
    invoke-static {v2, v3}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;F)Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    .line 1049
    :cond_1
    invoke-static/range {p2 .. p2}, Lcom/baidu/mobstat/bq;->s(Landroid/view/View;)Ljava/lang/String;

    move-result-object v8

    .line 1050
    invoke-static/range {p2 .. p2}, Lcom/baidu/mobstat/bq;->t(Landroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 1051
    invoke-static/range {p3 .. p3}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v6

    .line 1055
    invoke-static/range {p1 .. p1}, Lcom/baidu/mobstat/bq;->r(Landroid/view/View;)Z

    move-result v11

    .line 1056
    invoke-static/range {p3 .. p3}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v3, p3

    .line 1057
    invoke-static {v3, v2}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;Landroid/view/View;)Lorg/json/JSONArray;

    move-result-object v7

    .line 1059
    invoke-static/range {p1 .. p1}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 1060
    invoke-static {v2, v3}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sub-long v2, p4, p4

    .line 1063
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    const/4 v12, 0x1

    .line 1067
    new-instance v2, Lcom/baidu/mobstat/ax;

    move-object v4, v2

    const-string v20, ""

    move-wide/from16 v13, p4

    move-wide/from16 v15, p4

    move-wide/from16 v17, p4

    invoke-direct/range {v4 .. v20}, Lcom/baidu/mobstat/ax;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIJJJLjava/lang/String;Ljava/lang/String;)V

    .line 1070
    iget-object v3, v0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    invoke-direct {v0, v3, v1, v2}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;Landroid/view/View;Lcom/baidu/mobstat/ax;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 469
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 470
    invoke-virtual {p1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 474
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnScrollChangedListener;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewTreeObserver$OnScrollChangedListener;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 510
    :cond_0
    invoke-direct {p0, p3, p1}, Lcom/baidu/mobstat/ba;->a(Ljava/util/ArrayList;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 514
    :cond_1
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 518
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 519
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 525
    :cond_3
    :try_start_0
    invoke-virtual {v0, p2}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 526
    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/ba;Landroid/app/Activity;J)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;J)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobstat/ba;Landroid/view/View;Landroid/app/Activity;J)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;Landroid/app/Activity;J)V

    return-void
.end method

.method private a(Ljava/lang/ref/WeakReference;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;J)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 1464
    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    if-nez p1, :cond_1

    return-void

    .line 1469
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/ba;->d(Landroid/app/Activity;J)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_6

    .line 478
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 482
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 483
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_2

    .line 485
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    :cond_2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-nez v3, :cond_3

    .line 491
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 495
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 496
    invoke-virtual {v3}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 497
    :cond_4
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 501
    :cond_5
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_6
    :goto_1
    return-void
.end method

.method private a(Ljava/util/ArrayList;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;",
            "Landroid/view/ViewTreeObserver$OnScrollChangedListener;",
            ")V"
        }
    .end annotation

    .line 450
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 452
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_0

    goto :goto_1

    .line 457
    :cond_0
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 458
    invoke-direct {p0, v2, p2}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 461
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private a(Ljava/util/HashMap;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;J)V"
        }
    .end annotation

    if-eqz p1, :cond_4

    .line 786
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 790
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 791
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 792
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 794
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 795
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 799
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobstat/aw;

    .line 801
    invoke-virtual {v1}, Lcom/baidu/mobstat/aw;->e()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/baidu/mobstat/aw;->c()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-nez v6, :cond_3

    .line 802
    invoke-virtual {v1, p2, p3}, Lcom/baidu/mobstat/aw;->a(J)V

    goto :goto_1

    :cond_4
    :goto_2
    return-void
.end method

.method private a(Ljava/util/HashMap;Landroid/view/View;Lcom/baidu/mobstat/aw;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;",
            "Landroid/view/View;",
            "Lcom/baidu/mobstat/aw;",
            ")V"
        }
    .end annotation

    .line 838
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/aw;->a()Ljava/lang/String;

    move-result-object v0

    .line 839
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/aw;->c()J

    move-result-wide v1

    .line 840
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/aw;->e()J

    move-result-wide v3

    .line 841
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/aw;->d()Z

    move-result v5

    .line 843
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    return-void

    .line 851
    :cond_0
    invoke-direct/range {p0 .. p2}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;Landroid/view/View;)Ljava/util/LinkedHashMap;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 852
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 853
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    .line 854
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 855
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 856
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 857
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/ref/WeakReference;

    .line 858
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_0
    if-eqz v6, :cond_4

    .line 864
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/baidu/mobstat/aw;

    .line 865
    invoke-virtual {v10}, Lcom/baidu/mobstat/aw;->a()Ljava/lang/String;

    move-result-object v11

    .line 866
    invoke-virtual {v10}, Lcom/baidu/mobstat/aw;->c()J

    move-result-wide v12

    .line 867
    invoke-virtual {v10}, Lcom/baidu/mobstat/aw;->e()J

    move-result-wide v14

    .line 868
    invoke-virtual {v10}, Lcom/baidu/mobstat/aw;->d()Z

    move-result v7

    .line 870
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    goto :goto_1

    :cond_2
    if-eq v5, v7, :cond_3

    goto :goto_1

    :cond_3
    cmp-long v7, v14, v12

    if-gez v7, :cond_5

    cmp-long v7, v12, v1

    if-eqz v7, :cond_5

    goto :goto_1

    :cond_4
    const/4 v10, 0x0

    :cond_5
    if-nez v10, :cond_8

    if-nez v6, :cond_6

    .line 891
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :cond_6
    move-object/from16 v0, p3

    .line 893
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v8, :cond_7

    .line 896
    new-instance v8, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :cond_7
    move-object/from16 v0, p1

    .line 898
    invoke-virtual {v0, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 900
    :cond_8
    invoke-virtual {v10, v1, v2}, Lcom/baidu/mobstat/aw;->a(J)V

    .line 901
    invoke-virtual {v10, v3, v4}, Lcom/baidu/mobstat/aw;->b(J)V

    :goto_2
    return-void
.end method

.method private a(Ljava/util/HashMap;Landroid/view/View;Lcom/baidu/mobstat/ax;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;",
            "Landroid/view/View;",
            "Lcom/baidu/mobstat/ax;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    if-nez v1, :cond_0

    return-void

    .line 1107
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/ax;->k()J

    move-result-wide v3

    .line 1108
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v5

    .line 1109
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/ax;->d()Ljava/lang/String;

    move-result-object v7

    .line 1110
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/ax;->f()Ljava/lang/String;

    move-result-object v8

    .line 1113
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    goto/16 :goto_3

    .line 1122
    :cond_1
    invoke-direct/range {p0 .. p2}, Lcom/baidu/mobstat/ba;->b(Ljava/util/HashMap;Landroid/view/View;)Ljava/util/LinkedHashMap;

    move-result-object v7

    const/4 v8, 0x0

    if-eqz v7, :cond_2

    .line 1124
    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 1126
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    .line 1127
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 1128
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1130
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 1131
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/ref/WeakReference;

    .line 1132
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    goto :goto_0

    :cond_2
    move-object v7, v8

    move-object v9, v7

    :goto_0
    if-eqz v7, :cond_7

    .line 1138
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 1139
    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1140
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 1141
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    if-eqz v11, :cond_3

    .line 1143
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_4

    goto :goto_1

    .line 1147
    :cond_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/baidu/mobstat/ax;

    .line 1148
    invoke-virtual {v12}, Lcom/baidu/mobstat/ax;->k()J

    move-result-wide v13

    .line 1149
    invoke-virtual {v12}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v15

    .line 1151
    invoke-direct {v0, v12, v2}, Lcom/baidu/mobstat/ba;->a(Lcom/baidu/mobstat/ax;Lcom/baidu/mobstat/ax;)Z

    move-result v17

    if-nez v17, :cond_5

    goto :goto_2

    :cond_5
    cmp-long v17, v13, v15

    if-gez v17, :cond_6

    .line 1156
    invoke-virtual/range {p3 .. p3}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v13

    cmp-long v17, v15, v13

    if-eqz v17, :cond_6

    goto :goto_2

    :cond_6
    move-object v8, v12

    goto :goto_1

    :cond_7
    if-nez v8, :cond_c

    .line 1167
    invoke-direct {v0, v2}, Lcom/baidu/mobstat/ba;->a(Lcom/baidu/mobstat/ax;)Ljava/lang/String;

    move-result-object v3

    .line 1169
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    return-void

    :cond_8
    if-nez v7, :cond_a

    .line 1174
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1176
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1177
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1179
    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v9, :cond_9

    .line 1182
    new-instance v9, Ljava/lang/ref/WeakReference;

    invoke-direct {v9, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :cond_9
    move-object/from16 v1, p1

    .line 1184
    invoke-virtual {v1, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1186
    :cond_a
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_b

    .line 1189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1190
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1191
    invoke-virtual {v7, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1193
    :cond_b
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1197
    :cond_c
    invoke-virtual {v8, v5, v6}, Lcom/baidu/mobstat/ax;->a(J)V

    .line 1198
    invoke-virtual {v8, v3, v4}, Lcom/baidu/mobstat/ax;->b(J)V

    .line 1200
    invoke-virtual {v8}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v1

    invoke-virtual {v8}, Lcom/baidu/mobstat/ax;->i()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 1201
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1202
    invoke-virtual {v8, v1}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;)V

    :cond_d
    :goto_3
    return-void
.end method

.method private a(Ljava/util/HashMap;Ljava/util/HashMap;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;J)V"
        }
    .end annotation

    .line 1477
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    .line 1478
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1479
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1480
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1481
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1483
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 1487
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobstat/aw;

    .line 1488
    invoke-virtual {v1}, Lcom/baidu/mobstat/aw;->e()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/baidu/mobstat/aw;->c()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    .line 1489
    invoke-virtual {v1, p3, p4}, Lcom/baidu/mobstat/aw;->a(J)V

    goto :goto_1

    .line 1496
    :cond_3
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1497
    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_9

    .line 1498
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    .line 1499
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/HashMap;

    if-eqz p2, :cond_4

    .line 1501
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_2

    .line 1505
    :cond_5
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 1506
    :cond_6
    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1507
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1508
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 1510
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_7

    goto :goto_3

    .line 1514
    :cond_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobstat/ax;

    .line 1516
    invoke-virtual {v1}, Lcom/baidu/mobstat/ax;->k()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-nez v6, :cond_8

    .line 1517
    invoke-virtual {v1, p3, p4}, Lcom/baidu/mobstat/ax;->a(J)V

    .line 1519
    invoke-virtual {v1}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/baidu/mobstat/ax;->i()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1520
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1521
    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    return-void
.end method

.method private a(JJ)Z
    .locals 1

    sub-long/2addr p3, p1

    const-wide/16 p1, 0x0

    cmp-long v0, p3, p1

    if-lez v0, :cond_0

    const-wide/16 p1, 0x32

    cmp-long v0, p3, p1

    if-lez v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(Landroid/view/View;)Z
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    sget-object v1, Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;->TRACK_ALL:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    sget-object v1, Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;->TRACK_SINGLE:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    if-ne v0, v1, :cond_1

    .line 136
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->v(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    :goto_0
    return v2
.end method

.method private a(Lcom/baidu/mobstat/ax;Lcom/baidu/mobstat/ax;)Z
    .locals 12

    .line 1209
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->a()Ljava/lang/String;

    move-result-object v0

    .line 1210
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->b()Ljava/lang/String;

    move-result-object v1

    .line 1211
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->c()Lorg/json/JSONArray;

    move-result-object v2

    .line 1212
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->d()Ljava/lang/String;

    move-result-object v3

    .line 1213
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->e()Ljava/lang/String;

    move-result-object v4

    .line 1214
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->f()Ljava/lang/String;

    move-result-object v5

    .line 1215
    invoke-virtual {p2}, Lcom/baidu/mobstat/ax;->g()Z

    move-result p2

    .line 1217
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->a()Ljava/lang/String;

    move-result-object v6

    .line 1218
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->b()Ljava/lang/String;

    move-result-object v7

    .line 1219
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->c()Lorg/json/JSONArray;

    move-result-object v8

    .line 1220
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->d()Ljava/lang/String;

    move-result-object v9

    .line 1221
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->e()Ljava/lang/String;

    move-result-object v10

    .line 1222
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->f()Ljava/lang/String;

    move-result-object v11

    .line 1223
    invoke-virtual {p1}, Lcom/baidu/mobstat/ax;->g()Z

    move-result p1

    .line 1225
    invoke-direct {p0, v6, v0}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const/4 v6, 0x0

    if-nez v0, :cond_0

    return v6

    .line 1229
    :cond_0
    invoke-direct {p0, v7, v1}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return v6

    .line 1233
    :cond_1
    invoke-direct {p0, v8, v2}, Lcom/baidu/mobstat/ba;->a(Lorg/json/JSONArray;Lorg/json/JSONArray;)Z

    move-result v0

    if-nez v0, :cond_2

    return v6

    .line 1237
    :cond_2
    invoke-direct {p0, v9, v3}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    return v6

    .line 1241
    :cond_3
    invoke-direct {p0, v10, v4}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    return v6

    .line 1245
    :cond_4
    invoke-direct {p0, v11, v5}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    return v6

    :cond_5
    if-eq p1, p2, :cond_6

    return v6

    :cond_6
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic a(Lcom/baidu/mobstat/ba;Z)Z
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/baidu/mobstat/ba;->d:Z

    return p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-ne p1, p2, :cond_0

    goto :goto_0

    .line 1262
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/app/Activity;)Z
    .locals 2

    .line 559
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 560
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 561
    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 566
    :cond_0
    instance-of p1, p8, Lcom/baidu/mobstat/IIgnoreAutoTrace;

    if-eqz p1, :cond_1

    return v1

    :cond_1
    sub-long/2addr p6, p4

    const-wide/16 p1, 0x0

    cmp-long p3, p6, p1

    if-lez p3, :cond_2

    const-wide/16 p1, 0x1388

    cmp-long p3, p6, p1

    if-gez p3, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method

.method private a(Ljava/util/ArrayList;Landroid/view/View;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 539
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    .line 541
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 542
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-ne p2, v3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method

.method private a(Lorg/json/JSONArray;Lorg/json/JSONArray;)Z
    .locals 0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1275
    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic b(Lcom/baidu/mobstat/ba;F)F
    .locals 0

    .line 35
    iput p1, p0, Lcom/baidu/mobstat/ba;->z:F

    return p1
.end method

.method private b(Ljava/util/ArrayList;)Lcom/baidu/mobstat/ax;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;)",
            "Lcom/baidu/mobstat/ax;"
        }
    .end annotation

    move-object/from16 v0, p1

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    .line 1382
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_3

    .line 1386
    :cond_0
    new-instance v2, Lcom/baidu/mobstat/ba$4;

    move-object/from16 v3, p0

    invoke-direct {v2, v3}, Lcom/baidu/mobstat/ba$4;-><init>(Lcom/baidu/mobstat/ba;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1401
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1402
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    .line 1408
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v6, 0x0

    move-wide v8, v6

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/baidu/mobstat/ax;

    .line 1409
    invoke-virtual {v10}, Lcom/baidu/mobstat/ax;->i()J

    move-result-wide v11

    .line 1410
    invoke-virtual {v10}, Lcom/baidu/mobstat/ax;->l()Ljava/lang/String;

    move-result-object v13

    .line 1414
    :try_start_0
    invoke-static {v13}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 1415
    invoke-static {}, Lcom/baidu/mobstat/az;->a()Lcom/baidu/mobstat/az;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/baidu/mobstat/az;->d()J

    move-result-wide v16
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    cmp-long v18, v14, v16

    if-gez v18, :cond_1

    goto :goto_0

    :catch_0
    nop

    :cond_1
    cmp-long v14, v8, v6

    if-nez v14, :cond_2

    move-object v1, v10

    move-wide v8, v11

    :cond_2
    sub-long/2addr v11, v8

    cmp-long v14, v11, v6

    if-gez v14, :cond_3

    move-wide v11, v6

    .line 1434
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    const-string v15, "|"

    if-eqz v14, :cond_4

    .line 1435
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1437
    :cond_4
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1440
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1441
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1443
    :cond_5
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1446
    :goto_2
    invoke-virtual {v10}, Lcom/baidu/mobstat/ax;->h()I

    move-result v10

    add-int/2addr v5, v10

    goto/16 :goto_0

    :cond_6
    if-eqz v1, :cond_7

    .line 1450
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;)V

    .line 1451
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/baidu/mobstat/ax;->b(Ljava/lang/String;)V

    .line 1452
    invoke-virtual {v1, v5}, Lcom/baidu/mobstat/ax;->a(I)V

    :cond_7
    return-object v1

    :cond_8
    :goto_3
    move-object/from16 v3, p0

    return-object v1
.end method

.method static synthetic b(Lcom/baidu/mobstat/ba;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    return-object p0
.end method

.method private b(Ljava/util/HashMap;Landroid/view/View;)Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;"
        }
    .end annotation

    .line 1079
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1080
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1081
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1083
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    goto :goto_0

    .line 1088
    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    if-ne v2, p2, :cond_0

    .line 1090
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1092
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/HashMap;

    .line 1093
    invoke-virtual {p1, v1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private b(Landroid/app/Activity;J)V
    .locals 8

    .line 216
    iput-wide p2, p0, Lcom/baidu/mobstat/ba;->h:J

    .line 219
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 220
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->j:Ljava/lang/String;

    .line 223
    iget-object v1, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 224
    iput-wide v0, p0, Lcom/baidu/mobstat/ba;->i:J

    .line 229
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/ba;->p:Z

    if-eqz v0, :cond_1

    .line 230
    iget-wide v3, p0, Lcom/baidu/mobstat/ba;->g:J

    iget-object v7, p0, Lcom/baidu/mobstat/ba;->r:Ljava/util/List;

    move-object v1, p0

    move-object v2, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v7}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;JJLjava/util/List;)V

    .line 232
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->r:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    const/4 p1, 0x0

    .line 233
    iput-boolean p1, p0, Lcom/baidu/mobstat/ba;->q:Z

    .line 237
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;Ljava/util/HashMap;J)V

    .line 240
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->b(Ljava/util/HashMap;)V

    .line 241
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->c(Ljava/util/HashMap;)V

    .line 244
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->e(Ljava/util/HashMap;)V

    .line 245
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->f(Ljava/util/HashMap;)V

    .line 247
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->e:Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/baidu/mobstat/ba;->w:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-direct {p0, p1, p2}, Lcom/baidu/mobstat/ba;->a(Ljava/util/ArrayList;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    return-void
.end method

.method private b(Landroid/view/View;Landroid/app/Activity;J)V
    .locals 9

    if-nez p1, :cond_0

    return-void

    .line 769
    :cond_0
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 773
    :cond_1
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 777
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->r(Landroid/view/View;)Z

    move-result v8

    .line 779
    new-instance p2, Lcom/baidu/mobstat/aw;

    move-object v0, p2

    move-wide v2, p3

    move-wide v4, p3

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/baidu/mobstat/aw;-><init>(Ljava/lang/String;JJJZ)V

    .line 781
    iget-object p3, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    invoke-direct {p0, p3, p1, p2}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;Landroid/view/View;Lcom/baidu/mobstat/aw;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobstat/ba;Landroid/app/Activity;J)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/ba;->b(Landroid/app/Activity;J)V

    return-void
.end method

.method private b(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;)V"
        }
    .end annotation

    .line 906
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->d(Ljava/util/HashMap;)V

    return-void
.end method

.method private b(Ljava/util/HashMap;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;J)V"
        }
    .end annotation

    if-eqz p1, :cond_6

    .line 1000
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 1005
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1006
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1008
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1009
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_2

    goto :goto_0

    .line 1015
    :cond_2
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1016
    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1017
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1018
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1020
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    goto :goto_1

    .line 1024
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobstat/ax;

    .line 1026
    invoke-virtual {v2}, Lcom/baidu/mobstat/ax;->k()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_5

    .line 1027
    invoke-virtual {v2, p2, p3}, Lcom/baidu/mobstat/ax;->a(J)V

    .line 1029
    invoke-virtual {v2}, Lcom/baidu/mobstat/ax;->j()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/baidu/mobstat/ax;->i()J

    move-result-wide v5

    sub-long/2addr v3, v5

    .line 1030
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1031
    invoke-virtual {v2, v3}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    :goto_3
    return-void
.end method

.method private c()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 385
    iput-wide v0, p0, Lcom/baidu/mobstat/ba;->i:J

    const-string v0, ""

    .line 386
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->k:Ljava/lang/String;

    .line 387
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->l:Ljava/lang/String;

    .line 388
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->m:Ljava/lang/String;

    const/4 v1, 0x0

    .line 389
    iput-boolean v1, p0, Lcom/baidu/mobstat/ba;->n:Z

    .line 391
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->o:Ljava/lang/String;

    return-void
.end method

.method private c(Landroid/app/Activity;J)V
    .locals 10

    .line 589
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 590
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 591
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 593
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, v9

    move-object v3, v6

    move-object v4, v7

    move-object v5, v8

    .line 594
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;Landroid/view/View;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 597
    iget-boolean v0, p0, Lcom/baidu/mobstat/ba;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobstat/ba;->q:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/baidu/mobstat/ba;->g:J

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/baidu/mobstat/ba;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    invoke-direct {p0, v6}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v0

    .line 600
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v9}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601
    iput-object v0, p0, Lcom/baidu/mobstat/ba;->r:Ljava/util/List;

    const/4 v0, 0x1

    .line 603
    iput-boolean v0, p0, Lcom/baidu/mobstat/ba;->q:Z

    .line 606
    :cond_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 607
    iget-object v2, p0, Lcom/baidu/mobstat/ba;->w:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    iget-object v3, p0, Lcom/baidu/mobstat/ba;->e:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2, v3}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnScrollChangedListener;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->e:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/ba;->a(Ljava/util/ArrayList;)V

    .line 611
    iget-boolean v0, p0, Lcom/baidu/mobstat/ba;->d:Z

    if-eqz v0, :cond_2

    .line 612
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v0, p2, p3}, Lcom/baidu/mobstat/ba;->a(Ljava/lang/ref/WeakReference;J)V

    const/4 v0, 0x0

    .line 613
    iput-boolean v0, p0, Lcom/baidu/mobstat/ba;->d:Z

    .line 617
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    invoke-direct {p0, v0, p2, p3}, Lcom/baidu/mobstat/ba;->a(Ljava/util/HashMap;J)V

    .line 619
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 621
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/baidu/mobstat/ba;->b(Landroid/view/View;Landroid/app/Activity;J)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method static synthetic c(Lcom/baidu/mobstat/ba;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/baidu/mobstat/ba;->c()V

    return-void
.end method

.method static synthetic c(Lcom/baidu/mobstat/ba;Landroid/app/Activity;J)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/ba;->c(Landroid/app/Activity;J)V

    return-void
.end method

.method private c(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;)V"
        }
    .end annotation

    .line 912
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 913
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 914
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 915
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 918
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 921
    :cond_1
    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method static synthetic d(Lcom/baidu/mobstat/ba;)Ljava/lang/Object;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/baidu/mobstat/ba;->A:Ljava/lang/Object;

    return-object p0
.end method

.method private d(Landroid/app/Activity;J)V
    .locals 10

    .line 963
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->t:Ljava/util/HashMap;

    invoke-direct {p0, v0, p2, p3}, Lcom/baidu/mobstat/ba;->b(Ljava/util/HashMap;J)V

    .line 965
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    goto :goto_0

    .line 970
    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-nez v1, :cond_2

    goto :goto_0

    .line 975
    :cond_2
    invoke-static {v1}, Lcom/baidu/mobstat/bq;->d(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 979
    :cond_3
    invoke-direct {p0, v1}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 983
    :cond_4
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-nez v2, :cond_5

    goto :goto_0

    .line 987
    :cond_5
    move-object v8, v1

    check-cast v8, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 988
    :goto_1
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v9, v2, :cond_0

    .line 989
    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 991
    invoke-static {v4}, Lcom/baidu/mobstat/bq;->d(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object v2, p0

    move-object v3, v1

    move-object v5, p1

    move-wide v6, p2

    .line 992
    invoke-direct/range {v2 .. v7}, Lcom/baidu/mobstat/ba;->a(Landroid/view/View;Landroid/view/View;Landroid/app/Activity;J)V

    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_7
    return-void
.end method

.method static synthetic d(Lcom/baidu/mobstat/ba;Landroid/app/Activity;J)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobstat/ba;->d(Landroid/app/Activity;J)V

    return-void
.end method

.method private d(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/aw;",
            ">;>;)V"
        }
    .end annotation

    .line 925
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 927
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->s:Ljava/util/HashMap;

    .line 928
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 929
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 930
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 931
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 932
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 933
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 937
    :cond_1
    new-instance v0, Lcom/baidu/mobstat/ba$2;

    invoke-direct {v0, p0}, Lcom/baidu/mobstat/ba$2;-><init>(Lcom/baidu/mobstat/ba;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 953
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->v:Lcom/baidu/mobstat/ba$a;

    if-eqz v0, :cond_2

    .line 954
    invoke-interface {v0, p1}, Lcom/baidu/mobstat/ba$a;->a(Ljava/util/ArrayList;)V

    .line 957
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobstat/bd;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method private e(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;)V"
        }
    .end annotation

    .line 1283
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ba;->g(Ljava/util/HashMap;)V

    return-void
.end method

.method private f(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;)V"
        }
    .end annotation

    .line 1289
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1290
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1291
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1292
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    if-nez v1, :cond_0

    goto :goto_0

    .line 1298
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1299
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1300
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1301
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 1302
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 1305
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    goto :goto_0

    .line 1308
    :cond_2
    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method private g(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;>;)V"
        }
    .end annotation

    .line 1330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1333
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1334
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1335
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1336
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 1338
    invoke-direct {p0, v1}, Lcom/baidu/mobstat/ba;->h(Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1339
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1342
    :cond_0
    new-instance p1, Lcom/baidu/mobstat/ba$3;

    invoke-direct {p1, p0}, Lcom/baidu/mobstat/ba$3;-><init>(Lcom/baidu/mobstat/ba;)V

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1357
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->v:Lcom/baidu/mobstat/ba$a;

    if-eqz p1, :cond_1

    .line 1358
    invoke-interface {p1, v0}, Lcom/baidu/mobstat/ba$a;->b(Ljava/util/ArrayList;)V

    .line 1361
    :cond_1
    invoke-static {}, Lcom/baidu/mobstat/bd;->a()Lcom/baidu/mobstat/bd;

    move-result-object p1

    iget-object v1, p0, Lcom/baidu/mobstat/ba;->a:Landroid/content/Context;

    invoke-virtual {p1, v1, v0}, Lcom/baidu/mobstat/bd;->b(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method private h(Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;>;)",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobstat/ax;",
            ">;"
        }
    .end annotation

    .line 1365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1367
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1368
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1369
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1370
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1372
    invoke-direct {p0, v1}, Lcom/baidu/mobstat/ba;->b(Ljava/util/ArrayList;)Lcom/baidu/mobstat/ax;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1374
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;)Ljava/lang/String;
    .locals 7

    .line 369
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->s(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 370
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->t(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 371
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 372
    invoke-static {p3}, Lcom/baidu/mobstat/bq;->r(Landroid/view/View;)Z

    move-result v6

    .line 373
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->e(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/baidu/mobstat/bq;->c(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 374
    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;Landroid/view/View;)Lorg/json/JSONArray;

    move-result-object v2

    .line 376
    invoke-static {p3}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object p1

    .line 377
    invoke-static {p2, p1}, Lcom/baidu/mobstat/bq;->a(Landroid/view/View;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 380
    invoke-static/range {v0 .. v6}, Lcom/baidu/mobstat/ax;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Landroid/app/Activity;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1552
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 1553
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz p2, :cond_a

    if-nez p1, :cond_0

    goto/16 :goto_4

    .line 1559
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1560
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1565
    instance-of v4, p2, Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    .line 1566
    iget-object v4, p0, Lcom/baidu/mobstat/ba;->A:Ljava/lang/Object;

    monitor-enter v4

    .line 1567
    :try_start_0
    check-cast p2, Landroid/webkit/WebView;

    .line 1570
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobstat/ba;->a(Landroid/app/Activity;Landroid/webkit/WebView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1575
    :try_start_1
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->A:Ljava/lang/Object;

    const-wide/16 v5, 0x1388

    invoke-virtual {p1, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1580
    :catch_0
    :try_start_2
    iget p1, p0, Lcom/baidu/mobstat/ba;->y:F

    iget p2, p0, Lcom/baidu/mobstat/ba;->z:F

    mul-float p1, p1, p2

    float-to-int p1, p1

    .line 1581
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    .line 1582
    :cond_1
    instance-of p1, p2, Landroid/widget/ScrollView;

    if-eqz p1, :cond_2

    .line 1583
    check-cast p2, Landroid/widget/ScrollView;

    .line 1584
    invoke-virtual {p2}, Landroid/widget/ScrollView;->getChildCount()I

    move-result p1

    if-lez p1, :cond_5

    .line 1585
    invoke-virtual {p2, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    .line 1586
    invoke-virtual {p2, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    goto :goto_1

    .line 1588
    :cond_2
    instance-of p1, p2, Landroid/widget/ListView;

    if-eqz p1, :cond_3

    .line 1589
    check-cast p2, Landroid/widget/ListView;

    .line 1590
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->a(Landroid/widget/ListView;)I

    move-result p1

    :goto_0
    move p2, p1

    const/4 p1, 0x0

    goto :goto_1

    .line 1591
    :cond_3
    instance-of p1, p2, Landroid/widget/GridView;

    if-eqz p1, :cond_4

    .line 1592
    check-cast p2, Landroid/widget/GridView;

    .line 1593
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->a(Landroid/widget/GridView;)I

    move-result p1

    goto :goto_0

    .line 1594
    :cond_4
    invoke-static {p2}, Lcom/baidu/mobstat/bq;->q(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 1598
    :try_start_3
    check-cast p2, Landroid/support/v7/widget/RecyclerView;

    .line 1599
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->computeHorizontalScrollRange()I

    move-result p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1600
    :try_start_4
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollRange()I

    move-result p2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_1
    :cond_5
    const/4 p1, 0x0

    :catch_2
    const/4 p2, 0x0

    :goto_1
    if-nez p1, :cond_6

    move p1, v2

    :cond_6
    if-nez p2, :cond_7

    move p2, v3

    :cond_7
    if-lez p1, :cond_8

    goto :goto_2

    :cond_8
    const/4 p1, 0x0

    :goto_2
    if-lez p2, :cond_9

    goto :goto_3

    :cond_9
    const/4 p2, 0x0

    .line 1617
    :goto_3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1618
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 1554
    :cond_a
    :goto_4
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1555
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 148
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 151
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v3, Lcom/baidu/mobstat/ba$1;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/baidu/mobstat/ba$1;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/ref/WeakReference;J)V

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Landroid/view/KeyEvent;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 307
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    return-void

    .line 311
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    return-void

    .line 315
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v0, Lcom/baidu/mobstat/ba$9;

    invoke-direct {v0, p0}, Lcom/baidu/mobstat/ba$9;-><init>(Lcom/baidu/mobstat/ba;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Landroid/view/View;Landroid/app/Activity;)V
    .locals 8

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 277
    :cond_0
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 278
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 280
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 282
    iget-object p2, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v7, Lcom/baidu/mobstat/ba$8;

    move-object v0, v7

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobstat/ba$8;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Landroid/view/View;J)V

    invoke-virtual {p2, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/baidu/mobstat/ba;->f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 395
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v1, Lcom/baidu/mobstat/ba$10;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobstat/ba$10;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/ref/WeakReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 422
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 424
    new-instance v2, Lcom/baidu/mobstat/ba$12;

    invoke-direct {v2, p0, p1, v0, v1}, Lcom/baidu/mobstat/ba$12;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/ref/WeakReference;J)V

    .line 436
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->x:Ljava/lang/Runnable;

    if-eqz p1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 439
    :cond_1
    iput-object v2, p0, Lcom/baidu/mobstat/ba;->x:Ljava/lang/Runnable;

    .line 441
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    const-wide/16 v0, 0x15e

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 197
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 200
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v3, Lcom/baidu/mobstat/ba$6;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/baidu/mobstat/ba$6;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/ref/WeakReference;J)V

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b()Z
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/baidu/mobstat/ba;->f:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    sget-object v1, Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;->TRACK_NONE:Lcom/baidu/mobstat/MtjConfig$FeedTrackStrategy;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c(Landroid/app/Activity;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 256
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 259
    iget-object p1, p0, Lcom/baidu/mobstat/ba;->c:Landroid/os/Handler;

    new-instance v3, Lcom/baidu/mobstat/ba$7;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/baidu/mobstat/ba$7;-><init>(Lcom/baidu/mobstat/ba;Ljava/lang/ref/WeakReference;J)V

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
