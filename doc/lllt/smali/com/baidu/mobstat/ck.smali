.class public Lcom/baidu/mobstat/ck;
.super Lcom/baidu/mobstat/cj;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/ck$a;
    }
.end annotation


# static fields
.field static final synthetic f:Z


# instance fields
.field private g:Ljava/nio/ByteBuffer;

.field private final h:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    const-class v0, Lcom/baidu/mobstat/ck;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/baidu/mobstat/ck;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/baidu/mobstat/cj;-><init>()V

    .line 57
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/ck;->h:Ljava/util/Random;

    return-void
.end method

.method private a(Lcom/baidu/mobstat/cw$a;)B
    .locals 3

    .line 151
    sget-object v0, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 153
    :cond_0
    sget-object v0, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 155
    :cond_1
    sget-object v0, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_2

    const/4 p1, 0x2

    return p1

    .line 157
    :cond_2
    sget-object v0, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_3

    const/16 p1, 0x8

    return p1

    .line 159
    :cond_3
    sget-object v0, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_4

    const/16 p1, 0x9

    return p1

    .line 161
    :cond_4
    sget-object v0, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    if-ne p1, v0, :cond_5

    const/16 p1, 0xa

    return p1

    .line 163
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Don\'t know how to handle "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobstat/cw$a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(B)Lcom/baidu/mobstat/cw$a;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/co;
        }
    .end annotation

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 229
    new-instance v0, Lcom/baidu/mobstat/co;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknow optcode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-short p1, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :pswitch_0
    sget-object p1, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    return-object p1

    .line 224
    :pswitch_1
    sget-object p1, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    return-object p1

    .line 222
    :pswitch_2
    sget-object p1, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    return-object p1

    .line 219
    :cond_0
    sget-object p1, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;

    return-object p1

    .line 217
    :cond_1
    sget-object p1, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    return-object p1

    .line 215
    :cond_2
    sget-object p1, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 167
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    const-string v0, "SHA1"

    .line 171
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobstat/dh;->a([B)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 173
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(JI)[B
    .locals 5

    .line 204
    new-array v0, p3, [B

    mul-int/lit8 v1, p3, 0x8

    add-int/lit8 v1, v1, -0x8

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p3, :cond_0

    mul-int/lit8 v3, v2, 0x8

    sub-int v3, v1, v3

    ushr-long v3, p1, v3

    long-to-int v4, v3

    int-to-byte v3, v4

    .line 207
    aput-byte v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Lcom/baidu/mobstat/cy;Lcom/baidu/mobstat/df;)Lcom/baidu/mobstat/cj$b;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation

    const-string v0, "Sec-WebSocket-Key"

    .line 61
    invoke-interface {p1, v0}, Lcom/baidu/mobstat/cy;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Sec-WebSocket-Accept"

    invoke-interface {p2, v1}, Lcom/baidu/mobstat/df;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    invoke-interface {p2, v1}, Lcom/baidu/mobstat/df;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 65
    invoke-interface {p1, v0}, Lcom/baidu/mobstat/cy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 66
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ck;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 69
    sget-object p1, Lcom/baidu/mobstat/cj$b;->a:Lcom/baidu/mobstat/cj$b;

    return-object p1

    .line 70
    :cond_1
    sget-object p1, Lcom/baidu/mobstat/cj$b;->b:Lcom/baidu/mobstat/cj$b;

    return-object p1

    .line 62
    :cond_2
    :goto_0
    sget-object p1, Lcom/baidu/mobstat/cj$b;->b:Lcom/baidu/mobstat/cj$b;

    return-object p1
.end method

.method public a(Lcom/baidu/mobstat/cz;)Lcom/baidu/mobstat/cz;
    .locals 2

    const-string v0, "Upgrade"

    const-string v1, "websocket"

    .line 180
    invoke-interface {p1, v0, v1}, Lcom/baidu/mobstat/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Connection"

    .line 181
    invoke-interface {p1, v1, v0}, Lcom/baidu/mobstat/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Sec-WebSocket-Version"

    const-string v1, "8"

    .line 182
    invoke-interface {p1, v0, v1}, Lcom/baidu/mobstat/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 185
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->h:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 186
    invoke-static {v0}, Lcom/baidu/mobstat/dh;->a([B)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Sec-WebSocket-Key"

    invoke-interface {p1, v1, v0}, Lcom/baidu/mobstat/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/baidu/mobstat/cw;)Ljava/nio/ByteBuffer;
    .locals 13

    .line 84
    invoke-interface {p1}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->d:Lcom/baidu/mobstat/ce$b;

    sget-object v2, Lcom/baidu/mobstat/ce$b;->a:Lcom/baidu/mobstat/ce$b;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 86
    :goto_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v5, 0x7d

    const/4 v6, 0x2

    const/16 v7, 0x8

    if-gt v2, v5, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const v5, 0xffff

    if-gt v2, v5, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    const/16 v2, 0x8

    :goto_1
    if-le v2, v3, :cond_3

    add-int/lit8 v5, v2, 0x1

    goto :goto_2

    :cond_3
    move v5, v2

    :goto_2
    add-int/2addr v5, v3

    const/4 v8, 0x4

    if-eqz v1, :cond_4

    const/4 v9, 0x4

    goto :goto_3

    :cond_4
    const/4 v9, 0x0

    :goto_3
    add-int/2addr v5, v9

    .line 87
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v9

    add-int/2addr v5, v9

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 88
    invoke-interface {p1}, Lcom/baidu/mobstat/cw;->f()Lcom/baidu/mobstat/cw$a;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/baidu/mobstat/ck;->a(Lcom/baidu/mobstat/cw$a;)B

    move-result v9

    .line 89
    invoke-interface {p1}, Lcom/baidu/mobstat/cw;->d()Z

    move-result p1

    const/16 v10, -0x80

    if-eqz p1, :cond_5

    const/16 p1, -0x80

    goto :goto_4

    :cond_5
    const/4 p1, 0x0

    :goto_4
    int-to-byte p1, p1

    or-int/2addr p1, v9

    int-to-byte p1, p1

    .line 91
    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 92
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p1

    int-to-long v11, p1

    invoke-direct {p0, v11, v12, v2}, Lcom/baidu/mobstat/ck;->a(JI)[B

    move-result-object p1

    .line 93
    sget-boolean v9, Lcom/baidu/mobstat/ck;->f:Z

    if-nez v9, :cond_7

    array-length v9, p1

    if-ne v9, v2, :cond_6

    goto :goto_5

    :cond_6
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_7
    :goto_5
    if-ne v2, v3, :cond_9

    .line 96
    aget-byte p1, p1, v4

    if-eqz v1, :cond_8

    goto :goto_6

    :cond_8
    const/4 v10, 0x0

    :goto_6
    or-int/2addr p1, v10

    int-to-byte p1, p1

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_9

    :cond_9
    if-ne v2, v6, :cond_b

    if-eqz v1, :cond_a

    goto :goto_7

    :cond_a
    const/4 v10, 0x0

    :goto_7
    or-int/lit8 v2, v10, 0x7e

    int-to-byte v2, v2

    .line 98
    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 99
    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_9

    :cond_b
    if-ne v2, v7, :cond_11

    if-eqz v1, :cond_c

    goto :goto_8

    :cond_c
    const/4 v10, 0x0

    :goto_8
    or-int/lit8 v2, v10, 0x7f

    int-to-byte v2, v2

    .line 101
    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 102
    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :goto_9
    if-eqz v1, :cond_d

    .line 107
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 108
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->h:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 109
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 110
    :goto_a
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 111
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    rem-int/lit8 v2, v4, 0x4

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    invoke-virtual {v5, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/2addr v4, v3

    goto :goto_a

    .line 114
    :cond_d
    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 116
    :cond_e
    sget-boolean p1, Lcom/baidu/mobstat/ck;->f:Z

    if-nez p1, :cond_10

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p1

    if-nez p1, :cond_f

    goto :goto_b

    :cond_f
    new-instance p1, Ljava/lang/AssertionError;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(I)V

    throw p1

    .line 117
    :cond_10
    :goto_b
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-object v5

    .line 104
    :cond_11
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Size representation not supported/specified"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto :goto_d

    :goto_c
    throw p1

    :goto_d
    goto :goto_c
.end method

.method public a(Ljava/nio/ByteBuffer;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/cw;",
            ">;"
        }
    .end annotation

    .line 124
    new-instance v0, Lcom/baidu/mobstat/cx;

    invoke-direct {v0}, Lcom/baidu/mobstat/cx;-><init>()V

    .line 126
    :try_start_0
    invoke-interface {v0, p1}, Lcom/baidu/mobstat/cv;->a(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Lcom/baidu/mobstat/cn; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    .line 130
    invoke-interface {v0, p1}, Lcom/baidu/mobstat/cv;->a(Z)V

    .line 131
    sget-object p1, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;

    invoke-interface {v0, p1}, Lcom/baidu/mobstat/cv;->a(Lcom/baidu/mobstat/cw$a;)V

    .line 132
    invoke-interface {v0, p2}, Lcom/baidu/mobstat/cv;->b(Z)V

    .line 133
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 128
    new-instance p2, Lcom/baidu/mobstat/cr;

    invoke-direct {p2, p1}, Lcom/baidu/mobstat/cr;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    .line 378
    iput-object v0, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public b()Lcom/baidu/mobstat/cj$a;
    .locals 1

    .line 388
    sget-object v0, Lcom/baidu/mobstat/cj$a;->c:Lcom/baidu/mobstat/cj$a;

    return-object v0
.end method

.method public c()Lcom/baidu/mobstat/cj;
    .locals 1

    .line 383
    new-instance v0, Lcom/baidu/mobstat/ck;

    invoke-direct {v0}, Lcom/baidu/mobstat/ck;-><init>()V

    return-object v0
.end method

.method public c(Ljava/nio/ByteBuffer;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/cw;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cq;,
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 236
    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 239
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_3

    .line 242
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 243
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    .line 244
    iget-object v2, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 249
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 250
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v1, v3, v4, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 253
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 255
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v1}, Lcom/baidu/mobstat/ck;->e(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/cw;

    move-result-object v1

    .line 256
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    .line 257
    iput-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Lcom/baidu/mobstat/ck$a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 260
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    .line 261
    invoke-virtual {v0}, Lcom/baidu/mobstat/ck$a;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/ck;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 262
    sget-boolean v1, Lcom/baidu/mobstat/ck;->f:Z

    if-nez v1, :cond_2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-object v2, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    if-le v1, v2, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 263
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 264
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 265
    iput-object v0, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 270
    :cond_3
    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 271
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 273
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/ck;->e(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/cw;

    move-result-object v1

    .line 274
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/baidu/mobstat/ck$a; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 277
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    .line 278
    invoke-virtual {v1}, Lcom/baidu/mobstat/ck$a;->a()I

    move-result v1

    .line 279
    invoke-virtual {p0, v1}, Lcom/baidu/mobstat/ck;->a(I)I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    .line 280
    iget-object v1, p0, Lcom/baidu/mobstat/ck;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    :cond_4
    return-object v0
.end method

.method public e(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/cw;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/ck$a;,
            Lcom/baidu/mobstat/cn;
        }
    .end annotation

    .line 289
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_14

    .line 293
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    shr-int/lit8 v3, v2, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    and-int/lit8 v6, v2, 0x7f

    const/4 v7, 0x4

    shr-int/2addr v6, v7

    int-to-byte v6, v6

    if-nez v6, :cond_13

    .line 298
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v6

    and-int/lit8 v8, v6, -0x80

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    and-int/lit8 v6, v6, 0x7f

    int-to-byte v6, v6

    and-int/lit8 v2, v2, 0xf

    int-to-byte v2, v2

    .line 301
    invoke-direct {p0, v2}, Lcom/baidu/mobstat/ck;->a(B)Lcom/baidu/mobstat/cw$a;

    move-result-object v2

    if-nez v3, :cond_3

    .line 304
    sget-object v9, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_2

    sget-object v9, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_2

    sget-object v9, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_2

    goto :goto_2

    .line 305
    :cond_2
    new-instance p1, Lcom/baidu/mobstat/co;

    const-string v0, "control frames may no be fragmented"

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_2
    if-ltz v6, :cond_4

    const/16 v9, 0x7d

    if-le v6, v9, :cond_8

    .line 310
    :cond_4
    sget-object v9, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_12

    sget-object v9, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_12

    sget-object v9, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    if-eq v2, v9, :cond_12

    const/16 v9, 0x7e

    if-ne v6, v9, :cond_6

    if-lt v0, v7, :cond_5

    const/4 v6, 0x3

    new-array v6, v6, [B

    .line 318
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v9

    aput-byte v9, v6, v4

    .line 319
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    aput-byte v4, v6, v1

    .line 320
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v6}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v6

    const/4 v1, 0x4

    goto :goto_4

    .line 316
    :cond_5
    new-instance p1, Lcom/baidu/mobstat/ck$a;

    invoke-direct {p1, p0, v7}, Lcom/baidu/mobstat/ck$a;-><init>(Lcom/baidu/mobstat/ck;I)V

    throw p1

    :cond_6
    const/16 v1, 0xa

    if-lt v0, v1, :cond_11

    const/16 v4, 0x8

    new-array v6, v4, [B

    const/4 v9, 0x0

    :goto_3
    if-ge v9, v4, :cond_7

    .line 327
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v10

    aput-byte v10, v6, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 329
    :cond_7
    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v6}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v4}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v9

    const-wide/32 v11, 0x7fffffff

    cmp-long v4, v9, v11

    if-gtz v4, :cond_10

    long-to-int v6, v9

    :cond_8
    :goto_4
    if-eqz v8, :cond_9

    const/4 v4, 0x4

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    :goto_5
    add-int/2addr v1, v4

    add-int/2addr v1, v6

    if-lt v0, v1, :cond_f

    .line 346
    invoke-virtual {p0, v6}, Lcom/baidu/mobstat/ck;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v8, :cond_a

    new-array v1, v7, [B

    .line 349
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    :goto_6
    if-ge v5, v6, :cond_b

    .line 351
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    rem-int/lit8 v7, v5, 0x4

    aget-byte v7, v1, v7

    xor-int/2addr v4, v7

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 354
    :cond_a
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    invoke-virtual {v0, v1, v4, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 355
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 359
    :cond_b
    sget-object p1, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    if-ne v2, p1, :cond_c

    .line 360
    new-instance p1, Lcom/baidu/mobstat/cu;

    invoke-direct {p1}, Lcom/baidu/mobstat/cu;-><init>()V

    goto :goto_7

    .line 362
    :cond_c
    new-instance p1, Lcom/baidu/mobstat/cx;

    invoke-direct {p1}, Lcom/baidu/mobstat/cx;-><init>()V

    .line 363
    invoke-interface {p1, v3}, Lcom/baidu/mobstat/cv;->a(Z)V

    .line 364
    invoke-interface {p1, v2}, Lcom/baidu/mobstat/cv;->a(Lcom/baidu/mobstat/cw$a;)V

    .line 366
    :goto_7
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 367
    invoke-interface {p1, v0}, Lcom/baidu/mobstat/cv;->a(Ljava/nio/ByteBuffer;)V

    .line 368
    sget-object v0, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    if-ne v2, v0, :cond_e

    .line 369
    invoke-interface {p1}, Lcom/baidu/mobstat/cv;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobstat/di;->b(Ljava/nio/ByteBuffer;)Z

    move-result v0

    if-eqz v0, :cond_d

    goto :goto_8

    .line 370
    :cond_d
    new-instance p1, Lcom/baidu/mobstat/cn;

    const/16 v0, 0x3ef

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/cn;-><init>(I)V

    throw p1

    :cond_e
    :goto_8
    return-object p1

    .line 344
    :cond_f
    new-instance p1, Lcom/baidu/mobstat/ck$a;

    invoke-direct {p1, p0, v1}, Lcom/baidu/mobstat/ck$a;-><init>(Lcom/baidu/mobstat/ck;I)V

    throw p1

    .line 331
    :cond_10
    new-instance p1, Lcom/baidu/mobstat/cq;

    const-string v0, "Payloadsize is to big..."

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/cq;-><init>(Ljava/lang/String;)V

    throw p1

    .line 324
    :cond_11
    new-instance p1, Lcom/baidu/mobstat/ck$a;

    invoke-direct {p1, p0, v1}, Lcom/baidu/mobstat/ck$a;-><init>(Lcom/baidu/mobstat/ck;I)V

    throw p1

    .line 311
    :cond_12
    new-instance p1, Lcom/baidu/mobstat/co;

    const-string v0, "more than 125 octets"

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/String;)V

    throw p1

    .line 297
    :cond_13
    new-instance p1, Lcom/baidu/mobstat/co;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bad rsv "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/co;-><init>(Ljava/lang/String;)V

    throw p1

    .line 292
    :cond_14
    new-instance p1, Lcom/baidu/mobstat/ck$a;

    invoke-direct {p1, p0, v1}, Lcom/baidu/mobstat/ck$a;-><init>(Lcom/baidu/mobstat/ck;I)V

    goto :goto_a

    :goto_9
    throw p1

    :goto_a
    goto :goto_9
.end method
