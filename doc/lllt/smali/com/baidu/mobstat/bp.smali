.class public Lcom/baidu/mobstat/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/bp$c;,
        Lcom/baidu/mobstat/bp$b;,
        Lcom/baidu/mobstat/bp$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/bp$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Lcom/baidu/mobstat/br;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/baidu/mobstat/br;Z)V
    .locals 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/bp;->b:Ljava/util/List;

    .line 30
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobstat/bp;->c:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/baidu/mobstat/bp;->d:Lcom/baidu/mobstat/br;

    .line 32
    iput-boolean p3, p0, Lcom/baidu/mobstat/bp;->e:Z

    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/view/View;Lcom/baidu/mobstat/bp$c;Landroid/view/View;)V
    .locals 6

    if-nez p2, :cond_0

    return-void

    .line 88
    :cond_0
    invoke-static {p2}, Lcom/baidu/mobstat/ap;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 100
    :cond_1
    invoke-static {p1, p2}, Lcom/baidu/mobstat/bq;->c(Landroid/app/Activity;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 104
    :cond_2
    new-instance v0, Lcom/baidu/mobstat/bp$c;

    invoke-direct {v0, p2, p3, p4}, Lcom/baidu/mobstat/bp$c;-><init>(Landroid/view/View;Lcom/baidu/mobstat/bp$c;Landroid/view/View;)V

    if-eqz p3, :cond_7

    .line 108
    invoke-virtual {v0}, Lcom/baidu/mobstat/bp$c;->a()Ljava/lang/String;

    move-result-object p3

    .line 109
    invoke-virtual {v0}, Lcom/baidu/mobstat/bp$c;->b()Ljava/lang/String;

    move-result-object v1

    .line 111
    iget-boolean v2, p0, Lcom/baidu/mobstat/bp;->a:Z

    if-eqz v2, :cond_3

    .line 112
    invoke-virtual {v0}, Lcom/baidu/mobstat/bp$c;->c()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/baidu/mobstat/bq;->b(Landroid/view/View;Ljava/lang/String;)Z

    move-result p3

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/baidu/mobstat/bp;->b:Ljava/util/List;

    invoke-direct {p0, v2, p3, v1}, Lcom/baidu/mobstat/bp;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p3

    :goto_0
    if-nez p3, :cond_4

    .line 114
    iget-boolean v1, p0, Lcom/baidu/mobstat/bp;->e:Z

    if-eqz v1, :cond_7

    .line 115
    :cond_4
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v1

    const-string v2, "; content:"

    const-string v3, "accumulate view:"

    if-eqz v1, :cond_5

    if-eqz p3, :cond_5

    .line 116
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/baidu/mobstat/bq;->h(Landroid/view/View;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 116
    invoke-virtual {v1, v4}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 119
    :cond_5
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 120
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/baidu/mobstat/bq;->h(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-virtual {v1, v2}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    .line 124
    :cond_6
    iget-object v1, p0, Lcom/baidu/mobstat/bp;->d:Lcom/baidu/mobstat/br;

    invoke-virtual {v1, p2, p3}, Lcom/baidu/mobstat/br;->a(Landroid/view/View;Z)V

    .line 128
    :cond_7
    instance-of p3, p2, Landroid/webkit/WebView;

    if-eqz p3, :cond_8

    return-void

    .line 132
    :cond_8
    instance-of p3, p2, Landroid/view/ViewGroup;

    if-eqz p3, :cond_9

    .line 133
    check-cast p2, Landroid/view/ViewGroup;

    const/4 p3, 0x0

    .line 135
    :goto_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge p3, v1, :cond_9

    .line 136
    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0, p4}, Lcom/baidu/mobstat/bp;->a(Landroid/app/Activity;Landroid/view/View;Lcom/baidu/mobstat/bp$c;Landroid/view/View;)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    :cond_9
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/bp$b;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .line 143
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobstat/bp$b;

    .line 144
    iget-boolean v1, v0, Lcom/baidu/mobstat/bp$b;->c:Z

    if-eqz v1, :cond_1

    move-object v1, p3

    goto :goto_1

    :cond_1
    move-object v1, p2

    .line 146
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 150
    :cond_2
    iget-object v0, v0, Lcom/baidu/mobstat/bp$b;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .line 75
    iget-boolean v0, p0, Lcom/baidu/mobstat/bp;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/baidu/mobstat/bp;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobstat/bp;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    .line 79
    :cond_1
    invoke-static {p1}, Lcom/baidu/mobstat/bq;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/baidu/mobstat/bp;->a(Landroid/app/Activity;Landroid/view/View;Lcom/baidu/mobstat/bp$c;Landroid/view/View;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 11

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "meta"

    .line 41
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    const-string v3, "matchAll"

    .line 42
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 43
    :goto_0
    iput-boolean v2, p0, Lcom/baidu/mobstat/bp;->a:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    .line 48
    :goto_1
    iget-boolean v2, p0, Lcom/baidu/mobstat/bp;->a:Z

    if-eqz v2, :cond_2

    return-void

    :cond_2
    :try_start_1
    const-string v2, "data"

    .line 53
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    const/4 v2, 0x0

    .line 54
    :goto_2
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 55
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    const-string v4, "page"

    .line 57
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "layout"

    .line 58
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "contentAsLabel"

    .line 59
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v10

    const-string v4, "ignoreCellIndex"

    .line 61
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    const/4 v9, 0x1

    goto :goto_3

    :cond_3
    const/4 v9, 0x0

    .line 64
    :goto_3
    iget-object v3, p0, Lcom/baidu/mobstat/bp;->c:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 65
    new-instance v3, Lcom/baidu/mobstat/bp$b;

    move-object v5, v3

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, Lcom/baidu/mobstat/bp$b;-><init>(Lcom/baidu/mobstat/bp;Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 66
    iget-object v4, p0, Lcom/baidu/mobstat/bp;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_1
    :cond_5
    return-void
.end method
