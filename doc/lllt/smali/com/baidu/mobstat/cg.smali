.class public Lcom/baidu/mobstat/cg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobstat/ce;


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/baidu/mobstat/cj;",
            ">;"
        }
    .end annotation
.end field

.field public static b:I

.field public static c:Z

.field static final synthetic h:Z


# instance fields
.field public final d:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/nio/channels/SelectionKey;

.field public g:Ljava/nio/channels/ByteChannel;

.field private final i:Lcom/baidu/mobstat/ch;

.field private volatile j:Z

.field private k:Lcom/baidu/mobstat/ce$a;

.field private l:Lcom/baidu/mobstat/cj;

.field private m:Lcom/baidu/mobstat/ce$b;

.field private n:Lcom/baidu/mobstat/cw;

.field private o:Ljava/nio/ByteBuffer;

.field private p:Lcom/baidu/mobstat/cy;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    const-class v0, Lcom/baidu/mobstat/cg;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/baidu/mobstat/cg;->a:Ljava/util/List;

    const/16 v0, 0x4000

    .line 38
    sput v0, Lcom/baidu/mobstat/cg;->b:I

    const/4 v0, 0x0

    .line 39
    sput-boolean v0, Lcom/baidu/mobstat/cg;->c:Z

    .line 42
    sget-object v0, Lcom/baidu/mobstat/cg;->a:Ljava/util/List;

    new-instance v1, Lcom/baidu/mobstat/cl;

    invoke-direct {v1}, Lcom/baidu/mobstat/cl;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/baidu/mobstat/cg;->a:Ljava/util/List;

    new-instance v1, Lcom/baidu/mobstat/ck;

    invoke-direct {v1}, Lcom/baidu/mobstat/ck;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/baidu/mobstat/ch;Lcom/baidu/mobstat/cj;)V
    .locals 2

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 66
    iput-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z

    .line 67
    sget-object v1, Lcom/baidu/mobstat/ce$a;->a:Lcom/baidu/mobstat/ce$a;

    iput-object v1, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    const/4 v1, 0x0

    .line 70
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    .line 74
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    .line 79
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    .line 84
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    .line 86
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->q:Ljava/lang/String;

    .line 87
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->r:Ljava/lang/Integer;

    .line 88
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->s:Ljava/lang/Boolean;

    .line 90
    iput-object v1, p0, Lcom/baidu/mobstat/cg;->t:Ljava/lang/String;

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 101
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->d:Ljava/util/concurrent/BlockingQueue;

    .line 102
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->e:Ljava/util/concurrent/BlockingQueue;

    .line 103
    iput-object p1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    .line 104
    sget-object p1, Lcom/baidu/mobstat/ce$b;->a:Lcom/baidu/mobstat/ce$b;

    iput-object p1, p0, Lcom/baidu/mobstat/cg;->m:Lcom/baidu/mobstat/ce$b;

    if-eqz p2, :cond_0

    .line 106
    invoke-virtual {p2}, Lcom/baidu/mobstat/cj;->c()Lcom/baidu/mobstat/cj;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    :cond_0
    return-void

    .line 100
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "parameters must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Lcom/baidu/mobstat/dd;)V
    .locals 3

    .line 590
    sget-boolean v0, Lcom/baidu/mobstat/cg;->c:Z

    if-eqz v0, :cond_0

    .line 591
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "open using draft: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 592
    :cond_0
    sget-object v0, Lcom/baidu/mobstat/ce$a;->c:Lcom/baidu/mobstat/ce$a;

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    .line 594
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/dd;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 596
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/baidu/mobstat/cw;",
            ">;)V"
        }
    .end annotation

    .line 502
    invoke-virtual {p0}, Lcom/baidu/mobstat/cg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 504
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobstat/cw;

    .line 505
    invoke-virtual {p0, v0}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cw;)V

    goto :goto_0

    :cond_0
    return-void

    .line 503
    :cond_1
    new-instance p1, Lcom/baidu/mobstat/cs;

    invoke-direct {p1}, Lcom/baidu/mobstat/cs;-><init>()V

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/nio/ByteBuffer;",
            ">;)V"
        }
    .end annotation

    .line 584
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 585
    invoke-direct {p0, v0}, Lcom/baidu/mobstat/cg;->f(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(ILjava/lang/String;Z)V
    .locals 4

    .line 330
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->e:Lcom/baidu/mobstat/ce$a;

    if-eq v0, v1, :cond_a

    .line 331
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->c:Lcom/baidu/mobstat/ce$a;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_5

    const/16 v0, 0x3ee

    if-ne p1, v0, :cond_2

    .line 333
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 334
    :cond_1
    :goto_0
    sget-object p3, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    iput-object p3, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    .line 335
    invoke-virtual {p0, p1, p2, v2}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    return-void

    .line 338
    :cond_2
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v1}, Lcom/baidu/mobstat/cj;->b()Lcom/baidu/mobstat/cj$a;

    move-result-object v1

    sget-object v3, Lcom/baidu/mobstat/cj$a;->a:Lcom/baidu/mobstat/cj$a;

    if-eq v1, v3, :cond_4

    if-nez p3, :cond_3

    .line 342
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, p1, p2}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/baidu/mobstat/cn; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 344
    :try_start_1
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v3, p0, v1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 347
    :cond_3
    :goto_1
    new-instance v1, Lcom/baidu/mobstat/cu;

    invoke-direct {v1, p1, p2}, Lcom/baidu/mobstat/cu;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cw;)V
    :try_end_1
    .catch Lcom/baidu/mobstat/cn; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 349
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v3, p0, v1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    const-string v1, "generated frame is invalid"

    .line 350
    invoke-virtual {p0, v0, v1, v2}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    .line 353
    :cond_4
    :goto_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    goto :goto_4

    :cond_5
    const/4 v0, -0x3

    if-ne p1, v0, :cond_8

    .line 355
    sget-boolean v1, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v1, :cond_7

    if-eqz p3, :cond_6

    goto :goto_3

    :cond_6
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_7
    :goto_3
    const/4 v1, 0x1

    .line 356
    invoke-virtual {p0, v0, p2, v1}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    goto :goto_4

    :cond_8
    const/4 v0, -0x1

    .line 358
    invoke-virtual {p0, v0, p2, v2}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    :goto_4
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_9

    .line 361
    invoke-virtual {p0, p1, p2, p3}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    .line 362
    :cond_9
    sget-object p1, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    iput-object p1, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    const/4 p1, 0x0

    .line 363
    iput-object p1, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    :cond_a
    return-void
.end method

.method private c(Ljava/nio/ByteBuffer;)Z
    .locals 7

    .line 141
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 145
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 147
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 148
    iput-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 152
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 153
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    .line 155
    :goto_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    const/4 v1, 0x0

    .line 157
    :try_start_0
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    const/4 v3, 0x1

    if-nez v2, :cond_2

    .line 158
    invoke-direct {p0, v0}, Lcom/baidu/mobstat/cg;->e(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/cj$b;

    move-result-object v2

    .line 159
    sget-object v4, Lcom/baidu/mobstat/cj$b;->a:Lcom/baidu/mobstat/cj$b;
    :try_end_0
    .catch Lcom/baidu/mobstat/cm; {:try_start_0 .. :try_end_0} :catch_4

    if-ne v2, v4, :cond_2

    .line 161
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v2, p0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/baidu/mobstat/di;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/baidu/mobstat/cg;->f(Ljava/nio/ByteBuffer;)V

    const/4 v2, -0x3

    const-string v4, ""

    .line 162
    invoke-virtual {p0, v2, v4}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/baidu/mobstat/cn; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/baidu/mobstat/cm; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    :catch_0
    const/16 v2, 0x3ee

    :try_start_2
    const-string v4, "remote peer closed connection before flashpolicy could be transmitted"

    .line 164
    invoke-direct {p0, v2, v4, v3}, Lcom/baidu/mobstat/cg;->c(ILjava/lang/String;Z)V
    :try_end_2
    .catch Lcom/baidu/mobstat/cm; {:try_start_2 .. :try_end_2} :catch_4

    :goto_1
    return v1

    .line 172
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->m:Lcom/baidu/mobstat/ce$b;

    sget-object v4, Lcom/baidu/mobstat/ce$b;->a:Lcom/baidu/mobstat/ce$b;

    if-ne v2, v4, :cond_9

    .line 173
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    iget-object v4, p0, Lcom/baidu/mobstat/cg;->m:Lcom/baidu/mobstat/ce$b;

    invoke-virtual {v2, v4}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/ce$b;)V

    .line 174
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v2, v0}, Lcom/baidu/mobstat/cj;->d(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/dd;

    move-result-object v2

    .line 175
    instance-of v4, v2, Lcom/baidu/mobstat/df;

    const/16 v5, 0x3ea

    if-nez v4, :cond_3

    const-string v2, "wrong http function"

    .line 176
    invoke-virtual {p0, v5, v2, v1}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    return v1

    .line 179
    :cond_3
    check-cast v2, Lcom/baidu/mobstat/df;

    .line 180
    iget-object v4, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    iget-object v6, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    invoke-virtual {v4, v6, v2}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/cy;Lcom/baidu/mobstat/df;)Lcom/baidu/mobstat/cj$b;

    move-result-object v4

    .line 181
    sget-object v6, Lcom/baidu/mobstat/cj$b;->a:Lcom/baidu/mobstat/cj$b;
    :try_end_3
    .catch Lcom/baidu/mobstat/cp; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/baidu/mobstat/cm; {:try_start_3 .. :try_end_3} :catch_4

    if-ne v4, v6, :cond_4

    .line 183
    :try_start_4
    iget-object v4, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    iget-object v5, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    invoke-interface {v4, p0, v5, v2}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cy;Lcom/baidu/mobstat/df;)V
    :try_end_4
    .catch Lcom/baidu/mobstat/cn; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/baidu/mobstat/cp; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/baidu/mobstat/cm; {:try_start_4 .. :try_end_4} :catch_4

    .line 192
    :try_start_5
    invoke-direct {p0, v2}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/dd;)V

    return v3

    :catch_1
    move-exception v2

    .line 188
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v3, p0, v2}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    const/4 v3, -0x1

    .line 189
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v2, v1}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    return v1

    :catch_2
    move-exception v2

    .line 185
    invoke-virtual {v2}, Lcom/baidu/mobstat/cn;->a()I

    move-result v3

    invoke-virtual {v2}, Lcom/baidu/mobstat/cn;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v2, v1}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    return v1

    .line 195
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "draft "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " refuses handshake"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v5, v2}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;)V
    :try_end_5
    .catch Lcom/baidu/mobstat/cp; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/baidu/mobstat/cm; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_3

    :catch_3
    move-exception v2

    .line 199
    :try_start_6
    invoke-virtual {p0, v2}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cn;)V
    :try_end_6
    .catch Lcom/baidu/mobstat/cm; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    :catch_4
    move-exception v2

    .line 202
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-nez v3, :cond_8

    .line 203
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    .line 204
    invoke-virtual {v2}, Lcom/baidu/mobstat/cm;->a()I

    move-result v3

    if-nez v3, :cond_5

    .line 206
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    add-int/lit8 v3, v0, 0x10

    goto :goto_2

    .line 208
    :cond_5
    sget-boolean v4, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v4, :cond_7

    invoke-virtual {v2}, Lcom/baidu/mobstat/cm;->a()I

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lt v2, v0, :cond_6

    goto :goto_2

    :cond_6
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 210
    :cond_7
    :goto_2
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    .line 212
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_3

    .line 215
    :cond_8
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 216
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_9
    :goto_3
    return v1
.end method

.method private d(Ljava/nio/ByteBuffer;)V
    .locals 7

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/cj;->c(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object p1

    .line 227
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobstat/cw;

    .line 228
    sget-boolean v1, Lcom/baidu/mobstat/cg;->c:Z

    if-eqz v1, :cond_0

    .line 229
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "matched frame: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 230
    :cond_0
    invoke-interface {v0}, Lcom/baidu/mobstat/cw;->f()Lcom/baidu/mobstat/cw$a;

    move-result-object v1

    .line 231
    invoke-interface {v0}, Lcom/baidu/mobstat/cw;->d()Z

    move-result v2

    .line 233
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v4, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    if-ne v3, v4, :cond_1

    return-void

    .line 236
    :cond_1
    sget-object v3, Lcom/baidu/mobstat/cw$a;->f:Lcom/baidu/mobstat/cw$a;

    const/4 v4, 0x0

    if-ne v1, v3, :cond_5

    const/16 v1, 0x3ed

    const-string v2, ""

    .line 239
    instance-of v3, v0, Lcom/baidu/mobstat/ct;

    if-eqz v3, :cond_2

    .line 240
    check-cast v0, Lcom/baidu/mobstat/ct;

    .line 241
    invoke-interface {v0}, Lcom/baidu/mobstat/ct;->a()I

    move-result v1

    .line 242
    invoke-interface {v0}, Lcom/baidu/mobstat/ct;->b()Ljava/lang/String;

    move-result-object v2

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v3, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    const/4 v5, 0x1

    if-ne v0, v3, :cond_3

    .line 246
    invoke-virtual {p0, v1, v2, v5}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;Z)V

    goto :goto_0

    .line 249
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cj;->b()Lcom/baidu/mobstat/cj$a;

    move-result-object v0

    sget-object v3, Lcom/baidu/mobstat/cj$a;->c:Lcom/baidu/mobstat/cj$a;

    if-ne v0, v3, :cond_4

    .line 250
    invoke-direct {p0, v1, v2, v5}, Lcom/baidu/mobstat/cg;->c(ILjava/lang/String;Z)V

    goto :goto_0

    .line 252
    :cond_4
    invoke-virtual {p0, v1, v2, v4}, Lcom/baidu/mobstat/cg;->b(ILjava/lang/String;Z)V

    goto :goto_0

    .line 255
    :cond_5
    sget-object v3, Lcom/baidu/mobstat/cw$a;->d:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v3, :cond_6

    .line 256
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->b(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V

    goto :goto_0

    .line 258
    :cond_6
    sget-object v3, Lcom/baidu/mobstat/cw$a;->e:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v3, :cond_7

    .line 259
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->c(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V

    goto :goto_0

    :cond_7
    const/16 v3, 0x3ea

    if-eqz v2, :cond_c

    .line 261
    sget-object v5, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v5, :cond_8

    goto :goto_1

    .line 303
    :cond_8
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    if-nez v2, :cond_b

    .line 305
    sget-object v2, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;
    :try_end_0
    .catch Lcom/baidu/mobstat/cn; {:try_start_0 .. :try_end_0} :catch_3

    if-ne v1, v2, :cond_9

    .line 307
    :try_start_1
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobstat/di;->a(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/baidu/mobstat/cn; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    :catch_0
    move-exception v0

    .line 309
    :try_start_2
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 311
    :cond_9
    sget-object v2, Lcom/baidu/mobstat/cw$a;->c:Lcom/baidu/mobstat/cw$a;
    :try_end_2
    .catch Lcom/baidu/mobstat/cn; {:try_start_2 .. :try_end_2} :catch_3

    if-ne v1, v2, :cond_a

    .line 313
    :try_start_3
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/nio/ByteBuffer;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/baidu/mobstat/cn; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_1
    move-exception v0

    .line 315
    :try_start_4
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 318
    :cond_a
    new-instance p1, Lcom/baidu/mobstat/cn;

    const-string v0, "non control or continious frame expected"

    invoke-direct {p1, v3, v0}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1

    .line 304
    :cond_b
    new-instance p1, Lcom/baidu/mobstat/cn;

    const-string v0, "Continuous frame sequence not completed."

    invoke-direct {p1, v3, v0}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1

    .line 262
    :cond_c
    :goto_1
    sget-object v5, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    const/16 v6, 0x3ef

    if-eq v1, v5, :cond_e

    .line 263
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    if-nez v2, :cond_d

    .line 265
    iput-object v0, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    goto :goto_3

    .line 264
    :cond_d
    new-instance p1, Lcom/baidu/mobstat/cn;

    const-string v0, "Previous continuous frame sequence not completed."

    invoke-direct {p1, v3, v0}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_4
    .catch Lcom/baidu/mobstat/cn; {:try_start_4 .. :try_end_4} :catch_3

    :cond_e
    const-string v5, "Continuous frame sequence was not started."

    if-eqz v2, :cond_12

    .line 267
    :try_start_5
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    if-eqz v2, :cond_11

    .line 270
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v2}, Lcom/baidu/mobstat/cw;->f()Lcom/baidu/mobstat/cw$a;

    move-result-object v2

    sget-object v3, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    if-ne v2, v3, :cond_10

    .line 272
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v2}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    add-int/lit8 v2, v2, -0x40

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 273
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v3, v0}, Lcom/baidu/mobstat/cw;->a(Lcom/baidu/mobstat/cw;)V

    .line 274
    iget-object v3, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v3}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/baidu/mobstat/di;->a(Ljava/nio/ByteBuffer;I)Z

    move-result v2

    if-eqz v2, :cond_f

    goto :goto_2

    .line 275
    :cond_f
    new-instance p1, Lcom/baidu/mobstat/cn;

    invoke-direct {p1, v6}, Lcom/baidu/mobstat/cn;-><init>(I)V

    throw p1

    :cond_10
    :goto_2
    const/4 v2, 0x0

    .line 278
    iput-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    goto :goto_3

    .line 268
    :cond_11
    new-instance p1, Lcom/baidu/mobstat/cn;

    invoke-direct {p1, v3, v5}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1

    .line 279
    :cond_12
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    if-eqz v2, :cond_17

    .line 283
    :goto_3
    sget-object v2, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v2, :cond_14

    .line 284
    invoke-interface {v0}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {v2}, Lcom/baidu/mobstat/di;->b(Ljava/nio/ByteBuffer;)Z

    move-result v2

    if-eqz v2, :cond_13

    goto :goto_4

    .line 285
    :cond_13
    new-instance p1, Lcom/baidu/mobstat/cn;

    invoke-direct {p1, v6}, Lcom/baidu/mobstat/cn;-><init>(I)V

    throw p1

    .line 289
    :cond_14
    :goto_4
    sget-object v2, Lcom/baidu/mobstat/cw$a;->a:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v2, :cond_16

    iget-object v1, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v1}, Lcom/baidu/mobstat/cw;->f()Lcom/baidu/mobstat/cw$a;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobstat/cw$a;->b:Lcom/baidu/mobstat/cw$a;

    if-ne v1, v2, :cond_16

    .line 291
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v1}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, -0x40

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 292
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v2, v0}, Lcom/baidu/mobstat/cw;->a(Lcom/baidu/mobstat/cw;)V

    .line 293
    iget-object v2, p0, Lcom/baidu/mobstat/cg;->n:Lcom/baidu/mobstat/cw;

    invoke-interface {v2}, Lcom/baidu/mobstat/cw;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/baidu/mobstat/di;->a(Ljava/nio/ByteBuffer;I)Z

    move-result v1

    if-eqz v1, :cond_15

    goto :goto_5

    .line 294
    :cond_15
    new-instance p1, Lcom/baidu/mobstat/cn;

    invoke-direct {p1, v6}, Lcom/baidu/mobstat/cn;-><init>(I)V

    throw p1
    :try_end_5
    .catch Lcom/baidu/mobstat/cn; {:try_start_5 .. :try_end_5} :catch_3

    .line 298
    :cond_16
    :goto_5
    :try_start_6
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cw;)V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/baidu/mobstat/cn; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    :catch_2
    move-exception v0

    .line 300
    :try_start_7
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 280
    :cond_17
    new-instance p1, Lcom/baidu/mobstat/cn;

    invoke-direct {p1, v3, v5}, Lcom/baidu/mobstat/cn;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_7
    .catch Lcom/baidu/mobstat/cn; {:try_start_7 .. :try_end_7} :catch_3

    :cond_18
    return-void

    :catch_3
    move-exception p1

    .line 322
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 323
    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/cg;->a(Lcom/baidu/mobstat/cn;)V

    return-void
.end method

.method private e(Ljava/nio/ByteBuffer;)Lcom/baidu/mobstat/cj$b;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cm;
        }
    .end annotation

    .line 527
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 528
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sget-object v1, Lcom/baidu/mobstat/cj;->c:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 529
    sget-object p1, Lcom/baidu/mobstat/cj$b;->b:Lcom/baidu/mobstat/cj$b;

    return-object p1

    .line 530
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sget-object v1, Lcom/baidu/mobstat/cj;->c:[B

    array-length v1, v1

    if-lt v0, v1, :cond_3

    const/4 v0, 0x0

    .line 534
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 535
    sget-object v1, Lcom/baidu/mobstat/cj;->c:[B

    aget-byte v1, v1, v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    if-eq v1, v2, :cond_1

    .line 536
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    .line 537
    sget-object p1, Lcom/baidu/mobstat/cj$b;->b:Lcom/baidu/mobstat/cj$b;

    return-object p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_2
    sget-object p1, Lcom/baidu/mobstat/cj$b;->a:Lcom/baidu/mobstat/cj$b;

    return-object p1

    .line 531
    :cond_3
    new-instance p1, Lcom/baidu/mobstat/cm;

    sget-object v0, Lcom/baidu/mobstat/cj;->c:[B

    array-length v0, v0

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/cm;-><init>(I)V

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method private f(Ljava/nio/ByteBuffer;)V
    .locals 4

    .line 569
    sget-boolean v0, Lcom/baidu/mobstat/cg;->c:Z

    if-eqz v0, :cond_1

    .line 570
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "write("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "): {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_0

    const-string v2, "too big to display"

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 580
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {p1, p0}, Lcom/baidu/mobstat/ch;->b(Lcom/baidu/mobstat/ce;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/net/InetSocketAddress;
    .locals 1

    .line 649
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0}, Lcom/baidu/mobstat/ch;->c(Lcom/baidu/mobstat/ce;)Ljava/net/InetSocketAddress;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    .line 464
    invoke-direct {p0, p1, v0, v1}, Lcom/baidu/mobstat/cg;->c(ILjava/lang/String;Z)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 370
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobstat/cg;->c(ILjava/lang/String;Z)V

    return-void
.end method

.method protected declared-synchronized a(ILjava/lang/String;Z)V
    .locals 2

    monitor-enter p0

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->e:Lcom/baidu/mobstat/ce$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 385
    monitor-exit p0

    return-void

    .line 388
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->f:Ljava/nio/channels/SelectionKey;

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->f:Ljava/nio/channels/SelectionKey;

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->g:Ljava/nio/channels/ByteChannel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 394
    :try_start_2
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->g:Ljava/nio/channels/ByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 396
    :try_start_3
    iget-object v1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400
    :cond_2
    :goto_0
    :try_start_4
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;ILjava/lang/String;Z)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p1

    .line 402
    :try_start_5
    iget-object p2, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {p2, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 404
    :goto_1
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    if-eqz p1, :cond_3

    .line 405
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {p1}, Lcom/baidu/mobstat/cj;->a()V

    :cond_3
    const/4 p1, 0x0

    .line 406
    iput-object p1, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    .line 408
    sget-object p1, Lcom/baidu/mobstat/ce$a;->e:Lcom/baidu/mobstat/ce$a;

    iput-object p1, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    .line 409
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->clear()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 410
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected a(IZ)V
    .locals 1

    const-string v0, ""

    .line 413
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;Z)V

    return-void
.end method

.method public a(Lcom/baidu/mobstat/cn;)V
    .locals 2

    .line 468
    invoke-virtual {p1}, Lcom/baidu/mobstat/cn;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/baidu/mobstat/cn;->getMessage()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/baidu/mobstat/cg;->c(ILjava/lang/String;Z)V

    return-void
.end method

.method public a(Lcom/baidu/mobstat/cw;)V
    .locals 3

    .line 516
    sget-boolean v0, Lcom/baidu/mobstat/cg;->c:Z

    if-eqz v0, :cond_0

    .line 517
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send frame: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/cw;)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->f(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method public a(Lcom/baidu/mobstat/cz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/cp;
        }
    .end annotation

    .line 545
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->b:Lcom/baidu/mobstat/ce$a;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "shall only be called once"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 548
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/cz;)Lcom/baidu/mobstat/cz;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    .line 550
    invoke-interface {p1}, Lcom/baidu/mobstat/cz;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobstat/cg;->t:Ljava/lang/String;

    .line 551
    sget-boolean p1, Lcom/baidu/mobstat/cg;->h:Z

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/baidu/mobstat/cg;->t:Ljava/lang/String;

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 555
    :cond_3
    :goto_1
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    invoke-interface {p1, p0, v0}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Lcom/baidu/mobstat/cy;)V
    :try_end_0
    .catch Lcom/baidu/mobstat/cn; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 565
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;

    iget-object v1, p0, Lcom/baidu/mobstat/cg;->m:Lcom/baidu/mobstat/ce$b;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobstat/cj;->a(Lcom/baidu/mobstat/dd;Lcom/baidu/mobstat/ce$b;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->a(Ljava/util/List;)V

    return-void

    :catch_0
    move-exception p1

    .line 560
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 561
    new-instance v0, Lcom/baidu/mobstat/cp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rejected because of"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/baidu/mobstat/cp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :catch_1
    new-instance p1, Lcom/baidu/mobstat/cp;

    const-string v0, "Handshake data rejected by client."

    invoke-direct {p1, v0}, Lcom/baidu/mobstat/cp;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .line 114
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 116
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/baidu/mobstat/cg;->c:Z

    if-eqz v0, :cond_3

    .line 117
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "process("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "): {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_2

    const-string v2, "too big to display"

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([BII)V

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->a:Lcom/baidu/mobstat/ce$a;

    if-eq v0, v1, :cond_4

    .line 120
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->d(Ljava/nio/ByteBuffer;)V

    goto :goto_3

    .line 122
    :cond_4
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->c(Ljava/nio/ByteBuffer;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 123
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-ne v0, v1, :cond_6

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_2

    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 125
    :cond_6
    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 126
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->d(Ljava/nio/ByteBuffer;)V

    goto :goto_3

    .line 127
    :cond_7
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 128
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->o:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/cg;->d(Ljava/nio/ByteBuffer;)V

    .line 132
    :cond_8
    :goto_3
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/baidu/mobstat/cg;->d()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/baidu/mobstat/cg;->e()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result p1

    if-nez p1, :cond_9

    goto :goto_4

    :cond_9
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_a
    :goto_4
    return-void
.end method

.method public a([B)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Lcom/baidu/mobstat/cs;
        }
    .end annotation

    .line 498
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobstat/cg;->b(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method public b()V
    .locals 4

    .line 449
    invoke-virtual {p0}, Lcom/baidu/mobstat/cg;->g()Lcom/baidu/mobstat/ce$a;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobstat/ce$a;->a:Lcom/baidu/mobstat/ce$a;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    .line 450
    invoke-virtual {p0, v0, v2}, Lcom/baidu/mobstat/cg;->a(IZ)V

    goto :goto_0

    .line 451
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->r:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/baidu/mobstat/cg;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobstat/cg;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;Z)V

    goto :goto_0

    .line 453
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cj;->b()Lcom/baidu/mobstat/cj$a;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobstat/cj$a;->a:Lcom/baidu/mobstat/cj$a;

    const/16 v3, 0x3e8

    if-ne v0, v1, :cond_2

    .line 454
    invoke-virtual {p0, v3, v2}, Lcom/baidu/mobstat/cg;->a(IZ)V

    goto :goto_0

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {v0}, Lcom/baidu/mobstat/cj;->b()Lcom/baidu/mobstat/cj$a;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobstat/cj$a;->b:Lcom/baidu/mobstat/cj$a;

    if-ne v0, v1, :cond_3

    .line 456
    invoke-virtual {p0, v3, v2}, Lcom/baidu/mobstat/cg;->a(IZ)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x3ee

    .line 458
    invoke-virtual {p0, v0, v2}, Lcom/baidu/mobstat/cg;->a(IZ)V

    :goto_0
    return-void
.end method

.method public b(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 424
    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/mobstat/cg;->a(ILjava/lang/String;Z)V

    return-void
.end method

.method protected declared-synchronized b(ILjava/lang/String;Z)V
    .locals 1

    monitor-enter p0

    .line 428
    :try_start_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 429
    monitor-exit p0

    return-void

    .line 431
    :cond_0
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->r:Ljava/lang/Integer;

    .line 432
    iput-object p2, p0, Lcom/baidu/mobstat/cg;->q:Ljava/lang/String;

    .line 433
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/cg;->s:Ljava/lang/Boolean;

    const/4 v0, 0x1

    .line 435
    iput-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z

    .line 437
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0}, Lcom/baidu/mobstat/ch;->b(Lcom/baidu/mobstat/ce;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    :try_start_2
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/baidu/mobstat/ch;->b(Lcom/baidu/mobstat/ce;ILjava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 441
    :try_start_3
    iget-object p2, p0, Lcom/baidu/mobstat/cg;->i:Lcom/baidu/mobstat/ch;

    invoke-interface {p2, p0, p1}, Lcom/baidu/mobstat/ch;->a(Lcom/baidu/mobstat/ce;Ljava/lang/Exception;)V

    .line 443
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    if-eqz p1, :cond_1

    .line 444
    iget-object p1, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    invoke-virtual {p1}, Lcom/baidu/mobstat/cj;->a()V

    :cond_1
    const/4 p1, 0x0

    .line 445
    iput-object p1, p0, Lcom/baidu/mobstat/cg;->p:Lcom/baidu/mobstat/cy;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 446
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public b(Ljava/nio/ByteBuffer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Lcom/baidu/mobstat/cs;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 493
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->l:Lcom/baidu/mobstat/cj;

    iget-object v1, p0, Lcom/baidu/mobstat/cg;->m:Lcom/baidu/mobstat/ce$b;

    sget-object v2, Lcom/baidu/mobstat/ce$b;->a:Lcom/baidu/mobstat/ce$b;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/baidu/mobstat/cj;->a(Ljava/nio/ByteBuffer;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/baidu/mobstat/cg;->a(Ljava/util/Collection;)V

    return-void

    .line 492
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot send \'null\' data to a WebSocketImpl."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()Z
    .locals 2

    .line 608
    sget-boolean v0, Lcom/baidu/mobstat/cg;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->c:Lcom/baidu/mobstat/ce$a;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 609
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->c:Lcom/baidu/mobstat/ce$a;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public d()Z
    .locals 2

    .line 614
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->d:Lcom/baidu/mobstat/ce$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Z
    .locals 1

    .line 619
    iget-boolean v0, p0, Lcom/baidu/mobstat/cg;->j:Z

    return v0
.end method

.method public f()Z
    .locals 2

    .line 624
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    sget-object v1, Lcom/baidu/mobstat/ce$a;->e:Lcom/baidu/mobstat/ce$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Lcom/baidu/mobstat/ce$a;
    .locals 1

    .line 629
    iget-object v0, p0, Lcom/baidu/mobstat/cg;->k:Lcom/baidu/mobstat/ce$a;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 634
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 639
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
