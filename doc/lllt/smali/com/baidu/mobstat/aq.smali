.class Lcom/baidu/mobstat/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/aq$c;,
        Lcom/baidu/mobstat/aq$b;,
        Lcom/baidu/mobstat/aq$a;
    }
.end annotation


# static fields
.field private static final c:Ljava/nio/ByteBuffer;


# instance fields
.field private a:Lcom/baidu/mobstat/aq$a;

.field private b:Lcom/baidu/mobstat/aq$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobstat/aq;->c:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public constructor <init>(Ljava/net/URI;Lcom/baidu/mobstat/aq$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/baidu/mobstat/aq$c;
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p2, p0, Lcom/baidu/mobstat/aq;->a:Lcom/baidu/mobstat/aq$a;

    const/4 p2, 0x0

    .line 61
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wss://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/baidu/mobstat/aq;->c()Ljava/net/Socket;

    move-result-object p2

    .line 65
    :cond_0
    new-instance v0, Lcom/baidu/mobstat/aq$b;

    const/16 v1, 0x1388

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/baidu/mobstat/aq$b;-><init>(Lcom/baidu/mobstat/aq;Ljava/net/URI;ILjava/net/Socket;)V

    iput-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    .line 66
    iget-object p1, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    invoke-virtual {p1}, Lcom/baidu/mobstat/aq$b;->c()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 68
    new-instance p2, Lcom/baidu/mobstat/aq$c;

    invoke-direct {p2, p0, p1}, Lcom/baidu/mobstat/aq$c;-><init>(Lcom/baidu/mobstat/aq;Ljava/lang/Throwable;)V

    throw p2
.end method

.method static synthetic a(Lcom/baidu/mobstat/aq;)Lcom/baidu/mobstat/aq$a;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/baidu/mobstat/aq;->a:Lcom/baidu/mobstat/aq$a;

    return-object p0
.end method

.method private c()Ljava/net/Socket;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "TLS"

    .line 35
    invoke-static {v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 36
    invoke-virtual {v1, v0, v0, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 37
    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_0

    return-object v0

    .line 48
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/baidu/mobstat/aq$b;->d()V

    :cond_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/NotYetConnectedException;
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/aq$b;->a([B)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    invoke-virtual {v0}, Lcom/baidu/mobstat/aq$b;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    invoke-virtual {v0}, Lcom/baidu/mobstat/aq$b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobstat/aq;->b:Lcom/baidu/mobstat/aq$b;

    invoke-virtual {v0}, Lcom/baidu/mobstat/aq$b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
