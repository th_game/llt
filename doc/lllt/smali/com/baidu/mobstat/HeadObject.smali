.class public Lcom/baidu/mobstat/HeadObject;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field A:Lorg/json/JSONObject;

.field B:Lorg/json/JSONObject;

.field C:Ljava/lang/String;

.field a:Z

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:I

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:I

.field k:I

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field o:Ljava/lang/String;

.field p:Ljava/lang/String;

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Ljava/lang/String;

.field t:Ljava/lang/String;

.field u:Ljava/lang/String;

.field v:Ljava/lang/String;

.field w:Ljava/lang/String;

.field x:Ljava/lang/String;

.field y:Ljava/lang/String;

.field z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/baidu/mobstat/HeadObject;->a:Z

    const-string v0, "0"

    .line 21
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->e:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->f:Ljava/lang/String;

    const/4 v1, -0x1

    .line 25
    iput v1, p0, Lcom/baidu/mobstat/HeadObject;->g:I

    .line 30
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->l:Ljava/lang/String;

    return-void
.end method

.method private declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    monitor-enter p0

    .line 76
    :try_start_0
    iget-boolean v0, p0, Lcom/baidu/mobstat/HeadObject;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 77
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "android.permission.READ_PHONE_STATE"

    .line 80
    invoke-static {p1, v0}, Lcom/baidu/mobstat/bv;->e(Landroid/content/Context;Ljava/lang/String;)Z

    const-string v0, "android.permission.INTERNET"

    .line 81
    invoke-static {p1, v0}, Lcom/baidu/mobstat/bv;->e(Landroid/content/Context;Ljava/lang/String;)Z

    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    .line 82
    invoke-static {p1, v0}, Lcom/baidu/mobstat/bv;->e(Landroid/content/Context;Ljava/lang/String;)Z

    const-string v0, "android.permission.WRITE_SETTINGS"

    .line 83
    invoke-static {p1, v0}, Lcom/baidu/mobstat/bv;->e(Landroid/content/Context;Ljava/lang/String;)Z

    const-string v0, "phone"

    .line 85
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 87
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/CooperService;->getOSVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->b:Ljava/lang/String;

    .line 88
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/CooperService;->getOSSysVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->c:Ljava/lang/String;

    .line 89
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/CooperService;->getPhoneModel()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->n:Ljava/lang/String;

    .line 90
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/CooperService;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->o:Ljava/lang/String;

    .line 92
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobstat/CooperService;->getUUID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->z:Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobstat/CooperService;->getHeaderExt(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->A:Lorg/json/JSONObject;

    .line 95
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobstat/CooperService;->getPushId(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->B:Lorg/json/JSONObject;

    .line 96
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/baidu/mobstat/CooperService;->getDeviceId(Landroid/telephony/TelephonyManager;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->i:Ljava/lang/String;

    .line 97
    invoke-static {}, Lcom/baidu/mobstat/BasicStoreTools;->getInstance()Lcom/baidu/mobstat/BasicStoreTools;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobstat/BasicStoreTools;->getForTV(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "1"

    goto :goto_0

    :cond_1
    const-string v1, "0"

    :goto_0
    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    .line 99
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->w(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "2"

    .line 100
    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    .line 104
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :try_start_2
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobstat/CooperService;->isDeviceMacEnabled(Landroid/content/Context;)Z

    move-result v1

    .line 109
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/baidu/mobstat/CooperService;->getMacAddress(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobstat/HeadObject;->s:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    const/4 v1, 0x1

    .line 115
    :try_start_3
    invoke-static {v1, p1}, Lcom/baidu/mobstat/cc;->f(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobstat/HeadObject;->u:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    :catch_1
    :try_start_4
    invoke-static {p1, v1}, Lcom/baidu/mobstat/cc;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobstat/HeadObject;->v:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 126
    :catch_2
    :try_start_5
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/baidu/mobstat/CooperService;->getCUID(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobstat/HeadObject;->f:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 133
    :try_start_6
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/baidu/mobstat/CooperService;->getOperator(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->m:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 139
    :catch_3
    :try_start_7
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    .line 140
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->d(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 142
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 144
    iget v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    iget v2, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    xor-int/2addr v0, v2

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    .line 145
    iget v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    iget v2, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    xor-int/2addr v0, v2

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    .line 146
    iget v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    iget v2, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    xor-int/2addr v0, v2

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->j:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 152
    :catch_4
    :cond_3
    :try_start_8
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getAppChannel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->l:Ljava/lang/String;

    .line 153
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getAppKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->e:Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 156
    :try_start_9
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getAppVersionCode(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobstat/HeadObject;->g:I

    .line 157
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->h:Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 163
    :catch_5
    :try_start_a
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->checkCellLocationSetting(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 164
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->p:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string v0, "0_0_0"

    .line 166
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->p:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 173
    :catch_6
    :goto_1
    :try_start_b
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->checkGPSLocationSetting(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 174
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->q:Ljava/lang/String;

    goto :goto_2

    :cond_5
    const-string v0, ""

    .line 176
    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->q:Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 183
    :catch_7
    :goto_2
    :try_start_c
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getLinkedWay(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->r:Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 189
    :catch_8
    :try_start_d
    invoke-static {}, Lcom/baidu/mobstat/cc;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->w:Ljava/lang/String;

    .line 192
    sget-object v0, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->x:Ljava/lang/String;

    .line 193
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v0, p0, Lcom/baidu/mobstat/HeadObject;->y:Ljava/lang/String;

    .line 195
    invoke-static {}, Lcom/baidu/mobstat/CooperService;->instance()Lcom/baidu/mobstat/CooperService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobstat/CooperService;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobstat/HeadObject;->C:Ljava/lang/String;

    .line 197
    iput-boolean v1, p0, Lcom/baidu/mobstat/HeadObject;->a:Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 198
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public declared-synchronized installHeader(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 2

    monitor-enter p0

    .line 62
    :try_start_0
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/HeadObject;->a(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p2}, Lorg/json/JSONObject;->length()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 66
    monitor-exit p0

    return-void

    .line 69
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobstat/HeadObject;->updateHeader(Landroid/content/Context;Lorg/json/JSONObject;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setHeaderExt(Lorg/json/JSONObject;)V
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/baidu/mobstat/HeadObject;->A:Lorg/json/JSONObject;

    return-void
.end method

.method public setPushInfo(Lorg/json/JSONObject;)V
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/baidu/mobstat/HeadObject;->B:Lorg/json/JSONObject;

    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/baidu/mobstat/HeadObject;->C:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized updateHeader(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v0, "o"

    const-string v1, "Android"

    .line 202
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "st"

    const/4 v1, 0x0

    .line 203
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "s"

    .line 204
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->b:Ljava/lang/String;

    if-nez v2, :cond_0

    const-string v2, ""

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sv"

    .line 205
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, ""

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "k"

    .line 206
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->e:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v2, ""

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->e:Ljava/lang/String;

    :goto_2
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "pt"

    .line 207
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, "0"

    goto :goto_3

    :cond_3
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->d:Ljava/lang/String;

    :goto_3
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "i"

    const-string v2, ""

    .line 208
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "v"

    const-string v2, "3.9.2.0"

    .line 209
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    .line 210
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "a"

    .line 211
    iget v1, p0, Lcom/baidu/mobstat/HeadObject;->g:I

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "n"

    .line 212
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->h:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, ""

    goto :goto_4

    :cond_4
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->h:Ljava/lang/String;

    :goto_4
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "d"

    const-string v1, ""

    .line 213
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "mc"

    .line 214
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->s:Ljava/lang/String;

    if-nez v1, :cond_5

    const-string v1, ""

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->s:Ljava/lang/String;

    :goto_5
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "bm"

    .line 215
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->u:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, ""

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->u:Ljava/lang/String;

    :goto_6
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "dd"

    .line 216
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->i:Ljava/lang/String;

    if-nez v1, :cond_7

    const-string v1, ""

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->i:Ljava/lang/String;

    :goto_7
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ii"

    .line 217
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->f:Ljava/lang/String;

    if-nez v1, :cond_8

    const-string v1, ""

    goto :goto_8

    :cond_8
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->f:Ljava/lang/String;

    :goto_8
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "tg"

    const/4 v1, 0x1

    .line 218
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "w"

    .line 219
    iget v2, p0, Lcom/baidu/mobstat/HeadObject;->j:I

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "h"

    .line 220
    iget v2, p0, Lcom/baidu/mobstat/HeadObject;->k:I

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "dn"

    .line 221
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->v:Ljava/lang/String;

    if-nez v2, :cond_9

    const-string v2, ""

    goto :goto_9

    :cond_9
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->v:Ljava/lang/String;

    :goto_9
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "c"

    .line 222
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->l:Ljava/lang/String;

    if-nez v2, :cond_a

    const-string v2, ""

    goto :goto_a

    :cond_a
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->l:Ljava/lang/String;

    :goto_a
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "op"

    .line 223
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->m:Ljava/lang/String;

    if-nez v2, :cond_b

    const-string v2, ""

    goto :goto_b

    :cond_b
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->m:Ljava/lang/String;

    :goto_b
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "m"

    .line 224
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->n:Ljava/lang/String;

    if-nez v2, :cond_c

    const-string v2, ""

    goto :goto_c

    :cond_c
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->n:Ljava/lang/String;

    :goto_c
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ma"

    .line 225
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->o:Ljava/lang/String;

    if-nez v2, :cond_d

    const-string v2, ""

    goto :goto_d

    :cond_d
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->o:Ljava/lang/String;

    :goto_d
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "cl"

    .line 226
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->p:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "gl"

    .line 227
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->q:Ljava/lang/String;

    if-nez v2, :cond_e

    const-string v2, ""

    goto :goto_e

    :cond_e
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->q:Ljava/lang/String;

    :goto_e
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "l"

    .line 228
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->r:Ljava/lang/String;

    if-nez v2, :cond_f

    const-string v2, ""

    goto :goto_f

    :cond_f
    iget-object v2, p0, Lcom/baidu/mobstat/HeadObject;->r:Ljava/lang/String;

    :goto_f
    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "t"

    .line 229
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "pn"

    .line 230
    invoke-static {v1, p1}, Lcom/baidu/mobstat/cc;->h(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "rom"

    .line 231
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->w:Ljava/lang/String;

    if-nez v1, :cond_10

    const-string v1, ""

    goto :goto_10

    :cond_10
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->w:Ljava/lang/String;

    :goto_10
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "bo"

    .line 232
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->x:Ljava/lang/String;

    if-nez v1, :cond_11

    const-string v1, ""

    goto :goto_11

    :cond_11
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->x:Ljava/lang/String;

    :goto_11
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "bd"

    .line 233
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->y:Ljava/lang/String;

    if-nez v1, :cond_12

    const-string v1, ""

    goto :goto_12

    :cond_12
    iget-object v1, p0, Lcom/baidu/mobstat/HeadObject;->y:Ljava/lang/String;

    :goto_12
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "td"

    .line 234
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "at"

    const-string v1, "0"

    .line 240
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 243
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->u(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "pl"

    .line 244
    invoke-virtual {p2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const/4 v1, 0x0

    .line 248
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 249
    invoke-static {p1}, Lcom/baidu/mobstat/cc;->v(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :cond_13
    const-string p1, "scl"

    if-nez v1, :cond_14

    const-string v1, ""

    .line 251
    :cond_14
    invoke-virtual {p2, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "sign"

    .line 253
    iget-object v0, p0, Lcom/baidu/mobstat/HeadObject;->z:Ljava/lang/String;

    if-nez v0, :cond_15

    const-string v0, ""

    goto :goto_13

    :cond_15
    iget-object v0, p0, Lcom/baidu/mobstat/HeadObject;->z:Ljava/lang/String;

    :goto_13
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 256
    iget-object p1, p0, Lcom/baidu/mobstat/HeadObject;->A:Lorg/json/JSONObject;

    if-eqz p1, :cond_16

    iget-object p1, p0, Lcom/baidu/mobstat/HeadObject;->A:Lorg/json/JSONObject;

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result p1

    if-eqz p1, :cond_16

    const-string p1, "ext"

    .line 257
    iget-object v0, p0, Lcom/baidu/mobstat/HeadObject;->A:Lorg/json/JSONObject;

    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_14

    :cond_16
    const-string p1, "ext"

    .line 259
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 263
    :goto_14
    iget-object p1, p0, Lcom/baidu/mobstat/HeadObject;->B:Lorg/json/JSONObject;

    if-nez p1, :cond_17

    .line 264
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobstat/HeadObject;->B:Lorg/json/JSONObject;

    :cond_17
    const-string p1, "push"

    .line 266
    iget-object v0, p0, Lcom/baidu/mobstat/HeadObject;->B:Lorg/json/JSONObject;

    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "uid"

    .line 268
    iget-object v0, p0, Lcom/baidu/mobstat/HeadObject;->C:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_15

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 272
    :catch_0
    :goto_15
    monitor-exit p0

    return-void
.end method
