.class Lcom/baidu/mobstat/bm$a$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/baidu/mobstat/bm$a;->a(Ljava/lang/ref/WeakReference;Lorg/json/JSONObject;Lcom/baidu/mobstat/br;Landroid/os/Handler;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/ref/WeakReference;

.field final synthetic b:Z

.field final synthetic c:Lcom/baidu/mobstat/br;

.field final synthetic d:Lorg/json/JSONObject;

.field final synthetic e:Lcom/baidu/mobstat/bm$a;


# direct methods
.method constructor <init>(Lcom/baidu/mobstat/bm$a;Ljava/lang/ref/WeakReference;ZLcom/baidu/mobstat/br;Lorg/json/JSONObject;)V
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/baidu/mobstat/bm$a$1;->e:Lcom/baidu/mobstat/bm$a;

    iput-object p2, p0, Lcom/baidu/mobstat/bm$a$1;->a:Ljava/lang/ref/WeakReference;

    iput-boolean p3, p0, Lcom/baidu/mobstat/bm$a$1;->b:Z

    iput-object p4, p0, Lcom/baidu/mobstat/bm$a$1;->c:Lcom/baidu/mobstat/br;

    iput-object p5, p0, Lcom/baidu/mobstat/bm$a$1;->d:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 277
    invoke-static {}, Lcom/baidu/mobstat/bg;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 278
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bk;->b()Z

    move-result v0

    const-string v1, "no touch, skip doViewVisit"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobstat/bm$a$1;->e:Lcom/baidu/mobstat/bm$a;

    invoke-static {v0}, Lcom/baidu/mobstat/bm$a;->a(Lcom/baidu/mobstat/bm$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-static {}, Lcom/baidu/mobstat/bk;->c()Lcom/baidu/mobstat/bk;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bk;->a(Ljava/lang/String;)V

    .line 281
    :cond_0
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobstat/bo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    invoke-static {}, Lcom/baidu/mobstat/bo;->c()Lcom/baidu/mobstat/bo;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobstat/bo;->a(Ljava/lang/String;)V

    :cond_1
    return-void

    .line 288
    :cond_2
    invoke-static {}, Lcom/baidu/mobstat/bm;->c()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_3

    const/4 v0, 0x0

    .line 289
    invoke-static {v0}, Lcom/baidu/mobstat/bg;->a(Z)V

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobstat/bm$a$1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 294
    iget-boolean v1, p0, Lcom/baidu/mobstat/bm$a$1;->b:Z

    invoke-static {v0, v1}, Lcom/baidu/mobstat/ay;->c(Landroid/app/Activity;Z)V

    .line 295
    iget-object v1, p0, Lcom/baidu/mobstat/bm$a$1;->c:Lcom/baidu/mobstat/br;

    iget-object v2, p0, Lcom/baidu/mobstat/bm$a$1;->d:Lorg/json/JSONObject;

    iget-boolean v3, p0, Lcom/baidu/mobstat/bm$a$1;->b:Z

    invoke-virtual {v1, v0, v2, v3}, Lcom/baidu/mobstat/br;->a(Landroid/app/Activity;Lorg/json/JSONObject;Z)V

    :cond_4
    return-void
.end method
