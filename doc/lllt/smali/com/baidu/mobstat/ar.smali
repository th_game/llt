.class public Lcom/baidu/mobstat/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobstat/ar$a;
    }
.end annotation


# static fields
.field private static volatile a:Z = true


# instance fields
.field private b:Lcom/baidu/mobstat/ar$a;

.field private c:Landroid/app/Activity;

.field private d:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/baidu/mobstat/ar$a;)V
    .locals 2

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/baidu/mobstat/ar$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobstat/ar$1;-><init>(Lcom/baidu/mobstat/ar;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobstat/ar;->d:Landroid/os/Handler;

    .line 31
    iput-object p1, p0, Lcom/baidu/mobstat/ar;->b:Lcom/baidu/mobstat/ar$a;

    return-void
.end method

.method private a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
    .locals 1

    :goto_0
    if-eqz p1, :cond_0

    .line 106
    instance-of v0, p1, Lcom/baidu/mobstat/as;

    if-eqz v0, :cond_0

    .line 107
    check-cast p1, Lcom/baidu/mobstat/as;

    .line 108
    invoke-virtual {p1}, Lcom/baidu/mobstat/as;->a()Landroid/view/Window$Callback;

    move-result-object p1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method static synthetic a(Lcom/baidu/mobstat/ar;)Lcom/baidu/mobstat/ar$a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/baidu/mobstat/ar;->b:Lcom/baidu/mobstat/ar$a;

    return-object p0
.end method

.method public static a(Z)V
    .locals 0

    if-eqz p0, :cond_0

    .line 36
    invoke-static {}, Lcom/baidu/mobstat/au;->a()V

    .line 39
    :cond_0
    sput-boolean p0, Lcom/baidu/mobstat/ar;->a:Z

    return-void
.end method

.method public static a()Z
    .locals 1

    .line 43
    sget-boolean v0, Lcom/baidu/mobstat/ar;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/baidu/mobstat/ar;)Landroid/os/Handler;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/baidu/mobstat/ar;->d:Landroid/os/Handler;

    return-object p0
.end method

.method private b(Landroid/app/Activity;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ar;->d(Landroid/app/Activity;)V

    return-void
.end method

.method private c(Landroid/app/Activity;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 89
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 94
    :cond_1
    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/ar;->a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;

    move-result-object v0

    .line 95
    invoke-virtual {p1, v0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    return-void
.end method

.method private d(Landroid/app/Activity;)V
    .locals 3

    .line 116
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 121
    :cond_0
    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 128
    :cond_1
    new-instance v1, Lcom/baidu/mobstat/as;

    new-instance v2, Lcom/baidu/mobstat/ar$2;

    invoke-direct {v2, p0}, Lcom/baidu/mobstat/ar$2;-><init>(Lcom/baidu/mobstat/ar;)V

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobstat/as;-><init>(Landroid/view/Window$Callback;Lcom/baidu/mobstat/as$a;)V

    invoke-virtual {p1, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 65
    iput-object p1, p0, Lcom/baidu/mobstat/ar;->c:Landroid/app/Activity;

    .line 66
    invoke-direct {p0, p1}, Lcom/baidu/mobstat/ar;->b(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/baidu/mobstat/ar;->c:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/baidu/mobstat/ar;->c(Landroid/app/Activity;)V

    const/4 v0, 0x0

    .line 72
    iput-object v0, p0, Lcom/baidu/mobstat/ar;->c:Landroid/app/Activity;

    return-void
.end method
