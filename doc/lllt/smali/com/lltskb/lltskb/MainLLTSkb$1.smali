.class Lcom/lltskb/lltskb/MainLLTSkb$1;
.super Ljava/lang/Object;
.source "MainLLTSkb.java"

# interfaces
.implements Lcom/qq/e/ads/splash/SplashADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/MainLLTSkb;->showGDTAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/MainLLTSkb;

.field final synthetic val$btnClose:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;Landroid/widget/Button;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    iput-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->val$btnClose:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$onADPresent$2(Landroid/widget/Button;)V
    .locals 1

    const/4 v0, 0x0

    .line 191
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onADDismissed$0$MainLLTSkb$1()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    return-void
.end method

.method public synthetic lambda$onNoAD$1$MainLLTSkb$1()V
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$100(Lcom/lltskb/lltskb/MainLLTSkb;)V

    return-void
.end method

.method public onADClicked()V
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "onADClicked"

    .line 201
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setLastUpdateTime(J)V

    .line 203
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "guangdiantong"

    const-string v2, "\u70b9\u51fb"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onADDismissed()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "onADDismissed"

    .line 172
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$A-801hzc3xJQs5AEwn4XFo5ZEIM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$A-801hzc3xJQs5AEwn4XFo5ZEIM;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onADExposure()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "onADExposure"

    .line 213
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onADPresent()V
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "onADPresent"

    .line 188
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->val$btnClose:Landroid/widget/Button;

    new-instance v2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$i7BGqUvjeU0_bH11JTRqRQlBndk;

    invoke-direct {v2, v1}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$i7BGqUvjeU0_bH11JTRqRQlBndk;-><init>(Landroid/widget/Button;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 195
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "guangdiantong"

    const-string v2, "\u63a5\u6536"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onADTick(J)V
    .locals 2

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onADTick "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MainLLTSkb"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 1

    const-string p1, "MainLLTSkb"

    const-string v0, "onNoAD"

    .line 180
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 182
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$1;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$kxVZ8eInyYV7n9x8cPKkTS4ZCR8;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$1$kxVZ8eInyYV7n9x8cPKkTS4ZCR8;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$1;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
