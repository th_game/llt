.class public Lcom/lltskb/lltskb/AppContext;
.super Landroid/support/multidex/MultiDexApplication;
.source "AppContext.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field private static TAG:Ljava/lang/String; = "AppContext"

.field private static mInstance:Lcom/lltskb/lltskb/AppContext;


# instance fields
.field private mActivityStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mAppExecutors:Lcom/lltskb/lltskb/AppExecutors;

.field private mCrashReport:Ljava/lang/String;

.field private mDefaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 119
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    .line 41
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mActivityStack:Ljava/util/Stack;

    .line 120
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mDefaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/AppContext;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/lltskb/lltskb/AppContext;->sendCrashReport()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/AppContext;)Z
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/lltskb/lltskb/AppContext;->handlerException()Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/AppContext;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/lltskb/lltskb/AppContext;->mDefaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object p0
.end method

.method public static get()Lcom/lltskb/lltskb/AppContext;
    .locals 1

    .line 48
    sget-object v0, Lcom/lltskb/lltskb/AppContext;->mInstance:Lcom/lltskb/lltskb/AppContext;

    return-object v0
.end method

.method private getCrashReport(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/AppContext;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "<br/>"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    invoke-static {}, Lcom/lltskb/lltskb/utils/Logger;->getCachedLog()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getFeedBackString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 9

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 259
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 262
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-object v5, v2, v4

    .line 263
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    if-eqz v6, :cond_1

    const-string v8, "com.baidu.mobads"

    .line 264
    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v8

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/LltSettings;->getBaiduCrashed()Z

    move-result v8

    if-nez v8, :cond_1

    .line 266
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/engine/LltSettings;->setBaiduCrashed(Z)V

    goto :goto_1

    :cond_1
    if-eqz v6, :cond_2

    const-string v8, "com.qq.e.comm.PyServerManager.getDatasFromServe"

    .line 267
    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 268
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/engine/LltSettings;->setBaiduCrashed(Z)V

    :cond_2
    :goto_1
    const-string v6, "at "

    .line 270
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 273
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_4

    const-string v1, "<br/>Caused by:"

    .line 275
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/AppContext;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private handlerException()Z
    .locals 4

    .line 155
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastUpdate(J)V

    .line 157
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 160
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 162
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d004e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d004f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/lltskb/lltskb/AppContext$2;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/AppContext$2;-><init>(Lcom/lltskb/lltskb/AppContext;)V

    invoke-static {v2, v0, v1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$onCreate$0()V
    .locals 2

    .line 86
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->setUserAgent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 88
    sget-object v1, Lcom/lltskb/lltskb/AppContext;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private saveCrashReport(Ljava/lang/String;)V
    .locals 4

    .line 183
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    .line 184
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 186
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    return-void

    .line 192
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/utils/PermissionHelper;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/utils/PermissionHelper;-><init>(Landroid/content/Context;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 194
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/utils/PermissionHelper;->checkPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 201
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "/lltskb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    .line 205
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_3

    .line 206
    sget-object v2, Lcom/lltskb/lltskb/AppContext;->TAG:Ljava/lang/String;

    const-string v3, "mkdirs failed"

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_3
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-nez v1, :cond_4

    .line 210
    sget-object v1, Lcom/lltskb/lltskb/AppContext;->TAG:Ljava/lang/String;

    const-string v2, "createNewFile failed"

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 214
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 218
    :cond_4
    :goto_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "lltskbCrash.txt"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 222
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 223
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 227
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private sendCrashReport()V
    .locals 8

    .line 124
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    .line 127
    new-instance v3, Lcom/lltskb/lltskb/AppContext$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/AppContext$1;-><init>(Lcom/lltskb/lltskb/AppContext;)V

    .line 141
    new-instance v7, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u5d29\u6e83\u4fe1\u606f["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    const-string v5, "yjg72889@163.com"

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask$Listener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    .line 142
    invoke-virtual {v7, v0}, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public addActivity(Landroid/app/Activity;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .line 98
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 99
    invoke-static {p0}, Landroid/support/multidex/MultiDex;->install(Landroid/content/Context;)V

    return-void
.end method

.method public executeOnDiskIO(Ljava/lang/Runnable;)V
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->diskIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public executeOnMainThread(Ljava/lang/Runnable;)V
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public executeOnNetworkIO(Ljava/lang/Runnable;)V
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentContext()Landroid/content/Context;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/lltskb/lltskb/AppContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method public getExecutors()Lcom/lltskb/lltskb/AppExecutors;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mAppExecutors:Lcom/lltskb/lltskb/AppExecutors;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .line 65
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 67
    invoke-static {}, Lcom/lltskb/lltskb/utils/FeatureToggle;->initFeatures()V

    .line 69
    const-class v0, Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    sget-object v1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 73
    :cond_0
    sput-object p0, Lcom/lltskb/lltskb/AppContext;->mInstance:Lcom/lltskb/lltskb/AppContext;

    .line 74
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 76
    new-instance v0, Lcom/lltskb/lltskb/AppExecutors;

    invoke-direct {v0}, Lcom/lltskb/lltskb/AppExecutors;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mAppExecutors:Lcom/lltskb/lltskb/AppExecutors;

    .line 83
    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/lltskb/lltskb/-$$Lambda$AppContext$gf1Ghk5b7nEhJtdpIGzjPIuQr7c;->INSTANCE:Lcom/lltskb/lltskb/-$$Lambda$AppContext$gf1Ghk5b7nEhJtdpIGzjPIuQr7c;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 90
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public removeActivity(Landroid/app/Activity;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/AppContext;->getCrashReport(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    .line 237
    sget-object v0, Lcom/lltskb/lltskb/AppContext;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uncaughtException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/AppContext;->mCrashReport:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/AppContext;->saveCrashReport(Ljava/lang/String;)V

    .line 240
    new-instance v0, Lcom/lltskb/lltskb/AppContext$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/lltskb/lltskb/AppContext$3;-><init>(Lcom/lltskb/lltskb/AppContext;Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 253
    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext$3;->start()V

    return-void
.end method
