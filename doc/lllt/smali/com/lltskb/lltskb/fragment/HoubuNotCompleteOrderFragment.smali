.class public final Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "HoubuNotCompleteOrderFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuNotCompleteOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuNotCompleteOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment\n*L\n1#1,265:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 %2\u00020\u0001:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0008\u0010\u0014\u001a\u00020\u0013H\u0002J\u0008\u0010\u0015\u001a\u00020\u0013H\u0002J\u0006\u0010\u0016\u001a\u00020\u0013J\u0008\u0010\u0017\u001a\u00020\u0013H\u0002J&\u0010\u0018\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u0013H\u0016J\u0008\u0010 \u001a\u00020\u0013H\u0016J\u0008\u0010!\u001a\u00020\u0013H\u0002J\u0006\u0010\"\u001a\u00020\u0013J\u0008\u0010#\u001a\u00020\u0013H\u0002J\u0008\u0010$\u001a\u00020\u0013H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;",
        "Lcom/lltskb/lltskb/fragment/BaseFragment;",
        "()V",
        "houbuOrderLayout",
        "Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;",
        "isPaused",
        "",
        "lostTime",
        "",
        "Ljava/lang/Long;",
        "queryQueueDTO",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;",
        "rootView",
        "Landroid/view/View;",
        "updateHandler",
        "Landroid/os/Handler;",
        "updateRunnable",
        "Ljava/lang/Runnable;",
        "cancelOrder",
        "",
        "doCancelOrder",
        "doContinuePay",
        "initView",
        "onContinuePay",
        "onCreateView",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onPause",
        "onResume",
        "promptSignin",
        "refreshOrder",
        "updateTimeLeft",
        "updateView",
        "Companion",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$Companion;

.field public static final TAG:Ljava/lang/String; = "HoubuNotCompleteOrderFragment"

.field private static firstTimeRun:Z


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private houbuOrderLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

.field private isPaused:Z

.field private lostTime:Ljava/lang/Long;

.field private queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

.field private rootView:Landroid/view/View;

.field private updateHandler:Landroid/os/Handler;

.field private updateRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->Companion:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$Companion;

    const/4 v0, 0x1

    .line 32
    sput-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateHandler:Landroid/os/Handler;

    .line 41
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic access$cancelOrder(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->cancelOrder()V

    return-void
.end method

.method public static final synthetic access$doCancelOrder(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->doCancelOrder()V

    return-void
.end method

.method public static final synthetic access$doContinuePay(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->doContinuePay()V

    return-void
.end method

.method public static final synthetic access$getFirstTimeRun$cp()Z
    .locals 1

    .line 29
    sget-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->firstTimeRun:Z

    return v0
.end method

.method public static final synthetic access$getQueryQueueDTO$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    return-object p0
.end method

.method public static final synthetic access$getUpdateHandler$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Landroid/os/Handler;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateHandler:Landroid/os/Handler;

    return-object p0
.end method

.method public static final synthetic access$getUpdateRunnable$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Ljava/lang/Runnable;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateRunnable:Ljava/lang/Runnable;

    if-nez p0, :cond_0

    const-string v0, "updateRunnable"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$isPaused$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->isPaused:Z

    return p0
.end method

.method public static final synthetic access$onContinuePay(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->onContinuePay()V

    return-void
.end method

.method public static final synthetic access$promptSignin(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->promptSignin()V

    return-void
.end method

.method public static final synthetic access$setFirstTimeRun$cp(Z)V
    .locals 0

    .line 29
    sput-boolean p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public static final synthetic access$setPaused$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;Z)V
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->isPaused:Z

    return-void
.end method

.method public static final synthetic access$setQueryQueueDTO$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    return-void
.end method

.method public static final synthetic access$setUpdateHandler$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;Landroid/os/Handler;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateHandler:Landroid/os/Handler;

    return-void
.end method

.method public static final synthetic access$setUpdateRunnable$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;Ljava/lang/Runnable;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic access$updateTimeLeft(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateTimeLeft()V

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateView()V

    return-void
.end method

.method private final cancelOrder()V
    .locals 4

    const-string v0, "HoubuNotCompleteOrderFragment"

    const-string v1, "cancelOrder"

    .line 167
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$cancelOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$cancelOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;

    const v2, 0x7f0d02de

    const v3, 0x7f0d008b

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private final doCancelOrder()V
    .locals 4

    .line 180
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d008e

    const/high16 v2, -0x1000000

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 182
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final doContinuePay()V
    .locals 4

    const-string v0, "HoubuNotCompleteOrderFragment"

    const-string v1, "doContinuePay"

    .line 220
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d01a6

    const/high16 v2, -0x1000000

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 223
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    .line 262
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    return-void
.end method

.method private final onContinuePay()V
    .locals 5

    const-string v0, "HoubuNotCompleteOrderFragment"

    const-string v1, "onContinuePay"

    .line 205
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d0134

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 207
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "Locale.CHINA"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fmt"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getPrepay_amount()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x0

    aput-object v3, v2, v4

    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(locale, format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d0173

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v3, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$onContinuePay$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$onContinuePay$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v3, Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;

    invoke-static {v1, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private final promptSignin()V
    .locals 4

    const-string v0, "HoubuNotCompleteOrderFragment"

    const-string v1, "promptSignin"

    .line 90
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$promptSignin$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$promptSignin$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    const v2, 0x7f0d02de

    const v3, 0x7f0d010e

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private final updateTimeLeft()V
    .locals 9

    .line 139
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->lostTime:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-gez v5, :cond_2

    .line 141
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 143
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/16 v3, 0x3e8

    int-to-long v3, v3

    div-long/2addr v1, v3

    const/16 v5, 0x3c

    int-to-long v5, v5

    div-long/2addr v1, v5

    .line 144
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    div-long/2addr v7, v3

    rem-long/2addr v7, v5

    .line 145
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v3, 0x7f0d013e

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 147
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->houbuOrderLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    if-eqz v3, :cond_3

    sget-object v4, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v5, "Locale.CHINA"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "fmt"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    array-length v1, v5

    invoke-static {v5, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v4, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(locale, format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updateStatus(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private final updateView()V
    .locals 10

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->houbuOrderLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setQueryQueueOrder(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getBeginTime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getLoseTime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 112
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time left ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoubuNotCompleteOrderFragment"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_3

    const-wide/16 v2, 0x0

    .line 115
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 117
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->lostTime:Ljava/lang/Long;

    .line 118
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateTimeLeft()V

    .line 120
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d0137

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->houbuOrderLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    const-string v3, "java.lang.String.format(locale, format, *args)"

    const/4 v4, 0x1

    const-string v5, "Locale.CHINA"

    const/4 v6, 0x0

    if-eqz v2, :cond_5

    sget-object v7, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v7, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "fmt"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getPrepay_amount()Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    :cond_4
    move-object v9, v1

    :goto_2
    aput-object v9, v8, v6

    array-length v9, v8

    invoke-static {v8, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v0, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updatePriceView(Ljava/lang/String;)V

    .line 124
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_c

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->queryQueueDTO:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v2

    goto :goto_3

    :cond_6
    move-object v2, v1

    .line 125
    :goto_3
    iget-object v7, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    if-eqz v7, :cond_7

    invoke-virtual {v7}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_7

    const v1, 0x7f0902a0

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :cond_7
    const/16 v7, 0x8

    if-eqz v2, :cond_8

    if-eqz v1, :cond_b

    .line 127
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_8
    if-eqz v1, :cond_9

    .line 130
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    :cond_9
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v8, 0x7f0d0148

    invoke-virtual {v2, v8}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "AppContext.get().getStri\u2026ring.fmt_order_not_found)"

    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_a

    .line 132
    sget-object v8, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v8, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    const v9, 0x7f0d016b

    invoke-virtual {v5, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    array-length v5, v4

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v8, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    const/16 v6, 0x8

    .line 124
    :cond_b
    :goto_4
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public final initView()V
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v2, 0x7f090174

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->houbuOrderLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_1

    const v2, 0x7f09004b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 154
    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$initView$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$initView$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_3

    const v1, 0x7f090049

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/Button;

    :cond_3
    if-eqz v1, :cond_4

    .line 159
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$initView$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$initView$2;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_5

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0b0047

    const/4 v0, 0x0

    .line 102
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    .line 103
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->initView()V

    .line 104
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    return-object p1
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onDestroyView()V

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->_$_clearFindViewByIdCache()V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 52
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onPause()V

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->isPaused:Z

    return-void
.end method

.method public onResume()V
    .locals 3

    .line 57
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onResume()V

    const/4 v0, 0x0

    .line 58
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->isPaused:Z

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateRunnable:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    const-string v2, "updateRunnable"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final refreshOrder()V
    .locals 4

    .line 63
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v2, 0x7f0902a0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    .line 64
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0d022d

    const/high16 v3, -0x1000000

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 67
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$refreshOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$refreshOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 85
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->isPaused:Z

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->updateRunnable:Ljava/lang/Runnable;

    if-nez v1, :cond_2

    const-string v2, "updateRunnable"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
