.class public Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "BigScreenSelectStationFragment.java"

# interfaces
.implements Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;,
        Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adapter:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

.field private adapterSearch:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

.field private bigScreenModel:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

.field private et_search:Landroid/widget/EditText;

.field private listener:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;

.field private ll_station_search:Landroid/widget/LinearLayout;

.field private messageView:Landroid/widget/TextView;

.field private quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

.field private stationList:Landroid/widget/ListView;

.field private stationModelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;"
        }
    .end annotation
.end field

.field private stationSearchList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;"
        }
    .end annotation
.end field

.field private stationSearchListView:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    .line 49
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->bigScreenModel:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/EditText;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->et_search:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/LinearLayout;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->ll_station_search:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/ListView;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationList:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/view/widget/QuickLocationBar;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationSearchList:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationModelList:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/TextView;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->messageView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapterSearch:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    return-object p0
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapter:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    return-object p0
.end method

.method private listenerSearchEdit()V
    .locals 3

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationSearchList:Ljava/util/List;

    .line 122
    new-instance v0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationSearchList:Ljava/util/List;

    invoke-direct {v0, v1, v2, p0}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapterSearch:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    .line 123
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationSearchListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapterSearch:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->et_search:Landroid/widget/EditText;

    new-instance v1, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreateView$0$BigScreenSelectStationFragment(Landroid/view/View;)V
    .locals 0

    .line 64
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->dismiss()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->ll_station_search:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->et_search:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .line 59
    sget-object p3, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->TAG:Ljava/lang/String;

    const-string v0, "onCreateView"

    invoke-static {p3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p3, 0x0

    const v0, 0x7f0b0026

    .line 60
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f0902a0

    .line 62
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->messageView:Landroid/widget/TextView;

    const p2, 0x7f0900fb

    .line 63
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 64
    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$BigScreenSelectStationFragment$kqFbfvq18-jc1RurdNi6lSmiLKc;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$BigScreenSelectStationFragment$kqFbfvq18-jc1RurdNi6lSmiLKc;-><init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090223

    .line 65
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const v0, 0x7f0d0263

    .line 66
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    const p2, 0x7f090184

    .line 67
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationList:Landroid/widget/ListView;

    const p2, 0x7f090185

    .line 68
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationSearchListView:Landroid/widget/ListView;

    const p2, 0x7f0900c7

    .line 69
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->et_search:Landroid/widget/EditText;

    const p2, 0x7f090202

    .line 70
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    .line 71
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    new-instance v0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;-><init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;)V

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setOnTouchLitterChangedListener(Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;)V

    const p2, 0x7f090177

    .line 73
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->ll_station_search:Landroid/widget/LinearLayout;

    const p2, 0x7f090201

    .line 75
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setTextDialog(Landroid/widget/TextView;)V

    .line 78
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationModelList:Ljava/util/List;

    .line 80
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->bigScreenModel:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getStationMap()Ljava/util/Map;

    move-result-object p2

    .line 82
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 83
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    .line 84
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 87
    :cond_0
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, p3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    .line 88
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationModelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationModelList:Ljava/util/List;

    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 94
    new-instance p2, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationModelList:Ljava/util/List;

    invoke-direct {p2, v0, v1, p0}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;)V

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapter:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    .line 95
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->stationList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapter:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->adapter:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->getStationMap()Ljava/util/Map;

    move-result-object p2

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->quicLocationBar:Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p2

    new-array p3, p3, [Ljava/lang/String;

    invoke-interface {p2, p3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setCharacters([Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->listenerSearchEdit()V

    return-object p1
.end method

.method public onItemClick(Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;)V
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->dismiss()V

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->listener:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 108
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;->onStationSelected(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->listener:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;

    return-void
.end method
