.class public Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "MyOrderDetailListFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MyOrderDetailListFragment"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

.field private mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

.field private mShareUri:Landroid/net/Uri;

.field private mTabLayout:Landroid/support/design/widget/TabLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 418
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mShareUri:Landroid/net/Uri;

    return-void
.end method

.method private deleteSharedImage(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 355
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_data LIKE ?"

    invoke-virtual {p1, v1, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private getListContentHeight()I
    .locals 3

    .line 227
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 228
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->getLastVisiblePosition()I

    move-result v0

    if-gez v0, :cond_1

    return v1

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    .line 233
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    return v0
.end method

.method private getPassengers()Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation

    .line 389
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-eqz v0, :cond_7

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    if-nez v0, :cond_0

    goto :goto_3

    .line 391
    :cond_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 393
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 394
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v3, :cond_1

    goto :goto_2

    .line 396
    :cond_1
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x12

    if-eq v4, v5, :cond_2

    goto :goto_2

    .line 401
    :cond_2
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 402
    iget-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_5

    .line 408
    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    return-object v0

    :cond_7
    :goto_3
    const/4 v0, 0x0

    return-object v0
.end method

.method private initTabLayout(Landroid/support/design/widget/TabLayout;)V
    .locals 2

    const/4 v0, 0x1

    .line 173
    invoke-virtual {p1, v0}, Landroid/support/design/widget/TabLayout;->setTabMode(I)V

    const v0, 0x7f060060

    const v1, 0x7f060023

    .line 174
    invoke-virtual {p1, v0, v1}, Landroid/support/design/widget/TabLayout;->setTabTextColors(II)V

    .line 176
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    const v1, 0x7f0d0248

    .line 177
    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 178
    invoke-virtual {p1, v0}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 181
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    const v1, 0x7f0d023c

    .line 182
    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 183
    invoke-virtual {p1, v0}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 185
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    const v1, 0x7f0d0096

    .line 186
    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 187
    invoke-virtual {p1, v0}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    const/high16 v0, 0x41200000    # 10.0f

    .line 189
    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    return-void
.end method

.method private onCopyOrder()V
    .locals 3

    .line 197
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-nez v0, :cond_0

    return-void

    .line 201
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    .line 203
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    .line 204
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 207
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    const-string v2, "clipboard"

    .line 213
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    if-nez v1, :cond_3

    return-void

    .line 220
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\u8def\u8def\u901a"

    .line 221
    invoke-static {v2, v0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 222
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 223
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u5df2\u590d\u5236\u5230\u526a\u8d34\u677f"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private onShareOrder()V
    .locals 5

    const-string v0, "MyOrderDetailListFragment"

    const-string v1, "onShareOrder"

    .line 237
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->setDrawingCacheEnabled(Z)V

    .line 242
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XListView;->buildDrawingCache()V

    .line 244
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->destroyDrawingCache()V

    return-void

    .line 251
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getListContentHeight()I

    move-result v2

    if-lez v2, :cond_1

    .line 252
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 253
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v1, v4, v4, v3, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 271
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " content height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " w="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3, v3}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 288
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->destroyDrawingCache()V

    return-void

    .line 292
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uriString="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mShareUri:Landroid/net/Uri;

    .line 296
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "image/*"

    .line 298
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mShareUri:Landroid/net/Uri;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "\u901a\u77e5\u8d2d\u7968\u4eba"

    const-string v2, "android.intent.extra.TITLE"

    .line 300
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.SUBJECT"

    const-string v3, "\u8ba2\u5355\u4fe1\u606f"

    .line 301
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    const-string v3, "\u8def\u8def\u901a\u65f6\u523b\u8868"

    .line 302
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    .line 303
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 304
    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->startActivity(Landroid/content/Intent;)V

    .line 307
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->destroyDrawingCache()V

    return-void
.end method

.method public static saveBitmapToFile(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "MyOrderDetailListFragment"

    const/4 v1, 0x0

    .line 323
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 326
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    .line 327
    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 328
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 329
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_0

    .line 330
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "mkdirs failed"

    .line 331
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "createNewFile failed"

    .line 335
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_1
    new-instance p1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 338
    :try_start_1
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x3c

    invoke-virtual {p0, v1, v2, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p0, :cond_2

    .line 344
    :try_start_2
    invoke-virtual {p1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 346
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v4

    .line 344
    :cond_2
    :try_start_3
    invoke-virtual {p1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    .line 346
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 p0, 0x1

    return p0

    :catchall_0
    move-exception p0

    move-object v1, p1

    goto :goto_2

    :catchall_1
    move-exception p0

    :goto_2
    if-eqz v1, :cond_3

    .line 344
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 346
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_3
    :goto_3
    throw p0
.end method


# virtual methods
.method public getRealFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    .line 366
    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 369
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v2, "file"

    .line 370
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 371
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "content"

    .line 372
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 373
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 p1, 0x1

    new-array v4, p1, [Ljava/lang/String;

    const/4 p1, 0x0

    const-string v1, "_data"

    aput-object v1, v4, p1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 375
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 376
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    const/4 v1, -0x1

    if-le p2, v1, :cond_3

    .line 378
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    move-object v0, p2

    .line 381
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    :goto_0
    return-object v0
.end method

.method public synthetic lambda$onCreateView$0$MyOrderDetailListFragment(Landroid/view/View;)V
    .locals 0

    .line 103
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$onCreateView$1$MyOrderDetailListFragment(Landroid/view/View;)V
    .locals 2

    const-string p1, "kuntao_h5"

    .line 122
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 123
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "web_url"

    const-string v1, "http://wap.lltskb.com/ktzx.html"

    .line 124
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x0

    const-string v1, "web_post"

    .line 125
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "web_closeonback"

    .line 126
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_0
    return-void

    .line 133
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getPassengers()Ljava/util/Vector;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 134
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 138
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 139
    new-instance v1, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {v1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;-><init>()V

    .line 140
    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->setPassengers(Ljava/util/Vector;)V

    const p1, 0x7f0900df

    .line 141
    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    const/4 p1, 0x0

    .line 142
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 143
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_3
    :goto_0
    return-void
.end method

.method public synthetic lambda$onCreateView$2$MyOrderDetailListFragment(Landroid/view/View;)V
    .locals 0

    .line 154
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->onShareOrder()V

    return-void
.end method

.method public synthetic lambda$onCreateView$3$MyOrderDetailListFragment(Landroid/view/View;)V
    .locals 0

    .line 157
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->onCopyOrder()V

    return-void
.end method

.method public synthetic lambda$onCreateView$4$MyOrderDetailListFragment(Landroid/view/View;)V
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    const-string v0, "https://kyfw.12306.cn/otn/queryOrder/init"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .line 64
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 p3, 0x0

    const v0, 0x7f0b002b

    .line 85
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090187

    .line 86
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 87
    new-instance p2, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;-><init>(Landroid/app/Activity;)V

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    .line 88
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->setData(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V

    .line 89
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 91
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 92
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 93
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    const p2, 0x7f090228

    .line 96
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/support/design/widget/TabLayout;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mTabLayout:Landroid/support/design/widget/TabLayout;

    .line 97
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mTabLayout:Landroid/support/design/widget/TabLayout;

    const/16 v0, 0x8

    if-eqz p2, :cond_0

    .line 98
    invoke-virtual {p2, v0}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    :cond_0
    const p2, 0x7f0900fb

    .line 102
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 103
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$lUvqylhORnRf-W8hJ9B2_ay0KwY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$lUvqylhORnRf-W8hJ9B2_ay0KwY;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    const p2, 0x7f090223

    .line 106
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 107
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f0d01fc

    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    aput-object v5, v4, p3

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 108
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 112
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const p2, 0x7f090120

    .line 114
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 117
    invoke-static {}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isBaoxianEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    .line 118
    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 120
    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$DK9aRi6F4wWLtolXnRHf-zngzQQ;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$DK9aRi6F4wWLtolXnRHf-zngzQQ;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const p2, 0x7f090148

    .line 149
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 151
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0902e1

    .line 153
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 154
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$TgVVECkgNmuWvSlRvSEv0XCW8Zk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$TgVVECkgNmuWvSlRvSEv0XCW8Zk;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09026a

    .line 156
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 157
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$Ui0M-8rgwtGyMnD6QwggE5wGUUo;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$Ui0M-8rgwtGyMnD6QwggE5wGUUo;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const v0, 0x7f090067

    .line 161
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_5

    .line 163
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setVisibility(I)V

    const p2, 0x7f0d023d

    .line 164
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 165
    new-instance p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$8vNyJ9YzebDHHxUjheLoKIl1sQ0;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderDetailListFragment$8vNyJ9YzebDHHxUjheLoKIl1sQ0;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    return-object p1
.end method

.method public onStart()V
    .locals 2

    .line 69
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onStart()V

    const-string v0, "MyOrderDetailListFragment"

    const-string v1, "onStart"

    .line 70
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setOrderData(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V
    .locals 5

    .line 425
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 426
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->setData(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V

    .line 428
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/CompleteListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->notifyDataSetChanged()V

    .line 431
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 432
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-eqz v0, :cond_1

    const v0, 0x7f090223

    .line 433
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 434
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v1, 0x7f0d01fc

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->mOrder:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
