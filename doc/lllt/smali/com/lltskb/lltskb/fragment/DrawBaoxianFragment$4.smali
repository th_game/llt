.class Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;
.super Landroid/widget/BaseAdapter;
.source "DrawBaoxianFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->initView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    if-ltz p1, :cond_2

    .line 245
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 259
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0b0022

    const/4 v1, 0x0

    .line 260
    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 263
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez p1, :cond_1

    return-object p2

    :cond_1
    const p3, 0x7f0902a8

    .line 266
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 267
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0900c0

    .line 269
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/EditText;

    .line 270
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const-string v1, "**"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 271
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 274
    :cond_2
    invoke-virtual {p3}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$IdNoTextWatcher;

    if-eqz v0, :cond_3

    .line 276
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$IdNoTextWatcher;->setDTO(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V

    goto :goto_0

    .line 278
    :cond_3
    new-instance v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$IdNoTextWatcher;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {v0, v1, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$IdNoTextWatcher;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V

    .line 279
    invoke-virtual {p3, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :goto_0
    const p3, 0x7f0900c1

    .line 282
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/EditText;

    .line 284
    invoke-virtual {p3}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$MobileTextWatcher;

    if-eqz v0, :cond_4

    .line 286
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$MobileTextWatcher;->setDTO(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V

    goto :goto_1

    .line 288
    :cond_4
    new-instance v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$MobileTextWatcher;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {v0, v1, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$MobileTextWatcher;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V

    .line 289
    invoke-virtual {p3, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 292
    :goto_1
    invoke-virtual {p3, v0}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 294
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 295
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getMobileNo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    .line 297
    :cond_5
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 299
    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const p3, 0x7f090054

    .line 302
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/Button;

    if-eqz p3, :cond_6

    .line 304
    invoke-virtual {p3, p1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 305
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {p3, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    return-object p2
.end method
