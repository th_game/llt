.class public final Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "HoubuWaitingOrderFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuWaitingOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuWaitingOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment\n*L\n1#1,200:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\nH\u0002J\u0006\u0010\u000b\u001a\u00020\nJ&\u0010\u000c\u001a\u0004\u0018\u00010\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\nH\u0002J\u0006\u0010\u0014\u001a\u00020\nJ\u0008\u0010\u0015\u001a\u00020\nH\u0002J\u0010\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0012\u0010\u0019\u001a\u00020\n2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u001aH\u0002J\u0008\u0010\u001b\u001a\u00020\nH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;",
        "Lcom/lltskb/lltskb/fragment/BaseFragment;",
        "()V",
        "houbuOrderLinearLayout",
        "Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;",
        "queryQueueOrder",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "rootView",
        "Landroid/view/View;",
        "cancelOrder",
        "",
        "initView",
        "onCreateView",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "promptSignin",
        "refreshOrder",
        "reserveReturn",
        "showReserveReturnCheckDialog",
        "dto",
        "Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;",
        "showReserveReturnResult",
        "Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;",
        "updateView",
        "Companion",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$Companion;

.field public static final TAG:Ljava/lang/String; = "HoubuWaitingOrderFragment"

.field private static firstTimeRun:Z


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

.field private queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

.field private rootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->Companion:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$Companion;

    const/4 v0, 0x1

    .line 30
    sput-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method public static final synthetic access$cancelOrder(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->cancelOrder()V

    return-void
.end method

.method public static final synthetic access$getFirstTimeRun$cp()Z
    .locals 1

    .line 27
    sget-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->firstTimeRun:Z

    return v0
.end method

.method public static final synthetic access$getQueryQueueOrder$p(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    return-object p0
.end method

.method public static final synthetic access$promptSignin(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->promptSignin()V

    return-void
.end method

.method public static final synthetic access$reserveReturn(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->reserveReturn()V

    return-void
.end method

.method public static final synthetic access$setFirstTimeRun$cp(Z)V
    .locals 0

    .line 27
    sput-boolean p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public static final synthetic access$setQueryQueueOrder$p(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    return-void
.end method

.method public static final synthetic access$showReserveReturnCheckDialog(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->showReserveReturnCheckDialog(Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;)V

    return-void
.end method

.method public static final synthetic access$showReserveReturnResult(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->showReserveReturnResult(Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;)V

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->updateView()V

    return-void
.end method

.method private final cancelOrder()V
    .locals 4

    const-string v0, "HoubuWaitingOrderFragment"

    const-string v1, "cancelOrder"

    .line 118
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d008e

    const/high16 v2, -0x1000000

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 121
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final promptSignin()V
    .locals 4

    const-string v0, "HoubuWaitingOrderFragment"

    const-string v1, "promptSignin"

    .line 66
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$promptSignin$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$promptSignin$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    const v2, 0x7f0d02de

    const v3, 0x7f0d010e

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private final reserveReturn()V
    .locals 4

    .line 158
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d008e

    const/high16 v2, -0x1000000

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 160
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$reserveReturn$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$reserveReturn$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final showReserveReturnCheckDialog(Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;)V
    .locals 4

    .line 142
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d012a

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppContext.get().getStri\u2026t_confirm_reserve_return)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "Locale.CHINA"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;->getData()Ljava/lang/Float;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 v3, 0x0

    aput-object p1, v2, v3

    array-length p1, v2

    invoke-static {v2, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, v0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "java.lang.String.format(locale, format, *args)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0173

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast p1, Ljava/lang/CharSequence;

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$showReserveReturnCheckDialog$$inlined$apply$lambda$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$showReserveReturnCheckDialog$$inlined$apply$lambda$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v2, Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private final showReserveReturnResult(Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;)V
    .locals 4

    .line 183
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$showReserveReturnResult$listener$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$showReserveReturnResult$listener$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    const v1, 0x7f0d0107

    if-eqz p1, :cond_2

    .line 185
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 186
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;->getMsg()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const v3, 0x7f0d0173

    if-nez v2, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;->getMsg()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v1, v2, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckData;->getFlag()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 190
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d024a

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 189
    invoke-static {p1, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 193
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 192
    invoke-static {p1, v2, v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_0
    return-void

    .line 198
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    const v2, 0x7f0d02de

    invoke-static {p1, v2, v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private final updateView()V
    .locals 10

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setQueryQueueOrder(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V

    .line 87
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    const-string v2, "java.lang.String.format(locale, format, *args)"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "Locale.CHINA"

    const/4 v6, 0x0

    if-eqz v1, :cond_2

    sget-object v7, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v7, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "fmt"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getPrepay_amount()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_1
    move-object v9, v3

    :goto_0
    aput-object v9, v8, v6

    array-length v9, v8

    invoke-static {v8, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v0, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updatePriceView(Ljava/lang/String;)V

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v0, :cond_3

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_3

    const v3, 0x7f0902a0

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    :cond_3
    const/16 v7, 0x8

    if-eqz v1, :cond_4

    if-eqz v3, :cond_7

    .line 94
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    if-eqz v3, :cond_5

    .line 97
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    :cond_5
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v8, 0x7f0d0148

    invoke-virtual {v1, v8}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v8, "AppContext.get().getStri\u2026ring.fmt_order_not_found)"

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v3, :cond_6

    .line 99
    sget-object v8, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v8, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    const v9, 0x7f0d016a

    invoke-virtual {v5, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    array-length v5, v4

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v8, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    const/16 v6, 0x8

    .line 91
    :cond_7
    :goto_1
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public final initView()V
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v2, 0x7f090174

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setVisibility(I)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_2

    const v1, 0x7f090049

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/Button;

    :cond_2
    if-eqz v1, :cond_3

    .line 110
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$initView$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$initView$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0b004e

    const/4 v0, 0x0

    .line 78
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    .line 79
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->initView()V

    .line 80
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    return-object p1
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onDestroyView()V

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->_$_clearFindViewByIdCache()V

    return-void
.end method

.method public final refreshOrder()V
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v2, 0x7f0902a0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    .line 35
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 36
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0d022d

    const/high16 v3, -0x1000000

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 37
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method
