.class Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;
.super Ljava/lang/Object;
.source "SelectStationFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/SelectStationFragment;->initView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

.field final synthetic val$adapter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->val$adapter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 149
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 150
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->val$adapter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 153
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$100(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/GridView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/GridView;->setVisibility(I)V

    .line 155
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$200(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;

    move-result-object p1

    const v0, 0x7f0d0192

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 156
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$302(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0

    .line 158
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$100(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/GridView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 160
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$200(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;

    move-result-object p1

    const v1, 0x7f0d022e

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->access$302(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
