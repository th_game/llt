.class final Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;
.super Ljava/lang/Object;
.source "HoubuWaitingOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->refreshOrder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuWaitingOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuWaitingOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1\n*L\n1#1,200:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 38
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    .line 48
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    const-string v1, "ModelFactory.get().houbuTicketModel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->queryUnHonourHOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderDTO;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 50
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderData;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderData;->getList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 51
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    const/4 v1, 0x0

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->access$setQueryQueueOrder$p(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->access$setQueryQueueOrder$p(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V

    .line 58
    :cond_2
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1$3;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1$3;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$refreshOrder$1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
