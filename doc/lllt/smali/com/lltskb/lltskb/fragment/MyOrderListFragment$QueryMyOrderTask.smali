.class final Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;
.super Landroid/os/AsyncTask;
.source "MyOrderListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/fragment/MyOrderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryMyOrderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private flag:I

.field private fragmentWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/fragment/MyOrderListFragment;",
            ">;"
        }
    .end annotation
.end field

.field private strCond:Ljava/lang/String;

.field private strEnd:Ljava/lang/String;

.field private strStart:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 208
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 209
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->fragmentWeakReference:Ljava/lang/ref/WeakReference;

    .line 210
    iput p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->flag:I

    .line 211
    iput-object p3, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->type:Ljava/lang/String;

    .line 212
    iput-object p4, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strStart:Ljava/lang/String;

    .line 213
    iput-object p5, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strEnd:Ljava/lang/String;

    .line 214
    iput-object p6, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strCond:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;Landroid/view/View;)V
    .locals 0

    .line 250
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->dismiss()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 259
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->fragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;

    if-eqz p1, :cond_2

    .line 260
    invoke-virtual {p1}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object p1

    .line 266
    :try_start_0
    iget v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->flag:I

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strStart:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strEnd:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->strCond:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->queryMyOrder(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 271
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const-string p1, "error"

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$MyOrderListFragment$QueryMyOrderTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 223
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->cancel(Z)Z

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .line 231
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->fragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;

    if-eqz v0, :cond_4

    .line 232
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 236
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 237
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->access$100(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->notifyDataSetChanged()V

    .line 239
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->access$200(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)Lcom/lltskb/lltskb/view/widget/XListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XListView;->stopRefresh()V

    .line 240
    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 241
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 242
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->dismiss()V

    return-void

    :cond_1
    if-eqz p1, :cond_3

    .line 246
    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d010e

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 247
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->access$300(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V

    return-void

    .line 250
    :cond_2
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    const v4, 0x7f0d010f

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$QueryMyOrderTask$CctZRvkGvsgGORTKcbPzEQg7m9U;

    invoke-direct {v4, v0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$QueryMyOrderTask$CctZRvkGvsgGORTKcbPzEQg7m9U;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V

    invoke-static {v2, v3, v1, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 253
    :cond_3
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    :cond_4
    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 218
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->fragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;

    if-eqz v0, :cond_1

    .line 219
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 223
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d0226

    const/4 v2, -0x1

    new-instance v3, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$QueryMyOrderTask$mme94d2e70QSpUgjcsIfCjFymkU;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$QueryMyOrderTask$mme94d2e70QSpUgjcsIfCjFymkU;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 225
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    :cond_1
    :goto_0
    return-void
.end method
