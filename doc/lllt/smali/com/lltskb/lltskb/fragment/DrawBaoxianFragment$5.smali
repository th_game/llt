.class Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;
.super Landroid/os/AsyncTask;
.source "DrawBaoxianFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->onGetBaoxian(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 349
    check-cast p1, [Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->doInBackground([Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 376
    aget-object p1, p1, v0

    .line 377
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->access$300(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object p1

    .line 379
    invoke-static {}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->save()Z

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 349
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 367
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 368
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 369
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 370
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 353
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 354
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;->this$0:Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5$1;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;)V

    const/4 v3, -0x1

    invoke-static {v1, v0, v3, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 362
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
