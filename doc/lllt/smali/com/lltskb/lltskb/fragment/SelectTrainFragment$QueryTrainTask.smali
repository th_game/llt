.class final Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;
.super Landroid/os/AsyncTask;
.source "SelectTrainFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/fragment/SelectTrainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryTrainTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/util/Vector<",
        "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mBeginTime:I

.field private mEndTime:I

.field private mErrMsg:Ljava/lang/String;

.field private mIsAllTime:Z

.field private mIsAllTrainType:Z

.field private selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/fragment/SelectTrainFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V
    .locals 1

    .line 293
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 294
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private init()V
    .locals 6

    .line 298
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    if-nez v0, :cond_0

    const-string v0, "SelectTrainFragment"

    const-string v1, "fragment is null"

    .line 300
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 304
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    .line 305
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    const-string v4, "QB"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mIsAllTrainType:Z

    .line 307
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v1

    const-string v4, "00:00--24:00"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mIsAllTime:Z

    .line 308
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    const-string v1, "--"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 309
    array-length v1, v0

    const/4 v4, 0x2

    if-lt v1, v4, :cond_4

    .line 310
    aget-object v1, v0, v3

    const-string v4, ":"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 311
    array-length v5, v1

    if-lez v5, :cond_2

    .line 312
    aget-object v1, v1, v3

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mBeginTime:I

    goto :goto_1

    .line 314
    :cond_2
    iput v3, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mBeginTime:I

    .line 317
    :goto_1
    aget-object v0, v0, v2

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 318
    array-length v1, v0

    if-lez v1, :cond_3

    .line 319
    aget-object v0, v0, v3

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mEndTime:I

    goto :goto_2

    .line 321
    :cond_3
    iput v3, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mEndTime:I

    :cond_4
    :goto_2
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 284
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->doInBackground([Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/Vector;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 367
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "SelectTrainFragment"

    const-string v1, "fragment is null"

    .line 369
    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 373
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;-><init>()V

    .line 376
    :try_start_0
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v3

    .line 377
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v5

    .line 376
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 378
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object v2
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_9

    .line 388
    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_2

    .line 393
    :cond_1
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 394
    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 396
    iget-boolean v3, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mIsAllTrainType:Z

    const/4 v4, 0x0

    if-nez v3, :cond_5

    .line 397
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v3

    array-length v5, v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1
    if-ge v6, v5, :cond_4

    aget-object v8, v3, v6

    .line 398
    iget-object v9, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-static {v9, v8}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v7, 0x1

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    if-nez v7, :cond_5

    goto :goto_0

    .line 406
    :cond_5
    iget-boolean v3, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mIsAllTime:Z

    if-nez v3, :cond_6

    .line 407
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v5, ":"

    invoke-static {v3, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 408
    array-length v5, v3

    if-lez v5, :cond_6

    .line 409
    aget-object v3, v3, v4

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v3

    .line 410
    iget v4, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mBeginTime:I

    if-lt v3, v4, :cond_2

    iget v4, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mEndTime:I

    if-le v3, v4, :cond_6

    goto :goto_0

    .line 415
    :cond_6
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 416
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainNo()Ljava/lang/String;

    move-result-object v3

    .line 417
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    .line 419
    :cond_7
    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    return-object v0

    .line 389
    :cond_9
    :goto_2
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;

    return-object v0

    .line 380
    :cond_a
    :try_start_1
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;
    :try_end_1
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    .line 384
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 385
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic lambda$onPreExecute$0$SelectTrainFragment$QueryTrainTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 337
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 284
    check-cast p1, Ljava/util/Vector;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->onPostExecute(Ljava/util/Vector;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Vector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;)V"
        }
    .end annotation

    .line 344
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    if-nez v0, :cond_0

    const-string p1, "SelectTrainFragment"

    const-string v0, "fragment is null"

    .line 346
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 350
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_1

    .line 351
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$100(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 352
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->access$100(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->setData(Ljava/util/Vector;)V

    :cond_1
    if-eqz p1, :cond_3

    .line 355
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 361
    :cond_2
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void

    .line 356
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;

    if-nez p1, :cond_4

    .line 357
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v1, 0x7f0d01ee

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;

    .line 358
    :cond_4
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->mErrMsg:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 331
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->selectTrainFragmentWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    if-nez v0, :cond_0

    const-string v0, "SelectTrainFragment"

    const-string v1, "fragment is null"

    .line 333
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 337
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0231

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, -0x1000000

    new-instance v3, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$QueryTrainTask$9GJlCLKyg-gOZ9TlKsNf1oL0pL0;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$QueryTrainTask$9GJlCLKyg-gOZ9TlKsNf1oL0pL0;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 338
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->init()V

    .line 339
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
