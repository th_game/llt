.class final Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;
.super Ljava/lang/Object;
.source "HoubuNotCompleteOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic $dto:Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;

.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->$dto:Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 187
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 188
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->$dto:Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getMsg()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 190
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->$dto:Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;

    const v2, 0x7f0d0173

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getFlag()Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;

    iget-object v0, v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d008f

    new-instance v3, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-static {v0, v2, v1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    .line 193
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;

    iget-object v1, v1, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v3, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$2;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$2;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-static {v1, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_1

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;

    iget-object v0, v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d008d

    new-instance v3, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$3;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1$3;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-static {v0, v2, v1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_1
    return-void
.end method
