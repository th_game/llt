.class public final Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HoubuProcessedOrderFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<",
        "Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuProcessedOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuProcessedOrderFragment.kt\ncom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter\n*L\n1#1,140:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000cH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000cH\u0016R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\n\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;",
        "()V",
        "orderList",
        "",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "getOrderList",
        "()Ljava/util/List;",
        "setOrderList",
        "(Ljava/util/List;)V",
        "getItemCount",
        "",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "ViewHolder",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private orderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->orderList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->orderList:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 107
    check-cast p1, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->onBindViewHolder(Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;I)V
    .locals 5

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->getItemCount()I

    move-result v0

    if-lt p2, v0, :cond_0

    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->orderList:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    goto :goto_0

    :cond_1
    move-object p2, v1

    .line 124
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d0136

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "AppContext.get().getStri\u2026string.fmt_hb_order_paid)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p1}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;->getHoubuOrderLinearLayout()Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setQueryQueueOrder(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V

    .line 127
    invoke-virtual {p1}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;->getHoubuOrderLinearLayout()Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    move-result-object p1

    sget-object v2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v3, "Locale.CHINA"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getPrepay_amount()Ljava/lang/String;

    move-result-object v1

    :cond_2
    aput-object v1, v3, v4

    array-length p2, v3

    invoke-static {v3, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    invoke-static {v2, v0, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "java.lang.String.format(locale, format, *args)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updatePriceView(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance p2, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "parent.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;-><init>(Landroid/content/Context;)V

    .line 113
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    new-instance p1, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;

    check-cast p2, Landroid/view/View;

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p1
.end method

.method public final setOrderList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
            ">;)V"
        }
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->orderList:Ljava/util/List;

    return-void
.end method
