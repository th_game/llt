.class Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;
.super Landroid/widget/BaseAdapter;
.source "QueryBaoxianFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->initView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->access$000(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 71
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->getItem(I)Ljava/util/Map$Entry;

    move-result-object p1

    return-object p1
.end method

.method public getItem(I)Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->access$000(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 97
    check-cast p2, Landroid/widget/TextView;

    if-nez p2, :cond_0

    .line 99
    new-instance p2, Landroid/widget/TextView;

    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const p3, -0xffff01

    .line 100
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 101
    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f08011d

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    .line 103
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p3, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const/4 v0, 0x0

    .line 104
    invoke-virtual {p2, v0, v0, p3, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const p3, 0x7f08006b

    .line 105
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    const/16 p3, 0xa

    const/16 v0, 0xf

    .line 106
    invoke-virtual {p2, v0, p3, v0, p3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 107
    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f0700c6

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    add-int/lit8 p3, p3, 0x14

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setHeight(I)V

    .line 110
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;->getItem(I)Ljava/util/Map$Entry;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 112
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\r\n\u8ba2\u5355\u53f7: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2
.end method
