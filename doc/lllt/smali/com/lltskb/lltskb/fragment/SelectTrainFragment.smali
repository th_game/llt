.class public Lcom/lltskb/lltskb/fragment/SelectTrainFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "SelectTrainFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;,
        Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectTrainFragment"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

.field private mListener:Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;

.field private mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

.field private mRoot:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    return-object p0
.end method

.method private getMaxDays()J
    .locals 2

    .line 141
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->isStudentPurpose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxStudentDays()J

    move-result-wide v0

    return-wide v0

    .line 144
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxOtherDays()J

    move-result-wide v0

    return-wide v0
.end method

.method private initView(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    const-string p1, "SelectTrainFragment"

    const-string v0, "initView root is null"

    .line 84
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const v0, 0x7f090223

    .line 87
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const v1, 0x7f0d0265

    .line 88
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    const v0, 0x7f0900fb

    .line 90
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v0, 0x7f090067

    .line 93
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 96
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const v0, 0x7f090189

    .line 99
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    if-nez v0, :cond_4

    return-void

    .line 102
    :cond_4
    new-instance v1, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    .line 103
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$wd0XPGAfpZU_3m38PFu58VDw2Z4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$wd0XPGAfpZU_3m38PFu58VDw2Z4;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f090273

    .line 106
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 107
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v1, :cond_5

    .line 108
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$AMUVoExTG_adBnDElJSOUNv_1pA;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$AMUVoExTG_adBnDElJSOUNv_1pA;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v0, 0x7f0902bf

    .line 115
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 117
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$-AtDbyuei9srzi75PL5kudWD62Y;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$-AtDbyuei9srzi75PL5kudWD62Y;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    const v0, 0x7f0902ab

    .line 121
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 123
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$Vh0LgAGzOTPmrFENnCq8AJhf9WY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$Vh0LgAGzOTPmrFENnCq8AJhf9WY;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    const v0, 0x7f090060

    .line 127
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 128
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    const v0, 0x7f090061

    .line 129
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 130
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :cond_9
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->queryTrain()V

    return-void
.end method

.method private isStudentPurpose()Z
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0X00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private onChangeDate(Z)V
    .locals 11

    const-string v0, "SelectTrainFragment"

    const-string v1, "onChangeDate"

    .line 190
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 197
    :catch_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 199
    :goto_0
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    if-eqz p1, :cond_0

    sub-long/2addr v1, v3

    goto :goto_1

    :cond_0
    add-long/2addr v1, v3

    .line 205
    :goto_1
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 206
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    .line 207
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getMaxDays()J

    move-result-wide v7

    const-wide/16 v9, 0x18

    mul-long v7, v7, v9

    const-wide/16 v9, 0xe10

    mul-long v7, v7, v9

    const-wide/16 v9, 0x3e8

    mul-long v7, v7, v9

    add-long/2addr v7, v5

    sub-long/2addr v5, v3

    const/4 p1, 0x0

    const v3, 0x7f0d0111

    cmp-long v4, v1, v5

    if-gez v4, :cond_1

    .line 210
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_1
    cmp-long v4, v1, v7

    if-lez v4, :cond_2

    .line 213
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 216
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderDate(Ljava/lang/String;)V

    .line 218
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->queryTrain()V

    return-void
.end method

.method private prepareResult()V
    .locals 5

    .line 249
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->getSelectedItems()Ljava/util/Vector;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 257
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 261
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    return-void
.end method

.method private queryTrain()V
    .locals 4

    .line 270
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v0, :cond_0

    const-string v0, "SelectTrainFragment"

    const-string v1, "queryTrain mOrderConfig is null"

    .line 271
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_1

    const v1, 0x7f090273

    .line 276
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 277
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    .line 281
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$QueryTrainTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private showDateDialog()V
    .locals 6

    const-string v0, "SelectTrainFragment"

    const-string v1, "showDateDialog"

    .line 149
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 152
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->requestWindowFeature(I)Z

    .line 153
    new-instance v2, Lcom/lltskb/lltskb/view/CalendarView;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->getMaxDays()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/lltskb/lltskb/view/CalendarView;-><init>(Landroid/content/Context;J)V

    .line 155
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    const-string v3, "\u5e74\u6708\u65e5"

    .line 156
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 157
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz v3, :cond_0

    const v4, 0x7f0e00bd

    .line 159
    invoke-virtual {v3, v4}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 162
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 166
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd"

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 167
    iget-object v4, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 169
    :catch_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 171
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 172
    invoke-virtual {v4, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 173
    invoke-virtual {v4, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v3, 0x2

    invoke-virtual {v4, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/lltskb/lltskb/view/CalendarView;->setSelectedDate(III)V

    .line 175
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$DPfk4Q-pKZ57EeJN8TW9sUBS5Eg;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$DPfk4Q-pKZ57EeJN8TW9sUBS5Eg;-><init>(Lcom/lltskb/lltskb/fragment/SelectTrainFragment;Landroid/support/v7/app/AppCompatDialog;)V

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/view/CalendarView;->setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$1$SelectTrainFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 104
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->click(I)V

    return-void
.end method

.method public synthetic lambda$initView$2$SelectTrainFragment(Landroid/view/View;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$initView$3$SelectTrainFragment(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 117
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$initView$4$SelectTrainFragment(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x0

    .line 123
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$showDateDialog$5$SelectTrainFragment(Landroid/support/v7/app/AppCompatDialog;IIILjava/lang/String;)V
    .locals 2

    .line 177
    sget-object p5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    add-int/2addr p3, p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v0, p2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    const-string p2, "%04d-%02d-%02d"

    invoke-static {p5, p2, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 178
    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {p3, p2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderDate(Ljava/lang/String;)V

    .line 184
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->queryTrain()V

    .line 185
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 225
    :sswitch_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->dismiss()V

    goto :goto_0

    .line 228
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->queryTrain()V

    goto :goto_0

    .line 239
    :sswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->prepareResult()V

    .line 240
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mListener:Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;

    if-eqz p1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;->onSelected(Ljava/lang/String;)V

    .line 243
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->dismiss()V

    goto :goto_0

    .line 231
    :sswitch_3
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 232
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    .line 233
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mListener:Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;

    if-eqz p1, :cond_1

    .line 234
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;->onSelected(Ljava/lang/String;)V

    .line 236
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->dismiss()V

    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090060 -> :sswitch_3
        0x7f090061 -> :sswitch_2
        0x7f090067 -> :sswitch_1
        0x7f0900fb -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b0085

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mRoot:Landroid/view/View;

    .line 73
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mRoot:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->initView(Landroid/view/View;)V

    .line 75
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mRoot:Landroid/view/View;

    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$P1hmSBrvvzQEoQKVFySdhmNVl8s;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectTrainFragment$P1hmSBrvvzQEoQKVFySdhmNVl8s;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mRoot:Landroid/view/View;

    return-object p1
.end method

.method public setListener(Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mListener:Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;

    return-void
.end method

.method public setOrderConfig(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-void
.end method
