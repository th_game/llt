.class public Lcom/lltskb/lltskb/fragment/FeedBackFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "FeedBackFragment.java"


# instance fields
.field private mContent:Landroid/widget/EditText;

.field private mContextText:Ljava/lang/String;

.field private mEmail:Landroid/widget/EditText;

.field private mSubject:Landroid/widget/EditText;

.field private mSubjectText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic lambda$onCreateView$2(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public static newInstance()Lcom/lltskb/lltskb/fragment/FeedBackFragment;
    .locals 1

    .line 31
    new-instance v0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;-><init>()V

    return-object v0
.end method

.method private onSubmit()V
    .locals 5

    .line 82
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubject:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u6807\u9898\u4e0d\u80fd\u4e3a\u7a7a!"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubject:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mEmail:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u90ae\u7bb1\u4e0d\u80fd\u4e3a\u7a7a!"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mEmail:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void

    .line 96
    :cond_1
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmail(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u90ae\u7bb1\u683c\u5f0f\u4e0d\u6b63\u786e!"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mEmail:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void

    .line 102
    :cond_2
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContent:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u5185\u5bb9\u4e0d\u80fd\u4e3a\u7a7a!"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContent:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void

    .line 109
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_4

    .line 110
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "\u5185\u5bb9\u4e0d\u80fd\u5c11\u4e8e5\u4e2a\u5b57!"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContent:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void

    .line 115
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "<br/>"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/utils/Logger;->getCachedLog()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->getFeedBackString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-direct {p0, v0, v1, v2}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->send(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private send(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 123
    new-instance v6, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/FeedBackFragment$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/FeedBackFragment;)V

    move-object v0, v6

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask$Listener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/String;

    const/4 p2, 0x0

    const-string p3, ""

    aput-object p3, p1, p2

    .line 137
    invoke-virtual {v6, p1}, Lcom/lltskb/lltskb/engine/tasks/SendFeedbackTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreateView$0$FeedBackFragment(Landroid/view/View;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->onSubmit()V

    return-void
.end method

.method public synthetic lambda$onCreateView$1$FeedBackFragment(Landroid/view/View;)V
    .locals 0

    .line 72
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->dismiss()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b0040

    const/4 v0, 0x0

    .line 51
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090223

    .line 53
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const p3, 0x7f0d0118

    .line 54
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(I)V

    const p2, 0x7f0900ca

    .line 56
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubject:Landroid/widget/EditText;

    const p2, 0x7f0900bd

    .line 57
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mEmail:Landroid/widget/EditText;

    const p2, 0x7f0900bb

    .line 58
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContent:Landroid/widget/EditText;

    .line 60
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubjectText:Ljava/lang/String;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 61
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubject:Landroid/widget/EditText;

    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubjectText:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 64
    :cond_0
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContextText:Ljava/lang/String;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 65
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContent:Landroid/widget/EditText;

    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContextText:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const p2, 0x7f090053

    .line 68
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 69
    new-instance p3, Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$7T5-NL2jKmssVz5rzpJO8tOTDYQ;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$7T5-NL2jKmssVz5rzpJO8tOTDYQ;-><init>(Lcom/lltskb/lltskb/fragment/FeedBackFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f0900fb

    .line 71
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 72
    new-instance p3, Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$3UvfL2fJF5lRzCpTLE2LanBY08I;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$3UvfL2fJF5lRzCpTLE2LanBY08I;-><init>(Lcom/lltskb/lltskb/fragment/FeedBackFragment;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$Ye5EFNY3EapRaBGultToQFMRQfM;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$FeedBackFragment$Ye5EFNY3EapRaBGultToQFMRQfM;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mContextText:Ljava/lang/String;

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->mSubjectText:Ljava/lang/String;

    return-void
.end method
