.class final Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;
.super Ljava/lang/Object;
.source "HoubuProcessedOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->refreshOrder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 37
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    .line 47
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    const-string v1, "ModelFactory.get().houbuTicketModel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->queryProcessedHOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryProcessedHOrderDTO;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryProcessedHOrderDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderData;->getList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->access$setOrderList$p(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;Ljava/util/List;)V

    .line 51
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1$2;-><init>(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
