.class public Lcom/lltskb/lltskb/fragment/SelectStationFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "SelectStationFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectStationFragment"


# instance fields
.field private mBigGridView:Landroid/widget/GridView;

.field private mBigSectionHeader:Landroid/widget/TextView;

.field private mEditText:Landroid/widget/EditText;

.field private mGridView:Landroid/widget/GridView;

.field private mIsHistory:Ljava/lang/Boolean;

.field private mRequestCode:I

.field private mSectionHeader:Landroid/widget/TextView;

.field private mStationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    const/4 v1, 0x1

    .line 24
    iput v1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mRequestCode:I

    .line 25
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    .line 26
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigGridView:Landroid/widget/GridView;

    .line 32
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mIsHistory:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigSectionHeader:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/GridView;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigGridView:Landroid/widget/GridView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)Landroid/widget/TextView;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mSectionHeader:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$302(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mIsHistory:Ljava/lang/Boolean;

    return-object p1
.end method

.method private initView(Landroid/view/View;)V
    .locals 5

    const-string v0, "SelectStationFragment"

    const-string v1, "initView"

    .line 71
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090223

    .line 72
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 75
    iget v2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mRequestCode:I

    const v3, 0x7f0d0283

    if-eq v2, v1, :cond_1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    const/4 v4, 0x3

    goto :goto_0

    :cond_0
    const v3, 0x7f0d0055

    goto :goto_0

    :cond_1
    const v3, 0x7f0d0280

    .line 86
    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const v0, 0x7f09003b

    .line 89
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigGridView:Landroid/widget/GridView;

    .line 90
    new-instance v0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;-><init>(Landroid/content/Context;Z)V

    .line 92
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 96
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$XM_SsYUjqMahwfrVQ5tDFZzGHyg;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$XM_SsYUjqMahwfrVQ5tDFZzGHyg;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->setListener(Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;)V

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$Nn4WQ9sXRiE0SDpmcJX3klpu0p4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$Nn4WQ9sXRiE0SDpmcJX3klpu0p4;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f0902dd

    .line 113
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mBigSectionHeader:Landroid/widget/TextView;

    const v0, 0x7f0902dc

    .line 114
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mSectionHeader:Landroid/widget/TextView;

    const v0, 0x7f0901db

    .line 116
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    const v0, 0x7f0900ac

    .line 119
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    .line 120
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    .line 121
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$dlHQlpg41aJelaoHqmTyPsHF82k;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$dlHQlpg41aJelaoHqmTyPsHF82k;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 133
    new-instance p1, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;-><init>(Landroid/content/Context;Z)V

    .line 135
    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$f348Mgr3ur4Qkp1xUouFjgM7yCw;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$f348Mgr3ur4Qkp1xUouFjgM7yCw;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->setListener(Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;)V

    .line 143
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 144
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$8_ESl_4kdQyaypp_91XVE01TeSk;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$8_ESl_4kdQyaypp_91XVE01TeSk;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 187
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->setGridViewLongClick(Landroid/widget/GridView;)V

    return-void
.end method

.method static synthetic lambda$setGridViewLongClick$6(Landroid/widget/GridView;Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 0

    .line 205
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/GridView;->setTag(Ljava/lang/Object;)V

    .line 206
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "onItemLongClick position="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SelectStationFragment"

    invoke-static {p1, p0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    return p0
.end method

.method private onDeleteHistory(I)V
    .locals 2

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDeleteHistory position ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectStationFragment"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->removeMri(I)V

    .line 194
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p1}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    if-eqz p1, :cond_0

    .line 196
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    const-string v0, "SelectStationFragment"

    const-string v1, "dismiss"

    .line 60
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :try_start_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    :goto_0
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$initView$1$SelectStationFragment(Ljava/lang/String;)V
    .locals 3

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onItemSelected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectStationFragment"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const-string v1, " "

    const-string v2, ""

    .line 99
    invoke-static {p1, v1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$initView$2$SelectStationFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    const-string p2, "SelectStationFragment"

    const-string p4, "onItemClick "

    .line 105
    invoke-static {p2, p4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 107
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const-string p3, " "

    const-string p4, ""

    .line 108
    invoke-static {p1, p3, p4}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$initView$3$SelectStationFragment(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 p1, 0x6

    if-ne p2, p1, :cond_1

    .line 123
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 124
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const-string p3, " "

    const-string v0, ""

    .line 125
    invoke-static {p1, p3, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$initView$4$SelectStationFragment(Ljava/lang/String;)V
    .locals 3

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onItemSelected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectStationFragment"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const-string v1, " "

    const-string v2, ""

    .line 138
    invoke-static {p1, v1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$initView$5$SelectStationFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    const-string p2, "SelectStationFragment"

    const-string p4, "onItemClick "

    .line 179
    invoke-static {p2, p4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 181
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const-string p3, " "

    const-string p4, ""

    .line 182
    invoke-static {p1, p3, p4}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$7$SelectStationFragment(Landroid/widget/GridView;Landroid/view/MenuItem;)Z
    .locals 0

    .line 218
    invoke-virtual {p1}, Landroid/widget/GridView;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 219
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->onDeleteHistory(I)V

    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$null$8$SelectStationFragment(Landroid/view/MenuItem;)Z
    .locals 0

    const/4 p1, -0x1

    .line 224
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->onDeleteHistory(I)V

    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$onCreateView$0$SelectStationFragment(Landroid/view/View;)V
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V

    return-void
.end method

.method public synthetic lambda$setGridViewLongClick$9$SelectStationFragment(Landroid/widget/GridView;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .line 210
    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mIsHistory:Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-nez p3, :cond_0

    return-void

    :cond_0
    const p3, 0x7f0d021a

    .line 213
    invoke-interface {p2, p3}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 215
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, " pos="

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/widget/GridView;->getTag()Ljava/lang/Object;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string p4, "SelectStationFragment"

    invoke-static {p4, p3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p3, 0x7f0d00cf

    .line 217
    invoke-interface {p2, p3}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object p3

    new-instance p4, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$E6hAGyQP2EbmhhgawMN4ceW3Hf0;

    invoke-direct {p4, p0, p1}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$E6hAGyQP2EbmhhgawMN4ceW3Hf0;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Landroid/widget/GridView;)V

    invoke-interface {p3, p4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const p1, 0x7f0d00a9

    .line 223
    invoke-interface {p2, p1}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object p1

    new-instance p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$5WOkihFevu9jeVuvrQx8U-wXTEQ;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$5WOkihFevu9jeVuvrQx8U-wXTEQ;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 p3, 0x0

    const v0, 0x7f0b008c

    .line 47
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 48
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->initView(Landroid/view/View;)V

    .line 50
    new-instance p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$afrWoLg1YOGZXDqsFQo-vkafEWY;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$afrWoLg1YOGZXDqsFQo-vkafEWY;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x1

    .line 51
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mIsHistory:Ljava/lang/Boolean;

    .line 52
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mEditText:Landroid/widget/EditText;

    if-eqz p2, :cond_0

    .line 53
    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setLongClickable(Z)V

    :cond_0
    return-object p1
.end method

.method protected setGridViewLongClick(Landroid/widget/GridView;)V
    .locals 2

    const-string v0, "SelectStationFragment"

    const-string v1, "setGridViewLongClick"

    .line 201
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 204
    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$-HQr18V5zbhPJn0BQU3r-MpFuLo;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$-HQr18V5zbhPJn0BQU3r-MpFuLo;-><init>(Landroid/widget/GridView;)V

    invoke-virtual {p1, v0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 209
    new-instance v0, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$py5AuIKHlFA2RD2SBhF8Uj_kx0E;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectStationFragment$py5AuIKHlFA2RD2SBhF8Uj_kx0E;-><init>(Lcom/lltskb/lltskb/fragment/SelectStationFragment;Landroid/widget/GridView;)V

    invoke-virtual {p1, v0}, Landroid/widget/GridView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    :cond_0
    return-void
.end method

.method public setTextView(Landroid/widget/TextView;I)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mStationView:Landroid/widget/TextView;

    .line 41
    iput p2, p0, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->mRequestCode:I

    return-void
.end method
