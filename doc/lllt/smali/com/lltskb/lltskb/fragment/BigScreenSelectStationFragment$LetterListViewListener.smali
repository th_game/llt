.class Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;
.super Ljava/lang/Object;
.source "BigScreenSelectStationFragment.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LetterListViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;)V
    .locals 0

    .line 165
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;-><init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)V

    return-void
.end method


# virtual methods
.method public touchLetterChanged(Ljava/lang/String;)V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$900(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->getStationMap()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 170
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 171
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 172
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$LetterListViewListener;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$300(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    return-void
.end method
