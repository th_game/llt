.class public Lcom/lltskb/lltskb/fragment/MyOrderListFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "MyOrderListFragment.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MyOrderListFragment"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

.field private mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)Lcom/lltskb/lltskb/view/widget/XListView;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->startLogin()V

    return-void
.end method

.method private register()V
    .locals 3

    const-string v0, "MyOrderListFragment"

    const-string v1, "register"

    .line 123
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 125
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 126
    new-instance v1, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 145
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 147
    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    .line 148
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    :cond_0
    return-void
.end method

.method private showOrderDetails(I)V
    .locals 4

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showOrderDetails position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MyOrderListFragment"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 86
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, -0x1

    if-ltz p1, :cond_4

    .line 89
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 95
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragment_myorder_complete_detail"

    .line 96
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;

    if-nez v2, :cond_3

    .line 98
    new-instance v2, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;-><init>()V

    .line 101
    :cond_3
    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/fragment/MyOrderDetailListFragment;->setOrderData(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V

    .line 102
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x7f010013

    const v3, 0x7f010014

    .line 103
    invoke-virtual {p1, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    const v0, 0x7f0900df

    .line 104
    invoke-virtual {p1, v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    const/4 v0, 0x0

    .line 105
    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 106
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_4
    :goto_0
    return-void
.end method

.method private startLogin()V
    .locals 3

    const-string v0, "MyOrderListFragment"

    const-string v1, "startLogin"

    .line 154
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->register()V

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 159
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreateView$0$MyOrderListFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 64
    invoke-direct {p0, p3}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->showOrderDetails(I)V

    return-void
.end method

.method public synthetic lambda$onCreateView$1$MyOrderListFragment(Landroid/view/View;)V
    .locals 0

    .line 67
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->dismiss()V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "MyOrderListFragment"

    const-string v1, "onActivityCreated"

    .line 111
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->onRefresh()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const-string p3, "MyOrderListFragment"

    const-string v0, "onCreateView"

    .line 53
    invoke-static {p3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p3, 0x0

    const v0, 0x7f0b002b

    .line 54
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090187

    .line 55
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 56
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 57
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const-string v1, ""

    invoke-virtual {p2, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 58
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 59
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    .line 60
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p2, p0}, Lcom/lltskb/lltskb/view/widget/XListView;->setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V

    .line 62
    new-instance p2, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

    .line 63
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object p3, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    new-instance p3, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$DqIbZBaONU76NgKoobfLMjIV5y0;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$DqIbZBaONU76NgKoobfLMjIV5y0;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/view/widget/XListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const p2, 0x7f0900fb

    .line 66
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 67
    new-instance p3, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$EYYqbqBZUIo7saQ8GGxwBGiDm1U;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MyOrderListFragment$EYYqbqBZUIo7saQ8GGxwBGiDm1U;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090223

    .line 69
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const p3, 0x7f0d00af

    .line 70
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(I)V

    .line 72
    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 73
    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const p2, 0x7f090228

    .line 75
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    const/16 p3, 0x8

    .line 77
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-object p1
.end method

.method public onLoadMore()V
    .locals 0

    return-void
.end method

.method public onRefresh()V
    .locals 9

    const-string v0, "MyOrderListFragment"

    const-string v1, "onRefresh"

    .line 164
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0902e3

    .line 171
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez v1, :cond_1

    return-void

    .line 173
    :cond_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const v1, 0x7f09027f

    .line 175
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez v1, :cond_2

    return-void

    .line 177
    :cond_2
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f0901ba

    .line 179
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    if-nez v1, :cond_3

    return-void

    .line 181
    :cond_3
    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "G"

    goto :goto_0

    :cond_4
    const-string v1, "H"

    :goto_0
    move-object v5, v1

    const v1, 0x7f0900b9

    .line 183
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    if-nez v1, :cond_5

    return-void

    .line 185
    :cond_5
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const v1, 0x7f0901b9

    .line 186
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-nez v0, :cond_6

    return-void

    .line 188
    :cond_6
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_7

    const/4 v4, 0x1

    goto :goto_1

    :cond_7
    const/4 v0, 0x2

    const/4 v4, 0x2

    .line 190
    :goto_1
    new-instance v0, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;

    move-object v2, v0

    move-object v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;-><init>(Lcom/lltskb/lltskb/fragment/MyOrderListFragment;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v1, v3

    invoke-virtual {v0, v2, v1}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment$QueryMyOrderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
