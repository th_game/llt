.class public final Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "HoubuProcessedOrderFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "houbuOrderLinearLayout",
        "Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;",
        "getHoubuOrderLinearLayout",
        "()Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 137
    check-cast p1, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    return-void
.end method


# virtual methods
.method public final getHoubuOrderLinearLayout()Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter$ViewHolder;->houbuOrderLinearLayout:Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;

    return-object v0
.end method
