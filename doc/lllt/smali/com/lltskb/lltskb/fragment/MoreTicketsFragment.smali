.class public Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "MoreTicketsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;
    }
.end annotation


# static fields
.field public static final ARG_DATE:Ljava/lang/String; = "date"

.field public static final ARG_FROM:Ljava/lang/String; = "from"

.field public static final ARG_STARTDATE:Ljava/lang/String; = "start_date"

.field public static final ARG_TO:Ljava/lang/String; = "to"

.field private static final TAG:Ljava/lang/String; = "MoreTicketsFragment"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mDate:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private mRoot:Landroid/view/View;

.field private mStartDate:Ljava/lang/String;

.field private mTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTo:Ljava/lang/String;

.field private mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Landroid/view/View;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->queryTicket(Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private initData()V
    .locals 14

    .line 147
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    .line 155
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    const-string v1, "MoreTicketsFragment"

    if-nez v0, :cond_0

    const-string v0, "initData mTrainDTO is empty"

    .line 156
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 162
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mStartDate:Ljava/lang/String;

    .line 164
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_list:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v10, v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, v2

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;

    add-int/2addr v2, v4

    if-ne v2, v4, :cond_1

    .line 167
    iget-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->start_time:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->arrive_time:Ljava/lang/String;

    :goto_1
    if-eqz v3, :cond_2

    .line 170
    invoke-virtual {v3, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    add-int/lit8 v6, v6, 0x1

    .line 172
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mStartDate:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/lltskb/lltskb/utils/StringUtils;->getDate(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    move-object v10, v3

    .line 178
    :cond_2
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iget-object v13, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mFrom:Ljava/lang/String;

    invoke-virtual {v3, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v8, v2

    move-object v3, v12

    const/4 v9, 0x1

    goto :goto_0

    .line 185
    :cond_3
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iget-object v13, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTo:Ljava/lang/String;

    invoke-virtual {v3, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v7, v2

    :cond_4
    if-nez v9, :cond_5

    .line 191
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;-><init>()V

    .line 192
    iget-object v13, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iput-object v13, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mFrom:Ljava/lang/String;

    .line 193
    iget-object v13, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTo:Ljava/lang/String;

    iput-object v13, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mCurTo:Ljava/lang/String;

    .line 194
    iput v2, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    .line 195
    iput-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIsFromStart:Z

    .line 196
    iput-object v10, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mDate:Ljava/lang/String;

    .line 197
    iput v5, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    .line 198
    iget-object v13, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v13, v3, v5}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :cond_5
    if-nez v9, :cond_6

    goto :goto_2

    .line 204
    :cond_6
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;-><init>()V

    .line 205
    iget-object v11, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iput-object v11, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mCurTo:Ljava/lang/String;

    .line 206
    iput v2, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    .line 207
    iget-object v11, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mFrom:Ljava/lang/String;

    iput-object v11, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mFrom:Ljava/lang/String;

    .line 208
    iget-object v11, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mDate:Ljava/lang/String;

    iput-object v11, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mDate:Ljava/lang/String;

    .line 209
    iput-boolean v5, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIsFromStart:Z

    .line 210
    iput v5, v3, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    .line 211
    iget-object v11, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v11, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v3, v12

    goto :goto_0

    .line 226
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "initData mData is empty"

    .line 227
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 231
    :cond_8
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    .line 232
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIsFromStart:Z

    if-eqz v3, :cond_9

    .line 233
    iget v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    sub-int v3, v8, v3

    iput v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    goto :goto_3

    .line 235
    :cond_9
    iget v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    sub-int/2addr v3, v7

    iput v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    goto :goto_3

    .line 239
    :cond_a
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    sget-object v2, Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$PXaJK-KA4xtMGV7QY7AwrlcntKc;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$PXaJK-KA4xtMGV7QY7AwrlcntKc;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initData size="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private initView()V
    .locals 4

    const-string v0, "MoreTicketsFragment"

    const-string v1, "initView"

    .line 77
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    const-string v2, "from"

    .line 81
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mFrom:Ljava/lang/String;

    const-string v2, "to"

    .line 82
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTo:Ljava/lang/String;

    const-string v2, "date"

    .line 83
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mDate:Ljava/lang/String;

    const-string v2, "start_date"

    .line 84
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mStartDate:Ljava/lang/String;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    const v1, 0x7f0900fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    const v1, 0x7f090223

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d01d0

    .line 91
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 93
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    const v1, 0x7f090067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->initData()V

    .line 99
    new-instance v0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mFrom:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTo:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    const v1, 0x7f090188

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 101
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->setData(Ljava/util/Vector;)V

    .line 104
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->startQueryTask()V

    .line 106
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$bXCnV6j22_H7sQFYQsBvwwAkcHk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$bXCnV6j22_H7sQFYQsBvwwAkcHk;-><init>(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private isRunning()Z
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$initData$2(Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;)I
    .locals 0

    .line 239
    iget p0, p0, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    iget p1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private onRefresh()V
    .locals 2

    .line 262
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    return-void

    .line 267
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->startQueryTask()V

    return-void
.end method

.method private onSelectItem(I)V
    .locals 3

    .line 110
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    .line 118
    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    return-void

    .line 122
    :cond_2
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    const/4 v0, 0x0

    .line 123
    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mFrom:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mCurTo:Ljava/lang/String;

    const-string v2, "ticket_arrive_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->train_no:Ljava/lang/String;

    const-string v2, "train_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_train_code:Ljava/lang/String;

    const-string v2, "train_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mDate:Ljava/lang/String;

    const-string v1, "ticket_date"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_train_date:Ljava/lang/String;

    const-string v1, "start_train_date"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, 0x0

    const-string v1, "show_more_ticket"

    .line 134
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 135
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 137
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_3
    return-void
.end method

.method private queryTicket(Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;)Ljava/lang/String;
    .locals 6

    .line 291
    new-instance v0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;-><init>()V

    .line 293
    :try_start_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mFrom:Ljava/lang/String;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mCurTo:Ljava/lang/String;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mDate:Ljava/lang/String;

    const-string v4, "ADULT"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 294
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getResult()Ljava/util/Vector;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "\u5f53\u524d\u65e5\u671f\u6ca1\u6709\u627e\u5230\u8be5\u8f66\u6b21"

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v2, 0x0

    .line 307
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 309
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->train_no:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_0

    :cond_1
    move-object v2, v3

    :cond_2
    if-nez v2, :cond_3

    return-object v1

    .line 320
    :cond_3
    iput-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 322
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->toHtmlString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 296
    :cond_4
    :try_start_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 299
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private startQueryTask()V
    .locals 5

    .line 277
    new-instance v0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;-><init>(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    .line 279
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .line 326
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 329
    :cond_0
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->dismiss()V

    return-void
.end method

.method public synthetic lambda$initView$1$MoreTicketsFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 106
    invoke-direct {p0, p3}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->onSelectItem(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 247
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f090067

    if-eq p1, v0, :cond_1

    const v0, 0x7f0900fb

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->dismiss()V

    goto :goto_0

    .line 252
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->onRefresh()V

    :goto_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b005d

    const/4 v0, 0x0

    .line 65
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    .line 67
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$PrYF4erog2Ijdvwm-5OV1MYDlaA;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$MoreTicketsFragment$PrYF4erog2Ijdvwm-5OV1MYDlaA;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->initView()V

    .line 73
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mRoot:Landroid/view/View;

    return-object p1
.end method

.method public setTrainDTO(Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    return-void
.end method
