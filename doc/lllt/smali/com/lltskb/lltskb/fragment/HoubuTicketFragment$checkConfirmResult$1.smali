.class final Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;
.super Ljava/lang/Object;
.source "HoubuTicketFragment.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->checkConfirmResult(Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 152
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    const-string v0, "ModelFactory.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object p1

    .line 153
    invoke-interface {p1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 154
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->access$getListener$p(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;->onOrderSubmit()V

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->dismiss()V

    .line 158
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
