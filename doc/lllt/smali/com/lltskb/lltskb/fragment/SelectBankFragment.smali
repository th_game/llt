.class public Lcom/lltskb/lltskb/fragment/SelectBankFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "SelectBankFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final BANKID_ALIPAY:Ljava/lang/String; = "33000010"

.field private static final BANKID_CCB:Ljava/lang/String; = "01050000"

.field private static final BANKID_CMB:Ljava/lang/String; = "03080000"

.field private static final BANKID_OTHER:Ljava/lang/String; = "00000000"

.field private static final BANKID_WEIXIN:Ljava/lang/String; = "33000020"

.field private static final TAG:Ljava/lang/String; = "SelectBankFragment"


# instance fields
.field private mBankId:Ljava/lang/String;

.field private mIsAlipayClient:Z

.field private mIsGoToPay:Z

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 141
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsGoToPay:Z

    const/4 v1, 0x0

    .line 80
    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    .line 88
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsAlipayClient:Z

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/fragment/SelectBankFragment;Z)Z
    .locals 0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsGoToPay:Z

    return p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->startLogin()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Z
    .locals 0

    .line 41
    iget-boolean p0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsAlipayClient:Z

    return p0
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private onAbc()V
    .locals 1

    const-string v0, "01030000"

    .line 366
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onAlipay()V
    .locals 1

    const-string v0, "33000010"

    .line 370
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onAlipayClient()V
    .locals 1

    const-string v0, "33000010"

    .line 378
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    const/4 v0, 0x1

    .line 379
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsAlipayClient:Z

    return-void
.end method

.method private onBoc()V
    .locals 1

    const-string v0, "01040000"

    .line 362
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onCcb()V
    .locals 1

    const-string v0, "01050000"

    .line 358
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onCmb()V
    .locals 1

    const-string v0, "03080000"

    .line 354
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onIcbc()V
    .locals 1

    const-string v0, "01020000"

    .line 350
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onOther()V
    .locals 1

    const-string v0, "00000000"

    .line 337
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onUnion()V
    .locals 1

    const-string v0, "00011000"

    .line 346
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onWeixin()V
    .locals 1

    const-string v0, "33000020"

    .line 374
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private onZtyt()V
    .locals 1

    const-string v0, "00011001"

    .line 341
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mBankId:Ljava/lang/String;

    return-void
.end method

.method private procPayGateway()V
    .locals 4

    const-string v0, "SelectBankFragment"

    const-string v1, "procPayGateway"

    .line 175
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;-><init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    .line 287
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private register()V
    .locals 3

    const-string v0, "SelectBankFragment"

    const-string v1, "register"

    .line 146
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 148
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectBankFragment$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment$2;-><init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 163
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 164
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private startLogin()V
    .locals 3

    const-string v0, "SelectBankFragment"

    const-string v1, "startLogin"

    .line 168
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->register()V

    .line 170
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 293
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsAlipayClient:Z

    .line 294
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0900fb

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 326
    :pswitch_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onZtyt()V

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onWeixin()V

    goto :goto_0

    .line 323
    :pswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onUnion()V

    goto :goto_0

    .line 329
    :pswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onOther()V

    goto :goto_0

    .line 320
    :pswitch_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onIcbc()V

    goto :goto_0

    .line 317
    :pswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onCmb()V

    goto :goto_0

    .line 314
    :pswitch_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onCcb()V

    goto :goto_0

    .line 311
    :pswitch_7
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onBoc()V

    goto :goto_0

    .line 299
    :pswitch_8
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onAlipayClient()V

    goto :goto_0

    .line 302
    :pswitch_9
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onAlipay()V

    goto :goto_0

    .line 308
    :pswitch_a
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onAbc()V

    .line 333
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->procPayGateway()V

    return-void

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->dismiss()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7f090115
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b0021

    const/4 v0, 0x0

    .line 94
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090223

    .line 96
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const p3, 0x7f0d01f7

    .line 97
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(I)V

    const p2, 0x7f0900fb

    .line 99
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 100
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090117

    .line 102
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 103
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090116

    .line 105
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 106
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011e

    .line 108
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 109
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011a

    .line 111
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 112
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090115

    .line 114
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 115
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011b

    .line 117
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 118
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090118

    .line 120
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 121
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090119

    .line 123
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 124
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011d

    .line 126
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 127
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011f

    .line 129
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 130
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09011c

    .line 132
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 133
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectBankFragment$t7qSO9qmGISGBN4rrhNBVJdcBZA;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectBankFragment$t7qSO9qmGISGBN4rrhNBVJdcBZA;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public onResume()V
    .locals 4

    .line 52
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onResume()V

    .line 54
    iget-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->mIsGoToPay:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V

    const-string v2, "\u652f\u4ed8\u7ed3\u679c"

    const-string v3, "\u662f\u5426\u5df2\u7ecf\u6210\u529f\u652f\u4ed8?"

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :cond_0
    return-void
.end method
