.class final Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;
.super Ljava/lang/Object;
.source "HoubuWaitingOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->cancelOrder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuWaitingOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuWaitingOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1\n*L\n1#1,200:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 122
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-static {v1}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->access$getQueryQueueOrder$p(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getReserve_no()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->reserveReturnCheck(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 125
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment$cancelOrder$1;Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
