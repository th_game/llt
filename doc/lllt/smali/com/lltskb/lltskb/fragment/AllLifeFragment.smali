.class public Lcom/lltskb/lltskb/fragment/AllLifeFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "AllLifeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public static newInstance()Landroid/support/v4/app/Fragment;
    .locals 1

    .line 30
    new-instance v0, Lcom/lltskb/lltskb/fragment/AllLifeFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/AllLifeFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .line 124
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLatitude()D

    move-result-wide v0

    .line 125
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLongitude()D

    move-result-wide v2

    .line 126
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const-string v4, "qunar"

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x2

    const-string v8, "tongcheng"

    const-string v9, "\u70b9\u51fb"

    sparse-switch p1, :sswitch_data_0

    const-string p1, "http://www.17u.cn/scenery/#refid=17983686"

    goto/16 :goto_0

    :sswitch_0
    const-string p1, "http://r.union.meituan.com/url/visit/?a=1&key=90f3f6fec96edbd161847183ca9ba6a4414&url=http%3A%2F%2Fi.meituan.com%2F%3Ftype_v3%3D162%26nodown"

    goto/16 :goto_0

    :sswitch_1
    const-string p1, "http://wap.lltskb.com/zpxx"

    goto/16 :goto_0

    :sswitch_2
    const-string p1, "http://touch.qunar.com/h5/flight/flightorderqmc?bd_source=lulutong"

    goto/16 :goto_0

    :sswitch_3
    const-string p1, "https://h5.m.taobao.com/trip/car/search/index.html?ttid=12oap0000083"

    goto/16 :goto_0

    .line 144
    :sswitch_4
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "xingyuan"

    invoke-static {p1, v0, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "https://h5.qichedaquan.com/?utm_source=lulutong"

    goto/16 :goto_0

    .line 156
    :sswitch_5
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "mtlb"

    invoke-static {p1, v0, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://wap.lltskb.com/mtlb.html"

    goto/16 :goto_0

    :sswitch_6
    const-string p1, "http://wap.lltskb.com/9188/caipiao.html"

    goto/16 :goto_0

    :sswitch_7
    const-string p1, "http://ks.lltskb.com"

    goto/16 :goto_0

    .line 148
    :sswitch_8
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-static {p1, v8, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://jump.luna.58.com/i/26QL"

    goto/16 :goto_0

    :sswitch_9
    const-string p1, "http://m.jingzhengu.com/xiansuo/sellcar-lulutong.html"

    goto/16 :goto_0

    .line 128
    :sswitch_a
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v0, "http://i.58.com/HIh?lat=%f&lon=%f"

    invoke-static {p1, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 129
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u5de5\u4f5c"

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    :sswitch_b
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v0, "http://i.58.com/HIk?lat=%f&lon=%f"

    invoke-static {p1, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 133
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u623f\u5b50"

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :sswitch_c
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "\u9152\u5e97"

    invoke-static {p1, v4, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://touch.qunar.com/h5/hotel/?bd_source=lulutong"

    goto :goto_0

    .line 136
    :sswitch_d
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v0, "http://i.58.com/HIq?lat=%f&lon=%f"

    invoke-static {p1, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 137
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u5bb6\u653f"

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :sswitch_e
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-static {p1, v4, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://touch.qunar.com/h5/flight/?bd_source=lulutong"

    goto :goto_0

    :sswitch_f
    const-string p1, "http://wap.lltskb.com/ditie"

    goto :goto_0

    .line 168
    :sswitch_10
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "wangyibx"

    invoke-static {p1, v0, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://baoxian.163.com/activity/zxtpl/index.html?actiId=2016120718act215696590&remark=lulutong1"

    goto :goto_0

    .line 184
    :sswitch_11
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "baidunews"

    invoke-static {p1, v0, v9}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://cpu.baidu.com/1032/1003cd5d"

    goto :goto_0

    .line 193
    :sswitch_12
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/AllLifeFragment;->dismiss()V

    return-void

    .line 201
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/AllLifeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0900fb -> :sswitch_12
        0x7f090114 -> :sswitch_11
        0x7f090120 -> :sswitch_10
        0x7f09012e -> :sswitch_f
        0x7f090133 -> :sswitch_e
        0x7f090137 -> :sswitch_d
        0x7f090138 -> :sswitch_c
        0x7f090139 -> :sswitch_b
        0x7f09013c -> :sswitch_a
        0x7f09013d -> :sswitch_9
        0x7f09013e -> :sswitch_8
        0x7f090140 -> :sswitch_7
        0x7f090141 -> :sswitch_6
        0x7f090145 -> :sswitch_5
        0x7f09014f -> :sswitch_4
        0x7f090150 -> :sswitch_3
        0x7f090153 -> :sswitch_2
        0x7f09015e -> :sswitch_1
        0x7f090161 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b0020

    const/4 v0, 0x0

    .line 36
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string p2, "58tongcheng"

    .line 38
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result p2

    const p3, 0x7f090223

    .line 40
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    const v0, 0x7f0d004d

    .line 41
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    const p3, 0x7f090133

    .line 43
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 44
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f090138

    .line 46
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 47
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09013c

    .line 49
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 50
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v0, 0x8

    if-nez p2, :cond_0

    .line 52
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const p3, 0x7f090139

    .line 55
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 56
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p2, :cond_1

    .line 58
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const p3, 0x7f090141

    .line 61
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 62
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09014f

    .line 64
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 65
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09013d

    .line 67
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 68
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09013e

    .line 70
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 71
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p2, :cond_2

    .line 73
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const p3, 0x7f090137

    .line 76
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 77
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p2, :cond_3

    .line 79
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const p2, 0x7f090120

    .line 82
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 83
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f090161

    .line 85
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 86
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f090153

    .line 88
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 89
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f090150

    .line 91
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 92
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09015e

    .line 94
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 95
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f090145

    .line 97
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 98
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 101
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090114

    .line 103
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 104
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f09012e

    .line 106
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 107
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f090140

    .line 109
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 110
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f0900fb

    .line 112
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 113
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$AllLifeFragment$D4fuB4BruaaS2fQOjFPAiP3tGH8;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$AllLifeFragment$D4fuB4BruaaS2fQOjFPAiP3tGH8;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method
