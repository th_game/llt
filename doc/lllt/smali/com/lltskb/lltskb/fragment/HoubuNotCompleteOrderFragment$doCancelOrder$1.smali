.class final Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;
.super Ljava/lang/Object;
.source "HoubuNotCompleteOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->doCancelOrder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuNotCompleteOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuNotCompleteOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1\n*L\n1#1,265:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 183
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 184
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-static {v1}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->access$getQueryQueueDTO$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getReserve_no()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->cancelNotComplete(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 186
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doCancelOrder$1;Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
