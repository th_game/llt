.class Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;
.super Ljava/lang/Object;
.source "SelectBankFragment.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/SelectBankFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$002(Lcom/lltskb/lltskb/fragment/SelectBankFragment;Z)Z

    return-void
.end method

.method public onYes()V
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->dismiss()V

    .line 60
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SelectBankFragment"

    const-string v1, "getActivity is null"

    .line 62
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 65
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/lltskb/lltskb/order/CompleteOrderActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x1

    const-string v3, "auto_query"

    .line 66
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 68
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method
