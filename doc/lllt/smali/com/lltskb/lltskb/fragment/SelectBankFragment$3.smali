.class Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;
.super Landroid/os/AsyncTask;
.source "SelectBankFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/SelectBankFragment;->procPayGateway()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsPost:Z

.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x1

    .line 178
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->mIsPost:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    const/4 p1, 0x1

    .line 245
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->mIsPost:Z

    .line 246
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->get()Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    move-result-object p1

    .line 247
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 248
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 251
    :try_start_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 254
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 256
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->continuePayNoCompleteMyOrder(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 259
    :cond_1
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->payCheckNew()I

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string v0, "00000000"

    .line 262
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$300(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 263
    new-instance p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    invoke-direct {p1}, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;-><init>()V

    .line 264
    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_PAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    return-object p1

    .line 269
    :cond_3
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->payGateway()Ljava/lang/String;

    .line 270
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$300(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->webBussiness(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object v0

    const-string v2, "03080000"

    .line 271
    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v3}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$300(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    return-object v0

    .line 274
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$400(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 275
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getAlipayClient(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object v0

    .line 276
    iput-boolean v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->mIsPost:Z
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    return-object v0

    :catch_0
    move-exception p1

    .line 280
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 281
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_6
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .line 196
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 197
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const/4 v0, 0x0

    const v1, 0x7f0d010f

    if-nez p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 201
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    const v3, 0x7f0d0214

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 205
    :cond_0
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 206
    check-cast p1, Ljava/lang/String;

    const-string v2, "\u7528\u6237\u672a\u767b\u5f55"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$200(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)V

    return-void

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 214
    :cond_2
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    if-eqz v0, :cond_6

    .line 215
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 219
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$300(Lcom/lltskb/lltskb/fragment/SelectBankFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "00000000"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 223
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->get()Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    move-result-object v1

    .line 224
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getPayGatewayParams()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_4
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    .line 229
    iget-boolean v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->mIsPost:Z

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "?"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 233
    :cond_5
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "web_url"

    .line 234
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "web_params"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    iget-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->mIsPost:Z

    const-string v0, "web_post"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 p1, 0x1

    const-string v0, "web_closeonback"

    .line 237
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 239
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$002(Lcom/lltskb/lltskb/fragment/SelectBankFragment;Z)Z

    :cond_6
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 181
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0234

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3$1;-><init>(Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;)V

    const/4 v3, -0x1

    invoke-static {v1, v0, v3, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 190
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/SelectBankFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;->access$002(Lcom/lltskb/lltskb/fragment/SelectBankFragment;Z)Z

    .line 191
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
