.class public Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "QueryBaoxianFragment.java"


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)Ljava/util/Map;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mData:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->onDisplayItem(I)V

    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0900fb

    .line 59
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$QueryBaoxianFragment$mTWmR19_l4Kb7zCz0cKVJ63yKf0;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$QueryBaoxianFragment$mTWmR19_l4Kb7zCz0cKVJ63yKf0;-><init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f090223

    .line 64
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d0225

    .line 65
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f09017e

    .line 68
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    .line 69
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->initBaoxian(Landroid/content/Context;)Z

    .line 70
    invoke-static {}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getbaoxian()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mData:Ljava/util/Map;

    .line 71
    new-instance v0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    new-instance v0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$2;-><init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private onDisplayItem(I)V
    .locals 3

    .line 134
    new-instance v0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;-><init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V

    .line 177
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mData:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_2

    return-void

    .line 187
    :cond_2
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-nez p1, :cond_3

    return-void

    :cond_3
    const/16 v1, 0x7c

    .line 191
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_4

    return-void

    :cond_4
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 196
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$1$QueryBaoxianFragment(Landroid/view/View;)V
    .locals 0

    .line 61
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->dismiss()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0b0023

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mRootView:Landroid/view/View;

    .line 47
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mRootView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->initView(Landroid/view/View;)V

    .line 49
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mRootView:Landroid/view/View;

    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$QueryBaoxianFragment$Lj0VT3ICiHHa_3Ux8ZOxM1LD_zc;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$QueryBaoxianFragment$Lj0VT3ICiHHa_3Ux8ZOxM1LD_zc;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->mRootView:Landroid/view/View;

    return-object p1
.end method
