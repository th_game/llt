.class public Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "SelectPassengersFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;
    }
.end annotation


# static fields
.field public static final PURPOSE:Ljava/lang/String; = "purpose"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

.field private mListener:Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;

.field private mPurpose:Ljava/lang/String;

.field private mRefreshPassenger:Z

.field private maxCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    const/4 v0, 0x5

    .line 38
    iput v0, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->maxCount:I

    const/4 v0, 0x0

    .line 47
    iput-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mRefreshPassenger:Z

    return-void
.end method

.method private checkSelected()Z
    .locals 3

    .line 114
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d0057

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v2

    .line 122
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 123
    iget-boolean v1, v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-nez v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_3

    .line 129
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d009e

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :cond_3
    return v2
.end method

.method private initView(Landroid/view/View;)V
    .locals 4

    const v0, 0x7f090223

    .line 77
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mPurpose:Ljava/lang/String;

    const-string v2, "0X00"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0d0264

    .line 79
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    const v1, 0x7f0d0261

    .line 81
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v0, 0x7f0900fb

    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090181

    .line 87
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 89
    new-instance v1, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mPurpose:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    .line 90
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    iget v2, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->maxCount:I

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->setMaCount(I)V

    .line 91
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    new-instance v1, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectPassengersFragment$FOFUfZe1vYYSY4Jw8HTvAy6Ya-Y;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectPassengersFragment$FOFUfZe1vYYSY4Jw8HTvAy6Ya-Y;-><init>(Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f090042

    .line 94
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 95
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090061

    .line 96
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 97
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 100
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mRefreshPassenger:Z

    return-void
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 161
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$initView$1$SelectPassengersFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 92
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->click(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f090042

    if-eq p1, v0, :cond_4

    const v0, 0x7f090061

    if-eq p1, v0, :cond_1

    const v0, 0x7f0900fb

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->dismiss()V

    goto :goto_0

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->checkSelected()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    .line 151
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mListener:Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;

    if-eqz p1, :cond_3

    .line 152
    invoke-interface {p1}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;->onPassengersSelected()V

    .line 154
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->dismiss()V

    goto :goto_0

    :cond_4
    const/4 p1, 0x1

    .line 141
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mRefreshPassenger:Z

    .line 142
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/order/EditContactActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 145
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    if-eqz p3, :cond_0

    const-string v0, "purpose"

    .line 64
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mPurpose:Ljava/lang/String;

    :cond_0
    const p3, 0x7f0b0084

    const/4 v0, 0x0

    .line 67
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 68
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->initView(Landroid/view/View;)V

    .line 70
    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectPassengersFragment$kM2RjVx7vm_FVYLcs0bP4BPeqSo;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$SelectPassengersFragment$kM2RjVx7vm_FVYLcs0bP4BPeqSo;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public onQueryCompleted(I)V
    .locals 0

    .line 166
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mAdapter:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    if-eqz p1, :cond_0

    .line 167
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    :cond_0
    const/4 p1, 0x0

    .line 169
    iput-boolean p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mRefreshPassenger:Z

    return-void
.end method

.method public onStart()V
    .locals 4

    .line 106
    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onStart()V

    .line 107
    iget-boolean v0, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mRefreshPassenger:Z

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 109
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public setListener(Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->mListener:Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;

    return-void
.end method

.method public setMaxCount(I)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->maxCount:I

    return-void
.end method
