.class Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;
.super Landroid/os/AsyncTask;
.source "MoreTicketsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)V
    .locals 1

    .line 335
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 336
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 332
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 401
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 406
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    .line 408
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    .line 411
    iput v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    .line 412
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->publishProgress([Ljava/lang/Object;)V

    .line 420
    invoke-static {p1, v2}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$300(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mTickts:Ljava/lang/String;

    const/4 v4, 0x2

    .line 422
    iput v4, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    new-array v2, v3, [Ljava/lang/String;

    aput-object v0, v2, v5

    .line 423
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->publishProgress([Ljava/lang/Object;)V

    .line 424
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    :goto_0
    return-object v0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 332
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 4

    .line 383
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 384
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    if-nez p1, :cond_0

    return-void

    .line 389
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    .line 390
    iget v2, v1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    .line 391
    iput v2, v1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    goto :goto_0

    .line 394
    :cond_2
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$100(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->setData(Ljava/util/Vector;)V

    .line 395
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$200(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f090067

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0d0235

    .line 396
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 332
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 357
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 358
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    if-nez p1, :cond_0

    return-void

    .line 363
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$100(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->setData(Ljava/util/Vector;)V

    .line 364
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$200(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f090067

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0d0235

    .line 365
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 340
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 342
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    if-nez v0, :cond_0

    return-void

    .line 346
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    const/4 v3, 0x0

    .line 347
    iput v3, v2, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    goto :goto_0

    .line 349
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$100(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    move-result-object v1

    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->setData(Ljava/util/Vector;)V

    .line 351
    invoke-static {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$200(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0d028e

    .line 352
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 332
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .locals 1

    .line 371
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 373
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment$QueryTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    if-nez p1, :cond_0

    return-void

    .line 377
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$100(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->access$000(Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->setData(Ljava/util/Vector;)V

    return-void
.end method
