.class Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;
.super Ljava/lang/Object;
.source "BigScreenSelectStationFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->listenerSearchEdit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .line 141
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$100(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 142
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$200(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 143
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$300(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/ListView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 144
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$400(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 146
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$200(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 147
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$300(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/ListView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 148
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$400(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/view/widget/QuickLocationBar;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setVisibility(I)V

    .line 150
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$500(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->clear()V

    const/4 p1, 0x0

    .line 152
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$600(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$600(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v3}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$100(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$500(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v3}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$600(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 158
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$700(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {v2}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$500(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    :cond_3
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$1;->this$0:Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-static {p1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->access$800(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;)Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->notifyDataSetChanged()V

    :goto_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
