.class final Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;
.super Ljava/lang/Object;
.source "HoubuNotCompleteOrderFragment.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->doContinuePay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuNotCompleteOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuNotCompleteOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1\n*L\n1#1,265:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .line 224
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    const-string v1, "ModelFactory.get().houbuTicketModel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-static {v1}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->access$getQueryQueueDTO$p(Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getReserve_no()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 227
    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->continuePayNoCompleteMyOrder(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ContinuePayNoCompleteDTO;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 228
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/ContinuePayNoCompleteDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;->getFlag()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const/4 v3, 0x1

    .line 229
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 230
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/LoginModel;->get()Lcom/lltskb/lltskb/engine/online/LoginModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->conf()Z

    .line 231
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->payOrderInit()Z

    .line 232
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->paycheck()Lcom/lltskb/lltskb/engine/online/dto/PayCheckDTO;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 233
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 234
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getFlag()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 235
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getMerSignMsg()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_1

    :cond_1
    move-object v4, v2

    :goto_1
    const-string v1, "utf-8"

    if-eqz v4, :cond_2

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    const-string v5, "\r\n"

    const-string v6, ""

    .line 237
    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 238
    invoke-static {v4, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 241
    :cond_2
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getTranData()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    goto :goto_2

    :cond_3
    move-object v6, v2

    :goto_2
    if-eqz v6, :cond_4

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v7, "\r\n"

    const-string v8, ""

    .line 243
    invoke-static/range {v6 .. v11}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 244
    invoke-static {v5, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 247
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "appId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getAppId()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_5
    move-object v5, v2

    :goto_3
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&interfaceName="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getInterfaceName()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_6
    move-object v5, v2

    :goto_4
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&interfaceVersion="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getInterfaceVersion()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_7
    move-object v5, v2

    :goto_5
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&payOrderId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getPayOrderId()Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_8
    move-object v5, v2

    :goto_6
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&paymentType="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getPaymentType()Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    :cond_9
    move-object v5, v2

    :goto_7
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&merSignMsg="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&tranData="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&transType="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getTransType()Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    :cond_a
    move-object v4, v2

    :goto_8
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    const-class v6, Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckData;->getPayForm()Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayCheckPayForm;->getEpayurl()Ljava/lang/String;

    move-result-object v2

    :cond_b
    const-string v0, "web_url"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "web_params"

    .line 250
    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "web_post"

    .line 251
    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "web_closeonback"

    .line 252
    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 253
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment$doContinuePay$1;->this$0:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_c
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 258
    :cond_d
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    :cond_e
    return-void
.end method
