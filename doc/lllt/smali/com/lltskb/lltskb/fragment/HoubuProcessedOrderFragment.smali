.class public final Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "HoubuProcessedOrderFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuProcessedOrderFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuProcessedOrderFragment.kt\ncom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment\n*L\n1#1,140:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000c\u001a\u00020\rJ&\u0010\u000e\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\rH\u0002J\u0006\u0010\u0016\u001a\u00020\rJ\u0008\u0010\u0017\u001a\u00020\rH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;",
        "Lcom/lltskb/lltskb/fragment/BaseFragment;",
        "()V",
        "adapter",
        "Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;",
        "orderList",
        "",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "rootView",
        "Landroid/view/View;",
        "ticketView",
        "Landroid/support/v7/widget/RecyclerView;",
        "initView",
        "",
        "onCreateView",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "promptSignin",
        "refreshOrder",
        "updateView",
        "Companion",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$Companion;

.field public static final TAG:Ljava/lang/String; = "HoubuProcessedOrderFragment"

.field private static firstTimeRun:Z


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private adapter:Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

.field private orderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
            ">;"
        }
    .end annotation
.end field

.field private rootView:Landroid/view/View;

.field private ticketView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->Companion:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$Companion;

    const/4 v0, 0x1

    .line 29
    sput-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    .line 68
    new-instance v0, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->adapter:Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

    return-void
.end method

.method public static final synthetic access$getFirstTimeRun$cp()Z
    .locals 1

    .line 26
    sget-boolean v0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->firstTimeRun:Z

    return v0
.end method

.method public static final synthetic access$getOrderList$p(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->orderList:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$promptSignin(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->promptSignin()V

    return-void
.end method

.method public static final synthetic access$setFirstTimeRun$cp(Z)V
    .locals 0

    .line 26
    sput-boolean p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->firstTimeRun:Z

    return-void
.end method

.method public static final synthetic access$setOrderList$p(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;Ljava/util/List;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->orderList:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->updateView()V

    return-void
.end method

.method private final promptSignin()V
    .locals 4

    const-string v0, "HoubuProcessedOrderFragment"

    const-string v1, "promptSignin"

    .line 60
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$promptSignin$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$promptSignin$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    const v2, 0x7f0d02de

    const v3, 0x7f0d010e

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private final updateView()V
    .locals 9

    .line 79
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->adapter:Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->orderList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->setOrderList(Ljava/util/List;)V

    .line 80
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->adapter:Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;->notifyDataSetChanged()V

    .line 83
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->orderList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const v3, 0x7f0902a0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x8

    const/4 v4, 0x0

    if-eqz v1, :cond_2

    .line 85
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v2, :cond_1

    .line 86
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    .line 89
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v5, 0x7f0d0148

    invoke-virtual {v1, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "AppContext.get().getStri\u2026ring.fmt_order_not_found)"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_4

    .line 91
    sget-object v5, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v6, "Locale.CHINA"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v7

    const v8, 0x7f0d016a

    invoke-virtual {v7, v8}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    array-length v4, v6

    invoke-static {v6, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v5, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "java.lang.String.format(locale, format, *args)"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_4
    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public final initView()V
    .locals 4

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_0

    const v1, 0x7f0901bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->ticketView:Landroid/support/v7/widget/RecyclerView;

    .line 99
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->ticketView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->adapter:Lcom/lltskb/lltskb/fragment/ProcessedOrderTicketListItemAdapter;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->ticketView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->ticketView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_3

    new-instance v1, Landroid/support/v7/widget/DividerItemDecoration;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    check-cast v1, Landroid/support/v7/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 103
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0b004b

    const/4 v0, 0x0

    .line 73
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    .line 74
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->initView()V

    .line 75
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    return-object p1
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onDestroyView()V

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->_$_clearFindViewByIdCache()V

    return-void
.end method

.method public final refreshOrder()V
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v2, 0x7f0902a0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    .line 34
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0d022d

    const/high16 v3, -0x1000000

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 36
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment$refreshOrder$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method
