.class Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;
.super Landroid/os/AsyncTask;
.source "QueryBaoxianFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->onDisplayItem(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

.field final synthetic this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 163
    new-instance p1, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-direct {p1}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 166
    aget-object p1, p1, v0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->queryOrder(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 134
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4

    .line 150
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getErr()Ljava/lang/String;

    move-result-object v0

    .line 152
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u4fdd\u9669\u4fe1\u606f:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getInsuInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u4fdd\u9669\u6761\u6b3e:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getSafeguard()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u4fdd\u9669\u5355\u53f7:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getPolicyNo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u4fdd\u5355\u65f6\u95f4:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->mBaoxian:Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->getPolicyTime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0d0173

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 160
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 134
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 138
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;->this$0:Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3$1;-><init>(Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment$3;)V

    const v2, 0x7f0d01a6

    const/high16 v3, -0x1000000

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 146
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
