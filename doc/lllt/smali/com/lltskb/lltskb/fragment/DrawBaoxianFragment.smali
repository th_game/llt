.class public Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "DrawBaoxianFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$MobileTextWatcher;,
        Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$IdNoTextWatcher;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DrawBaoxianFragment"


# instance fields
.field private mListAdapter:Landroid/widget/BaseAdapter;

.field mRootView:Landroid/view/View;

.field private mUsers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->addSelf()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Landroid/widget/BaseAdapter;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)Ljava/util/Vector;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->doGetBX(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private addSelf()V
    .locals 4

    .line 212
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelf()Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 216
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 217
    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private checkOneUser(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 3

    .line 541
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const-string v2, "**"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 545
    :cond_0
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    .line 546
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v0, 0xb

    if-eq p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    .line 547
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    const-string v0, "\u624b\u673a\u53f7\u7801\u6709\u8bef!"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 542
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    const-string v0, "\u7528\u6237\u7684\u8eab\u4efd\u8bc1\u4fe1\u606f\u6709\u8bef!"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1
.end method

.method private checkUserInfo()Z
    .locals 3

    .line 528
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    .line 529
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 530
    iget-object v2, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v2, :cond_1

    goto :goto_1

    .line 532
    :cond_1
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->checkOneUser(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 537
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method private doGetBX(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 389
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 467
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const-string v4, "\n"

    if-lez v3, :cond_1

    .line 468
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " \u9886\u53d6\u6210\u529f:\n"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 470
    :cond_1
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " \u9886\u53d6\u5931\u8d25,\u53ef\u80fd\u7684\u539f\u56e0:\n"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 554
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 555
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\+86"

    .line 556
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, " "

    .line 557
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private initSpannableTextView()V
    .locals 5

    .line 173
    new-instance v0, Landroid/text/SpannableString;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0067

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v1, 0x11

    const/16 v2, 0xd

    .line 174
    invoke-direct {p0, v0, v2, v1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->setClickSpan(Landroid/text/SpannableString;II)V

    .line 176
    new-instance v3, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$2;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$2;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    const/16 v1, 0x18

    const/16 v2, 0x12

    .line 183
    invoke-direct {p0, v0, v2, v1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->setClickSpan(Landroid/text/SpannableString;II)V

    .line 184
    new-instance v3, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$3;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$3;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 192
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    const v2, 0x7f090260

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 194
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    const-string v0, "DrawBaoxianFragment"

    const-string v1, "initView"

    .line 226
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0900fb

    .line 227
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 228
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090223

    .line 230
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d0065

    .line 231
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f090181

    .line 233
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 234
    new-instance v1, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$4;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    .line 312
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f090074

    .line 314
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 315
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->initBaoxian(Landroid/content/Context;)Z

    return-void
.end method

.method private isRuleNotConfirmed()Z
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 202
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private onGetBaoxian(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "DrawBaoxianFragment"

    const-string v1, "onSubmit"

    .line 339
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->isRuleNotConfirmed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    const v0, 0x7f0d02de

    const v1, 0x7f0d0068

    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 346
    :cond_1
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->checkOneUser(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 349
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$5;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 384
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private onSubmit()V
    .locals 4

    const-string v0, "DrawBaoxianFragment"

    const-string v1, "onSubmit"

    .line 478
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->isRuleNotConfirmed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d02de

    const v2, 0x7f0d0068

    invoke-static {v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 484
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->checkUserInfo()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 487
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$6;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$6;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    .line 523
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setClickSpan(Landroid/text/SpannableString;II)V
    .locals 4

    .line 206
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    const/16 v1, 0x21

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 207
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f060023

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 323
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090054

    if-eq v0, v1, :cond_2

    const p1, 0x7f090074

    if-eq v0, p1, :cond_1

    const p1, 0x7f0900fb

    if-eq v0, p1, :cond_0

    goto :goto_0

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->dismiss()V

    goto :goto_0

    .line 328
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->onSubmit()V

    goto :goto_0

    .line 331
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->onGetBaoxian(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)V

    :goto_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 p3, 0x0

    const v0, 0x7f0b003e

    .line 132
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    .line 133
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->initView(Landroid/view/View;)V

    .line 135
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    sget-object p2, Lcom/lltskb/lltskb/fragment/-$$Lambda$DrawBaoxianFragment$iSCan79Lrbw_AchZj6pTye5saug;->INSTANCE:Lcom/lltskb/lltskb/fragment/-$$Lambda$DrawBaoxianFragment$iSCan79Lrbw_AchZj6pTye5saug;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 140
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->addSelf()V

    .line 158
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    if-eqz p1, :cond_2

    .line 159
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 141
    :cond_1
    :goto_0
    new-instance p1, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;

    new-instance p2, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$1;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment$1;-><init>(Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;)V

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;)V

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Boolean;

    .line 155
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v0, p2, p3

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 163
    :cond_2
    :goto_1
    :try_start_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->initSpannableTextView()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 165
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 168
    :goto_2
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mRootView:Landroid/view/View;

    return-object p1
.end method

.method public setPassengers(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;)V"
        }
    .end annotation

    .line 126
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->mUsers:Ljava/util/Vector;

    return-void
.end method
