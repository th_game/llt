.class public final Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;
.super Lcom/lltskb/lltskb/fragment/BaseFragment;
.source "HoubuTicketFragment.kt"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;,
        Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuTicketFragment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuTicketFragment.kt\ncom/lltskb/lltskb/fragment/HoubuTicketFragment\n*L\n1#1,220:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\u0018\u0000 /2\u00020\u00012\u00020\u0002:\u0002/0B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0006\u0010\u0015\u001a\u00020\u0016J\u0008\u0010\u0017\u001a\u00020\u0016H\u0002J&\u0010\u0018\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016JD\u0010\u001f\u001a\u00020\u00162\u0008\u0010 \u001a\u0004\u0018\u00010!2\u0008\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020%2\u0006\u0010\'\u001a\u00020%2\u0006\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020%H\u0016J\u0008\u0010*\u001a\u00020\u0016H\u0002J\u0008\u0010+\u001a\u00020\u0016H\u0002J\u0008\u0010,\u001a\u00020\u0016H\u0002J\u000e\u0010-\u001a\u00020\u00162\u0006\u0010.\u001a\u00020\u000bR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;",
        "Lcom/lltskb/lltskb/fragment/BaseFragment;",
        "Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;",
        "()V",
        "deadlineView",
        "Landroid/widget/TextView;",
        "houbuListItemAdapter",
        "Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;",
        "listView",
        "Landroid/support/v7/widget/RecyclerView;",
        "listener",
        "Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;",
        "orderConfig",
        "Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;",
        "peopleView",
        "rootView",
        "Landroid/view/View;",
        "checkConfirmResult",
        "",
        "dto",
        "Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;",
        "initView",
        "",
        "onConfirmHB",
        "onCreateView",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDateTimeSet",
        "datePickerView",
        "Landroid/widget/DatePicker;",
        "timePickerView",
        "Landroid/widget/TimePicker;",
        "year",
        "",
        "monthOfYear",
        "dayOfMonth",
        "hourOfDay",
        "minute",
        "onSelectPeople",
        "onUserSelected",
        "selectDateTime",
        "setListener",
        "l",
        "Companion",
        "Listener",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Companion;

.field public static final TAG:Ljava/lang/String; = "HoubuTicketFragment"


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private deadlineView:Landroid/widget/TextView;

.field private final houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

.field private listView:Landroid/support/v7/widget/RecyclerView;

.field private listener:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;

.field private orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

.field private peopleView:Landroid/widget/TextView;

.field private rootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->Companion:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;-><init>()V

    .line 53
    new-instance v0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-direct {v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    return-void
.end method

.method public static final synthetic access$checkConfirmResult(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;)Z
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->checkConfirmResult(Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getListener$p(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listener:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;

    return-object p0
.end method

.method public static final synthetic access$onConfirmHB(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->onConfirmHB()V

    return-void
.end method

.method public static final synthetic access$onSelectPeople(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->onSelectPeople()V

    return-void
.end method

.method public static final synthetic access$onUserSelected(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->onUserSelected()V

    return-void
.end method

.method public static final synthetic access$selectDateTime(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->selectDateTime()V

    return-void
.end method

.method public static final synthetic access$setListener$p(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listener:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;

    return-void
.end method

.method private final checkConfirmResult(Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 142
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;->getStatus()Z

    move-result v1

    const v2, 0x7f0d02de

    const/4 v3, 0x0

    if-nez v1, :cond_2

    .line 143
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;->getMessages()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object p1, v3

    :goto_0
    invoke-static {p1, v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;ILandroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v1, v2, p1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v0

    .line 146
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;->getFlag()Z

    move-result v1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_3

    goto :goto_1

    .line 151
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    const v0, 0x7f0d0173

    const v1, 0x7f0d01fb

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$checkConfirmResult$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-static {p1, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v4

    .line 147
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBData;->getMsg()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    move-object p1, v3

    :goto_2
    invoke-static {p1, v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v1, v2, p1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v0
.end method

.method private final onConfirmHB()V
    .locals 12

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->deadlineView:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x10

    const v3, 0x7f0d02de

    if-ge v0, v2, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d025d

    invoke-static {v0, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0d0184

    invoke-static {v0, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 103
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    :goto_2
    const/4 v2, 0x0

    const-string v4, ","

    .line 104
    invoke-static {v0, v4, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 105
    array-length v0, v0

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-le v0, v4, :cond_5

    .line 106
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v6, "Locale.CHINA"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d0140

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "AppContext.get().getStri\u2026x_passengers_limit_houbu)"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    array-length v2, v5

    invoke-static {v5, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v6, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "java.lang.String.format(locale, format, *args)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v2, v3, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 111
    :cond_5
    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iget-object v4, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->deadlineView:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_3

    :cond_6
    move-object v4, v1

    :goto_3
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 112
    iget-object v4, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move-object v6, v4

    check-cast v6, Ljava/lang/CharSequence;

    new-array v7, v5, [Ljava/lang/String;

    const-string v4, " "

    aput-object v4, v7, v2

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x6

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 113
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v5, :cond_7

    .line 114
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 115
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 117
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v6

    const-string v7, "ModelFactory.get()"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v6

    .line 118
    invoke-interface {v6, v2, v5}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->isAllowedDeadlineTime(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 119
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 120
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0d00f4

    invoke-static {v0, v3, v2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 125
    :cond_7
    iget-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-string v2, "#"

    invoke-static {v1, v4, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "StringUtils.replace(jzParam,\" \",\"#\")"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 126
    iget-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-string v3, ":"

    invoke-static {v1, v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "StringUtils.replace(jzParam,\":\",\"#\")"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 127
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$onConfirmHB$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$onConfirmHB$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final onSelectPeople()V
    .locals 7

    .line 166
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 167
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    :cond_1
    const v0, 0x7f010019

    const v2, 0x7f010018

    if-eqz v1, :cond_2

    .line 170
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 171
    :cond_2
    new-instance v3, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    invoke-direct {v3}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;-><init>()V

    const/4 v4, 0x3

    .line 172
    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->setMaxCount(I)V

    .line 174
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "purpose"

    const-string v6, "ADULT"

    .line 175
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->setArguments(Landroid/os/Bundle;)V

    .line 178
    new-instance v4, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$onSelectPeople$1;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$onSelectPeople$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V

    check-cast v4, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->setListener(Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;)V

    if-eqz v1, :cond_3

    .line 180
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    :cond_3
    if-eqz v1, :cond_4

    const v0, 0x7f0900e2

    .line 181
    check-cast v3, Landroid/support/v4/app/Fragment;

    .line 182
    const-class v2, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-virtual {v1, v0, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_4
    if-eqz v1, :cond_5

    .line 184
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_5
    return-void
.end method

.method private final onUserSelected()V
    .locals 6

    .line 188
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    const-string v1, "PassengerModel.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "users"

    .line 191
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 192
    invoke-virtual {v0, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 193
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    iget-boolean v5, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-eqz v5, :cond_0

    const-string v5, "[\u7ae5]"

    .line 195
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v5, "-"

    .line 196
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    .line 197
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderPerson(Ljava/lang/String;)V

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->peopleView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private final selectDateTime()V
    .locals 6

    .line 205
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    const-string v1, "ModelFactory.get().houbuTicketModel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    new-instance v1, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object v3, p0

    check-cast v3, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;Ljava/util/Date;Z)V

    .line 208
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getDeadlineTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setMaxDate(Ljava/util/Date;)V

    .line 209
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setMinDate(Ljava/util/Date;)V

    .line 210
    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->show()V

    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public final initView()V
    .locals 5

    .line 65
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    const-string v1, "ModelFactory.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    const-string v1, "ModelFactory.get().houbuTicketModel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const v3, 0x7f0901bf

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 67
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    check-cast v3, Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v3, Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 70
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v1, :cond_3

    const v3, 0x7f090276

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    goto :goto_1

    :cond_3
    move-object v1, v2

    :goto_1
    iput-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->deadlineView:Landroid/widget/TextView;

    .line 72
    iget-object v1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->setHoubuTicketList(Ljava/util/List;)V

    .line 74
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_4

    const v1, 0x7f090223

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_2

    :cond_4
    move-object v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 75
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v3, 0x7f0d00b4

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_6

    const v1, 0x7f09012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    :cond_6
    move-object v0, v2

    :goto_3
    if-eqz v0, :cond_7

    .line 78
    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$1;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_8

    const v1, 0x7f09014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_4

    :cond_8
    move-object v0, v2

    :goto_4
    if-eqz v0, :cond_9

    .line 81
    new-instance v1, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$2;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_9
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_a

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_5

    :cond_a
    move-object v0, v2

    :goto_5
    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->peopleView:Landroid/widget/TextView;

    .line 85
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->orderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    if-eqz v0, :cond_b

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/Button;

    :cond_b
    if-eqz v2, :cond_c

    .line 88
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$initView$3;-><init>(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_c
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0b004c

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    .line 56
    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->initView()V

    .line 57
    iget-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->rootView:Landroid/view/View;

    return-object p1
.end method

.method public onDateTimeSet(Landroid/widget/DatePicker;Landroid/widget/TimePicker;IIIII)V
    .locals 1

    .line 214
    sget-object p1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object p1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string p2, "Locale.CHINA"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x5

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 v0, 0x0

    aput-object p3, p2, v0

    const/4 p3, 0x1

    add-int/2addr p4, p3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p2, p3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 p4, 0x2

    aput-object p3, p2, p4

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 p4, 0x3

    aput-object p3, p2, p4

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 p4, 0x4

    aput-object p3, p2, p4

    array-length p3, p2

    invoke-static {p2, p3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string p3, "%04d-%02d-%02d %02d:%02d"

    invoke-static {p1, p3, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "java.lang.String.format(locale, format, *args)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "onDateTimeSet "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "HoubuTicketFragment"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object p2, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->deadlineView:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onDestroyView()V

    invoke-virtual {p0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->_$_clearFindViewByIdCache()V

    return-void
.end method

.method public final setListener(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;)V
    .locals 1

    const-string v0, "l"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iput-object p1, p0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->listener:Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;

    return-void
.end method
