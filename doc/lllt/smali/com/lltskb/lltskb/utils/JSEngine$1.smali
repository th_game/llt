.class Lcom/lltskb/lltskb/utils/JSEngine$1;
.super Landroid/webkit/WebViewClient;
.source "JSEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/utils/JSEngine;->loadUrlTask(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/utils/JSEngine;

.field final synthetic val$webview:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/utils/JSEngine;Landroid/webkit/WebView;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->this$0:Lcom/lltskb/lltskb/utils/JSEngine;

    iput-object p2, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->val$webview:Landroid/webkit/WebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .line 103
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 104
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onPageFinished url="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "JSEngine"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->this$0:Lcom/lltskb/lltskb/utils/JSEngine;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/JSEngine;->access$002(Lcom/lltskb/lltskb/utils/JSEngine;Z)Z

    .line 106
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->this$0:Lcom/lltskb/lltskb/utils/JSEngine;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/JSEngine;->access$100(Lcom/lltskb/lltskb/utils/JSEngine;)Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 107
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->this$0:Lcom/lltskb/lltskb/utils/JSEngine;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/JSEngine;->access$100(Lcom/lltskb/lltskb/utils/JSEngine;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->notify()V

    .line 108
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    const-string p1, "JSEngine"

    const-string p3, "onReceivedSslError"

    .line 91
    invoke-static {p1, p3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    const-string p1, "JSEngine"

    const-string v0, "shouldOverrideUrlLoading"

    .line 97
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine$1;->val$webview:Landroid/webkit/WebView;

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
