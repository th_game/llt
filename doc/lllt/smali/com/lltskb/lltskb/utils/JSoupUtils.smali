.class public Lcom/lltskb/lltskb/utils/JSoupUtils;
.super Ljava/lang/Object;
.source "JSoupUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPageDocument(Ljava/lang/String;)Lorg/jsoup/nodes/Document;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 42
    :cond_0
    :try_start_0
    invoke-static {p0}, Lorg/jsoup/Jsoup;->parse(Ljava/lang/String;)Lorg/jsoup/nodes/Document;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    const-string v1, "JsoupUtil.getPageDocument"

    .line 44
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-object v0

    .line 45
    :goto_1
    throw p0
.end method

.method public static getPayInitParam(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/PayInfo;
    .locals 8

    const-string v0, "merCustomIp"

    const-string v1, "name"

    const/4 v2, 0x0

    .line 57
    :try_start_0
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getPageDocument(Ljava/lang/String;)Lorg/jsoup/nodes/Document;

    move-result-object p0

    if-eqz p0, :cond_d

    const-string v3, "myForm"

    .line 59
    invoke-virtual {p0, v1, v3}, Lorg/jsoup/nodes/Document;->getElementsByAttributeValue(Ljava/lang/String;Ljava/lang/String;)Lorg/jsoup/select/Elements;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v2

    .line 61
    :cond_0
    invoke-virtual {p0}, Lorg/jsoup/select/Elements;->first()Lorg/jsoup/nodes/Element;

    move-result-object p0

    if-eqz p0, :cond_d

    .line 63
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;-><init>()V

    const-string v4, "1"

    .line 64
    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setBusinessType(Ljava/lang/String;)V

    const-string v4, "action"

    .line 65
    invoke-virtual {p0, v4}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPayUrl(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lorg/jsoup/nodes/Element;->childNodes()Ljava/util/List;

    move-result-object p0

    .line 67
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/jsoup/nodes/Node;

    const-string v5, "interfaceName"

    .line 68
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, "value"

    if-eqz v5, :cond_2

    .line 69
    :try_start_1
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceName(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v5, "interfaceVersion"

    .line 70
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 71
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceVersion(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v5, "tranData"

    .line 72
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 73
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTranData(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v5, "merSignMsg"

    .line 74
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 75
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerSignMsg(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v5, "appId"

    .line 76
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 77
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setAppId(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v5, "transType"

    .line 78
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 79
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTransType(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v5, "channelId"

    .line 80
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 81
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setChannelId(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    :cond_8
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 83
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerCustomIp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v5, "orderTimeoutDate"

    .line 84
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 85
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setOrderTimeoutDate(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 86
    :cond_a
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 87
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerCustomIp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v5, "paymentType"

    .line 88
    invoke-virtual {v4, v1}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 89
    invoke-virtual {v4, v6}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPaymentType(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_c
    return-object v3

    :catch_0
    move-exception p0

    const-string v0, "JsoupUtil"

    const-string v1, "getPayInitParam"

    .line 96
    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_d
    return-object v2
.end method

.method public static getWapPayInitParam(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/PayInfo;
    .locals 7

    const-string v0, "name"

    const/4 v1, 0x0

    .line 161
    :try_start_0
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getPageDocument(Ljava/lang/String;)Lorg/jsoup/nodes/Document;

    move-result-object p0

    if-eqz p0, :cond_c

    const-string v2, "form"

    .line 163
    invoke-virtual {p0, v2}, Lorg/jsoup/nodes/Document;->getElementsByTag(Ljava/lang/String;)Lorg/jsoup/select/Elements;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v1

    .line 165
    :cond_0
    invoke-virtual {p0}, Lorg/jsoup/select/Elements;->first()Lorg/jsoup/nodes/Element;

    move-result-object p0

    if-eqz p0, :cond_c

    .line 167
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;-><init>()V

    const-string v3, "1"

    .line 168
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setBusinessType(Ljava/lang/String;)V

    const-string v3, "action"

    .line 169
    invoke-virtual {p0, v3}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPayUrl(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lorg/jsoup/nodes/Element;->childNodes()Ljava/util/List;

    move-result-object p0

    .line 171
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/jsoup/nodes/Node;

    const-string v4, "interfaceName"

    .line 172
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "value"

    if-eqz v4, :cond_2

    .line 173
    :try_start_1
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceName(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v4, "interfaceVersion"

    .line 174
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 175
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceVersion(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v4, "tranData"

    .line 176
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 177
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTranData(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v4, "merSignMsg"

    .line 178
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 179
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerSignMsg(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v4, "appId"

    .line 180
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 181
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setAppId(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v4, "transType"

    .line 182
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 183
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTransType(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v4, "channelId"

    .line 184
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 185
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setChannelId(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v4, "merCustomIp"

    .line 186
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 187
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerCustomIp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v4, "orderTimeoutDate"

    .line 188
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 189
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setOrderTimeoutDate(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v4, "paymentType"

    .line 190
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    invoke-virtual {v3, v5}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPaymentType(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_b
    return-object v2

    :catch_0
    move-exception p0

    const-string v0, "JsoupUtil"

    const-string v2, "getPayInitParam"

    .line 198
    invoke-static {v0, v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_c
    return-object v1
.end method

.method public static parseAlipayHtml(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 6

    const-string v0, "value"

    const/4 v1, 0x0

    .line 103
    :try_start_0
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getPageDocument(Ljava/lang/String;)Lorg/jsoup/nodes/Document;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 105
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;-><init>()V

    const-string v3, "J_qrPayLoopCheckUrl"

    .line 107
    invoke-virtual {p0, v3}, Lorg/jsoup/nodes/Document;->getElementById(Ljava/lang/String;)Lorg/jsoup/nodes/Element;

    move-result-object v3

    if-nez v3, :cond_0

    return-object v1

    .line 109
    :cond_0
    invoke-virtual {v3, v0}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    const-string v3, "J_qrContextId"

    .line 111
    invoke-virtual {p0, v3}, Lorg/jsoup/nodes/Document;->getElementById(Ljava/lang/String;)Lorg/jsoup/nodes/Element;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v1

    .line 114
    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "qrTokenId=&"

    .line 115
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "name"

    .line 116
    invoke-virtual {p0, v4}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0, v0}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "&_="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception p0

    const-string v0, "JsoupUtil"

    const-string v2, "parseAlipayHtml"

    .line 122
    invoke-static {v0, v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-object v1
.end method

.method public static parseHtmlForm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 5

    const/4 v0, 0x0

    .line 129
    :try_start_0
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getPageDocument(Ljava/lang/String;)Lorg/jsoup/nodes/Document;

    move-result-object p0

    if-eqz p0, :cond_4

    const-string v1, "form"

    .line 131
    invoke-virtual {p0, v1}, Lorg/jsoup/nodes/Document;->getElementsByTag(Ljava/lang/String;)Lorg/jsoup/select/Elements;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    .line 133
    :cond_0
    invoke-virtual {p0}, Lorg/jsoup/select/Elements;->first()Lorg/jsoup/nodes/Element;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 135
    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;-><init>()V

    const-string v2, "action"

    .line 136
    invoke-virtual {p0, v2}, Lorg/jsoup/nodes/Element;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    .line 137
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 138
    invoke-virtual {p0}, Lorg/jsoup/nodes/Element;->childNodes()Ljava/util/List;

    move-result-object p0

    .line 139
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/jsoup/nodes/Node;

    .line 140
    instance-of v4, v3, Lorg/jsoup/nodes/Element;

    if-eqz v4, :cond_1

    const-string v4, "name"

    .line 142
    invoke-virtual {v3, v4}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "value"

    invoke-virtual {v3, v4}, Lorg/jsoup/nodes/Node;->attr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "utf-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    if-lez p0, :cond_3

    .line 147
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 149
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p0

    const-string v1, "JsoupUtil"

    const-string v2, "parseWebBussiness"

    .line 154
    invoke-static {v1, v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    return-object v0
.end method
