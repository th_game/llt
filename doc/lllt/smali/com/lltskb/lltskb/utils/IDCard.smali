.class public Lcom/lltskb/lltskb/utils/IDCard;
.super Ljava/lang/Object;
.source "IDCard.java"


# static fields
.field private static _areaCode:[Ljava/lang/String;

.field private static areaCodeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static dateMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _codeError:Ljava/lang/String;

.field private ai:[I

.field final vi:[I

.field final wi:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x23

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "11"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v3, "12"

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v4, 0x2

    const-string v5, "13"

    aput-object v5, v0, v4

    const/4 v4, 0x3

    const-string v5, "14"

    aput-object v5, v0, v4

    const/4 v4, 0x4

    const-string v5, "15"

    aput-object v5, v0, v4

    const/4 v4, 0x5

    const-string v5, "21"

    aput-object v5, v0, v4

    const/4 v4, 0x6

    const-string v5, "22"

    aput-object v5, v0, v4

    const/4 v4, 0x7

    const-string v5, "23"

    aput-object v5, v0, v4

    const/16 v4, 0x8

    const-string v5, "31"

    aput-object v5, v0, v4

    const/16 v4, 0x9

    const-string v5, "32"

    aput-object v5, v0, v4

    const/16 v4, 0xa

    const-string v5, "33"

    aput-object v5, v0, v4

    const/16 v4, 0xb

    const-string v5, "34"

    aput-object v5, v0, v4

    const/16 v4, 0xc

    const-string v5, "35"

    aput-object v5, v0, v4

    const/16 v4, 0xd

    const-string v5, "36"

    aput-object v5, v0, v4

    const/16 v4, 0xe

    const-string v5, "37"

    aput-object v5, v0, v4

    const/16 v4, 0xf

    const-string v5, "41"

    aput-object v5, v0, v4

    const/16 v4, 0x10

    const-string v5, "42"

    aput-object v5, v0, v4

    const/16 v4, 0x11

    const-string v5, "43"

    aput-object v5, v0, v4

    const/16 v4, 0x12

    const-string v5, "44"

    aput-object v5, v0, v4

    const/16 v4, 0x13

    const-string v5, "45"

    aput-object v5, v0, v4

    const/16 v4, 0x14

    const-string v5, "46"

    aput-object v5, v0, v4

    const/16 v4, 0x15

    const-string v5, "50"

    aput-object v5, v0, v4

    const/16 v4, 0x16

    const-string v5, "51"

    aput-object v5, v0, v4

    const/16 v4, 0x17

    const-string v5, "52"

    aput-object v5, v0, v4

    const/16 v4, 0x18

    const-string v5, "53"

    aput-object v5, v0, v4

    const/16 v4, 0x19

    const-string v5, "54"

    aput-object v5, v0, v4

    const/16 v4, 0x1a

    const-string v5, "61"

    aput-object v5, v0, v4

    const/16 v4, 0x1b

    const-string v5, "62"

    aput-object v5, v0, v4

    const/16 v4, 0x1c

    const-string v5, "63"

    aput-object v5, v0, v4

    const/16 v4, 0x1d

    const-string v5, "64"

    aput-object v5, v0, v4

    const/16 v4, 0x1e

    .line 19
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "65"

    aput-object v6, v0, v4

    const/16 v4, 0x1f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "71"

    aput-object v7, v0, v4

    const/16 v4, 0x20

    const-string v7, "81"

    aput-object v7, v0, v4

    const/16 v4, 0x21

    const-string v7, "82"

    aput-object v7, v0, v4

    const/16 v4, 0x22

    const-string v7, "91"

    aput-object v7, v0, v4

    sput-object v0, Lcom/lltskb/lltskb/utils/IDCard;->_areaCode:[Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    .line 26
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v4, "01"

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const/4 v4, 0x0

    const-string v7, "02"

    invoke-virtual {v0, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "03"

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "04"

    invoke-virtual {v0, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "05"

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "06"

    invoke-virtual {v0, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "07"

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "08"

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "09"

    invoke-virtual {v0, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    const-string v7, "10"

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    invoke-virtual {v0, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/IDCard;->areaCodeMap:Ljava/util/HashMap;

    .line 39
    sget-object v0, Lcom/lltskb/lltskb/utils/IDCard;->_areaCode:[Ljava/lang/String;

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 40
    sget-object v5, Lcom/lltskb/lltskb/utils/IDCard;->areaCodeMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12

    new-array v1, v0, [I

    .line 15
    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/lltskb/lltskb/utils/IDCard;->wi:[I

    const/16 v1, 0xb

    new-array v1, v1, [I

    .line 17
    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/lltskb/lltskb/utils/IDCard;->vi:[I

    new-array v0, v0, [I

    .line 18
    iput-object v0, p0, Lcom/lltskb/lltskb/utils/IDCard;->ai:[I

    return-void

    :array_0
    .array-data 4
        0x7
        0x9
        0xa
        0x5
        0x8
        0x4
        0x2
        0x1
        0x6
        0x3
        0x7
        0x9
        0xa
        0x5
        0x8
        0x4
        0x2
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x0
        0x58
        0x9
        0x8
        0x7
        0x6
        0x5
        0x4
        0x3
        0x2
    .end array-data
.end method


# virtual methods
.method public containsAllNumber(Ljava/lang/String;)Z
    .locals 5

    .line 112
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 113
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    const/16 v0, 0x11

    .line 115
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    .line 117
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    const/4 v0, 0x0

    .line 118
    :goto_1
    array-length v1, p1

    const/4 v3, 0x1

    if-ge v0, v1, :cond_4

    .line 119
    aget-char v1, p1, v0

    const/16 v4, 0x30

    if-lt v1, v4, :cond_3

    aget-char v1, p1, v0

    const/16 v4, 0x39

    if-le v1, v4, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 120
    :cond_3
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u9519\u8bef\uff1a\u8f93\u5165\u7684\u8eab\u4efd\u8bc1\u53f7\u7b2c"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/2addr v0, v3

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\u4f4d\u5305\u542b\u5b57\u6bcd"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v2

    :cond_4
    return v3
.end method

.method public getCodeError()Ljava/lang/String;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return-object v0
.end method

.method public getVerify(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 188
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x11

    const/4 v2, 0x0

    const/16 v3, 0x12

    if-ne v0, v3, :cond_0

    .line 189
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 192
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    add-int/lit8 v3, v0, 0x1

    .line 195
    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 196
    iget-object v5, p0, Lcom/lltskb/lltskb/utils/IDCard;->ai:[I

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v5, v0

    move v0, v3

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    .line 200
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/IDCard;->wi:[I

    aget v0, v0, v2

    iget-object v3, p0, Lcom/lltskb/lltskb/utils/IDCard;->ai:[I

    aget v3, v3, v2

    mul-int v0, v0, v3

    add-int/2addr p1, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 202
    :cond_2
    rem-int/lit8 v2, p1, 0xb

    :cond_3
    const/4 p1, 0x2

    if-ne v2, p1, :cond_4

    const-string p1, "X"

    goto :goto_2

    .line 205
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->vi:[I

    aget p1, p1, v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public uptoeighteen(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x6

    const/4 v1, 0x0

    .line 210
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 211
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "19"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0xf

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->getVerify(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public verify(Ljava/lang/String;)Z
    .locals 3

    const-string v0, ""

    .line 133
    iput-object v0, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    .line 135
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->verifyLength(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 139
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->containsAllNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 145
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0xf

    if-ne v0, v2, :cond_2

    .line 146
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->uptoeighteen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 151
    :cond_2
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->verifyAreaCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 155
    :cond_3
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->verifyBirthdayCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    return v1

    .line 159
    :cond_4
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->verifyMOD(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_5

    return v1

    :cond_5
    const/4 p1, 0x1

    return p1
.end method

.method public verifyAreaCode(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 57
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 59
    sget-object v1, Lcom/lltskb/lltskb/utils/IDCard;->areaCodeMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 62
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u9519\u8bef\uff1a\u8f93\u5165\u7684\u8eab\u4efd\u8bc1\u53f7\u7684\u5730\u533a\u7801(1-2\u4f4d)["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]\u4e0d\u7b26\u5408\u4e2d\u56fd\u884c\u653f\u533a\u5212\u5206\u4ee3\u7801\u89c4\u5b9a(GB/T2260-1999)"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v0
.end method

.method public verifyBirthdayCode(Ljava/lang/String;)Z
    .locals 11

    const/16 v0, 0xc

    const/16 v1, 0xa

    .line 70
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x12

    if-ne v6, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 72
    :goto_0
    sget-object v6, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    const-string v7, "\u9519\u8bef\uff1a\u8f93\u5165\u7684\u8eab\u4efd\u8bc1\u53f7"

    if-nez v6, :cond_2

    .line 73
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_1

    const-string v0, "(11-12\u4f4d)"

    goto :goto_1

    :cond_1
    const-string v0, "(9-10\u4f4d)"

    :goto_1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u4e0d\u5b58\u5728["

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]\u6708\u4efd,\u4e0d\u7b26\u5408\u8981\u6c42(GB/T7408)"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v5

    :cond_2
    const/16 v6, 0xe

    .line 77
    invoke-virtual {p1, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 78
    sget-object v6, Lcom/lltskb/lltskb/utils/IDCard;->dateMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    const/4 v6, 0x6

    .line 79
    invoke-virtual {p1, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 80
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "["

    const-string v6, "(13-14\u4f4d)"

    const-string v8, "(11-13\u4f4d)"

    if-eqz v2, :cond_5

    .line 84
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt p1, v2, :cond_3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ge p1, v4, :cond_b

    .line 85
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_4

    goto :goto_2

    :cond_4
    move-object v6, v8

    :goto_2
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]\u53f7\u4e0d\u7b26\u5408\u5c0f\u67081-30\u5929\u5927\u67081-31\u5929\u7684\u89c4\u5b9a(GB/T7408)"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v5

    .line 92
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit8 v2, v2, 0x4

    const-string v9, "]\u53f7\u5728"

    if-nez v2, :cond_6

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    if-nez v2, :cond_7

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit16 v2, v2, 0x190

    if-nez v2, :cond_a

    .line 93
    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v10, 0x1d

    if-gt v2, v10, :cond_8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_b

    .line 94
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_9

    goto :goto_3

    :cond_9
    move-object v6, v8

    :goto_3
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\u95f0\u5e74\u7684\u60c5\u51b5\u4e0b\u672a\u7b26\u54081-29\u53f7\u7684\u89c4\u5b9a(GB/T7408)"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v5

    .line 100
    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v10, 0x1c

    if-gt v2, v10, :cond_c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_b

    goto :goto_4

    :cond_b
    return v4

    .line 101
    :cond_c
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_d

    goto :goto_5

    :cond_d
    move-object v6, v8

    :goto_5
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\u5e73\u5e74\u7684\u60c5\u51b5\u4e0b\u672a\u7b26\u54081-28\u53f7\u7684\u89c4\u5b9a(GB/T7408)"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    return v5
.end method

.method public verifyLength(Ljava/lang/String;)Z
    .locals 1

    .line 46
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v0, 0xf

    if-eq p1, v0, :cond_1

    const/16 v0, 0x12

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "\u9519\u8bef\uff1a\u8f93\u5165\u7684\u8eab\u4efd\u8bc1\u53f7\u4e0d\u662f15\u4f4d\u548c18\u4f4d\u7684"

    .line 50
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public verifyMOD(Ljava/lang/String;)Z
    .locals 4

    const/16 v0, 0x11

    const/16 v1, 0x12

    .line 167
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "x"

    .line 168
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "X"

    if-eqz v2, :cond_0

    .line 169
    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    move-object v0, v3

    .line 172
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/utils/IDCard;->getVerify(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 173
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const-string p1, "\u9519\u8bef\uff1a\u8f93\u5165\u7684\u8eab\u4efd\u8bc1\u53f7\u6700\u672b\u5c3e\u7684\u6570\u5b57\u9a8c\u8bc1\u7801\u9519\u8bef"

    .line 180
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/IDCard;->_codeError:Ljava/lang/String;

    const/4 p1, 0x0

    return p1
.end method
