.class public Lcom/lltskb/lltskb/utils/DayStyle;
.super Ljava/lang/Object;
.source "DayStyle.java"


# static fields
.field private static final vecStrWeekDayNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    invoke-static {}, Lcom/lltskb/lltskb/utils/DayStyle;->getWeekDayNames()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/utils/DayStyle;->vecStrWeekDayNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getWeekDay(II)I
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    add-int/lit8 v0, p0, 0x2

    const/4 v2, 0x7

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :cond_1
    :goto_0
    if-ne p1, v1, :cond_2

    add-int/lit8 v0, p0, 0x1

    :cond_2
    return v0
.end method

.method public static getWeekDayName(I)Ljava/lang/String;
    .locals 1

    .line 37
    sget-object v0, Lcom/lltskb/lltskb/utils/DayStyle;->vecStrWeekDayNames:[Ljava/lang/String;

    aget-object p0, v0, p0

    return-object p0
.end method

.method private static getWeekDayNames()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    const-string v2, "\u5468\u65e5"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "\u5468\u4e00"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "\u5468\u4e8c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\u5468\u4e09"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\u5468\u56db"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\u5468\u4e94"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\u5468\u516d"

    aput-object v2, v0, v1

    return-object v0
.end method
