.class public Lcom/lltskb/lltskb/utils/MetricsEventId;
.super Ljava/lang/Object;
.source "MetricsEventId.java"


# static fields
.field public static final EventIdBaiduSSP:Ljava/lang/String; = "baidussp"

.field public static final EventIdBigScreen:Ljava/lang/String; = "bigscreen"

.field public static final EventIdBookTicket:Ljava/lang/String; = "bookticket"

.field public static final EventIdCCQuery:Ljava/lang/String; = "ccquery"

.field public static final EventIdCZQuery:Ljava/lang/String; = "czquery"

.field public static final EventIdDDQuery:Ljava/lang/String; = "ddquery"

.field public static final EventIdGuangdiantong:Ljava/lang/String; = "guangdiantong"

.field public static final EventIdHeiniuBX:Ljava/lang/String; = "heiniubx"

.field public static final EventIdHongbao:Ljava/lang/String; = "hongbao"

.field public static final EventIdInmobi:Ljava/lang/String; = "inmobi"

.field public static final EventIdMeituan:Ljava/lang/String; = "meituanbx"

.field public static final EventIdMtlb:Ljava/lang/String; = "mtlb"

.field public static final EventIdPufa:Ljava/lang/String; = "pufa"

.field public static final EventIdQiche:Ljava/lang/String; = "qiche"

.field public static final EventIdQunar:Ljava/lang/String; = "qunar"

.field public static final EventIdSettings:Ljava/lang/String; = "settings"

.field public static final EventIdTongcheng:Ljava/lang/String; = "tongcheng"

.field public static final EventIdWangyiBX:Ljava/lang/String; = "wangyibx"

.field public static final EventIdXinchengBX:Ljava/lang/String; = "xinchengbx"

.field public static final EventIdXindan:Ljava/lang/String; = "xindan"

.field public static final EventIdXingyuan:Ljava/lang/String; = "xingyuan"

.field public static final EventIdZZQuery:Ljava/lang/String; = "zzquery"

.field public static final EventIdbaiduNews:Ljava/lang/String; = "baidunews"

.field public static final LabelAccessHome:Ljava/lang/String; = "\u8bbf\u95ee\u4e3b\u9875"

.field public static final LabelAccount:Ljava/lang/String; = "\u5f53\u524d\u8d26\u6237"

.field public static final LabelAdult:Ljava/lang/String; = "\u6210\u4eba\u7968"

.field public static final LabelBaike:Ljava/lang/String; = "\u7ad9\u540d\u767e\u79d1"

.field public static final LabelBigScreen:Ljava/lang/String; = "\u8f66\u7ad9\u5927\u5c4f"

.field public static final LabelBookTicket:Ljava/lang/String; = "\u5728\u7ebf\u8ba2\u7968"

.field public static final LabelClassic:Ljava/lang/String; = "\u7ecf\u5178\u754c\u9762"

.field public static final LabelClicked:Ljava/lang/String; = "\u70b9\u51fb"

.field public static final LabelCompleteOrder:Ljava/lang/String; = "\u5df2\u5b8c\u6210\u8ba2\u5355"

.field public static final LabelContacts:Ljava/lang/String; = "\u5e38\u7528\u8054\u7cfb\u4eba"

.field public static final LabelFanCheng:Ljava/lang/String; = "\u8fd4\u7a0b"

.field public static final LabelFeedback:Ljava/lang/String; = "\u610f\u89c1\u53cd\u9988"

.field public static final LabelFontSize:Ljava/lang/String; = "\u5b57\u4f53\u5927\u5c0f"

.field public static final LabelFuzzy:Ljava/lang/String; = "\u6a21\u7cca\u7ad9\u540d"

.field public static final LabelHelp:Ljava/lang/String; = "\u5e2e\u52a9\u8bf4\u660e"

.field public static final LabelHide:Ljava/lang/String; = "\u9690\u85cf\u8f66\u6b21"

.field public static final LabelHistroy:Ljava/lang/String; = "\u5386\u53f2\u67e5\u8be2"

.field public static final LabelHotel:Ljava/lang/String; = "\u9152\u5e97"

.field public static final LabelHouse:Ljava/lang/String; = "\u623f\u5b50"

.field public static final LabelJPK:Ljava/lang/String; = "\u68c0\u7968\u53e3"

.field public static final LabelJiazheng:Ljava/lang/String; = "\u5bb6\u653f"

.field public static final LabelJob:Ljava/lang/String; = "\u5de5\u4f5c"

.field public static final LabelLeftTicket:Ljava/lang/String; = "\u4f59\u7968\u67e5\u8be2"

.field public static final LabelLift:Ljava/lang/String; = "\u751f\u6d3b\u670d\u52a1"

.field public static final LabelLineUp:Ljava/lang/String; = "\u5019\u8865\u8ba2\u5355"

.field public static final LabelMidQuery:Ljava/lang/String; = "\u4e2d\u8f6c\u67e5\u8be2"

.field public static final LabelMobileCheck:Ljava/lang/String; = "\u624b\u673a\u6838\u9a8c"

.field public static final LabelMonitor:Ljava/lang/String; = "\u76d1\u63a7\u7ba1\u7406"

.field public static final LabelNewUI:Ljava/lang/String; = "\u65b0\u754c\u9762"

.field public static final LabelNoCompleteOrder:Ljava/lang/String; = "\u672a\u5b8c\u6210\u8ba2\u5355"

.field public static final LabelOffline:Ljava/lang/String; = "\u79bb\u7ebf\u67e5\u8be2"

.field public static final LabelOnline:Ljava/lang/String; = "\u8054\u7f51\u67e5\u8be2"

.field public static final LabelOnlineQueryFailed:Ljava/lang/String; = "\u8054\u7f51\u67e5\u8be2\u5931\u8d25"

.field public static final LabelQSS:Ljava/lang/String; = "\u8d77\u552e\u65f6\u95f4"

.field public static final LabelQiChe:Ljava/lang/String; = "\u6c7d\u8f66"

.field public static final LabelReceived:Ljava/lang/String; = "\u63a5\u6536"

.field public static final LabelRefresh:Ljava/lang/String; = "\u5237\u65b0"

.field public static final LabelRequested:Ljava/lang/String; = "\u8bf7\u6c42"

.field public static final LabelRunChart:Ljava/lang/String; = "\u8fd0\u884c\u56fe"

.field public static final LabelSelectStation:Ljava/lang/String; = "\u9009\u62e9\u8f66\u7ad9"

.field public static final LabelShare:Ljava/lang/String; = "\u5206\u4eab"

.field public static final LabelSkipped:Ljava/lang/String; = "\u8df3\u8fc7"

.field public static final LabelStudent:Ljava/lang/String; = "\u5b66\u751f\u7968"

.field public static final LabelSuccess:Ljava/lang/String; = "\u6210\u529f"

.field public static final LabelSwitch:Ljava/lang/String; = "\u5207\u6362"

.field public static final LabelTYZK:Ljava/lang/String; = "\u505c\u8fd0\u589e\u5f00"

.field public static final LabelTakeDate:Ljava/lang/String; = "\u4e58\u5750\u65e5\u671f"

.field public static final LabelTicket:Ljava/lang/String; = "\u673a\u7968"

.field public static final LabelTicketQueryFailed:Ljava/lang/String; = "\u4f59\u7968\u67e5\u8be2\u5931\u8d25"

.field public static final LabelTodayHistroy:Ljava/lang/String; = "\u5386\u53f2\u4e0a\u7684\u4eca\u5929"

.field public static final LabelTrainType:Ljava/lang/String; = "\u8f66\u578b"

.field public static final LabelUpdate:Ljava/lang/String; = "\u68c0\u67e5\u66f4\u65b0"

.field public static final LabelZWD:Ljava/lang/String; = "\u6b63\u665a\u70b9"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
