.class public Lcom/lltskb/lltskb/utils/LLTConsts;
.super Ljava/lang/Object;
.source "LLTConsts.java"


# static fields
.field public static final ALI_BUS:Ljava/lang/String; = "https://h5.m.taobao.com/trip/car/search/index.html?ttid=12oap0000083"

.field public static final AUTO_SHOW_BAOXIAN:Ljava/lang/String; = "auto_show_baoxian"

.field public static final BAIDU_BAIKE_FORMAT:Ljava/lang/String; = "https://wapbaike.baidu.com/item/%s"

.field public static final BAIDU_BAIKE_STATION:Ljava/lang/String; = "<u><a href=\"http://wapbaike.baidu.com/search?word=%s&submit=\u8fdb\u5165\u8bcd\u6761&uid=&ssid=&st=1&bd_page_type=1&bk_fr=srch\">%s</a></u>"

.field public static final BAIDU_NEWS:Ljava/lang/String; = "http://cpu.baidu.com/1032/1003cd5d"

.field public static final BAOXIAN:Ljava/lang/String; = "http://www.dainar.com/hkout/page/pingan/wap/ywx3/?a=169"

.field public static final BIND_TEL:Ljava/lang/String; = "https://kyfw.12306.cn/otn/userSecurity/bindTel"

.field public static final BOOK_LIFE_FORMAT:Ljava/lang/String; = "http://jump.luna.58.com/jump/sclk?url=UWd1sHcYnHEdnW9QPj9OPjDdsyd1symVUZR_IgwfUh0hm1d1pAR8uv6dUzun0jdkyMIunAGjuMPvyH6Eijb3UguQRMPvwbG1IbIhUNF5ngKpIduRpMwyUb6r0RKGIywFEyw3nZuQujKCNaYdXR7ZRy6xpjKnidmduvOyU-I7NRKJHMGzpbwu0hGByyOcH-uRuvOyINFbgY-ONywFEgD3IgG5ngKKNW6dIWubRgGdrZRONhFdHD_QPN6D&lat=%f&lon=%f"

.field public static final BOOK_TICKET:Ljava/lang/String; = "http://app.fishlee.net/12306/login.html"

.field public static BOOK_TICKET_DAYS:I = 0x3c

.field public static final BUY_LOTTERY:Ljava/lang/String; = "http://4g.9188.com/?comeFrom=lltskb"

.field public static final CAIPIAO:Ljava/lang/String; = "http://wap.lltskb.com/9188/caipiao.html"

.field public static final CTRIP_JIESONGZHAN:Ljava/lang/String; = "http://m.ctrip.com/webapp/carch/chf/train?s=car&Allianceid=109045&sid=552847&popup=close&autoawaken=close"

.field public static final CTRIP_MASHANGJIAOCHE:Ljava/lang/String; = "http://m.ctrip.com/webapp/carch/rtn/index?s=car&isbk=0&Allianceid=109045&sid=552847&popup=close&autoawaken=close"

.field public static final DITIE_MAP:Ljava/lang/String; = "http://wap.lltskb.com/ditie"

.field public static final ERSHOU_URL:Ljava/lang/String; = "http://luna.58.com/show/ads?n=s-24145281489415-ms-f-802&c=lltskb_wap&ct=undefined&cn=1&rf1=&rf2=http%3A%2F%2Fwap.lltskb.com&h1=38&if=&fc=undefined&bc=undefined&w1=300"

.field public static final FAQ:Ljava/lang/String; = "http://wap.lltskb.com/wtfk/"

.field public static final FIND_HOMEMAKING_FORMAT:Ljava/lang/String; = "http://i.58.com/HIq?lat=%f&lon=%f"

.field public static final FIND_HOUSE_FORMAT:Ljava/lang/String; = "http://i.58.com/HIk?lat=%f&lon=%f"

.field public static final FIND_JOB_FORMAT:Ljava/lang/String; = "http://i.58.com/HIh?lat=%f&lon=%f"

.field public static final FLAG_ORDER_FROM_MONITOR:I = 0x4

.field public static final FLAG_ORDER_FROM_MYORDR:I = 0x3

.field public static final FLAG_ORDER_FROM_NORMAL:I = 0x0

.field public static final FLAG_ORDER_FROM_TRAIN_DETAIL:I = 0x2

.field public static final FLAG_ORDER_FROM_ZZQUERY:I = 0x1

.field public static final FORGET_PASS:Ljava/lang/String; = "https://kyfw.12306.cn/otn/forgetPassword/initforgetMyPassword"

.field public static final FRAGMENT_MYBAOXIAN_QUERY:Ljava/lang/String; = "fragment_mybaoxian_query"

.field public static final FRAGMENT_MYORDER_COMPLETE_DETAIL:Ljava/lang/String; = "fragment_myorder_complete_detail"

.field public static final FRAGMENT_MYORDER_QUERY:Ljava/lang/String; = "fragment_myorder_query"

.field public static final GDT_APP_ID:Ljava/lang/String; = "1107837297"

.field public static final GDT_SPLASH_ID:Ljava/lang/String; = "4060449291954141"

.field public static final JINGZHUNGU_URL:Ljava/lang/String; = "http://m.jingzhengu.com/xiansuo/sellcar-lulutong.html"

.field public static final KUNTAO_H5_URL:Ljava/lang/String; = "http://wap.lltskb.com/ktzx.html"

.field public static final LINEUP_ORDR_URL:Ljava/lang/String; = "https://kyfw.12306.cn/otn/view/lineUp_order.html"

.field public static final LLTSKB_LIFE_URL:Ljava/lang/String; = "http://wap.lltskb.com/shfw/"

.field public static final LLT_EXAM:Ljava/lang/String; = "http://ks.lltskb.com"

.field public static final LOGIN_RESULT_BROADCAST:Ljava/lang/String; = "com.lltskb.lltskb.order.login.result"

.field public static LOGIN_RESULT_MSG:Ljava/lang/String; = "login.msg"

.field public static final LOGIN_RESULT_STATUS:Ljava/lang/String; = "login.result"

.field public static final LOTTERY_ACTIVITY:Ljava/lang/String; = "http://4g.9188.com/activity/xrdl/index.html?in=llt"

.field public static final MEITUAN_LIBAO:Ljava/lang/String; = "http://wap.lltskb.com/mtlb.html"

.field public static final MEITUAN_TOURIST:Ljava/lang/String; = "http://r.union.meituan.com/url/visit/?a=1&key=90f3f6fec96edbd161847183ca9ba6a4414&url=http%3A%2F%2Fi.meituan.com%2F%3Ftype_v3%3D162%26nodown"

.field public static final NETWORD_ERROR_URL:Ljava/lang/String; = "http://www.12306.cn/mormhweb/logFiles/error.html"

.field public static final NETWORK_ERROR:Ljava/lang/String; = "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

.field public static final ONLINE_LOGIN:Ljava/lang/String; = "https://kyfw.12306.cn/otn/login/init"

.field public static final ONLINE_USER_INDEX:Ljava/lang/String; = "https://kyfw.12306.cn/otn/index/init"

.field public static final ORDER_ARRIVE_TIME:Ljava/lang/String; = "order_arrive_time"

.field public static final ORDER_DEPART_DATE:Ljava/lang/String; = "order_depart_date"

.field public static final ORDER_DURATION:Ljava/lang/String; = "order_duration"

.field public static final ORDER_FROM_FLAG:Ljava/lang/String; = "order_from_flag"

.field public static final ORDER_FROM_STATION:Ljava/lang/String; = "order_from_station"

.field public static final ORDER_PURPOSE:Ljava/lang/String; = "order_purpose"

.field public static final ORDER_SEAT_PRICE:Ljava/lang/String; = "order_seat_price"

.field public static final ORDER_SEAT_TYPE:Ljava/lang/String; = "order_seat_type"

.field public static final ORDER_START_TIME:Ljava/lang/String; = "order_start_time"

.field public static final ORDER_TOUR_FLAG:Ljava/lang/String; = "tour_flag"

.field public static final ORDER_TO_STATION:Ljava/lang/String; = "order_to_station"

.field public static final ORDER_TRAIN_CODE:Ljava/lang/String; = "order_train_code"

.field public static final PARAM_STATION_NAME:Ljava/lang/String; = "station"

.field public static final PUFA_BANK_URL:Ljava/lang/String; = "http://wap.lltskb.com/pfyh.html"

.field public static final QICHE_PRICE:Ljava/lang/String; = "http://auto.news18a.com/m/price/lulutong/"

.field public static final QUERY_DATE:Ljava/lang/String; = "query_date"

.field public static final QUERY_JPK:Ljava/lang/String; = "query_jpk"

.field public static final QUERY_METHOD:Ljava/lang/String; = "query_method"

.field public static final QUERY_METHOD_NORMAL:Ljava/lang/String; = "query_method_normal"

.field public static final QUERY_METHOD_SKBCX:Ljava/lang/String; = "query_method_skbcx"

.field public static final QUERY_METHOD_TRAINNO:Ljava/lang/String; = "query_method_trainno"

.field public static final QUERY_RESULT_CC_FUZZY:Ljava/lang/String; = "query_result_cc_fuzzy"

.field public static final QUERY_RESULT_QIYE:Ljava/lang/String; = "query_result_qiye"

.field public static final QUERY_RUNCHART_MAXDATE:Ljava/lang/String; = "run_chart_maxdate"

.field public static final QUERY_RUNCHART_MINDATE:Ljava/lang/String; = "run_chart_mindate"

.field public static final QUERY_RUNCHART_RULEINDEX:Ljava/lang/String; = "run_chart_runindex"

.field public static final QUERY_RUNCHART_STATION:Ljava/lang/String; = "run_chart_station"

.field public static final QUERY_TYPE:Ljava/lang/String; = "query_type"

.field public static final QUERY_TYPE_STATION:Ljava/lang/String; = "query_type_station"

.field public static final QUERY_TYPE_TICKET:Ljava/lang/String; = "query_type_ticket"

.field public static final QUERY_TYPE_TRAIN:Ljava/lang/String; = "query_type_train"

.field public static final QUERY_TYPE_TRAIN_BAIKE:Ljava/lang/String; = "query_type_train_baike"

.field public static final QUERY_TYPE_TRAIN_ZWD:Ljava/lang/String; = "query_type_train_zwd"

.field public static final QUERY_TYPE_ZZTIME:Ljava/lang/String; = "query_type_zztime"

.field public static final QUNAR_FLIGHT:Ljava/lang/String; = "http://touch.qunar.com/h5/flight/?bd_source=lulutong"

.field public static final QUNAR_FLIGHT_FORMAT:Ljava/lang/String; = "http://touch.qunar.com/h5/flight/flightlist?bd_source=lulutong&startCity=%s&destCity=%s&startDate=%s&backDate=&flightType=oneWay"

.field public static final QUNAR_FLIGHT_WS:Ljava/lang/String; = "http://ws.qunar.com/all_lp.jcp?from=%s&to=%s&goDate=%s&count=2&output=json"

.field public static final QUNAR_HOTEL:Ljava/lang/String; = "http://touch.qunar.com/h5/hotel/?bd_source=lulutong"

.field public static final QUNAR_HOTEL_FORMAT:Ljava/lang/String; = "http://touch.qunar.com/h5/hotel/hotellist?bd_source=lulutong&keywords=&city=%s&checkInDate=%s&checkOutDate="

.field public static final QUNAR_ORDER_QUERY:Ljava/lang/String; = "http://touch.qunar.com/h5/flight/flightorderqmc?bd_source=lulutong"

.field public static final REGISTER:Ljava/lang/String; = "https://kyfw.12306.cn/otn/regist/init"

.field public static final REQUEST_CODE:Ljava/lang/String; = "code"

.field public static final REQUEST_CODE_FROM:I = 0x1

.field public static final REQUEST_CODE_STATION:I = 0x3

.field public static final REQUEST_CODE_TO:I = 0x2

.field public static final SHOW_MORE_TICKETS:Ljava/lang/String; = "show_more_ticket"

.field public static final START_TRAIN_DATE:Ljava/lang/String; = "start_train_date"

.field public static final STATION_CODE:Ljava/lang/String; = "station_code"

.field public static final TICKET_ARRIVE_STATION:Ljava/lang/String; = "ticket_arrive_station"

.field public static final TICKET_DATE:Ljava/lang/String; = "ticket_date"

.field public static final TICKET_RETURN:Ljava/lang/String; = "https://kyfw.12306.cn/otn/queryOrder/init"

.field public static final TICKET_START_STATION:Ljava/lang/String; = "ticket_start_station"

.field public static final TIELU_ZHAOPIN:Ljava/lang/String; = "http://wap.lltskb.com/zpxx"

.field public static final TONGCHENG_ADDR:Ljava/lang/String; = "http://www.17u.cn/scenery/#refid=17983686"

.field public static final TONGCHENG_JH:Ljava/lang/String; = "http://jump.luna.58.com/i/26QL"

.field public static final TRAIN_CODE:Ljava/lang/String; = "train_code"

.field public static final TRAIN_NAME:Ljava/lang/String; = "train_name"

.field public static final TRAIN_TYPE:Ljava/lang/String; = "ticket_type"

.field public static final TUIA_URL:Ljava/lang/String; = "https://engine.tuia.cn/index/activity?appKey=3ZoJy5pWvuzs9VkB4dTdQTZJa9sE&adslotId=1297"

.field public static final USER_AGENT:Ljava/lang/String; = "Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/537.1 LBBROWSER"

.field public static final USER_NOT_SIGNIN:Ljava/lang/String; = "\u7528\u6237\u672a\u767b\u5f55"

.field public static final WANGYI_BAOXIAN:Ljava/lang/String; = "http://baoxian.163.com/activity/zxtpl/index.html?actiId=2016120718act215696590&remark=lulutong1"

.field public static final WANGYI_ZENGXIAN:Ljava/lang/String; = "http://baoxian.163.com/activity/didiCoupon.html?remark=lulutong1"

.field public static final WEB_CLOSEONBACK:Ljava/lang/String; = "web_closeonback"

.field public static final WEB_GET:Ljava/lang/String; = "web_get"

.field public static final WEB_PARAMS:Ljava/lang/String; = "web_params"

.field public static final WEB_POST:Ljava/lang/String; = "web_post"

.field public static final WEB_URL:Ljava/lang/String; = "web_url"

.field public static final XINDAN_BAOXIAN:Ljava/lang/String; = "http://xbbapi.data88.cn/insurance/doInsure"

.field public static final XINDAN_H5_URL:Ljava/lang/String; = "http://t.cn/AipnJuQ3"

.field public static final XINGYUAN_QICHE:Ljava/lang/String; = "https://h5.qichedaquan.com/?utm_source=lulutong"

.field public static final key:Ljava/lang/String; = "adfja3290alks*&^t83qelkasd823rhl"

.field public static final mGHId:Ljava/lang/String; = "1a1560ba368e4892ce3be77481bc5adb"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
