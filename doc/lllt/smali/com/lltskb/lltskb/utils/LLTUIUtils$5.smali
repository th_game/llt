.class final Lcom/lltskb/lltskb/utils/LLTUIUtils$5;
.super Ljava/lang/Object;
.source "LLTUIUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/utils/LLTUIUtils;->showRandCodeDlg(Landroid/content/Context;Ljava/lang/String;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$l:Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;

.field final synthetic val$username:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/widget/EditText;Landroid/content/Context;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V
    .locals 0

    .line 1690
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$username:Landroid/widget/EditText;

    iput-object p2, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$l:Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1694
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$username:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 1695
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 1700
    :cond_0
    iget-object p2, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$l:Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;

    if-eqz p2, :cond_1

    .line 1701
    invoke-interface {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;->onClickOk(Ljava/lang/String;)V

    .line 1703
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->access$000()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->access$200(Landroid/support/v7/app/AlertDialog;)V

    .line 1704
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->access$000()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    return-void

    .line 1696
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;->val$context:Landroid/content/Context;

    const-string p2, "\u9a8c\u8bc1\u7801\u662f6\u4f4d\u6570\u5b57"

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 1697
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->access$000()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->access$100(Landroid/support/v7/app/AlertDialog;)V

    return-void
.end method
