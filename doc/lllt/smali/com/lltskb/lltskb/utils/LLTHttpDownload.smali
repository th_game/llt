.class public Lcom/lltskb/lltskb/utils/LLTHttpDownload;
.super Ljava/lang/Object;
.source "LLTHttpDownload.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LLTHttpDownload"


# instance fields
.field private mErrorMsg:Ljava/lang/String;

.field private mResult:Ljava/lang/String;

.field private mTag:Ljava/lang/Object;

.field private mUrl:Ljava/net/URL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mUrl:Ljava/net/URL;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/utils/LLTHttpDownload;)Ljava/net/URL;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mUrl:Ljava/net/URL;

    return-object p0
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/net/URL;)Ljava/net/URL;
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mUrl:Ljava/net/URL;

    return-object p1
.end method

.method static synthetic access$102(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mResult:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mResult:Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;-><init>(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/lang/String;ILcom/lltskb/lltskb/utils/IDownloadListener;)V

    .line 92
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    const-wide/16 v1, 0x1f4

    .line 97
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    .line 100
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 103
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, p1

    int-to-long v5, p3

    cmp-long v7, v3, v5

    if-gez v7, :cond_0

    .line 105
    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    .line 107
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 112
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 114
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mResult:Ljava/lang/String;

    return-object p1
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;)Z
    .locals 11

    const-string v0, "\u5b57\u8282\u8bfb\u53d6\u6d41\u5173\u95ed\u5931\u8d25\uff01"

    const/16 v1, 0x64

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 127
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mUrl:Ljava/net/URL;

    .line 128
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mUrl:Ljava/net/URL;

    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;

    .line 129
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 130
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 131
    new-instance p3, Ljava/io/File;

    invoke-direct {p3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_0

    .line 133
    invoke-virtual {p3}, Ljava/io/File;->createNewFile()Z

    .line 135
    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result p1

    if-gtz p1, :cond_1

    const/4 p1, 0x1

    .line 138
    :cond_1
    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 p3, 0x7800

    new-array p3, p3, [B

    const/4 v5, 0x0

    .line 142
    :goto_0
    invoke-virtual {v4, p3}, Ljava/io/InputStream;->read([B)I

    move-result v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v7, -0x1

    const-string v8, ""

    if-eq v6, v7, :cond_4

    .line 143
    :try_start_1
    invoke-virtual {p2, p3, v3, v6}, Ljava/io/FileOutputStream;->write([BII)V

    mul-int/lit8 v7, v5, 0x64

    int-to-float v7, v7

    int-to-float v9, p1

    div-float/2addr v7, v9

    float-to-int v7, v7

    if-eqz p4, :cond_3

    const/4 v9, 0x2

    .line 147
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, "%"

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p4, v9, v7, v8}, Lcom/lltskb/lltskb/utils/IDownloadListener;->OnStatus(IILjava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 149
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 150
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_2

    .line 168
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 170
    sget-object p2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_2
    :goto_1
    return v3

    :cond_3
    add-int/2addr v5, v6

    goto :goto_0

    .line 156
    :cond_4
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 157
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v4, :cond_5

    .line 168
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 170
    sget-object p2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_5
    :goto_2
    if-eqz p4, :cond_6

    const/4 p1, 0x3

    .line 176
    invoke-interface {p4, p1, v1, v8}, Lcom/lltskb/lltskb/utils/IDownloadListener;->OnStatus(IILjava/lang/String;)Z

    :cond_6
    return v2

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_2
    move-exception p1

    .line 159
    :try_start_5
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    const-string p1, "\u4e0b\u8f7d\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5\u7f51\u7edc\u8bbe\u7f6e\uff01"

    .line 160
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mErrorMsg:Ljava/lang/String;

    if-eqz p4, :cond_7

    .line 162
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mErrorMsg:Ljava/lang/String;

    invoke-interface {p4, v2, v1, p1}, Lcom/lltskb/lltskb/utils/IDownloadListener;->OnStatus(IILjava/lang/String;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_7
    if-eqz v4, :cond_8

    .line 168
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    .line 170
    sget-object p2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_8
    :goto_3
    return v3

    :goto_4
    if-eqz v4, :cond_9

    .line 168
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_5

    :catch_4
    move-exception p2

    .line 170
    sget-object p3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Ljava/io/IOException;->printStackTrace()V

    .line 172
    :cond_9
    :goto_5
    goto :goto_7

    :goto_6
    throw p1

    :goto_7
    goto :goto_6
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->mTag:Ljava/lang/Object;

    return-void
.end method
