.class Lcom/lltskb/lltskb/utils/DynamicJsUtil$Base32;
.super Ljava/lang/Object;
.source "DynamicJsUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/utils/DynamicJsUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Base32"
.end annotation


# static fields
.field private static delta:I = -0x61c88648


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    const-string v0, ""

    .line 74
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    .line 77
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/DynamicJsUtil$Base32;->stringToLongArray(Ljava/lang/String;Z)[I

    move-result-object p0

    const/4 v1, 0x0

    .line 78
    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/DynamicJsUtil$Base32;->stringToLongArray(Ljava/lang/String;Z)[I

    move-result-object p1

    .line 79
    array-length v2, p1

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    new-array v2, v3, [I

    .line 81
    array-length v3, p1

    invoke-static {p1, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v2

    .line 84
    :cond_1
    array-length v2, p0

    sub-int/2addr v2, v0

    .line 85
    aget v0, p0, v2

    aget v3, p0, v1

    const/16 v3, 0x34

    add-int/lit8 v4, v2, 0x1

    .line 87
    div-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x6

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v3, v3

    move v4, v0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v5, v3, -0x1

    if-lez v3, :cond_3

    .line 89
    sget v3, Lcom/lltskb/lltskb/utils/DynamicJsUtil$Base32;->delta:I

    add-int/2addr v0, v3

    and-int/lit8 v0, v0, -0x1

    ushr-int/lit8 v3, v0, 0x2

    and-int/lit8 v3, v3, 0x3

    move v6, v4

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_2

    add-int/lit8 v7, v4, 0x1

    .line 92
    aget v8, p0, v7

    ushr-int/lit8 v9, v6, 0x5

    shl-int/lit8 v10, v8, 0x2

    xor-int/2addr v9, v10

    ushr-int/lit8 v10, v8, 0x3

    shl-int/lit8 v11, v6, 0x4

    xor-int/2addr v10, v11

    add-int/2addr v9, v10

    xor-int/2addr v8, v0

    and-int/lit8 v10, v4, 0x3

    xor-int/2addr v10, v3

    .line 93
    aget v10, p1, v10

    xor-int/2addr v6, v10

    add-int/2addr v8, v6

    xor-int v6, v9, v8

    .line 94
    aget v8, p0, v4

    add-int/2addr v8, v6

    and-int/lit8 v6, v8, -0x1

    aput v6, p0, v4

    move v4, v7

    goto :goto_1

    .line 96
    :cond_2
    aget v7, p0, v1

    ushr-int/lit8 v8, v6, 0x5

    shl-int/lit8 v9, v7, 0x2

    xor-int/2addr v8, v9

    ushr-int/lit8 v9, v7, 0x3

    shl-int/lit8 v10, v6, 0x4

    xor-int/2addr v9, v10

    add-int/2addr v8, v9

    xor-int/2addr v7, v0

    and-int/lit8 v4, v4, 0x3

    xor-int/2addr v3, v4

    .line 97
    aget v3, p1, v3

    xor-int/2addr v3, v6

    add-int/2addr v7, v3

    xor-int v3, v8, v7

    .line 98
    aget v4, p0, v2

    add-int/2addr v4, v3

    and-int/lit8 v4, v4, -0x1

    aput v4, p0, v2

    move v3, v5

    goto :goto_0

    .line 100
    :cond_3
    invoke-static {p0, v1}, Lcom/lltskb/lltskb/utils/DynamicJsUtil$Base32;->longArrayToString([IZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static longArrayToString([IZ)Ljava/lang/String;
    .locals 9

    .line 14
    array-length v0, p0

    add-int/lit8 v1, v0, -0x1

    shl-int/lit8 v2, v1, 0x2

    if-eqz p1, :cond_1

    .line 17
    aget v1, p0, v1

    add-int/lit8 v3, v2, -0x3

    if-lt v1, v3, :cond_0

    if-le v1, v2, :cond_2

    :cond_0
    const/4 p0, 0x0

    return-object p0

    :cond_1
    move v1, v2

    .line 24
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_7

    .line 26
    aget v5, p0, v4

    and-int/lit16 v5, v5, 0xff

    .line 27
    aget v6, p0, v4

    ushr-int/lit8 v6, v6, 0x8

    and-int/lit16 v6, v6, 0xff

    .line 28
    aget v7, p0, v4

    ushr-int/lit8 v7, v7, 0x10

    and-int/lit16 v7, v7, 0xff

    .line 29
    aget v8, p0, v4

    ushr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    if-eqz v5, :cond_3

    int-to-char v5, v5

    .line 31
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    if-eqz v6, :cond_4

    int-to-char v5, v6

    .line 33
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    if-eqz v7, :cond_5

    int-to-char v5, v7

    .line 35
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    if-eqz v8, :cond_6

    int-to-char v5, v8

    .line 37
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_7
    if-eqz p1, :cond_8

    .line 42
    invoke-virtual {v2, v3, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 45
    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method public static stringToLongArray(Ljava/lang/String;Z)[I
    .locals 10

    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 51
    rem-int/lit8 v1, v0, 0x4

    if-nez v1, :cond_0

    div-int/lit8 v1, v0, 0x4

    goto :goto_0

    :cond_0
    div-int/lit8 v1, v0, 0x4

    add-int/lit8 v1, v1, 0x1

    .line 52
    :goto_0
    new-array v2, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v0, :cond_5

    add-int/lit8 v5, v4, 0x4

    if-le v5, v0, :cond_4

    add-int/lit8 v6, v4, 0x1

    if-lt v6, v0, :cond_1

    const/4 v7, 0x0

    goto :goto_2

    .line 55
    :cond_1
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    shl-int/lit8 v7, v7, 0x8

    :goto_2
    add-int/lit8 v8, v4, 0x2

    if-lt v8, v0, :cond_2

    const/4 v8, 0x0

    goto :goto_3

    .line 56
    :cond_2
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    shl-int/lit8 v8, v8, 0x10

    :goto_3
    add-int/lit8 v9, v4, 0x3

    if-lt v9, v0, :cond_3

    const/4 v6, 0x0

    goto :goto_4

    .line 57
    :cond_3
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    shl-int/lit8 v6, v6, 0x18

    :goto_4
    shr-int/lit8 v9, v4, 0x2

    .line 58
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    or-int/2addr v4, v7

    or-int/2addr v4, v8

    or-int/2addr v4, v6

    aput v4, v2, v9

    goto :goto_5

    :cond_4
    shr-int/lit8 v6, v4, 0x2

    .line 62
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    shl-int/lit8 v8, v8, 0x8

    or-int/2addr v7, v8

    add-int/lit8 v8, v4, 0x2

    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    shl-int/lit8 v8, v8, 0x10

    or-int/2addr v7, v8

    add-int/lit8 v4, v4, 0x3

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    shl-int/lit8 v4, v4, 0x18

    or-int/2addr v4, v7

    aput v4, v2, v6

    :goto_5
    move v4, v5

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_6

    add-int/lit8 p0, v1, 0x1

    .line 65
    new-array p0, p0, [I

    .line 66
    invoke-static {v2, v3, p0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    aput v0, p0, v1

    goto :goto_6

    :cond_6
    move-object p0, v2

    :goto_6
    return-object p0
.end method
