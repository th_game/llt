.class public Lcom/lltskb/lltskb/utils/PermissionHelper;
.super Ljava/lang/Object;
.source "PermissionHelper.java"


# static fields
.field public static final PACKAGE:Ljava/lang/String; = "PermissionHelper"

.field private static alertDialog:Landroid/app/AlertDialog;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic lambda$showMissingPermissionDialog$0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 95
    sget-object p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method static synthetic lambda$showMissingPermissionDialog$1(Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 96
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/PermissionHelper;->startAppSettings(Landroid/content/Context;)V

    return-void
.end method

.method public static showMissingPermissionDialog(Landroid/content/Context;)V
    .locals 3

    .line 89
    sget-object v0, Lcom/lltskb/lltskb/utils/PermissionHelper;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 92
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0d0216

    .line 93
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0d008a

    .line 95
    sget-object v2, Lcom/lltskb/lltskb/utils/-$$Lambda$PermissionHelper$CCImNYS0prUByg6HC0Y4UR2Bet4;->INSTANCE:Lcom/lltskb/lltskb/utils/-$$Lambda$PermissionHelper$CCImNYS0prUByg6HC0Y4UR2Bet4;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0d0268

    .line 96
    new-instance v2, Lcom/lltskb/lltskb/utils/-$$Lambda$PermissionHelper$C0T3GPvOVCuV-Sm0sSJD7QIHMD0;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/utils/-$$Lambda$PermissionHelper$C0T3GPvOVCuV-Sm0sSJD7QIHMD0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    sput-object p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->alertDialog:Landroid/app/AlertDialog;

    .line 99
    sget-object p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static startAppSettings(Landroid/content/Context;)V
    .locals 3

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public checkPermission(Ljava/lang/String;)Z
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public varargs checkPermissions([Ljava/lang/String;)Z
    .locals 4

    .line 37
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 38
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/utils/PermissionHelper;->checkPermission(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public permissionsCheck(Ljava/lang/String;I)V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->mContext:Landroid/content/Context;

    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_0

    return-void

    .line 70
    :cond_0
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, p1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-array v2, v2, [Ljava/lang/String;

    aput-object p1, v2, v1

    invoke-static {v0, v2, p2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/PermissionHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-array v2, v2, [Ljava/lang/String;

    aput-object p1, v2, v1

    invoke-static {v0, v2, p2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    :goto_0
    return-void
.end method
