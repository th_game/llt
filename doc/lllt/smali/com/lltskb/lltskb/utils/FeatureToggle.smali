.class public Lcom/lltskb/lltskb/utils/FeatureToggle;
.super Ljava/lang/Object;
.source "FeatureToggle.java"


# static fields
.field public static final BAOXIAN_HINT:Ljava/lang/String; = "baoxian_hint"

.field public static final KUNTAOH5:Ljava/lang/String; = "kuntao_h5"

.field public static final TONGCHENG:Ljava/lang/String; = "58tongcheng"

.field public static final UAMTK_OAUTH:Ljava/lang/String; = "uamtk"

.field private static mFeatureMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initFeatures()V
    .locals 3

    .line 23
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "baoxian_hint"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    const-string v2, "58tongcheng"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    const-string v2, "uamtk"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    const-string v2, "kuntao_h5"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static isBaoxianEnabled()Z
    .locals 1

    const-string v0, "kuntao_h5"

    .line 40
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isFeatureEnabled(Ljava/lang/String;)Z
    .locals 1

    .line 30
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 32
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static setFeatureEnabled(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .line 36
    sget-object v0, Lcom/lltskb/lltskb/utils/FeatureToggle;->mFeatureMaps:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
