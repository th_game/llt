.class final Lcom/lltskb/lltskb/utils/LLTUIUtils$2;
.super Ljava/lang/Object;
.source "LLTUIUtils.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cxt:Lcom/lltskb/lltskb/BaseActivity;

.field final synthetic val$dlg:Landroid/support/v7/app/AppCompatDialog;

.field final synthetic val$id:I

.field final synthetic val$l:Landroid/widget/AdapterView$OnItemClickListener;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/BaseActivity;ILjava/util/List;Landroid/widget/AdapterView$OnItemClickListener;Landroid/support/v7/app/AppCompatDialog;)V
    .locals 0

    .line 1354
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$cxt:Lcom/lltskb/lltskb/BaseActivity;

    iput p2, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$id:I

    iput-object p3, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$list:Ljava/util/List;

    iput-object p4, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$l:Landroid/widget/AdapterView$OnItemClickListener;

    iput-object p5, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$dlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 1359
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$cxt:Lcom/lltskb/lltskb/BaseActivity;

    iget v1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$id:I

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1361
    iget-object v1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$list:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1364
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$l:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v2, :cond_1

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    .line 1365
    invoke-interface/range {v2 .. v7}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 1367
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;->val$dlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method
