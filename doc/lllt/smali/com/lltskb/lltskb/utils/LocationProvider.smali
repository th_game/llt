.class public Lcom/lltskb/lltskb/utils/LocationProvider;
.super Ljava/lang/Object;
.source "LocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationProvider"

.field private static mInstance:Lcom/lltskb/lltskb/utils/LocationProvider;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLatitude:D

.field private mLocation:Landroid/location/Location;

.field private mLocationMgr:Landroid/location/LocationManager;

.field private mLongitude:D


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 29
    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    .line 30
    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    const-wide v0, 0x4043f3bd48cb4aedL    # 39.904214

    .line 46
    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    const-wide v0, 0x405d1a130df9bdc8L    # 116.40741300000002

    .line 47
    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/utils/LocationProvider;
    .locals 1

    .line 35
    sget-object v0, Lcom/lltskb/lltskb/utils/LocationProvider;->mInstance:Lcom/lltskb/lltskb/utils/LocationProvider;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/lltskb/lltskb/utils/LocationProvider;

    invoke-direct {v0}, Lcom/lltskb/lltskb/utils/LocationProvider;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LocationProvider;->mInstance:Lcom/lltskb/lltskb/utils/LocationProvider;

    .line 40
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/utils/LocationProvider;->mInstance:Lcom/lltskb/lltskb/utils/LocationProvider;

    return-object v0
.end method


# virtual methods
.method public getLatitude()D
    .locals 2

    .line 114
    iget-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .line 118
    iget-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    return-wide v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3

    .line 51
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mContext:Landroid/content/Context;

    .line 52
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mContext:Landroid/content/Context;

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p1, v0}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    const-string v0, "LocationProvider"

    if-eqz p1, :cond_0

    const-string p1, "init permission not granted"

    .line 53
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 56
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/location/LocationManager;

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocationMgr:Landroid/location/LocationManager;

    .line 57
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocationMgr:Landroid/location/LocationManager;

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-string v1, "network"

    .line 58
    invoke-virtual {p1, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    .line 59
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    if-nez p1, :cond_2

    .line 60
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocationMgr:Landroid/location/LocationManager;

    invoke-virtual {p1, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    .line 61
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    if-nez p1, :cond_3

    .line 63
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocationMgr:Landroid/location/LocationManager;

    invoke-virtual {p1, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    const-string p1, "init location failed"

    .line 64
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    .line 67
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    .line 68
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init latitude="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, " longitude="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .line 91
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    .line 92
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    if-eqz p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocationMgr:Landroid/location/LocationManager;

    invoke-virtual {p1, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 94
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    .line 95
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLocation:Landroid/location/Location;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    .line 96
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onLocationChanged latitude="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLatitude:D

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, " longitude="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/lltskb/lltskb/utils/LocationProvider;->mLongitude:D

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "LocationProvider"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
