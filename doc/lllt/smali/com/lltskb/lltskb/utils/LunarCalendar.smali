.class public Lcom/lltskb/lltskb/utils/LunarCalendar;
.super Ljava/lang/Object;
.source "LunarCalendar.java"


# static fields
.field static chineseDateFormat:Ljava/text/SimpleDateFormat;

.field static final chineseNumber:[Ljava/lang/String;

.field static final lunarInfo:[J


# instance fields
.field private day:I

.field private leap:Z

.field private month:I

.field private year:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\u4e00"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "\u4e8c"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "\u4e09"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "\u56db"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\u4e94"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\u516d"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\u4e03"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\u516b"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\u4e5d"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\u5341"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\u5341\u4e00"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\u5341\u4e8c"

    aput-object v2, v0, v1

    .line 27
    sput-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseNumber:[Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy\u5e74MM\u6708dd\u65e5"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseDateFormat:Ljava/text/SimpleDateFormat;

    const/16 v0, 0x96

    new-array v0, v0, [J

    .line 35
    fill-array-data v0, :array_0

    sput-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->lunarInfo:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x4bd8
        0x4ae0
        0xa570
        0x54d5
        0xd260
        0xd950
        0x16554
        0x56a0
        0x9ad0
        0x55d2
        0x4ae0
        0xa5b6
        0xa4d0
        0xd250
        0x1d255
        0xb540
        0xd6a0
        0xada2
        0x95b0
        0x14977
        0x4970
        0xa4b0
        0xb4b5
        0x6a50
        0x6d40
        0x1ab54
        0x2b60
        0x9570
        0x52f2
        0x4970
        0x6566
        0xd4a0
        0xea50
        0x6e95
        0x5ad0
        0x2b60
        0x186e3
        0x92e0
        0x1c8d7
        0xc950
        0xd4a0
        0x1d8a6
        0xb550
        0x56a0
        0x1a5b4
        0x25d0
        0x92d0
        0xd2b2
        0xa950
        0xb557
        0x6ca0
        0xb550
        0x15355
        0x4da0
        0xa5d0
        0x14573
        0x52d0
        0xa9a8
        0xe950
        0x6aa0
        0xaea6
        0xab50
        0x4b60
        0xaae4
        0xa570
        0x5260
        0xf263
        0xd950
        0x5b57
        0x56a0
        0x96d0
        0x4dd5
        0x4ad0
        0xa4d0
        0xd4d4
        0xd250
        0xd558
        0xb540
        0xb5a0
        0x195a6
        0x95b0
        0x49b0
        0xa974
        0xa4b0
        0xb27a
        0x6a50
        0x6d40
        0xaf46
        0xab60
        0x9570
        0x4af5
        0x4970
        0x64b0
        0x74a3
        0xea50
        0x6b58
        0x55c0
        0xab60
        0x96d5
        0x92e0
        0xc960
        0xd954
        0xd4a0
        0xda50
        0x7552
        0x56a0
        0xabb7
        0x25d0
        0x92d0
        0xcab5
        0xa950
        0xb4a0
        0xbaa4
        0xad50
        0x55d9
        0x4ba0
        0xa5b0
        0x15176
        0x52b0
        0xa930
        0x7954
        0x6aa0
        0xad50
        0x5b52
        0x4b60
        0xa6e6
        0xa4e0
        0xd260
        0xea65
        0xd530
        0x5aa0
        0x76a3
        0x96d0
        0x4bd7
        0x4ad0
        0xa4d0
        0x1d0b6
        0xd250
        0xd520
        0xdd45
        0xb5a0
        0x56d0
        0x55b2
        0x49b0
        0xa577
        0xa4b0
        0xaa50
        0x1b255
        0x6d20
        0xada0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .line 108
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;-><init>(Ljava/util/Calendar;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Calendar;)V
    .locals 6

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    :try_start_0
    sget-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseDateFormat:Ljava/text/SimpleDateFormat;

    const-string v1, "1900\u5e741\u670831\u65e5"

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 140
    :goto_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x5265c00

    div-long/2addr v1, v3

    long-to-int p1, v1

    const/16 v0, 0x76c

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    const/16 v3, 0x802

    if-ge v0, v3, :cond_0

    if-lez p1, :cond_0

    .line 150
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->yearDays(I)I

    move-result v2

    sub-int/2addr p1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    if-gez p1, :cond_1

    add-int/2addr p1, v2

    add-int/lit8 v0, v0, -0x1

    .line 160
    :cond_1
    iput v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    .line 163
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->leapMonth(I)I

    move-result v0

    .line 164
    iput-boolean v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    const/4 v2, 0x1

    move v3, p1

    const/4 p1, 0x1

    const/4 v4, 0x0

    :goto_2
    const/16 v5, 0xd

    if-ge p1, v5, :cond_4

    if-lez v3, :cond_4

    if-lez v0, :cond_2

    add-int/lit8 v4, v0, 0x1

    if-ne p1, v4, :cond_2

    .line 170
    iget-boolean v4, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    if-nez v4, :cond_2

    add-int/lit8 p1, p1, -0x1

    .line 172
    iput-boolean v2, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    .line 173
    iget v4, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/LunarCalendar;->leapDays(I)I

    move-result v4

    goto :goto_3

    .line 175
    :cond_2
    iget v4, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    invoke-static {v4, p1}, Lcom/lltskb/lltskb/utils/LunarCalendar;->monthDays(II)I

    move-result v4

    :goto_3
    sub-int/2addr v3, v4

    .line 179
    iget-boolean v5, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    if-eqz v5, :cond_3

    add-int/lit8 v5, v0, 0x1

    if-ne p1, v5, :cond_3

    .line 180
    iput-boolean v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    .line 181
    :cond_3
    iget-boolean v5, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_4
    if-nez v3, :cond_6

    if-lez v0, :cond_6

    add-int/2addr v0, v2

    if-ne p1, v0, :cond_6

    .line 186
    iget-boolean v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    if-eqz v0, :cond_5

    .line 187
    iput-boolean v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    goto :goto_4

    .line 189
    :cond_5
    iput-boolean v2, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    add-int/lit8 p1, p1, -0x1

    :cond_6
    :goto_4
    if-gez v3, :cond_7

    add-int/2addr v3, v4

    add-int/lit8 p1, p1, -0x1

    .line 200
    :cond_7
    iput p1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    add-int/2addr v3, v2

    .line 201
    iput v3, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    return-void
.end method

.method public constructor <init>(Ljava/util/Date;)V
    .locals 0

    .line 115
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LunarCalendar;->date2calendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/utils/LunarCalendar;-><init>(Ljava/util/Calendar;)V

    return-void
.end method

.method private static final cyclicalm(I)Ljava/lang/String;
    .locals 15

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u7532"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "\u4e59"

    aput-object v4, v1, v3

    const/4 v4, 0x2

    const-string v5, "\u4e19"

    aput-object v5, v1, v4

    const/4 v5, 0x3

    const-string v6, "\u4e01"

    aput-object v6, v1, v5

    const/4 v6, 0x4

    const-string v7, "\u620a"

    aput-object v7, v1, v6

    const/4 v7, 0x5

    const-string v8, "\u5df1"

    aput-object v8, v1, v7

    const/4 v8, 0x6

    const-string v9, "\u5e9a"

    aput-object v9, v1, v8

    const/4 v9, 0x7

    const-string v10, "\u8f9b"

    aput-object v10, v1, v9

    const/16 v10, 0x8

    const-string v11, "\u58ec"

    aput-object v11, v1, v10

    const/16 v11, 0x9

    const-string v12, "\u7678"

    aput-object v12, v1, v11

    const/16 v12, 0xc

    new-array v13, v12, [Ljava/lang/String;

    const-string v14, "\u5b50"

    aput-object v14, v13, v2

    const-string v2, "\u4e11"

    aput-object v2, v13, v3

    const-string v2, "\u5bc5"

    aput-object v2, v13, v4

    const-string v2, "\u536f"

    aput-object v2, v13, v5

    const-string v2, "\u8fb0"

    aput-object v2, v13, v6

    const-string v2, "\u5df3"

    aput-object v2, v13, v7

    const-string v2, "\u5348"

    aput-object v2, v13, v8

    const-string v2, "\u672a"

    aput-object v2, v13, v9

    const-string v2, "\u7533"

    aput-object v2, v13, v10

    const-string v2, "\u9149"

    aput-object v2, v13, v11

    const-string v2, "\u620c"

    aput-object v2, v13, v0

    const/16 v0, 0xb

    const-string v2, "\u4ea5"

    aput-object v2, v13, v0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v2, p0, 0xa

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    rem-int/2addr p0, v12

    aget-object p0, v13, p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static date2calendar(Ljava/util/Date;)Ljava/util/Calendar;
    .locals 1

    .line 119
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 120
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    return-object v0
.end method

.method public static getChinaDayString(I)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\u521d"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "\u5341"

    aput-object v2, v0, v1

    const/4 v2, 0x2

    const-string v3, "\u5eff"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "\u5345"

    aput-object v3, v0, v2

    .line 219
    rem-int/lit8 v2, p0, 0xa

    if-nez v2, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v2, -0x1

    :goto_0
    const/16 v2, 0x1e

    if-le p0, v2, :cond_1

    const-string p0, ""

    return-object p0

    :cond_1
    const/16 v2, 0xa

    if-ne p0, v2, :cond_2

    const-string p0, "\u521d\u5341"

    return-object p0

    .line 225
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    div-int/2addr p0, v2

    aget-object p0, v0, p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseNumber:[Ljava/lang/String;

    aget-object p0, p0, v1

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final leapDays(I)I
    .locals 5

    .line 61
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->leapMonth(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    sget-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->lunarInfo:[J

    add-int/lit16 p0, p0, -0x76c

    aget-wide v1, v0, p0

    const-wide/32 v3, 0x10000

    and-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long p0, v1, v3

    if-eqz p0, :cond_0

    const/16 p0, 0x1e

    return p0

    :cond_0
    const/16 p0, 0x1d

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static final leapMonth(I)I
    .locals 5

    .line 72
    sget-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->lunarInfo:[J

    add-int/lit16 p0, p0, -0x76c

    aget-wide v1, v0, p0

    const-wide/16 v3, 0xf

    and-long/2addr v1, v3

    long-to-int p0, v1

    return p0
.end method

.method private static final monthDays(II)I
    .locals 3

    .line 77
    sget-object v0, Lcom/lltskb/lltskb/utils/LunarCalendar;->lunarInfo:[J

    add-int/lit16 p0, p0, -0x76c

    aget-wide v1, v0, p0

    const/high16 p0, 0x10000

    shr-int/2addr p0, p1

    int-to-long p0, p0

    and-long/2addr p0, v1

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/16 p0, 0x1d

    return p0

    :cond_0
    const/16 p0, 0x1e

    return p0
.end method

.method private static final yearDays(I)I
    .locals 7

    const/16 v0, 0x15c

    const v1, 0x8000

    :goto_0
    const/16 v2, 0x8

    if-le v1, v2, :cond_1

    .line 53
    sget-object v2, Lcom/lltskb/lltskb/utils/LunarCalendar;->lunarInfo:[J

    add-int/lit16 v3, p0, -0x76c

    aget-wide v3, v2, v3

    int-to-long v5, v1

    and-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    shr-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    :cond_1
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->leapDays(I)I

    move-result p0

    add-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public final animalsYear()Ljava/lang/String;
    .locals 5

    const/16 v0, 0xc

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u9f20"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "\u725b"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "\u864e"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "\u5154"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "\u9f99"

    aput-object v3, v1, v2

    const/4 v3, 0x5

    const-string v4, "\u86c7"

    aput-object v4, v1, v3

    const/4 v3, 0x6

    const-string v4, "\u9a6c"

    aput-object v4, v1, v3

    const/4 v3, 0x7

    const-string v4, "\u7f8a"

    aput-object v4, v1, v3

    const/16 v3, 0x8

    const-string v4, "\u7334"

    aput-object v4, v1, v3

    const/16 v3, 0x9

    const-string v4, "\u9e21"

    aput-object v4, v1, v3

    const/16 v3, 0xa

    const-string v4, "\u72d7"

    aput-object v4, v1, v3

    const/16 v3, 0xb

    const-string v4, "\u732a"

    aput-object v4, v1, v3

    .line 86
    iget v3, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    sub-int/2addr v3, v2

    rem-int/2addr v3, v0

    aget-object v0, v1, v3

    return-object v0
.end method

.method public final cyclical()Ljava/lang/String;
    .locals 1

    .line 98
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    add-int/lit16 v0, v0, -0x76c

    add-int/lit8 v0, v0, 0x24

    .line 99
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->cyclicalm(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChinaDay()I
    .locals 1

    .line 233
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    return v0
.end method

.method public getChinaMonth()I
    .locals 1

    .line 237
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    return v0
.end method

.method public getChinaMonthString()Ljava/lang/String;
    .locals 3

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    if-eqz v1, :cond_0

    const-string v1, "\u95f0"

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseNumber:[Ljava/lang/String;

    iget v2, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u6708"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChineseHoliday()Ljava/lang/String;
    .locals 4

    .line 245
    iget-boolean v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 247
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v2, :cond_1

    const-string v0, "\u6625\u8282"

    return-object v0

    .line 249
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/16 v3, 0xf

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v3, :cond_2

    const-string v0, "\u5143\u5bb5\u8282"

    return-object v0

    .line 251
    :cond_2
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v2, :cond_3

    const-string v0, "\u7aef\u5348\u8282"

    return-object v0

    .line 255
    :cond_3
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v2, :cond_4

    const-string v0, "\u4e03\u5915\u8282"

    return-object v0

    .line 257
    :cond_4
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_5

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v3, :cond_5

    const-string v0, "\u4e2d\u79cb\u8282"

    return-object v0

    .line 259
    :cond_5
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_6

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v3, :cond_6

    const-string v0, "\u91cd\u9633\u8282"

    return-object v0

    .line 261
    :cond_6
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_7

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    if-ne v0, v2, :cond_7

    const-string v0, "\u814a\u516b\u8282"

    return-object v0

    .line 263
    :cond_7
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    if-ne v0, v3, :cond_8

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    const/16 v2, 0x17

    if-ne v0, v2, :cond_8

    const-string v0, "\u5c0f\u5e74"

    return-object v0

    .line 265
    :cond_8
    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    if-ne v0, v3, :cond_9

    iget v0, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    const/16 v2, 0x1e

    if-ne v0, v2, :cond_9

    const-string v0, "\u9664\u5915"

    return-object v0

    :cond_9
    return-object v1
.end method

.method public getDate()Ljava/util/Date;
    .locals 3

    .line 210
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 211
    iget v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 212
    iget v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    sub-int/2addr v1, v2

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 213
    iget v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 214
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->year:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\u5e74"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->leap:Z

    if-eqz v1, :cond_0

    const-string v1, "\u95f0"

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/lltskb/lltskb/utils/LunarCalendar;->chineseNumber:[Ljava/lang/String;

    iget v2, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->month:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u6708"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/utils/LunarCalendar;->day:I

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChinaDayString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
