.class public Lcom/lltskb/lltskb/utils/JSEngine;
.super Ljava/lang/Object;
.source "JSEngine.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "JSEngine"

.field private static mInstance:Lcom/lltskb/lltskb/utils/JSEngine;


# instance fields
.field private javascriptInterfaceBroken:Z

.field private mContext:Landroid/content/Context;

.field private volatile mExecuted:Z

.field private mKey:Ljava/lang/String;

.field private final mLock:Ljava/lang/Object;

.field private mResult:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z

    .line 38
    iput-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->javascriptInterfaceBroken:Z

    .line 47
    invoke-direct {p0}, Lcom/lltskb/lltskb/utils/JSEngine;->configureWebView()V

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/utils/JSEngine;Z)Z
    .locals 0

    .line 25
    iput-boolean p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/utils/JSEngine;)Ljava/lang/Object;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/utils/JSEngine;Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/utils/JSEngine;->loadUrlTask(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/utils/JSEngine;)Landroid/content/Context;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/utils/JSEngine;)Z
    .locals 0

    .line 25
    iget-boolean p0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->javascriptInterfaceBroken:Z

    return p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/utils/JSEngine;)Ljava/lang/String;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mKey:Ljava/lang/String;

    return-object p0
.end method

.method private configureWebView()V
    .locals 2

    .line 52
    :try_start_0
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->javascriptInterfaceBroken:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :catch_0
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "configureWebView "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->javascriptInterfaceBroken:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSEngine"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/utils/JSEngine;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/utils/JSEngine;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/utils/JSEngine;->mInstance:Lcom/lltskb/lltskb/utils/JSEngine;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/lltskb/lltskb/utils/JSEngine;

    invoke-direct {v1}, Lcom/lltskb/lltskb/utils/JSEngine;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/utils/JSEngine;->mInstance:Lcom/lltskb/lltskb/utils/JSEngine;

    .line 43
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/utils/JSEngine;->mInstance:Lcom/lltskb/lltskb/utils/JSEngine;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private loadUrlTask(Ljava/lang/String;)V
    .locals 6

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadUrlTask url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSEngine"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/lltskb/lltskb/BaseActivity;

    if-nez v2, :cond_0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v2, Landroid/webkit/WebView;

    invoke-direct {v2, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 86
    new-instance v0, Lcom/lltskb/lltskb/utils/JSEngine$1;

    invoke-direct {v0, p0, v2}, Lcom/lltskb/lltskb/utils/JSEngine$1;-><init>(Lcom/lltskb/lltskb/utils/JSEngine;Landroid/webkit/WebView;)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 113
    new-instance v0, Lcom/lltskb/lltskb/utils/JSEngine$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/utils/JSEngine$2;-><init>(Lcom/lltskb/lltskb/utils/JSEngine;)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 133
    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v3, 0x1

    .line 135
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 137
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 138
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 139
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 140
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 141
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 142
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 143
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 145
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_1

    const/4 v4, 0x0

    .line 146
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 149
    :cond_1
    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 150
    invoke-virtual {v2, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    const-string p1, "loadUrlTask end"

    .line 152
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 80
    :cond_2
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "loadUrlTask returned context="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getSignEncrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getSignEncrypt key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSEngine"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 202
    iput-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z

    const/4 v0, 0x0

    .line 203
    iput-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mResult:Ljava/lang/String;

    .line 204
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mKey:Ljava/lang/String;

    .line 206
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    new-instance v0, Lcom/lltskb/lltskb/utils/JSEngine$4;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/utils/JSEngine$4;-><init>(Lcom/lltskb/lltskb/utils/JSEngine;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 277
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    monitor-enter p1

    .line 278
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 280
    :try_start_1
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 283
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 286
    :cond_0
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string p1, "JSEngine"

    const-string v0, "getSignEncrypt end"

    .line 287
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mResult:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception v0

    .line 286
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :goto_1
    throw v0

    :goto_2
    goto :goto_1
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadUrl url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSEngine"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 158
    iput-boolean v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z

    .line 159
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    .line 160
    iget-object v1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    instance-of v2, v1, Lcom/lltskb/lltskb/BaseActivity;

    if-nez v2, :cond_0

    goto :goto_2

    .line 166
    :cond_0
    check-cast v1, Lcom/lltskb/lltskb/BaseActivity;

    new-instance v2, Lcom/lltskb/lltskb/utils/JSEngine$3;

    invoke-direct {v2, p0, p1}, Lcom/lltskb/lltskb/utils/JSEngine$3;-><init>(Lcom/lltskb/lltskb/utils/JSEngine;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 182
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    monitor-enter p1

    .line 185
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 187
    :try_start_1
    iget-object v1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 189
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_1
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string p1, "JSEngine"

    const-string v0, "loadUrl end"

    .line 194
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    .line 193
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 161
    :cond_2
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "loadUrl returned context="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "JSEngine"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mContext:Landroid/content/Context;

    return-void
.end method

.method public setSignValue(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setSignValue = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSEngine"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mResult:Ljava/lang/String;

    const/4 p1, 0x1

    .line 71
    iput-boolean p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mExecuted:Z

    .line 72
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    monitor-enter p1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/JSEngine;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 74
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
