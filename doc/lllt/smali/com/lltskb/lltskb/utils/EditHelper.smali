.class public Lcom/lltskb/lltskb/utils/EditHelper;
.super Ljava/lang/Object;
.source "EditHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EditHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static installEditClear(Landroid/view/View;)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    .line 37
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0900fd

    .line 39
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const v2, 0x7f09022a

    .line 42
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    if-eqz v0, :cond_2

    const/16 v2, 0x8

    .line 44
    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 45
    sget-object v2, Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$K1mR5jYLZ07HFoMvfQV0gFaoGgE;->INSTANCE:Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$K1mR5jYLZ07HFoMvfQV0gFaoGgE;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 50
    :cond_2
    instance-of v0, p0, Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 51
    check-cast p0, Landroid/widget/EditText;

    .line 53
    new-instance v0, Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$B6i-K61c6hYldGQFXzuxiuYXgWA;

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$B6i-K61c6hYldGQFXzuxiuYXgWA;-><init>(Landroid/widget/EditText;Landroid/widget/ImageView;)V

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 73
    new-instance v0, Lcom/lltskb/lltskb/utils/EditHelper$1;

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/utils/EditHelper$1;-><init>(Landroid/widget/EditText;Landroid/widget/ImageView;)V

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    new-instance v0, Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$KJRVvH0iAvYZxb8RuAKUGET5r-U;

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/utils/-$$Lambda$EditHelper$KJRVvH0iAvYZxb8RuAKUGET5r-U;-><init>(Landroid/widget/EditText;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method

.method static synthetic lambda$installEditClear$0(Landroid/widget/CompoundButton;Z)V
    .locals 0

    return-void
.end method

.method static synthetic lambda$installEditClear$1(Landroid/widget/EditText;Landroid/widget/ImageView;Landroid/view/View;Z)V
    .locals 4

    .line 54
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    .line 55
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 58
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    :goto_0
    invoke-virtual {p0}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v0

    const/16 v2, 0x50

    invoke-virtual {p0}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, p1, v0, v2, v3}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 61
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    if-eqz p3, :cond_1

    if-eqz p1, :cond_2

    .line 64
    invoke-virtual {p1, p2, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 68
    invoke-virtual {p0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object p0

    invoke-virtual {p1, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_2
    :goto_1
    return-void
.end method

.method static synthetic lambda$installEditClear$2(Landroid/widget/EditText;Landroid/widget/ImageView;Landroid/view/View;)V
    .locals 0

    const-string p2, ""

    .line 101
    invoke-virtual {p0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/16 p0, 0x8

    .line 102
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private static showPassword(Landroid/view/View;Z)V
    .locals 1

    .line 26
    instance-of v0, p0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 27
    check-cast p0, Landroid/widget/EditText;

    if-eqz p1, :cond_0

    .line 29
    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    goto :goto_0

    .line 31
    :cond_0
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    :cond_1
    :goto_0
    return-void
.end method
