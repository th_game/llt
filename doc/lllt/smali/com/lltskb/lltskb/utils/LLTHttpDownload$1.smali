.class Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;
.super Ljava/lang/Thread;
.source "LLTHttpDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/utils/LLTHttpDownload;->download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

.field final synthetic val$l:Lcom/lltskb/lltskb/utils/IDownloadListener;

.field final synthetic val$timeout:I

.field final synthetic val$urlStr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/lang/String;ILcom/lltskb/lltskb/utils/IDownloadListener;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    iput-object p2, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$urlStr:Ljava/lang/String;

    iput p3, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$timeout:I

    iput-object p4, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$l:Lcom/lltskb/lltskb/utils/IDownloadListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const-string v0, "\u6587\u672c\u6587\u4ef6\u8bfb\u53d6\u6d41\u5173\u95ed\u5931\u8d25\uff01"

    .line 48
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    const-string v1, "LLTHttpDownload"

    const-string v2, "network run"

    .line 49
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x64

    const/4 v4, 0x0

    .line 56
    :try_start_0
    iget-object v5, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    new-instance v6, Ljava/net/URL;

    iget-object v7, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$urlStr:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->access$002(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/net/URL;)Ljava/net/URL;

    .line 58
    iget-object v5, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->access$000(Lcom/lltskb/lltskb/utils/LLTHttpDownload;)Ljava/net/URL;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 59
    iget v6, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$timeout:I

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 61
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    .line 62
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :goto_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 64
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 67
    iget-object v5, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->access$102(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 78
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_1

    :catchall_0
    move-exception v2

    goto :goto_3

    :catch_1
    move-exception v2

    move-object v6, v4

    :goto_1
    :try_start_3
    const-string v5, "\u6587\u672c\u6587\u4ef6\u4e0b\u8f7d\u5931\u8d25\uff01"

    .line 69
    invoke-static {v1, v5}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 71
    iget-object v2, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$l:Lcom/lltskb/lltskb/utils/IDownloadListener;

    if-eqz v2, :cond_1

    .line 72
    iget-object v2, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$l:Lcom/lltskb/lltskb/utils/IDownloadListener;

    const/4 v5, 0x1

    const-string v7, "\u4e0b\u8f7d\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5\u7f51\u7edc\u8bbe\u7f6e\uff01"

    invoke-interface {v2, v5, v3, v7}, Lcom/lltskb/lltskb/utils/IDownloadListener;->OnStatus(IILjava/lang/String;)Z

    .line 74
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->this$0:Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-static {v2, v4}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->access$102(Lcom/lltskb/lltskb/utils/LLTHttpDownload;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v6, :cond_2

    .line 78
    :try_start_4
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v2

    .line 81
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 85
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/LLTHttpDownload$1;->val$l:Lcom/lltskb/lltskb/utils/IDownloadListener;

    if-eqz v0, :cond_3

    const/4 v1, 0x3

    const-string v2, "\u4e0b\u8f7d\u5b8c\u6210\uff01"

    .line 86
    invoke-interface {v0, v1, v3, v2}, Lcom/lltskb/lltskb/utils/IDownloadListener;->OnStatus(IILjava/lang/String;)Z

    :cond_3
    return-void

    :catchall_1
    move-exception v2

    move-object v4, v6

    :goto_3
    if-eqz v4, :cond_4

    .line 78
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    :catch_3
    move-exception v3

    .line 81
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 83
    :cond_4
    :goto_4
    goto :goto_6

    :goto_5
    throw v2

    :goto_6
    goto :goto_5
.end method
