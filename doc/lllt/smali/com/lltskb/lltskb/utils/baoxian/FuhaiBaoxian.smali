.class public Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;
.super Lcom/lltskb/lltskb/utils/baoxian/BaseBaoxian;
.source "FuhaiBaoxian.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "fuhaibx"

.field private static final ad:Ljava/lang/String; = "0"

.field private static final channel_id:Ljava/lang/String; = "18"

.field private static final production_code:Ljava/lang/String; = "PC0000000138"


# instance fields
.field private cardId:Ljava/lang/String;

.field private errMsg:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private ordersn:Ljava/lang/String;

.field private police_no:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/lltskb/lltskb/utils/baoxian/BaseBaoxian;-><init>()V

    return-void
.end method

.method private parseResult(Ljava/lang/String;)Z
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "parseResult result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fuhaibx"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 121
    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    .line 122
    instance-of v0, p1, Lorg/json/JSONObject;

    if-eqz v0, :cond_a

    .line 123
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    const-string v2, "status"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "200"

    .line 124
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    const-string v2, "policy_no"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->police_no:Ljava/lang/String;

    .line 126
    check-cast p1, Lorg/json/JSONObject;

    const-string v0, "ordersn"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->ordersn:Ljava/lang/String;

    .line 127
    iget-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->cardId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->police_no:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->ordersn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v2}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->addBaoxian(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const-string p1, "10001"

    .line 142
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "\u53c2\u6570\u4f20\u8f93\u7f3a\u5c11"

    .line 143
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    const-string p1, "10002"

    .line 144
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "\u88ab\u4fdd\u4eba\u4fe1\u606f\u4e0d\u80fd\u4e3a\u7a7a"

    .line 145
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    const-string p1, "10003"

    .line 146
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "\u8bf7\u586b\u5199\u6b63\u786e\u7684\u624b\u673a\u53f7"

    .line 147
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string p1, "10004"

    .line 148
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    const-string p1, "\u8bf7\u6b63\u786e\u586b\u5199\u8eab\u4efd\u8bc1"

    .line 149
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string p1, "10005"

    .line 150
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    const-string p1, "\u88ab\u4fdd\u4eba\u4e0d\u5728\u4fdd\u969c\u5e74\u9f84\u8303\u56f4\u5185"

    .line 151
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string p1, "10008"

    .line 152
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_6

    const-string p1, "\u60a8\u5df2\u7ecf\u6295\u4fdd\uff0c\u8bf7\u52ff\u91cd\u590d\u6295\u4fdd"

    .line 153
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_6
    const-string p1, "10009"

    .line 154
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    const-string p1, "\u4ea7\u54c1\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 155
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_7
    const-string p1, "10007"

    .line 156
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    const-string p1, "\u63d0\u4ea4\u5931\u8d25\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5"

    .line 157
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_8
    const-string p1, "10010"

    .line 158
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_9

    const-string p1, "\u6b64\u6b21\u8d60\u9669\u5df2\u7ecf\u5168\u90e8\u8d60\u9001\u5b8c\u6bd5"

    .line 159
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    goto :goto_0

    :cond_9
    const-string p1, "\u7cfb\u7edf\u5f02\u5e38"

    .line 161
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 165
    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :catch_1
    :cond_a
    :goto_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public getErrMsg()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getPoliceNo()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->police_no:Ljava/lang/String;

    return-object v0
.end method

.method public submitBXOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const-string v0, "fuhaibx"

    const-string v1, "submitBXOrder"

    .line 87
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->name:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->cardId:Ljava/lang/String;

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "channel_id=18&name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&mobile="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&cardId="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&product_code="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "PC0000000138"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&ad="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "0"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "http://fhm.bigins.cn/metlife/saveinfo"

    const-string p3, "application/x-www-form-urlencoded"

    .line 104
    invoke-static {p2, p1, p3}, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->httpPost(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 106
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "params="

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_0

    const-string p1, "submitBXOrder failed"

    .line 108
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "\u7f51\u7edc\u6545\u969c"

    .line 109
    iput-object p1, p0, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->errMsg:Ljava/lang/String;

    const/4 p1, 0x0

    return p1

    .line 113
    :cond_0
    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/utils/baoxian/FuhaiBaoxian;->parseResult(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
