.class public Lcom/lltskb/lltskb/utils/LLTUIUtils;
.super Ljava/lang/Object;
.source "LLTUIUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;,
        Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LLTUIUtils"

.field private static countryCode:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mComments:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mProgDlg:Landroid/support/v7/app/AppCompatDialog;

.field private static mRandCodeDlg:Landroid/support/v7/app/AlertDialog;

.field private static mToast:Landroid/widget/Toast;

.field private static mVers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static province:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final seatMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final seatNames:[Ljava/lang/String;

.field private static final seatType:[Ljava/lang/String;

.field public static final seats:[Ljava/lang/String;

.field private static trainTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static userAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 162
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    .line 163
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    const/16 v0, 0xd

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u5546\u52a1\u5ea7"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "\u7279\u7b49\u5ea7"

    aput-object v4, v1, v3

    const/4 v4, 0x2

    const-string v5, "\u4e00\u7b49\u5ea7"

    aput-object v5, v1, v4

    const/4 v5, 0x3

    const-string v6, "\u4e8c\u7b49\u5ea7"

    aput-object v6, v1, v5

    const/4 v6, 0x4

    const-string v7, "\u9ad8\u7ea7\u8f6f\u5367"

    aput-object v7, v1, v6

    const/4 v7, 0x5

    const-string v8, "\u8f6f\u5367"

    aput-object v8, v1, v7

    const/4 v8, 0x6

    const-string v9, "\u786c\u5367"

    aput-object v9, v1, v8

    const/4 v9, 0x7

    const-string v10, "\u8f6f\u5ea7"

    aput-object v10, v1, v9

    const/16 v10, 0x8

    const-string v11, "\u786c\u5ea7"

    aput-object v11, v1, v10

    const/16 v11, 0x9

    const-string v12, "\u65e0\u5ea7"

    aput-object v12, v1, v11

    const/16 v12, 0xa

    const-string v13, "\u52a8\u5367"

    aput-object v13, v1, v12

    const/16 v13, 0xb

    const-string v14, "\u9ad8\u7ea7\u52a8\u5367"

    aput-object v14, v1, v13

    const/16 v14, 0xc

    const-string v15, "\u5176\u5b83"

    aput-object v15, v1, v14

    .line 1108
    sput-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    new-array v1, v0, [Ljava/lang/String;

    const-string v15, "swz"

    aput-object v15, v1, v2

    const-string v15, "tz"

    aput-object v15, v1, v3

    const-string v15, "zy"

    aput-object v15, v1, v4

    const-string v15, "ze"

    aput-object v15, v1, v5

    const-string v15, "gr"

    aput-object v15, v1, v6

    const-string v15, "rw"

    aput-object v15, v1, v7

    const-string v15, "yw"

    aput-object v15, v1, v8

    const-string v15, "rz"

    aput-object v15, v1, v9

    const-string v15, "yz"

    aput-object v15, v1, v10

    const-string v15, "wz"

    aput-object v15, v1, v11

    const-string v15, "srrb"

    aput-object v15, v1, v12

    const-string v15, "yyrw"

    aput-object v15, v1, v13

    const-string v15, "qt"

    aput-object v15, v1, v14

    .line 1110
    sput-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatNames:[Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "9"

    aput-object v1, v0, v2

    const-string v1, "P"

    aput-object v1, v0, v3

    const-string v1, "M"

    aput-object v1, v0, v4

    const-string v1, "O"

    aput-object v1, v0, v5

    const-string v1, "6"

    aput-object v1, v0, v6

    const-string v1, "4"

    aput-object v1, v0, v7

    const-string v1, "3"

    aput-object v1, v0, v8

    const-string v1, "2"

    aput-object v1, v0, v9

    const-string v1, "1"

    aput-object v1, v0, v10

    const-string v1, "WZ"

    aput-object v1, v0, v11

    const-string v1, "F"

    aput-object v1, v0, v12

    const-string v1, "A"

    aput-object v1, v0, v13

    const-string v1, ""

    aput-object v1, v0, v14

    .line 1117
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatType:[Ljava/lang/String;

    .line 1119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatMaps:Ljava/util/Map;

    const-string v0, "Mozilla/5.0 (Linux; Android 8.0.0; SM-G9550 Build/R16NW; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.100 Mobile Safari/537.36"

    .line 1929
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->userAgent:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/support/v7/app/AlertDialog;
    .locals 1

    .line 79
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mRandCodeDlg:Landroid/support/v7/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v7/app/AlertDialog;)V
    .locals 0

    .line 79
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->keepDialogOpen(Landroid/support/v7/app/AlertDialog;)V

    return-void
.end method

.method static synthetic access$200(Landroid/support/v7/app/AlertDialog;)V
    .locals 0

    .line 79
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeDialog(Landroid/support/v7/app/AlertDialog;)V

    return-void
.end method

.method private static byte2HexFormatted([B)Ljava/lang/String;
    .locals 8

    .line 1835
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v1, p0

    const/4 v2, 0x2

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    .line 1836
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_3

    .line 1837
    aget-byte v3, p0, v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 1838
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1840
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-le v4, v2, :cond_1

    add-int/lit8 v6, v4, -0x2

    .line 1842
    invoke-virtual {v3, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1843
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1844
    array-length v3, p0

    sub-int/2addr v3, v5

    if-ge v1, v3, :cond_2

    const/16 v3, 0x3a

    .line 1845
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1847
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static cancelToast()V
    .locals 1

    .line 837
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    const/4 v0, 0x0

    .line 839
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    :cond_0
    return-void
.end method

.method private static closeDialog(Landroid/support/v7/app/AlertDialog;)V
    .locals 2

    .line 1739
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mShowing"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    .line 1740
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1741
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 1743
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V
    .locals 1

    .line 1237
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "input_method"

    .line 1239
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/inputmethod/InputMethodManager;

    .line 1240
    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public static closeSoftMethod(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 1

    .line 1071
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "input_method"

    .line 1074
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/inputmethod/InputMethodManager;

    .line 1075
    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public static dip2px(Landroid/content/Context;I)I
    .locals 0

    if-nez p0, :cond_0

    return p1

    .line 109
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    int-to-float p1, p1

    mul-float p1, p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method public static exit()V
    .locals 1

    .line 1862
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void
.end method

.method public static getCertificateSHA1Fingerprint(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    .line 1781
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1784
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x40

    .line 1790
    :try_start_0
    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    if-nez p0, :cond_0

    return-object v0

    .line 1797
    :cond_0
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const/4 v1, 0x0

    .line 1800
    aget-object p0, p0, v1

    invoke-virtual {p0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object p0

    .line 1802
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_1
    const-string p0, "X509"

    .line 1806
    invoke-static {p0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object p0
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1814
    :try_start_2
    invoke-virtual {p0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object p0

    check-cast p0, Ljava/security/cert/X509Certificate;
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v0, 0x0

    :try_start_3
    const-string v1, "SHA1"

    .line 1822
    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 1824
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p0

    .line 1826
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->byte2HexFormatted([B)Ljava/lang/String;

    move-result-object v0
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    .line 1828
    :goto_0
    invoke-virtual {p0}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    :goto_1
    return-object v0

    :catch_2
    move-exception p0

    .line 1816
    invoke-virtual {p0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    return-object v0

    :catch_3
    move-exception p0

    .line 1808
    invoke-virtual {p0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    return-object v0

    :catch_4
    move-exception p0

    .line 1792
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    return-object v0
.end method

.method public static getColor(Landroid/content/Context;I)I
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 138
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    return v0
.end method

.method public static getCountryCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1641
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initCountryCode()V

    .line 1642
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "CN"

    return-object p0
.end method

.method public static getCountryList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1635
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initCountryCode()V

    .line 1636
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 1637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public static getCountryNameByCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1648
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initCountryCode()V

    .line 1649
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1650
    sget-object v2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1651
    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getDefaultSHA1()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x3b

    new-array v0, v0, [C

    .line 1851
    fill-array-data v0, :array_0

    .line 1858
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1

    nop

    :array_0
    .array-data 2
        0x42s
        0x43s
        0x3as
        0x30s
        0x31s
        0x3as
        0x42s
        0x31s
        0x3as
        0x37s
        0x45s
        0x3as
        0x45s
        0x33s
        0x3as
        0x30s
        0x37s
        0x3as
        0x41s
        0x39s
        0x3as
        0x39s
        0x46s
        0x3as
        0x35s
        0x37s
        0x3as
        0x39s
        0x38s
        0x3as
        0x35s
        0x30s
        0x3as
        0x42s
        0x30s
        0x3as
        0x34s
        0x46s
        0x3as
        0x30s
        0x37s
        0x3as
        0x46s
        0x46s
        0x3as
        0x39s
        0x42s
        0x3as
        0x46s
        0x42s
        0x3as
        0x37s
        0x32s
        0x3as
        0x46s
        0x44s
        0x3as
        0x42s
        0x46s
    .end array-data
.end method

.method private static getDefaultUserAgent()Ljava/lang/String;
    .locals 7

    .line 1945
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v1, "http.agent"

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    .line 1947
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1949
    :catch_0
    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1952
    :cond_0
    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1954
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1955
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_3

    .line 1956
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1f

    if-le v5, v6, :cond_2

    const/16 v6, 0x7f

    if-lt v5, v6, :cond_1

    goto :goto_2

    .line 1960
    :cond_1
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 1958
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v3

    const-string v5, "\\u%04x"

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1963
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHoliday(Landroid/content/Context;III)Ljava/lang/String;
    .locals 8

    .line 1316
    new-instance v7, Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x50

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;-><init>(Landroid/content/Context;IIJ)V

    const/4 p0, 0x1

    .line 1317
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, v5

    invoke-virtual/range {v0 .. v6}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setData(IIILjava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 1318
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHoliday()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 1995
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "1"

    .line 1998
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    .line 2001
    :cond_1
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    return v0

    .line 2004
    :cond_2
    invoke-virtual {p2, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, -0x1

    return p0

    :cond_3
    return v0
.end method

.method public static getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 1218
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const-string v0, ","

    .line 1221
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v1

    .line 1225
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1226
    array-length v2, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_3

    aget-object v5, p0, v4

    const/16 v6, 0x2d

    .line 1227
    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-ltz v6, :cond_2

    .line 1229
    invoke-virtual {v5, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1232
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getProviceNameByCode(I)Ljava/lang/String;
    .locals 3

    .line 1299
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initProvince()V

    .line 1300
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1301
    sget-object v2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p0, :cond_0

    return-object v1

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getProvinceCode(Ljava/lang/String;)I
    .locals 1

    .line 1292
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initProvince()V

    .line 1293
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    if-eqz p0, :cond_0

    .line 1294
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0

    :cond_0
    const/16 p0, 0xb

    return p0
.end method

.method public static getProvinceList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1284
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initProvince()V

    .line 1285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1286
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 1287
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public static getScreenHeight(Landroid/content/Context;)I
    .locals 1

    const-string v0, "window"

    .line 98
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/WindowManager;

    .line 99
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 100
    invoke-virtual {p0}, Landroid/view/Display;->getHeight()I

    move-result p0

    return p0
.end method

.method public static getScreenWidth(Landroid/content/Context;)I
    .locals 1

    const-string v0, "window"

    .line 90
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/WindowManager;

    .line 91
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 92
    invoke-virtual {p0}, Landroid/view/Display;->getWidth()I

    move-result p0

    return p0
.end method

.method public static getSeatName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1131
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatNames:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1132
    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1133
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getSeatNameFromType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1183
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatType:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1184
    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1185
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getSeatType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1122
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatMaps:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1123
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1124
    sget-object v2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatMaps:Ljava/util/Map;

    aget-object v1, v1, v0

    sget-object v3, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatNames:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1127
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatMaps:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static getSeatTypeForHoubu(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1175
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1176
    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1177
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatType:[Ljava/lang/String;

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1147
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatNames:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1148
    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1149
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seatType:[Ljava/lang/String;

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getSignature(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    if-nez p0, :cond_0

    return-object v0

    .line 1757
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_1

    return-object v0

    .line 1760
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x40

    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 1763
    :cond_2
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-nez p0, :cond_3

    return-object v0

    .line 1766
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1767
    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    aget-object v4, p0, v3

    .line 1768
    invoke-virtual {v4}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1771
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 1773
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    return-object v0
.end method

.method public static getTrainTypeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1094
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initTrainTypeMap()V

    .line 1095
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    return-object v0
.end method

.method public static getTrainTypeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1099
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->initTrainTypeMap()V

    .line 1100
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1101
    sget-object v2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1102
    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 1

    .line 1967
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->userAgent:Ljava/lang/String;

    return-object v0
.end method

.method public static getVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string v0, "2.7.0.150115"

    if-nez p0, :cond_0

    return-object v0

    .line 122
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 123
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    .line 122
    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    .line 125
    :cond_1
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 127
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const-string p0, "1.9.6.140210"

    return-object p0
.end method

.method public static getVersionCode(Landroid/content/Context;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 148
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    if-nez p0, :cond_1

    const p0, 0x24a63

    return p0

    .line 150
    :cond_1
    iget p0, p0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    .line 153
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    return v0
.end method

.method public static hideBgTaskNotify(Landroid/content/Context;)V
    .locals 2

    .line 1870
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/service/SearchService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1871
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public static hideLoadingDialog()V
    .locals 2

    const/4 v0, 0x0

    .line 993
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 994
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 998
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    throw v1

    :catch_0
    :cond_0
    :goto_0
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private static initCountryCode()V
    .locals 3

    .line 1429
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    if-eqz v0, :cond_0

    return-void

    .line 1431
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    .line 1432
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CN"

    const-string v2, "\u4e2d\u56fdCHINA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "US"

    const-string v2, "\u7f8e\u56fdUNITEDSTATES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1434
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AF"

    const-string v2, "\u963f\u5bcc\u6c57AFGHANISTANA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AL"

    const-string v2, "\u963f\u5c14\u5df4\u5c3c\u4e9aALBANIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1436
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DZ"

    const-string v2, "\u963f\u5c14\u53ca\u5229\u4e9aALGERIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1437
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AD"

    const-string v2, "\u5b89\u9053\u5c14ANDORRA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1438
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AO"

    const-string v2, "\u5b89\u54e5\u62c9ANGOLA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1439
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AG"

    const-string v2, "\u5b89\u63d0\u74dc\u548c\u5df4\u5e03\u8fbeANTIGUABARBUDA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1440
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AE"

    const-string v2, "\u963f\u62c9\u4f2f\u8054\u5408\u914b\u957f\u56fdARAB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1441
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AR"

    const-string v2, "\u963f\u6839\u5ef7ARGENTINA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1442
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AM"

    const-string v2, "\u4e9a\u7f8e\u5c3c\u4e9aARMENIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1443
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AW"

    const-string v2, "\u963f\u9c81\u5df4ARUBA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1444
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AU"

    const-string v2, "\u6fb3\u5927\u5229\u4e9aAUSTRALIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1445
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AT"

    const-string v2, "\u5965\u5730\u5229AUSTRIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1446
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "AZ"

    const-string v2, "\u963f\u585e\u62dc\u7586\u5171\u548c\u56fdAZERBAIJAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BS"

    const-string v2, "\u5df4\u54c8\u9a6cBAHAMAS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BH"

    const-string v2, "\u5df4\u6797BAHRAIN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1449
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BD"

    const-string v2, "\u5b5f\u52a0\u62c9\u56fdBANGLADESH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BB"

    const-string v2, "\u5df4\u5df4\u591a\u65afBARBADOS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BY"

    const-string v2, "\u767d\u4fc4\u7f57\u65afBELARUS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BE"

    const-string v2, "\u6bd4\u5229\u65f6BELGIUM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BZ"

    const-string v2, "\u4f2f\u91cc\u5179BELIZE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v2, "\u4f2f\u5229\u5179BELIZE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BJ"

    const-string v2, "\u8d1d\u5b81BENIN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BT"

    const-string v2, "\u4e0d\u4e39BHUTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BO"

    const-string v2, "\u73bb\u5229\u7ef4\u4e9aBOLIVIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1458
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BA"

    const-string v2, "\u6ce2\u65af\u5c3c\u4e9a\u548c\u9ed1\u585e\u54e5\u7ef4\u90a3BOSNIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1459
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BW"

    const-string v2, "\u535a\u8328\u74e6\u7eb3BOTSWANA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1460
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BR"

    const-string v2, "\u5df4\u897fBRAZIL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1461
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BG"

    const-string v2, "\u4fdd\u52a0\u5229\u4e9aBULGARIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1462
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BF"

    const-string v2, "\u5e03\u57fa\u7eb3\u6cd5\u7d22BURKINAFASO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BI"

    const-string v2, "\u5e03\u9686\u8feaBURUNDI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BN"

    const-string v2, "\u6587\u83b1BruneiDarussalam"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1465
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KH"

    const-string v2, "\u67ec\u57d4\u5be8CAMBODIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1466
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CM"

    const-string v2, "\u5580\u9ea6\u9686CAMEROON"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1467
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CA"

    const-string v2, "\u52a0\u62ff\u5927CANADA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1468
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KY"

    const-string v2, "\u4f5b\u5f97\u89d2CAPEVERDE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1469
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TD"

    const-string v2, "\u4e4d\u5f97CHAD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1470
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CL"

    const-string v2, "\u667a\u5229CHILE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1471
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CO"

    const-string v2, "\u54e5\u4f26\u6bd4\u4e9aCOLOMBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1472
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v2, "\u54e5\u4f26\u6bd4\u4e9aCOLUMBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1473
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KM"

    const-string v2, "\u79d1\u6469\u7f57COMOROS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1474
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CG"

    const-string v2, "\u521a\u679c\uff08\u5e03\uff09CONGO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1475
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CK"

    const-string v2, "\u5e93\u514b\u7fa4\u5c9bCOOKISLANDS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1476
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CI"

    const-string v2, "\u79d1\u7279\u8fea\u74e6COTEDLVOIRE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1477
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "HR"

    const-string v2, "\u514b\u7f57\u5730\u4e9aCROATIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1478
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CU"

    const-string v2, "\u53e4\u5df4\u5171\u548c\u56fdCUBA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1479
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CY"

    const-string v2, "\u585e\u6d66\u8def\u65afCYPRUS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1480
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CZ"

    const-string v2, "\u6377\u514b\u5171\u548c\u56fdCZECHREPUBLIC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1481
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CF"

    const-string v2, "\u4e2d\u975e\u5171\u548c\u56fdCentral Africa Republic"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1482
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CRC"

    const-string v2, "\u54e5\u65af\u8fbe\u9ece\u52a0CostaRica"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1483
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "YD"

    const-string v2, "\u4e5f\u95e8\u6c11\u4e3b\u4eba\u6c11\u5171\u548c\u56fdDEMOCRATICYEMEN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1484
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DK"

    const-string v2, "\u4e39\u9ea6DENMARK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1485
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DJ"

    const-string v2, "\u5409\u5e03\u63d0DJIBOUTI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1486
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DM"

    const-string v2, "\u591a\u7c73\u5c3c\u514bDOMINICA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1487
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DO"

    const-string v2, "\u591a\u7c73\u5c3c\u52a0DOMINICAN REPUBLIC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1488
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "EC"

    const-string v2, "\u5384\u74dc\u591a\u5c14ECUADOR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1489
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "EG"

    const-string v2, "\u57c3\u53caEGYPT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1490
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "EV"

    const-string v2, "\u8428\u5c14\u74e6\u591aEL SALVADOR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1491
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GQ"

    const-string v2, "\u8d64\u9053\u51e0\u5185\u4e9aEQUATORIALGUINEA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1492
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ER"

    const-string v2, "\u5384\u7acb\u7279\u91cc\u4e9aERITREA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1493
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "EE"

    const-string v2, "\u7231\u6c99\u5c3c\u4e9aESTONIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1494
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ET"

    const-string v2, "\u57c3\u585e\u4fc4\u6bd4\u4e9aETHIOPIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1495
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "FJ"

    const-string v2, "\u6590\u6d4eFIJI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1496
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "FI"

    const-string v2, "\u82ac\u5170FINLAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1497
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "FR"

    const-string v2, "\u6cd5\u56fdFRANCE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1498
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GA"

    const-string v2, "\u52a0\u84ecGABON"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1499
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GM"

    const-string v2, "\u5188\u6bd4\u4e9aGAMBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1500
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CE"

    const-string v2, "\u683c\u9c81\u5409\u4e9aGEORGIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1501
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DE"

    const-string v2, "\u5fb7\u56fdGERMANY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GH"

    const-string v2, "\u52a0\u7eb3GHANA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1503
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GR"

    const-string v2, "\u5e0c\u814aGREECE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1504
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GL"

    const-string v2, "\u683c\u6797\u7eb3\u8fbeGRENADA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1505
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GN"

    const-string v2, "\u51e0\u5185\u4e9aGUINEA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1506
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GW"

    const-string v2, "\u51e0\u5185\u4e9a\u6bd4\u7ecdGUINEA-BISSAU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1507
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v2, "\u51e0\u5185\u4e9a\u6bd4\u7ecdGUINEABISSAU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GY"

    const-string v2, "\u572d\u4e9a\u90a3GUYANA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1509
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GT"

    const-string v2, "\u5371\u5730\u9a6c\u62c9Guatemala"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1510
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "HT"

    const-string v2, "\u6d77\u5730HAITI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1511
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NL"

    const-string v2, "\u8377\u5170HOLLAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1512
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "HN"

    const-string v2, "\u6d2a\u90fd\u62c9\u65afHONDURAS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1513
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "HU"

    const-string v2, "\u5308\u7259\u5229HUNGARY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1514
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IS"

    const-string v2, "\u51b0\u5c9bICELAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1515
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IN"

    const-string v2, "\u5370\u5ea6INDIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ID"

    const-string v2, "\u5370\u5ea6\u5c3c\u897f\u4e9aINDONESIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IR"

    const-string v2, "\u4f0a\u6717IRAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IQ"

    const-string v2, "\u4f0a\u62c9\u514bIRAQ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IE"

    const-string v2, "\u7231\u5c14\u5170IRELAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IL"

    const-string v2, "\u4ee5\u8272\u5217ISRAEL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "IT"

    const-string v2, "\u610f\u5927\u5229ITALY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "JM"

    const-string v2, "\u7259\u4e70\u52a0JAMAICA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1523
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "JP"

    const-string v2, "\u65e5\u672cJAPAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1524
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "JO"

    const-string v2, "\u7ea6\u65e6JORDAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1525
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KZ"

    const-string v2, "\u54c8\u8428\u514b\u65af\u5766KAZAKHSTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KE"

    const-string v2, "\u80af\u5c3c\u4e9aKENYA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KG"

    const-string v2, "\u5409\u5c14\u5409\u65af\u5171\u548c\u56fdKIRGIZSTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KI"

    const-string v2, "\u57fa\u91cc\u5df4\u65afKIRIBATI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1529
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KR"

    const-string v2, "\u97e9\u56fdKOREA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1530
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KW"

    const-string v2, "\u79d1\u5a01\u7279KUWAIT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1531
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "DPR"

    const-string v2, "\u671d\u9c9cKorea"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1532
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LA"

    const-string v2, "\u8001\u631dLAOS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1533
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LV"

    const-string v2, "\u62c9\u8131\u7ef4\u4e9aLATVIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1534
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LB"

    const-string v2, "\u9ece\u5df4\u5ae9LEBANON"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LS"

    const-string v2, "\u83b1\u7d22\u6258LESOTHO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LR"

    const-string v2, "\u5229\u6bd4\u91cc\u4e9aLIBERIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LY"

    const-string v2, "\u5229\u6bd4\u4e9aLIBYA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1538
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LI"

    const-string v2, "\u5217\u652f\u6566\u58eb\u767bLIECHTENSTEIN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1539
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LT"

    const-string v2, "\u7acb\u9676\u5b9bLITHUANIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1540
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LU"

    const-string v2, "\u5362\u68ee\u5821LUXEMBOURG"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1541
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MK"

    const-string v2, "\u9a6c\u5176\u987fMACEDONIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1542
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MG"

    const-string v2, "\u9a6c\u8fbe\u52a0\u65af\u52a0MADAGASCAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1543
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MW"

    const-string v2, "\u9a6c\u62c9\u7ef4MALAWI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1544
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MY"

    const-string v2, "\u9a6c\u6765\u897f\u4e9aMALAYSIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1545
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MV"

    const-string v2, "\u9a6c\u5c14\u4ee3\u592bMALDIVES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1546
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ML"

    const-string v2, "\u9a6c\u91ccMALI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1547
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MT"

    const-string v2, "\u9a6c\u8033\u4ed6MALTA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1548
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MH"

    const-string v2, "\u9a6c\u7ecd\u5c14\u7fa4\u5c9bMARSHALL ISLANDS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1549
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MR"

    const-string v2, "\u6bdb\u91cc\u5854\u5c3c\u4e9aMAURITANIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1550
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MU"

    const-string v2, "\u6bdb\u91cc\u6c42\u65afMAURITIUS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1551
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MX"

    const-string v2, "\u58a8\u897f\u54e5MEXICO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1552
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "FM"

    const-string v2, "\u5bc6\u514b\u7f57\u5c3c\u897f\u4e9a\u8054\u90a6MICRONESIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1553
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MD"

    const-string v2, "\u6469\u5c14\u591a\u74e6MOLDOVA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1554
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MC"

    const-string v2, "\u6469\u7eb3\u54e5MONACO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1555
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MN"

    const-string v2, "\u8499\u53e4MONGOLIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1556
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ME"

    const-string v2, "\u9ed1\u5c71MONTENEGRO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1557
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MA"

    const-string v2, "\u6469\u6d1b\u54e5MOROCCO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1558
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MZ"

    const-string v2, "\u83ab\u6851\u6bd4\u514bMOZAMBIQUE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1559
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "MM"

    const-string v2, "\u7f05\u7538MYANMAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1560
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NA"

    const-string v2, "\u7eb3\u7c73\u6bd4\u4e9aNAMIBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1561
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NR"

    const-string v2, "\u7459\u9c81NAURU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1562
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NP"

    const-string v2, "\u5c3c\u6cca\u5c14NEPAL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1563
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NZ"

    const-string v2, "\u65b0\u897f\u5170NEWZEALAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1564
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NI"

    const-string v2, "\u5c3c\u52a0\u62c9\u74dcNICARAGUA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1565
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NE"

    const-string v2, "\u5c3c\u65e5\u5c14NIGER"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1566
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NG"

    const-string v2, "\u5c3c\u65e5\u5229\u4e9aNIGERIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1567
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "NO"

    const-string v2, "\u632a\u5a01NORWAY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1568
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "OM"

    const-string v2, "\u963f\u66fcOMAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1569
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PK"

    const-string v2, "\u5df4\u57fa\u65af\u5766PAKISTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1570
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PW"

    const-string v2, "\u5e15\u52b3PALAU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1571
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "BL"

    const-string v2, "\u5df4\u52d2\u65af\u5766PALESTINE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1572
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PA"

    const-string v2, "\u5df4\u62ff\u9a6cPANAMA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1573
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PG"

    const-string v2, "\u5df4\u5e03\u4e9a\u65b0\u51e0\u5185\u4e9aPAPUANEWGUINEA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1574
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PY"

    const-string v2, "\u5df4\u62c9\u572dPARAGUAY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1575
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PE"

    const-string v2, "\u79d8\u9c81PERU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1576
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PH"

    const-string v2, "\u83f2\u5f8b\u5bbePHILIPPINES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1577
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PL"

    const-string v2, "\u6ce2\u5170POLAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1578
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PT"

    const-string v2, "\u8461\u8404\u7259PORTUGAL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1579
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "PR"

    const-string v2, "\u6ce2\u591a\u9ece\u5404PUERTO RICO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1580
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "QA"

    const-string v2, "\u5361\u5854\u5c14QATAR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1581
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "RO"

    const-string v2, "\u7f57\u9a6c\u5c3c\u4e9aROMANIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1582
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "RU"

    const-string v2, "\u4fc4\u7f57\u65afRUSSIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1583
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "RW"

    const-string v2, "\u5362\u65fa\u8fbeRWANDA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1584
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "KNA"

    const-string v2, "\u5723\u57fa\u8328\u548c\u5c3c\u7ef4\u65afSAINT KITTS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1585
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "VC"

    const-string v2, "\u5723\u6587\u68ee\u7279\u548c\u683c\u6797\u7eb3\u4e01\u65afSAINT VINCENT AND THE GRENADIN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1586
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LC"

    const-string v2, "\u5723\u5362\u897f\u4e9aSAINTLUCIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1587
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "WS"

    const-string v2, "\u7f8e\u5c5e\u8428\u6469\u4e9aSAMOA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1588
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SM"

    const-string v2, "\u5723\u9a6c\u529b\u8bfaSANMARINO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1589
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ST"

    const-string v2, "\u5723\u591a\u7f8e\u548c\u666e\u6797\u897f\u6bd4SAOTOMEPRINCIPE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1590
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SA"

    const-string v2, "\u6c99\u7279\u963f\u62c9\u4f2fSAUDIARABIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1591
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SN"

    const-string v2, "\u585e\u5185\u52a0\u5c14SENEGAL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1592
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CS"

    const-string v2, "\u585e\u5c14\u7ef4\u4e9aSERBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1593
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SC"

    const-string v2, "\u585e\u820c\u5c14SEYCHELLES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1594
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SL"

    const-string v2, "\u585e\u62c9\u5229\u6602SIERRALEONE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1595
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SG"

    const-string v2, "\u65b0\u52a0\u5761SINGAPORE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1596
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SK"

    const-string v2, "\u65af\u6d1b\u4f10\u514bSLOVAKIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1597
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v2, "\u65af\u6d1b\u4f10\u514b\u5171\u548c\u56fdSLOVAKREPUBLIC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1598
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SI"

    const-string v2, "\u65af\u6d1b\u6587\u5c3c\u4e9aSLOVENIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1599
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SB"

    const-string v2, "\u6240\u7f57\u95e8\u7fa4\u5c9bSOLOMON ISLANDS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1600
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SO"

    const-string v2, "\u7d22\u9a6c\u91ccSOMALI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1601
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v2, "\u7d22\u9a6c\u91ccSOMALIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1602
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ZA"

    const-string v2, "\u5357\u975eSOUTHAFRICA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1603
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ES"

    const-string v2, "\u897f\u73ed\u7259SPAIN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1604
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "LK"

    const-string v2, "\u65af\u91cc\u5170\u5361SRILANKA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1605
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SD"

    const-string v2, "\u82cf\u4e39SUDAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1606
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SR"

    const-string v2, "\u82cf\u91cc\u5357SURINAM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1607
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SZ"

    const-string v2, "\u65af\u5a01\u58eb\u5170SWAZILAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1608
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SE"

    const-string v2, "\u745e\u5178SWEDEN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1609
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "CH"

    const-string v2, "\u745e\u58ebSWITZERLAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1610
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "SY"

    const-string v2, "\u53d9\u5229\u4e9aSYRIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1611
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TJ"

    const-string v2, "\u5854\u5409\u514b\u65af\u5766TAJIKISTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1612
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TZ"

    const-string v2, "\u5766\u6851\u5c3c\u4e9aTANZANIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1613
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TH"

    const-string v2, "\u6cf0\u56fdTHAILAND"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1614
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "UGA"

    const-string v2, "\u4e4c\u5e72\u8fbeTHE REPUBLIC OF UGANDA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1615
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TL"

    const-string v2, "\u4e1c\u5e1d\u6c76TIMOR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1616
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TG"

    const-string v2, "\u591a\u54e5TOGO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1617
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TO"

    const-string v2, "\u6c64\u52a0TONGA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1618
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TT"

    const-string v2, "\u7279\u7acb\u5c3c\u8fbe\u548c\u591a\u5df4\u54e5TRINIDADANDTOBAGO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1619
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TN"

    const-string v2, "\u7a81\u5c3c\u65afTUNISIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1620
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TR"

    const-string v2, "\u571f\u8033\u5176TURKEY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1621
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "TM"

    const-string v2, "\u571f\u5e93\u66fc\u65af\u5766TURKMENISTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "UKR"

    const-string v2, "\u4e4c\u514b\u5170UKRAINE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1623
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "GB"

    const-string v2, "\u82f1\u56fdUNITED KINGDOM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "UZB"

    const-string v2, "\u4e4c\u5179\u522b\u514b\u65af\u5766UZBEKISTAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "UY"

    const-string v2, "\u4e4c\u62c9\u572dUruguay"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1626
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "VU"

    const-string v2, "\u74e6\u52aa\u963f\u56feVANUATU"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1627
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "VA"

    const-string v2, "\u68b5\u8482\u5188VATICAN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1628
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "VIE"

    const-string v2, "\u8d8a\u5357VIETNAM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1629
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "VE"

    const-string v2, "\u59d4\u5185\u745e\u62c9Venezuela"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1630
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ZM"

    const-string v2, "\u8d5e\u6bd4\u4e9aZAMBIA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1631
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->countryCode:Ljava/util/Map;

    const-string v1, "ZW"

    const-string v2, "\u6d25\u5df4\u5e03\u97e6ZIMBABWE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static initProvince()V
    .locals 3

    .line 1246
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    if-eqz v0, :cond_0

    return-void

    .line 1247
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    .line 1248
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5317\u4eac"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1249
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5929\u6d25"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1250
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6cb3\u5317"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1251
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5c71\u897f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1252
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5185\u8499\u53e4"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1253
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u8fbd\u5b81"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1254
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5409\u6797"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1255
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u9ed1\u9f99\u6c5f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1256
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u4e0a\u6d77"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1257
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6c5f\u82cf"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1258
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6d59\u6c5f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5b89\u5fbd"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1261
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u798f\u5efa"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1262
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x24

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6c5f\u897f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1263
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5c71\u4e1c"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1264
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6cb3\u5357"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1265
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x2a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6e56\u5317"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1266
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x2b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6e56\u5357"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1267
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x2c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5e7f\u4e1c"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1268
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x2d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5e7f\u897f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1269
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u6d77\u5357"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1270
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u91cd\u5e86"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1271
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x33

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u56db\u5ddd"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1272
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x34

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u8d35\u5dde"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1273
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x35

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u4e91\u5357"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1274
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x36

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u897f\u85cf"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1275
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x3d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u9655\u897f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x3e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u7518\u8083"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x3f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u9752\u6d77"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u5b81\u590f"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->province:Ljava/util/Map;

    const/16 v1, 0x41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u65b0\u7586"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static initTrainTypeMap()V
    .locals 3

    .line 1081
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1082
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    .line 1083
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "\u5168\u90e8"

    const-string v2, "QB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "\u9ad8\u94c1/\u57ce\u9645"

    const-string v2, "G"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "\u52a8\u8f66"

    const-string v2, "D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "Z\u5b57\u5934"

    const-string v2, "Z"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "T\u5b57\u5934"

    const-string v2, "T"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1088
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "K\u5b57\u5934"

    const-string v2, "K"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1089
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->trainTypeMap:Ljava/util/Map;

    const-string v1, "\u5176\u4ed6"

    const-string v2, "QT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static is12306Crack(Ljava/lang/String;)Z
    .locals 1

    .line 1312
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "\u975e\u6cd5\u8bf7\u6c42"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\u5378\u8f7d"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1

    .line 1322
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 v0, 0x0

    .line 1323
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p0

    .line 1325
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-lez p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isLandscape(Landroid/content/Context;)Z
    .locals 1

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    iget p0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isTabletModel(Landroid/content/Context;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isTopActivity(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .line 1198
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1202
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/ActivityManager;

    .line 1205
    invoke-virtual {p0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object p0

    .line 1206
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 1208
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1209
    iget v2, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v3, 0x64

    if-ne v2, v3, :cond_2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 1210
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x1

    return p0

    :cond_3
    return v1
.end method

.method public static isValidContext(Landroid/content/Context;)Z
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 877
    :cond_0
    instance-of v0, p0, Landroid/app/Activity;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 880
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result p0

    xor-int/2addr p0, v1

    return p0

    :cond_1
    return v1
.end method

.method private static keepDialogOpen(Landroid/support/v7/app/AlertDialog;)V
    .locals 2

    .line 1728
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mShowing"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    .line 1729
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 v1, 0x0

    .line 1730
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 1732
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method static synthetic lambda$showAlertDialog$0(Landroid/support/v7/app/AppCompatDialog;Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 0

    .line 914
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    if-eqz p1, :cond_0

    .line 916
    invoke-interface {p1, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$showConfirmDialog$1(Landroid/support/v7/app/AppCompatDialog;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;Landroid/view/View;)V
    .locals 0

    .line 1048
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    if-eqz p1, :cond_0

    .line 1050
    invoke-interface {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;->onYes()V

    :cond_0
    return-void
.end method

.method static synthetic lambda$showConfirmDialog$2(Landroid/support/v7/app/AppCompatDialog;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;Landroid/view/View;)V
    .locals 0

    .line 1056
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    if-eqz p1, :cond_0

    .line 1058
    invoke-interface {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;->onNo()V

    :cond_0
    return-void
.end method

.method public static px2dip(Landroid/content/Context;F)I
    .locals 0

    .line 114
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method public static setDialogFullscreen(Landroid/app/Dialog;)V
    .locals 3

    if-eqz p0, :cond_0

    .line 1972
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1974
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x11

    .line 1976
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, -0x1

    .line 1977
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1978
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1980
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 1981
    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public static setLoadingDialogMsg(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 1

    .line 931
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result p0

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    if-nez p0, :cond_0

    goto :goto_0

    .line 933
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->isShowing()Z

    move-result p0

    if-nez p0, :cond_1

    return-object v0

    .line 935
    :cond_1
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f09017d

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_2

    .line 937
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 938
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    .line 939
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 941
    :cond_2
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    return-object p0

    :cond_3
    :goto_0
    return-object v0
.end method

.method public static setUserAgent()V
    .locals 2

    .line 1931
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v0

    .line 1932
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1933
    new-instance v0, Landroid/webkit/WebView;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 1934
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 1935
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    .line 1939
    sput-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->userAgent:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public static showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;
    .locals 2

    .line 868
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 870
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 871
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 872
    invoke-static {p0, p1, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p0

    return-object p0
.end method

.method public static showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;
    .locals 1

    .line 886
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 889
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2, p3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p0

    return-object p0
.end method

.method public static showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;
    .locals 2

    .line 894
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 898
    :cond_0
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    const v1, 0x7f0e0003

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    const p0, 0x7f0b001e

    .line 899
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    const p0, 0x7f0902f6

    .line 900
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_1

    .line 902
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 p0, 0x0

    .line 904
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    const p0, 0x7f090268

    .line 906
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_2

    .line 908
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const p0, 0x7f090061

    .line 911
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 913
    new-instance p1, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$oif_Y4UdqcPZjKkaLSBKo9YXjFk;

    invoke-direct {p1, v0, p3}, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$oif_Y4UdqcPZjKkaLSBKo9YXjFk;-><init>(Landroid/support/v7/app/AppCompatDialog;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 922
    :cond_3
    :try_start_0
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method

.method public static showBgTaskNotify(Landroid/content/Context;)V
    .locals 2

    .line 1901
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/service/SearchService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V
    .locals 1

    .line 1009
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1012
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1013
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 1012
    invoke-static {p0, p1, p2, p3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method public static showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V
    .locals 2

    .line 1017
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1020
    :cond_0
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    const v1, 0x7f0e0003

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    const p0, 0x7f0b001f

    .line 1021
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    const/4 p0, 0x1

    .line 1022
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    const p0, 0x7f0902f6

    .line 1024
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_1

    .line 1026
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const p0, 0x7f090268

    .line 1029
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_2

    .line 1031
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1032
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const p1, 0x800013

    .line 1033
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    :cond_2
    const p0, 0x7f090309

    .line 1046
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 1047
    new-instance p1, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$tQPzdqjOyo0AJW7-Ev2yXVIhnps;

    invoke-direct {p1, v0, p3}, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$tQPzdqjOyo0AJW7-Ev2yXVIhnps;-><init>(Landroid/support/v7/app/AppCompatDialog;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p0, 0x7f0902ac

    .line 1054
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 1055
    new-instance p1, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$mnvOCdi4ydNz7VvBB9T2AuvHcJc;

    invoke-direct {p1, v0, p3}, Lcom/lltskb/lltskb/utils/-$$Lambda$LLTUIUtils$mnvOCdi4ydNz7VvBB9T2AuvHcJc;-><init>(Landroid/support/v7/app/AppCompatDialog;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1063
    :try_start_0
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 1065
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;
    .locals 1

    .line 983
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 986
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p0

    return-object p0
.end method

.method public static showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;
    .locals 2

    .line 950
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    .line 952
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 953
    new-instance p2, Landroid/support/v7/app/AppCompatDialog;

    const v1, 0x7f0e0003

    invoke-direct {p2, p0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    sput-object p2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 954
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    const p2, 0x7f0b0079

    invoke-virtual {p0, p2}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    .line 955
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    const p2, 0x7f09017d

    invoke-virtual {p0, p2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 956
    sget-object p2, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p2, v0}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz p0, :cond_1

    .line 958
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 959
    :cond_1
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0, p3}, Landroid/support/v7/app/AppCompatDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 960
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    const/4 p1, 0x0

    if-eqz p3, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    .line 961
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDialog;->setCanceledOnTouchOutside(Z)V

    .line 963
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 965
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_3

    const p2, 0x3f19999a    # 0.6f

    .line 967
    iput p2, p1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 969
    invoke-virtual {p0, p1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 974
    :cond_3
    :try_start_0
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 976
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 979
    :goto_1
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mProgDlg:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method public static showNewVersionComments(Landroid/content/Context;Z)Z
    .locals 12

    const-string v0, "LLTUIUtils"

    const-string v1, "showNewVersionComments"

    .line 171
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 175
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "<small><font color=\"#4a4afa\">\u2606&nbsp;"

    const-string v3, "</font><br/></small>"

    .line 178
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "1.9.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 179
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u67e5\u8be2\u7ed3\u679c\u754c\u9762\u4f7f\u7528\u66f4\u6e05\u6670\u7684\u989c\u8272\u663e\u793a\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u6539\u9884\u5b9a\u7ba1\u7406\u754c\u9762\uff0c\u663e\u793a\u9875\u9762\u8fdb\u5ea6\u6761\u3002"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u4fdd\u5b58\u67e5\u8be2\u65e5\u671f\u529f\u80fd\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u63d0\u9ad8\u542f\u52a8\u754c\u9762\u7684\u901f\u5ea6\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u70b9\u51fb\u5237\u65b0\u9a8c\u8bc1\u7801\u529f\u80fd\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4fee\u6539\u9a8c\u8bc1\u7801\u663e\u793a\u5bf9\u8bdd\u6846\uff0c\u652f\u6301\u8f6f\u952e\u76d8\u5f39\u51fa\u65f6\uff0c\u5bf9\u8bdd\u6846\u5411\u4e0a\u79fb\u52a8\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u6b63\u665a\u70b9\u4f7f\u7528\u65b0\u63a5\u53e3\u67e5\u8be2,\u901f\u5ea6\u66f4\u5feb\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u9884\u5b9a\u7ba1\u7406\u4e2d\uff0c\u6309back\u952e\u56de\u9000\u6d4f\u89c8\u5668\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u65b0\u7248\u672c\u66f4\u65b0\u7279\u6027\u63d0\u793a\u3002"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 187
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "1.9.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 188
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f59\u7968\u67e5\u8be2\u663e\u793a\u5177\u4f53\u4f59\u7968\u6570\u91cf\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u6781\u7aef\u60c5\u51b5\u4e0b\u5f02\u5e38\u9000\u51fa\u95ee\u9898\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 191
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "1.9.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 192
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f7f\u7528\u52a8\u6001\u8c03\u5ea6\u6570\u636e\uff0c\u63d0\u9ad8\u67e5\u8be2\u6570\u636e\u51c6\u786e\u5ea6\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u67e5\u8be2\u7ed3\u679c\u754c\u9762\u5fae\u8c03\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 194
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "1.9.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u4e58\u8f66\u65e5\u671f\u8bbe\u7f6e\u9009\u9879\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u6539\u5b57\u4f53\u8bbe\u7f6e\u5bf9\u8bdd\u6846\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u6539\u542f\u52a8\u754c\u9762\uff0c\u589e\u52a0\u7248\u672c\u53f7\u663e\u793a\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u6539\u8ba2\u7968\u754c\u9762\uff0c\u9ed8\u8ba4\u4e0d\u6253\u5f00\u7f51\u9875\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u67e5\u8be2\u7684\u663e\u793a\u95ee\u9898\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u6539\u4e2d\u8f6c\u67e5\u8be2\u63d0\u793a\u4fe1\u606f\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "1.9.10"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 202
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u5217\u8f66\u8fd0\u884c\u89c4\u5f8b\u8ba1\u7b97\u7684\u4e00\u5904\u95ee\u9898\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 203
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 204
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u7cbe\u786e\u5217\u8f66\u5e2d\u4f4d\u4fe1\u606f\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u7cbe\u786e\u5217\u8f66\u540d\u79f0\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u9ad8\u94c1\u8f6f\u5367\u7968\u4ef7\u4fe1\u606f\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u7cbe\u786e\u7968\u4ef7\u8ba1\u7b97\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f18\u5316\u5206\u4eab\u7ed3\u679c\u5185\u5bb9\u3002"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 207
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 208
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u7ad9\u7ad9\u67e5\u8be2\u6709\u65f6\u95ea\u9000\u7684\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 209
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 210
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u6b63\u68c0\u67e5\u66f4\u65b0\u7684bug"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 212
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u7cbe\u786e\u8f66\u6b21\u67e5\u8be2\u4e2d\uff0c\u505c\u9760\u8f66\u7ad9\u7684\u8f66\u6b21\u540d\u79f0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 214
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u8f66\u6b21\u67e5\u8be2\u4e2d\u7684\u4e00\u4e9bbug"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 216
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u6b63\u665a\u70b9\u67e5\u8be2\u7ed3\u679c\u65e0\u6cd5\u5206\u4eab\u7684\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u8c03\u6574\u4e3b\u754c\u9762\u5207\u6362\u65b9\u5f0f\uff0c\u6807\u9898\u56fa\u5b9a"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u7a0b\u5e8f\u7a33\u5b9a\u6027"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 219
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.0.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 220
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u5386\u65f6\u53ef\u80fd\u8ba1\u7b97\u9519\u8bef\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 221
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 222
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u652f\u6301Android TV \u5e73\u53f0\u64cd\u4f5c"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 224
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d\u8f66\u6b21\u67e5\u8be2\u65e0\u6cd5\u8f93\u5165\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d\u90e8\u5206\u624b\u673a\u542f\u52a8\u505c\u6b62\u9519\u8bef"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d\u4f59\u7968\u67e5\u8be2\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u754c\u9762\u8c03\u6574\u9002\u914d\u5e73\u677f\u67e5\u8be2"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 234
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u5206\u4eab\u529f\u80fd\u64cd\u4f5c\u4f18\u5316"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u754c\u9762\u8c03\u6574\u4f18\u5316"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 237
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 238
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590dpad\u67e5\u8be2\u51fa\u9519\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u5e73\u677f\u754c\u9762\u8c03\u6574\u4f18\u5316"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 241
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 242
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d\u90e8\u5206\u8f66\u6b21\u67e5\u8be2\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 244
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u754c\u9762"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 246
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.1.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 247
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a012306\u6ce8\u518c\u94fe\u63a5"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u90e8\u5206\u624b\u673a\u5f02\u5e38\u9000\u51fa\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 250
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 251
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a012306\u5728\u7ebf\u8d2d\u7968\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 253
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 254
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u8ba2\u5b66\u751f\u7968\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u5207\u6362\u8d26\u53f7\u7684bug"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 256
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 257
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u8054\u7cfb\u4eba\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u8ba2\u7968\u5931\u8d25\uff0c\u5f39\u51fa\u9a8c\u8bc1\u7801\u8f93\u5165\u6846\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u7528\u6237\u672a\u767b\u5f55\u60c5\u51b5\u4e0b\u7684\u5904\u7406\u6d41\u7a0b"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 260
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 261
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u4ece\u67e5\u8be2\u7ed3\u679c\u4e2d\u76f4\u63a5\u8ba2\u7968\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u4f59\u7968\u67e5\u8be2\uff0c\u65e0\u6cd5\u67e5\u770b\u8f66\u6b21\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 264
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u8fd0\u884c\u89c4\u5f8b\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u67e5\u8be2\u7ed3\u679c\u663e\u793a"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 266
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 267
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u7968\u4ef7\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 270
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u5728\u7ebf\u67e5\u8be2\u65f6\uff0c\u6309\u7c7b\u578b\u8fc7\u6ee4\u8f66\u6b21\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 272
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 273
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u6b63\u665a\u70b9\u65e0\u6cd5\u67e5\u8be2\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 275
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 276
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u8054\u7f51\u8f66\u6b21\u67e5\u8be2\u5931\u8d25\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 278
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.5.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 279
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u95ee\u9898\u53cd\u9988\u529f\u80fd\uff0c\u5728\u8bbe\u7f6e\u9875\u9762\u91cc\u9762\u9009\u62e9"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 281
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 282
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u7968\u4ef7\u8ba1\u7b97\u65b9\u6cd5\uff0c\u7968\u4ef7\u66f4\u51c6\u786e"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 284
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 285
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u8f66\u6b21\u7c7b\u578b\u589e\u52a0\u9ad8\u94c1\u9009\u9879"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u5386\u65f6\u8ba1\u7b97\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u7968\u4ef7\u9519\u8bef\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 289
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 290
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u62a2\u7968\u8f66\u6b21\u591a\u9009\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u8054\u7cfb\u4eba\u7684\u5220\u9664\u7f16\u8f91\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u89e3\u51b3\u65e5\u671f\u67e5\u8be2\u7684\u5c0f\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 294
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 295
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u62a2\u7968\u63d0\u793a\u97f3\u8bbe\u7f6e\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u8054\u7cfb\u4eba\u7684\u72b6\u6001\u63d0\u793a"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u6b63\u5176\u4ed6\u5c0f\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 299
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 300
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u767b\u5f55\u5b89\u5168\u8bbe\u7f6e\u9009\u9879"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 302
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 303
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u52a0\u9000\u7968\u6539\u7b7e\u7684\u94fe\u63a5"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 305
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 306
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d12306\u767b\u5f55\u8ba2\u7968\u9519\u8bef\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 308
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 309
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4fee\u590d\u90e8\u5206\u673a\u5668\u65e0\u6cd5\u8fd0\u884c\u7a0b\u5e8f\u7684\u95ee\u9898"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 311
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 312
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u5f3a\u7a0b\u5e8f\u7a33\u5b9a\u6027\uff0c\u4f18\u5316\u95ee\u9898\u53cd\u9988\u529f\u80fd"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 314
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.6.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 315
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u5f3a\u7a0b\u5e8f\u7a33\u5b9a\u6027\u548c\u5b89\u5168\u6027"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u51cf\u5c11\u8ba2\u7968\u7b49\u5f85\u65f6\u95f4"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u7528\u6237\u767b\u5f55\u4ee5\u53ca\u8ba2\u7968\u4f53\u9a8c\uff0c\u64cd\u4f5c\u53ef\u4ee5\u6309\u540e\u9000\u952e\u53d6\u6d88"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 319
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 320
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u589e\u5f3a\u7a0b\u5e8f\u7a33\u5b9a\u6027"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u63d0\u4ea4\u8ba2\u5355\u5931\u8d25\u65f6\uff0c\u63d0\u4f9b\u91cd\u8bd5\u529f\u80fd"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 323
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 324
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u754c\u9762\u663e\u793a\u5fae\u8c03"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 327
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 328
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 330
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 331
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u8ba2\u7968\u7b97\u6cd5\uff0c\u63d0\u9ad8\u8ba2\u7968\u6210\u529f\u7387"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 334
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 335
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u65e5\u671f\u9009\u62e9\u589e\u52a0\u519c\u5386\u663e\u793a"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 337
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 338
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u589e\u52a0\u67e5\u8be2\u7ed3\u679c\u7684\u8fc7\u6ee4\u529f\u80fd"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u767b\u5f55\u6d41\u7a0b"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u67e5\u8be2\u7ed3\u679c\u754c\u9762\u663e\u793a\u8c03\u6574"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 343
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 344
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u7d27\u6025\u4fee\u590d\u5d29\u6e83\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 346
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 347
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4fee\u6b63\u90e8\u5206\u8f66\u6b21\u5386\u65f6\u8ba1\u7b97\u9519\u8bef\u7684\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u90e8\u5206\u60c5\u51b5\u4e0b\uff0c\u5728\u7ebf\u67e5\u8be2\u6ca1\u6709\u7ed3\u679c\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 351
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 352
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u6b63\u90e8\u5206\u8f66\u6b21\u5217\u8f66\u8fd0\u884c\u89c4\u5f8b\u9519\u8bef\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 354
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.7.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u8ba2\u7968\u6d41\u7a0b\uff0c\u8ba2\u7968\u901f\u5ea6\u66f4\u5feb"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 357
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 358
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u6539\u7528\u6237\u53d1\u73b0\u7684\u90e8\u5206\u8f66\u6b21\u67e5\u8be2\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 360
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 361
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u6b63\u5df2\u5b8c\u6210\u8ba2\u5355\u67e5\u8be2\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u6b63\u5b66\u751f\u8054\u7cfb\u4eba\u7684\u6dfb\u52a0\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 364
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 365
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u7d27\u6025\u4fee\u590d12306\u66f4\u65b0\u9a8c\u8bc1\u7801\u5bfc\u81f4\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u8ba2\u5355\u63d0\u4ea4\u7b97\u6cd5\uff0c\u63d0\u9ad8\u4e0b\u5355\u6210\u529f\u7387"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 368
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 369
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u6839\u636e\u53cd\u9988\u8c03\u6574\u754c\u9762"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 370
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 371
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u8f66\u6b21\u8f66\u7ad9\u5386\u53f2\u8bb0\u5f55\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u8f93\u5165\u6846\u589e\u52a0\u6e05\u9664\u6309\u94ae"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u4e2d\u65ad\u67e5\u8be2\u8fc7\u7a0b\u529f\u80fd\uff08\u6309\u540e\u9000\u952e\uff09"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 375
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 376
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u79bb\u7ebf\u7ad9\u7ad9\u67e5\u8be2\u901f\u5ea6"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u7528\u6237\u767b\u5f55\u8fc7\u7a0b\uff0c\u89e3\u51b3\u6709\u65f6\u767b\u5f55\u5931\u8d25\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 379
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 380
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u652f\u4ed8\u754c\u9762"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 382
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 383
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u590d\u8fd0\u884c\u65f6\u95f4\u9519\u8bef\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 385
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 386
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u7ed3\u679c\u6392\u5e8f\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u90e8\u5206\u624b\u673a\u8f93\u5165\u7ad9\u540d\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 389
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.8.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 390
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u9a8c\u8bc1\u7801\u663e\u793a"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u5df2\u5b8c\u6210\u8ba2\u5355\u663e\u793a"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u8054\u7f51\u67e5\u8be2\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 394
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 395
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u7ecf\u5178\u754c\u9762\u67e5\u8be2\u7ed3\u679c\u663e\u793a"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 397
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 398
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u513f\u7ae5\u7968\u8ba2\u7968\u529f\u80fd\uff0c\u53ef\u5728\u9009\u62e9\u4e58\u5ba2\u65f6\u6dfb\u52a0\u513f\u7ae5\u7968"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u8ba2\u7968\u6709\u65f6\u67e5\u4e0d\u5230\u8f66\u7968\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 401
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 402
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u8f66\u6b21\u8ba2\u7968\u754c\u9762"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 404
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 405
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u90e8\u5206\u7528\u6237\u65e0\u6cd5\u8ba2\u7968\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u7ad9\u540d\u8f93\u5165"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u7ecf\u5178\u754c\u9762\u7528\u6237\u64cd\u4f5c"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 411
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 412
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u589e\u52a0\u652f\u4ed8\u5b9d\u5ba2\u6237\u7aef\u652f\u4ed8\u529f\u80fd"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u589e\u52a0\u8ba2\u7968\u8bbe\u7f6e\u4f59\u7968\u67e5\u8be2\u9891\u7387"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u7ecf\u5178\u754c\u9762\u8f66\u6b21\u67e5\u8be2\u7ed3\u679c\u663e\u793a"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 417
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 418
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 420
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "2.9.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 421
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u8ba2\u7968\u8f66\u6b21\u9009\u62e9\u64cd\u4f5c"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 423
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 424
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u8ba2\u7968\u7528\u6237\u4f53\u9a8c"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u589e\u52a0\u9000\u7968\u529f\u80fd"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 427
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 428
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u90e8\u5206\u8f66\u6b21\u67e5\u8be2\u62a5\u9519\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 430
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 431
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 433
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 434
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u6e05\u9664\u67e5\u8be2\u8bb0\u5f55\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 437
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 438
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u4f59\u7968\u67e5\u8be2\u663e\u793a\u7968\u4ef7"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u8f66\u6b21\u62c5\u5f53\u4f01\u4e1a\u4fe1\u606f"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 441
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 442
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u663e\u793a\u672a\u5b8c\u6210\u8ba2\u5355\u652f\u4ed8\u5269\u4f59\u65f6\u95f4"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u79bb\u7ebf\u67e5\u8be2\u6709\u65f6\u901f\u5ea6\u6162\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 445
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 446
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 448
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 449
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u6b63\u665a\u70b9\u67e5\u8be2\u65b9\u5f0f\u9009\u9879"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 452
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 453
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u4f59\u7968\u67e5\u8be2\u6709\u65f6\u51fa\u9519\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 456
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.0.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 457
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u89e3\u51b3\u652f\u4ed8\u5b9d\u5ba2\u6237\u7aef\u652f\u4ed8\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 459
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 460
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u5f3a\u68c0\u7968\u53e3\u4fe1\u606f\u51c6\u786e\u6027"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 461
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 462
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a012306\u7528\u6237\u6ce8\u518c\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 464
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 465
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u7528\u6237\u624b\u673a\u6838\u9a8c\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 467
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 468
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4fee\u6539\u7528\u6237\u53cd\u9988\u7684\u95ee\u9898"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 470
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 471
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u62a2\u7968\u903b\u8f91"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u4f18\u5316\u4f59\u7968\u67e5\u8be2"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 477
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 478
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u589e\u52a0\u5217\u8f66\u8fd0\u884c\u56fe\u663e\u793a"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 480
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 481
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u65b0\u589e\u5217\u8f66\u901f\u5ea6\u663e\u793a"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u65b0\u589e\u5373\u5c06\u5230\u8fbe\u8f66\u7ad9\u663e\u793a"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 485
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 486
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u89e3\u51b3\u73af\u7ebf\u67e5\u8be2\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 488
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.1.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 489
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u89e3\u51b3\u5217\u8f66\u8fd0\u884c\u89c4\u5219\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 491
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 492
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u89e3\u51b3\u5347\u7ea7\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 494
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 495
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u589e\u52a0\u4f59\u7968\u4e0d\u8db3\u65f6,\u66f4\u591a\u4e58\u8f66\u65b9\u6848\u67e5\u8be2\u529f\u80fd"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 498
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 499
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u4f18\u5316\u4e58\u8f66\u65b9\u6848\u67e5\u8be2\u529f\u80fd"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 502
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 503
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u7528\u6237\u64cd\u4f5c\u4f53\u9a8c"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u95ee\u9898"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 506
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 507
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u4f18\u531612306\u652f\u4ed8\u65b9\u5f0f"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 509
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 510
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u589e\u52a0\u8fd0\u884c\u56fe\u9690\u85cf\u9009\u9879"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 513
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 514
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 517
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 518
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u7ad9\u7ad9\u67e5\u8be2\u901f\u5ea6"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u5176\u4ed6\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 521
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 522
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u6a21\u7cca\u67e5\u8be2\u7b97\u6cd5"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 524
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.2.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 525
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 527
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 528
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u5f3a\u5236\u4e2d\u8f6c\u529f\u80fd"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 530
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 531
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u754c\u9762,\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 533
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 534
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u4e2d\u8f6c\u5f02\u5e38\u7684\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 536
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 537
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u66f4\u65b0\u8d2d\u7968\u65f6\u95f406:00-23:00"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 540
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 541
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u65e0\u6cd5\u652f\u4ed8\u7684\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 543
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 544
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 546
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 547
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u6b63\u665a\u70b9\u67e5\u8be2\u7b97\u6cd5"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 549
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 550
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u7528\u6237\u754c\u9762\u8c03\u6574"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 552
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 553
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u8f66\u7ad9\u67e5\u8be2\u63d0\u4f9b\u8f66\u6b21\u8fc7\u6ee4\u529f\u80fd"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 555
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.3.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 556
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u5b66\u751f\u8eab\u4efd\u8d2d\u4e70\u6210\u4eba\u7968\u529f\u80fd"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 558
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 559
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u4f59\u7968\u67e5\u8be2\u7b97\u6cd5"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 561
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 562
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u8054\u7cfb\u4eba\u9690\u79c1\u4fdd\u62a4"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u8f66\u6b21\u4fe1\u606f\u663e\u793a"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u94c1\u8def\u5c40\u62db\u8058\u4fe1\u606f"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 566
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 567
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u5206\u4eab\u529f\u80fd,\u652f\u6301\u5206\u4eab\u81f3qq"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 570
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 571
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u589e\u52a0\u540e\u53f0\u5237\u7968\u529f\u80fd,\u8bf7\u5408\u7406\u4f7f\u7528"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 573
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 574
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u540e\u53f0\u5237\u7968\u63d0\u793a"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u65e0\u6cd5\u5206\u4eab\u7684\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 577
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 578
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4fee\u590d\u56e012306\u6539\u7248\u5bfc\u81f4\u7684\u65e0\u6cd5\u8ba2\u7968\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 580
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 581
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 583
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 584
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4fee\u590d\u8054\u7cfb\u4eba\u62a4\u7167\u7f16\u8f91\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u8d2d\u7968\u9884\u552e\u671f\u8c03\u6574\u4e3a30\u5929"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 587
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 588
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u90e8\u5206\u624b\u673a\u65e0\u6cd5\u542f\u52a8\u652f\u4ed8\u5b9d\u5ba2\u6237\u7aef\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u65e5\u671f\u9009\u62e9\u754c\u9762"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.4.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 592
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u8ba2\u7968\u65e5\u671f\u8bbe\u5b9a"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u89e3\u51b3\u7528\u6237\u63d0\u51fa\u7684\u95ee\u9898"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 595
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 596
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u4f18\u5316\u6b63\u665a\u70b9\u67e5\u8be2"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u90e8\u5206\u624b\u673a\u65e0\u6cd5\u542f\u52a8\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u90e8\u5206\u624b\u673a\u663e\u793a\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 600
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 601
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u4f18\u5316\u540e\u53f0\u76d1\u63a7\u7b97\u6cd5"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u90e8\u5206\u754c\u9762\u4f18\u5316\u663e\u793a"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 604
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 605
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u4fee\u590d\u4f4e\u7248\u672c\u4e0a\u7684\u95ea\u9000\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 607
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 608
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u7528\u6237\u4f53\u9a8c\u4f18\u5316\uff0c\u652f\u6301\u4e0b\u62c9\u5237\u65b0"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 610
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 611
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u4f59\u7968\u67e5\u8be2\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 613
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 614
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 616
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 617
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b312306\u5347\u7ea7\u5bfc\u81f4\u7684\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 620
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 621
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 623
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 624
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 627
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.5.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 628
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u65e5\u671f\u9009\u62e9\u663e\u793a\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u5176\u4ed6\u95ee\u9898"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 631
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 632
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 634
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 635
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 637
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 638
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 640
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 641
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 643
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 644
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\u589e\u52a0\u52a8\u5367\u5e2d\u4f4d\u652f\u6301"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 646
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 647
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 649
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 650
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 652
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 653
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 654
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.6.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 655
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 657
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 658
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0D\uff0cG\uff0cC\u8f66\u6b21\u9009\u5ea7\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 660
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 661
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u62c5\u5f53\u4f01\u4e1a\u7ea0\u9519\u529f\u80fd"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 663
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 664
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u589e\u52a0\u5fae\u4fe1\u652f\u4ed8"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 666
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 667
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 669
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 670
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u767b\u5f55\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 672
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 673
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f18\u5316\u754c\u9762\u663e\u793a"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 675
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 676
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u767b\u5f55\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 678
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.7.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 679
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f18\u5316\u672a\u5b8c\u6210\u8ba2\u5355\u67e5\u8be2"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 681
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 682
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u767b\u5f55\u5907\u7528\u65b9\u6848"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 683
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 684
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u7528\u6237\u53d1\u73b0\u7684\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 685
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 686
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u6807\u6ce8\u590d\u5174\u53f7\u5217\u8f66\u4fe1\u606f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 687
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 688
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u6700\u65b0\u7cfb\u7edf\u7684\u9002\u914d\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 690
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 691
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 693
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 694
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u8f66\u6b21\u505c\u5f00\u589e\u5f00\u4fe1\u606f\u67e5\u8be2"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 696
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 697
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 699
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 700
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4f18\u5316\u7ad9\u540d\u8f93\u5165\uff0c\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 702
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 703
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 705
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.8.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 706
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u8f66\u7ad9\u8d77\u552e\u65f6\u95f4\u4ee5\u53ca\u68c0\u7968\u53e3\u67e5\u8be2\u529f\u80fd"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 708
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 709
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u65e0\u6cd5\u652f\u4ed8\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 710
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 711
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u8f66\u7ad9\u5927\u5c4f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 713
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 714
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 716
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 717
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 719
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 720
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u7f51\u7edc\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 722
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 723
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u7528\u6237\u53cd\u9988\u7684\u8ba2\u7968\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 725
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 726
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u4ea4\u8def\u4fe1\u606f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u89e3\u51b3\u767b\u5f55\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 729
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 730
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u8f66\u7ad9\u8f66\u6b21\u4fe1\u606f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 732
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 733
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u7968\u4ef7\u8ba1\u7b97\u6539\u8fdb"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 735
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "3.9.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 736
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u4e2d\u8f6c\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 738
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 739
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 741
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.1"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 742
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u4ea4\u8def\u8868\u67e5\u8be2\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 744
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.2"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 745
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u7528\u6237\u53cd\u9988\u7684\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 747
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.3"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 748
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u65b0\u7684\u8f66\u6b21\u7c7b\u578b\u652f\u6301"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 750
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.4"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 751
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u8ba2\u7968\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 753
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.5"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 754
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u589e\u52a0\u5217\u8f66\u9910\u8f66\u4fe1\u606f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 756
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.6"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 757
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u4fee\u590d\u7528\u6237\u53cd\u9988\u95ee\u9898"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 759
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.7"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 760
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u4f18\u5316\u8f66\u6b21\u505c\u8fd0\u4fe1\u606f\u67e5\u8be2"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 762
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.8"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 763
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 765
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.0.9"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 766
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 768
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    const-string v5, "4.1.0"

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 769
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u4f18\u5316\u5728\u7ebf\u67e5\u8be2"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 772
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getUpdateVer()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v3, "2016"

    .line 774
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v1, "3.9.0"

    .line 777
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/lltskb/lltskb/engine/ResMgr;->setContext(Landroid/content/Context;)V

    .line 778
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v3

    .line 779
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    return v2

    .line 781
    :cond_4
    invoke-virtual {v3, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5

    if-nez p1, :cond_5

    return v2

    .line 785
    :cond_5
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->setUpdateVer(Ljava/lang/String;)V

    const/4 v3, 0x5

    if-eqz p1, :cond_6

    const/16 v3, 0xa

    .line 790
    :cond_6
    sget-object v4, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const-string v5, ""

    :goto_0
    if-ltz v4, :cond_8

    if-lez v3, :cond_8

    .line 791
    sget-object v6, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mVers:Ljava/util/Vector;

    invoke-virtual {v6, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez p1, :cond_7

    .line 792
    invoke-static {v6, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->compareVersion(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_8

    .line 793
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "<h5><font color=\"#b4cc55\">&nbsp;v&nbsp;"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "</font></h5>"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mComments:Ljava/util/Vector;

    .line 794
    invoke-virtual {v5, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 799
    :cond_8
    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_9

    return v2

    .line 802
    :cond_9
    new-instance p1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const p0, 0x7f0d01d9

    .line 803
    invoke-virtual {p1, p0}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 805
    :try_start_0
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 807
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 808
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const p0, 0x7f0d01f4

    const/4 v1, 0x0

    .line 811
    invoke-virtual {p1, p0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 813
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p0

    .line 823
    :try_start_1
    invoke-virtual {p0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 824
    invoke-virtual {p0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    if-eqz p0, :cond_a

    const p1, 0x7f0e00bd

    .line 826
    invoke-virtual {p0, p1}, Landroid/view/Window;->setWindowAnimations(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p0

    .line 828
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_a
    :goto_2
    const-string p0, "showNewVersionComments end"

    .line 830
    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x1

    return p0
.end method

.method public static showRandCodeDlg(Landroid/content/Context;Ljava/lang/String;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V
    .locals 7

    .line 1668
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "12306\u624b\u673a\u9a8c\u8bc1"

    .line 1669
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1671
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b007b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1673
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    const v2, 0x7f0902a6

    .line 1674
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 1676
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->isInfoShow()Z

    move-result v3

    if-nez v3, :cond_0

    const p1, 0x7f0d0237

    .line 1677
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    const v3, 0x7f0d0236

    .line 1679
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1680
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v4, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1681
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    const p1, 0x7f0900cc

    .line 1687
    invoke-virtual {v1, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    .line 1689
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;

    invoke-direct {v1, p1, p0, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils$5;-><init>(Landroid/widget/EditText;Landroid/content/Context;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V

    const-string p0, "\u5b8c\u6210\u6ce8\u518c"

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1708
    new-instance p0, Lcom/lltskb/lltskb/utils/LLTUIUtils$6;

    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils$6;-><init>(Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V

    const-string p1, "\u53d6\u6d88"

    invoke-virtual {v0, p1, p0}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1721
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object p0

    sput-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mRandCodeDlg:Landroid/support/v7/app/AlertDialog;

    return-void
.end method

.method public static showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lltskb/lltskb/BaseActivity;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/widget/AdapterView$OnItemClickListener;",
            ")V"
        }
    .end annotation

    const-string v0, "LLTUIUtils"

    const-string v1, "showSelectDialog"

    .line 1330
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    const v1, 0x7f0e0003

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0b0087

    .line 1332
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    .line 1334
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$1;

    const v2, 0x109000f

    invoke-direct {v1, p0, v2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$1;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v2, 0x7f0901e9

    .line 1352
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/widget/ListView;

    .line 1353
    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1354
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;

    move-object v2, v1

    move-object v3, p0

    move v4, p2

    move-object v5, p1

    move-object v6, p3

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, Lcom/lltskb/lltskb/utils/LLTUIUtils$2;-><init>(Lcom/lltskb/lltskb/BaseActivity;ILjava/util/List;Landroid/widget/AdapterView$OnItemClickListener;Landroid/support/v7/app/AppCompatDialog;)V

    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1371
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    if-eqz p0, :cond_0

    .line 1373
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    const/4 p1, 0x1

    .line 1374
    invoke-virtual {v8, p0, p1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1375
    invoke-virtual {v8, p0}, Landroid/widget/ListView;->setSelection(I)V

    .line 1377
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void
.end method

.method public static showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;Ljava/lang/String;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lltskb/lltskb/BaseActivity;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/widget/AdapterView$OnItemClickListener;",
            ")V"
        }
    .end annotation

    const-string v0, "LLTUIUtils"

    const-string v1, "showSelectDialog"

    .line 1382
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    const v1, 0x7f0e0003

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0b0087

    .line 1384
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    .line 1386
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$3;

    const v2, 0x109000f

    invoke-direct {v1, p0, v2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$3;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const p0, 0x7f0901e9

    .line 1404
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/ListView;

    .line 1405
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1406
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTUIUtils$4;

    invoke-direct {v1, p3, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils$4;-><init>(Landroid/widget/AdapterView$OnItemClickListener;Landroid/support/v7/app/AppCompatDialog;)V

    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1419
    invoke-interface {p1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 p2, 0x1

    .line 1420
    invoke-virtual {p0, p1, p2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1421
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 1423
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void
.end method

.method public static showToast(Landroid/content/Context;I)V
    .locals 1

    .line 844
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 846
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 1

    .line 850
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 854
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->cancelToast()V

    .line 855
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    .line 856
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    sput-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    goto :goto_0

    .line 858
    :cond_1
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {p0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 859
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {p0, v0}, Landroid/widget/Toast;->setDuration(I)V

    .line 861
    :goto_0
    sget-object p0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 863
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public static startActivity(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .line 1889
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1890
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x10

    if-lt p1, v0, :cond_0

    .line 1891
    new-instance p1, Lcom/lltskb/lltskb/utils/LLTUIUtils$7;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils$7;-><init>(Landroid/app/Activity;)V

    .line 1895
    invoke-virtual {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils$7;->trans()V

    :cond_0
    return-void
.end method

.method public static startActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 1881
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1882
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 1884
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method public static testAlipay(Landroid/content/Context;)V
    .locals 3

    .line 1876
    new-instance v0, Landroid/content/Intent;

    const-string v1, "alipayqr://platformapi/startapp?saId=10000007&clientVersion=3.7.0.0718&qrcode=https%3A%2F%2Fqr.alipay.com%2Fltk4al8ldodiw7y3f0%3F_s%3Dweb-other"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1877
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1748
    invoke-static {p0}, Landroid/support/v4/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 1749
    invoke-static {p0, p1}, Landroid/support/v4/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    return-object p0
.end method
