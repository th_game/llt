.class public Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;
.super Landroid/support/v7/app/AppCompatDialog;
.source "SelectTrainTypeDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;
    }
.end annotation


# instance fields
.field private chkBoxDC:Landroid/widget/CheckBox;

.field private chkBoxG:Landroid/widget/CheckBox;

.field private chkBoxK:Landroid/widget/CheckBox;

.field private chkBoxPK:Landroid/widget/CheckBox;

.field private chkBoxPKE:Landroid/widget/CheckBox;

.field private chkBoxT:Landroid/widget/CheckBox;

.field private chkBoxZ:Landroid/widget/CheckBox;

.field private chkboxAll:Landroid/widget/CheckBox;

.field private mFilter:I

.field private mListener:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;)V
    .locals 1

    .line 43
    invoke-direct {p0, p1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 46
    invoke-virtual {p1, v0}, Landroid/view/Window;->requestFeature(I)Z

    const v0, 0x106000d

    .line 47
    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    const v0, 0x7f0e000b

    .line 48
    invoke-virtual {p1, v0}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_0
    const p1, 0x7f0b0095

    .line 51
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->setContentView(I)V

    .line 52
    iput-object p3, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mListener:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;

    .line 53
    iput p2, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    .line 55
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkboxAll:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxDC:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxZ:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxT:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPK:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPKE:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxK:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxG:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)I
    .locals 0

    .line 20
    iget p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    return p0
.end method

.method static synthetic access$802(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;I)I
    .locals 0

    .line 20
    iput p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    return p1
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mListener:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;

    return-object p0
.end method

.method private initView()V
    .locals 3

    const v0, 0x7f09022f

    .line 61
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkboxAll:Landroid/widget/CheckBox;

    const v0, 0x7f090230

    .line 62
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxDC:Landroid/widget/CheckBox;

    const v0, 0x7f090236

    .line 63
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxZ:Landroid/widget/CheckBox;

    const v0, 0x7f090235

    .line 64
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxT:Landroid/widget/CheckBox;

    const v0, 0x7f090232

    .line 65
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxK:Landroid/widget/CheckBox;

    const v0, 0x7f090233

    .line 66
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPK:Landroid/widget/CheckBox;

    const v0, 0x7f090234

    .line 67
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPKE:Landroid/widget/CheckBox;

    const v0, 0x7f090231

    .line 68
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxG:Landroid/widget/CheckBox;

    .line 70
    iget v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    const/16 v1, 0xff

    and-int/2addr v0, v1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 72
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkboxAll:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    if-nez v0, :cond_2

    .line 74
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_3

    .line 75
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxDC:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_3
    if-nez v0, :cond_4

    .line 77
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 78
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxT:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_5
    if-nez v0, :cond_6

    .line 80
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 81
    :cond_6
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxK:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_7
    if-nez v0, :cond_8

    .line 83
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_9

    .line 84
    :cond_8
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxZ:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_9
    if-nez v0, :cond_a

    .line 86
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_b

    .line 87
    :cond_a
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPK:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_b
    if-nez v0, :cond_c

    .line 89
    iget v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_d

    .line 90
    :cond_c
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPKE:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_d
    if-nez v0, :cond_e

    .line 92
    iget v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->mFilter:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_f

    .line 93
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxG:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 96
    :cond_f
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkboxAll:Landroid/widget/CheckBox;

    new-instance v1, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$1;-><init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    new-instance v0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;-><init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V

    .line 137
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxDC:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 138
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxZ:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 139
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxT:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 140
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxK:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 141
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPK:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 142
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxPKE:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 143
    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->chkBoxG:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f090206

    .line 145
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 146
    new-instance v1, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;-><init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09007c

    .line 183
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 184
    new-instance v1, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$4;-><init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
