.class public Lcom/lltskb/lltskb/view/DateWidgetDayHeader;
.super Landroid/view/View;
.source "DateWidgetDayHeader.java"


# static fields
.field private static fTextSize:I = 0x16


# instance fields
.field private Calendar_WeekBgColor:I

.field private Calendar_WeekFontColor:I

.field private iWeekDay:I

.field private pt:Landroid/graphics/Paint;

.field private rect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .line 37
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    const/4 v0, -0x1

    .line 32
    iput v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->iWeekDay:I

    const/16 v0, 0x10

    .line 38
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->fTextSize:I

    .line 39
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f060001

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->Calendar_WeekBgColor:I

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f060002

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->Calendar_WeekFontColor:I

    return-void
.end method

.method private drawDayHeader(Landroid/graphics/Canvas;)V
    .locals 5

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->Calendar_WeekBgColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 63
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 64
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    sget v1, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->fTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 66
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 67
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    iget v2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->Calendar_WeekFontColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->iWeekDay:I

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/DayStyle;->getWeekDayName(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    iget-object v2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    shr-int/2addr v3, v1

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    .line 72
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    shr-int/lit8 v1, v3, 0x1

    sub-int/2addr v2, v1

    .line 73
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->getHeight()I

    move-result v1

    .line 74
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->getTextHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    .line 75
    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->bottom:F

    sub-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v2, v2

    int-to-float v1, v1

    .line 76
    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private getTextHeight()I
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->pt:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 47
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 50
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->rect:Landroid/graphics/RectF;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->drawDayHeader(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setData(I)V
    .locals 0

    .line 86
    iput p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->iWeekDay:I

    return-void
.end method
