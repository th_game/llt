.class Lcom/lltskb/lltskb/view/WebBrowser$9;
.super Landroid/webkit/WebChromeClient;
.source "WebBrowser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/WebBrowser;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/WebBrowser;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 471
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$9;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 475
    invoke-interface {p2, p1, v0, v1}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    return-void
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 1

    .line 488
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    .line 490
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$9;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1200(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/ProgressBar;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 491
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$9;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1200(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/ProgressBar;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->postInvalidate()V

    .line 492
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$9;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1200(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/ProgressBar;

    move-result-object p1

    const/16 v0, 0x64

    if-ge p2, v0, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    .line 480
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 482
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$9;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$600(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
