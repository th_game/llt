.class Lcom/lltskb/lltskb/view/WebBrowser$3;
.super Ljava/lang/Object;
.source "WebBrowser.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/WebBrowser;->destroyWebView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/WebBrowser;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const-string v0, "WebBrowser"

    const-string v1, "destroyWebView"

    .line 100
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->removeAllViews()V

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$200(Lcom/lltskb/lltskb/view/WebBrowser;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$200(Lcom/lltskb/lltskb/view/WebBrowser;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "12306.cn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroyDrawingCache()V

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$3;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 111
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method
