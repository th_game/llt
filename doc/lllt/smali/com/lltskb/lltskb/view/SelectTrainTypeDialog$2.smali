.class Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;
.super Ljava/lang/Object;
.source "SelectTrainTypeDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 125
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    .line 126
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$000(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$100(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$300(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    .line 129
    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$200(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$600(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    .line 130
    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$400(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$500(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    .line 131
    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$700(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 132
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$2;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$000(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void
.end method
