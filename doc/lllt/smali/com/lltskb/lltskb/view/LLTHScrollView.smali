.class public Lcom/lltskb/lltskb/view/LLTHScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "LLTHScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;,
        Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LLTScrollView"


# instance fields
.field mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 26
    new-instance p1, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    invoke-direct {p1}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    const-string p1, "LLTScrollView"

    const-string v0, "contructor"

    .line 36
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance p1, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    invoke-direct {p1}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance p1, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    invoke-direct {p1}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    return-void
.end method


# virtual methods
.method public AddOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->AddOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V

    return-void
.end method

.method public RemoveOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->RemoveOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView;->mScrollViewObserver:Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->NotifyOnScrollChanged(IIII)V

    .line 79
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 63
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
