.class Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;
.super Ljava/lang/Object;
.source "CalendarView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/CalendarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Pre_MonthOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/CalendarView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/CalendarView;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 363
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 364
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$510(Lcom/lltskb/lltskb/view/CalendarView;)I

    .line 366
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$500(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result p1

    const/16 v0, 0xb

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 367
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$502(Lcom/lltskb/lltskb/view/CalendarView;I)I

    .line 368
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$610(Lcom/lltskb/lltskb/view/CalendarView;)I

    .line 371
    :cond_0
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 372
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v3}, Lcom/lltskb/lltskb/view/CalendarView;->access$500(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v3

    invoke-virtual {p1, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 373
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/CalendarView;->access$600(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v1

    invoke-virtual {p1, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 374
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 375
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/16 v0, 0xc

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 376
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/16 v0, 0xd

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 377
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/16 v0, 0xe

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 378
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$200(Lcom/lltskb/lltskb/view/CalendarView;)V

    .line 380
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p1, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 381
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iget-object v0, p1, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/CalendarView;->GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p1, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 383
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/CalendarView;->access$300(Lcom/lltskb/lltskb/view/CalendarView;)Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    return-void
.end method
