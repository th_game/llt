.class public Lcom/lltskb/lltskb/view/LLTProgressDialog;
.super Landroid/support/v7/app/AppCompatDialog;
.source "LLTProgressDialog.java"


# static fields
.field public static final STYLE_CANCEL:I = 0x4

.field public static final STYLE_NO:I = 0x2

.field public static final STYLE_OK:I = 0x10

.field public static final STYLE_PROG:I = 0x8

.field public static final STYLE_YES:I = 0x1


# instance fields
.field private mBtnLayout:Landroid/view/View;

.field private mCancel:Landroid/widget/Button;

.field private mMessage:Landroid/widget/TextView;

.field private mNo:Landroid/widget/Button;

.field private mOk:Landroid/widget/Button;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mStyle:I

.field private mTitle:Landroid/widget/TextView;

.field private mYes:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->init()V

    return-void
.end method

.method private init()V
    .locals 1

    const v0, 0x7f0b0096

    .line 61
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setContentView(I)V

    const v0, 0x7f090223

    .line 62
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f090190

    .line 63
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mMessage:Landroid/widget/TextView;

    const v0, 0x7f0901ac

    .line 64
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f09005b

    .line 65
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mBtnLayout:Landroid/view/View;

    const v0, 0x7f09031d

    .line 66
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mYes:Landroid/widget/Button;

    const v0, 0x7f09019a

    .line 67
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mNo:Landroid/widget/Button;

    const v0, 0x7f09007b

    .line 68
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mCancel:Landroid/widget/Button;

    const v0, 0x7f0901a0

    .line 69
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mOk:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method public setButtonVisibility(I)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mBtnLayout:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setCancelBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mCancel:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setIndeterminate(Z)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    return-void
.end method

.method public setMax(I)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    return-void
.end method

.method public setMessage(I)V
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 88
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMin(I)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    return-void
.end method

.method public setNoBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mNo:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOkBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mOk:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setProgress(I)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method public setStyle(I)V
    .locals 3

    .line 113
    iput p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    .line 114
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    and-int/lit8 p1, p1, 0x17

    const/16 v0, 0x8

    if-nez p1, :cond_0

    .line 115
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mBtnLayout:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 118
    :cond_0
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    and-int/lit8 p1, p1, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    .line 119
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mYes:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 121
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mYes:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 124
    :goto_0
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    and-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_2

    .line 125
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mNo:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 127
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mNo:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    :goto_1
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    const/4 v2, 0x4

    and-int/2addr p1, v2

    if-nez p1, :cond_3

    .line 131
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mCancel:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 133
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mCancel:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 136
    :goto_2
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    and-int/lit8 p1, p1, 0x10

    if-nez p1, :cond_4

    .line 137
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mOk:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 139
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mOk:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 142
    :goto_3
    iget p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mStyle:I

    and-int/2addr p1, v0

    if-nez p1, :cond_5

    .line 143
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_4

    .line 145
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_4
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setYesBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTProgressDialog;->mYes:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
