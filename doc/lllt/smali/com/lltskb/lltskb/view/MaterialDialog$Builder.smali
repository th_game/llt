.class Lcom/lltskb/lltskb/view/MaterialDialog$Builder;
.super Ljava/lang/Object;
.source "MaterialDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/MaterialDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Builder"
.end annotation


# instance fields
.field private mAlertDialogWindow:Landroid/view/Window;

.field private mButtonLayout:Landroid/widget/LinearLayout;

.field private mMessageContentRoot:Landroid/view/ViewGroup;

.field private mMessageView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/lltskb/lltskb/view/MaterialDialog;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/view/MaterialDialog;)V
    .locals 6

    .line 253
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$200(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$102(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/support/v7/app/AlertDialog;)Landroid/support/v7/app/AlertDialog;

    .line 255
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 257
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x20008

    .line 258
    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 260
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0xf

    .line 261
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 263
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    .line 264
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 267
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$200(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0b005a

    const/4 v3, 0x0

    .line 268
    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    .line 269
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 270
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 272
    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v3, 0x7f0800fd

    invoke-virtual {v1, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 274
    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090223

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mTitleView:Landroid/widget/TextView;

    .line 277
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090190

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageView:Landroid/widget/TextView;

    .line 278
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090079

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 279
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$302(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/Button;)Landroid/widget/Button;

    .line 280
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09005d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$402(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/Button;)Landroid/widget/Button;

    .line 281
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090191

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageContentRoot:Landroid/view/ViewGroup;

    .line 283
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$500(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090099

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 286
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 287
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$500(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 289
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$600(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$600(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setTitle(I)V

    .line 292
    :cond_1
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$700(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 293
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$700(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setTitle(Ljava/lang/CharSequence;)V

    .line 295
    :cond_2
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$700(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v1, 0x8

    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$600(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-nez v0, :cond_3

    .line 296
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    :cond_3
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$800(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 299
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$800(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setMessage(I)V

    .line 301
    :cond_4
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$900(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 302
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$900(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setMessage(Ljava/lang/CharSequence;)V

    .line 304
    :cond_5
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1000(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_6

    .line 305
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 306
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1000(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(I)V

    .line 307
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    iget-object v5, p1, Lcom/lltskb/lltskb/view/MaterialDialog;->pListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    invoke-static {}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1100()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 309
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setElevation(F)V

    .line 312
    :cond_6
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1200(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-eq v0, v4, :cond_7

    .line 313
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 314
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1200(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(I)V

    .line 315
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    iget-object v5, p1, Lcom/lltskb/lltskb/view/MaterialDialog;->nListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    invoke-static {}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1100()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 317
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setElevation(F)V

    .line 320
    :cond_7
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1300(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1400(Lcom/lltskb/lltskb/view/MaterialDialog;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 321
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 322
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1300(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 323
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    iget-object v5, p1, Lcom/lltskb/lltskb/view/MaterialDialog;->pListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    invoke-static {}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1100()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 325
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setElevation(F)V

    .line 329
    :cond_8
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1500(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1400(Lcom/lltskb/lltskb/view/MaterialDialog;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 330
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 331
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1500(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 332
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    iget-object v2, p1, Lcom/lltskb/lltskb/view/MaterialDialog;->nListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    invoke-static {}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1100()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 334
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setElevation(F)V

    .line 337
    :cond_9
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1300(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1400(Lcom/lltskb/lltskb/view/MaterialDialog;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1000(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-ne v0, v4, :cond_a

    .line 338
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 340
    :cond_a
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1500(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1400(Lcom/lltskb/lltskb/view/MaterialDialog;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1200(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-ne v0, v4, :cond_b

    .line 341
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 343
    :cond_b
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1600(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    const v1, 0x7f09018e

    if-eq v0, v4, :cond_c

    .line 344
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 346
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1600(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 348
    :cond_c
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1700(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 349
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 351
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1700(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 354
    :cond_d
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1800(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 355
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1800(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setContentView(Landroid/view/View;)V

    goto :goto_0

    .line 356
    :cond_e
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1900(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    if-eqz v0, :cond_f

    .line 357
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$1900(Lcom/lltskb/lltskb/view/MaterialDialog;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setContentView(I)V

    .line 359
    :cond_f
    :goto_0
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2000(Lcom/lltskb/lltskb/view/MaterialDialog;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 360
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2000(Lcom/lltskb/lltskb/view/MaterialDialog;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog;->setCancelable(Z)V

    .line 361
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 362
    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-static {p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_10
    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/view/MaterialDialog;Lcom/lltskb/lltskb/view/MaterialDialog$1;)V
    .locals 0

    .line 244
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;-><init>(Lcom/lltskb/lltskb/view/MaterialDialog;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/lltskb/lltskb/view/MaterialDialog$Builder;)Landroid/view/Window;
    .locals 0

    .line 244
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    return-object p0
.end method


# virtual methods
.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 527
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f09018e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 529
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2

    .line 534
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f09018e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 536
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 1

    .line 541
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 542
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog;->setCancelable(Z)V

    return-void
.end method

.method public setContentView(I)V
    .locals 2

    .line 518
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageContentRoot:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 521
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageContentRoot:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageContentRoot:Landroid/view/ViewGroup;

    .line 522
    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 4

    .line 487
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 489
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 490
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2400(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/ListView;)V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090192

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 496
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 497
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 499
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    .line 500
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/AutoCompleteTextView;

    if-eqz v2, :cond_3

    .line 502
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    const/4 v3, 0x1

    .line 503
    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 504
    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 505
    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public setMessage(I)V
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mMessageView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 386
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 5

    .line 418
    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$200(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 419
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 421
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v2, 0x7f0800fc

    .line 422
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 423
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    const/16 v2, 0xde

    .line 424
    invoke-static {v2, p1, p1, p1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    const/high16 v2, 0x41600000    # 14.0f

    .line 425
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextSize(F)V

    const/16 v2, 0x11

    .line 426
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 427
    iget-object v2, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I

    move-result v2

    invoke-virtual {v0, p1, p1, p1, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 428
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    iget-object p2, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p2

    if-lez p2, :cond_0

    const/16 p2, 0x14

    const/16 v2, 0xa

    .line 430
    iget-object v3, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    const/high16 v4, 0x41100000    # 9.0f

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I

    move-result v3

    invoke-virtual {v1, p2, p1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 431
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 432
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    const/4 p2, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 434
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 435
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 5

    .line 397
    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$200(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 398
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 400
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0800fc

    .line 401
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    const/16 v1, 0xff

    const/16 v2, 0x23

    const/16 v3, 0x9f

    const/16 v4, 0xf2

    .line 402
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 403
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x11

    .line 404
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setGravity(I)V

    const/high16 p1, 0x41600000    # 14.0f

    .line 405
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 406
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I

    move-result p1

    iget-object v1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    const/high16 v2, 0x42000000    # 32.0f

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I

    move-result v1

    iget-object v2, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->this$0:Lcom/lltskb/lltskb/view/MaterialDialog;

    const/high16 v3, 0x41100000    # 9.0f

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/view/MaterialDialog;->access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3, v1, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 407
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 4

    .line 441
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->mAlertDialogWindow:Landroid/view/Window;

    const v1, 0x7f090099

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 442
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 443
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 445
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 447
    new-instance v1, Lcom/lltskb/lltskb/view/MaterialDialog$Builder$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder$1;-><init>(Lcom/lltskb/lltskb/view/MaterialDialog$Builder;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 459
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 461
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 463
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 465
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-ge v1, v2, :cond_1

    .line 466
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 467
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 468
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 469
    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 470
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 473
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 474
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_2

    .line 476
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    .line 477
    invoke-virtual {v1, v3}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 478
    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 479
    invoke-virtual {v1, v3}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method
