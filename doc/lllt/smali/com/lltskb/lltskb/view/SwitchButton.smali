.class public Lcom/lltskb/lltskb/view/SwitchButton;
.super Landroid/view/View;
.source "SwitchButton.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SwitchButton"


# instance fields
.field private DEFAULT_HEIGHT:I

.field private DEFAULT_WIDTH:I

.field private MARGIN:I

.field private downX:F

.field private listener:Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;

.field private mPaint:Landroid/graphics/Paint;

.field private nowStatus:Z

.field private nowX:F

.field private onSlip:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 p1, 0x82

    .line 30
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    const/16 p1, 0x46

    .line 31
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_HEIGHT:I

    const/4 p1, 0x6

    .line 32
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->MARGIN:I

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    .line 46
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 59
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 p1, 0x82

    .line 30
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    const/16 p1, 0x46

    .line 31
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_HEIGHT:I

    const/4 p1, 0x6

    .line 32
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->MARGIN:I

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    .line 46
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 69
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p1, 0x82

    .line 30
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    const/16 p1, 0x46

    .line 31
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_HEIGHT:I

    const/4 p1, 0x6

    .line 32
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->MARGIN:I

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    .line 46
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 80
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->init()V

    return-void
.end method

.method private drawRoundRect(Landroid/graphics/Canvas;I)V
    .locals 4

    .line 96
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getWidth()I

    move-result p2

    .line 98
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getHeight()I

    move-result v0

    .line 99
    new-instance v1, Landroid/graphics/RectF;

    int-to-float p2, p2

    int-to-float v2, v0

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, p2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 101
    div-int/lit8 v0, v0, 0x2

    int-to-float p2, v0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, p2, p2, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private getMeasuredSize(IZ)I
    .locals 3

    .line 258
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 260
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    if-eqz p2, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getPaddingRight()I

    move-result v2

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getPaddingBottom()I

    move-result v2

    :goto_0
    add-int/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_1

    goto :goto_2

    :cond_1
    if-eqz p2, :cond_2

    .line 270
    iget p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    goto :goto_1

    :cond_2
    iget p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_HEIGHT:I

    :goto_1
    add-int/2addr p2, v1

    if-nez v0, :cond_3

    .line 272
    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_2

    :cond_3
    move p1, p2

    :goto_2
    return p1
.end method


# virtual methods
.method public init()V
    .locals 2

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 88
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 89
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->MARGIN:I

    .line 90
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x18

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_HEIGHT:I

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    .line 92
    invoke-virtual {p0, p0}, Lcom/lltskb/lltskb/view/SwitchButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public isChecked()Z
    .locals 1

    .line 230
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 106
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDraw nowX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ",nowStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SwitchButton"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getWidth()I

    move-result v0

    .line 110
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getHeight()I

    move-result v1

    .line 113
    iget v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    div-int/lit8 v3, v0, 0x2

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    const v2, -0x666667

    .line 115
    invoke-direct {p0, p1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->drawRoundRect(Landroid/graphics/Canvas;I)V

    goto :goto_0

    :cond_0
    const v2, -0xd954d8

    .line 118
    invoke-direct {p0, p1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->drawRoundRect(Landroid/graphics/Canvas;I)V

    .line 121
    :goto_0
    iget-boolean v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 122
    iget v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    sub-int v5, v0, v1

    int-to-float v5, v5

    cmpl-float v6, v2, v5

    if-ltz v6, :cond_3

    move v2, v5

    goto :goto_1

    .line 127
    :cond_1
    iget-boolean v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    if-eqz v2, :cond_2

    sub-int v2, v0, v1

    int-to-float v2, v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_1
    cmpg-float v5, v2, v3

    if-gez v5, :cond_4

    goto :goto_2

    :cond_4
    sub-int/2addr v0, v1

    int-to-float v3, v0

    cmpl-float v0, v2, v3

    if-lez v0, :cond_5

    goto :goto_2

    :cond_5
    move v3, v2

    .line 144
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v4

    add-float/2addr v3, v0

    .line 145
    iget v1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->MARGIN:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v4, v1

    int-to-float v1, v4

    iget-object v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    const/4 v0, 0x1

    .line 245
    invoke-direct {p0, p1, v0}, Lcom/lltskb/lltskb/view/SwitchButton;->getMeasuredSize(IZ)I

    move-result p1

    const/4 v0, 0x0

    .line 246
    invoke-direct {p0, p2, v0}, Lcom/lltskb/lltskb/view/SwitchButton;->getMeasuredSize(IZ)I

    move-result p2

    .line 247
    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/view/SwitchButton;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .line 153
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onTouch event="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SwitchButton"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getWidth()I

    move-result p1

    .line 155
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->getHeight()I

    move-result v1

    .line 157
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_5

    if-eq v2, v4, :cond_1

    const/4 v5, 0x2

    if-eq v2, v5, :cond_0

    const/4 v5, 0x3

    if-eq v2, v5, :cond_1

    goto/16 :goto_2

    .line 169
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    goto/16 :goto_2

    .line 174
    :cond_1
    iput-boolean v3, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    .line 175
    iget-boolean v2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 176
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    div-int/lit8 v5, p1, 0x2

    div-int/lit8 v6, v1, 0x2

    sub-int/2addr v5, v6

    int-to-float v5, v5

    const/4 v6, 0x0

    cmpl-float p2, p2, v5

    if-ltz p2, :cond_2

    .line 177
    iput-boolean v4, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    sub-int p2, p1, v1

    int-to-float p2, p2

    .line 178
    iput p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    goto :goto_0

    .line 180
    :cond_2
    iput-boolean v3, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 181
    iput v6, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    .line 183
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouch1 nowX="

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v3, ",nowStatus="

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v5, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-boolean p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    if-ne v2, p2, :cond_4

    xor-int/lit8 p2, v2, 0x1

    .line 185
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 186
    iget-boolean p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    if-eqz p2, :cond_3

    sub-int/2addr p1, v1

    int-to-float p1, p1

    .line 187
    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    goto :goto_1

    .line 189
    :cond_3
    iput v6, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    .line 192
    :cond_4
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "onTouch2 nowX="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->listener:Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;

    if-eqz p1, :cond_7

    .line 194
    iget-boolean p2, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    invoke-interface {p1, p0, p2}, Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;->OnChanged(Lcom/lltskb/lltskb/view/SwitchButton;Z)V

    goto :goto_2

    .line 159
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    int-to-float p1, p1

    cmpl-float p1, v0, p1

    if-gtz p1, :cond_8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    int-to-float v0, v1

    cmpl-float p1, p1, v0

    if-lez p1, :cond_6

    goto :goto_3

    .line 162
    :cond_6
    iput-boolean v4, p0, Lcom/lltskb/lltskb/view/SwitchButton;->onSlip:Z

    .line 163
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->downX:F

    .line 164
    iget p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->downX:F

    iput p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    .line 200
    :cond_7
    :goto_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->invalidate()V

    return v4

    :cond_8
    :goto_3
    return v3
.end method

.method public setChecked(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 221
    iget v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->DEFAULT_WIDTH:I

    int-to-float v0, v0

    iput v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 223
    iput v0, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowX:F

    .line 225
    :goto_0
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->nowStatus:Z

    .line 226
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/SwitchButton;->invalidate()V

    return-void
.end method

.method public setOnChangedListener(Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/lltskb/lltskb/view/SwitchButton;->listener:Lcom/lltskb/lltskb/view/SwitchButton$OnChangedListener;

    return-void
.end method
