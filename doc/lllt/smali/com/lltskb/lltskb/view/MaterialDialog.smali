.class public Lcom/lltskb/lltskb/view/MaterialDialog;
.super Ljava/lang/Object;
.source "MaterialDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/MaterialDialog$Builder;
    }
.end annotation


# static fields
.field private static final BUTTON_BOTTOM:I = 0x9

.field private static final BUTTON_TOP:I = 0x9


# instance fields
.field private mAlertDialog:Landroid/support/v7/app/AlertDialog;

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mBackgroundResId:I

.field private mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

.field private mCancel:Z

.field private mContext:Landroid/content/Context;

.field private mHasShow:Z

.field private mLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private mMessage:Ljava/lang/CharSequence;

.field private mMessageContentView:Landroid/view/View;

.field private mMessageContentViewResId:I

.field private mMessageResId:I

.field private mNegativeButton:Landroid/widget/Button;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mPositiveButton:Landroid/widget/Button;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleResId:I

.field private mView:Landroid/view/View;

.field private nId:I

.field nListener:Landroid/view/View$OnClickListener;

.field private nText:Ljava/lang/String;

.field private pId:I

.field pListener:Landroid/view/View$OnClickListener;

.field private pText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 47
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mHasShow:Z

    const/4 v0, -0x1

    .line 48
    iput v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundResId:I

    .line 53
    iput v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pId:I

    iput v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nId:I

    .line 59
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/support/v7/app/AlertDialog;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pId:I

    return p0
.end method

.method static synthetic access$102(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/support/v7/app/AlertDialog;)Landroid/support/v7/app/AlertDialog;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1100()Z
    .locals 1

    .line 30
    invoke-static {}, Lcom/lltskb/lltskb/view/MaterialDialog;->isLollipop()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nId:I

    return p0
.end method

.method static synthetic access$1300(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/lltskb/lltskb/view/MaterialDialog;Ljava/lang/String;)Z
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1500(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundResId:I

    return p0
.end method

.method static synthetic access$1700(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentViewResId:I

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/Context;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/lltskb/lltskb/view/MaterialDialog;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mCancel:Z

    return p0
.end method

.method static synthetic access$2100(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/content/DialogInterface$OnDismissListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-object p0
.end method

.method static synthetic access$2200(Lcom/lltskb/lltskb/view/MaterialDialog;F)I
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->dip2px(F)I

    move-result p0

    return p0
.end method

.method static synthetic access$2400(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/ListView;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mPositiveButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$302(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mPositiveButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/widget/Button;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mNegativeButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$402(Lcom/lltskb/lltskb/view/MaterialDialog;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mNegativeButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/view/MaterialDialog;)Landroid/view/View;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mTitleResId:I

    return p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mTitle:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/view/MaterialDialog;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageResId:I

    return p0
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/view/MaterialDialog;)Ljava/lang/CharSequence;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessage:Ljava/lang/CharSequence;

    return-object p0
.end method

.method private dip2px(F)I
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float p1, p1, v0

    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method private static isLollipop()Z
    .locals 2

    .line 142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isNullOrEmpty(Ljava/lang/String;)Z
    .locals 0

    if-eqz p1, :cond_1

    .line 548
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 5

    .line 553
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 560
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    const/4 v4, 0x0

    .line 561
    invoke-interface {v0, v2, v4, p1}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 562
    invoke-virtual {v4, v1, v1}, Landroid/view/View;->measure(II)V

    .line 563
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 566
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 567
    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v2

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int v2, v2, v0

    add-int/2addr v3, v2

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 568
    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    if-nez v0, :cond_0

    return-void

    .line 128
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 1

    .line 572
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getNegativeButton()Landroid/widget/Button;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mNegativeButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getPositiveButton()Landroid/widget/Button;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mPositiveButton:Landroid/widget/Button;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 108
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 109
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-object p0
.end method

.method public setBackgroundResource(I)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 117
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundResId:I

    .line 118
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz p1, :cond_0

    .line 119
    iget v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBackgroundResId:I

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setBackgroundResource(I)V

    :cond_0
    return-object p0
.end method

.method public setCanceledOnTouchOutside(Z)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 230
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mCancel:Z

    .line 231
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz p1, :cond_0

    .line 232
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mCancel:Z

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setCanceledOnTouchOutside(Z)V

    :cond_0
    return-object p0
.end method

.method public setContentView(I)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 98
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentViewResId:I

    const/4 v0, 0x0

    .line 99
    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentView:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setContentView(I)V

    :cond_0
    return-object p0
.end method

.method public setContentView(Landroid/view/View;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 83
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentView:Landroid/view/View;

    const/4 p1, 0x0

    .line 84
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentViewResId:I

    .line 85
    iget-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz p1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageContentView:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setContentView(Landroid/view/View;)V

    :cond_0
    return-object p0
.end method

.method public setMessage(I)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 165
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessageResId:I

    .line 166
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setMessage(I)V

    :cond_0
    return-object p0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 174
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mMessage:Ljava/lang/CharSequence;

    .line 175
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p0
.end method

.method public setNegativeButton(ILandroid/view/View$OnClickListener;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 0

    .line 207
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nId:I

    .line 208
    iput-object p2, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nText:Ljava/lang/String;

    .line 215
    iput-object p2, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->nListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-object p0
.end method

.method public setPositiveButton(ILandroid/view/View$OnClickListener;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 0

    .line 183
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pId:I

    .line 184
    iput-object p2, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pText:Ljava/lang/String;

    .line 201
    iput-object p2, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->pListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public setTitle(I)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 147
    iput p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mTitleResId:I

    .line 148
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setTitle(I)V

    :cond_0
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 156
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mTitle:Ljava/lang/CharSequence;

    .line 157
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p0
.end method

.method public setView(Landroid/view/View;)Lcom/lltskb/lltskb/view/MaterialDialog;
    .locals 1

    .line 74
    iput-object p1, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mView:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;->setView(Landroid/view/View;)V

    :cond_0
    return-object p0
.end method

.method public show()V
    .locals 2

    .line 64
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mHasShow:Z

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/view/MaterialDialog$Builder;-><init>(Lcom/lltskb/lltskb/view/MaterialDialog;Lcom/lltskb/lltskb/view/MaterialDialog$1;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mBuilder:Lcom/lltskb/lltskb/view/MaterialDialog$Builder;

    goto :goto_0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mAlertDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    :goto_0
    const/4 v0, 0x1

    .line 69
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/MaterialDialog;->mHasShow:Z

    return-void
.end method
