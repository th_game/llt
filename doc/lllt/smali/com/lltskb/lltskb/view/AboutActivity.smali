.class public Lcom/lltskb/lltskb/view/AboutActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "AboutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method private init()V
    .locals 3

    const v0, 0x7f0b001c

    .line 34
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->setContentView(I)V

    const v0, 0x7f09022b

    .line 35
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f090096

    .line 36
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CollapsingToolbarLayout;

    .line 37
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 38
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/AboutActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    .line 40
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    const v0, 0x7f0d002b

    .line 42
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/design/widget/CollapsingToolbarLayout;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f090305

    .line 44
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902c5

    .line 46
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 48
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const v0, 0x7f090299

    .line 51
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 53
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const v0, 0x7f09025f

    .line 56
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    const-string v1, "2019-09-25"

    .line 58
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v0, 0x7f090179

    .line 62
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 63
    new-instance v1, Lcom/lltskb/lltskb/view/AboutActivity$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/AboutActivity$1;-><init>(Lcom/lltskb/lltskb/view/AboutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static start(Landroid/content/Context;)V
    .locals 2

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/view/AboutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 29
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/AboutActivity;->init()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 75
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-eq v0, v1, :cond_0

    .line 80
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/AboutActivity;->finish()V

    const/4 p1, 0x1

    return p1
.end method
