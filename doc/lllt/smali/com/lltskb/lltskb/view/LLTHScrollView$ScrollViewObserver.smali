.class public Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;
.super Ljava/lang/Object;
.source "LLTHScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/LLTHScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScrollViewObserver"
.end annotation


# instance fields
.field mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public AddOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public NotifyOnScrollChanged(IIII)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 127
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 128
    iget-object v1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;->onScrollChanged(IIII)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public RemoveOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/view/LLTHScrollView$ScrollViewObserver;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
