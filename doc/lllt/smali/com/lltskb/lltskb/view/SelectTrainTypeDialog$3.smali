.class Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;
.super Ljava/lang/Object;
.source "SelectTrainTypeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 151
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$000(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    const-string v0, "\u5168\u90e8"

    if-eqz p1, :cond_0

    .line 153
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->string2Type(Ljava/lang/String;)I

    move-result v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$802(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;I)I

    goto/16 :goto_0

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$100(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    const-string v0, ""

    if-eqz p1, :cond_1

    .line 157
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u52a8\u8f66;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$200(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 159
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u76f4\u5feb;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$300(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 161
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u7279\u5feb;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$600(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 163
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u5feb\u901f;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$400(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 165
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u666e\u5feb;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$500(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 167
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u666e\u5ba2;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$700(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Landroid/widget/CheckBox;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 169
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u9ad8\u94c1;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 170
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_8

    return-void

    .line 173
    :cond_8
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->string2Type(Ljava/lang/String;)I

    move-result v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$802(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;I)I

    .line 175
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->dismiss()V

    .line 176
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$900(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 177
    iget-object p1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$900(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;

    move-result-object p1

    iget-object v1, p0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$3;->this$0:Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->access$800(Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;)I

    move-result v1

    invoke-interface {p1, v1, v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;->onSetTrainFilter(ILjava/lang/String;)V

    :cond_9
    return-void
.end method
