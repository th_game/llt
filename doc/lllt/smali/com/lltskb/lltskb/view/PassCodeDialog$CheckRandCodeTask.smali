.class final Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;
.super Landroid/os/AsyncTask;
.source "PassCodeDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/PassCodeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CheckRandCodeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private randCode:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/view/PassCodeDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/PassCodeDialog;Ljava/lang/String;)V
    .locals 1

    .line 185
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 186
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    .line 187
    iput-object p2, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->randCode:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4

    .line 224
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 226
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 229
    :cond_0
    new-instance v2, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$200(Lcom/lltskb/lltskb/view/PassCodeDialog;)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    :goto_0
    invoke-direct {v2, v3}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;-><init>(I)V

    .line 231
    aget-object p1, p1, v1

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->captchaCheck64(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 182
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 204
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez v0, :cond_0

    return-void

    .line 210
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    .line 211
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "\u9a8c\u8bc1\u7801\u9519\u8bef"

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 212
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$000(Lcom/lltskb/lltskb/view/PassCodeDialog;)V

    goto :goto_0

    .line 214
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$100(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 215
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$100(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;

    move-result-object p1

    iget-object v1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->randCode:Ljava/lang/String;

    invoke-interface {p1, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;->onSetPassCode(Ljava/lang/String;)V

    .line 218
    :cond_2
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->dismiss()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 182
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 191
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 192
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0901aa

    .line 196
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 198
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method
