.class public Lcom/lltskb/lltskb/view/PassImageView;
.super Landroid/support/v7/widget/AppCompatImageView;
.source "PassImageView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PassImageView"


# instance fields
.field private mCircleSize:I

.field private mEnableClick:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mPaint:Landroid/graphics/Paint;

.field private mVec:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 129
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    const/4 p1, 0x0

    .line 38
    iput p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    .line 40
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    .line 41
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 138
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    const/4 p1, 0x0

    .line 38
    iput p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    .line 40
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    .line 41
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 148
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    const/4 p1, 0x0

    .line 38
    iput p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    .line 40
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    .line 41
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method private addPoint(Landroid/graphics/Point;)V
    .locals 6

    const/4 v0, -0x1

    const v1, 0x98967f

    const/4 v2, 0x0

    .line 170
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 171
    iget-object v3, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Point;

    .line 172
    iget v4, p1, Landroid/graphics/Point;->x:I

    iget v5, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p1, Landroid/graphics/Point;->y:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v4, v3

    if-le v1, v4, :cond_0

    move v0, v2

    move v1, v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 179
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouchEvent minDist="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ",pos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PassImageView"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x50

    if-ge v1, v2, :cond_2

    if-ltz v0, :cond_2

    .line 181
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    return-void

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 186
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getRandCode()Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getRandCode()Ljava/lang/String;
    .locals 11

    .line 91
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getWidth()I

    move-result v0

    .line 92
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getHeight()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    float-to-double v4, v3

    mul-int/lit16 v6, v0, 0xbe

    int-to-double v6, v6

    const-wide v8, 0x4072500000000000L    # 293.0

    .line 96
    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v6, v8

    const/high16 v8, 0x433e0000    # 190.0f

    cmpl-double v9, v4, v6

    if-lez v9, :cond_0

    mul-float v4, v2, v8

    const v5, 0x43928000    # 293.0f

    div-float/2addr v4, v5

    move v5, v4

    move v4, v2

    goto :goto_1

    :cond_0
    mul-int/lit16 v4, v1, 0x125

    int-to-float v4, v4

    div-float/2addr v4, v8

    cmpl-float v5, v2, v4

    if-lez v5, :cond_1

    goto :goto_0

    :cond_1
    move v4, v2

    :goto_0
    move v5, v3

    :goto_1
    sub-float/2addr v2, v4

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-int v2, v2

    sub-float/2addr v3, v5

    div-float/2addr v3, v6

    float-to-int v6, v3

    .line 106
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "w="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " real_w="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, " h="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " real_h="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassImageView"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    div-float/2addr v8, v5

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 110
    :goto_2
    iget-object v7, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-ge v5, v7, :cond_5

    .line 111
    iget-object v7, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v7, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Point;

    .line 112
    iget v9, v7, Landroid/graphics/Point;->x:I

    sub-int/2addr v9, v2

    .line 113
    iget v7, v7, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v6

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    int-to-float v7, v7

    sub-float/2addr v7, v3

    mul-float v7, v7, v8

    float-to-int v7, v7

    add-int/lit8 v7, v7, -0x1e

    if-gez v7, :cond_2

    const/4 v7, 0x0

    :cond_2
    const/16 v10, 0xa0

    if-le v7, v10, :cond_3

    const/16 v7, 0xa0

    :cond_3
    const-string v10, ","

    if-lez v5, :cond_4

    .line 118
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_4
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 121
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRandCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initIcon()V
    .locals 2

    .line 52
    :try_start_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassImageView;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 54
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassImageView"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 61
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;->onDraw(Landroid/graphics/Canvas;)V

    const/4 v0, 0x0

    .line 63
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 64
    iget-object v1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 65
    iget-object v2, p0, Lcom/lltskb/lltskb/view/PassImageView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget v2, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    if-nez v2, :cond_0

    .line 68
    :try_start_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 70
    :catch_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    .line 75
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/lltskb/lltskb/view/PassImageView;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 76
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, v1, Landroid/graphics/Point;->y:I

    iget v5, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, v1, Landroid/graphics/Point;->x:I

    iget v6, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v6, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v1, v6

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 77
    iget-object v1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 79
    :cond_1
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget v3, p0, Lcom/lltskb/lltskb/view/PassImageView;->mCircleSize:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/lltskb/lltskb/view/PassImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 155
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 157
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    .line 158
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, p1}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/view/PassImageView;->addPoint(Landroid/graphics/Point;)V

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouchEvent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PassImageView"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassImageView;->invalidate()V

    return v1

    .line 164
    :cond_0
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    return p1
.end method

.method public setEnableClick(Z)V
    .locals 0

    .line 44
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mEnableClick:Z

    if-nez p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->clear()V

    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassImageView;->mVec:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 33
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
