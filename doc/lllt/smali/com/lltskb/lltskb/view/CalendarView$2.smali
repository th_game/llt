.class Lcom/lltskb/lltskb/view/CalendarView$2;
.super Ljava/lang/Object;
.source "CalendarView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarTitle()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/CalendarView;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/CalendarView;Landroid/view/View;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 199
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$000(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$008(Lcom/lltskb/lltskb/view/CalendarView;)I

    .line 204
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 205
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v2}, Lcom/lltskb/lltskb/view/CalendarView;->access$100(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 206
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$200(Lcom/lltskb/lltskb/view/CalendarView;)V

    .line 207
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    sget-object v2, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    iput-object v2, v0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 208
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iget-object v2, v0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/CalendarView;->GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 209
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$300(Lcom/lltskb/lltskb/view/CalendarView;)Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    .line 210
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$000(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v0

    const/4 v2, 0x0

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$2;->val$view:Landroid/view/View;

    const v0, 0x7f090064

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 212
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
