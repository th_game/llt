.class public Lcom/lltskb/lltskb/view/RunChartTopLayout;
.super Ljava/lang/Object;
.source "RunChartTopLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "RunChartTopLayout"


# instance fields
.field private mDate:Ljava/lang/String;

.field private mDetector:Landroid/view/GestureDetector;

.field private mIsCollapsed:Z

.field private mMaxDate:Ljava/lang/String;

.field private mMinDate:Ljava/lang/String;

.field private mResult:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private mRootView:Landroid/view/View;

.field private mRuleIndex:I

.field private mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

.field private mStation:Ljava/lang/String;

.field private mTrain:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDetector:Landroid/view/GestureDetector;

    .line 53
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    .line 54
    iput-object p4, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mMinDate:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mMaxDate:Ljava/lang/String;

    .line 57
    iput p7, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRuleIndex:I

    .line 58
    iput-object p8, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mResult:Ljava/util/List;

    .line 59
    iput-object p3, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mTrain:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDate:Ljava/lang/String;

    .line 61
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartTopLayout;->initView()V

    return-void
.end method

.method private collapse(Z)V
    .locals 5

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    .line 138
    :cond_0
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    const/4 p1, 0x5

    new-array p1, p1, [I

    .line 141
    fill-array-data p1, :array_0

    .line 144
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    aget v3, p1, v2

    .line 145
    iget-object v4, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 147
    iget-boolean v4, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    if-eqz v4, :cond_1

    const/16 v4, 0x8

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/RunChartView;->updateRunChart(Z)V

    return-void

    :array_0
    .array-data 4
        0x7f0902a4
        0x7f090071
        0x7f0902d2
        0x7f090178
        0x7f090175
    .end array-data
.end method

.method private initView()V
    .locals 13

    const-string v0, "RunChartTopLayout"

    const-string v1, "initView"

    .line 65
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    if-nez v1, :cond_0

    return-void

    :cond_0
    const v2, 0x7f090154

    .line 69
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/RunChartView;

    iput-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    .line 71
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mResult:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 72
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 75
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/16 v2, 0xe

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    .line 76
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mResult:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 77
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mResult:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 78
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mResult:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v5, 0x0

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 82
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    .line 83
    iget-object v8, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 84
    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getAuxDay()I

    move-result v5

    goto :goto_0

    :cond_4
    move v10, v5

    .line 89
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "auxdy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    const v1, 0x7f090071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    const v1, 0x7f0902fa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v5, 0x7f0d012e

    invoke-virtual {v2, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v5, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mTrain:Ljava/lang/String;

    .line 96
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 95
    invoke-static {v1, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    new-instance v1, Lcom/lltskb/lltskb/view/-$$Lambda$RunChartTopLayout$O53hUm8Ja1QlVF4mcnUZXLhRRmU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/-$$Lambda$RunChartTopLayout$O53hUm8Ja1QlVF4mcnUZXLhRRmU;-><init>(Lcom/lltskb/lltskb/view/RunChartTopLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "20160101"

    .line 101
    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDate:Ljava/lang/String;

    .line 103
    :cond_5
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDate:Ljava/lang/String;

    const-string v2, "-"

    const-string v5, ""

    invoke-static {v1, v2, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v6, "yyyyMMdd"

    invoke-direct {v2, v6, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 108
    :try_start_0
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 109
    iget-object v2, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/view/RunChartView;->setCurrentDate(Ljava/util/Date;)V

    .line 110
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 113
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 116
    :goto_2
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    const v2, 0x7f0902a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 118
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v6, "yy\u5e74MM\u6708"

    invoke-direct {v2, v6, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 120
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndex(Ljava/lang/String;)I

    move-result v12

    .line 123
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    const/16 v0, 0x4000

    const/16 v11, 0x4000

    goto :goto_3

    :cond_6
    move v11, v0

    .line 126
    :goto_3
    iget-object v6, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    iget-object v7, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mMinDate:Ljava/lang/String;

    iget-object v8, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mMaxDate:Ljava/lang/String;

    iget v9, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRuleIndex:I

    invoke-virtual/range {v6 .. v12}, Lcom/lltskb/lltskb/view/RunChartView;->setParams(Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 128
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/RunChartView;->updateRunChart(Z)V

    .line 129
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 130
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isShowRunchart()Z

    move-result v0

    xor-int/2addr v0, v4

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/RunChartTopLayout;->collapse(Z)V

    :cond_7
    return-void
.end method

.method private updateChart()V
    .locals 4

    .line 212
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    if-nez v0, :cond_0

    return-void

    .line 214
    :cond_0
    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/RunChartView;->updateRunChart(Z)V

    .line 215
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/RunChartView;->getCurrentMonth()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 217
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    .line 218
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 219
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v3, "yy\u5e74MM\u6708"

    invoke-direct {v1, v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 221
    iget-object v2, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRootView:Landroid/view/View;

    const v3, 0x7f0902a4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 223
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$RunChartTopLayout(Landroid/view/View;)V
    .locals 0

    .line 98
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mIsCollapsed:Z

    xor-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/RunChartTopLayout;->collapse(Z)V

    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mRunChartView:Lcom/lltskb/lltskb/view/RunChartView;

    const/4 p2, 0x0

    if-nez p1, :cond_0

    return p2

    :cond_0
    const/4 p4, 0x0

    cmpg-float v0, p3, p4

    if-gez v0, :cond_1

    .line 201
    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/RunChartView;->nextMonth()V

    .line 202
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartTopLayout;->updateChart()V

    goto :goto_0

    :cond_1
    cmpl-float p3, p3, p4

    if-lez p3, :cond_2

    .line 204
    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/RunChartView;->prevMonth()V

    .line 205
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartTopLayout;->updateChart()V

    :cond_2
    :goto_0
    return p2
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 161
    iget-object p1, p0, Lcom/lltskb/lltskb/view/RunChartTopLayout;->mDetector:Landroid/view/GestureDetector;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    const/4 p1, 0x1

    return p1
.end method
