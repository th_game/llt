.class public Lcom/lltskb/lltskb/view/ViewAbout;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "ViewAbout.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 812
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreate$0$ViewAbout(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 845
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showNewVersionComments(Landroid/content/Context;Z)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 823
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0b001b

    .line 825
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->setContentView(I)V

    const p1, 0x7f0d002b

    .line 826
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->setTitle(I)V

    const p1, 0x7f0902c5

    .line 828
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_0

    .line 830
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const p1, 0x7f090272

    .line 833
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_1

    .line 835
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const p1, 0x7f09025f

    .line 838
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_2

    const-string v0, "20181021"

    .line 840
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const p1, 0x7f090179

    .line 844
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/ViewAbout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 845
    new-instance v0, Lcom/lltskb/lltskb/view/-$$Lambda$ViewAbout$SXbDwf7QK43vNYVPBpdDFkn3jis;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/-$$Lambda$ViewAbout$SXbDwf7QK43vNYVPBpdDFkn3jis;-><init>(Lcom/lltskb/lltskb/view/ViewAbout;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 816
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/ViewAbout;->finish()V

    .line 817
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
