.class Lcom/lltskb/lltskb/view/RunChartView$DayHead;
.super Landroid/view/View;
.source "RunChartView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/RunChartView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DayHead"
.end annotation


# instance fields
.field private fTextSize:I

.field private mBgColor:I

.field private mText:Ljava/lang/String;

.field private mTextColor:I

.field private pt:Landroid/graphics/Paint;

.field private rect:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/lltskb/lltskb/view/RunChartView;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/view/RunChartView;Landroid/content/Context;II)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->this$0:Lcom/lltskb/lltskb/view/RunChartView;

    .line 86
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    .line 72
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    const/4 p1, -0x1

    .line 74
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mBgColor:I

    const/high16 p1, -0x1000000

    .line 75
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mTextColor:I

    const/16 p1, 0xc

    .line 87
    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->fTextSize:I

    .line 88
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p1, p3, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private drawDayHeader(Landroid/graphics/Canvas;)V
    .locals 5

    .line 113
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mBgColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 116
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 117
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->fTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 118
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    iget v2, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mTextColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mText:Ljava/lang/String;

    .line 124
    iget-object v2, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    shr-int/2addr v3, v1

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    .line 125
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    shr-int/lit8 v1, v3, 0x1

    sub-int/2addr v2, v1

    .line 126
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getHeight()I

    move-result v1

    .line 127
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getTextHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    .line 128
    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->bottom:F

    sub-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v2, v2

    int-to-float v1, v1

    .line 129
    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private getTextHeight()I
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->pt:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 101
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->rect:Landroid/graphics/RectF;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 108
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->drawDayHeader(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setBackColor(I)V
    .locals 0

    .line 92
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mBgColor:I

    return-void
.end method

.method public setFontSize(I)V
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->fTextSize:I

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mText:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->postInvalidate()V

    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    .line 96
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->mTextColor:I

    return-void
.end method
