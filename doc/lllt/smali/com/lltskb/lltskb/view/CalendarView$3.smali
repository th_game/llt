.class Lcom/lltskb/lltskb/view/CalendarView$3;
.super Ljava/lang/Object;
.source "CalendarView.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/CalendarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/CalendarView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/CalendarView;)V
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnClick(Lcom/lltskb/lltskb/view/DateWidgetDayCell;)V
    .locals 5

    .line 415
    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isEnableDays()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x1

    .line 420
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setSelected(Z)V

    .line 421
    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/CalendarView;->access$300(Lcom/lltskb/lltskb/view/CalendarView;)Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    .line 422
    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iget-object v1, v1, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isEnableDays()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 423
    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iget-object v1, v1, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    iget-object v2, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v2}, Lcom/lltskb/lltskb/view/CalendarView;->access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v2}, Lcom/lltskb/lltskb/view/CalendarView;->access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/lltskb/lltskb/view/CalendarView$3;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v3}, Lcom/lltskb/lltskb/view/CalendarView;->access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHoliday()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v0, v2, v3, p1}, Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;->onDateSet(IIILjava/lang/String;)V

    :cond_1
    return-void
.end method
