.class Lcom/lltskb/lltskb/view/WebBrowser$12;
.super Landroid/os/AsyncTask;
.source "WebBrowser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/WebBrowser;->startDownload(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private apkName:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field final synthetic this$0:Lcom/lltskb/lltskb/view/WebBrowser;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 722
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    .line 724
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->path:Ljava/lang/String;

    .line 725
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->apkName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1300(Lcom/lltskb/lltskb/view/WebBrowser$12;)Ljava/lang/String;
    .locals 0

    .line 722
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->path:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/lltskb/lltskb/view/WebBrowser$12;)Ljava/lang/String;
    .locals 0

    .line 722
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->apkName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/lltskb/lltskb/view/WebBrowser$12;[Ljava/lang/Object;)V
    .locals 0

    .line 722
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser$12;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 722
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser$12;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    const/4 v0, 0x0

    .line 730
    aget-object p1, p1, v0

    .line 731
    new-instance v0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v0}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    .line 732
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/view/WebBrowser;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->path:Ljava/lang/String;

    const/16 v1, 0x2f

    .line 733
    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->apkName:Ljava/lang/String;

    const-string v1, "soft.apk"

    .line 734
    iput-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->apkName:Ljava/lang/String;

    .line 736
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->path:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->apkName:Ljava/lang/String;

    new-instance v3, Lcom/lltskb/lltskb/view/WebBrowser$12$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/view/WebBrowser$12$1;-><init>(Lcom/lltskb/lltskb/view/WebBrowser$12;)V

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 751
    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 752
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser$12;->isCancelled()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "\u53d6\u6d88\u4e86"

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected onCancelled(Ljava/lang/Object;)V
    .locals 1

    .line 787
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    const-string v0, "\u60a8\u53d6\u6d88\u4e86\u4e0b\u8f7d"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1800(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .line 766
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1700(Lcom/lltskb/lltskb/view/WebBrowser;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    .line 767
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1800(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)V

    return-void

    .line 772
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 760
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1600(Lcom/lltskb/lltskb/view/WebBrowser;)V

    .line 761
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 3

    const/4 v0, 0x0

    .line 777
    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 778
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1700(Lcom/lltskb/lltskb/view/WebBrowser;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setProgress(I)V

    .line 779
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1700(Lcom/lltskb/lltskb/view/WebBrowser;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMax(I)V

    .line 780
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$12;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1700(Lcom/lltskb/lltskb/view/WebBrowser;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u5df2\u7ecf\u4e0b\u8f7d\u4e86"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 781
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method
