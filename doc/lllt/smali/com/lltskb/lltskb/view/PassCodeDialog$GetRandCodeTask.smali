.class final Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;
.super Landroid/os/AsyncTask;
.source "PassCodeDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/PassCodeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GetRandCodeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/view/PassCodeDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/PassCodeDialog;)V
    .locals 1

    .line 239
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 240
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 237
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .line 299
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 303
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$200(Lcom/lltskb/lltskb/view/PassCodeDialog;)I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    :goto_0
    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;-><init>(I)V

    .line 305
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->getCaptchaImage64()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled(Ljava/lang/Object;)V
    .locals 1

    .line 247
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez p1, :cond_0

    return-void

    .line 252
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "\u7528\u6237\u53d6\u6d88\u4e86"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .line 275
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0901aa

    .line 280
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/16 v2, 0x8

    .line 282
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const/4 v1, 0x0

    if-nez p1, :cond_2

    .line 285
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object p1

    const v2, 0x7f0800c8

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/view/PassImageView;->setImageResource(I)V

    .line 286
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d010f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0d0089

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 289
    :cond_2
    instance-of v2, p1, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 290
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object v2

    move-object v3, p1

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/PassImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 291
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/PassImageView;->setEnableClick(Z)V

    .line 293
    :cond_3
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$402(Lcom/lltskb/lltskb/view/PassCodeDialog;Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;)Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    .line 294
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0901aa

    .line 262
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 264
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 266
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/PassImageView;->setEnableClick(Z)V

    .line 267
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object v1

    const v2, 0xcdcdcd

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/PassImageView;->setBackgroundColor(I)V

    .line 268
    invoke-static {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 270
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
