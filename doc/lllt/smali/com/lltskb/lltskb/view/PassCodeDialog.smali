.class public Lcom/lltskb/lltskb/view/PassCodeDialog;
.super Landroid/support/v7/app/AppCompatDialog;
.source "PassCodeDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;,
        Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;,
        Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;
    }
.end annotation


# static fields
.field public static final MODE_LOGIN:I = 0x1

.field public static final MODE_ORDER:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PassCodeDialog"


# instance fields
.field private mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

.field private mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

.field private mImageView:Lcom/lltskb/lltskb/view/PassImageView;

.field private mListener:Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;

.field private mMode:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    .line 114
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    .line 115
    iput p3, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mMode:I

    .line 116
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/PassCodeDialog;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getLoginRandCode()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mListener:Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/PassCodeDialog;)I
    .locals 0

    .line 25
    iget p0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mMode:I

    return p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/PassCodeDialog;)Lcom/lltskb/lltskb/view/PassImageView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    return-object p0
.end method

.method static synthetic access$402(Lcom/lltskb/lltskb/view/PassCodeDialog;Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;)Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    return-object p1
.end method

.method private checkRandCode(Ljava/lang/String;)V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    return-void

    .line 124
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;-><init>(Lcom/lltskb/lltskb/view/PassCodeDialog;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    .line 126
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getLoginRandCode()V
    .locals 5

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getLoginRandCode mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassCodeDialog"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    return-void

    .line 177
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;-><init>(Lcom/lltskb/lltskb/view/PassCodeDialog;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    .line 179
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private initView()V
    .locals 2

    const v0, 0x7f0b0075

    .line 130
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->setContentView(I)V

    const v0, 0x7f0901a6

    .line 131
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/PassImageView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassImageView;->initIcon()V

    :cond_0
    const v0, 0x7f0902ce

    .line 136
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138
    new-instance v1, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$wM_OK3eA4966A5r1mf5c8kFn0N8;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$wM_OK3eA4966A5r1mf5c8kFn0N8;-><init>(Lcom/lltskb/lltskb/view/PassCodeDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f0902ae

    .line 141
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    new-instance v1, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$Biu3eKVorbGF-hqe_9dDdYs0rb4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$Biu3eKVorbGF-hqe_9dDdYs0rb4;-><init>(Lcom/lltskb/lltskb/view/PassCodeDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v0, 0x7f090261

    .line 146
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 148
    new-instance v1, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$Pyc4K4z_xCU87uS5L9TAGrLk78E;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/-$$Lambda$PassCodeDialog$Pyc4K4z_xCU87uS5L9TAGrLk78E;-><init>(Lcom/lltskb/lltskb/view/PassCodeDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getLoginRandCode()V

    return-void
.end method


# virtual methods
.method public getRandCode()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassImageView;->getRandCode()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic lambda$initView$0$PassCodeDialog(Landroid/view/View;)V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getLoginRandCode()V

    return-void
.end method

.method public synthetic lambda$initView$1$PassCodeDialog(Landroid/view/View;)V
    .locals 0

    .line 143
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getRandCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->checkRandCode(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$2$PassCodeDialog(Landroid/view/View;)V
    .locals 2

    .line 149
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object p1

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq p1, v1, :cond_0

    .line 150
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mCheckRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog$CheckRandCodeTask;->cancel(Z)Z

    .line 153
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object p1

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq p1, v1, :cond_1

    .line 154
    iget-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mGetRandCodeTask:Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/PassCodeDialog$GetRandCodeTask;->cancel(Z)Z

    .line 157
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->dismiss()V

    return-void
.end method

.method public setListener(Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mListener:Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;

    return-void
.end method

.method public show()V
    .locals 7

    const-string v0, "PassCodeDialog"

    .line 30
    :try_start_0
    invoke-super {p0}, Landroid/support/v7/app/AppCompatDialog;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 37
    iget-object v1, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/4 v4, 0x0

    if-eqz v1, :cond_3

    .line 38
    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/PassImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_0

    .line 40
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 43
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getScreenHeight(Landroid/content/Context;)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    div-int/lit8 v5, v5, 0x3

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 44
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    mul-int/lit16 v5, v5, 0x125

    div-int/lit16 v5, v5, 0xbe

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 45
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 46
    iget-object v6, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    invoke-virtual {v6, v1}, Lcom/lltskb/lltskb/view/PassImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getScreenWidth(Landroid/content/Context;)I

    move-result v5

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 50
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isTabletModel(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 51
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    mul-int/lit8 v5, v5, 0x8

    div-int/lit8 v5, v5, 0xa

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 53
    :cond_2
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    mul-int/lit16 v5, v5, 0xbe

    div-int/lit16 v5, v5, 0x125

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 54
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 55
    iget-object v6, p0, Lcom/lltskb/lltskb/view/PassCodeDialog;->mImageView:Lcom/lltskb/lltskb/view/PassImageView;

    invoke-virtual {v6, v1}, Lcom/lltskb/lltskb/view/PassImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    .line 59
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_4

    return-void

    .line 65
    :cond_4
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 66
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 68
    :cond_5
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    if-nez v4, :cond_6

    return-void

    .line 73
    :cond_6
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isTabletModel(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 74
    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 75
    iput v3, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_1

    .line 77
    :cond_7
    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 78
    iput v3, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 81
    :goto_1
    :try_start_1
    invoke-virtual {v1, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    .line 83
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :catchall_1
    move-exception v1

    .line 32
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
