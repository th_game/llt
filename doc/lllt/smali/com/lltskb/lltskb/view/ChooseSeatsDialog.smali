.class public Lcom/lltskb/lltskb/view/ChooseSeatsDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ChooseSeatsDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ChooseSeatsDialog"


# instance fields
.field private alertDialog:Landroid/support/v7/app/AlertDialog;

.field private btnIds:[I

.field private choose_seats:Ljava/lang/String;

.field private curSelected:I

.field private firstLine:Landroid/view/View;

.field private lastButton:Landroid/widget/CompoundButton;

.field private listener:Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;

.field private seatCount:I

.field private seatName:Ljava/lang/String;

.field private seatType:Ljava/lang/String;

.field private secondLine:Landroid/view/View;

.field private ticketCount:I

.field private tvHint:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 40
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->btnIds:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f09003f
        0x7f090043
        0x7f090048
        0x7f09004c
        0x7f090052
    .end array-data
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    return p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)Landroid/widget/CompoundButton;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->lastButton:Landroid/widget/CompoundButton;

    return-object p0
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;Landroid/widget/CompoundButton;)Landroid/widget/CompoundButton;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->lastButton:Landroid/widget/CompoundButton;

    return-object p1
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->updateCurrentChecked()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->onDialogShow()V

    return-void
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->onSeatsSelected()V

    return-void
.end method

.method private hideSeatView(II)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->firstLine:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->firstLine:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 168
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 171
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 173
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return-void
.end method

.method public static newInstance(IILjava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;)Lcom/lltskb/lltskb/view/ChooseSeatsDialog;
    .locals 1

    .line 55
    new-instance v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;

    invoke-direct {v0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;-><init>()V

    .line 56
    iput p0, v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    .line 57
    iput p1, v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->ticketCount:I

    .line 58
    iput-object p2, v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatName:Ljava/lang/String;

    .line 59
    iput-object p4, v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->listener:Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;

    .line 60
    iput-object p3, v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatType:Ljava/lang/String;

    return-object v0
.end method

.method private onDialogShow()V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 182
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$4;-><init>(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private onSeatsSelected()V
    .locals 2

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSeatsSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChooseSeatsDialog"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    iget v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    if-eq v0, v1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00a5

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->listener:Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;

    if-eqz v0, :cond_1

    .line 197
    iget-object v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;->onChooseSeats(Ljava/lang/String;)V

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->dismiss()V

    return-void
.end method

.method private setCheckedListners(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 4

    .line 226
    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->btnIds:[I

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, v0, v2

    .line 227
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    .line 228
    invoke-virtual {v3, p2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateCurrentChecked()V
    .locals 8

    const/4 v0, 0x0

    .line 203
    iput v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    const-string v1, ""

    .line 204
    iput-object v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    .line 205
    iget-object v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->btnIds:[I

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v2, :cond_1

    aget v5, v1, v3

    .line 206
    iget-object v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->firstLine:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ToggleButton;

    .line 207
    invoke-virtual {v5}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 208
    iget v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    .line 209
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "1"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Landroid/widget/ToggleButton;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 213
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->btnIds:[I

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_3

    aget v5, v1, v3

    .line 214
    iget-object v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ToggleButton;

    .line 215
    invoke-virtual {v5}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 216
    iget v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    .line 217
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "2"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Landroid/widget/ToggleButton;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const v1, 0x7f0d00a6

    .line 220
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->tvHint:Landroid/widget/TextView;

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    iget v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-static {v3, v1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 234
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onClick choose="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->choose_seats:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ChooseSeatsDialog"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, -0x2

    if-ne p2, p1, :cond_0

    .line 236
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->listener:Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;

    if-eqz p1, :cond_0

    const-string p2, ""

    .line 237
    invoke-interface {p1, p2}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;->onChooseSeats(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .line 67
    new-instance p1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0d00a3

    .line 68
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v1, 0x0

    .line 69
    invoke-virtual {p1, v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0d01e2

    .line 70
    invoke-virtual {p1, v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 73
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0b0028

    .line 74
    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 76
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    const v2, 0x7f0900d3

    .line 77
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->firstLine:Landroid/view/View;

    const v2, 0x7f0901e0

    .line 78
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    .line 80
    iget v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 81
    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatType:Ljava/lang/String;

    if-eqz v2, :cond_2

    const-string v4, "P"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatType:Ljava/lang/String;

    const-string v4, "9"

    .line 86
    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 88
    :goto_0
    iget-object v4, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatType:Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string v5, "M"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    if-eqz v2, :cond_5

    :cond_4
    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_6

    const v4, 0x7f090043

    const v5, 0x7f09024e

    .line 91
    invoke-direct {p0, v4, v5}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->hideSeatView(II)V

    :cond_6
    if-eqz v2, :cond_7

    const v2, 0x7f09004c

    const v4, 0x7f090270

    .line 95
    invoke-direct {p0, v2, v4}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->hideSeatView(II)V

    :cond_7
    const v2, 0x7f0d00a6

    .line 98
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f090264

    .line 99
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->tvHint:Landroid/widget/TextView;

    .line 100
    iget-object v4, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->tvHint:Landroid/widget/TextView;

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Object;

    iget v8, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->curSelected:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v0

    iget v8, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v5, v2, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0d00a7

    .line 102
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f090297

    .line 103
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 105
    iget v4, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->ticketCount:I

    if-nez v4, :cond_8

    .line 106
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatName:Ljava/lang/String;

    aput-object v6, v5, v0

    const-string v6, "\u5f88\u591a"

    aput-object v6, v5, v3

    invoke-static {v4, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 108
    :cond_8
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->seatName:Ljava/lang/String;

    aput-object v6, v5, v0

    iget v6, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->ticketCount:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :goto_2
    new-instance v1, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$1;-><init>(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V

    .line 130
    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->firstLine:Landroid/view/View;

    invoke-direct {p0, v2, v1}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->setCheckedListners(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 131
    iget-object v2, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->secondLine:Landroid/view/View;

    invoke-direct {p0, v2, v1}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->setCheckedListners(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 133
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    .line 134
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 135
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    new-instance v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$2;-><init>(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 146
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    new-instance v0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog$3;-><init>(Lcom/lltskb/lltskb/view/ChooseSeatsDialog;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 152
    iget-object p1, p0, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->alertDialog:Landroid/support/v7/app/AlertDialog;

    return-object p1
.end method

.method public show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 0

    .line 244
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    .line 245
    invoke-virtual {p1, p0, p2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 246
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method
