.class Lcom/lltskb/lltskb/view/WebBrowser$8;
.super Landroid/webkit/WebViewClient;
.source "WebBrowser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/WebBrowser;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/WebBrowser;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    .line 339
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 383
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$600(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 384
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/WebBrowser;->access$600(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    const-string v0, "qunar.com"

    .line 394
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    const-string p2, "javascript:var removead = function(){var _close = document.getElementById(\'closeClient\');if( _close != null ) _close.click();var _allDiv = document.getElementsByTagName(\'div\');for(var i = 0 ;_allDiv != null && i < _allDiv.length;i++){ if( _allDiv[i].className == \'ad\' ){ _allDiv[i].style.display=\'none\';};}};removead();"

    .line 396
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 358
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 360
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "onPageStarted="

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p3, "WebBrowser"

    invoke-static {p3, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "https://kyfw.12306.cn/otn/index/init"

    .line 361
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 362
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->access$500(Lcom/lltskb/lltskb/view/WebBrowser;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 363
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 372
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    .line 331
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 2

    .line 464
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 465
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser$8;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 467
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z

    move-result p1

    return p1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .line 415
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shouldOverrideUrlLoading url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WebBrowser"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    :cond_0
    const-string v1, ".apk"

    .line 418
    invoke-virtual {p2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const-string p1, ".lltskb.com"

    .line 419
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "qunar"

    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 420
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$700(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)V

    :cond_2
    return v2

    :cond_3
    const-string v1, "mailto:"

    .line 424
    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "geo:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "tel:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "sms:"

    .line 425
    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    .line 438
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v1, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$800(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    return v2

    .line 442
    :cond_5
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v1, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$900(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    return v2

    .line 446
    :cond_6
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v1, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1000(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    return v2

    .line 450
    :cond_7
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {v1, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$1100(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    return v2

    .line 457
    :cond_8
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->access$202(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)Ljava/lang/String;

    return v0

    .line 426
    :cond_9
    :goto_0
    new-instance p1, Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p1, v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 428
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 429
    iget-object p2, p0, Lcom/lltskb/lltskb/view/WebBrowser$8;->this$0:Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 432
    invoke-virtual {p1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    :cond_a
    :goto_1
    return v2
.end method
