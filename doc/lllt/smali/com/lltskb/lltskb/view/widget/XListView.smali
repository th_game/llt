.class public Lcom/lltskb/lltskb/view/widget/XListView;
.super Landroid/widget/ListView;
.source "XListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;,
        Lcom/lltskb/lltskb/view/widget/XListView$OnXScrollListener;
    }
.end annotation


# static fields
.field private static final OFFSET_RADIO:F = 1.8f

.field private static final PULL_LOAD_MORE_DELTA:I = 0x32

.field private static final SCROLL_BACK_FOOTER:I = 0x1

.field private static final SCROLL_BACK_HEADER:I = 0x0

.field private static final SCROLL_DURATION:I = 0x190


# instance fields
.field private mEnableAutoLoad:Z

.field private mEnablePullLoad:Z

.field private mEnablePullRefresh:Z

.field private mFooterLayout:Landroid/widget/LinearLayout;

.field private mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

.field private mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

.field private mHeaderContent:Landroid/widget/RelativeLayout;

.field private mHeaderHeight:I

.field private mHeaderTime:Landroid/widget/TextView;

.field private mIsFooterReady:Z

.field private mLastY:F

.field private mListener:Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;

.field private mPullLoading:Z

.field private mPullRefreshing:Z

.field private mScrollBack:I

.field private mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mScroller:Landroid/widget/Scroller;

.field private mTotalItemCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 78
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x40800000    # -1.0f

    .line 45
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    const/4 v0, 0x0

    .line 65
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mIsFooterReady:Z

    const/4 v1, 0x1

    .line 67
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    .line 68
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 70
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    .line 71
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnableAutoLoad:Z

    .line 72
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 79
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p2, -0x40800000    # -1.0f

    .line 45
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    const/4 p2, 0x0

    .line 65
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mIsFooterReady:Z

    const/4 v0, 0x1

    .line 67
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    .line 68
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 70
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    .line 71
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnableAutoLoad:Z

    .line 72
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 84
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p2, -0x40800000    # -1.0f

    .line 45
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    const/4 p2, 0x0

    .line 65
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mIsFooterReady:Z

    const/4 p3, 0x1

    .line 67
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    .line 68
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 70
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    .line 71
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnableAutoLoad:Z

    .line 72
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 89
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/view/widget/XListView;I)I
    .locals 0

    .line 31
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    return p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/widget/XListView;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderContent:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/widget/XListView;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->startLoadMore()V

    return-void
.end method

.method private initWithContext(Landroid/content/Context;)V
    .locals 2

    .line 93
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    .line 94
    invoke-super {p0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 97
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const v1, 0x7f0900ea

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderContent:Landroid/widget/RelativeLayout;

    .line 99
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const v1, 0x7f0900ec

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderTime:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->addHeaderView(Landroid/view/View;)V

    .line 103
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    .line 104
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterLayout:Landroid/widget/LinearLayout;

    .line 105
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0x11

    .line 107
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0, v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 113
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XListView$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/widget/XListView$1;-><init>(Lcom/lltskb/lltskb/view/widget/XListView;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    return-void
.end method

.method private invokeOnScrolling()V
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    instance-of v1, v0, Lcom/lltskb/lltskb/view/widget/XListView$OnXScrollListener;

    if-eqz v1, :cond_0

    .line 254
    check-cast v0, Lcom/lltskb/lltskb/view/widget/XListView$OnXScrollListener;

    .line 255
    invoke-interface {v0, p0}, Lcom/lltskb/lltskb/view/widget/XListView$OnXScrollListener;->onXScrolling(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private loadMore()V
    .locals 1

    .line 434
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mListener:Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;

    if-eqz v0, :cond_0

    .line 435
    invoke-interface {v0}, Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;->onLoadMore()V

    :cond_0
    return-void
.end method

.method private refresh()V
    .locals 1

    .line 428
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mListener:Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;

    if-eqz v0, :cond_0

    .line 429
    invoke-interface {v0}, Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;->onRefresh()V

    :cond_0
    return-void
.end method

.method private resetFooterHeight()V
    .locals 7

    .line 315
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 318
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollBack:I

    .line 319
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v4, 0x0

    neg-int v5, v3

    const/16 v6, 0x190

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 320
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->invalidate()V

    :cond_0
    return-void
.end method

.method private resetHeaderHeight()V
    .locals 7

    .line 276
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v3

    if-nez v3, :cond_0

    return-void

    .line 280
    :cond_0
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    if-gt v3, v0, :cond_1

    return-void

    .line 285
    :cond_1
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    if-le v3, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 289
    :goto_0
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollBack:I

    .line 290
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v4, 0x0

    sub-int v5, v0, v3

    const/16 v6, 0x190

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 293
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->invalidate()V

    return-void
.end method

.method private startLoadMore()V
    .locals 2

    const/4 v0, 0x1

    .line 325
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 326
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 327
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->loadMore()V

    return-void
.end method

.method private updateFooterHeight(F)V
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v0

    float-to-int p1, p1

    add-int/2addr v0, p1

    .line 299
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    if-nez p1, :cond_1

    const/16 p1, 0x32

    if-le v0, p1, :cond_0

    .line 302
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    goto :goto_0

    .line 304
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 308
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    return-void
.end method

.method private updateHeaderHeight(F)V
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    float-to-int p1, p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v1

    add-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    .line 262
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    if-nez p1, :cond_1

    .line 264
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result p1

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    if-le p1, v1, :cond_0

    .line 265
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    goto :goto_0

    .line 267
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 272
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setSelection(I)V

    return-void
.end method


# virtual methods
.method public autoRefresh()V
    .locals 3

    .line 236
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    .line 238
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    if-nez v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v0

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    if-le v0, v2, :cond_0

    .line 241
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    goto :goto_0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 247
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 248
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 249
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->refresh()V

    return-void
.end method

.method public computeScroll()V
    .locals 2

    .line 385
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollBack:I

    if-nez v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    goto :goto_0

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    .line 392
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->postInvalidate()V

    .line 393
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->invokeOnScrolling()V

    .line 396
    :cond_1
    invoke-super {p0}, Landroid/widget/ListView;->computeScroll()V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .line 421
    iput p4, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mTotalItemCount:I

    .line 422
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 423
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 407
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    if-nez p2, :cond_1

    .line 411
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnableAutoLoad:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getLastVisiblePosition()I

    move-result p1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getCount()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    if-ne p1, p2, :cond_1

    .line 412
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->startLoadMore()V

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 332
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 333
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    .line 336
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v2, :cond_4

    .line 360
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    .line 361
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_2

    .line 363
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderHeight:I

    if-le v0, v1, :cond_1

    .line 364
    iput-boolean v3, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 365
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 366
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->refresh()V

    .line 369
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->resetHeaderHeight()V

    goto :goto_0

    .line 371
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getLastVisiblePosition()I

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mTotalItemCount:I

    sub-int/2addr v1, v3

    if-ne v0, v1, :cond_9

    .line 373
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_3

    .line 374
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->startLoadMore()V

    .line 376
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->resetFooterHeight()V

    goto :goto_0

    .line 342
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    sub-float/2addr v0, v1

    .line 343
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    .line 345
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getFirstVisiblePosition()I

    move-result v1

    const/4 v2, 0x0

    const v4, 0x3fe66666    # 1.8f

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v1

    if-gtz v1, :cond_5

    cmpl-float v1, v0, v2

    if-lez v1, :cond_6

    :cond_5
    div-float/2addr v0, v4

    .line 348
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->updateHeaderHeight(F)V

    .line 349
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->invokeOnScrolling()V

    goto :goto_0

    .line 351
    :cond_6
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->getLastVisiblePosition()I

    move-result v1

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mTotalItemCount:I

    sub-int/2addr v5, v3

    if-ne v1, v5, :cond_9

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    .line 352
    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v1

    if-gtz v1, :cond_7

    cmpg-float v1, v0, v2

    if-gez v1, :cond_9

    :cond_7
    neg-float v0, v0

    div-float/2addr v0, v4

    .line 354
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->updateFooterHeight(F)V

    goto :goto_0

    .line 338
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mLastY:F

    .line 380
    :cond_9
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .line 31
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .line 136
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mIsFooterReady:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 137
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mIsFooterReady:Z

    .line 138
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->addFooterView(Landroid/view/View;)V

    .line 141
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAutoLoadEnable(Z)V
    .locals 0

    .line 191
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnableAutoLoad:Z

    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .line 401
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method public setPullLoadEnable(Z)V
    .locals 1

    .line 162
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    .line 164
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullLoad:Z

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 165
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    .line 166
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->hide()V

    .line 167
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0, v0, v0, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setPadding(IIII)V

    .line 168
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 171
    :cond_0
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 172
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0, v0, v0, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setPadding(IIII)V

    .line 173
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->show()V

    .line 174
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 176
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    new-instance v0, Lcom/lltskb/lltskb/view/widget/XListView$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/widget/XListView$2;-><init>(Lcom/lltskb/lltskb/view/widget/XListView;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method public setPullRefreshEnable(Z)V
    .locals 1

    .line 150
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mEnablePullRefresh:Z

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderContent:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method

.method public setRefreshTime(Ljava/lang/String;)V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mHeaderTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mListener:Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;

    return-void
.end method

.method public stopLoadMore()V
    .locals 2

    .line 208
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 209
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullLoading:Z

    .line 210
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    :cond_0
    return-void
.end method

.method public stopRefresh()V
    .locals 1

    .line 198
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 199
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XListView;->mPullRefreshing:Z

    .line 200
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XListView;->resetHeaderHeight()V

    :cond_0
    return-void
.end method
