.class public Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;
.super Landroid/app/AlertDialog;
.source "DateTimePickerDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/DatePicker$OnDateChangedListener;
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;
    }
.end annotation


# static fields
.field private static final DAY:Ljava/lang/String; = "day"

.field private static final HOUR_OF_DAY:Ljava/lang/String; = "hourOfDay"

.field private static final MINUTE:Ljava/lang/String; = "minute"

.field private static final MONTH:Ljava/lang/String; = "month"

.field private static final YEAR:Ljava/lang/String; = "year"


# instance fields
.field private final mCalendar:Ljava/util/Calendar;

.field private final mDatePicker:Landroid/widget/DatePicker;

.field private final mDateTimeCallBack:Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;

.field private mInitialDay:I

.field private mInitialHour:I

.field private mInitialMinute:I

.field private mInitialMonth:I

.field private mInitialYear:I

.field private final mTimePicker:Landroid/widget/TimePicker;

.field private final mTitleDateFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;Ljava/util/Date;Z)V
    .locals 6

    .line 62
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 64
    iput-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDateTimeCallBack:Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;

    .line 66
    new-instance p2, Ljava/util/GregorianCalendar;

    invoke-direct {p2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 67
    invoke-virtual {p2, p3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    const/4 p3, 0x1

    .line 69
    invoke-virtual {p2, p3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result p3

    iput p3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialYear:I

    const/4 p3, 0x2

    .line 70
    invoke-virtual {p2, p3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMonth:I

    const/4 v0, 0x5

    .line 71
    invoke-virtual {p2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialDay:I

    const/16 v0, 0xb

    .line 72
    invoke-virtual {p2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialHour:I

    const/16 v0, 0xc

    .line 73
    invoke-virtual {p2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result p2

    iput p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMinute:I

    const/4 p2, 0x3

    .line 75
    invoke-static {p3, p2}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTitleDateFormat:Ljava/text/DateFormat;

    .line 77
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    .line 78
    iget v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialYear:I

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMonth:I

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialDay:I

    iget v4, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialHour:I

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMinute:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->updateTitle(IIIII)V

    const p2, 0x104000a

    .line 80
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, -0x1

    invoke-virtual {p0, p3, p2, p0}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/high16 p2, 0x1040000

    .line 81
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    move-object v0, p3

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    const/4 v1, -0x2

    invoke-virtual {p0, v1, p2, v0}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const-string p2, "layout_inflater"

    .line 83
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    const p2, 0x7f0b002d

    .line 84
    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 85
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setView(Landroid/view/View;)V

    const p2, 0x7f09009f

    .line 87
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/DatePicker;

    iput-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    .line 88
    iget-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    iget p3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialYear:I

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMonth:I

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialDay:I

    invoke-virtual {p2, p3, v0, v1, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    const p2, 0x7f090222

    .line 90
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TimePicker;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    .line 91
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 92
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    iget p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialHour:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 93
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    iget p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMinute:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 94
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-virtual {p1, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    return-void
.end method

.method private updateTitle(IIIII)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 113
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    .line 114
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    const/4 p2, 0x5

    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->set(II)V

    .line 115
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    const/16 p2, 0xb

    invoke-virtual {p1, p2, p4}, Ljava/util/Calendar;->set(II)V

    .line 116
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    const/16 p2, 0xc

    invoke-virtual {p1, p2, p5}, Ljava/util/Calendar;->set(II)V

    .line 118
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTitleDateFormat:Ljava/text/DateFormat;

    iget-object p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .line 98
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDateTimeCallBack:Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;

    if-eqz p1, :cond_0

    .line 99
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {p1}, Landroid/widget/DatePicker;->clearFocus()V

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDateTimeCallBack:Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    .line 103
    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    .line 104
    invoke-virtual {p1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    .line 105
    invoke-virtual {p1}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    .line 106
    invoke-virtual {p1}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    .line 107
    invoke-virtual {p1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 100
    invoke-interface/range {v0 .. v7}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog$OnDateTimeSetListener;->onDateTimeSet(Landroid/widget/DatePicker;Landroid/widget/TimePicker;IIIII)V

    :cond_0
    return-void
.end method

.method public onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 6

    .line 150
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialYear:I

    .line 151
    iput p3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMonth:I

    .line 152
    iput p4, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialDay:I

    .line 153
    iget v4, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialHour:I

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMinute:I

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->updateTitle(IIIII)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 7

    .line 135
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "year"

    .line 136
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "month"

    .line 137
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "day"

    .line 138
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "hourOfDay"

    .line 139
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v0, "minute"

    .line 140
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 141
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {p1, v2, v3, v4, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 142
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 143
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    move-object v1, p0

    .line 144
    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->updateTitle(IIIII)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .line 124
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    const-string v2, "year"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 126
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    const-string v2, "month"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v1

    const-string v2, "day"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "hourOfDay"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 129
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "minute"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 6

    .line 158
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialHour:I

    .line 159
    iput p3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMinute:I

    .line 160
    iget v1, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialYear:I

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialMonth:I

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mInitialDay:I

    move-object v0, p0

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->updateTitle(IIIII)V

    return-void
.end method

.method public setMaxDate(Ljava/util/Date;)V
    .locals 3

    .line 168
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/DatePicker;->setMaxDate(J)V

    return-void
.end method

.method public setMinDate(Ljava/util/Date;)V
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/DateTimePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/DatePicker;->setMinDate(J)V

    return-void
.end method
