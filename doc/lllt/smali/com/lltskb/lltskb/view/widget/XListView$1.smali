.class Lcom/lltskb/lltskb/view/widget/XListView$1;
.super Ljava/lang/Object;
.source "XListView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/widget/XListView;->initWithContext(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/widget/XListView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/widget/XListView;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XListView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->access$100(Lcom/lltskb/lltskb/view/widget/XListView;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->access$002(Lcom/lltskb/lltskb/view/widget/XListView;I)I

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XListView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 122
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 123
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    return-void
.end method
