.class Lcom/lltskb/lltskb/view/widget/XScrollView$1;
.super Ljava/lang/Object;
.source "XScrollView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/widget/XScrollView;->initWithContext(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/widget/XScrollView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/widget/XScrollView;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XScrollView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->access$100(Lcom/lltskb/lltskb/view/widget/XScrollView;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->access$002(Lcom/lltskb/lltskb/view/widget/XScrollView;I)I

    .line 124
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView$1;->this$0:Lcom/lltskb/lltskb/view/widget/XScrollView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 126
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 127
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 129
    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    return-void
.end method
