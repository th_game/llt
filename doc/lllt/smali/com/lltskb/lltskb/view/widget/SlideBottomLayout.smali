.class public Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;
.super Landroid/widget/LinearLayout;
.source "SlideBottomLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SlideBottomLayout"


# instance fields
.field private arriveTop:Z

.field private childView:Landroid/view/View;

.field private downY:I

.field private hideWeight:F

.field private mScroller:Landroid/widget/Scroller;

.field private moveY:I

.field private movedDis:I

.field private movedMaxDis:I

.field private shortSlideListener:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;

.field private visibilityHeight:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/high16 p1, 0x3e800000    # 0.25f

    .line 68
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hideWeight:F

    const/4 p1, 0x0

    .line 81
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    const/high16 p1, 0x43480000    # 200.0f

    .line 86
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 97
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x3e800000    # 0.25f

    .line 68
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hideWeight:F

    const/4 v0, 0x0

    .line 81
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    const/high16 v0, 0x43480000    # 200.0f

    .line 86
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    .line 98
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->initAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 102
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p3, 0x3e800000    # 0.25f

    .line 68
    iput p3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hideWeight:F

    const/4 p3, 0x0

    .line 81
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    const/high16 p3, 0x43480000    # 200.0f

    .line 86
    iput p3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    .line 103
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->initAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private initAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0700c8

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    iput p2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    .line 118
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->initConfig(Landroid/content/Context;)V

    return-void
.end method

.method private initConfig(Landroid/content/Context;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    :cond_0
    return-void
.end method


# virtual methods
.method public arriveTop()Z
    .locals 1

    .line 301
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    return v0
.end method

.method public computeScroll()V
    .locals 2

    .line 180
    invoke-super {p0}, Landroid/widget/LinearLayout;->computeScroll()V

    .line 181
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->scrollTo(II)V

    .line 185
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->postInvalidate()V

    :cond_1
    return-void
.end method

.method public hide()V
    .locals 0

    .line 285
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->scroll2BottomImmediate()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 134
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 135
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 141
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->childView:Landroid/view/View;

    return-void

    .line 139
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "there just alow one child-View in the SlideBottomLayout!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "there have no child-View in the SlideBottomLayout\uff01"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .line 153
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 154
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->childView:Landroid/view/View;

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->childView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    add-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 155
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onLayout l="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ",t="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ",r="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ",b="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "SlideBottomLayout"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 146
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 147
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->childView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    iget p2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    sub-float/2addr p1, p2

    float-to-int p1, p1

    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    .line 148
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "onMeasure movedMaxDis="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "SlideBottomLayout"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 160
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 161
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v2, :cond_1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    goto :goto_0

    .line 167
    :cond_0
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->touchActionMove(F)Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    .line 171
    :cond_1
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->touchActionUp(F)Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    .line 163
    :cond_2
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->touchActionDown(F)Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    .line 175
    :cond_3
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public scroll2BottomImmediate()V
    .locals 4

    .line 317
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getScrollY()I

    move-result v2

    neg-int v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 318
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->postInvalidate()V

    .line 319
    iput v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    .line 320
    iput-boolean v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    const-string v0, "SlideBottomLayout"

    const-string v1, "scroll2BottomImmediate"

    .line 321
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090249

    .line 322
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const v1, 0x7f0d02c4

    .line 324
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method public scroll2TopImmediate()V
    .locals 4

    .line 305
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 306
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->invalidate()V

    .line 307
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    const/4 v0, 0x1

    .line 308
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    const-string v0, "SlideBottomLayout"

    const-string v1, "scroll2TopImmediate"

    .line 309
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090249

    .line 310
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const v1, 0x7f0d00ad

    .line 312
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method public setHideWeight(F)V
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 73
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hideWeight:F

    return-void

    .line 72
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "hideWeight should belong (0f,1f]"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setShortSlideListener(Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->shortSlideListener:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;

    return-void
.end method

.method public setVisibilityHeight(F)V
    .locals 0

    .line 89
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->visibilityHeight:F

    return-void
.end method

.method public show()V
    .locals 0

    .line 278
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->scroll2TopImmediate()V

    return-void
.end method

.method public switchVisible()Z
    .locals 2

    const-string v0, "SlideBottomLayout"

    const-string v1, "switchVisible"

    .line 292
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hide()V

    goto :goto_0

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->show()V

    .line 297
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop()Z

    move-result v0

    return v0
.end method

.method public touchActionDown(F)Z
    .locals 1

    float-to-int p1, p1

    .line 265
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->downY:I

    .line 268
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->arriveTop:Z

    if-nez p1, :cond_0

    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->downY:I

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public touchActionMove(F)Z
    .locals 4

    float-to-int p1, p1

    .line 232
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->moveY:I

    .line 234
    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->downY:I

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->moveY:I

    sub-int/2addr p1, v0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lez p1, :cond_2

    .line 236
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    .line 237
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    if-le v2, v3, :cond_0

    .line 238
    iput v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    .line 240
    :cond_0
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    if-ge v2, v3, :cond_1

    .line 241
    invoke-virtual {p0, v1, p1}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->scrollBy(II)V

    .line 242
    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->moveY:I

    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->downY:I

    return v0

    :cond_1
    return v1

    .line 246
    :cond_2
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    .line 247
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    if-gez v2, :cond_3

    iput v1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    .line 248
    :cond_3
    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    if-lez v2, :cond_4

    .line 249
    invoke-virtual {p0, v1, p1}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->scrollBy(II)V

    .line 251
    :cond_4
    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->moveY:I

    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->downY:I

    return v0
.end method

.method public touchActionUp(F)Z
    .locals 2

    .line 206
    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedDis:I

    int-to-float p1, p1

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->movedMaxDis:I

    int-to-float v0, v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hideWeight:F

    mul-float v0, v0, v1

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->switchVisible()Z

    goto :goto_0

    .line 209
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->shortSlideListener:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;

    if-eqz p1, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->switchVisible()Z

    goto :goto_0

    .line 213
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hide()V

    :goto_0
    const/4 p1, 0x1

    return p1
.end method
