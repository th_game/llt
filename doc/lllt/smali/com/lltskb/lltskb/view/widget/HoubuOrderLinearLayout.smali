.class public final Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;
.super Landroid/widget/LinearLayout;
.source "HoubuOrderLinearLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHoubuOrderLinearLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HoubuOrderLinearLayout.kt\ncom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,150:1\n1587#2,2:151\n*E\n*S KotlinDebug\n*F\n+ 1 HoubuOrderLinearLayout.kt\ncom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout\n*L\n89#1,2:151\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001\"B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0019\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007B!\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nB\'\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0012J\u0012\u0010\u001a\u001a\u00020\u00182\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u000eH\u0002J\u000e\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u001eJ\u0008\u0010!\u001a\u00020\u0018H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "defStyleRes",
        "(Landroid/content/Context;Landroid/util/AttributeSet;II)V",
        "deadlineView",
        "Landroid/widget/TextView;",
        "peopleView",
        "priceView",
        "queryQueueOrder",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "reserveNoView",
        "reserveTimeView",
        "statusView",
        "trainContainer",
        "setQueryQueueOrder",
        "",
        "order",
        "updatePassengers",
        "passengerView",
        "updatePriceView",
        "price",
        "",
        "updateStatus",
        "status",
        "updateTrain",
        "ViewHolder",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private final deadlineView:Landroid/widget/TextView;

.field private final peopleView:Landroid/widget/TextView;

.field private final priceView:Landroid/widget/TextView;

.field private queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

.field private final reserveNoView:Landroid/widget/TextView;

.field private final reserveTimeView:Landroid/widget/TextView;

.field private final statusView:Landroid/widget/TextView;

.field private final trainContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 33
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0b0049

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f090163

    .line 34
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.layout_train_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    const p1, 0x7f0902b3

    .line 35
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_order_desc_no)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveNoView:Landroid/widget/TextView;

    const p1, 0x7f0902b4

    .line 36
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_order_desc_time)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveTimeView:Landroid/widget/TextView;

    const p1, 0x7f0902c0

    .line 37
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_price)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->priceView:Landroid/widget/TextView;

    const p1, 0x7f0902ea

    .line 38
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_status)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    const p1, 0x7f090276

    .line 39
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_deadline_time)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->deadlineView:Landroid/widget/TextView;

    const p1, 0x7f09023e

    .line 40
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.tv_OrderPeople)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->peopleView:Landroid/widget/TextView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 33
    move-object p2, p0

    check-cast p2, Landroid/view/ViewGroup;

    const v0, 0x7f0b0049

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f090163

    .line 34
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.layout_train_container)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    const p1, 0x7f0902b3

    .line 35
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_no)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveNoView:Landroid/widget/TextView;

    const p1, 0x7f0902b4

    .line 36
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveTimeView:Landroid/widget/TextView;

    const p1, 0x7f0902c0

    .line 37
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_price)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->priceView:Landroid/widget/TextView;

    const p1, 0x7f0902ea

    .line 38
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_status)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    const p1, 0x7f090276

    .line 39
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_deadline_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->deadlineView:Landroid/widget/TextView;

    const p1, 0x7f09023e

    .line 40
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_OrderPeople)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->peopleView:Landroid/widget/TextView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 33
    move-object p2, p0

    check-cast p2, Landroid/view/ViewGroup;

    const p3, 0x7f0b0049

    invoke-virtual {p1, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f090163

    .line 34
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.layout_train_container)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    const p1, 0x7f0902b3

    .line 35
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_no)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveNoView:Landroid/widget/TextView;

    const p1, 0x7f0902b4

    .line 36
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveTimeView:Landroid/widget/TextView;

    const p1, 0x7f0902c0

    .line 37
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_price)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->priceView:Landroid/widget/TextView;

    const p1, 0x7f0902ea

    .line 38
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_status)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    const p1, 0x7f090276

    .line 39
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_deadline_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->deadlineView:Landroid/widget/TextView;

    const p1, 0x7f09023e

    .line 40
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_OrderPeople)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->peopleView:Landroid/widget/TextView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 32
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 33
    move-object p2, p0

    check-cast p2, Landroid/view/ViewGroup;

    const p3, 0x7f0b0049

    invoke-virtual {p1, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f090163

    .line 34
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.layout_train_container)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    const p1, 0x7f0902b3

    .line 35
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_no)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveNoView:Landroid/widget/TextView;

    const p1, 0x7f0902b4

    .line 36
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_order_desc_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->reserveTimeView:Landroid/widget/TextView;

    const p1, 0x7f0902c0

    .line 37
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_price)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->priceView:Landroid/widget/TextView;

    const p1, 0x7f0902ea

    .line 38
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_status)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    const p1, 0x7f090276

    .line 39
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_deadline_time)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->deadlineView:Landroid/widget/TextView;

    const p1, 0x7f09023e

    .line 40
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.tv_OrderPeople)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->peopleView:Landroid/widget/TextView;

    return-void
.end method

.method private final updatePassengers(Landroid/widget/TextView;)V
    .locals 4

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    const-string v1, ""

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getPassengers()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Iterable;

    .line 151
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/Passenger;

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/Passenger;->getPassenger_name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 91
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/Passenger;->getTicket_type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "];"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 93
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private final updateTrain()V
    .locals 17

    move-object/from16 v0, p0

    .line 98
    iget-object v1, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getNeeds()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const/4 v3, 0x0

    if-nez v1, :cond_2

    .line 100
    :goto_1
    iget-object v1, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 101
    iget-object v1, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_1

    :cond_1
    return-void

    .line 106
    :cond_2
    :goto_2
    iget-object v4, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const-string v6, "view"

    if-ge v4, v5, :cond_3

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0b004d

    .line 108
    invoke-virtual {v4, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 109
    new-instance v5, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v4}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;-><init>(Landroid/view/View;)V

    .line 110
    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    iget-object v5, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 114
    :cond_3
    :goto_3
    iget-object v2, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-le v2, v4, :cond_4

    .line 115
    iget-object v2, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_3

    .line 118
    :cond_4
    iget-object v2, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    const/4 v4, 0x1

    sub-int/2addr v2, v4

    if-ltz v2, :cond_7

    const/4 v5, 0x0

    .line 119
    :goto_4
    iget-object v7, v0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->trainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 120
    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_6

    check-cast v7, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;

    .line 121
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;

    .line 122
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v9

    const v10, 0x7f0d0128

    invoke-virtual {v9, v10}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "AppContext.get().getStri\u2026string.fmt_concat_string)"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getTrainCodeTextView()Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getBoard_train_code()Ljava/lang/String;

    move-result-object v11

    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getFromTextView()Landroid/widget/TextView;

    move-result-object v10

    sget-object v11, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v11, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v12, "Locale.CHINA"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x2

    new-array v14, v13, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getFrom_station_name()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v3

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getStart_time()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v4

    array-length v15, v14

    invoke-static {v14, v15}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v14

    invoke-static {v11, v9, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const-string v14, "java.lang.String.format(locale, format, *args)"

    invoke-static {v11, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getToTextView()Landroid/widget/TextView;

    move-result-object v10

    sget-object v11, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v11, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v15, v13, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getTo_station_name()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v3

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getArrive_time()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v4

    array-length v4, v15

    invoke-static {v15, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v11, v9, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getSeatTextView()Landroid/widget/TextView;

    move-result-object v4

    sget-object v10, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v10, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v10, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v11, v13, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getTrain_date_str()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v3

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getSeat_name()Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x1

    aput-object v13, v11, v15

    array-length v13, v11

    invoke-static {v11, v13}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v9, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getQueue_num()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 129
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getInfoTextView()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getSeat_name()Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x1

    goto :goto_5

    .line 132
    :cond_5
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getInfoTextView()Landroid/widget/TextView;

    move-result-object v4

    sget-object v9, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v9, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v9, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v10

    const v11, 0x7f0d0150

    invoke-virtual {v10, v11}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "AppContext.get().getString(R.string.fmt_queue_num)"

    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x1

    new-array v12, v11, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/NeedsTrain;->getQueue_num()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v12, v3

    array-length v8, v12

    invoke-static {v12, v8}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    invoke-static {v9, v10, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_5
    invoke-virtual {v7}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->getDelButton()Landroid/widget/Button;

    move-result-object v4

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    if-eq v5, v2, :cond_7

    add-int/lit8 v5, v5, 0x1

    const/4 v4, 0x1

    goto/16 :goto_4

    .line 120
    :cond_6
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type com.lltskb.lltskb.view.widget.HoubuOrderLinearLayout.ViewHolder"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final setQueryQueueOrder(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V
    .locals 9

    .line 55
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-nez p1, :cond_0

    const/16 p1, 0x8

    .line 57
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setVisibility(I)V

    return-void

    .line 61
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updateTrain()V

    .line 62
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->peopleView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->updatePassengers(Landroid/widget/TextView;)V

    .line 64
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d0135

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const v0, 0x7f0902b3

    .line 65
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "java.lang.String.format(locale, format, *args)"

    const/4 v2, 0x1

    const-string v3, "Locale.CHINA"

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v0, :cond_2

    .line 66
    sget-object v6, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v7, v2, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v8, :cond_1

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getReserve_no()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_1
    move-object v8, v5

    :goto_0
    aput-object v8, v7, v4

    array-length v8, v7

    invoke-static {v7, v8}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v7

    invoke-static {v6, p1, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d0138

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const v0, 0x7f0902b4

    .line 69
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 70
    sget-object v6, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getReserve_time()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    move-object v3, v5

    :goto_1
    aput-object v3, v2, v4

    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v6, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const p1, 0x7f090276

    .line 72
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_6

    .line 73
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getRealize_limit_time()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v5

    :goto_2
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->queryQueueOrder:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;->getStatus_name()Ljava/lang/String;

    move-result-object v5

    :cond_7
    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->setVisibility(I)V

    return-void
.end method

.method public final updatePriceView(Ljava/lang/String;)V
    .locals 1

    const-string v0, "price"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->priceView:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final updateStatus(Ljava/lang/String;)V
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;->statusView:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
