.class public Lcom/lltskb/lltskb/view/widget/QuickLocationBar;
.super Landroid/view/View;
.source "QuickLocationBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;
    }
.end annotation


# instance fields
.field private characters:[Ljava/lang/String;

.field private choose:I

.field private mOnTouchLetterChangedListener:Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;

.field private mTextDialog:Landroid/widget/TextView;

.field private paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 39
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 p1, 0x1b

    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "#"

    aput-object v1, p1, v0

    const/4 v0, 0x1

    const-string v1, "A"

    aput-object v1, p1, v0

    const/4 v0, 0x2

    const-string v1, "B"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "C"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "D"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "E"

    aput-object v1, p1, v0

    const/4 v0, 0x6

    const-string v1, "F"

    aput-object v1, p1, v0

    const/4 v0, 0x7

    const-string v1, "G"

    aput-object v1, p1, v0

    const/16 v0, 0x8

    const-string v1, "H"

    aput-object v1, p1, v0

    const/16 v0, 0x9

    const-string v1, "I"

    aput-object v1, p1, v0

    const/16 v0, 0xa

    const-string v1, "J"

    aput-object v1, p1, v0

    const/16 v0, 0xb

    const-string v1, "K"

    aput-object v1, p1, v0

    const/16 v0, 0xc

    const-string v1, "L"

    aput-object v1, p1, v0

    const/16 v0, 0xd

    const-string v1, "M"

    aput-object v1, p1, v0

    const/16 v0, 0xe

    const-string v1, "N"

    aput-object v1, p1, v0

    const/16 v0, 0xf

    const-string v1, "O"

    aput-object v1, p1, v0

    const/16 v0, 0x10

    const-string v1, "P"

    aput-object v1, p1, v0

    const/16 v0, 0x11

    const-string v1, "Q"

    aput-object v1, p1, v0

    const/16 v0, 0x12

    const-string v1, "R"

    aput-object v1, p1, v0

    const/16 v0, 0x13

    const-string v1, "S"

    aput-object v1, p1, v0

    const/16 v0, 0x14

    const-string v1, "T"

    aput-object v1, p1, v0

    const/16 v0, 0x15

    const-string v1, "U"

    aput-object v1, p1, v0

    const/16 v0, 0x16

    const-string v1, "V"

    aput-object v1, p1, v0

    const/16 v0, 0x17

    const-string v1, "W"

    aput-object v1, p1, v0

    const/16 v0, 0x18

    const-string v1, "X"

    aput-object v1, p1, v0

    const/16 v0, 0x19

    const-string v1, "Y"

    aput-object v1, p1, v0

    const/16 v0, 0x1a

    const-string v1, "Z"

    aput-object v1, p1, v0

    .line 20
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 p1, 0x1b

    new-array p1, p1, [Ljava/lang/String;

    const/4 p2, 0x0

    const-string v0, "#"

    aput-object v0, p1, p2

    const/4 p2, 0x1

    const-string v0, "A"

    aput-object v0, p1, p2

    const/4 p2, 0x2

    const-string v0, "B"

    aput-object v0, p1, p2

    const/4 p2, 0x3

    const-string v0, "C"

    aput-object v0, p1, p2

    const/4 p2, 0x4

    const-string v0, "D"

    aput-object v0, p1, p2

    const/4 p2, 0x5

    const-string v0, "E"

    aput-object v0, p1, p2

    const/4 p2, 0x6

    const-string v0, "F"

    aput-object v0, p1, p2

    const/4 p2, 0x7

    const-string v0, "G"

    aput-object v0, p1, p2

    const/16 p2, 0x8

    const-string v0, "H"

    aput-object v0, p1, p2

    const/16 p2, 0x9

    const-string v0, "I"

    aput-object v0, p1, p2

    const/16 p2, 0xa

    const-string v0, "J"

    aput-object v0, p1, p2

    const/16 p2, 0xb

    const-string v0, "K"

    aput-object v0, p1, p2

    const/16 p2, 0xc

    const-string v0, "L"

    aput-object v0, p1, p2

    const/16 p2, 0xd

    const-string v0, "M"

    aput-object v0, p1, p2

    const/16 p2, 0xe

    const-string v0, "N"

    aput-object v0, p1, p2

    const/16 p2, 0xf

    const-string v0, "O"

    aput-object v0, p1, p2

    const/16 p2, 0x10

    const-string v0, "P"

    aput-object v0, p1, p2

    const/16 p2, 0x11

    const-string v0, "Q"

    aput-object v0, p1, p2

    const/16 p2, 0x12

    const-string v0, "R"

    aput-object v0, p1, p2

    const/16 p2, 0x13

    const-string v0, "S"

    aput-object v0, p1, p2

    const/16 p2, 0x14

    const-string v0, "T"

    aput-object v0, p1, p2

    const/16 p2, 0x15

    const-string v0, "U"

    aput-object v0, p1, p2

    const/16 p2, 0x16

    const-string v0, "V"

    aput-object v0, p1, p2

    const/16 p2, 0x17

    const-string v0, "W"

    aput-object v0, p1, p2

    const/16 p2, 0x18

    const-string v0, "X"

    aput-object v0, p1, p2

    const/16 p2, 0x19

    const-string v0, "Y"

    aput-object v0, p1, p2

    const/16 p2, 0x1a

    const-string v0, "Z"

    aput-object v0, p1, p2

    .line 20
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p1, 0x1b

    new-array p1, p1, [Ljava/lang/String;

    const/4 p2, 0x0

    const-string p3, "#"

    aput-object p3, p1, p2

    const/4 p2, 0x1

    const-string p3, "A"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "B"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string p3, "C"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "D"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "E"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    const-string p3, "F"

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-string p3, "G"

    aput-object p3, p1, p2

    const/16 p2, 0x8

    const-string p3, "H"

    aput-object p3, p1, p2

    const/16 p2, 0x9

    const-string p3, "I"

    aput-object p3, p1, p2

    const/16 p2, 0xa

    const-string p3, "J"

    aput-object p3, p1, p2

    const/16 p2, 0xb

    const-string p3, "K"

    aput-object p3, p1, p2

    const/16 p2, 0xc

    const-string p3, "L"

    aput-object p3, p1, p2

    const/16 p2, 0xd

    const-string p3, "M"

    aput-object p3, p1, p2

    const/16 p2, 0xe

    const-string p3, "N"

    aput-object p3, p1, p2

    const/16 p2, 0xf

    const-string p3, "O"

    aput-object p3, p1, p2

    const/16 p2, 0x10

    const-string p3, "P"

    aput-object p3, p1, p2

    const/16 p2, 0x11

    const-string p3, "Q"

    aput-object p3, p1, p2

    const/16 p2, 0x12

    const-string p3, "R"

    aput-object p3, p1, p2

    const/16 p2, 0x13

    const-string p3, "S"

    aput-object p3, p1, p2

    const/16 p2, 0x14

    const-string p3, "T"

    aput-object p3, p1, p2

    const/16 p2, 0x15

    const-string p3, "U"

    aput-object p3, p1, p2

    const/16 p2, 0x16

    const-string p3, "V"

    aput-object p3, p1, p2

    const/16 p2, 0x17

    const-string p3, "W"

    aput-object p3, p1, p2

    const/16 p2, 0x18

    const-string p3, "X"

    aput-object p3, p1, p2

    const/16 p2, 0x19

    const-string p3, "Y"

    aput-object p3, p1, p2

    const/16 p2, 0x1a

    const-string p3, "Z"

    aput-object p3, p1, p2

    .line 20
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 86
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr p1, v1

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    array-length v1, v1

    int-to-float v1, v1

    mul-float p1, p1, v1

    float-to-int p1, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    .line 90
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    .line 91
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->setBackgroundColor(I)V

    .line 92
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->invalidate()V

    .line 93
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mTextDialog:Landroid/widget/TextView;

    if-eqz p1, :cond_4

    const/16 v0, 0x8

    .line 94
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 101
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    if-eq v0, p1, :cond_4

    if-ltz p1, :cond_4

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    array-length v3, v0

    if-ge p1, v3, :cond_4

    .line 103
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mOnTouchLetterChangedListener:Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;

    if-eqz v3, :cond_2

    .line 104
    aget-object v0, v0, p1

    .line 105
    invoke-interface {v3, v0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;->touchLetterChanged(Ljava/lang/String;)V

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mTextDialog:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 108
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mTextDialog:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    :cond_3
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    .line 112
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->invalidate()V

    :cond_4
    :goto_0
    return v2
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 54
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 55
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->getWidth()I

    move-result v0

    .line 56
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->getHeight()I

    move-result v1

    .line 57
    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    array-length v2, v2

    div-int/2addr v1, v2

    const/4 v2, 0x0

    .line 58
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 60
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f06009c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 62
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    const/high16 v5, 0x43160000    # 150.0f

    int-to-float v6, v0

    mul-float v6, v6, v5

    const/high16 v5, 0x43a00000    # 320.0f

    div-float/2addr v6, v5

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 64
    iget v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->choose:I

    if-ne v2, v3, :cond_0

    .line 65
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f060023

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 69
    :cond_0
    div-int/lit8 v3, v0, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-int v4, v1, v2

    add-int/2addr v4, v1

    int-to-float v4, v4

    .line 72
    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    aget-object v5, v5, v2

    iget-object v6, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v3, v4, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 73
    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->reset()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setCharacters([Ljava/lang/String;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->characters:[Ljava/lang/String;

    .line 79
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->invalidate()V

    return-void
.end method

.method public setOnTouchLitterChangedListener(Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mOnTouchLetterChangedListener:Lcom/lltskb/lltskb/view/widget/QuickLocationBar$OnTouchLetterChangedListener;

    return-void
.end method

.method public setTextDialog(Landroid/widget/TextView;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/QuickLocationBar;->mTextDialog:Landroid/widget/TextView;

    return-void
.end method
