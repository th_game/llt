.class public Lcom/lltskb/lltskb/view/widget/ColorEvaluator;
.super Ljava/lang/Object;
.source "ColorEvaluator.java"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# instance fields
.field private mCurrentBlue:I

.field private mCurrentGreen:I

.field private mCurrentRed:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 7
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    .line 9
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    .line 11
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    return-void
.end method

.method private getCurrentColor(IIIIF)I
    .locals 0

    if-le p1, p2, :cond_0

    int-to-float p1, p1

    int-to-float p3, p3

    mul-float p5, p5, p3

    int-to-float p3, p4

    sub-float/2addr p5, p3

    sub-float/2addr p1, p5

    float-to-int p1, p1

    if-ge p1, p2, :cond_1

    goto :goto_0

    :cond_0
    int-to-float p1, p1

    int-to-float p3, p3

    mul-float p5, p5, p3

    int-to-float p3, p4

    sub-float/2addr p5, p3

    add-float/2addr p1, p5

    float-to-int p1, p1

    if-le p1, p2, :cond_1

    :goto_0
    move p1, p2

    :cond_1
    return p1
.end method

.method private getHexString(I)Ljava/lang/String;
    .locals 2

    .line 78
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .line 15
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    .line 16
    move-object v1, p3

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x3

    .line 17
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v6, 0x5

    .line 18
    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    const/4 v8, 0x7

    .line 19
    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    .line 20
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 21
    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 22
    invoke-virtual {v1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 25
    iput v4, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    .line 27
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    if-ne v0, v1, :cond_1

    .line 28
    iput v7, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    .line 30
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    if-ne v0, v1, :cond_2

    .line 31
    iput v9, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    :cond_2
    sub-int v0, v4, v2

    .line 34
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    sub-int v0, v7, v3

    .line 35
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sub-int v1, v9, v5

    .line 36
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int v8, v6, v0

    add-int v10, v8, v1

    .line 38
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    if-eq v0, v2, :cond_3

    const/4 v5, 0x0

    move-object v0, p0

    move v1, v4

    move v3, v10

    move v4, v5

    move v5, p1

    .line 39
    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getCurrentColor(IIIIF)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    goto :goto_0

    .line 41
    :cond_3
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    if-eq v0, v3, :cond_4

    move-object v0, p0

    move v1, v7

    move v2, v3

    move v3, v10

    move v4, v6

    move v5, p1

    .line 42
    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getCurrentColor(IIIIF)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    goto :goto_0

    .line 44
    :cond_4
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    if-eq v0, v5, :cond_5

    move-object v0, p0

    move v1, v9

    move v2, v5

    move v3, v10

    move v4, v8

    move v5, p1

    .line 45
    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getCurrentColor(IIIIF)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    .line 49
    :cond_5
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentRed:I

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentGreen:I

    .line 50
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->mCurrentBlue:I

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
