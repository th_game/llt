.class public Lcom/lltskb/lltskb/view/widget/SwitchView;
.super Landroid/view/View;
.source "SwitchView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;
    }
.end annotation


# instance fields
.field private anim:Landroid/animation/ValueAnimator;

.field private anim2:Landroid/animation/ObjectAnimator;

.field private anim3:Landroid/animation/ObjectAnimator;

.field private anim4:Landroid/animation/ObjectAnimator;

.field private animSet:Landroid/animation/AnimatorSet;

.field private animatorLift:F

.field private animatorRight:F

.field private bgColor:I

.field private bgPaint:Landroid/graphics/Paint;

.field private bgPath:Landroid/graphics/Path;

.field private checked:Z

.field private clickColor:Ljava/lang/String;

.field private clickPaint:Landroid/graphics/Paint;

.field private clickPath:Landroid/graphics/Path;

.field private leftColor:Ljava/lang/String;

.field private leftTextPaint:Landroid/graphics/Paint;

.field private mClickWidth:F

.field private mHeight:F

.field private mLeftTextX:F

.field private mLiftTextY:F

.field private mRightTextX:F

.field private mRightTexty:F

.field private mWidth:F

.field private onClickCheckedListener:Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;

.field private padding:F

.field rgb:I

.field private rightColor:Ljava/lang/String;

.field private rightTextPaint:Landroid/graphics/Paint;

.field private roundRect:Landroid/graphics/RectF;

.field private textLeft:Ljava/lang/String;

.field private textLeftClickColor:Ljava/lang/String;

.field private textLeftColor:Ljava/lang/String;

.field private textRight:Ljava/lang/String;

.field private textRightClickColor:Ljava/lang/String;

.field private textRightColor:Ljava/lang/String;

.field private time:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0, p1, p2, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p1, 0xff

    .line 54
    invoke-static {p1, p1, p1}, Landroid/graphics/Color;->rgb(III)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rgb:I

    .line 78
    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->init(Landroid/util/AttributeSet;)V

    .line 79
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->initPaint()V

    .line 80
    invoke-virtual {p0, p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/widget/SwitchView;)F
    .locals 0

    .line 24
    iget p0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    return p0
.end method

.method static synthetic access$102(Lcom/lltskb/lltskb/view/widget/SwitchView;F)F
    .locals 0

    .line 24
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorRight:F

    return p1
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/view/widget/SwitchView;F)F
    .locals 0

    .line 24
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorLift:F

    return p1
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/widget/SwitchView;F)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/SwitchView;->offsetWidth(F)V

    return-void
.end method

.method private dp2px(F)I
    .locals 1

    .line 265
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float p1, p1, v0

    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method private getPath(Landroid/graphics/Path;FFF)V
    .locals 3

    .line 227
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->roundRect:Landroid/graphics/RectF;

    const/4 v1, 0x0

    add-float/2addr v1, p2

    add-float v2, v1, p4

    add-float/2addr p3, p2

    add-float/2addr p3, p4

    iget p4, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    sub-float/2addr p4, p2

    invoke-virtual {v0, v2, v1, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Path;->rewind()V

    .line 230
    iget-object p2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->roundRect:Landroid/graphics/RectF;

    iget p3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    const/high16 p4, 0x40000000    # 2.0f

    div-float v0, p3, p4

    div-float/2addr p3, p4

    sget-object p4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    return-void
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 5

    .line 85
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/lltskb/lltskb/R$styleable;->SwitchView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 86
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rgb:I

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgColor:I

    const/16 v0, 0x8b

    const/16 v2, 0x22

    .line 87
    invoke-static {v2, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftColor:Ljava/lang/String;

    .line 88
    invoke-static {v2, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightColor:Ljava/lang/String;

    .line 89
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftColor:Ljava/lang/String;

    .line 90
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightColor:Ljava/lang/String;

    .line 91
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rgb:I

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftClickColor:Ljava/lang/String;

    .line 92
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rgb:I

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightClickColor:Ljava/lang/String;

    const/4 v0, 0x4

    .line 93
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->checked:Z

    const/4 v0, 0x5

    .line 94
    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeft:Ljava/lang/String;

    const/16 v0, 0x8

    .line 95
    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRight:Ljava/lang/String;

    const/high16 v0, 0x40800000    # 4.0f

    .line 96
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->dp2px(F)I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->padding:F

    const/16 v0, 0xb

    const/16 v1, 0x12c

    .line 97
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->time:I

    .line 98
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private initAnim()V
    .locals 6

    .line 180
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 183
    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    .line 184
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim:Landroid/animation/ValueAnimator;

    new-array v4, v3, [F

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorRight:F

    aput v5, v4, v2

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    aput v5, v4, v1

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 186
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftClickColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightClickColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    .line 188
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftColor:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightColor:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim:Landroid/animation/ValueAnimator;

    new-array v4, v3, [F

    iget v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorLift:F

    aput v5, v4, v2

    const/4 v5, 0x0

    aput v5, v4, v1

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 191
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftClickColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightClickColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightColor:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    .line 193
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightColor:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftColor:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->toHexEncoding(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V

    .line 195
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/lltskb/lltskb/view/widget/SwitchView$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/widget/SwitchView$1;-><init>(Lcom/lltskb/lltskb/view/widget/SwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 209
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 210
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->time:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 211
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private initPaint()V
    .locals 4

    .line 105
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animSet:Landroid/animation/AnimatorSet;

    .line 106
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim:Landroid/animation/ValueAnimator;

    .line 108
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim2:Landroid/animation/ObjectAnimator;

    const-string v1, "textLeftColor"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 113
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    .line 114
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim3:Landroid/animation/ObjectAnimator;

    const-string v1, "textRightColor"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 118
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->anim4:Landroid/animation/ObjectAnimator;

    const-string v1, "clickColor"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 123
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->roundRect:Landroid/graphics/RectF;

    .line 124
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPath:Landroid/graphics/Path;

    .line 125
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPath:Landroid/graphics/Path;

    .line 126
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPaint:Landroid/graphics/Paint;

    .line 127
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 130
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPaint:Landroid/graphics/Paint;

    .line 131
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->refreshColor()V

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 134
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->sp2px(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 136
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 137
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    .line 138
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->sp2px(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 139
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 141
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftClickColor:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightColor:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightClickColor:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftColor:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    return-void
.end method

.method private initPath()V
    .locals 6

    .line 162
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mWidth:F

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->padding:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    .line 163
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 164
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/view/widget/SwitchView;->offsetWidth(F)V

    goto :goto_0

    .line 166
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->offsetWidth(F)V

    .line 168
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mWidth:F

    invoke-direct {p0, v0, v1, v3, v1}, Lcom/lltskb/lltskb/view/widget/SwitchView;->getPath(Landroid/graphics/Path;FFF)V

    .line 169
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeft:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    sub-float/2addr v0, v3

    div-float/2addr v0, v2

    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->padding:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mLeftTextX:F

    .line 170
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    add-float/2addr v3, v0

    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRight:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v2

    add-float/2addr v3, v0

    iput v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mRightTextX:F

    .line 171
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 172
    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    div-float/2addr v3, v2

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float/2addr v4, v0

    div-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mLiftTextY:F

    .line 173
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 174
    iget v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    div-float/2addr v3, v2

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float/2addr v4, v0

    div-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mRightTexty:F

    .line 175
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorRight:F

    .line 176
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->animatorLift:F

    return-void
.end method

.method private offsetWidth(F)V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->padding:F

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mClickWidth:F

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/lltskb/lltskb/view/widget/SwitchView;->getPath(Landroid/graphics/Path;FFF)V

    return-void
.end method

.method private refreshColor()V
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftColor:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setClickColor(I)V

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightColor:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setClickColor(I)V

    :goto_0
    return-void
.end method

.method private varargs setAnimView(Landroid/animation/ObjectAnimator;[Ljava/lang/Object;)V
    .locals 0

    .line 216
    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->setObjectValues([Ljava/lang/Object;)V

    .line 217
    new-instance p2, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;

    invoke-direct {p2}, Lcom/lltskb/lltskb/view/widget/ColorEvaluator;-><init>()V

    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    return-void
.end method


# virtual methods
.method public getClickColor()Ljava/lang/String;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickColor:Ljava/lang/String;

    return-object v0
.end method

.method public getTextLeftColor()Ljava/lang/String;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeftColor:Ljava/lang/String;

    return-object v0
.end method

.method public getTextRightColor()Ljava/lang/String;
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRightColor:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 353
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->checked:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 330
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->checked:Z

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setChecked(Z)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v0, 0x0

    .line 253
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 254
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->bgPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 255
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 259
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textLeft:Ljava/lang/String;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mLeftTextX:F

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mLiftTextY:F

    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 260
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->textRight:Ljava/lang/String;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mRightTextX:F

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mRightTexty:F

    iget-object v3, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 235
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 236
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    .line 237
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 238
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne p1, v2, :cond_0

    .line 240
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mWidth:F

    :cond_0
    if-ne p2, v2, :cond_1

    .line 243
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    .line 245
    :cond_1
    iget p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mWidth:F

    float-to-int p1, p1

    iget p2, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->mHeight:F

    float-to-int p2, p2

    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setMeasuredDimension(II)V

    .line 247
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->initPath()V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 334
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->checked:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 337
    :cond_0
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->checked:Z

    .line 338
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->initAnim()V

    .line 339
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->onClickCheckedListener:Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;

    if-eqz p1, :cond_1

    .line 340
    invoke-interface {p1}, Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;->onClick()V

    :cond_1
    return-void
.end method

.method public setClickColor(I)V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setClickColor(Ljava/lang/String;)V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->clickPaint:Landroid/graphics/Paint;

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setOnClickCheckedListener(Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;)V
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->onClickCheckedListener:Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;

    return-void
.end method

.method public setTextLeftColor(Ljava/lang/String;)V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->leftTextPaint:Landroid/graphics/Paint;

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setTextRightColor(Ljava/lang/String;)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/SwitchView;->rightTextPaint:Landroid/graphics/Paint;

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public sp2px(F)I
    .locals 1

    .line 276
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float p1, p1, v0

    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method public toHexEncoding(I)Ljava/lang/String;
    .locals 6

    .line 310
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 311
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 312
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 313
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    .line 314
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const-string v4, "0"

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 315
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v5, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 316
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v5, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_2
    const-string v3, "#"

    .line 317
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 318
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 319
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 320
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
