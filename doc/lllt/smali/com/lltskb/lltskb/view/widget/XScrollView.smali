.class public Lcom/lltskb/lltskb/view/widget/XScrollView;
.super Landroid/widget/ScrollView;
.source "XScrollView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;,
        Lcom/lltskb/lltskb/view/widget/XScrollView$OnXScrollListener;
    }
.end annotation


# static fields
.field private static final OFFSET_RADIO:F = 1.8f

.field private static final PULL_LOAD_MORE_DELTA:I = 0x32

.field private static final SCROLL_BACK_FOOTER:I = 0x1

.field private static final SCROLL_BACK_HEADER:I = 0x0

.field private static final SCROLL_DURATION:I = 0x190


# instance fields
.field private mContentLayout:Landroid/widget/LinearLayout;

.field private mEnableAutoLoad:Z

.field private mEnablePullLoad:Z

.field private mEnablePullRefresh:Z

.field private mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

.field private mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

.field private mHeaderContent:Landroid/widget/RelativeLayout;

.field private mHeaderHeight:I

.field private mHeaderTime:Landroid/widget/TextView;

.field private mLastY:F

.field private mLayout:Landroid/widget/LinearLayout;

.field private mListener:Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;

.field private mPullLoading:Z

.field private mPullRefreshing:Z

.field private mScrollBack:I

.field private mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mScroller:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 77
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x40800000    # -1.0f

    .line 46
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    const/4 v0, 0x1

    .line 69
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    const/4 v1, 0x0

    .line 70
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 72
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    .line 73
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnableAutoLoad:Z

    .line 74
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 78
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p2, -0x40800000    # -1.0f

    .line 46
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    const/4 p2, 0x1

    .line 69
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    const/4 v0, 0x0

    .line 70
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 72
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    .line 73
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnableAutoLoad:Z

    .line 74
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 83
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p2, -0x40800000    # -1.0f

    .line 46
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    const/4 p2, 0x1

    .line 69
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    const/4 p3, 0x0

    .line 70
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 72
    iput-boolean p2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    .line 73
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnableAutoLoad:Z

    .line 74
    iput-boolean p3, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 88
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->initWithContext(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/view/widget/XScrollView;I)I
    .locals 0

    .line 32
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    return p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/widget/XScrollView;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderContent:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/widget/XScrollView;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->startLoadMore()V

    return-void
.end method

.method private initWithContext(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f0b009e

    const/4 v1, 0x0

    .line 92
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    .line 93
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09009a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    .line 95
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    .line 97
    invoke-virtual {p0, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 100
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    .line 101
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const v1, 0x7f0900ea

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderContent:Landroid/widget/RelativeLayout;

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const v1, 0x7f0900ec

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderTime:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0900ed

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 104
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 107
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    .line 108
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0x11

    .line 111
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 112
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0900db

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 113
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0, v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 118
    new-instance v0, Lcom/lltskb/lltskb/view/widget/XScrollView$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView$1;-><init>(Lcom/lltskb/lltskb/view/widget/XScrollView;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 136
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private invokeOnScrolling()V
    .locals 2

    .line 284
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    instance-of v1, v0, Lcom/lltskb/lltskb/view/widget/XScrollView$OnXScrollListener;

    if-eqz v1, :cond_0

    .line 285
    check-cast v0, Lcom/lltskb/lltskb/view/widget/XScrollView$OnXScrollListener;

    .line 286
    invoke-interface {v0, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView$OnXScrollListener;->onXScrolling(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private isBottom()Z
    .locals 2

    .line 435
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->computeVerticalScrollRange()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 436
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getScrollY()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isTop()Z
    .locals 2

    .line 431
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getScrollY()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private loadMore()V
    .locals 1

    .line 500
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mListener:Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;

    if-eqz v0, :cond_0

    .line 501
    invoke-interface {v0}, Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;->onLoadMore()V

    :cond_0
    return-void
.end method

.method private refresh()V
    .locals 1

    .line 494
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mListener:Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;

    if-eqz v0, :cond_0

    .line 495
    invoke-interface {v0}, Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;->onRefresh()V

    :cond_0
    return-void
.end method

.method private resetFooterHeight()V
    .locals 7

    .line 356
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 359
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollBack:I

    .line 360
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v4, 0x0

    neg-int v5, v3

    const/16 v6, 0x190

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 361
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->invalidate()V

    :cond_0
    return-void
.end method

.method private resetHeaderHeight()V
    .locals 7

    .line 312
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v3

    if-nez v3, :cond_0

    return-void

    .line 316
    :cond_0
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-gt v3, v0, :cond_1

    return-void

    .line 321
    :cond_1
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-le v3, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 325
    :goto_0
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollBack:I

    .line 326
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v4, 0x0

    sub-int v5, v0, v3

    const/16 v6, 0x190

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 329
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->invalidate()V

    return-void
.end method

.method private resetHeaderOrBottom()V
    .locals 2

    .line 412
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->isTop()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    .line 415
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 416
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 417
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->refresh()V

    .line 419
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->resetHeaderHeight()V

    goto :goto_0

    .line 421
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->isBottom()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 423
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_2

    .line 424
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->startLoadMore()V

    .line 426
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->resetFooterHeight()V

    :cond_3
    :goto_0
    return-void
.end method

.method private startLoadMore()V
    .locals 2

    .line 366
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 367
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 368
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 369
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->loadMore()V

    :cond_0
    return-void
.end method

.method private updateFooterHeight(F)V
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v0

    float-to-int p1, p1

    add-int/2addr v0, p1

    .line 335
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    if-nez p1, :cond_1

    const/16 p1, 0x32

    if-le v0, p1, :cond_0

    .line 338
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    goto :goto_0

    .line 340
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 344
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    .line 347
    new-instance p1, Lcom/lltskb/lltskb/view/widget/XScrollView$4;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView$4;-><init>(Lcom/lltskb/lltskb/view/widget/XScrollView;)V

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private updateHeaderHeight(F)V
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    float-to-int p1, p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v1

    add-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    .line 293
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    if-nez p1, :cond_1

    .line 295
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result p1

    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-le p1, v0, :cond_0

    .line 296
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 303
    :cond_1
    :goto_0
    new-instance p1, Lcom/lltskb/lltskb/view/widget/XScrollView$3;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView$3;-><init>(Lcom/lltskb/lltskb/view/widget/XScrollView;)V

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XScrollView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public autoRefresh()V
    .locals 3

    .line 267
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    .line 269
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    if-nez v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v0

    iget v2, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderHeight:I

    if-le v0, v2, :cond_0

    .line 272
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    goto :goto_0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 278
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 279
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setState(I)V

    .line 280
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->refresh()V

    return-void
.end method

.method public computeScroll()V
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollBack:I

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setVisibleHeight(I)V

    goto :goto_0

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    .line 448
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->postInvalidate()V

    .line 449
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->invokeOnScrolling()V

    .line 451
    :cond_1
    invoke-super {p0}, Landroid/widget/ScrollView;->computeScroll()V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .line 488
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 489
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3

    .line 468
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 472
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    add-int/2addr v2, v0

    sub-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 475
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnableAutoLoad:Z

    if-eqz v0, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->startLoadMore()V

    .line 481
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .line 460
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 461
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 375
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 376
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    .line 379
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 402
    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    .line 404
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->resetHeaderOrBottom()V

    goto :goto_0

    .line 385
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    sub-float/2addr v0, v1

    .line 386
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    .line 388
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->isTop()Z

    move-result v1

    const/4 v2, 0x0

    const v3, 0x3fe66666    # 1.8f

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeader:Lcom/lltskb/lltskb/view/widget/XHeaderView;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->getVisibleHeight()I

    move-result v1

    if-gtz v1, :cond_2

    cmpl-float v1, v0, v2

    if-lez v1, :cond_3

    :cond_2
    div-float/2addr v0, v3

    .line 390
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->updateHeaderHeight(F)V

    .line 391
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->invokeOnScrolling()V

    goto :goto_0

    .line 393
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->isBottom()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getBottomMargin()I

    move-result v1

    if-gtz v1, :cond_4

    cmpg-float v1, v0, v2

    if-gez v1, :cond_6

    :cond_4
    neg-float v0, v0

    div-float/2addr v0, v3

    .line 395
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->updateFooterHeight(F)V

    goto :goto_0

    .line 381
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLastY:F

    .line 408
    :cond_6
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public setAutoLoadEnable(Z)V
    .locals 0

    .line 222
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnableAutoLoad:Z

    return-void
.end method

.method public setContentView(Landroid/view/ViewGroup;)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    const v1, 0x7f09009a

    .line 150
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setIXScrollViewListener(Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;)V
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mListener:Lcom/lltskb/lltskb/view/widget/XScrollView$IXScrollViewListener;

    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method public setPullLoadEnable(Z)V
    .locals 2

    .line 193
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    .line 195
    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullLoad:Z

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 196
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setBottomMargin(I)V

    .line 197
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->hide()V

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v0, v0, v1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setPadding(IIII)V

    .line 199
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 202
    :cond_0
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 203
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0, v0, v0, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setPadding(IIII)V

    .line 204
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->show()V

    .line 205
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    .line 207
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    new-instance v0, Lcom/lltskb/lltskb/view/widget/XScrollView$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/widget/XScrollView$2;-><init>(Lcom/lltskb/lltskb/view/widget/XScrollView;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method public setPullRefreshEnable(Z)V
    .locals 1

    .line 181
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mEnablePullRefresh:Z

    .line 184
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderContent:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method

.method public setRefreshTime(Ljava/lang/String;)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mHeaderTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    return-void

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    const v1, 0x7f09009a

    .line 170
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mContentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public stopLoadMore()V
    .locals 2

    .line 239
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 240
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullLoading:Z

    .line 241
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mFooterView:Lcom/lltskb/lltskb/view/widget/XFooterView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XFooterView;->setState(I)V

    :cond_0
    return-void
.end method

.method public stopRefresh()V
    .locals 1

    .line 229
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 230
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XScrollView;->mPullRefreshing:Z

    .line 231
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/widget/XScrollView;->resetHeaderHeight()V

    :cond_0
    return-void
.end method
