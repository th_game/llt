.class public final Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;
.super Ljava/lang/Object;
.source "HoubuOrderLinearLayout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\r\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u000f\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0011\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000cR\u0011\u0010\u0013\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000c\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;",
        "",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "delButton",
        "Landroid/widget/Button;",
        "getDelButton",
        "()Landroid/widget/Button;",
        "fromTextView",
        "Landroid/widget/TextView;",
        "getFromTextView",
        "()Landroid/widget/TextView;",
        "infoTextView",
        "getInfoTextView",
        "seatTextView",
        "getSeatTextView",
        "toTextView",
        "getToTextView",
        "trainCodeTextView",
        "getTrainCodeTextView",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final delButton:Landroid/widget/Button;

.field private final fromTextView:Landroid/widget/TextView;

.field private final infoTextView:Landroid/widget/TextView;

.field private final seatTextView:Landroid/widget/TextView;

.field private final toTextView:Landroid/widget/TextView;

.field private final trainCodeTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0902fb

    .line 141
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_train_code)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->trainCodeTextView:Landroid/widget/TextView;

    const v0, 0x7f090287

    .line 142
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_from)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->fromTextView:Landroid/widget/TextView;

    const v0, 0x7f0902f7

    .line 143
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_to)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->toTextView:Landroid/widget/TextView;

    const v0, 0x7f0902d6

    .line 144
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_seat)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->seatTextView:Landroid/widget/TextView;

    const v0, 0x7f090293

    .line 145
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_info)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->infoTextView:Landroid/widget/TextView;

    const v0, 0x7f09004d

    .line 146
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.btn_del)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->delButton:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method public final getDelButton()Landroid/widget/Button;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->delButton:Landroid/widget/Button;

    return-object v0
.end method

.method public final getFromTextView()Landroid/widget/TextView;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->fromTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getInfoTextView()Landroid/widget/TextView;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->infoTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getSeatTextView()Landroid/widget/TextView;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->seatTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getToTextView()Landroid/widget/TextView;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->toTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTrainCodeTextView()Landroid/widget/TextView;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/HoubuOrderLinearLayout$ViewHolder;->trainCodeTextView:Landroid/widget/TextView;

    return-object v0
.end method
