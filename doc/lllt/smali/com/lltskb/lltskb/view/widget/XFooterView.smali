.class public Lcom/lltskb/lltskb/view/widget/XFooterView;
.super Landroid/widget/LinearLayout;
.source "XFooterView.java"


# static fields
.field public static final STATE_LOADING:I = 0x2

.field public static final STATE_NORMAL:I = 0x0

.field public static final STATE_READY:I = 0x1


# instance fields
.field private final ROTATE_ANIM_DURATION:I

.field private mHintView:Landroid/widget/TextView;

.field private mLayout:Landroid/view/View;

.field private mProgressBar:Landroid/view/View;

.field private mRotateDownAnim:Landroid/view/animation/Animation;

.field private mRotateUpAnim:Landroid/view/animation/Animation;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xb4

    .line 26
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->ROTATE_ANIM_DURATION:I

    const/4 v0, 0x0

    .line 39
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mState:I

    .line 43
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 p2, 0xb4

    .line 26
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->ROTATE_ANIM_DURATION:I

    const/4 p2, 0x0

    .line 39
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mState:I

    .line 48
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 10

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0b009b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    .line 53
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XFooterView;->addView(Landroid/view/View;)V

    .line 57
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    const v0, 0x7f0900dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mProgressBar:Landroid/view/View;

    .line 58
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    const v0, 0x7f0900da

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    .line 61
    new-instance p1, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43340000    # 180.0f

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateUpAnim:Landroid/view/animation/Animation;

    .line 63
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateUpAnim:Landroid/view/animation/Animation;

    const-wide/16 v0, 0xb4

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 64
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateUpAnim:Landroid/view/animation/Animation;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 66
    new-instance p1, Landroid/view/animation/RotateAnimation;

    const/high16 v4, 0x43340000    # 180.0f

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/high16 v9, 0x3f000000    # 0.5f

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateDownAnim:Landroid/view/animation/Animation;

    .line 68
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateDownAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 69
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mRotateDownAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    return-void
.end method


# virtual methods
.method public getBottomMargin()I
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 140
    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    return v0
.end method

.method public hide()V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    .line 164
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 165
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public loading()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public normal()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setBottomMargin(I)V
    .locals 1

    if-gez p1, :cond_0

    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 129
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 130
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setState(I)V
    .locals 3

    .line 82
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mState:I

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-ne p1, v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    goto :goto_1

    .line 107
    :cond_2
    iget v1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mState:I

    if-eq v1, v0, :cond_4

    .line 110
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    const v1, 0x7f0d015f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 103
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mHintView:Landroid/widget/TextView;

    const v1, 0x7f0d015e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 118
    :cond_4
    :goto_1
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mState:I

    return-void
.end method

.method public show()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    .line 173
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 174
    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XFooterView;->mLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
