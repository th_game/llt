.class public Lcom/lltskb/lltskb/view/widget/XHeaderView;
.super Landroid/widget/LinearLayout;
.source "XHeaderView.java"


# static fields
.field public static final STATE_NORMAL:I = 0x0

.field public static final STATE_READY:I = 0x1

.field public static final STATE_REFRESHING:I = 0x2


# instance fields
.field private final ROTATE_ANIM_DURATION:I

.field private mArrowImageView:Landroid/widget/ImageView;

.field private mContainer:Landroid/widget/LinearLayout;

.field private mHintTextView:Landroid/widget/TextView;

.field private mIsFirst:Z

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mRotateDownAnim:Landroid/view/animation/Animation;

.field private mRotateUpAnim:Landroid/view/animation/Animation;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xb4

    .line 30
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->ROTATE_ANIM_DURATION:I

    const/4 v0, 0x0

    .line 40
    iput v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    .line 49
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 p2, 0xb4

    .line 30
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->ROTATE_ANIM_DURATION:I

    const/4 p2, 0x0

    .line 40
    iput p2, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 10

    .line 59
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v1, 0x7f0b009c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mContainer:Landroid/widget/LinearLayout;

    .line 61
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 p1, 0x50

    .line 62
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->setGravity(I)V

    const p1, 0x7f0900e9

    .line 64
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    const p1, 0x7f0900eb

    .line 65
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mHintTextView:Landroid/widget/TextView;

    const p1, 0x7f0900ee

    .line 66
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/widget/XHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mProgressBar:Landroid/widget/ProgressBar;

    .line 68
    new-instance p1, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, -0x3ccc0000    # -180.0f

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateUpAnim:Landroid/view/animation/Animation;

    .line 70
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateUpAnim:Landroid/view/animation/Animation;

    const-wide/16 v0, 0xb4

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 71
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateUpAnim:Landroid/view/animation/Animation;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 73
    new-instance p1, Landroid/view/animation/RotateAnimation;

    const/high16 v4, -0x3ccc0000    # -180.0f

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/high16 v9, 0x3f000000    # 0.5f

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateDownAnim:Landroid/view/animation/Animation;

    .line 75
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateDownAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 76
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateDownAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    return-void
.end method


# virtual methods
.method public getVisibleHeight()I
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return v0
.end method

.method public setState(I)V
    .locals 5

    .line 80
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mIsFirst:Z

    if-eqz v0, :cond_0

    .line 81
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mIsFirst:Z

    return-void

    :cond_0
    const/4 v0, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 87
    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->clearAnimation()V

    .line 88
    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v4, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 93
    iget-object v2, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    if-eqz p1, :cond_4

    if-eq p1, v1, :cond_3

    if-eq p1, v3, :cond_2

    goto :goto_1

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mHintTextView:Landroid/widget/TextView;

    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 110
    :cond_3
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    if-eq v0, v1, :cond_7

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 112
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateUpAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 113
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mHintTextView:Landroid/widget/TextView;

    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 98
    :cond_4
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    if-ne v0, v1, :cond_5

    .line 99
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mRotateDownAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 102
    :cond_5
    iget v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    if-ne v0, v3, :cond_6

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 106
    :cond_6
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mHintTextView:Landroid/widget/TextView;

    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 125
    :cond_7
    :goto_1
    iput p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mState:I

    return-void
.end method

.method public setVisibleHeight(I)V
    .locals 1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 136
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 137
    iget-object p1, p0, Lcom/lltskb/lltskb/view/widget/XHeaderView;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
