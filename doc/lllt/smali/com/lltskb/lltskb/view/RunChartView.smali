.class public Lcom/lltskb/lltskb/view/RunChartView;
.super Landroid/widget/LinearLayout;
.source "RunChartView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/RunChartView$DayCell;,
        Lcom/lltskb/lltskb/view/RunChartView$DayHead;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RunChartView"


# instance fields
.field private mAuxDay:I

.field private mCellHeight:I

.field private mCellWidth:I

.field private mCurrentDate:Ljava/util/Date;

.field private mCurrentMonth:Ljava/util/Date;

.field private mMaxDate:Ljava/lang/String;

.field private mMinDate:Ljava/lang/String;

.field private mRuleIndex:I

.field private mSchData:[B

.field private mSchedule:Lcom/lltskb/lltskb/engine/Schedule;

.field private mStartDate:Ljava/util/Date;

.field private mStationIdx:I

.field private mTrainIdx:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 153
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 61
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mAuxDay:I

    .line 63
    new-instance p1, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {p1}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchedule:Lcom/lltskb/lltskb/engine/Schedule;

    const/4 p1, -0x1

    .line 64
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStationIdx:I

    .line 65
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mTrainIdx:I

    const/16 p1, 0x14

    new-array p1, p1, [B

    .line 66
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    .line 154
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 162
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 61
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mAuxDay:I

    .line 63
    new-instance p1, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {p1}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchedule:Lcom/lltskb/lltskb/engine/Schedule;

    const/4 p1, -0x1

    .line 64
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStationIdx:I

    .line 65
    iput p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mTrainIdx:I

    const/16 p1, 0x14

    new-array p1, p1, [B

    .line 66
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    .line 163
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView;->initView()V

    return-void
.end method

.method private createHeaderLayout()Landroid/view/View;
    .locals 6

    const/4 v0, 0x0

    .line 245
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/RunChartView;->createLayout(I)Landroid/widget/LinearLayout;

    move-result-object v1

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    .line 247
    new-instance v2, Lcom/lltskb/lltskb/view/RunChartView$DayHead;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellWidth:I

    iget v5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;-><init>(Lcom/lltskb/lltskb/view/RunChartView;Landroid/content/Context;II)V

    add-int/lit8 v0, v0, 0x1

    .line 248
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/DayStyle;->getWeekDayName(I)Ljava/lang/String;

    move-result-object v3

    .line 249
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->setText(Ljava/lang/String;)V

    const v3, -0xe7652a

    .line 250
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->setBackColor(I)V

    const/4 v3, -0x1

    .line 251
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/RunChartView$DayHead;->setTextColor(I)V

    .line 252
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private createLayout(I)Landroid/widget/LinearLayout;
    .locals 4

    .line 168
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 169
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-object v0
.end method

.method private createRunChart()V
    .locals 8

    .line 189
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView;->createHeaderLayout()Landroid/view/View;

    move-result-object v0

    .line 190
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/RunChartView;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 192
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/RunChartView;->createLayout(I)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    :goto_1
    const/4 v4, 0x7

    if-ge v3, v4, :cond_0

    .line 194
    new-instance v4, Lcom/lltskb/lltskb/view/RunChartView$DayCell;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellWidth:I

    iget v7, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;-><init>(Lcom/lltskb/lltskb/view/RunChartView;Landroid/content/Context;II)V

    .line 195
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 197
    :cond_0
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/view/RunChartView;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private getRunStatus(Ljava/util/Date;)I
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 315
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMinDate:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMaxDate:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 318
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "yyyyMMdd"

    invoke-direct {v1, v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 319
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 322
    invoke-static {}, Lcom/lltskb/lltskb/engine/Rule;->get()Lcom/lltskb/lltskb/engine/Rule;

    move-result-object v4

    .line 323
    iget-object v5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMinDate:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMaxDate:Ljava/lang/String;

    iget v8, p0, Lcom/lltskb/lltskb/view/RunChartView;->mRuleIndex:I

    iget v9, p0, Lcom/lltskb/lltskb/view/RunChartView;->mAuxDay:I

    move-object v7, p1

    invoke-virtual/range {v4 .. v9}, Lcom/lltskb/lltskb/engine/Rule;->outOfDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)B

    move-result v1

    const/4 v2, 0x3

    if-nez v1, :cond_2

    .line 325
    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchedule:Lcom/lltskb/lltskb/engine/Schedule;

    iget v4, p0, Lcom/lltskb/lltskb/view/RunChartView;->mTrainIdx:I

    iget v5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStationIdx:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    .line 326
    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    aget-byte v3, v3, v0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    goto :goto_0

    .line 329
    :cond_1
    iget-object v3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchedule:Lcom/lltskb/lltskb/engine/Schedule;

    iget v5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mTrainIdx:I

    const/16 v6, 0x4000

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v7, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    invoke-virtual {v3, v5, v6, p1, v7}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    .line 330
    iget-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mSchData:[B

    aget-byte p1, p1, v0

    if-ne p1, v4, :cond_2

    goto :goto_0

    :cond_2
    move v2, v1

    :goto_0
    return v2

    :cond_3
    :goto_1
    return v0
.end method

.method private initView()V
    .locals 2

    const-string v0, "RunChartView"

    const-string v1, "initView"

    .line 178
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x23

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellWidth:I

    .line 180
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    .line 181
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    .line 182
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentDate:Ljava/util/Date;

    return-void
.end method

.method private updateFirstDate()V
    .locals 7

    .line 259
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    .line 260
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 262
    iget-object v2, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 263
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/Calendar;->set(III)V

    const/4 v2, 0x7

    .line 264
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v2, v1

    .line 266
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    mul-int/lit8 v2, v2, 0x18

    mul-int/lit16 v2, v2, 0xe10

    int-to-long v1, v2

    const-wide/16 v5, 0x3e8

    mul-long v1, v1, v5

    sub-long/2addr v3, v1

    .line 267
    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 269
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStartDate:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public getCurrentMonth()Ljava/util/Date;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    return-object v0
.end method

.method public nextMonth()V
    .locals 5

    .line 282
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, 0x2

    .line 284
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x1

    const/16 v4, 0xb

    if-ne v2, v4, :cond_0

    .line 285
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/2addr v1, v3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0

    .line 287
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v0, v2, v1, v3}, Ljava/util/Calendar;->set(III)V

    .line 289
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLayout l="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " r="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " changed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RunChartView"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iget v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    sub-int/2addr p5, p3

    add-int/lit8 p5, p5, -0x6

    .line 35
    div-int/lit8 p5, p5, 0x6

    iput p5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    .line 36
    iget p3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    const/16 p5, 0x14

    if-ge p3, p5, :cond_0

    .line 37
    iput v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    :cond_0
    if-nez p1, :cond_1

    return-void

    :cond_1
    sub-int/2addr p4, p2

    .line 43
    div-int/lit8 p4, p4, 0x7

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 44
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getChildCount()I

    move-result p3

    if-ge p2, p3, :cond_3

    .line 45
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/view/RunChartView;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/LinearLayout;

    .line 46
    iget p5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    mul-int v0, p2, p5

    mul-int/lit8 v1, p4, 0x7

    mul-int v2, p2, p5

    add-int/2addr v2, p5

    invoke-virtual {p3, p1, v0, v1, v2}, Landroid/widget/LinearLayout;->layout(IIII)V

    const/4 p5, 0x0

    .line 47
    :goto_1
    invoke-virtual {p3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge p5, v0, :cond_2

    .line 48
    invoke-virtual {p3, p5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    mul-int v1, p5, p4

    add-int v2, v1, p4

    .line 52
    iget v3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCellHeight:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public prevMonth()V
    .locals 4

    .line 293
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, 0x2

    .line 295
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_0

    .line 296
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int/2addr v1, v3

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0

    .line 298
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int/2addr v1, v3

    invoke-virtual {v0, v2, v1, v3}, Ljava/util/Calendar;->set(III)V

    .line 300
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    return-void
.end method

.method public setCurrentDate(Ljava/util/Date;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentDate:Ljava/util/Date;

    .line 274
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentMonth:Ljava/util/Date;

    return-void
.end method

.method public setParams(Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMinDate:Ljava/lang/String;

    .line 305
    iput-object p2, p0, Lcom/lltskb/lltskb/view/RunChartView;->mMaxDate:Ljava/lang/String;

    .line 306
    iput p3, p0, Lcom/lltskb/lltskb/view/RunChartView;->mRuleIndex:I

    .line 307
    iput p4, p0, Lcom/lltskb/lltskb/view/RunChartView;->mAuxDay:I

    .line 308
    iput p5, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStationIdx:I

    .line 309
    iput p6, p0, Lcom/lltskb/lltskb/view/RunChartView;->mTrainIdx:I

    .line 311
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "setParams min="

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " max="

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " rule="

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " auxDay="

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "RunChartView"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateRunChart(Z)V
    .locals 14

    .line 203
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 204
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView;->createRunChart()V

    .line 206
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/RunChartView;->updateFirstDate()V

    .line 207
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 208
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x5

    if-ge v3, v4, :cond_6

    add-int/lit8 v5, v3, 0x1

    .line 210
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/RunChartView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    const/16 v7, 0x8

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    .line 211
    :goto_1
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v7, 0x0

    :goto_2
    const/4 v8, 0x7

    if-ge v7, v8, :cond_5

    .line 213
    iget-object v8, p0, Lcom/lltskb/lltskb/view/RunChartView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    mul-int/lit8 v10, v3, 0x7

    add-int/2addr v10, v7

    mul-int/lit8 v10, v10, 0x18

    mul-int/lit16 v10, v10, 0xe10

    int-to-long v10, v10

    const-wide/16 v12, 0x3e8

    mul-long v10, v10, v12

    add-long/2addr v8, v10

    .line 214
    invoke-virtual {v1, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 215
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/lltskb/lltskb/view/RunChartView$DayCell;

    .line 216
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 218
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 221
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x2

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    add-int/2addr v13, v12

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, "\u6708"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0xc

    .line 222
    invoke-virtual {v10, v12}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setFontSize(I)V

    goto :goto_3

    .line 224
    :cond_2
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    .line 226
    :goto_3
    invoke-virtual {v10, v11}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setText(Ljava/lang/String;)V

    .line 228
    invoke-virtual {v1, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 229
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/view/RunChartView;->getRunStatus(Ljava/util/Date;)I

    move-result v8

    if-nez v8, :cond_3

    .line 230
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0600c5

    invoke-static {v8, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v8

    invoke-virtual {v10, v8}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setBackColor(I)V

    goto :goto_4

    .line 232
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/RunChartView;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0600c6

    invoke-static {v8, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v8

    invoke-virtual {v10, v8}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setBackColor(I)V

    .line 235
    :goto_4
    iget-object v8, p0, Lcom/lltskb/lltskb/view/RunChartView;->mCurrentDate:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v11

    cmp-long v13, v8, v11

    if-nez v13, :cond_4

    const/4 v8, -0x1

    .line 236
    invoke-virtual {v10, v8}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setTextColor(I)V

    goto :goto_5

    :cond_4
    const/high16 v8, -0x1000000

    .line 238
    invoke-virtual {v10, v8}, Lcom/lltskb/lltskb/view/RunChartView$DayCell;->setTextColor(I)V

    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_5
    move v3, v5

    goto/16 :goto_0

    :cond_6
    return-void
.end method
