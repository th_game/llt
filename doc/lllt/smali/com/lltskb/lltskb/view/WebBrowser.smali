.class public Lcom/lltskb/lltskb/view/WebBrowser;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "WebBrowser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WebBrowser"


# instance fields
.field private mCloseOnBack:Z

.field private mCookie:Ljava/lang/String;

.field private mCurrentUrl:Ljava/lang/String;

.field private mDownloadTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAliBus:Z

.field private mIsLogin:Z

.field private mLastPressBack:Z

.field private mProgBar:Landroid/widget/ProgressBar;

.field private mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

.field private mTitleView:Landroid/widget/TextView;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 185
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 131
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mLastPressBack:Z

    .line 173
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mIsAliBus:Z

    return-void
.end method

.method private HandleQunar(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    if-eqz p2, :cond_0

    const-string p1, "qunaraphone://"

    .line 669
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 671
    new-instance p1, Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p1, v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 672
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 673
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 674
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    const-string p1, "WebBrowser"

    const-string p2, "start quanr client"

    .line 675
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->destroyWebView()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/webkit/WebView;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->handleAlipay(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1100(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->HandleQunar(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1200(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/ProgressBar;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgBar:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/lltskb/lltskb/view/WebBrowser;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$1700(Lcom/lltskb/lltskb/view/WebBrowser;)Lcom/lltskb/lltskb/view/LLTProgressDialog;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->showAlert(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/os/AsyncTask;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mDownloadTask:Landroid/os/AsyncTask;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/WebBrowser;)Ljava/lang/String;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCurrentUrl:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCurrentUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/lltskb/lltskb/view/WebBrowser;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mLastPressBack:Z

    return p1
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/view/WebBrowser;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCloseOnBack:Z

    return p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/view/WebBrowser;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mIsLogin:Z

    return p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/view/WebBrowser;)Landroid/widget/TextView;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mTitleView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/view/WebBrowser;Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startDownload(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->handleIntent(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/view/WebBrowser;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->handleWeixin(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private destroyWebView()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 91
    :try_start_0
    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 93
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 97
    new-instance v0, Lcom/lltskb/lltskb/view/WebBrowser$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/WebBrowser$3;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/WebBrowser;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method private handleAlipay(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 8

    .line 610
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const-string p1, "://platformapi/startapp?saId="

    .line 625
    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    const-string v1, "alipayqr:"

    .line 627
    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "alipays:"

    const/4 v3, 0x1

    const-string v4, "start alipays"

    const-string v5, "android.intent.action.VIEW"

    const-string v6, "intent:"

    const-string v7, "WebBrowser"

    if-nez v1, :cond_3

    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_1

    :cond_1
    if-ltz p1, :cond_2

    .line 639
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "other alipay uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "alipays"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 641
    new-instance p2, Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {p2, v5, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 642
    invoke-static {p0, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 643
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 644
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    .line 645
    invoke-static {v7, v4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_2
    const-string p1, "alipays%3A%2F%2Fplatformapi%2Fstartapp%3F"

    .line 648
    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-lez p1, :cond_5

    const-string v1, "alipay third method"

    .line 649
    invoke-static {v7, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "utf-8"

    invoke-static {p1, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 654
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 657
    :goto_0
    new-instance p1, Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-direct {p1, v5, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 658
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 659
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 660
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    .line 661
    invoke-static {v7, v4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 628
    :cond_3
    :goto_1
    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 629
    invoke-virtual {p2, v6, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 631
    :cond_4
    new-instance p1, Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-direct {p1, v5, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 632
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 633
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 634
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    .line 635
    invoke-static {v7, v4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_5
    return v0
.end method

.method private handleIntent(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .line 562
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "intent://"

    .line 565
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    const-string v0, "WebBrowser"

    const-string v2, "handleIntent"

    .line 568
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 570
    :try_start_0
    invoke-static {p2, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p2

    .line 572
    invoke-static {p0, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 573
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 574
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 575
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    goto :goto_0

    :cond_2
    const-string v3, "browser_fallback_url"

    .line 577
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 578
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception p1

    .line 585
    invoke-virtual {p1}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method private handleLocalStorate()V
    .locals 4

    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "window.localStorage.setItem(\'closeDate\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\');"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:(function({var localStorage = window.localStorage;localStorage.setItem(\'closeDate\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\')})()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript: var removeAd = function(){var elems = document.getElementsByClassName(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "J_smartBanner trip-smart-banner "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\');for (var i=0;i<elems.length;i+=1){    elems[i].style.display = \'none\';};};removeAd();"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 549
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:var removeAd2 = function(){window.localStorage.setItem(\'closeDate\',\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "\');};removeAd2();"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 551
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private handleWeixin(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .line 591
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const-string p1, "weixin:"

    .line 595
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 596
    new-instance p1, Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {p1, v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 597
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isIntentHasActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 598
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/WebBrowser;->startActivity(Landroid/content/Intent;)V

    .line 599
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    const-string p1, "WebBrowser"

    const-string p2, "start weixin"

    .line 600
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method private initView()V
    .locals 8

    const/4 v0, 0x1

    .line 202
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/WebBrowser;->requestWindowFeature(I)Z

    .line 204
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "web_url"

    .line 205
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "web_post"

    .line 206
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "web_closeonback"

    .line 207
    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCloseOnBack:Z

    .line 208
    iput-object v2, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCurrentUrl:Ljava/lang/String;

    const-string v5, "https://h5.m.taobao.com/trip/car/search/index.html?ttid=12oap0000083"

    .line 210
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mIsAliBus:Z

    const-string v5, "https://kyfw.12306.cn/otn/login/init"

    .line 211
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mIsLogin:Z

    .line 213
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 216
    :try_start_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    .line 218
    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "WebBrowser"

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const v5, 0x7f0b009a

    .line 221
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->setContentView(I)V

    const v5, 0x7f09031a

    .line 223
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/webkit/WebView;

    iput-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    const v5, 0x7f09017c

    .line 224
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    const v5, 0x7f090319

    .line 225
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    iput-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgBar:Landroid/widget/ProgressBar;

    const v5, 0x7f090223

    .line 226
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mTitleView:Landroid/widget/TextView;

    .line 227
    iget-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mTitleView:Landroid/widget/TextView;

    const/high16 v6, 0x41600000    # 14.0f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    const v5, 0x7f0900fb

    .line 230
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 231
    new-instance v6, Lcom/lltskb/lltskb/view/WebBrowser$5;

    invoke-direct {v6, p0}, Lcom/lltskb/lltskb/view/WebBrowser$5;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f090067

    .line 244
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 245
    invoke-virtual {v5, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 246
    new-instance v6, Lcom/lltskb/lltskb/view/WebBrowser$6;

    invoke-direct {v6, p0}, Lcom/lltskb/lltskb/view/WebBrowser$6;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f09004a

    .line 255
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/view/WebBrowser;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 256
    new-instance v6, Lcom/lltskb/lltskb/view/WebBrowser$7;

    invoke-direct {v6, p0}, Lcom/lltskb/lltskb/view/WebBrowser$7;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    invoke-virtual {v5, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 266
    iget-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->requestFocusFromTouch()Z

    .line 267
    iget-object v5, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    const/16 v6, 0x64

    .line 268
    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 269
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 270
    invoke-virtual {v5, v3}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 272
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 273
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 275
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 276
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 278
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    const-string v6, "utf-8"

    .line 279
    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 281
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 282
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 283
    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 284
    sget-object v6, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 286
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_0

    .line 287
    invoke-virtual {v5, v3}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 323
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->solveInputIssue()V

    .line 325
    iget-object v3, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    if-eqz v3, :cond_4

    .line 326
    new-instance v5, Lcom/lltskb/lltskb/view/WebBrowser$8;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/view/WebBrowser$8;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 471
    iget-object v3, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    new-instance v5, Lcom/lltskb/lltskb/view/WebBrowser$9;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/view/WebBrowser$9;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 500
    iget-object v3, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCurrentUrl:Ljava/lang/String;

    if-eqz v3, :cond_1

    const-string v5, "qunar.com"

    .line 503
    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 504
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->prepareCookieForQunar()V

    .line 505
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 506
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v5

    .line 507
    invoke-virtual {v5, v0}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 508
    invoke-virtual {v5}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    .line 509
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCookie:Ljava/lang/String;

    invoke-virtual {v5, v3, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_2

    const-string v0, "12306.cn"

    .line 511
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 512
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->prepareCookieFor12306()V

    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    const-string v0, "web_params"

    .line 516
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    goto :goto_2

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 523
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/lltskb/lltskb/view/WebBrowser$10;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/WebBrowser$10;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    :cond_4
    return-void
.end method

.method private prepareCookieFor12306()V
    .locals 0

    .line 704
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    return-void
.end method

.method private prepareCookieForQunar()V
    .locals 6

    .line 849
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 850
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x1b7740

    add-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 851
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QN75="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ";path=/;domain=.qunar.com;expires="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    invoke-virtual {v1}, Ljava/util/Date;->toGMTString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCookie:Ljava/lang/String;

    return-void
.end method

.method private showAlert(Ljava/lang/String;)V
    .locals 2

    .line 795
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 797
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const v1, 0x7f0e0143

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x10

    .line 800
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    const v1, 0x7f0d010f

    .line 801
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(I)V

    .line 802
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 803
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V

    .line 805
    new-instance p1, Lcom/lltskb/lltskb/view/WebBrowser$13;

    invoke-direct {p1, p0, v0}, Lcom/lltskb/lltskb/view/WebBrowser$13;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;Lcom/lltskb/lltskb/view/LLTProgressDialog;)V

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showProgressDialog()V
    .locals 3

    .line 814
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    if-nez v0, :cond_0

    .line 815
    new-instance v0, Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const v1, 0x7f0e0143

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMax(I)V

    .line 820
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMin(I)V

    .line 822
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 824
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const-string v2, "\u4e0b\u8f7d"

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 825
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const-string v2, "\u6b63\u5728\u4e0b\u8f7d"

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 826
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setIndeterminate(Z)V

    .line 827
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    new-instance v1, Lcom/lltskb/lltskb/view/WebBrowser$14;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/WebBrowser$14;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setCancelBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 838
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V

    return-void
.end method

.method private solveInputIssue()V
    .locals 2

    .line 683
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 684
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocusFromTouch()Z

    .line 685
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/lltskb/lltskb/view/WebBrowser$11;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/WebBrowser$11;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private startDownload(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    const-string v1, "\u5f00\u59cb\u4e0b\u8f7d"

    .line 720
    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 722
    new-instance v1, Lcom/lltskb/lltskb/view/WebBrowser$12;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/WebBrowser$12;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mDownloadTask:Landroid/os/AsyncTask;

    .line 791
    iget-object v1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mDownloadTask:Landroid/os/AsyncTask;

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private tryQuitApp()V
    .locals 4

    const-string v0, "WebBrowser"

    const-string v1, "tryQuitApp"

    .line 134
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mLastPressBack:Z

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->finish()V

    goto :goto_0

    :cond_0
    const v0, 0x7f0d00ac

    const/4 v1, 0x0

    .line 138
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/lltskb/lltskb/view/WebBrowser$4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/view/WebBrowser$4;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    .line 147
    iput-boolean v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mLastPressBack:Z

    :goto_0
    return-void
.end method


# virtual methods
.method clearHistory()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Lcom/lltskb/lltskb/view/WebBrowser$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/WebBrowser$2;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/view/WebBrowser;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 177
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 179
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->initView()V

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    const-string v0, "WebBrowser"

    const-string v1, "onDestroy"

    .line 64
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->clearHistory()V

    .line 66
    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    move-result-wide v0

    .line 67
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    new-instance v3, Lcom/lltskb/lltskb/view/WebBrowser$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/view/WebBrowser$1;-><init>(Lcom/lltskb/lltskb/view/WebBrowser;)V

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 74
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 155
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result p1

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mCloseOnBack:Z

    if-nez p1, :cond_0

    .line 156
    iget-object p1, p0, Lcom/lltskb/lltskb/view/WebBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->goBack()V

    return p2

    .line 160
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/WebBrowser;->tryQuitApp()V

    return p2

    .line 165
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method
