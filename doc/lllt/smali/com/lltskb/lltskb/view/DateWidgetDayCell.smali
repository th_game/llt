.class public Lcom/lltskb/lltskb/view/DateWidgetDayCell;
.super Landroid/view/View;
.source "DateWidgetDayCell.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;
    }
.end annotation


# static fields
.field public static ANIM_ALPHA_DURATION:I = 0x64

.field private static Calendar_DayBgColor:I

.field private static isHoliday_BgColor:I

.field private static isToday_BgColor:I


# instance fields
.field private bHoliday:Z

.field private bIsEnableDays:Z

.field private bSelected:Z

.field private bToday:Z

.field private bTouchedDown:Z

.field private fTextSize:I

.field private iDateDay:I

.field private iDateMonth:I

.field private iDateYear:I

.field private isPresentMonth_FontColor:I

.field private itemClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

.field private mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

.field private maxEnableDays:J

.field private pt:Landroid/graphics/Paint;

.field private rect:Landroid/graphics/RectF;

.field private sDate:Ljava/lang/String;

.field private unPresentMonth_FontColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIJ)V
    .locals 3

    .line 76
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x1c

    .line 39
    iput v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    const/4 v1, 0x0

    .line 42
    iput-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->itemClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    .line 43
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    .line 44
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    const-string v1, ""

    .line 45
    iput-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->sDate:Ljava/lang/String;

    const/4 v1, 0x0

    .line 49
    iput v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateYear:I

    .line 50
    iput v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    .line 51
    iput v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    .line 54
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    .line 55
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bIsEnableDays:Z

    .line 56
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bToday:Z

    .line 57
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bTouchedDown:Z

    .line 58
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bHoliday:Z

    .line 62
    iput v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isPresentMonth_FontColor:I

    .line 63
    iput v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->unPresentMonth_FontColor:I

    const-wide/16 v1, 0x3c

    .line 68
    iput-wide v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->maxEnableDays:J

    .line 77
    iput-wide p4, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->maxEnableDays:J

    .line 78
    iput v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    const/16 p4, 0x14

    .line 79
    invoke-static {p1, p4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p4

    iput p4, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    const/4 p4, 0x1

    .line 80
    invoke-virtual {p0, p4}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setFocusable(Z)V

    .line 81
    new-instance p4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p4, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p4}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->initColor(Landroid/content/Context;)V

    return-void
.end method

.method private calcEnableDays()Z
    .locals 7

    .line 127
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 129
    iget-wide v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->maxEnableDays:J

    long-to-int v2, v1

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 130
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 131
    iget v2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateYear:I

    iget v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    iget v4, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Calendar;->set(III)V

    .line 132
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/4 v0, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    return v0

    .line 134
    :cond_0
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0xf731400

    sub-long/2addr v3, v5

    cmp-long v5, v1, v3

    if-gez v5, :cond_1

    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bToday:Z

    if-nez v1, :cond_1

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private drawDayView(Landroid/graphics/Canvas;Z)V
    .locals 10

    .line 163
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    iget-object p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bHoliday:Z

    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bToday:Z

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getColorBkg(ZZ)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    iget-object p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_1
    :goto_0
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 167
    new-instance p2, Landroid/graphics/LinearGradient;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    const v6, -0x55ab00

    const/16 v7, -0x2245

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v1, p2

    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    goto :goto_1

    :cond_2
    move-object p2, v0

    .line 171
    :goto_1
    iget-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    if-eqz v1, :cond_3

    .line 172
    new-instance p2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    const v7, -0xddaa67

    const v8, -0x442201

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v2, p2

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    :cond_3
    if-eqz p2, :cond_4

    .line 177
    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 178
    iget-object p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 181
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_2
    return-void
.end method

.method public static getColorBkg(ZZ)I
    .locals 0

    if-eqz p1, :cond_0

    .line 291
    sget p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isToday_BgColor:I

    return p0

    :cond_0
    if-eqz p0, :cond_1

    .line 293
    sget p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isHoliday_BgColor:I

    return p0

    .line 294
    :cond_1
    sget p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->Calendar_DayBgColor:I

    return p0
.end method

.method private getQingmingDate()I
    .locals 5

    .line 223
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateYear:I

    rem-int/lit8 v0, v0, 0x64

    int-to-double v1, v0

    const-wide v3, 0x3fcf0068db8bac71L    # 0.2422

    .line 224
    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v1, v1, v3

    const-wide v3, 0x40133d70a3d70a3dL    # 4.81

    add-double/2addr v1, v3

    double-to-int v1, v1

    div-int/lit8 v0, v0, 0x4

    sub-int/2addr v1, v0

    return v1
.end method

.method private getTextHeight()I
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private initColor(Landroid/content/Context;)V
    .locals 2

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isPresentMonth_FontColor:I

    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0600dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->unPresentMonth_FontColor:I

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isToday_BgColor:I

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->Calendar_DayBgColor:I

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f060084

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    sput p1, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isHoliday_BgColor:I

    return-void
.end method

.method public static startAlphaAnimIn(Landroid/view/View;)V
    .locals 3

    .line 352
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 353
    sget v1, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->ANIM_ALPHA_DURATION:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 354
    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->startNow()V

    .line 355
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public IsViewFocused()Z
    .locals 1

    .line 157
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bTouchedDown:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public doItemClick()V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->itemClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    if-eqz v0, :cond_0

    .line 312
    invoke-interface {v0, p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;->OnClick(Lcom/lltskb/lltskb/view/DateWidgetDayCell;)V

    :cond_0
    return-void
.end method

.method public drawDayNumber(Landroid/graphics/Canvas;)V
    .locals 8

    .line 230
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 231
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 232
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 233
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 234
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 235
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->isPresentMonth_FontColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 236
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 238
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bIsEnableDays:Z

    if-nez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->unPresentMonth_FontColor:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 241
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    const v3, -0xffff01

    if-ne v0, v2, :cond_1

    .line 242
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 244
    :cond_1
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 247
    :cond_2
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bToday:Z

    if-eqz v0, :cond_3

    .line 248
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 249
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const/high16 v4, -0x10000

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 252
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget-object v4, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    shr-int/2addr v4, v2

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->sDate:Ljava/lang/String;

    .line 253
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    shr-int/2addr v4, v2

    sub-int/2addr v0, v4

    .line 255
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHeight()I

    move-result v5

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getTextHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Paint$FontMetrics;->bottom:F

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 258
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->sDate:Ljava/lang/String;

    int-to-float v0, v0

    int-to-float v6, v4

    iget-object v7, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 260
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChinaDay()I

    move-result v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChinaDayString(I)Ljava/lang/String;

    move-result-object v0

    .line 261
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    iget v6, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 262
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 263
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    const v6, -0x777778

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 264
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChinaDay()I

    move-result v5

    if-ne v5, v2, :cond_4

    .line 265
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChinaMonthString()Ljava/lang/String;

    move-result-object v0

    .line 267
    :cond_4
    iget-object v5, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 268
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHoliday()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 271
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object v0, v1

    .line 276
    :cond_5
    iget-object v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    shr-int/2addr v3, v2

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    .line 277
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    shr-int/lit8 v2, v3, 0x1

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 279
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getTextHeight()I

    move-result v2

    add-int/2addr v4, v2

    add-int/lit8 v4, v4, 0x2

    int-to-float v2, v4

    iget-object v3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->pt:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getDate()Ljava/util/Calendar;
    .locals 3

    .line 94
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 96
    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateYear:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 97
    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 98
    iget v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    return-object v0
.end method

.method public getHoliday()Ljava/lang/String;
    .locals 3

    .line 193
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LunarCalendar;->getChineseHoliday()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 197
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_1

    const-string v0, "\u5143\u65e6"

    return-object v0

    .line 199
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 202
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getQingmingDate()I

    move-result v1

    if-ne v0, v1, :cond_7

    const-string v0, "\u6e05\u660e\u8282"

    return-object v0

    :cond_2
    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 204
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_3

    const-string v0, "\u52b3\u52a8\u8282"

    return-object v0

    .line 206
    :cond_3
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_4

    const-string v0, "\u513f\u7ae5\u8282"

    return-object v0

    .line 208
    :cond_4
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_5

    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_5

    const-string v0, "\u5efa\u515a\u8282"

    return-object v0

    .line 210
    :cond_5
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_6

    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_6

    const-string v0, "\u5efa\u519b\u8282"

    return-object v0

    .line 212
    :cond_6
    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    const/16 v2, 0x9

    if-ne v0, v2, :cond_7

    iget v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    if-ne v0, v1, :cond_7

    const-string v0, "\u56fd\u5e86\u8282"

    return-object v0

    :cond_7
    const/4 v0, 0x0

    return-object v0
.end method

.method public isEnableDays()Z
    .locals 1

    .line 138
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bIsEnableDays:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 145
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 147
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 148
    iget-object v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->rect:Landroid/graphics/RectF;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 150
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->IsViewFocused()Z

    move-result v0

    .line 152
    invoke-direct {p0, p1, v0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->drawDayView(Landroid/graphics/Canvas;Z)V

    .line 153
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->drawDayNumber(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 342
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p2

    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    .line 345
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->doItemClick()V

    :cond_1
    return p2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 319
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 321
    iput-boolean v2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bTouchedDown:Z

    .line 322
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->invalidate()V

    .line 323
    invoke-static {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->startAlphaAnimIn(Landroid/view/View;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 325
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 327
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bTouchedDown:Z

    .line 328
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->invalidate()V

    const/4 v0, 0x1

    .line 330
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-ne p1, v2, :cond_2

    .line 332
    iput-boolean v1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bTouchedDown:Z

    .line 333
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->invalidate()V

    .line 334
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->doItemClick()V

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public setData(IIILjava/lang/Boolean;Ljava/lang/Boolean;I)V
    .locals 0

    .line 105
    iput p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateYear:I

    .line 106
    iput p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    .line 107
    iput p3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    .line 109
    iget p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateDay:I

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 110
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget p3, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->iDateMonth:I

    add-int/2addr p3, p2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\u6708"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->sDate:Ljava/lang/String;

    .line 111
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 p2, 0x12

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->fTextSize:I

    goto :goto_0

    .line 113
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->sDate:Ljava/lang/String;

    .line 116
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->calcEnableDays()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bIsEnableDays:Z

    .line 117
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bToday:Z

    .line 118
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bHoliday:Z

    .line 120
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->getDate()Ljava/util/Calendar;

    move-result-object p1

    .line 122
    new-instance p2, Lcom/lltskb/lltskb/utils/LunarCalendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/utils/LunarCalendar;-><init>(Ljava/util/Date;)V

    iput-object p2, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->mLunar:Lcom/lltskb/lltskb/utils/LunarCalendar;

    return-void
.end method

.method public setItemClick(Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;)V
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->itemClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    return-void
.end method

.method public setSelected(Z)V
    .locals 1

    .line 300
    iget-boolean v0, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    if-eq v0, p1, :cond_0

    .line 301
    iput-boolean p1, p0, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->bSelected:Z

    .line 302
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->invalidate()V

    :cond_0
    return-void
.end method
