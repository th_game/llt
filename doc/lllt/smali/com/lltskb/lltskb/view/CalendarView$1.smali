.class Lcom/lltskb/lltskb/view/CalendarView$1;
.super Ljava/lang/Object;
.source "CalendarView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarTitle()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/view/CalendarView;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/view/CalendarView;Landroid/view/View;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 175
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$000(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$010(Lcom/lltskb/lltskb/view/CalendarView;)I

    .line 180
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 181
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v1}, Lcom/lltskb/lltskb/view/CalendarView;->access$100(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 182
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$200(Lcom/lltskb/lltskb/view/CalendarView;)V

    .line 183
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    sget-object v1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    iput-object v1, v0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 184
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    iget-object v1, v0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/CalendarView;->GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 185
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$300(Lcom/lltskb/lltskb/view/CalendarView;)Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    .line 186
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->this$0:Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {v0}, Lcom/lltskb/lltskb/view/CalendarView;->access$000(Lcom/lltskb/lltskb/view/CalendarView;)I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 187
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView$1;->val$view:Landroid/view/View;

    const v0, 0x7f09005f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 188
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
