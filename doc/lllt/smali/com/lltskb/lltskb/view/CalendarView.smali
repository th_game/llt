.class public Lcom/lltskb/lltskb/view/CalendarView;
.super Landroid/widget/LinearLayout;
.source "CalendarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/view/CalendarView$Next_MonthOnClickListener;,
        Lcom/lltskb/lltskb/view/CalendarView$Pre_MonthOnClickListener;,
        Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;
    }
.end annotation


# static fields
.field public static calStartDate:Ljava/util/Calendar;


# instance fields
.field private Calendar_Width:I

.field private Cell_Width:I

.field private calCalendar:Ljava/util/Calendar;

.field private calSelected:Ljava/util/Calendar;

.field private calToday:Ljava/util/Calendar;

.field private days:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/lltskb/lltskb/view/DateWidgetDayCell;",
            ">;"
        }
    .end annotation
.end field

.field endDate:Ljava/util/Calendar;

.field flag:[Ljava/lang/Boolean;

.field private iFirstDayOfWeek:I

.field private iMonthViewCurrentMonth:I

.field private iMonthViewCurrentYear:I

.field private layContent:Landroid/widget/LinearLayout;

.field mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

.field private mMaxEnableDays:J

.field private mOnDayCellClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

.field private mPageIndex:I

.field startDate:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 71
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    .line 34
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v1, 0x0

    .line 37
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    .line 38
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    const/4 v2, 0x2

    .line 39
    iput v2, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    .line 41
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    .line 42
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    .line 44
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    const-wide/16 v1, 0x3c

    .line 46
    iput-wide v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mMaxEnableDays:J

    .line 48
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->flag:[Ljava/lang/Boolean;

    .line 49
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 50
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 52
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    .line 412
    new-instance v0, Lcom/lltskb/lltskb/view/CalendarView$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/CalendarView$3;-><init>(Lcom/lltskb/lltskb/view/CalendarView;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mOnDayCellClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    .line 72
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 3

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    .line 34
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v1, 0x0

    .line 37
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    .line 38
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    const/4 v2, 0x2

    .line 39
    iput v2, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    .line 41
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    .line 42
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    .line 44
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    const-wide/16 v1, 0x3c

    .line 46
    iput-wide v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mMaxEnableDays:J

    .line 48
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->flag:[Ljava/lang/Boolean;

    .line 49
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 50
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 52
    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    .line 412
    new-instance v0, Lcom/lltskb/lltskb/view/CalendarView$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/view/CalendarView$3;-><init>(Lcom/lltskb/lltskb/view/CalendarView;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mOnDayCellClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    .line 63
    iput-wide p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->mMaxEnableDays:J

    .line 64
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 27
    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    .line 34
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v0, 0x0

    .line 37
    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    .line 38
    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    const/4 v1, 0x2

    .line 39
    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    .line 41
    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    .line 42
    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    .line 44
    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    const-wide/16 v0, 0x3c

    .line 46
    iput-wide v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mMaxEnableDays:J

    .line 48
    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->flag:[Ljava/lang/Boolean;

    .line 49
    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 50
    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 52
    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    .line 412
    new-instance p2, Lcom/lltskb/lltskb/view/CalendarView$3;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/view/CalendarView$3;-><init>(Lcom/lltskb/lltskb/view/CalendarView;)V

    iput-object p2, p0, Lcom/lltskb/lltskb/view/CalendarView;->mOnDayCellClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    .line 81
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private UpdateStartDateForMonth()V
    .locals 6

    .line 272
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    .line 273
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    .line 275
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/4 v3, 0x0

    const/16 v4, 0xb

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 276
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/16 v4, 0xc

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 277
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    const/16 v4, 0xd

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 280
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    const/4 v4, 0x6

    const/4 v5, 0x7

    if-ne v0, v1, :cond_0

    .line 283
    sget-object v3, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    const/4 v3, 0x6

    :cond_0
    if-ne v0, v2, :cond_1

    .line 289
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sub-int/2addr v0, v2

    if-gez v0, :cond_2

    const/4 v0, 0x6

    goto :goto_0

    :cond_1
    move v0, v3

    .line 294
    :cond_2
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    neg-int v0, v0

    iget v2, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    mul-int/lit8 v2, v2, 0x23

    add-int/2addr v0, v2

    invoke-virtual {v1, v5, v0}, Ljava/util/Calendar;->add(II)V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    return p0
.end method

.method static synthetic access$008(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    return v0
.end method

.method static synthetic access$010(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/view/CalendarView;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->UpdateStartDateForMonth()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/view/CalendarView;)Lcom/lltskb/lltskb/view/DateWidgetDayCell;
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->updateCalendar()Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/view/CalendarView;)Ljava/util/Calendar;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    return p0
.end method

.method static synthetic access$502(Lcom/lltskb/lltskb/view/CalendarView;I)I
    .locals 0

    .line 24
    iput p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    return p1
.end method

.method static synthetic access$508(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    return v0
.end method

.method static synthetic access$510(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    return v0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    return p0
.end method

.method static synthetic access$608(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    return v0
.end method

.method static synthetic access$610(Lcom/lltskb/lltskb/view/CalendarView;)I
    .locals 2

    .line 24
    iget v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentYear:I

    return v0
.end method

.method private createLayout(I)Landroid/widget/LinearLayout;
    .locals 4

    .line 137
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 138
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-object v0
.end method

.method private generateCalendarHeader()Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    .line 148
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/CalendarView;->createLayout(I)Landroid/widget/LinearLayout;

    move-result-object v1

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    .line 152
    new-instance v2, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    .line 153
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v5

    const/16 v6, 0x23

    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;-><init>(Landroid/content/Context;II)V

    .line 155
    iget v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/DayStyle;->getWeekDay(II)I

    move-result v3

    .line 156
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/DateWidgetDayHeader;->setData(I)V

    .line 157
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private generateCalendarMain()Landroid/view/View;
    .locals 4

    const/4 v0, 0x1

    .line 224
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/CalendarView;->createLayout(I)Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    .line 226
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    const/16 v1, 0x69

    const/16 v2, 0xff

    const/16 v3, 0x67

    invoke-static {v2, v1, v1, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 227
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarTitle()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 228
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarHeader()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 229
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarRow()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private generateCalendarRow()Landroid/view/View;
    .locals 9

    const/4 v0, 0x0

    .line 240
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/view/CalendarView;->createLayout(I)Landroid/widget/LinearLayout;

    move-result-object v1

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    .line 243
    new-instance v2, Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v6, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    iget-wide v7, p0, Lcom/lltskb/lltskb/view/CalendarView;->mMaxEnableDays:J

    move-object v3, v2

    move v5, v6

    invoke-direct/range {v3 .. v8}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;-><init>(Landroid/content/Context;IIJ)V

    .line 245
    iget-object v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mOnDayCellClick:Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setItemClick(Lcom/lltskb/lltskb/view/DateWidgetDayCell$OnItemClick;)V

    .line 246
    iget-object v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private generateCalendarTitle()Landroid/view/View;
    .locals 6

    .line 165
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0b0027

    const/4 v2, 0x0

    .line 166
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 168
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    mul-int/lit8 v2, v2, 0x7

    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x28

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f090064

    .line 170
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 171
    new-instance v2, Lcom/lltskb/lltskb/view/CalendarView$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/view/CalendarView$1;-><init>(Lcom/lltskb/lltskb/view/CalendarView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f09005f

    .line 193
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 194
    new-instance v3, Lcom/lltskb/lltskb/view/CalendarView$2;

    invoke-direct {v3, p0, v0}, Lcom/lltskb/lltskb/view/CalendarView$2;-><init>(Lcom/lltskb/lltskb/view/CalendarView;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    const/4 v4, 0x4

    const/4 v5, 0x0

    if-nez v3, :cond_0

    const/4 v3, 0x4

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    const/4 v3, 0x2

    if-ge v1, v3, :cond_1

    const/4 v4, 0x0

    :cond_1
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method

.method private getCalendarStartDate()Ljava/util/Calendar;
    .locals 5

    .line 255
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 256
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    iget v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 258
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 259
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 260
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    goto :goto_0

    .line 262
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 263
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iget v1, p0, Lcom/lltskb/lltskb/view/CalendarView;->iFirstDayOfWeek:I

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 266
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->UpdateStartDateForMonth()V

    .line 267
    sget-object v0, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3

    .line 94
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 95
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 99
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 100
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 104
    iget p1, v1, Landroid/graphics/Point;->y:I

    mul-int/lit8 p1, p1, 0x9

    div-int/lit8 p1, p1, 0xa

    iput p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    goto :goto_0

    .line 106
    :cond_0
    iget p1, v1, Landroid/graphics/Point;->x:I

    mul-int/lit8 p1, p1, 0x9

    div-int/lit8 p1, p1, 0xa

    iput p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    .line 109
    :goto_0
    iget p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Calendar_Width:I

    div-int/lit8 p1, p1, 0x7

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->Cell_Width:I

    .line 111
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->getCalendarStartDate()Ljava/util/Calendar;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    .line 112
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->generateCalendarMain()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->addView(Landroid/view/View;)V

    .line 113
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->updateCalendar()Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 116
    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->requestFocus()Z

    :cond_1
    const/4 p1, -0x1

    .line 118
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->setBackgroundColor(I)V

    .line 120
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->GetStartDate()Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 121
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/CalendarView;->GetTodayDate()Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    .line 123
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    return-void
.end method

.method private updateCalendar()Lcom/lltskb/lltskb/view/DateWidgetDayCell;
    .locals 19

    move-object/from16 v0, p0

    .line 301
    iget-object v1, v0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    const/4 v4, 0x1

    const-wide/16 v5, 0x0

    cmp-long v7, v1, v5

    if-eqz v7, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 302
    :goto_0
    iget-object v2, v0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 303
    iget-object v5, v0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 304
    iget-object v7, v0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 305
    iget-object v9, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    sget-object v10, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v9, 0x0

    move-object v10, v9

    const/4 v9, 0x0

    .line 307
    :goto_1
    iget-object v11, v0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v9, v11, :cond_7

    .line 308
    iget-object v11, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    invoke-virtual {v11, v4}, Ljava/util/Calendar;->get(I)I

    move-result v13

    .line 309
    iget-object v11, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    invoke-virtual {v11, v6}, Ljava/util/Calendar;->get(I)I

    move-result v14

    .line 310
    iget-object v11, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    invoke-virtual {v11, v8}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 311
    iget-object v11, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    const/4 v12, 0x7

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 312
    iget-object v3, v0, Lcom/lltskb/lltskb/view/CalendarView;->days:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    .line 317
    iget-object v12, v0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    invoke-virtual {v12, v4}, Ljava/util/Calendar;->get(I)I

    move-result v12

    if-ne v12, v13, :cond_1

    .line 318
    iget-object v12, v0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    invoke-virtual {v12, v6}, Ljava/util/Calendar;->get(I)I

    move-result v12

    if-ne v12, v14, :cond_1

    .line 319
    iget-object v12, v0, Lcom/lltskb/lltskb/view/CalendarView;->calToday:Ljava/util/Calendar;

    invoke-virtual {v12, v8}, Ljava/util/Calendar;->get(I)I

    move-result v12

    if-ne v12, v15, :cond_1

    const/4 v6, 0x7

    const/4 v12, 0x1

    goto :goto_2

    :cond_1
    const/4 v6, 0x7

    const/4 v12, 0x0

    :goto_2
    if-eq v11, v6, :cond_3

    if-ne v11, v4, :cond_2

    goto :goto_3

    :cond_2
    const/4 v6, 0x0

    goto :goto_4

    :cond_3
    :goto_3
    const/4 v6, 0x1

    :goto_4
    if-nez v14, :cond_4

    if-ne v15, v4, :cond_4

    const/4 v6, 0x1

    :cond_4
    if-eqz v1, :cond_5

    if-ne v7, v15, :cond_5

    if-ne v5, v14, :cond_5

    if-ne v2, v13, :cond_5

    const/4 v11, 0x1

    goto :goto_5

    :cond_5
    const/4 v11, 0x0

    .line 342
    :goto_5
    invoke-virtual {v3, v11}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setSelected(Z)V

    if-eqz v11, :cond_6

    move-object v10, v3

    .line 348
    :cond_6
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    iget v6, v0, Lcom/lltskb/lltskb/view/CalendarView;->iMonthViewCurrentMonth:I

    move-object v12, v3

    move/from16 v18, v6

    invoke-virtual/range {v12 .. v18}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->setData(IIILjava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 351
    iget-object v6, v0, Lcom/lltskb/lltskb/view/CalendarView;->calCalendar:Ljava/util/Calendar;

    invoke-virtual {v6, v8, v4}, Ljava/util/Calendar;->add(II)V

    .line 352
    invoke-virtual {v3}, Lcom/lltskb/lltskb/view/DateWidgetDayCell;->invalidate()V

    add-int/lit8 v9, v9, 0x1

    const/4 v6, 0x2

    goto :goto_1

    .line 355
    :cond_7
    iget-object v1, v0, Lcom/lltskb/lltskb/view/CalendarView;->layContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    return-object v10
.end method


# virtual methods
.method protected GetDateShortString(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 2

    .line 461
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    .line 462
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Calendar;

    const/4 v0, 0x5

    const/16 v1, 0x29

    .line 463
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->add(II)V

    return-object p1
.end method

.method public GetStartDate()Ljava/util/Calendar;
    .locals 4

    .line 441
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    .line 442
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x0

    const/16 v2, 0xb

    .line 443
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xc

    .line 444
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xd

    .line 445
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    .line 446
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    const/4 v2, 0x7

    .line 448
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    const/4 v3, 0x6

    :cond_0
    neg-int v1, v3

    .line 454
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    return-object v0
.end method

.method public GetTodayDate()Ljava/util/Calendar;
    .locals 3

    .line 429
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0xb

    .line 430
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xc

    .line 431
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xd

    .line 432
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    .line 433
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    return-object v0
.end method

.method public setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->mListener:Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;

    return-void
.end method

.method public setSelectedDate(III)V
    .locals 4

    .line 468
    iget-object v0, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 469
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    .line 470
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    const/4 p2, 0x5

    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->set(II)V

    .line 471
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->UpdateStartDateForMonth()V

    .line 472
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Calendar;

    const/16 p3, 0x23

    .line 473
    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->add(II)V

    const/4 v2, 0x0

    .line 474
    iput v2, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    .line 475
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->calSelected:Ljava/util/Calendar;

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 476
    iget v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    .line 477
    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 479
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->UpdateStartDateForMonth()V

    .line 480
    sget-object p1, Lcom/lltskb/lltskb/view/CalendarView;->calStartDate:Ljava/util/Calendar;

    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    .line 481
    iget-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->startDate:Ljava/util/Calendar;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->GetEndDate(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/view/CalendarView;->endDate:Ljava/util/Calendar;

    .line 482
    invoke-direct {p0}, Lcom/lltskb/lltskb/view/CalendarView;->updateCalendar()Lcom/lltskb/lltskb/view/DateWidgetDayCell;

    const p1, 0x7f090064

    .line 484
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x4

    if-eqz p1, :cond_2

    .line 486
    iget p3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    if-nez p3, :cond_1

    const/4 p3, 0x4

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const p1, 0x7f09005f

    .line 488
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/view/CalendarView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 490
    iget p3, p0, Lcom/lltskb/lltskb/view/CalendarView;->mPageIndex:I

    if-ge p3, v0, :cond_3

    const/4 p2, 0x0

    :cond_3
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void
.end method
