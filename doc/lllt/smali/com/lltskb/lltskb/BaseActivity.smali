.class public abstract Lcom/lltskb/lltskb/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;
    }
.end annotation


# static fields
.field private static final MY_PERMISSION_REQUEST_CODE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BaseActivity"


# instance fields
.field private isAlive:Z

.field private permissionRequestCallback:Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 55
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x1

    .line 33
    iput-boolean v0, p0, Lcom/lltskb/lltskb/BaseActivity;->isAlive:Z

    return-void
.end method


# virtual methods
.method public btn_faq(Landroid/view/View;)V
    .locals 1

    const-string p1, "BaseActivity"

    const-string v0, "btn_faq"

    .line 110
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://wap.lltskb.com/wtfk/"

    .line 111
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method protected dismissFragment(Ljava/lang/String;)Z
    .locals 3

    .line 71
    invoke-virtual {p0}, Lcom/lltskb/lltskb/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 72
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/lltskb/lltskb/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 74
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 75
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 76
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public finish()V
    .locals 2

    .line 94
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    .line 95
    instance-of v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;

    if-eqz v0, :cond_0

    return-void

    .line 99
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 100
    new-instance v0, Lcom/lltskb/lltskb/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/BaseActivity$1;-><init>(Lcom/lltskb/lltskb/BaseActivity;)V

    .line 105
    invoke-virtual {v0}, Lcom/lltskb/lltskb/BaseActivity$1;->trans()V

    :cond_1
    return-void
.end method

.method protected isAlive()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/lltskb/lltskb/BaseActivity;->isAlive:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 37
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 38
    iput-boolean p1, p0, Lcom/lltskb/lltskb/BaseActivity;->isAlive:Z

    .line 39
    invoke-virtual {p0}, Lcom/lltskb/lltskb/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/AppContext;

    invoke-virtual {p1, p0}, Lcom/lltskb/lltskb/AppContext;->addActivity(Landroid/app/Activity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 44
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 45
    invoke-virtual {p0}, Lcom/lltskb/lltskb/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/AppContext;

    invoke-virtual {v0, p0}, Lcom/lltskb/lltskb/AppContext;->removeActivity(Landroid/app/Activity;)V

    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/lltskb/lltskb/BaseActivity;->isAlive:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/lltskb/lltskb/BaseActivity;->finish()V

    const/4 p1, 0x1

    return p1

    .line 89
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/AppCompatActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/lltskb/lltskb/BaseActivity;->permissionRequestCallback:Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    goto :goto_0

    .line 123
    :cond_1
    array-length p1, p3

    const/4 v1, 0x0

    if-lez p1, :cond_2

    aget p1, p3, v1

    if-nez p1, :cond_2

    .line 126
    invoke-interface {v0}, Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;->onGranted()V

    goto :goto_0

    .line 129
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/BaseActivity;->permissionRequestCallback:Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;

    invoke-interface {p1}, Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;->onDenied()Z

    move-result p1

    if-nez p1, :cond_4

    .line 130
    array-length p1, p2

    if-eqz p1, :cond_3

    aget-object p1, p2, v1

    invoke-static {p0, p1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 131
    :cond_3
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/PermissionHelper;->showMissingPermissionDialog(Landroid/content/Context;)V

    :cond_4
    :goto_0
    return-void
.end method

.method protected requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V
    .locals 1

    .line 60
    iput-object p2, p0, Lcom/lltskb/lltskb/BaseActivity;->permissionRequestCallback:Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;

    .line 61
    new-instance p2, Lcom/lltskb/lltskb/utils/PermissionHelper;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/utils/PermissionHelper;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/utils/PermissionHelper;->checkPermission(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object p1, p0, Lcom/lltskb/lltskb/BaseActivity;->permissionRequestCallback:Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;

    invoke-interface {p1}, Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;->onGranted()V

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 67
    invoke-virtual {p2, p1, v0}, Lcom/lltskb/lltskb/utils/PermissionHelper;->permissionsCheck(Ljava/lang/String;I)V

    return-void
.end method
