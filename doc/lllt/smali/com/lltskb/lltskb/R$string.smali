.class public final Lcom/lltskb/lltskb/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0d0000

.field public static final abc_action_bar_up_description:I = 0x7f0d0001

.field public static final abc_action_menu_overflow_description:I = 0x7f0d0002

.field public static final abc_action_mode_done:I = 0x7f0d0003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0d0004

.field public static final abc_activitychooserview_choose_application:I = 0x7f0d0005

.field public static final abc_capital_off:I = 0x7f0d0006

.field public static final abc_capital_on:I = 0x7f0d0007

.field public static final abc_font_family_body_1_material:I = 0x7f0d0008

.field public static final abc_font_family_body_2_material:I = 0x7f0d0009

.field public static final abc_font_family_button_material:I = 0x7f0d000a

.field public static final abc_font_family_caption_material:I = 0x7f0d000b

.field public static final abc_font_family_display_1_material:I = 0x7f0d000c

.field public static final abc_font_family_display_2_material:I = 0x7f0d000d

.field public static final abc_font_family_display_3_material:I = 0x7f0d000e

.field public static final abc_font_family_display_4_material:I = 0x7f0d000f

.field public static final abc_font_family_headline_material:I = 0x7f0d0010

.field public static final abc_font_family_menu_material:I = 0x7f0d0011

.field public static final abc_font_family_subhead_material:I = 0x7f0d0012

.field public static final abc_font_family_title_material:I = 0x7f0d0013

.field public static final abc_search_hint:I = 0x7f0d0014

.field public static final abc_searchview_description_clear:I = 0x7f0d0015

.field public static final abc_searchview_description_query:I = 0x7f0d0016

.field public static final abc_searchview_description_search:I = 0x7f0d0017

.field public static final abc_searchview_description_submit:I = 0x7f0d0018

.field public static final abc_searchview_description_voice:I = 0x7f0d0019

.field public static final abc_shareactionprovider_share_with:I = 0x7f0d001a

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0d001b

.field public static final abc_toolbar_collapse_description:I = 0x7f0d001c

.field public static final about:I = 0x7f0d001d

.field public static final about_back_query:I = 0x7f0d001e

.field public static final about_back_query_desc:I = 0x7f0d001f

.field public static final about_declaimer:I = 0x7f0d0020

.field public static final about_declaimer_desc:I = 0x7f0d0021

.field public static final about_histroy:I = 0x7f0d0022

.field public static final about_histroy_desc:I = 0x7f0d0023

.field public static final about_image:I = 0x7f0d0024

.field public static final about_input_station:I = 0x7f0d0025

.field public static final about_input_station_desc:I = 0x7f0d0026

.field public static final about_input_train:I = 0x7f0d0027

.field public static final about_input_train_desc:I = 0x7f0d0028

.field public static final about_intro:I = 0x7f0d0029

.field public static final about_introduce:I = 0x7f0d002a

.field public static final about_lltskb:I = 0x7f0d002b

.field public static final about_price:I = 0x7f0d002c

.field public static final about_price_desc:I = 0x7f0d002d

.field public static final about_special_char:I = 0x7f0d002e

.field public static final about_special_char_desc:I = 0x7f0d002f

.field public static final about_train_color:I = 0x7f0d0030

.field public static final about_train_color_desc:I = 0x7f0d0031

.field public static final about_update:I = 0x7f0d0032

.field public static final about_update_desc:I = 0x7f0d0033

.field public static final about_useful_links:I = 0x7f0d0034

.field public static final about_useful_links_desc:I = 0x7f0d0035

.field public static final accesshome:I = 0x7f0d0036

.field public static final account:I = 0x7f0d0037

.field public static final acount_name:I = 0x7f0d0038

.field public static final active_n:I = 0x7f0d0039

.field public static final active_y:I = 0x7f0d003a

.field public static final ad_baoxian:I = 0x7f0d003b

.field public static final ad_duia:I = 0x7f0d003c

.field public static final ad_lingbx:I = 0x7f0d003d

.field public static final ad_mtlb:I = 0x7f0d003e

.field public static final ad_pufabank:I = 0x7f0d003f

.field public static final add_child_ticket:I = 0x7f0d0040

.field public static final add_contact_succeed:I = 0x7f0d0041

.field public static final add_user:I = 0x7f0d0042

.field public static final adding_contact:I = 0x7f0d0043

.field public static final addownload:I = 0x7f0d0044

.field public static final ads_jingzhungu:I = 0x7f0d0045

.field public static final adult:I = 0x7f0d0046

.field public static final adult_ticket:I = 0x7f0d0047

.field public static final alert_type:I = 0x7f0d0048

.field public static final ali_qiche_ticket:I = 0x7f0d0049

.field public static final alipay:I = 0x7f0d004a

.field public static final alipay_client:I = 0x7f0d004b

.field public static final all:I = 0x7f0d004c

.field public static final all_life:I = 0x7f0d004d

.field public static final app_crash:I = 0x7f0d004e

.field public static final app_crash_info:I = 0x7f0d004f

.field public static final app_name:I = 0x7f0d0050

.field public static final app_title:I = 0x7f0d0051

.field public static final appbar_scrolling_view_behavior:I = 0x7f0d0052

.field public static final arrive:I = 0x7f0d0053

.field public static final arrive_station:I = 0x7f0d0054

.field public static final arrive_station_name:I = 0x7f0d0055

.field public static final arrive_time_short:I = 0x7f0d0056

.field public static final at_least_one_passenger:I = 0x7f0d0057

.field public static final back:I = 0x7f0d0058

.field public static final background_book:I = 0x7f0d0059

.field public static final backtrip:I = 0x7f0d005a

.field public static final baidu_news:I = 0x7f0d005b

.field public static final baike:I = 0x7f0d005c

.field public static final bank_abc:I = 0x7f0d005d

.field public static final bank_boc:I = 0x7f0d005e

.field public static final bank_ccb:I = 0x7f0d005f

.field public static final bank_cmb:I = 0x7f0d0060

.field public static final bank_icbc:I = 0x7f0d0061

.field public static final bank_other:I = 0x7f0d0062

.field public static final bank_union:I = 0x7f0d0063

.field public static final bank_ztyt:I = 0x7f0d0064

.field public static final baoxian:I = 0x7f0d0065

.field public static final baoxian_confirm:I = 0x7f0d0066

.field public static final baoxian_confirm_hint:I = 0x7f0d0067

.field public static final baoxian_please_confirm:I = 0x7f0d0068

.field public static final baoxian_rule:I = 0x7f0d0069

.field public static final baoxian_secure:I = 0x7f0d006a

.field public static final begin_station:I = 0x7f0d006b

.field public static final bg_task_btn_fmt:I = 0x7f0d006c

.field public static final bg_tasks:I = 0x7f0d006d

.field public static final bg_tasks_msg_fmt:I = 0x7f0d006e

.field public static final bg_tasks_running:I = 0x7f0d006f

.field public static final bianzu:I = 0x7f0d0070

.field public static final big_screen:I = 0x7f0d0071

.field public static final big_station:I = 0x7f0d0072

.field public static final bind_tel_success:I = 0x7f0d0073

.field public static final birthday:I = 0x7f0d0074

.field public static final book:I = 0x7f0d0075

.field public static final book_faq:I = 0x7f0d0076

.field public static final book_flight:I = 0x7f0d0077

.field public static final book_hotel:I = 0x7f0d0078

.field public static final book_info:I = 0x7f0d0079

.field public static final book_life:I = 0x7f0d007a

.field public static final book_normal:I = 0x7f0d007b

.field public static final book_scenery:I = 0x7f0d007c

.field public static final book_student:I = 0x7f0d007d

.field public static final book_ticket:I = 0x7f0d007e

.field public static final book_ticket_web:I = 0x7f0d007f

.field public static final book_type:I = 0x7f0d0080

.field public static final bottom_sheet_behavior:I = 0x7f0d0081

.field public static final build_date:I = 0x7f0d0082

.field public static final buy_ticket:I = 0x7f0d0083

.field public static final buycar:I = 0x7f0d0084

.field public static final buylottery:I = 0x7f0d0085

.field public static final by_order_time:I = 0x7f0d0086

.field public static final by_take_time:I = 0x7f0d0087

.field public static final can_not_buy_student:I = 0x7f0d0088

.field public static final can_not_get_passcode:I = 0x7f0d0089

.field public static final cancel:I = 0x7f0d008a

.field public static final cancel_hb_order_msg:I = 0x7f0d008b

.field public static final cancel_order:I = 0x7f0d008c

.field public static final cancel_order_failed:I = 0x7f0d008d

.field public static final cancel_order_in_progress:I = 0x7f0d008e

.field public static final cancel_order_success:I = 0x7f0d008f

.field public static final cancel_queue_failed:I = 0x7f0d0090

.field public static final cancel_queue_in_progress:I = 0x7f0d0091

.field public static final cancel_queue_success:I = 0x7f0d0092

.field public static final canceled_by_user:I = 0x7f0d0093

.field public static final cc_query_select_dlg_title:I = 0x7f0d0094

.field public static final ccquery:I = 0x7f0d0095

.field public static final changeTS:I = 0x7f0d0096

.field public static final character_counter_pattern:I = 0x7f0d0097

.field public static final check_mobile_msg:I = 0x7f0d0098

.field public static final check_new_version:I = 0x7f0d0099

.field public static final check_order_info:I = 0x7f0d009a

.field public static final check_status:I = 0x7f0d009b

.field public static final check_user_info:I = 0x7f0d009c

.field public static final check_user_status:I = 0x7f0d009d

.field public static final child_must_with_adult:I = 0x7f0d009e

.field public static final child_ticket:I = 0x7f0d009f

.field public static final child_ticket_warm_hint:I = 0x7f0d00a0

.field public static final child_ticket_warm_message:I = 0x7f0d00a1

.field public static final chk_lagecy_line:I = 0x7f0d00a2

.field public static final choose_seats:I = 0x7f0d00a3

.field public static final choose_seats_hint:I = 0x7f0d00a4

.field public static final choose_seats_not_completed:I = 0x7f0d00a5

.field public static final choosed_seats:I = 0x7f0d00a6

.field public static final choosed_seats_left:I = 0x7f0d00a7

.field public static final clear_account:I = 0x7f0d00a8

.field public static final clear_all_records:I = 0x7f0d00a9

.field public static final clear_user_confirm:I = 0x7f0d00aa

.field public static final close:I = 0x7f0d00ab

.field public static final close_confirm:I = 0x7f0d00ac

.field public static final collapse:I = 0x7f0d00ad

.field public static final common_failure:I = 0x7f0d00ae

.field public static final complete_order:I = 0x7f0d00af

.field public static final complete_order_query:I = 0x7f0d00b0

.field public static final confirmUserPass:I = 0x7f0d00b1

.field public static final confirm_del_contact:I = 0x7f0d00b2

.field public static final confirm_edit_contact:I = 0x7f0d00b3

.field public static final confirm_houbu_order:I = 0x7f0d00b4

.field public static final confirm_order_in_progress:I = 0x7f0d00b5

.field public static final confirm_signout_message:I = 0x7f0d00b6

.field public static final confirm_single_for_queue:I = 0x7f0d00b7

.field public static final confirm_to_delete_task:I = 0x7f0d00b8

.field public static final connect_to_network:I = 0x7f0d00b9

.field public static final contacts:I = 0x7f0d00ba

.field public static final content:I = 0x7f0d00bb

.field public static final continue_pay:I = 0x7f0d00bc

.field public static final copy_order:I = 0x7f0d00bd

.field public static final copyright:I = 0x7f0d00be

.field public static final country:I = 0x7f0d00bf

.field public static final country_region:I = 0x7f0d00c0

.field public static final ctrip_jiesongzhan:I = 0x7f0d00c1

.field public static final ctrip_mashangjiaoche:I = 0x7f0d00c2

.field public static final current_account:I = 0x7f0d00c3

.field public static final current_day:I = 0x7f0d00c4

.field public static final czquery:I = 0x7f0d00c5

.field public static final data_loading:I = 0x7f0d00c6

.field public static final data_ver:I = 0x7f0d00c7

.field public static final date_sample:I = 0x7f0d00c8

.field public static final dd_qiye:I = 0x7f0d00c9

.field public static final deadline_time:I = 0x7f0d00ca

.field public static final default_query_date:I = 0x7f0d00cb

.field public static final del:I = 0x7f0d00cc

.field public static final delete:I = 0x7f0d00cd

.field public static final delete_contact_succeed:I = 0x7f0d00ce

.field public static final delete_records:I = 0x7f0d00cf

.field public static final deleting_contact:I = 0x7f0d00d0

.field public static final departure:I = 0x7f0d00d1

.field public static final direction:I = 0x7f0d00d2

.field public static final direction_all:I = 0x7f0d00d3

.field public static final direction_down:I = 0x7f0d00d4

.field public static final direction_end:I = 0x7f0d00d5

.field public static final direction_up:I = 0x7f0d00d6

.field public static final discount_range:I = 0x7f0d00d7

.field public static final ditie_map:I = 0x7f0d00d8

.field public static final dont_select_train:I = 0x7f0d00d9

.field public static final download:I = 0x7f0d00da

.field public static final download_failed:I = 0x7f0d00db

.field public static final draw:I = 0x7f0d00dc

.field public static final draw_all:I = 0x7f0d00dd

.field public static final edit:I = 0x7f0d00de

.field public static final edit_user:I = 0x7f0d00df

.field public static final email:I = 0x7f0d00e0

.field public static final emailaddr:I = 0x7f0d00e1

.field public static final end_date:I = 0x7f0d00e2

.field public static final end_station:I = 0x7f0d00e3

.field public static final enter_year:I = 0x7f0d00e4

.field public static final err_adjacent_date:I = 0x7f0d00e5

.field public static final err_cant_submit_hborder:I = 0x7f0d00e6

.field public static final err_check_phone_failed:I = 0x7f0d00e7

.field public static final err_contact_type_is_empty:I = 0x7f0d00e8

.field public static final err_date_not_in_order_range:I = 0x7f0d00e9

.field public static final err_download_file_failed:I = 0x7f0d00ea

.field public static final err_exceeds_max_houbu_count:I = 0x7f0d00eb

.field public static final err_fmt_no_train_found:I = 0x7f0d00ec

.field public static final err_from_station_not_found:I = 0x7f0d00ed

.field public static final err_have_no_complete:I = 0x7f0d00ee

.field public static final err_houbu_01_11:I = 0x7f0d00ef

.field public static final err_houbu_02_12:I = 0x7f0d00f0

.field public static final err_houbu_03_13:I = 0x7f0d00f1

.field public static final err_houbu_04_14:I = 0x7f0d00f2

.field public static final err_houbu_exceed_limit:I = 0x7f0d00f3

.field public static final err_houbu_not_allowed_time:I = 0x7f0d00f4

.field public static final err_id_type_is_empty:I = 0x7f0d00f5

.field public static final err_invalid_range:I = 0x7f0d00f6

.field public static final err_name_is_empty:I = 0x7f0d00f7

.field public static final err_network_error:I = 0x7f0d00f8

.field public static final err_no_jpk_info:I = 0x7f0d00f9

.field public static final err_no_train_found:I = 0x7f0d00fa

.field public static final err_no_train_stop_info_found:I = 0x7f0d00fb

.field public static final err_no_train_time_found:I = 0x7f0d00fc

.field public static final err_no_train_type_found:I = 0x7f0d00fd

.field public static final err_no_train_with_seats_found:I = 0x7f0d00fe

.field public static final err_not_train_found:I = 0x7f0d00ff

.field public static final err_order_no_order_found:I = 0x7f0d0100

.field public static final err_order_not_enough_tickets:I = 0x7f0d0101

.field public static final err_province_is_empty:I = 0x7f0d0102

.field public static final err_put_background:I = 0x7f0d0103

.field public static final err_query_user_info:I = 0x7f0d0104

.field public static final err_school_is_empty:I = 0x7f0d0105

.field public static final err_student_no_is_empty:I = 0x7f0d0106

.field public static final err_system_busy:I = 0x7f0d0107

.field public static final err_to_station_not_found:I = 0x7f0d0108

.field public static final err_train_info_not_found:I = 0x7f0d0109

.field public static final err_train_info_wrong:I = 0x7f0d010a

.field public static final err_unknow_error:I = 0x7f0d010b

.field public static final err_unzip_failed:I = 0x7f0d010c

.field public static final err_user_name_conflicts:I = 0x7f0d010d

.field public static final err_user_not_login:I = 0x7f0d010e

.field public static final error:I = 0x7f0d010f

.field public static final ershou:I = 0x7f0d0110

.field public static final exceed_date:I = 0x7f0d0111

.field public static final exit:I = 0x7f0d0112

.field public static final exit_platform:I = 0x7f0d0113

.field public static final exitconfirm:I = 0x7f0d0114

.field public static final faq:I = 0x7f0d0115

.field public static final feature_is_under_developing:I = 0x7f0d0116

.field public static final feed_back:I = 0x7f0d0117

.field public static final feedback:I = 0x7f0d0118

.field public static final female:I = 0x7f0d0119

.field public static final filter:I = 0x7f0d011a

.field public static final find_house:I = 0x7f0d011b

.field public static final find_housemaking:I = 0x7f0d011c

.field public static final find_job:I = 0x7f0d011d

.field public static final fk_content:I = 0x7f0d011e

.field public static final fk_subject:I = 0x7f0d011f

.field public static final flight:I = 0x7f0d0120

.field public static final flight_long:I = 0x7f0d0121

.field public static final flight_ticket:I = 0x7f0d0122

.field public static final fmt_arrive_time:I = 0x7f0d0123

.field public static final fmt_buy_less_forward:I = 0x7f0d0124

.field public static final fmt_buy_more_backward:I = 0x7f0d0125

.field public static final fmt_buy_more_forward:I = 0x7f0d0126

.field public static final fmt_cancel_order_confirm_msg:I = 0x7f0d0127

.field public static final fmt_concat_string:I = 0x7f0d0128

.field public static final fmt_confirm_houbu:I = 0x7f0d0129

.field public static final fmt_confirm_reserve_return:I = 0x7f0d012a

.field public static final fmt_correct_qiye_content:I = 0x7f0d012b

.field public static final fmt_correct_qiye_subject:I = 0x7f0d012c

.field public static final fmt_dd_qiye:I = 0x7f0d012d

.field public static final fmt_drop_down:I = 0x7f0d012e

.field public static final fmt_duration:I = 0x7f0d012f

.field public static final fmt_err_station_not_found:I = 0x7f0d0130

.field public static final fmt_err_station_not_found_in_range:I = 0x7f0d0131

.field public static final fmt_food_coach:I = 0x7f0d0132

.field public static final fmt_from_to:I = 0x7f0d0133

.field public static final fmt_hb_continue_pay_confirm:I = 0x7f0d0134

.field public static final fmt_hb_order_no:I = 0x7f0d0135

.field public static final fmt_hb_order_paid:I = 0x7f0d0136

.field public static final fmt_hb_order_price:I = 0x7f0d0137

.field public static final fmt_hb_order_time:I = 0x7f0d0138

.field public static final fmt_houbu_confirm:I = 0x7f0d0139

.field public static final fmt_houbu_max_count_tips:I = 0x7f0d013a

.field public static final fmt_jpk_title:I = 0x7f0d013b

.field public static final fmt_left_ticket_1:I = 0x7f0d013c

.field public static final fmt_left_ticket_2:I = 0x7f0d013d

.field public static final fmt_left_time:I = 0x7f0d013e

.field public static final fmt_max_passengers_limit:I = 0x7f0d013f

.field public static final fmt_max_passengers_limit_houbu:I = 0x7f0d0140

.field public static final fmt_monitor_details_long:I = 0x7f0d0141

.field public static final fmt_monitor_details_short:I = 0x7f0d0142

.field public static final fmt_monitor_task_title:I = 0x7f0d0143

.field public static final fmt_new_data_found:I = 0x7f0d0144

.field public static final fmt_new_version_found:I = 0x7f0d0145

.field public static final fmt_no_big_screen_data:I = 0x7f0d0146

.field public static final fmt_order_continue_pay_confirm:I = 0x7f0d0147

.field public static final fmt_order_not_found:I = 0x7f0d0148

.field public static final fmt_order_passenger_info:I = 0x7f0d0149

.field public static final fmt_order_price_page:I = 0x7f0d014a

.field public static final fmt_order_progress_hint:I = 0x7f0d014b

.field public static final fmt_order_train_code:I = 0x7f0d014c

.field public static final fmt_order_wait_count:I = 0x7f0d014d

.field public static final fmt_order_wait_time:I = 0x7f0d014e

.field public static final fmt_qss_result:I = 0x7f0d014f

.field public static final fmt_queue_num:I = 0x7f0d0150

.field public static final fmt_start_time:I = 0x7f0d0151

.field public static final fmt_station_range:I = 0x7f0d0152

.field public static final fmt_station_title:I = 0x7f0d0153

.field public static final fmt_zwd_arrive_early:I = 0x7f0d0154

.field public static final fmt_zwd_arrive_later:I = 0x7f0d0155

.field public static final fmt_zwd_arrive_ontime:I = 0x7f0d0156

.field public static final fmt_zwd_in_query:I = 0x7f0d0157

.field public static final fmt_zwd_info:I = 0x7f0d0158

.field public static final fmt_zwd_title:I = 0x7f0d0159

.field public static final fmt_zwd_wd_hours_mins:I = 0x7f0d015a

.field public static final fmt_zwd_wd_mins:I = 0x7f0d015b

.field public static final font_sample:I = 0x7f0d015c

.field public static final font_size:I = 0x7f0d015d

.field public static final footer_hint_load_normal:I = 0x7f0d015e

.field public static final footer_hint_load_ready:I = 0x7f0d015f

.field public static final forget_pass:I = 0x7f0d0160

.field public static final found_ticket_hint:I = 0x7f0d0161

.field public static final fu:I = 0x7f0d0162

.field public static final fuzzy_query:I = 0x7f0d0163

.field public static final gangway:I = 0x7f0d0164

.field public static final get_order_wait_time:I = 0x7f0d0165

.field public static final get_pass_code:I = 0x7f0d0166

.field public static final get_passcode_failed:I = 0x7f0d0167

.field public static final get_queue_count:I = 0x7f0d0168

.field public static final gong:I = 0x7f0d0169

.field public static final hb_complete_order:I = 0x7f0d016a

.field public static final hb_not_complete_order:I = 0x7f0d016b

.field public static final hb_not_redeem_order:I = 0x7f0d016c

.field public static final header_hint_refresh_loading:I = 0x7f0d016d

.field public static final header_hint_refresh_normal:I = 0x7f0d016e

.field public static final header_hint_refresh_ready:I = 0x7f0d016f

.field public static final header_hint_refresh_time:I = 0x7f0d0170

.field public static final help_lltskb:I = 0x7f0d0171

.field public static final hide_train:I = 0x7f0d0172

.field public static final hint:I = 0x7f0d0173

.field public static final hint_account:I = 0x7f0d0174

.field public static final hint_arrive_station:I = 0x7f0d0175

.field public static final hint_arrivestation:I = 0x7f0d0176

.field public static final hint_confirmUserPass:I = 0x7f0d0177

.field public static final hint_content:I = 0x7f0d0178

.field public static final hint_email:I = 0x7f0d0179

.field public static final hint_id_no:I = 0x7f0d017a

.field public static final hint_input_student_no:I = 0x7f0d017b

.field public static final hint_inputtrain:I = 0x7f0d017c

.field public static final hint_no_more_five:I = 0x7f0d017d

.field public static final hint_order:I = 0x7f0d017e

.field public static final hint_pay_12306:I = 0x7f0d017f

.field public static final hint_phone:I = 0x7f0d0180

.field public static final hint_rand_code:I = 0x7f0d0181

.field public static final hint_seat_type:I = 0x7f0d0182

.field public static final hint_select_alert_type:I = 0x7f0d0183

.field public static final hint_select_passenger:I = 0x7f0d0184

.field public static final hint_select_seat:I = 0x7f0d0185

.field public static final hint_select_seat_type:I = 0x7f0d0186

.field public static final hint_select_station:I = 0x7f0d0187

.field public static final hint_select_train_type:I = 0x7f0d0188

.field public static final hint_start_date:I = 0x7f0d0189

.field public static final hint_start_station:I = 0x7f0d018a

.field public static final hint_startstation:I = 0x7f0d018b

.field public static final hint_train_no:I = 0x7f0d018c

.field public static final hint_train_time:I = 0x7f0d018d

.field public static final hint_userPass:I = 0x7f0d018e

.field public static final hint_user_name:I = 0x7f0d018f

.field public static final hint_user_password:I = 0x7f0d0190

.field public static final history_today:I = 0x7f0d0191

.field public static final histroy_station:I = 0x7f0d0192

.field public static final home:I = 0x7f0d0193

.field public static final home_page:I = 0x7f0d0194

.field public static final hotel:I = 0x7f0d0195

.field public static final hotel_long:I = 0x7f0d0196

.field public static final houbu:I = 0x7f0d0197

.field public static final houbu_cutoff_time_info:I = 0x7f0d0198

.field public static final houbu_info:I = 0x7f0d0199

.field public static final houbu_list:I = 0x7f0d019a

.field public static final houbu_order_tips:I = 0x7f0d019b

.field public static final huoche_service:I = 0x7f0d019c

.field public static final id_no:I = 0x7f0d019d

.field public static final id_type:I = 0x7f0d019e

.field public static final idtype_1:I = 0x7f0d019f

.field public static final idtype_B:I = 0x7f0d01a0

.field public static final idtype_C:I = 0x7f0d01a1

.field public static final idtype_G:I = 0x7f0d01a2

.field public static final image:I = 0x7f0d01a3

.field public static final imgview:I = 0x7f0d01a4

.field public static final in_book_ticket:I = 0x7f0d01a5

.field public static final in_process:I = 0x7f0d01a6

.field public static final in_query:I = 0x7f0d01a7

.field public static final initializing:I = 0x7f0d01a8

.field public static final input_cannt_be_empty:I = 0x7f0d01a9

.field public static final input_station_hint:I = 0x7f0d01aa

.field public static final install_app_complete:I = 0x7f0d01ab

.field public static final install_data_in_progress:I = 0x7f0d01ac

.field public static final invalid_op:I = 0x7f0d01ad

.field public static final is_pay_succeed:I = 0x7f0d01ae

.field public static final jlb:I = 0x7f0d01af

.field public static final jlb_claims:I = 0x7f0d01b0

.field public static final jpk:I = 0x7f0d01b1

.field public static final key_exceed_cancel_num:I = 0x7f0d01b2

.field public static final key_exceed_ticket:I = 0x7f0d01b3

.field public static final key_has_order_not_paid:I = 0x7f0d01b4

.field public static final key_no_enough_ticket:I = 0x7f0d01b5

.field public static final km:I = 0x7f0d01b6

.field public static final liangche:I = 0x7f0d01b7

.field public static final life:I = 0x7f0d01b8

.field public static final life_service:I = 0x7f0d01b9

.field public static final lineup_order:I = 0x7f0d01ba

.field public static final llt_datebase:I = 0x7f0d01bb

.field public static final llt_share:I = 0x7f0d01bc

.field public static final lltexam:I = 0x7f0d01bd

.field public static final login:I = 0x7f0d01be

.field public static final login_in_progress:I = 0x7f0d01bf

.field public static final login_success:I = 0x7f0d01c0

.field public static final login_tips:I = 0x7f0d01c1

.field public static final male:I = 0x7f0d01c2

.field public static final max_five_passengers:I = 0x7f0d01c3

.field public static final meitun_toruist:I = 0x7f0d01c4

.field public static final menu_correct_qiye:I = 0x7f0d01c5

.field public static final message_too_long:I = 0x7f0d01c6

.field public static final mid_query:I = 0x7f0d01c7

.field public static final mobile:I = 0x7f0d01c8

.field public static final mobile_check_n:I = 0x7f0d01c9

.field public static final mobile_check_y:I = 0x7f0d01ca

.field public static final mobile_heyan:I = 0x7f0d01cb

.field public static final mod_mobile:I = 0x7f0d01cc

.field public static final monitor:I = 0x7f0d01cd

.field public static final monitor_manage:I = 0x7f0d01ce

.field public static final more_ticket:I = 0x7f0d01cf

.field public static final more_ticket_title:I = 0x7f0d01d0

.field public static final msg_retry_login:I = 0x7f0d01d1

.field public static final must_input_complete_info:I = 0x7f0d01d2

.field public static final name:I = 0x7f0d01d3

.field public static final name_sample:I = 0x7f0d01d4

.field public static final need_network:I = 0x7f0d01d5

.field public static final network_error:I = 0x7f0d01d6

.field public static final new_data_found:I = 0x7f0d01d7

.field public static final new_mobile_no:I = 0x7f0d01d8

.field public static final new_ver_comments:I = 0x7f0d01d9

.field public static final new_version_found:I = 0x7f0d01da

.field public static final next:I = 0x7f0d01db

.field public static final next_day:I = 0x7f0d01dc

.field public static final next_page:I = 0x7f0d01dd

.field public static final next_step:I = 0x7f0d01de

.field public static final no:I = 0x7f0d01df

.field public static final no_available_account:I = 0x7f0d01e0

.field public static final no_big_screen_info:I = 0x7f0d01e1

.field public static final no_choose_seats:I = 0x7f0d01e2

.field public static final no_contact:I = 0x7f0d01e3

.field public static final no_direct_train:I = 0x7f0d01e4

.field public static final no_infor_found:I = 0x7f0d01e5

.field public static final no_limit:I = 0x7f0d01e6

.field public static final no_login:I = 0x7f0d01e7

.field public static final no_monitor_tasks:I = 0x7f0d01e8

.field public static final no_nocomplete_found:I = 0x7f0d01e9

.field public static final no_passenger_selected:I = 0x7f0d01ea

.field public static final no_result_found:I = 0x7f0d01eb

.field public static final no_seat_type_found:I = 0x7f0d01ec

.field public static final no_share_app_found:I = 0x7f0d01ed

.field public static final no_train_found:I = 0x7f0d01ee

.field public static final no_train_found_fmt:I = 0x7f0d01ef

.field public static final no_train_time_found:I = 0x7f0d01f0

.field public static final no_train_type_found:I = 0x7f0d01f1

.field public static final no_update_found:I = 0x7f0d01f2

.field public static final nocomplete_order:I = 0x7f0d01f3

.field public static final ok:I = 0x7f0d01f4

.field public static final old_moible_no:I = 0x7f0d01f5

.field public static final online_login:I = 0x7f0d01f6

.field public static final online_pay:I = 0x7f0d01f7

.field public static final online_query:I = 0x7f0d01f8

.field public static final order_date:I = 0x7f0d01f9

.field public static final order_has_many_tickets:I = 0x7f0d01fa

.field public static final order_houbu_successfully:I = 0x7f0d01fb

.field public static final order_no_details_fmt:I = 0x7f0d01fc

.field public static final order_not_enough_tickets:I = 0x7f0d01fd

.field public static final order_status:I = 0x7f0d01fe

.field public static final order_stu_ticket:I = 0x7f0d01ff

.field public static final order_success:I = 0x7f0d0200

.field public static final order_success_common:I = 0x7f0d0201

.field public static final order_ticket_date_hint:I = 0x7f0d0202

.field public static final order_ticket_success:I = 0x7f0d0203

.field public static final order_type:I = 0x7f0d0204

.field public static final order_type_G:I = 0x7f0d0205

.field public static final order_type_H:I = 0x7f0d0206

.field public static final order_user:I = 0x7f0d0207

.field public static final orignal_scheme:I = 0x7f0d0208

.field public static final passcode:I = 0x7f0d0209

.field public static final passcode_dialog_title:I = 0x7f0d020a

.field public static final passcode_invalid_inpput:I = 0x7f0d020b

.field public static final passenger:I = 0x7f0d020c

.field public static final password:I = 0x7f0d020d

.field public static final password_toggle_content_description:I = 0x7f0d020e

.field public static final path_password_eye:I = 0x7f0d020f

.field public static final path_password_eye_mask_strike_through:I = 0x7f0d0210

.field public static final path_password_eye_mask_visible:I = 0x7f0d0211

.field public static final path_password_strike_through:I = 0x7f0d0212

.field public static final pause:I = 0x7f0d0213

.field public static final pay_failed:I = 0x7f0d0214

.field public static final pay_result:I = 0x7f0d0215

.field public static final permission_request_message:I = 0x7f0d0216

.field public static final phone:I = 0x7f0d0217

.field public static final pingan:I = 0x7f0d0218

.field public static final pku:I = 0x7f0d0219

.field public static final please_select_ops:I = 0x7f0d021a

.field public static final please_wait:I = 0x7f0d021b

.field public static final prev_day:I = 0x7f0d021c

.field public static final prev_page:I = 0x7f0d021d

.field public static final price_query:I = 0x7f0d021e

.field public static final price_sample:I = 0x7f0d021f

.field public static final prog_ver:I = 0x7f0d0220

.field public static final qiche_daquan:I = 0x7f0d0221

.field public static final qiche_price:I = 0x7f0d0222

.field public static final qiche_ticket:I = 0x7f0d0223

.field public static final query:I = 0x7f0d0224

.field public static final query_baoxian:I = 0x7f0d0225

.field public static final query_complete_in_progress:I = 0x7f0d0226

.field public static final query_condition:I = 0x7f0d0227

.field public static final query_contact_in_progress:I = 0x7f0d0228

.field public static final query_freq:I = 0x7f0d0229

.field public static final query_freq_fmt:I = 0x7f0d022a

.field public static final query_history:I = 0x7f0d022b

.field public static final query_histroy:I = 0x7f0d022c

.field public static final query_in_progress:I = 0x7f0d022d

.field public static final query_result:I = 0x7f0d022e

.field public static final query_ticket:I = 0x7f0d022f

.field public static final query_time:I = 0x7f0d0230

.field public static final query_train_in_progress:I = 0x7f0d0231

.field public static final query_zwd:I = 0x7f0d0232

.field public static final qunar_order_query:I = 0x7f0d0233

.field public static final redirect_to_pay_page:I = 0x7f0d0234

.field public static final refresh:I = 0x7f0d0235

.field public static final regist_randcode_msg:I = 0x7f0d0236

.field public static final regist_randcode_msg_2:I = 0x7f0d0237

.field public static final regist_user:I = 0x7f0d0238

.field public static final register:I = 0x7f0d0239

.field public static final remember_name:I = 0x7f0d023a

.field public static final remember_pass:I = 0x7f0d023b

.field public static final resginTicket:I = 0x7f0d023c

.field public static final resign_ticket:I = 0x7f0d023d

.field public static final result_arrive_station:I = 0x7f0d023e

.field public static final result_arrive_time:I = 0x7f0d023f

.field public static final result_cc_name:I = 0x7f0d0240

.field public static final result_cz_name:I = 0x7f0d0241

.field public static final result_dist:I = 0x7f0d0242

.field public static final result_order_for_queue:I = 0x7f0d0243

.field public static final result_start_station:I = 0x7f0d0244

.field public static final result_start_time:I = 0x7f0d0245

.field public static final result_train_type:I = 0x7f0d0246

.field public static final retry:I = 0x7f0d0247

.field public static final returnTicket:I = 0x7f0d0248

.field public static final return_order:I = 0x7f0d0249

.field public static final return_reserve_success:I = 0x7f0d024a

.field public static final return_resign:I = 0x7f0d024b

.field public static final return_ticket_success:I = 0x7f0d024c

.field public static final reverse_query:I = 0x7f0d024d

.field public static final run:I = 0x7f0d024e

.field public static final run_chart:I = 0x7f0d024f

.field public static final sale_time:I = 0x7f0d0250

.field public static final school_in_province:I = 0x7f0d0251

.field public static final school_name:I = 0x7f0d0252

.field public static final school_system:I = 0x7f0d0253

.field public static final search_menu_title:I = 0x7f0d0254

.field public static final seat:I = 0x7f0d0255

.field public static final sel_seat_a:I = 0x7f0d0256

.field public static final sel_seat_b:I = 0x7f0d0257

.field public static final sel_seat_c:I = 0x7f0d0258

.field public static final sel_seat_d:I = 0x7f0d0259

.field public static final sel_seat_f:I = 0x7f0d025a

.field public static final select_account:I = 0x7f0d025b

.field public static final select_all:I = 0x7f0d025c

.field public static final select_date:I = 0x7f0d025d

.field public static final select_histroy:I = 0x7f0d025e

.field public static final select_mid_station:I = 0x7f0d025f

.field public static final select_monitor_train:I = 0x7f0d0260

.field public static final select_passengers:I = 0x7f0d0261

.field public static final select_sort_type:I = 0x7f0d0262

.field public static final select_station:I = 0x7f0d0263

.field public static final select_student:I = 0x7f0d0264

.field public static final select_train:I = 0x7f0d0265

.field public static final sequence_no:I = 0x7f0d0266

.field public static final set_station:I = 0x7f0d0267

.field public static final settings:I = 0x7f0d0268

.field public static final sex:I = 0x7f0d0269

.field public static final shanghai:I = 0x7f0d026a

.field public static final share_image_style:I = 0x7f0d026b

.field public static final share_lltskb:I = 0x7f0d026c

.field public static final share_message:I = 0x7f0d026d

.field public static final share_order:I = 0x7f0d026e

.field public static final share_result:I = 0x7f0d026f

.field public static final share_style:I = 0x7f0d0270

.field public static final share_text_style:I = 0x7f0d0271

.field public static final show_runchart:I = 0x7f0d0272

.field public static final sign_out_inprogress:I = 0x7f0d0273

.field public static final sign_out_success:I = 0x7f0d0274

.field public static final signout:I = 0x7f0d0275

.field public static final silent:I = 0x7f0d0276

.field public static final skip_ads:I = 0x7f0d0277

.field public static final skip_btn_fmt:I = 0x7f0d0278

.field public static final sort:I = 0x7f0d0279

.field public static final sort_dlg_title:I = 0x7f0d027a

.field public static final sound:I = 0x7f0d027b

.field public static final st12306_weichat:I = 0x7f0d027c

.field public static final start:I = 0x7f0d027d

.field public static final start_date:I = 0x7f0d027e

.field public static final start_station:I = 0x7f0d027f

.field public static final start_station_name:I = 0x7f0d0280

.field public static final start_time:I = 0x7f0d0281

.field public static final start_time_short:I = 0x7f0d0282

.field public static final station_name:I = 0x7f0d0283

.field public static final station_qss_not_found:I = 0x7f0d0284

.field public static final status:I = 0x7f0d0285

.field public static final status_bar_notification_info_overflow:I = 0x7f0d0286

.field public static final status_checkin:I = 0x7f0d0287

.field public static final status_delay:I = 0x7f0d0288

.field public static final status_earlier:I = 0x7f0d0289

.field public static final status_fmt_delay:I = 0x7f0d028a

.field public static final status_ontime:I = 0x7f0d028b

.field public static final status_stop_checkin:I = 0x7f0d028c

.field public static final status_wait:I = 0x7f0d028d

.field public static final stop:I = 0x7f0d028e

.field public static final stop_book:I = 0x7f0d028f

.field public static final stop_run:I = 0x7f0d0290

.field public static final student:I = 0x7f0d0291

.field public static final student_no:I = 0x7f0d0292

.field public static final subject:I = 0x7f0d0293

.field public static final submit:I = 0x7f0d0294

.field public static final submit_houbu_order:I = 0x7f0d0295

.field public static final submit_order_in_progress:I = 0x7f0d0296

.field public static final sync_contact:I = 0x7f0d0297

.field public static final system_maintain:I = 0x7f0d0298

.field public static final system_maintain_time:I = 0x7f0d0299

.field public static final tabcc:I = 0x7f0d029a

.field public static final tabcz:I = 0x7f0d029b

.field public static final tabsettings:I = 0x7f0d029c

.field public static final tabtj:I = 0x7f0d029d

.field public static final tabyd:I = 0x7f0d029e

.field public static final tabzz:I = 0x7f0d029f

.field public static final take_date:I = 0x7f0d02a0

.field public static final text_sample:I = 0x7f0d02a1

.field public static final textview:I = 0x7f0d02a2

.field public static final ticket:I = 0x7f0d02a3

.field public static final ticket_adult:I = 0x7f0d02a4

.field public static final ticket_child:I = 0x7f0d02a5

.field public static final ticket_count:I = 0x7f0d02a6

.field public static final ticket_num:I = 0x7f0d02a7

.field public static final ticket_query:I = 0x7f0d02a8

.field public static final ticket_special:I = 0x7f0d02a9

.field public static final ticket_status:I = 0x7f0d02aa

.field public static final ticket_student:I = 0x7f0d02ab

.field public static final ticket_type:I = 0x7f0d02ac

.field public static final ticketprice:I = 0x7f0d02ad

.field public static final tieluzhaop:I = 0x7f0d02ae

.field public static final time_sample:I = 0x7f0d02af

.field public static final title:I = 0x7f0d02b0

.field public static final total_num:I = 0x7f0d02b1

.field public static final total_price:I = 0x7f0d02b2

.field public static final train:I = 0x7f0d02b3

.field public static final train_dc:I = 0x7f0d02b4

.field public static final train_g:I = 0x7f0d02b5

.field public static final train_info:I = 0x7f0d02b6

.field public static final train_k:I = 0x7f0d02b7

.field public static final train_lk:I = 0x7f0d02b8

.field public static final train_name:I = 0x7f0d02b9

.field public static final train_pk:I = 0x7f0d02ba

.field public static final train_pke:I = 0x7f0d02bb

.field public static final train_stop_info:I = 0x7f0d02bc

.field public static final train_t:I = 0x7f0d02bd

.field public static final train_time:I = 0x7f0d02be

.field public static final train_time_close:I = 0x7f0d02bf

.field public static final train_type:I = 0x7f0d02c0

.field public static final train_z:I = 0x7f0d02c1

.field public static final trip_sample:I = 0x7f0d02c2

.field public static final try_online_login:I = 0x7f0d02c3

.field public static final uncollapse:I = 0x7f0d02c4

.field public static final unknown_error:I = 0x7f0d02c5

.field public static final update:I = 0x7f0d02c6

.field public static final update_contact_succeed:I = 0x7f0d02c7

.field public static final update_data_successfully:I = 0x7f0d02c8

.field public static final update_failed:I = 0x7f0d02c9

.field public static final update_found:I = 0x7f0d02ca

.field public static final updating:I = 0x7f0d02cb

.field public static final updating_contact:I = 0x7f0d02cc

.field public static final use_classui:I = 0x7f0d02cd

.field public static final use_mid_query:I = 0x7f0d02ce

.field public static final userPass:I = 0x7f0d02cf

.field public static final user_info:I = 0x7f0d02d0

.field public static final user_name:I = 0x7f0d02d1

.field public static final user_status_not_pass:I = 0x7f0d02d2

.field public static final user_status_pass:I = 0x7f0d02d3

.field public static final user_status_please_report:I = 0x7f0d02d4

.field public static final user_status_pre_pass:I = 0x7f0d02d5

.field public static final user_status_wait_validate:I = 0x7f0d02d6

.field public static final user_type:I = 0x7f0d02d7

.field public static final verified:I = 0x7f0d02d8

.field public static final version_no:I = 0x7f0d02d9

.field public static final vibrate:I = 0x7f0d02da

.field public static final wait_pay:I = 0x7f0d02db

.field public static final wait_room_wicket:I = 0x7f0d02dc

.field public static final waiting_query:I = 0x7f0d02dd

.field public static final warning:I = 0x7f0d02de

.field public static final web_version:I = 0x7f0d02df

.field public static final weixin_pay:I = 0x7f0d02e0

.field public static final window:I = 0x7f0d02e1

.field public static final yes:I = 0x7f0d02e2

.field public static final yuding:I = 0x7f0d02e3

.field public static final yzm:I = 0x7f0d02e4

.field public static final zcquery:I = 0x7f0d02e5

.field public static final zero:I = 0x7f0d02e6

.field public static final zhan:I = 0x7f0d02e7

.field public static final zwd:I = 0x7f0d02e8

.field public static final zwd_query_type:I = 0x7f0d02e9

.field public static final zwd_switch:I = 0x7f0d02ea

.field public static final zwd_type_def:I = 0x7f0d02eb

.field public static final zwd_type_yzm:I = 0x7f0d02ec

.field public static final zy:I = 0x7f0d02ed

.field public static final zzquery:I = 0x7f0d02ee


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
