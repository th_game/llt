.class Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;
.super Landroid/os/AsyncTask;
.source "MainLLTSkb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/MainLLTSkb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InitResourceTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mainLLTSkbWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/MainLLTSkb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 1

    .line 1928
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1929
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->mainLLTSkbWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private testCode()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1924
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "init resource task begin"

    .line 1964
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\u521d\u59cb\u5316\u5931\u8d25\uff0c\u8bbe\u5907\u5269\u4f59\u5b58\u50a8\u7a7a\u95f4\u4e0d\u8db3\uff0c\u8bf7\u91ca\u653e\u8bbe\u5907\u7a7a\u95f4\u540e\u518d\u8bd5"

    .line 1970
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->init()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    return-object v0

    .line 1977
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->testCode()V

    .line 1978
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init resource task end ver="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1

    :catch_0
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1924
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 1944
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1945
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->mainLLTSkbWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/MainLLTSkb;

    if-eqz v0, :cond_0

    .line 1947
    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1800(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1950
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 1951
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d010f

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1952
    new-instance v2, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask$1;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 1934
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1935
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->load()V

    .line 1936
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->mainLLTSkbWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/MainLLTSkb;

    if-eqz v0, :cond_0

    .line 1938
    invoke-virtual {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->readConfig()V

    :cond_0
    return-void
.end method
