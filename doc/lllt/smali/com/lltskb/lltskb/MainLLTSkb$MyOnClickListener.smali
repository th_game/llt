.class public Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;
.super Ljava/lang/Object;
.source "MainLLTSkb.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/MainLLTSkb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyOnClickListener"
.end annotation


# instance fields
.field private index:I

.field final synthetic this$0:Lcom/lltskb/lltskb/MainLLTSkb;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V
    .locals 0

    .line 1260
    iput-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1261
    iput p2, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->index:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1266
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1500(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1267
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1600(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/CustomViewPager;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1000(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/ZZQueryTabView;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1700(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/SettingsTabView;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 1269
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1600(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/CustomViewPager;

    move-result-object p1

    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->index:I

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/CustomViewPager;->setCurrentItem(I)V

    .line 1270
    iget p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->index:I

    if-nez p1, :cond_1

    .line 1271
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1000(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/ZZQueryTabView;

    move-result-object p1

    const v0, 0x7f090084

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    if-eqz p1, :cond_2

    .line 1273
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 1276
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1700(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/SettingsTabView;

    move-result-object p1

    const v0, 0x7f090209

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/SwitchButton;

    if-eqz p1, :cond_2

    .line 1277
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    :cond_2
    :goto_0
    return-void
.end method
