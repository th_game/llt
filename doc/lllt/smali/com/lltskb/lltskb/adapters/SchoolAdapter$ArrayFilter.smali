.class Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;
.super Landroid/widget/Filter;
.source "SchoolAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/SchoolAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArrayFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/SchoolAdapter;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/adapters/SchoolAdapter;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/SchoolAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/adapters/SchoolAdapter;Lcom/lltskb/lltskb/adapters/SchoolAdapter$1;)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;-><init>(Lcom/lltskb/lltskb/adapters/SchoolAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .line 87
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_7

    .line 88
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_3

    .line 95
    :cond_0
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    const/16 v4, 0x41

    const/4 v5, 0x1

    if-lt v3, v4, :cond_1

    const/16 v4, 0x5a

    if-le v3, v4, :cond_2

    :cond_1
    const/16 v4, 0x61

    if-lt v3, v4, :cond_3

    const/16 v4, 0x7a

    if-gt v3, v4, :cond_3

    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    .line 100
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getSchoolsByPin(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_1

    .line 101
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getSchoolsByName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 103
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v5, :cond_5

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 104
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 105
    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 108
    :cond_5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    .line 110
    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 112
    :cond_6
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 113
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 90
    :cond_7
    :goto_3
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 91
    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .line 122
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/SchoolAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/adapters/SchoolAdapter;->access$102(Lcom/lltskb/lltskb/adapters/SchoolAdapter;Ljava/util/List;)Ljava/util/List;

    .line 123
    iget p1, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez p1, :cond_0

    .line 124
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/SchoolAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SchoolAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 126
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SchoolAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/SchoolAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SchoolAdapter;->notifyDataSetInvalidated()V

    :goto_0
    return-void
.end method
