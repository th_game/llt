.class Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;
.super Landroid/os/AsyncTask;
.source "ZwdListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/ZwdListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryZwdTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private adapterWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/adapters/ZwdListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private bJpk:Z

.field private date:Ljava/lang/String;

.field private dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;ZLjava/lang/String;)V
    .locals 1

    .line 363
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 364
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    .line 365
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    .line 366
    iput-boolean p3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->bJpk:Z

    .line 367
    iput-object p4, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->date:Ljava/lang/String;

    return-void
.end method

.method private queryStZWD(Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 465
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "H"

    .line 466
    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 470
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->get()Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    move-result-object v1

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->queryDelayInfo(Ljava/lang/String;)Z

    .line 472
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->get()Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    move-result-object v1

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->getZwdItem(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;

    move-result-object p1

    .line 473
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-eqz p1, :cond_2

    if-eqz v1, :cond_2

    .line 475
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$900(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-eqz v1, :cond_2

    const-string v2, "W"

    .line 477
    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 478
    new-instance v1, Lcom/lltskb/lltskb/engine/online/CdZwdQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/CdZwdQuery;-><init>()V

    .line 479
    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/CdZwdQuery;->queryZwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    return-object p1

    :cond_2
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 357
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 372
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 377
    :cond_0
    iget-boolean v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->bJpk:Z

    if-eqz v1, :cond_2

    .line 378
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->get()Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;

    move-result-object p1

    .line 380
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->date:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->getTicketCheck(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 381
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00f9

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1

    .line 387
    :cond_2
    invoke-static {p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$800(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "A"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 388
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->queryStZWD(Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 389
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    return-object v1

    .line 394
    :cond_3
    new-instance v1, Lcom/lltskb/lltskb/engine/online/ZWDQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;-><init>()V

    const/4 v2, 0x2

    move-object v3, v0

    :goto_0
    const-string v9, "retry"

    if-lez v2, :cond_5

    .line 399
    :try_start_0
    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    const-string v6, "1234"

    const/16 v7, 0x2ee0

    invoke-static {p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$800(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Ljava/lang/String;

    move-result-object v8

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;->requestZwd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 400
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v4, :cond_4

    goto :goto_1

    :cond_4
    add-int/lit8 v2, v2, -0x1

    const-wide/16 v4, 0x12c

    .line 405
    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v4

    .line 407
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_2

    .line 410
    :cond_5
    :goto_1
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_2
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz p1, :cond_6

    goto :goto_3

    :cond_6
    move-object v0, v3

    goto :goto_3

    .line 415
    :goto_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    :goto_3
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 357
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5

    .line 435
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 436
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-nez v0, :cond_0

    return-void

    .line 441
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "\u7f51\u7edc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 442
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iput-object p1, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    const/4 p1, 0x0

    .line 443
    iput p1, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 445
    iget-boolean v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->bJpk:Z

    if-nez v1, :cond_2

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->get()Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->isZwdReady(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 446
    :goto_0
    iget-object v1, v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 447
    iget-object v1, v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    .line 448
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->get()Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->getZwdItem(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 450
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$900(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 454
    :cond_2
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 456
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$702(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    const-string p1, ""

    .line 457
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$600(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 423
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 424
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->dto:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    const/4 v1, 0x1

    iput v1, v0, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 425
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-nez v0, :cond_0

    return-void

    .line 430
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method
