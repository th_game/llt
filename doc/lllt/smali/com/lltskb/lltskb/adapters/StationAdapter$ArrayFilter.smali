.class Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;
.super Landroid/widget/Filter;
.source "StationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/StationAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArrayFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/StationAdapter;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/adapters/StationAdapter;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/adapters/StationAdapter;Lcom/lltskb/lltskb/adapters/StationAdapter$1;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;-><init>(Lcom/lltskb/lltskb/adapters/StationAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 5

    .line 42
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 43
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getSx(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 53
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 54
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 55
    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 58
    :cond_1
    iput-object v2, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 59
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 45
    :cond_2
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getMri()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    .line 46
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez p1, :cond_3

    goto :goto_1

    .line 47
    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .line 68
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/adapters/StationAdapter;->access$002(Lcom/lltskb/lltskb/adapters/StationAdapter;Ljava/util/List;)Ljava/util/List;

    .line 69
    iget p1, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationAdapter;->notifyDataSetInvalidated()V

    :goto_0
    return-void
.end method
