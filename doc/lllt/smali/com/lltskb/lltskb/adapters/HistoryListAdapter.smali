.class public Lcom/lltskb/lltskb/adapters/HistoryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "HistoryListAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 41
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mData:Ljava/util/Vector;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    if-ltz p1, :cond_2

    .line 50
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 69
    check-cast p2, Landroid/widget/TextView;

    if-nez p2, :cond_0

    .line 71
    new-instance p2, Landroid/widget/TextView;

    iget-object p3, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 73
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 74
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x11

    .line 75
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setGravity(I)V

    const/high16 p1, 0x41800000    # 16.0f

    .line 76
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextSize(F)V

    const p1, -0x99999a

    .line 77
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 p1, 0xa

    const/4 p3, 0x0

    .line 78
    invoke-virtual {p2, p3, p1, p3, p1}, Landroid/widget/TextView;->setPadding(IIII)V

    return-object p2
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Vector;

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mData:Ljava/util/Vector;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 32
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->mData:Ljava/util/Vector;

    :goto_0
    return-void
.end method
