.class Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;
.super Ljava/lang/Object;
.source "SelectPassengerListAdapter.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->onItemSelected(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

.field final synthetic val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v1, "1"

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onYes()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    .line 96
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 99
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method
