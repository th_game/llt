.class Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;
.super Ljava/lang/Object;
.source "SelectPassengerListAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCheckedChanged ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectPassengerListAdapter"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    if-nez p1, :cond_0

    return-void

    .line 177
    :cond_0
    iget-boolean v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-nez p2, :cond_5

    .line 179
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    sub-int/2addr p2, v1

    iput p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    .line 180
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 181
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 184
    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$200(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-boolean v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    if-nez v2, :cond_2

    .line 185
    sget-object p1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object p2

    const v2, 0x7f0d013f

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$300(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {p1, p2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 186
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$400(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$500(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 191
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object p2

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0264

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    const/4 p2, 0x0

    :cond_3
    if-eqz p2, :cond_4

    .line 195
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 196
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d00a0

    const v2, 0x7f0d00a1

    new-instance v3, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;

    invoke-direct {v3, p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    .line 214
    :cond_4
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-boolean p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    .line 215
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 216
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    :cond_5
    :goto_0
    return-void
.end method
