.class Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;
.super Ljava/lang/Object;
.source "SelectPassengerListAdapter.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

.field final synthetic val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 3

    .line 207
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v1, "1"

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iget-object v1, v1, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iget-object v0, v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 210
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iget-object v0, v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onYes()V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iget-object v0, v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 202
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3$1;->this$1:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    iget-object v0, v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method
