.class Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;
.super Landroid/os/AsyncTask;
.source "ZwdListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/ZwdListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryZwdYzmTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private adapterWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/adapters/ZwdListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;

.field private yzm:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 493
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-string v0, "0"

    .line 492
    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->type:Ljava/lang/String;

    .line 494
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    .line 495
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->yzm:Ljava/lang/String;

    .line 496
    iput-object p3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 488
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 501
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 506
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/ZWDQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;-><init>()V

    .line 514
    :try_start_0
    invoke-static {p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object v2

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object p1

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->yzm:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, v3, v4}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;->query(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 516
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 488
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 536
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 537
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-nez v0, :cond_0

    return-void

    .line 541
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object v1

    iput-object p1, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    .line 542
    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object p1

    const/4 v1, 0x0

    iput v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 543
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 524
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 525
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-nez v0, :cond_0

    return-void

    .line 530
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 531
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method
