.class public Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;
.super Landroid/widget/BaseAdapter;
.source "BigScreenStationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;,
        Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;
    }
.end annotation


# instance fields
.field private alphaIndexer:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private inflater:Landroid/view/LayoutInflater;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;",
            "Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    iput-object p3, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->listener:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;

    .line 38
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 39
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    .line 40
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->alphaIndexer:Ljava/util/Map;

    const/4 p1, 0x0

    const/4 p3, 0x0

    .line 42
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_2

    .line 43
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    add-int/lit8 v1, p3, -0x1

    if-ltz v1, :cond_0

    .line 44
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    goto :goto_1

    :cond_0
    const-string v1, " "

    .line 46
    :goto_1
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iput-boolean p1, v2, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->isShowPY:Z

    .line 47
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->alphaIndexer:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->isShowPY:Z

    :cond_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;)Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->listener:Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$Listener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;)Ljava/util/List;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getStationMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->alphaIndexer:Ljava/util/Map;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 79
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->inflater:Landroid/view/LayoutInflater;

    const p3, 0x7f0b0050

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 80
    new-instance p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;

    invoke-direct {p3, p0, v0}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;-><init>(Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$1;)V

    const v0, 0x7f090108

    .line 82
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->alpha:Landroid/widget/TextView;

    const v0, 0x7f090109

    .line 84
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const v0, 0x7f0901c9

    .line 86
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->rl_item:Landroid/widget/RelativeLayout;

    .line 87
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;

    .line 92
    :goto_0
    iget-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-boolean v0, v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->isShowPY:Z

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->alpha:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->alpha:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;->list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->firstPY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 98
    :cond_1
    iget-object v0, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->alpha:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    :goto_1
    iget-object p3, p3, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$ViewHolder;->rl_item:Landroid/widget/RelativeLayout;

    new-instance v0, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/BigScreenStationAdapter;I)V

    invoke-virtual {p3, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method
