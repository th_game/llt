.class Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;
.super Landroid/widget/Filter;
.source "StationSelectAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/StationSelectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StationFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Lcom/lltskb/lltskb/adapters/StationSelectAdapter$1;)V
    .locals 0

    .line 125
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;-><init>(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3

    .line 129
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 130
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 184
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 185
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getSx(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    .line 187
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez p1, :cond_1

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 131
    :cond_2
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getMri()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    .line 132
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez p1, :cond_3

    goto :goto_2

    .line 133
    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_2
    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mri size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "StationSelectAdapter"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_4

    .line 136
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 139
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$100(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 140
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 142
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5317\u4eac"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 143
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u4e0a\u6d77"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 144
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5e7f\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 145
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6df1\u5733"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 146
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5929\u6d25"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 147
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5357\u4eac"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 148
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u676d\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 149
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u798f\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 150
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6b66\u6c49"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 151
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6c88\u9633"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 152
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6210\u90fd"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 153
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u91cd\u5e86"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 154
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u957f\u6c99"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 155
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u897f\u5b89"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 156
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u90d1\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 157
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5408\u80a5"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 158
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5f90\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 159
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u9752\u5c9b"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 160
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6d4e\u5357"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 161
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u82cf\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 162
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5170\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 163
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5357\u660c"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 164
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5e38\u5dde"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 165
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6606\u660e"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 166
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u5357\u5b81"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 167
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u957f\u6625"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 168
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u54c8\u5c14\u6ee8"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 169
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u8d35\u9633"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 170
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u547c\u548c\u6d69\u7279"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 171
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u6d77\u53e3"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 172
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u62c9\u8428"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 173
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u592a\u539f"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 174
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u94f6\u5ddd"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 175
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u53a6\u95e8"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 176
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u4e4c\u9c81\u6728\u9f50"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 177
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const-string v2, "\u77f3\u5bb6\u5e84"

    invoke-static {v1, v2, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 179
    :cond_5
    iput-object p1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 180
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .line 196
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->access$302(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/util/List;)Ljava/util/List;

    .line 197
    iget p1, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez p1, :cond_0

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 200
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;->this$0:Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->notifyDataSetInvalidated()V

    :goto_0
    return-void
.end method
