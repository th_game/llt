.class public Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;
.super Landroid/widget/BaseAdapter;
.source "NoCompleteListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketTask;,
        Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mActivity:Landroid/app/Activity;

    .line 53
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->initData()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->onHotel(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->returnTicketAffirm(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V

    return-void
.end method

.method private getDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 114
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd HH:mm"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 117
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 123
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 119
    invoke-virtual {p1}, Ljava/text/ParseException;->printStackTrace()V

    const-string p1, "00:00"

    return-object p1
.end method

.method private getTime(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 100
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 103
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "HH:mm"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 110
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 106
    invoke-virtual {p1}, Ljava/text/ParseException;->printStackTrace()V

    const-string p1, "00:00"

    return-object p1
.end method

.method private initData()V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 44
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method private initView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 2

    .line 127
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->getDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f090273

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 128
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    const v1, 0x7f090288

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 129
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->start_time:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->getTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0902e5

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 130
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    const v1, 0x7f0902f8

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 131
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->arrive_time:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->getTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f09024d

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 132
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_type_name:Ljava/lang/String;

    const v1, 0x7f0902db

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 134
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    const v1, 0x7f0902fe

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u53a2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f090265

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 136
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_name:Ljava/lang/String;

    const v1, 0x7f0902d9

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 138
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    const v1, 0x7f0902bb

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 139
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_name:Ljava/lang/String;

    const v1, 0x7f090290

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 140
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_type_name:Ljava/lang/String;

    const v1, 0x7f0902f2

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u00a5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->str_ticket_price_page:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0902f0

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    .line 143
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_status_name:Ljava/lang/String;

    const v1, 0x7f0902f1

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->setItemText(Landroid/view/View;ILjava/lang/String;)V

    const v0, 0x7f090057

    .line 145
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    new-instance v1, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    const v0, 0x7f09006a

    .line 156
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 158
    new-instance v0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$2;-><init>(Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 169
    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->return_flag:Ljava/lang/String;

    const-string v0, "Y"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    .line 170
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 p2, 0x8

    .line 172
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private onHotel(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 3

    if-eqz p1, :cond_5

    .line 190
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    if-nez v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v1, "\u4e1c"

    .line 197
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "\u5357"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "\u897f"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "\u5317"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 198
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_3

    const/4 v1, 0x0

    .line 199
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v1, "\u4e0a\u6d77\u8679\u6865"

    .line 202
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "\u4e0a\u6d77"

    .line 204
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mActivity:Landroid/app/Activity;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->getDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void
.end method

.method private returnTicketAffirm(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 181
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v0, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;-><init>(Landroid/app/Activity;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, p1, v1

    .line 185
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_0
    return-void
.end method

.method private setItemText(Landroid/view/View;ILjava/lang/String;)V
    .locals 0

    .line 208
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_0

    return-void

    .line 210
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 62
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mData:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 72
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->mActivity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    if-eqz v0, :cond_0

    const p2, 0x7f0b0060

    const/4 v1, 0x0

    .line 88
    invoke-virtual {v0, p2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 92
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    if-eqz p1, :cond_1

    .line 94
    invoke-direct {p0, p2, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->initView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V

    :cond_1
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->initData()V

    .line 36
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
