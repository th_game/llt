.class public Lcom/lltskb/lltskb/adapters/CompleteListAdapter;
.super Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;
.source "CompleteListAdapter.java"


# instance fields
.field private mData:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->mData:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->mData:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 39
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->mData:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public setData(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/CompleteListAdapter;->mData:Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    return-void
.end method
