.class public Lcom/lltskb/lltskb/adapters/StopStationListAdapter;
.super Landroid/widget/BaseAdapter;
.source "StopStationListAdapter.java"


# instance fields
.field mContext:Landroid/content/Context;

.field mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method private setupView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;)V
    .locals 3

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const v0, 0x7f0902e9

    .line 88
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->stopover_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09024d

    .line 91
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 92
    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 42
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    .line 70
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0b008e

    .line 71
    invoke-virtual {p2, v0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 74
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;

    if-nez p1, :cond_1

    return-object p2

    .line 78
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->setupView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;)V

    return-object p2
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;",
            ">;)V"
        }
    .end annotation

    .line 31
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->mData:Ljava/util/Vector;

    .line 32
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/StopStationListAdapter;->notifyDataSetChanged()V

    return-void
.end method
