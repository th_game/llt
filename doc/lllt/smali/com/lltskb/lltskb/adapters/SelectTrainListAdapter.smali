.class public Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SelectTrainListAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectTrainListAdapter"


# instance fields
.field private mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    new-instance v0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 36
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "click ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectTrainListAdapter"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-eqz p1, :cond_0

    .line 51
    iget-boolean v0, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    .line 52
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 67
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedItems()Ljava/util/Vector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 40
    :cond_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 41
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 42
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 107
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0b0093

    .line 108
    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 111
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez p1, :cond_1

    return-object p2

    :cond_1
    const p3, 0x7f09000d

    .line 113
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    const v1, 0x7f090008

    .line 114
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09000c

    .line 115
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f09000b

    .line 116
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f09008e

    .line 118
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 119
    invoke-virtual {v4, p1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-boolean v5, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 122
    iget-object v5, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 124
    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object p3, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0d0152

    invoke-virtual {p3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 128
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    aput-object v6, v5, v0

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const/4 v8, 0x2

    aput-object v6, v5, v8

    const/4 v6, 0x3

    iget-object v9, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    aput-object v9, v5, v6

    invoke-static {v4, p3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 131
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object p3, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v1, ":"

    invoke-static {p3, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_2

    .line 134
    array-length v1, p3

    if-lt v1, v8, :cond_2

    .line 135
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0d012f

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    aget-object v6, p3, v0

    aput-object v6, v5, v0

    aget-object p3, p3, v7

    aput-object p3, v5, v7

    invoke-static {v4, v1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->toHtmlString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;)V"
        }
    .end annotation

    .line 57
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->mData:Ljava/util/Vector;

    .line 58
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/SelectTrainListAdapter;->notifyDataSetChanged()V

    return-void
.end method
