.class Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;
.super Ljava/lang/Object;
.source "SelectPassengerListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->onSelectPassengerType(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

.field final synthetic val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    if-nez p3, :cond_0

    .line 359
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string p2, "1"

    iput-object p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 360
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f0d0046

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    if-ne p3, p1, :cond_1

    .line 362
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string p2, "3"

    iput-object p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 363
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->val$item:Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {p2}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f0d0291

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 365
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method
