.class public final Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HoubuListItemAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;,
        Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<",
        "Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001b\u001cB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;",
        "()V",
        "houbuTicketList",
        "",
        "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
        "getHoubuTicketList",
        "()Ljava/util/List;",
        "setHoubuTicketList",
        "(Ljava/util/List;)V",
        "lister",
        "Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;",
        "getLister",
        "()Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;",
        "setLister",
        "(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;)V",
        "getItemCount",
        "",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "Listener",
        "ViewHolder",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private houbuTicketList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
            ">;"
        }
    .end annotation
.end field

.field private lister:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final getHoubuTicketList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->houbuTicketList:Ljava/util/List;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->houbuTicketList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 57
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getLister()Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->lister:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 13
    check-cast p1, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->onBindViewHolder(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;I)V
    .locals 5

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->getItemCount()I

    move-result v0

    if-lt p2, v0, :cond_0

    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->houbuTicketList:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    goto :goto_0

    :cond_1
    move-object p2, v1

    :goto_0
    if-nez p2, :cond_2

    return-void

    .line 36
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getTrainCodeTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v2

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getFromTextView()Landroid/widget/TextView;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v3

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v4

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getToTextView()Landroid/widget/TextView;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v4

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v4

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getSeatTextView()Landroid/widget/TextView;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    move-result-object v4

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getSeatType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatNameFromType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getInfoTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->getSuccessRate()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;->getFlag()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateFlag;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateFlag;->getInfo()Ljava/lang/String;

    move-result-object v1

    :cond_3
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getDelButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 42
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;->getDelButton()Landroid/widget/Button;

    move-result-object p1

    new-instance p2, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1;-><init>(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0b004d

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 23
    new-instance p2, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;

    const-string v0, "inflate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.method public final setHoubuTicketList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
            ">;)V"
        }
    .end annotation

    .line 15
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->houbuTicketList:Ljava/util/List;

    return-void
.end method

.method public final setLister(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->lister:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;

    return-void
.end method
