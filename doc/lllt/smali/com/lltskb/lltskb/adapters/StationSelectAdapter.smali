.class public Lcom/lltskb/lltskb/adapters/StationSelectAdapter;
.super Landroid/widget/BaseAdapter;
.source "StationSelectAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;,
        Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "StationSelectAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;

.field private mIsBigMode:Z

.field private mListener:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;

.field private mValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mContext:Landroid/content/Context;

    .line 46
    iput-boolean p2, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mIsBigMode:Z

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)Z
    .locals 0

    .line 24
    iget-boolean p0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mIsBigMode:Z

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->addStation(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$302(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mValues:Ljava/util/List;

    return-object p1
.end method

.method private addStation(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 111
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 114
    :cond_0
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mValues:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 57
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;-><init>(Lcom/lltskb/lltskb/adapters/StationSelectAdapter;Lcom/lltskb/lltskb/adapters/StationSelectAdapter$1;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$StationFilter;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mValues:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mValues:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 86
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0b008b

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const/4 p3, 0x1

    .line 87
    invoke-virtual {p2, p3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    const v0, 0x7f090203

    .line 88
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_1
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {p2, p3}, Landroid/view/View;->setLongClickable(Z)V

    return-object p2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f090203

    .line 208
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_0

    return-void

    .line 210
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 211
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mListener:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;

    if-eqz v0, :cond_1

    .line 212
    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;->onItemSelected(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setListener(Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->mListener:Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;

    return-void
.end method
