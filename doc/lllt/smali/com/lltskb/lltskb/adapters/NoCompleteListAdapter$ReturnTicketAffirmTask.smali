.class Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;
.super Landroid/os/AsyncTask;
.source "NoCompleteListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReturnTicketAffirmTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private activityWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private info:Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)V
    .locals 1

    .line 217
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 218
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    .line 219
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->info:Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    return-void
.end method

.method private showReturnConfirm(Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;)V
    .locals 4

    .line 273
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->train_date:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 275
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->start_time:Ljava/lang/String;

    const/16 v2, 0xb

    const/16 v3, 0x10

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ",\r\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u6b21\u5217\u8f66,\r\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->coach_name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u8f66"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->seat_name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->seat_type_name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ",\r\n\u7968\u4ef7"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->ticket_price:D

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, "\u5143,\u9000\u7968\u8d39"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->return_cost:D

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, "\u5143,\u5e94\u9000\u7968\u6b3e"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->return_price:D

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, "\u5143\r\n\u786e\u8ba4\u9000\u7968\u5417?"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 284
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 289
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask$1;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask$1;-><init>(Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;Landroid/app/Activity;)V

    const-string v2, "\u8bf7\u786e\u8ba4"

    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 213
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 252
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object p1

    .line 255
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->info:Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->returnTicketAffirm(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;

    move-result-object v0

    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    .line 264
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 265
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .line 233
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 234
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 235
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 236
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 240
    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;

    if-eqz v1, :cond_1

    .line 241
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;

    .line 242
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->showReturnConfirm(Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;)V

    return-void

    .line 245
    :cond_1
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_2

    const v1, 0x7f0d010f

    .line 246
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast p1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :cond_2
    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 223
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter$ReturnTicketAffirmTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 224
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f0d01a6

    .line 227
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0xffffff

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 228
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    :cond_1
    :goto_0
    return-void
.end method
