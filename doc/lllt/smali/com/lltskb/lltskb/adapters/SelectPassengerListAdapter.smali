.class public Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SelectPassengerListAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectPassengerListAdapter"


# instance fields
.field private mAddOnClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mPurpose:Ljava/lang/String;

.field private maxCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x5

    .line 40
    iput v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->maxCount:I

    .line 149
    new-instance v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mAddOnClickListener:Landroid/view/View$OnClickListener;

    .line 169
    new-instance v0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$3;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    .line 46
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    .line 47
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mPurpose:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    return-object p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Z
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isExceedMax()Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->maxCount:I

    return p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Z
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPurposeStudent()Z

    move-result p0

    return p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)Z
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPassengerStudent(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Landroid/view/View;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->onSelectPassengerType(Landroid/view/View;)V

    return-void
.end method

.method private isExceedMax()Z
    .locals 2

    .line 55
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iget v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->maxCount:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isPassengerStudent(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 67
    :cond_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v1, :cond_1

    return v0

    .line 71
    :cond_1
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    const-string v1, "3"

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    return p1

    .line 75
    :cond_2
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private isPurposeStudent()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mPurpose:Ljava/lang/String;

    const-string v1, "0X00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private onItemSelected(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V
    .locals 4

    if-eqz p1, :cond_6

    .line 79
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 80
    :cond_0
    iget-boolean v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 81
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    sub-int/2addr v0, v1

    iput v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    goto :goto_0

    .line 83
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPurposeStudent()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPassengerStudent(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0264

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 85
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-boolean v2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    goto :goto_0

    .line 87
    :cond_2
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-boolean v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    if-eqz v0, :cond_3

    .line 88
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-boolean v2, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    goto :goto_0

    .line 90
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isExceedMax()Z

    move-result v0

    if-nez v0, :cond_5

    .line 91
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0d00a0

    const v2, 0x7f0d00a1

    new-instance v3, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;

    invoke-direct {v3, p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_0

    .line 112
    :cond_4
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-boolean v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    goto :goto_0

    .line 116
    :cond_5
    sget-object p1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0d013f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget v3, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->maxCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 122
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    .line 123
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    :cond_6
    :goto_1
    return-void
.end method

.method private onSelectPassengerType(Landroid/view/View;)V
    .locals 4

    .line 343
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    if-nez p1, :cond_0

    return-void

    .line 346
    :cond_0
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 348
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    .line 351
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 352
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0046

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0291

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/lltskb/lltskb/BaseActivity;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    new-instance v3, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;

    invoke-direct {v3, p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$5;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V

    invoke-static {v1, v0, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;Ljava/lang/String;Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 2

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "click ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectPassengerListAdapter"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 129
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->onItemSelected(Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const v0, 0x7f09008f

    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 226
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v2, 0x7f0b0077

    .line 227
    invoke-virtual {p2, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 228
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/CheckBox;

    :cond_0
    const p3, 0x7f0902a8

    .line 231
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    const v2, 0x7f090300

    .line 232
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 233
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v3, 0x7f09028e

    .line 234
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f09024a

    .line 235
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/4 v5, 0x0

    .line 237
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 238
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    if-nez p1, :cond_1

    return-object p2

    .line 240
    :cond_1
    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 241
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 244
    iget-boolean v6, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    const/4 v7, 0x4

    if-nez v6, :cond_3

    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPurposeStudent()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v8, "2"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0

    .line 247
    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mAddOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 245
    :cond_3
    :goto_0
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    :goto_1
    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {p3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v8, "3"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    .line 256
    iget-boolean v8, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    const/4 v9, 0x1

    if-eqz v8, :cond_4

    .line 257
    iget-object v8, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v10, 0x7f0d009f

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 260
    :cond_4
    iget-object v8, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v8, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    .line 261
    iget-object v10, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v10, v10, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    invoke-static {v10}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 262
    iget-object v8, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v8, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name_real:Ljava/lang/String;

    :cond_5
    if-eqz v6, :cond_6

    .line 266
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " \u25e2"

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 268
    :cond_6
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->isPurposeStudent()Z

    move-result v8

    if-eqz v8, :cond_7

    if-nez v6, :cond_7

    .line 271
    iget-object v8, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-boolean v1, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    .line 273
    :cond_7
    iget-object v8, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-boolean v8, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 276
    :goto_2
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    if-eqz v6, :cond_8

    .line 278
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setClickable(Z)V

    .line 279
    iget-object v5, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f060025

    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 280
    new-instance v5, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$4;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$4;-><init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 287
    :cond_8
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 288
    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f060021

    invoke-static {v6, v8}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 289
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    :goto_3
    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->getSecurePassenderId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 297
    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 298
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    const-string v2, "#ff0077ff"

    if-eqz p1, :cond_d

    const/high16 v3, -0x10000

    if-eq p1, v9, :cond_c

    const/4 v5, 0x2

    if-eq p1, v5, :cond_b

    const/4 v2, 0x3

    if-eq p1, v2, :cond_a

    if-eq p1, v7, :cond_9

    .line 326
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 327
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 328
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 329
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 320
    :cond_9
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/high16 p1, -0x1000000

    .line 321
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 322
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 323
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 308
    :cond_a
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 310
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 311
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 304
    :cond_b
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 305
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_4

    .line 314
    :cond_c
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 315
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 316
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 317
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 300
    :cond_d
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 301
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :goto_4
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .line 338
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->mData:Ljava/util/Vector;

    .line 339
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setMaCount(I)V
    .locals 0

    .line 51
    iput p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->maxCount:I

    return-void
.end method
