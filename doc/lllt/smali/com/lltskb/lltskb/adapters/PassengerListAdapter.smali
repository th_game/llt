.class public Lcom/lltskb/lltskb/adapters/PassengerListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PassengerListAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mPass:Lcom/lltskb/lltskb/engine/online/PassengerModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 46
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mPass:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    .line 47
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    .line 48
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mPass:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 49
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 63
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 91
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0b0098

    .line 92
    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f0902a8

    .line 95
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    const v1, 0x7f090300

    .line 96
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09008f

    .line 97
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const v3, 0x7f0902ea

    .line 98
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f09028e

    .line 99
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 101
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez p1, :cond_1

    return-object p2

    .line 103
    :cond_1
    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-boolean v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 107
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->getSecurePassenderId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v1, 0x8

    .line 109
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 110
    iget p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    const-string v1, "#ff0077ff"

    const/4 v4, 0x1

    if-eqz p1, :cond_6

    const/high16 v5, -0x10000

    if-eq p1, v4, :cond_5

    const/4 v6, 0x2

    if-eq p1, v6, :cond_4

    const/4 v1, 0x3

    const v6, 0x7f0d02d6

    if-eq p1, v1, :cond_3

    const/4 v1, 0x4

    if-eq p1, v1, :cond_2

    .line 142
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 144
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_0

    .line 136
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    const v0, 0x7f0d02d4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 p1, -0x1000000

    .line 137
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 124
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 126
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 127
    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 118
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    const v0, 0x7f0d02d5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 130
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0d02d2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 112
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mContext:Landroid/content/Context;

    const v0, 0x7f0d02d3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :goto_0
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .line 29
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mPass:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mPass:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 32
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    .line 33
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    .line 35
    :cond_0
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
