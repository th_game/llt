.class Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;
.super Landroid/widget/Filter;
.source "TrainAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/TrainAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArrayFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/TrainAdapter;


# direct methods
.method private constructor <init>(Lcom/lltskb/lltskb/adapters/TrainAdapter;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/TrainAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lltskb/lltskb/adapters/TrainAdapter;Lcom/lltskb/lltskb/adapters/TrainAdapter$1;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;-><init>(Lcom/lltskb/lltskb/adapters/TrainAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .line 39
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    .line 40
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainSx(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 49
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 50
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 51
    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 54
    :cond_1
    iput-object v3, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 55
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0

    .line 42
    :cond_2
    :goto_0
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 43
    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .line 64
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/TrainAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/adapters/TrainAdapter;->access$002(Lcom/lltskb/lltskb/adapters/TrainAdapter;Ljava/util/List;)Ljava/util/List;

    .line 65
    iget p1, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez p1, :cond_0

    .line 66
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/TrainAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/TrainAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 68
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainAdapter$ArrayFilter;->this$0:Lcom/lltskb/lltskb/adapters/TrainAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/TrainAdapter;->notifyDataSetInvalidated()V

    :goto_0
    return-void
.end method
