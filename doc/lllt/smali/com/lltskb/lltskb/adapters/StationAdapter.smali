.class public Lcom/lltskb/lltskb/adapters/StationAdapter;
.super Landroid/widget/BaseAdapter;
.source "StationAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilter:Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;

.field private mObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/adapters/StationAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mObjects:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mObjects:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 90
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;-><init>(Lcom/lltskb/lltskb/adapters/StationAdapter;Lcom/lltskb/lltskb/adapters/StationAdapter$1;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mFilter:Lcom/lltskb/lltskb/adapters/StationAdapter$ArrayFilter;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mObjects:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 97
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p2, :cond_0

    .line 109
    new-instance p2, Landroid/widget/TextView;

    iget-object p3, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 110
    iget-object p3, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mObjects:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 112
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    .line 113
    iget-object p3, p0, Lcom/lltskb/lltskb/adapters/StationAdapter;->mObjects:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const p1, -0xddddde

    .line 116
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 p1, 0x11

    .line 117
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setGravity(I)V

    const/4 p1, 0x5

    const/16 p3, 0xf

    .line 118
    invoke-virtual {p2, p3, p1, p3, p1}, Landroid/widget/TextView;->setPadding(IIII)V

    const/16 p1, 0x64

    .line 119
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setMinWidth(I)V

    const/high16 p1, 0x41900000    # 18.0f

    .line 121
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-object p2
.end method
