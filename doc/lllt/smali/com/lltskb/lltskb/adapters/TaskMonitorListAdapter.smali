.class public Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;
.super Landroid/widget/BaseAdapter;
.source "TaskMonitorListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;

    .line 36
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;)Lcom/lltskb/lltskb/order/MonitorManager;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;

    return-object p0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    if-ltz p1, :cond_1

    .line 53
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mMonitor:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/order/MonitorManager;->get(I)Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    move-object/from16 v0, p0

    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 72
    iget-object v2, v0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0b005b

    move-object/from16 v4, p3

    .line 73
    invoke-virtual {v2, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object/from16 v2, p2

    .line 77
    :goto_0
    invoke-virtual/range {p0 .. p1}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    if-nez v3, :cond_1

    return-object v2

    .line 79
    :cond_1
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getOrderConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v4

    if-nez v4, :cond_2

    return-object v2

    .line 84
    :cond_2
    iget-object v5, v0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f0d0143

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 85
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    const/4 v7, 0x3

    new-array v8, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v1

    .line 86
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    aput-object v9, v8, v10

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x2

    aput-object v9, v8, v11

    .line 85
    invoke-static {v6, v5, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0902f6

    .line 87
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 88
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f090277

    .line 90
    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 92
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object v8

    const-string v9, ","

    invoke-static {v8, v9}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 95
    array-length v12, v8

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v12, :cond_4

    aget-object v14, v8, v13

    .line 96
    invoke-static {v14}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 97
    invoke-static {v15}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 98
    invoke-static {v14}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 103
    :cond_4
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 104
    iget-object v8, v0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0d0141

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 105
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v1

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v10

    .line 106
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v11

    .line 105
    invoke-static {v9, v8, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 108
    :cond_5
    iget-object v7, v0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0d0142

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 109
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    new-array v9, v11, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v1

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v8, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const v4, 0x7f0902ea

    .line 114
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 116
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getProgress()I

    move-result v5

    .line 117
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getOrderParams()Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    move-result-object v6

    .line 119
    iget-object v7, v0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0d014b

    .line 120
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v1

    .line 119
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 122
    iget-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-static {v7}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 123
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " -"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v6, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 126
    :cond_6
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f09004d

    .line 128
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 130
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 131
    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    const v4, 0x7f090072

    .line 134
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 136
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 137
    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    const v4, 0x7f0901a8

    .line 140
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    if-eqz v4, :cond_a

    .line 142
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->isPaused()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v1, 0x8

    :cond_9
    invoke-virtual {v4, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_a
    return-object v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 151
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 153
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    if-nez v1, :cond_1

    return-void

    .line 156
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f090072

    if-ne v2, v3, :cond_3

    .line 157
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->resume()V

    .line 159
    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0d0213

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->pause()V

    .line 162
    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0d027d

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    .line 164
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 165
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v2, 0x7f09004d

    if-ne p1, v2, :cond_4

    .line 167
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0d02de

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 168
    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0d00b8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 169
    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;I)V

    invoke-static {v3, p1, v2, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :cond_4
    :goto_1
    return-void
.end method
