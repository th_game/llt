.class public Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MyOrderListAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mContext:Landroid/content/Context;

    .line 43
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    .line 44
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->initData()V

    return-void
.end method

.method private initData()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 33
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderComplete()Ljava/util/Vector;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private refresh(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V
    .locals 9

    if-nez p2, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0902b1

    .line 80
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 81
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_date:Ljava/lang/String;

    .line 82
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0xa

    if-le v2, v4, :cond_1

    .line 83
    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const v0, 0x7f0902b5

    .line 88
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902fc

    .line 91
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->from_station_name_page:Ljava/lang/String;

    .line 93
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const-string v4, "\""

    const-string v5, "]"

    const-string v6, "["

    const-string v7, ""

    if-nez v2, :cond_2

    .line 94
    invoke-static {v1, v6, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v1, v5, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-static {v1, v4, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    :cond_2
    iget-object v2, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->to_station_name_page:Ljava/lang/String;

    .line 100
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 101
    invoke-static {v2, v6, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-static {v2, v5, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-static {v2, v4, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 105
    :cond_3
    iget-object v4, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0d014c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 106
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v8, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->train_code_page:Ljava/lang/String;

    aput-object v8, v6, v3

    const/4 v8, 0x1

    aput-object v1, v6, v8

    const/4 v1, 0x2

    aput-object v2, v6, v1

    invoke-static {v5, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902e5

    .line 109
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 110
    iget-object v2, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->start_train_date_page:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0d014a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_total_price_page:Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-static {v2, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0902c0

    .line 114
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 115
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902ee

    .line 117
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    iget v2, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_totalnum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902b7

    .line 120
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    iget-object v2, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902ba

    .line 123
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_4

    .line 124
    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 125
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d0149

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v1, v3

    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v1, v8

    invoke-static {v2, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 127
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    .line 129
    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 68
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    if-eqz v0, :cond_0

    const p2, 0x7f0b002c

    const/4 v1, 0x0

    .line 70
    invoke-virtual {v0, p2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 73
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 74
    invoke-direct {p0, p2, p1}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->refresh(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;)V

    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/MyOrderListAdapter;->initData()V

    .line 25
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
