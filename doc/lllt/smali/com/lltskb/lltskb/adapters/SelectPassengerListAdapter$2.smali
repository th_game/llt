.class Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;
.super Ljava/lang/Object;
.source "SelectPassengerListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onClick ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SelectPassengerListAdapter"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    if-nez p1, :cond_0

    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$200(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 157
    sget-object p1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d013f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v3}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$300(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 158
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$100(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 162
    :cond_1
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    add-int/2addr v0, v1

    iput v0, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    .line 163
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengersIncludeChild()Ljava/util/Vector;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->access$002(Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;Ljava/util/Vector;)Ljava/util/Vector;

    .line 164
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter$2;->this$0:Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/SelectPassengerListAdapter;->notifyDataSetChanged()V

    return-void
.end method
