.class public Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;
.super Landroid/widget/BaseAdapter;
.source "MoreTicketsListAdatper.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mFrom:Ljava/lang/String;

.field private mTo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mFrom:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mTo:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 50
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 59
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 77
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0b005e

    .line 78
    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 81
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;

    if-nez p1, :cond_1

    return-object p2

    :cond_1
    const p3, 0x7f090071

    .line 83
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 85
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    const-string v2, ""

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 89
    :goto_0
    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mCurTo:Ljava/lang/String;

    .line 90
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 91
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 94
    :cond_3
    iget-boolean v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIsFromStart:Z

    const-string v4, "</font> "

    const-string v5, " <font color=\"#167efb\">"

    if-eqz v1, :cond_4

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mFrom:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 97
    :cond_4
    iget v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    if-gez v1, :cond_5

    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mFrom:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mTo:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 100
    :cond_5
    iget v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    const-string v4, "</font>"

    if-nez v1, :cond_6

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mFrom:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 104
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mFrom:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mTo:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const p3, 0x7f090293

    .line 108
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 111
    iget-boolean v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIsFromStart:Z

    const/4 v3, 0x1

    if-eqz v1, :cond_7

    .line 112
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v4, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0126

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    iget v6, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 114
    :cond_7
    iget v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    if-gez v1, :cond_8

    .line 115
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v4, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0124

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    iget v6, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    neg-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 116
    :cond_8
    iget v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    if-nez v1, :cond_9

    .line 117
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v4, 0x7f0d0208

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 119
    :cond_9
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v4, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0125

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    iget v6, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mIndex:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 121
    :goto_2
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0901a7

    .line 123
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const v4, 0x7f0902f4

    .line 125
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 127
    iget v5, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mStatus:I

    const/16 v6, 0x8

    if-eqz v5, :cond_c

    if-eq v5, v3, :cond_b

    const/4 v3, 0x2

    if-eq v5, v3, :cond_a

    goto :goto_3

    .line 139
    :cond_a
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;->mTickts:Ljava/lang/String;

    .line 140
    invoke-virtual {p3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 141
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 134
    :cond_b
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v1, 0x7f0d01a7

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 136
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 129
    :cond_c
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mContext:Landroid/content/Context;

    const v1, 0x7f0d02dd

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-virtual {p3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 131
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :goto_3
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_d

    .line 145
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 147
    :cond_d
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    return-object p2
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/MoreTicketsDTO;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-virtual {p1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Vector;

    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->mData:Ljava/util/Vector;

    .line 40
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/MoreTicketsListAdatper;->notifyDataSetChanged()V

    return-void
.end method
