.class public Lcom/lltskb/lltskb/adapters/ZwdListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ZwdListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;,
        Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;,
        Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ZwdListAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDTO:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

.field mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

.field private mPassEdt:Landroid/widget/EditText;

.field private mPassImage:Landroid/widget/ImageView;

.field private mProgBar:Landroid/widget/ProgressBar;

.field private mQueryDate:Ljava/lang/String;

.field private mQueryJpk:Z

.field private mSelectMode:Z

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const/4 p1, 0x0

    .line 61
    iput-boolean p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mSelectMode:Z

    .line 62
    iput-boolean p2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mQueryJpk:Z

    .line 63
    iput-object p3, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mQueryDate:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mType:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Landroid/view/View;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->onQuery(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Landroid/widget/ProgressBar;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mProgBar:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->setPassImage(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Landroid/widget/ImageView;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->requestPassCode()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Landroid/support/v7/app/AppCompatDialog;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Landroid/widget/EditText;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassEdt:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Ljava/lang/String;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->queryZwd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mDTO:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    return-object p0
.end method

.method static synthetic access$702(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mDTO:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    return-object p1
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)Ljava/lang/String;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mType:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;)Ljava/lang/String;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->formatZwdInfo(Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private formatZwdInfo(Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0158

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 234
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->message:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->arrive:Ljava/lang/String;

    aput-object p1, v2, v3

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private onQuery(Landroid/view/View;)V
    .locals 4

    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    .line 199
    new-instance v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mQueryJpk:Z

    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mQueryDate:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;ZLjava/lang/String;)V

    .line 200
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private queryZwd(Ljava/lang/String;)V
    .locals 4

    .line 241
    new-instance v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mType:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$QueryZwdYzmTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private requestPassCode()V
    .locals 5

    const-string v0, "ZwdListAdapter"

    const-string v1, "requestPassCode"

    .line 337
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    new-instance v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    .line 341
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setPassImage(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setPassImage this"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZwdListAdapter"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mProgBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    .line 349
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz p1, :cond_1

    .line 352
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 353
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 99
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->getItem(I)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 118
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0b009f

    .line 119
    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 122
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->getItem(I)Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    move-result-object p1

    if-nez p1, :cond_1

    return-object p2

    :cond_1
    const p3, 0x7f0902e9

    .line 128
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    if-eqz p3, :cond_2

    .line 130
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const p3, 0x7f09024d

    .line 134
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    const/4 v1, 0x1

    if-eqz p3, :cond_3

    .line 136
    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0123

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 137
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->arrive_time:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v3, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const p3, 0x7f0902e5

    .line 140
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    if-eqz p3, :cond_4

    .line 142
    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0151

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->start_time:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v3, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const p3, 0x7f0901ab

    .line 146
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const/16 v2, 0x8

    if-eqz p3, :cond_6

    .line 148
    iget v3, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    if-eq v3, v1, :cond_5

    .line 149
    invoke-virtual {p3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 151
    :cond_5
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    :goto_0
    const p3, 0x7f09030a

    .line 155
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    if-eqz p3, :cond_8

    .line 157
    iget v3, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    if-eq v3, v1, :cond_7

    .line 158
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 161
    :cond_7
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0157

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_1
    const p3, 0x7f090066

    .line 166
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/Button;

    if-eqz p3, :cond_9

    .line 168
    invoke-virtual {p3, p1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 169
    new-instance v1, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$1;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    invoke-virtual {p3, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    const p3, 0x7f09008c

    .line 177
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/CheckBox;

    if-eqz p3, :cond_b

    .line 179
    iget-boolean v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->isSelected:Z

    invoke-virtual {p3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 180
    invoke-virtual {p3, p1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 181
    iget-boolean p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mSelectMode:Z

    if-eqz p1, :cond_a

    goto :goto_2

    :cond_a
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {p3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 182
    new-instance p1, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$2;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$2;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    invoke-virtual {p3, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_b
    return-object p2
.end method

.method public isSelectMode()Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mSelectMode:Z

    return v0
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;",
            ">;)V"
        }
    .end annotation

    .line 92
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    .line 93
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setSelectMode(Z)V
    .locals 0

    .line 83
    iput-boolean p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mSelectMode:Z

    .line 84
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setZWDType(Ljava/lang/String;)V
    .locals 3

    .line 68
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mType:Ljava/lang/String;

    .line 69
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mDTO:Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    const/4 v0, 0x0

    const-string v1, ""

    if-eqz p1, :cond_0

    .line 70
    iput-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    .line 71
    iput v0, p1, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mData:Ljava/util/Vector;

    if-eqz p1, :cond_1

    .line 74
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    .line 75
    iput-object v1, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    .line 76
    iput v0, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public showPassCodeDlg()V
    .locals 4

    const-string v0, "ZwdListAdapter"

    const-string v1, "showPassCodeDlg"

    .line 252
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "activity is finished"

    .line 255
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    if-nez v1, :cond_5

    .line 260
    new-instance v1, Landroid/support/v7/app/AppCompatDialog;

    iget-object v2, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0003

    invoke-direct {v1, v2, v3}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 261
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    const v2, 0x7f0e00bd

    .line 263
    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0b0076

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    .line 268
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0901a6

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    .line 269
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0901a5

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassEdt:Landroid/widget/EditText;

    .line 270
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0901ac

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mProgBar:Landroid/widget/ProgressBar;

    .line 272
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassImage:Landroid/widget/ImageView;

    new-instance v2, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$3;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$3;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassEdt:Landroid/widget/EditText;

    const/4 v2, 0x0

    sget-object v3, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v2, v3}, Landroid/text/method/QwertyKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/QwertyKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 287
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0902f6

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    const v2, 0x7f0d020a

    .line 289
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 291
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f0902ae

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 293
    new-instance v2, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$4;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$4;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v2, 0x7f090261

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 307
    new-instance v2, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$5;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$5;-><init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 322
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 325
    :cond_5
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassEdt:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 328
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 329
    invoke-direct {p0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->requestPassCode()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 331
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 332
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
