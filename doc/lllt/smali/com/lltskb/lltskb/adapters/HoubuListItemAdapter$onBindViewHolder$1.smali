.class public final Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1;
.super Ljava/lang/Object;
.source "HoubuListItemAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->onBindViewHolder(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$ViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1",
        "Landroid/view/View$OnClickListener;",
        "onClick",
        "",
        "v",
        "Landroid/view/View;",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 42
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1;->this$0:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 45
    :goto_0
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$onBindViewHolder$1;->this$0:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->getLister()Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;->onItemDelete(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)V

    :cond_1
    return-void
.end method
