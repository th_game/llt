.class Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;
.super Landroid/os/AsyncTask;
.source "ZwdListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/adapters/ZwdListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestPasscodeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private adapterWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/adapters/ZwdListAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;)V
    .locals 1

    .line 549
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 550
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .line 570
    :try_start_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;-><init>(I)V

    .line 571
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->getImage()Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 547
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 559
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 560
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;->adapterWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-eqz v0, :cond_0

    .line 562
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->access$1000(Lcom/lltskb/lltskb/adapters/ZwdListAdapter;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 547
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter$RequestPasscodeTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 554
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
