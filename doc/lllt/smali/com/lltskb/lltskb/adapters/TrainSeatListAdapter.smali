.class public Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;
.super Landroid/widget/BaseAdapter;
.source "TrainSeatListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;,
        Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mListener:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;

    return-void
.end method

.method private setupView(Landroid/view/View;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V
    .locals 8

    if-eqz p1, :cond_9

    if-nez p2, :cond_0

    goto/16 :goto_5

    :cond_0
    const v0, 0x7f0902d8

    .line 102
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    iget-object v1, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902c0

    .line 105
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 106
    iget-object v1, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902da

    .line 109
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    .line 110
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 112
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0600dc

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 113
    iget-object v1, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/16 v3, 0x65e0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eq v2, v3, :cond_2

    const/16 v3, 0x6709

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "\u6709"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const-string v2, "\u65e0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, -0x1

    :goto_1
    const v2, 0x7f0d013c

    if-eqz v1, :cond_5

    const v3, 0x7f060071

    if-eq v1, v6, :cond_4

    .line 147
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0d013d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v7, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v2, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 140
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v7, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v2, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 115
    :cond_5
    iget v1, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    const v3, 0x7f06006b

    if-nez v1, :cond_6

    .line 116
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 119
    :cond_6
    iget v1, p2, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    const v2, 0x7f0d0197

    if-ne v1, v4, :cond_7

    .line 120
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 124
    :cond_7
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f06009c

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 127
    new-instance v1, Lcom/lltskb/lltskb/adapters/-$$Lambda$TrainSeatListAdapter$poAlmq6_w05xeIV85f_hzqWacsw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/-$$Lambda$TrainSeatListAdapter$poAlmq6_w05xeIV85f_hzqWacsw;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f080120

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    const/4 v6, 0x0

    :goto_3
    const v0, 0x7f090047

    .line 154
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const/high16 v0, 0x41800000    # 16.0f

    if-eqz v6, :cond_8

    const v1, 0x7f0d0075

    .line 156
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(I)V

    .line 157
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00b8

    invoke-virtual {p1, v1, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    const v1, 0x7f08011f

    .line 158
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 159
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_4

    :cond_8
    const v1, 0x7f0d01cd

    .line 161
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(I)V

    .line 162
    iget-object v1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00bb

    invoke-virtual {p1, v1, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    const v1, 0x7f080122

    .line 163
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 164
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextSize(F)V

    .line 166
    :goto_4
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 167
    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    :goto_5
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mData:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 60
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mData:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 89
    iget-object p2, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0b0080

    const/4 v1, 0x0

    .line 90
    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 92
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    .line 93
    invoke-direct {p0, p2, p1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->setupView(Landroid/view/View;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V

    return-object p2
.end method

.method public synthetic lambda$setupView$0$TrainSeatListAdapter(Landroid/view/View;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mListener:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;->onHoubu(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mListener:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;

    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;->onButtonClicked(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V

    :cond_0
    return-void
.end method

.method public setData(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;",
            ">;)V"
        }
    .end annotation

    .line 50
    iput-object p1, p0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->mData:Ljava/util/Vector;

    .line 51
    invoke-virtual {p0}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->notifyDataSetChanged()V

    return-void
.end method
