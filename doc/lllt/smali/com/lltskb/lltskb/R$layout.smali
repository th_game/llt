.class public final Lcom/lltskb/lltskb/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0b0000

.field public static final abc_action_bar_up_container:I = 0x7f0b0001

.field public static final abc_action_menu_item_layout:I = 0x7f0b0002

.field public static final abc_action_menu_layout:I = 0x7f0b0003

.field public static final abc_action_mode_bar:I = 0x7f0b0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0b0005

.field public static final abc_activity_chooser_view:I = 0x7f0b0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0b0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0b0008

.field public static final abc_alert_dialog_material:I = 0x7f0b0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0b000a

.field public static final abc_dialog_title_material:I = 0x7f0b000b

.field public static final abc_expanded_menu_layout:I = 0x7f0b000c

.field public static final abc_list_menu_item_checkbox:I = 0x7f0b000d

.field public static final abc_list_menu_item_icon:I = 0x7f0b000e

.field public static final abc_list_menu_item_layout:I = 0x7f0b000f

.field public static final abc_list_menu_item_radio:I = 0x7f0b0010

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0b0011

.field public static final abc_popup_menu_item_layout:I = 0x7f0b0012

.field public static final abc_screen_content_include:I = 0x7f0b0013

.field public static final abc_screen_simple:I = 0x7f0b0014

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0b0015

.field public static final abc_screen_toolbar:I = 0x7f0b0016

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0b0017

.field public static final abc_search_view:I = 0x7f0b0018

.field public static final abc_select_dialog_material:I = 0x7f0b0019

.field public static final abc_tooltip:I = 0x7f0b001a

.field public static final about:I = 0x7f0b001b

.field public static final about_app:I = 0x7f0b001c

.field public static final ad:I = 0x7f0b001d

.field public static final alert_ok:I = 0x7f0b001e

.field public static final alert_yesno:I = 0x7f0b001f

.field public static final all_life:I = 0x7f0b0020

.field public static final bankselector:I = 0x7f0b0021

.field public static final baoxian_item:I = 0x7f0b0022

.field public static final baoxian_list:I = 0x7f0b0023

.field public static final baoxian_navi:I = 0x7f0b0024

.field public static final big_screen:I = 0x7f0b0025

.field public static final big_screen_selection:I = 0x7f0b0026

.field public static final calendar_titlebar:I = 0x7f0b0027

.field public static final choose_seats:I = 0x7f0b0028

.field public static final choose_seats_item:I = 0x7f0b0029

.field public static final complete_order:I = 0x7f0b002a

.field public static final completeorder_list:I = 0x7f0b002b

.field public static final completeorder_listitem:I = 0x7f0b002c

.field public static final date_time_picker:I = 0x7f0b002d

.field public static final day_navi:I = 0x7f0b002e

.field public static final design_bottom_navigation_item:I = 0x7f0b002f

.field public static final design_bottom_sheet_dialog:I = 0x7f0b0030

.field public static final design_layout_snackbar:I = 0x7f0b0031

.field public static final design_layout_snackbar_include:I = 0x7f0b0032

.field public static final design_layout_tab_icon:I = 0x7f0b0033

.field public static final design_layout_tab_text:I = 0x7f0b0034

.field public static final design_menu_item_action_area:I = 0x7f0b0035

.field public static final design_navigation_item:I = 0x7f0b0036

.field public static final design_navigation_item_header:I = 0x7f0b0037

.field public static final design_navigation_item_separator:I = 0x7f0b0038

.field public static final design_navigation_item_subheader:I = 0x7f0b0039

.field public static final design_navigation_menu:I = 0x7f0b003a

.field public static final design_navigation_menu_item:I = 0x7f0b003b

.field public static final design_text_input_password_icon:I = 0x7f0b003c

.field public static final direction:I = 0x7f0b003d

.field public static final draw_baoxain:I = 0x7f0b003e

.field public static final edit_user:I = 0x7f0b003f

.field public static final feed_back:I = 0x7f0b0040

.field public static final flight_item:I = 0x7f0b0041

.field public static final flight_item_n:I = 0x7f0b0042

.field public static final font_dialog:I = 0x7f0b0043

.field public static final houbu_deadline_item:I = 0x7f0b0044

.field public static final houbu_item_footer:I = 0x7f0b0045

.field public static final houbu_item_header:I = 0x7f0b0046

.field public static final houbu_nocomplete_order:I = 0x7f0b0047

.field public static final houbu_order:I = 0x7f0b0048

.field public static final houbu_order_item:I = 0x7f0b0049

.field public static final houbu_people_item:I = 0x7f0b004a

.field public static final houbu_processed_order:I = 0x7f0b004b

.field public static final houbu_tickets_confirm:I = 0x7f0b004c

.field public static final houbu_train_item:I = 0x7f0b004d

.field public static final houbu_waiting_order:I = 0x7f0b004e

.field public static final include_toolbar:I = 0x7f0b004f

.field public static final item_station:I = 0x7f0b0050

.field public static final login:I = 0x7f0b0051

.field public static final main_lltskb:I = 0x7f0b0052

.field public static final main_lltskb_header:I = 0x7f0b0053

.field public static final main_tab_ad:I = 0x7f0b0054

.field public static final main_tab_cc:I = 0x7f0b0055

.field public static final main_tab_cz:I = 0x7f0b0056

.field public static final main_tab_order:I = 0x7f0b0057

.field public static final main_tab_settings:I = 0x7f0b0058

.field public static final main_tab_zz:I = 0x7f0b0059

.field public static final material_dialog:I = 0x7f0b005a

.field public static final monitor_info_item:I = 0x7f0b005b

.field public static final monitor_task:I = 0x7f0b005c

.field public static final more_tickets:I = 0x7f0b005d

.field public static final more_tickets_item:I = 0x7f0b005e

.field public static final navbar:I = 0x7f0b005f

.field public static final nocomplete_listitem:I = 0x7f0b0060

.field public static final nocomplete_order:I = 0x7f0b0061

.field public static final notification_action:I = 0x7f0b0062

.field public static final notification_action_tombstone:I = 0x7f0b0063

.field public static final notification_media_action:I = 0x7f0b0064

.field public static final notification_media_cancel_action:I = 0x7f0b0065

.field public static final notification_template_big_media:I = 0x7f0b0066

.field public static final notification_template_big_media_custom:I = 0x7f0b0067

.field public static final notification_template_big_media_narrow:I = 0x7f0b0068

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0b0069

.field public static final notification_template_custom_big:I = 0x7f0b006a

.field public static final notification_template_icon_group:I = 0x7f0b006b

.field public static final notification_template_lines_media:I = 0x7f0b006c

.field public static final notification_template_media:I = 0x7f0b006d

.field public static final notification_template_media_custom:I = 0x7f0b006e

.field public static final notification_template_part_chronometer:I = 0x7f0b006f

.field public static final notification_template_part_time:I = 0x7f0b0070

.field public static final order_bottom:I = 0x7f0b0071

.field public static final order_list_view:I = 0x7f0b0072

.field public static final order_ticket:I = 0x7f0b0073

.field public static final order_train_info:I = 0x7f0b0074

.field public static final pass_code_dialog:I = 0x7f0b0075

.field public static final passcodedlg:I = 0x7f0b0076

.field public static final passenger_select_item:I = 0x7f0b0077

.field public static final passengers:I = 0x7f0b0078

.field public static final progress:I = 0x7f0b0079

.field public static final query_result:I = 0x7f0b007a

.field public static final rand_code_dlg:I = 0x7f0b007b

.field public static final regist_user:I = 0x7f0b007c

.field public static final result_bottombar:I = 0x7f0b007d

.field public static final result_item:I = 0x7f0b007e

.field public static final run_chart:I = 0x7f0b007f

.field public static final seat_details:I = 0x7f0b0080

.field public static final select_dialog_item_material:I = 0x7f0b0081

.field public static final select_dialog_multichoice_material:I = 0x7f0b0082

.field public static final select_dialog_singlechoice_material:I = 0x7f0b0083

.field public static final select_passenger:I = 0x7f0b0084

.field public static final select_train:I = 0x7f0b0085

.field public static final show_ticket_listitem:I = 0x7f0b0086

.field public static final single_select_dialog:I = 0x7f0b0087

.field public static final soft_details:I = 0x7f0b0088

.field public static final soft_list_item:I = 0x7f0b0089

.field public static final splash:I = 0x7f0b008a

.field public static final station_list_item:I = 0x7f0b008b

.field public static final station_select:I = 0x7f0b008c

.field public static final stationinfolistitem:I = 0x7f0b008d

.field public static final stopstation:I = 0x7f0b008e

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0b008f

.field public static final ticketlist:I = 0x7f0b0090

.field public static final ticketlistitem:I = 0x7f0b0091

.field public static final train_details:I = 0x7f0b0092

.field public static final train_select_item:I = 0x7f0b0093

.field public static final traininfolistitem:I = 0x7f0b0094

.field public static final traintypes:I = 0x7f0b0095

.field public static final update_dialog:I = 0x7f0b0096

.field public static final user_heyan:I = 0x7f0b0097

.field public static final user_itemlist:I = 0x7f0b0098

.field public static final user_profile:I = 0x7f0b0099

.field public static final web_browser:I = 0x7f0b009a

.field public static final widget_footer:I = 0x7f0b009b

.field public static final widget_header:I = 0x7f0b009c

.field public static final widget_listview_container:I = 0x7f0b009d

.field public static final widget_scrollview_container:I = 0x7f0b009e

.field public static final zwd_listitem:I = 0x7f0b009f

.field public static final zwd_query:I = 0x7f0b00a0


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
