.class Lcom/lltskb/lltskb/MainLLTSkb$10;
.super Ljava/lang/Object;
.source "MainLLTSkb.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/IUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/MainLLTSkb;->doUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/MainLLTSkb;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 1001
    iput-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$MainLLTSkb$10(Landroid/view/View;)V
    .locals 0

    .line 1017
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$2$MainLLTSkb$10()V
    .locals 1

    .line 1026
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1300(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/engine/UpdateMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/UpdateMgr;->updateProgram()Z

    return-void
.end method

.method public synthetic lambda$null$4$MainLLTSkb$10()V
    .locals 1

    .line 1035
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1300(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/engine/UpdateMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/UpdateMgr;->updateData()Z

    return-void
.end method

.method public synthetic lambda$null$6$MainLLTSkb$10(Landroid/view/View;)V
    .locals 0

    .line 1046
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$8$MainLLTSkb$10(Landroid/view/View;)V
    .locals 0

    .line 1068
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$9$MainLLTSkb$10(Landroid/view/View;)V
    .locals 0

    .line 1104
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$onCheckResult$1$MainLLTSkb$10()V
    .locals 3

    .line 1012
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1200(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1013
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1015
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 1016
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 1017
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$g9Hs9sZCkFBSjbord14_rXN8jIY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$g9Hs9sZCkFBSjbord14_rXN8jIY;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$onCheckResult$3$MainLLTSkb$10(Ljava/lang/String;)V
    .locals 4

    .line 1023
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1200(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1024
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1025
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const v2, 0x7f0d01da

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1026
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$W-UGub4JZYxtIIPOM2L8dBdI4SE;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$W-UGub4JZYxtIIPOM2L8dBdI4SE;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {v2, v0, p1, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->showSelectDlg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$onCheckResult$5$MainLLTSkb$10(Ljava/lang/String;)V
    .locals 4

    .line 1032
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1200(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1033
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1034
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const v2, 0x7f0d01d7

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1035
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$bGUaUAGJ-6_vfCgwd2Js__XDiXY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$bGUaUAGJ-6_vfCgwd2Js__XDiXY;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {v2, v0, p1, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->showSelectDlg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$onCheckResult$7$MainLLTSkb$10()V
    .locals 3

    .line 1042
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1043
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 1044
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 1045
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 1046
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$JNix6TRD7g9v7hes6IbdGRyCAes;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$JNix6TRD7g9v7hes6IbdGRyCAes;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$onDownload$10$MainLLTSkb$10(ILjava/lang/String;II)V
    .locals 3

    const/4 v0, -0x1

    const v1, 0x7f0d02c6

    const/16 v2, 0x10

    if-eq p1, v0, :cond_7

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    if-eq p1, v0, :cond_0

    goto/16 :goto_2

    .line 1072
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 1074
    :cond_1
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1076
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string p4, "MainLLTSkb"

    invoke-static {p4, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setProgress(I)V

    .line 1079
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const p4, 0x7f0d02cb

    invoke-virtual {p3, p4}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 1080
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_2
    if-ne p4, v0, :cond_3

    .line 1084
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    sget-object p3, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    const-string p4, "lltskb.apk"

    invoke-static {p1, p3, p4}, Lcom/lltskb/lltskb/utils/LLTUtils;->installApk(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1086
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const p3, 0x7f090313

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_4

    .line 1088
    iget-object p4, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p4}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1000(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/ZZQueryTabView;

    move-result-object p4

    if-eqz p4, :cond_4

    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1000(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/ZZQueryTabView;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_4
    if-eqz p1, :cond_5

    .line 1091
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1094
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1100(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1095
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    invoke-virtual {p1, p3, p4}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastUpdate(J)V

    .line 1096
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/LltSettings;->setHasNewVersion(Z)V

    .line 1097
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$1200(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1100
    :goto_2
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    if-nez p1, :cond_6

    return-void

    .line 1101
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 1102
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 1103
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 1104
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$lVSaU1-_J3k72ScxiZd8t8RVS24;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$lVSaU1-_J3k72ScxiZd8t8RVS24;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 1064
    :cond_7
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    if-nez p1, :cond_8

    return-void

    .line 1065
    :cond_8
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 1066
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 1067
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 1068
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object p1

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$bDtK5c1XE0LidHXGvnWAyuQqC3g;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$bDtK5c1XE0LidHXGvnWAyuQqC3g;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    return-void
.end method

.method public onCheckResult(ILjava/lang/String;)Z
    .locals 5

    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p1, v0, :cond_6

    if-eqz p1, :cond_4

    if-eq p1, v2, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    return v2

    .line 1030
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/LltSettings;->setHasNewVersion(Z)V

    .line 1031
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance v0, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$AnKtg0OyDPncL2s_cONLMJsBgGw;

    invoke-direct {v0, p0, p2}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$AnKtg0OyDPncL2s_cONLMJsBgGw;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1037
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$800(Lcom/lltskb/lltskb/MainLLTSkb;)I

    move-result p1

    if-ne p1, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 1021
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/LltSettings;->setHasNewVersion(Z)V

    .line 1022
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance v0, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$7lG26_r5eFnSza67OPbtURkyjI4;

    invoke-direct {v0, p0, p2}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$7lG26_r5eFnSza67OPbtURkyjI4;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1028
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$800(Lcom/lltskb/lltskb/MainLLTSkb;)I

    move-result p1

    if-ne p1, v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    .line 1007
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastUpdate(J)V

    .line 1008
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setHasNewVersion(Z)V

    .line 1009
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$700(Lcom/lltskb/lltskb/MainLLTSkb;)Z

    move-result p1

    if-nez p1, :cond_5

    return v1

    .line 1011
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$9N4jvufP2TMZLXbPkkU5_EiMbic;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$9N4jvufP2TMZLXbPkkU5_EiMbic;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/MainLLTSkb;->runOnUiThread(Ljava/lang/Runnable;)V

    return v2

    .line 1039
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$700(Lcom/lltskb/lltskb/MainLLTSkb;)Z

    move-result p1

    if-nez p1, :cond_7

    return v1

    .line 1041
    :cond_7
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$HGkc2Z1k31idYUm8CdcKnuH3i60;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$HGkc2Z1k31idYUm8CdcKnuH3i60;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/MainLLTSkb;->runOnUiThread(Ljava/lang/Runnable;)V

    return v2
.end method

.method public onDownload(IIILjava/lang/String;)Z
    .locals 8

    .line 1058
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$10;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    new-instance v7, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$4X-B4ek6zA_fY8NLRCkcWylyNrs;

    move-object v1, v7

    move-object v2, p0

    move v3, p2

    move-object v4, p4

    move v5, p3

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$10$4X-B4ek6zA_fY8NLRCkcWylyNrs;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$10;ILjava/lang/String;II)V

    invoke-virtual {v0, v7}, Lcom/lltskb/lltskb/MainLLTSkb;->runOnUiThread(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method
