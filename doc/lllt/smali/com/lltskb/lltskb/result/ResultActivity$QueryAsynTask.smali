.class Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;
.super Landroid/os/AsyncTask;
.source "ResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/result/ResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryAsynTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/lltskb/lltskb/engine/ResultItem;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private QiYe:Ljava/lang/String;

.field private queryType:I

.field private querycc:Lcom/lltskb/lltskb/engine/QueryCC;

.field private stationName:Ljava/lang/String;

.field private trainName:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/result/ResultActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/result/ResultActivity;)V
    .locals 1

    .line 1163
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-string v0, ""

    .line 1159
    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->QiYe:Ljava/lang/String;

    .line 1164
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/lltskb/lltskb/engine/ResultItem;)Ljava/lang/Object;
    .locals 12

    .line 1215
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/result/ResultActivity;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const-string v2, ""

    .line 1221
    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->QiYe:Ljava/lang/String;

    const/4 v3, 0x0

    .line 1222
    aget-object p1, p1, v3

    if-nez p1, :cond_1

    return-object v1

    :cond_1
    const/4 v4, 0x1

    .line 1226
    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    .line 1227
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1228
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1229
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result v7

    if-nez v7, :cond_2

    .line 1230
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResultMgr;->pushResult()V

    .line 1234
    :cond_2
    iget-object v7, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    const-string v8, "-"

    invoke-virtual {v7, v8, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    .line 1235
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v2

    .line 1236
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v7

    .line 1238
    iput v4, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->queryType:I

    .line 1241
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    .line 1242
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x30

    const-string v10, " \u5171"

    const/4 v11, 0x2

    if-lt v8, v9, :cond_3

    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x39

    if-le v8, v9, :cond_4

    .line 1243
    :cond_3
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x41

    if-lt v8, v9, :cond_8

    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x5a

    if-gt v8, v9, :cond_8

    .line 1244
    :cond_4
    new-instance v8, Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-direct {v8, v7, v2}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    iput-object v8, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    .line 1245
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    iget-object v7, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    const/16 v2, 0x5b

    .line 1246
    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_5

    .line 1248
    invoke-virtual {v5, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1249
    :cond_5
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v3

    invoke-virtual {v2, v6, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 1250
    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1251
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/QueryCC;->getLX()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v4

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "\u7ad9"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1253
    iget-object v6, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getQiYe()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->QiYe:Ljava/lang/String;

    .line 1254
    iget v6, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-nez v6, :cond_6

    .line 1255
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->stationName:Ljava/lang/String;

    goto :goto_0

    .line 1256
    :cond_6
    iget v4, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-ne v4, v11, :cond_7

    const/16 v4, 0xe

    .line 1257
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->stationName:Ljava/lang/String;

    .line 1260
    :cond_7
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;

    move-result-object p1

    .line 1261
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v4

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {p1, v5, v0}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->getJlb(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    goto :goto_1

    .line 1264
    :cond_8
    iput v11, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->queryType:I

    .line 1265
    new-instance p1, Lcom/lltskb/lltskb/engine/QueryCZ;

    invoke-direct {p1, v7, v2}, Lcom/lltskb/lltskb/engine/QueryCZ;-><init>(ZZ)V

    .line 1266
    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->setDate(Ljava/lang/String;)V

    .line 1267
    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/QueryCZ;->query(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1270
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v4

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " \u8d9f\u8f66"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1271
    iput-object v5, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->stationName:Ljava/lang/String;

    .line 1274
    :goto_1
    iput-object v5, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->trainName:Ljava/lang/String;

    .line 1275
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 1276
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 1277
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setPos(Ljava/lang/Object;)V

    .line 1278
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitleFmt(Ljava/lang/String;)V

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1155
    check-cast p1, [Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->doInBackground([Lcom/lltskb/lltskb/engine/ResultItem;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .line 1186
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1187
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/result/ResultActivity;

    if-nez p1, :cond_0

    return-void

    .line 1192
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 1193
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1194
    iget v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->queryType:I

    const-string v2, "query_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1195
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->trainName:Ljava/lang/String;

    const-string v2, "train_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1196
    iget-object v1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "ticket_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "query_result_cc_fuzzy"

    .line 1197
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1198
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->QiYe:Ljava/lang/String;

    const-string v2, "query_result_qiye"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1199
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    if-eqz v1, :cond_1

    .line 1200
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->stationName:Ljava/lang/String;

    const-string v2, "run_chart_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1201
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getRuleIndex()I

    move-result v1

    const-string v2, "run_chart_runindex"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1202
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getMinDate()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run_chart_mindate"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1203
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->querycc:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getMaxDate()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run_chart_maxdate"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1205
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->stationName:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1208
    :goto_0
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 1168
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1169
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/result/ResultActivity;

    if-nez v0, :cond_0

    return-void

    .line 1174
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultActivity;->cancelQueryFlight()V

    const/4 v1, -0x1

    .line 1175
    new-instance v2, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask$1;-><init>(Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;Lcom/lltskb/lltskb/result/ResultActivity;)V

    const-string v3, "\u6b63\u5728\u67e5\u8be2\u4e2d..."

    invoke-static {v0, v3, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
