.class public Lcom/lltskb/lltskb/result/ViewShowResult;
.super Lcom/lltskb/lltskb/result/BaseResultActivity;
.source "ViewShowResult.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;


# static fields
.field public static final MENU_BACK_ID:I = 0x1

.field public static final MENU_SMS_ID:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ViewShowResult"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

.field private mFlightTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mIsFuzzy:Z

.field private mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mRunChart:Lcom/lltskb/lltskb/view/RunChartTopLayout;

.field private mTimeView:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 394
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;-><init>()V

    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mIsFuzzy:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/result/ViewShowResult;)Lcom/lltskb/lltskb/result/ResultListAdapter;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/result/ViewShowResult;)Ljava/lang/String;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getMessage()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private checkToobarButtons()V
    .locals 2

    const-string v0, "ViewShowResult"

    const-string v1, "checkToolbarButtons"

    .line 139
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 142
    :goto_0
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->updateToolbar(Z)V

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mTimeView:Landroid/view/View;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    .line 145
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private getMessage()Ljava/lang/String;
    .locals 5

    const-string v0, "ViewShowResult"

    const-string v1, "getMessage"

    .line 496
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 498
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mTitleView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 499
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 504
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    .line 505
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v2, 0x0

    .line 507
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 508
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz v3, :cond_3

    .line 509
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_2

    :cond_2
    const-string v4, "\n\n"

    .line 510
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResultItem;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 520
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initView()V
    .locals 20

    move-object/from16 v6, p0

    const-string v0, "ViewShowResult"

    const-string v1, "initView"

    .line 154
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x1

    .line 155
    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/result/ViewShowResult;->requestWindowFeature(I)Z

    const v1, 0x7f0b0090

    .line 156
    invoke-virtual {v6, v1}, Lcom/lltskb/lltskb/result/ViewShowResult;->setContentView(I)V

    .line 158
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getFilterType()I

    move-result v1

    iput v1, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mFilter:I

    .line 160
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getResult()Ljava/util/List;

    move-result-object v1

    iput-object v1, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mResult:Ljava/util/List;

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getIntent()Landroid/content/Intent;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 164
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "query_type"

    .line 166
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    const-string v0, "ticket_start_station"

    .line 167
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mStart:Ljava/lang/String;

    const-string v0, "ticket_arrive_station"

    .line 168
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mEnd:Ljava/lang/String;

    const-string v0, "ticket_date"

    .line 169
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    const-string v0, "train_name"

    .line 170
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTrain:Ljava/lang/String;

    const-string v0, "query_result_cc_fuzzy"

    .line 171
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mIsFuzzy:Z

    const-string v0, "result_can_sort"

    .line 172
    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mCanSort:Z

    goto :goto_0

    :cond_0
    const-string v1, "bundle is null"

    .line 175
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "intent is null"

    .line 178
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    iget v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    if-eqz v0, :cond_2

    const/16 v0, 0xff

    .line 182
    iput v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mFilter:I

    :cond_2
    const v0, 0x7f090059

    .line 185
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 187
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Ev_f4_2ZCC3Je07bW_nqfjx30tU;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Ev_f4_2ZCC3Je07bW_nqfjx30tU;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const v0, 0x7f090173

    .line 192
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 194
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, v7}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 195
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 196
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, v7}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    .line 197
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, v6}, Lcom/lltskb/lltskb/view/widget/XListView;->setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V

    .line 198
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    const v0, 0x7f09015f

    .line 200
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTimeView:Landroid/view/View;

    const v0, 0x7f0901c2

    .line 201
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTitleView:Landroid/widget/TextView;

    .line 202
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setSelected(Z)V

    .line 204
    new-instance v10, Lcom/lltskb/lltskb/result/ResultListAdapter;

    iget-object v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mStart:Ljava/lang/String;

    iget-object v3, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mEnd:Ljava/lang/String;

    iget-object v4, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    iget v5, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    move-object v0, v10

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/lltskb/lltskb/result/ResultListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v10, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    .line 205
    iget v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mFilter:I

    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->setFilter(I)V

    .line 206
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 208
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 209
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, v6}, Lcom/lltskb/lltskb/view/widget/XListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 210
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setChoiceMode(I)V

    const v0, 0x7f0901c1

    .line 213
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    if-eqz v0, :cond_7

    .line 215
    iget v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    if-ne v2, v7, :cond_6

    iget-boolean v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mIsFuzzy:Z

    if-nez v2, :cond_6

    .line 216
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v2, ""

    if-eqz v8, :cond_5

    .line 224
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v4, "run_chart_station"

    .line 226
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "run_chart_mindate"

    .line 227
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "run_chart_maxdate"

    .line 228
    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, "run_chart_runindex"

    .line 229
    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    const-string v11, "query_result_qiye"

    .line 231
    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 232
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v11

    const v12, 0x7f0d012d

    if-nez v11, :cond_4

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 234
    sget-object v11, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v12, v7, [Ljava/lang/Object;

    aput-object v3, v12, v9

    invoke-static {v11, v2, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 236
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 237
    sget-object v11, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v12, v7, [Ljava/lang/Object;

    aput-object v2, v12, v9

    invoke-static {v11, v3, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    :goto_1
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setClickable(Z)V

    .line 240
    new-instance v2, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$mHfP7d80OreX0QXFqmnAZW8t5IQ;

    invoke-direct {v2, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$mHfP7d80OreX0QXFqmnAZW8t5IQ;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v15, v4

    move-object/from16 v16, v5

    move-object/from16 v17, v8

    move/from16 v18, v10

    goto :goto_2

    :cond_5
    move-object v15, v2

    move-object/from16 v16, v15

    move-object/from16 v17, v16

    const/16 v18, 0x0

    :goto_2
    const v0, 0x7f090165

    .line 245
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 246
    new-instance v0, Lcom/lltskb/lltskb/view/RunChartTopLayout;

    iget-object v13, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    iget-object v14, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTrain:Ljava/lang/String;

    iget-object v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mResult:Ljava/util/List;

    move-object v11, v0

    move-object/from16 v19, v2

    invoke-direct/range {v11 .. v19}, Lcom/lltskb/lltskb/view/RunChartTopLayout;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mRunChart:Lcom/lltskb/lltskb/view/RunChartTopLayout;

    goto :goto_3

    .line 248
    :cond_6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    :goto_3
    const v0, 0x7f0900d5

    .line 252
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 254
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 255
    iget v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    if-ne v2, v7, :cond_8

    iget-boolean v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mIsFuzzy:Z

    if-nez v2, :cond_8

    const/16 v2, 0x8

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_9
    const v0, 0x7f0900f5

    .line 259
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 261
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 262
    iget v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    if-ne v2, v7, :cond_a

    iget-boolean v2, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mIsFuzzy:Z

    if-nez v2, :cond_a

    goto :goto_5

    :cond_a
    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_b
    const v0, 0x7f090077

    .line 265
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTicket:Landroid/widget/Button;

    .line 267
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mTicket:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$3Mwcka3VXY5Bp-lfh-xklXdAfU8;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$3Mwcka3VXY5Bp-lfh-xklXdAfU8;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006c

    .line 269
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetFilter:Landroid/widget/Button;

    const v0, 0x7f09006e

    .line 270
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetSort:Landroid/widget/Button;

    .line 272
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetSort:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$xMcs9POxBnfPuZ2nKJ8Min0D-KA;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$xMcs9POxBnfPuZ2nKJ8Min0D-KA;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006d

    .line 274
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetMid:Landroid/widget/Button;

    .line 275
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetMid:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$PuS4NVLPSnfXR-lB-uN1LY3KnjM;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$PuS4NVLPSnfXR-lB-uN1LY3KnjM;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetFilter:Landroid/widget/Button;

    if-eqz v0, :cond_c

    .line 278
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSetFilter:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Nz8T-fZc_RFYbgITKIi-pqP0XuM;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Nz8T-fZc_RFYbgITKIi-pqP0XuM;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_c
    const v0, 0x7f090078

    .line 281
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mZwd:Landroid/widget/Button;

    .line 282
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mZwd:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Mo_6yF67qyoKd8gH-xti2DOvEGs;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$Mo_6yF67qyoKd8gH-xti2DOvEGs;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090047

    .line 284
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mBook:Landroid/widget/Button;

    .line 285
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mBook:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$CkR0UDQbTZkrLH9cwvRFEagz0eM;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$CkR0UDQbTZkrLH9cwvRFEagz0eM;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006f

    .line 287
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 288
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$dScCWQt1T_CtPt3HnJpt_tWqTC4;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$dScCWQt1T_CtPt3HnJpt_tWqTC4;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090044

    .line 290
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 291
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$N3ca2EimT-oKfAteeA0LeIjvzmY;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$N3ca2EimT-oKfAteeA0LeIjvzmY;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901e1

    .line 293
    invoke-virtual {v6, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    .line 294
    iget-object v0, v6, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$XZu0vnLrHECZ5SrLXTnsl1A-Ipk;

    invoke-direct {v1, v6}, Lcom/lltskb/lltskb/result/-$$Lambda$ViewShowResult$XZu0vnLrHECZ5SrLXTnsl1A-Ipk;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->initDirection()V

    .line 306
    invoke-direct/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->checkToobarButtons()V

    .line 313
    invoke-direct/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->requestAd()V

    return-void
.end method

.method private isCZQuery()Z
    .locals 2

    .line 377
    iget v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private onBack()V
    .locals 4

    const-string v0, "ViewShowResult"

    const-string v1, "onBack"

    .line 552
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setSelectMode(Z)V

    .line 555
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->checkToobarButtons()V

    .line 556
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    .line 557
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    if-nez v2, :cond_0

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 558
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 559
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/ResultItem;

    if-nez v3, :cond_1

    goto :goto_1

    .line 561
    :cond_1
    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 563
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    return-void

    .line 566
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 567
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->finish()V

    return-void
.end method

.method private onSetSort()V
    .locals 7

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\u51fa\u53d1\u65f6\u95f4"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "\u5230\u8fbe\u65f6\u95f4"

    aput-object v3, v0, v2

    const/4 v3, 0x2

    const-string v4, "\u901f\u5ea6\u4f18\u5148"

    aput-object v4, v0, v3

    const/4 v4, 0x3

    const-string v5, "\u8ddd\u79bb\u4f18\u5148"

    aput-object v5, v0, v4

    .line 610
    sget v5, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    if-ne v5, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 612
    :cond_0
    sget v5, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v6, 0x20

    if-ne v5, v6, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    .line 614
    :cond_1
    sget v3, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v5, 0x10

    if-ne v3, v5, :cond_2

    const/4 v1, 0x3

    .line 618
    :cond_2
    :goto_0
    new-instance v3, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/result/ViewShowResult$4;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/result/ViewShowResult$4;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    .line 619
    invoke-virtual {v3, v0, v1, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 635
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 636
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "\u9009\u62e9\u6392\u5e8f\u65b9\u6cd5"

    .line 637
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 638
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 639
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    return-void
.end method

.method private onShare()V
    .locals 2

    const-string v0, "ViewShowResult"

    const-string v1, "onShare"

    .line 424
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 426
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-nez v0, :cond_0

    return-void

    .line 428
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setSelectMode(Z)V

    .line 430
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->checkToobarButtons()V

    return-void

    .line 434
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/result/ViewShowResult$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ViewShowResult$2;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    return-void
.end method

.method private queryFlight()V
    .locals 5

    const-string v0, "ViewShowResult"

    const-string v1, "queryFlight"

    .line 95
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    if-eqz v0, :cond_0

    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-nez v0, :cond_1

    return-void

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 108
    new-instance v0, Lcom/lltskb/lltskb/result/ViewShowResult$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ViewShowResult$1;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mFlightTask:Landroid/os/AsyncTask;

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mFlightTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mStart:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mEnd:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private requestAd()V
    .locals 0

    return-void
.end method

.method private sendSMS_bk()V
    .locals 4

    .line 455
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isSelectMode()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setSelectMode(Z)V

    .line 457
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void

    .line 460
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 462
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xa0

    if-le v2, v3, :cond_1

    .line 463
    new-instance v2, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v3, "\u8b66\u544a"

    .line 465
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    const-string v3, "\u4fe1\u606f\u592a\u957f\uff0c\u53ef\u80fd\u4f1a\u4ee5\u5f69\u4fe1\u65b9\u5f0f\u53d1\u9001\uff0c\u7ee7\u7eed\u5417?"

    .line 466
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 467
    new-instance v3, Lcom/lltskb/lltskb/result/ViewShowResult$3;

    invoke-direct {v3, p0, v0}, Lcom/lltskb/lltskb/result/ViewShowResult$3;-><init>(Lcom/lltskb/lltskb/result/ViewShowResult;Ljava/lang/String;)V

    const-string v0, "\u662f"

    invoke-virtual {v2, v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    const/4 v0, 0x0

    const-string v3, "\u5426"

    .line 481
    invoke-virtual {v2, v3, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 482
    invoke-virtual {v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 483
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto :goto_0

    .line 484
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 485
    new-instance v1, Landroid/content/Intent;

    const-string v2, "smsto:"

    .line 486
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "android.intent.action.SENDTO"

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "sms_body"

    .line 487
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, -0x1

    .line 488
    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->startActivityForResult(Landroid/content/Intent;I)V

    .line 490
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onBack()V

    return-void
.end method


# virtual methods
.method cancelQueryFlight()V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mFlightTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    const-string v0, "ViewShowResult"

    const-string v1, "cancelQueryFlight"

    .line 86
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mFlightTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public synthetic lambda$initView$0$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 188
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onJlb()V

    return-void
.end method

.method public synthetic lambda$initView$1$ViewShowResult(Landroid/view/View;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mTrain:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->showQiyePopMenu(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$10$ViewShowResult(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 295
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    if-eqz p1, :cond_2

    .line 296
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 297
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 298
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 299
    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    :cond_2
    :goto_1
    return-void
.end method

.method public synthetic lambda$initView$2$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 267
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onTicket()V

    return-void
.end method

.method public synthetic lambda$initView$3$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 272
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onSetSort()V

    return-void
.end method

.method public synthetic lambda$initView$4$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 275
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onSetMid()V

    return-void
.end method

.method public synthetic lambda$initView$5$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 278
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onSetFilter()V

    return-void
.end method

.method public synthetic lambda$initView$6$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 282
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onZwd()V

    return-void
.end method

.method public synthetic lambda$initView$7$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 285
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onBook()V

    return-void
.end method

.method public synthetic lambda$initView$8$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 288
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onShare()V

    return-void
.end method

.method public synthetic lambda$initView$9$ViewShowResult(Landroid/view/View;)V
    .locals 0

    .line 291
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onBack()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 525
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 526
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 538
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "ViewShowResult"

    const-string v1, "onCreate"

    .line 66
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x3

    .line 69
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ViewShowResult;->requestWindowFeature(I)Z

    .line 71
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->initView()V

    .line 76
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->queryFlight()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const-string v2, "\u5206\u4eab\u7ed3\u679c"

    .line 401
    invoke-interface {p1, v0, v1, v0, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v1, 0x1

    const-string v2, "\u8fd4\u56de"

    .line 402
    invoke-interface {p1, v0, v1, v0, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 403
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const-string p1, "ViewShowResult"

    const-string p2, "onItemLongClick"

    .line 573
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setSelectMode(Z)V

    .line 575
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 576
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->checkToobarButtons()V

    return p2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 545
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onBack()V

    const/4 p1, 0x1

    return p1

    .line 548
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLoadMore()V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 408
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 414
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onShare()V

    goto :goto_0

    .line 411
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->onBackPressed()V

    .line 417
    :goto_0
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onRefresh()V
    .locals 2

    .line 644
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->stopRefresh()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "ViewShowResult"

    const-string v1, "onRestoreInstanceState"

    .line 585
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "selectmode"

    .line 586
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setSelectMode(Z)V

    .line 589
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectAll:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    .line 593
    :cond_0
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "ViewShowResult"

    const-string v1, "onSaveInstanceState"

    .line 601
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "selectmode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 603
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected setFilter(I)V
    .locals 7

    .line 322
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mResult:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 323
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->isZZQuery()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->isCZQuery()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mCanSort:Z

    if-eqz v1, :cond_2

    .line 327
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultItem;->updateHouCheTime()V

    .line 328
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sort exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ViewShowResult"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_2
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 335
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    const/4 v4, 0x1

    .line 336
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 337
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 338
    invoke-static {p1, v5}, Lcom/lltskb/lltskb/utils/LLTUtils;->isMatchFilter(ILcom/lltskb/lltskb/engine/ResultItem;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mSelectedDirection:I

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/LLTUtils;->isSameDirection(ILcom/lltskb/lltskb/engine/ResultItem;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 339
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 343
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v3, :cond_5

    const p1, 0x7f0d010f

    const v0, 0x7f0d01eb

    const/4 v1, 0x0

    .line 344
    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 348
    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 349
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 350
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v4

    if-ne v4, v3, :cond_6

    .line 351
    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 352
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 357
    :cond_6
    iput-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    .line 359
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    if-eqz v0, :cond_7

    .line 360
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mDisplayItems:Ljava/util/List;

    iget v2, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mQueryType:I

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setResultList(Ljava/util/List;I)V

    .line 361
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mAdapter:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    .line 363
    :cond_7
    iput p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mFilter:I

    .line 365
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mTitleView:Landroid/widget/TextView;

    if-nez p1, :cond_8

    return-void

    .line 368
    :cond_8
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ViewShowResult;->getResultTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    :cond_9
    :goto_3
    return-void
.end method
