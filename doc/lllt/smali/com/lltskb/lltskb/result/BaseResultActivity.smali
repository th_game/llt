.class public abstract Lcom/lltskb/lltskb/result/BaseResultActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "BaseResultActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BaseResultActivity"


# instance fields
.field protected mBook:Landroid/widget/Button;

.field protected mCanSort:Z

.field protected mDate:Ljava/lang/String;

.field protected mDirection:Landroid/widget/Button;

.field protected mDisplayItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mEnd:Ljava/lang/String;

.field protected mFilter:I

.field protected mIsFuzzy:Z

.field protected mQueryType:I

.field protected mResult:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mSelectAll:Landroid/widget/CheckBox;

.field protected mSelectedDirection:I

.field protected mSetFilter:Landroid/widget/Button;

.field protected mSetMid:Landroid/widget/Button;

.field protected mSetSort:Landroid/widget/Button;

.field protected mStart:Ljava/lang/String;

.field protected mTicket:Landroid/widget/Button;

.field protected mTrain:Ljava/lang/String;

.field protected mTrainInfoView:Landroid/widget/TextView;

.field protected mZwd:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    const/4 v1, 0x1

    .line 56
    iput-boolean v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mCanSort:Z

    .line 57
    iput-boolean v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mIsFuzzy:Z

    .line 59
    iput v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    const/16 v0, 0xff

    .line 66
    iput v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mFilter:I

    return-void
.end method


# virtual methods
.method public btn_flight(Landroid/view/View;)V
    .locals 2

    .line 357
    iget-object p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_hotel(Landroid/view/View;)V
    .locals 1

    .line 361
    iget-object p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method abstract cancelQueryFlight()V
.end method

.method protected feedbackQiye(ILjava/lang/String;)V
    .locals 8

    const-string v0, "BaseResultActivity"

    const-string v1, "feedbackQiye"

    .line 119
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 121
    invoke-static {}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->newInstance()Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    move-result-object v1

    .line 123
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d012c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 124
    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-static {v3, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 126
    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0d012b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v6

    invoke-static {v3, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 127
    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->setContent(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->setSubject(Ljava/lang/String;)V

    const p2, 0x7f010018

    const v2, 0x7f010019

    .line 130
    invoke-virtual {v0, p2, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 131
    const-class p2, Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 136
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method protected getResultTitle()Ljava/lang/String;
    .locals 7

    .line 417
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->getTitleFmt()Ljava/lang/String;

    move-result-object v0

    .line 418
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDisplayItems:Ljava/util/List;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    :goto_0
    if-gez v1, :cond_1

    const/4 v1, 0x0

    .line 420
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->isZZQuery()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 421
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v4, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 422
    :cond_2
    iget v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    .line 423
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    const v6, 0x7f0d0153

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v0, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 425
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDirection()V
    .locals 2

    const v0, 0x7f09004e

    .line 140
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDirection:Landroid/widget/Button;

    .line 141
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDirection:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$TqIMdRz21W0rhanZEx2hAv2IQQ4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$TqIMdRz21W0rhanZEx2hAv2IQQ4;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901c3

    .line 143
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 144
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$kU2g_FwxCGXfXTxV-gY4NEv6wXI;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$kU2g_FwxCGXfXTxV-gY4NEv6wXI;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method protected isZZQuery()Z
    .locals 1

    .line 409
    iget v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$initDirection$0$BaseResultActivity(Landroid/view/View;)V
    .locals 0

    .line 141
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onDirection()V

    return-void
.end method

.method public synthetic lambda$initDirection$1$BaseResultActivity(Landroid/widget/RadioGroup;I)V
    .locals 0

    const p1, 0x7f0901b3

    if-ne p2, p1, :cond_0

    const/4 p1, 0x0

    .line 146
    iput p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    goto :goto_0

    :cond_0
    const p1, 0x7f0901b6

    if-ne p2, p1, :cond_1

    const/4 p1, 0x1

    .line 148
    iput p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    goto :goto_0

    :cond_1
    const p1, 0x7f0901b4

    if-ne p2, p1, :cond_2

    const/4 p1, 0x2

    .line 150
    iput p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    goto :goto_0

    :cond_2
    const p1, 0x7f0901b5

    if-ne p2, p1, :cond_3

    const/4 p1, 0x3

    .line 152
    iput p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    .line 154
    :cond_3
    :goto_0
    iget p1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mFilter:I

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->setFilter(I)V

    return-void
.end method

.method public synthetic lambda$null$3$BaseResultActivity(Ljava/lang/String;)V
    .locals 2

    .line 375
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d01af

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p1

    .line 376
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->setDialogFullscreen(Landroid/app/Dialog;)V

    return-void
.end method

.method public synthetic lambda$onJlb$4$BaseResultActivity()V
    .locals 3

    .line 373
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->buildJlbMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$YvcTGi_OSNZ0tXJOWOl4z6ZAHeE;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$YvcTGi_OSNZ0tXJOWOl4z6ZAHeE;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$onSetFilter$2$BaseResultActivity(ILjava/lang/String;)V
    .locals 0

    .line 366
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->setFilter(I)V

    return-void
.end method

.method public synthetic lambda$onSetMid$5$BaseResultActivity(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .line 396
    new-instance p1, Landroid/content/Intent;

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isClassicStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/lltskb/lltskb/result/ResultActivity;

    goto :goto_0

    :cond_0
    const-class v0, Lcom/lltskb/lltskb/result/ViewShowResult;

    :goto_0
    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x0

    const-string v1, "query_type"

    .line 397
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "result_can_sort"

    .line 398
    invoke-virtual {p1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p5, "ticket_start_station"

    .line 399
    invoke-virtual {p1, p5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "ticket_arrive_station"

    .line 400
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "ticket_date"

    .line 401
    invoke-virtual {p1, p2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected onBook()V
    .locals 3

    const-string v0, "BaseResultActivity"

    const-string v1, "onBook"

    .line 346
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->cancelQueryFlight()V

    .line 349
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    const-string v2, "order_from_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    const-string v2, "order_to_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "order_depart_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected onDirection()V
    .locals 3

    const v0, 0x7f09012d

    .line 159
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 161
    iput v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    const/16 v1, 0x8

    .line 162
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->updateDirection()V

    .line 165
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method protected onJlb()V
    .locals 2

    .line 372
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$BVpyD5-BDwGcCnhnPhaiTzYm8x0;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$BVpyD5-BDwGcCnhnPhaiTzYm8x0;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onSetFilter()V
    .locals 3

    .line 366
    new-instance v0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    iget v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mFilter:I

    new-instance v2, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$HYnJzW_866MyD77_N1Qwo_bNtZQ;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$HYnJzW_866MyD77_N1Qwo_bNtZQ;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;-><init>(Landroid/content/Context;ILcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;)V

    .line 368
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->show()V

    return-void
.end method

.method protected onSetMid()V
    .locals 11

    const-string v0, "BaseResultActivity"

    const-string v1, "onSetMid"

    .line 385
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    .line 387
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const-string v1, "-"

    const-string v2, ""

    .line 390
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 391
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v9

    .line 392
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v8

    .line 393
    new-instance v0, Lcom/lltskb/lltskb/action/MidQueryHelper;

    iget-object v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    new-instance v10, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$3-gOyzzWzIfreyv3FGkTWyqzCLY;

    invoke-direct {v10, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$BaseResultActivity$3-gOyzzWzIfreyv3FGkTWyqzCLY;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;)V

    move-object v3, v0

    move-object v4, p0

    invoke-direct/range {v3 .. v10}, Lcom/lltskb/lltskb/action/MidQueryHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/lltskb/lltskb/action/MidQueryHelper$Listener;)V

    .line 405
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->searchMidStation()V

    return-void
.end method

.method protected onTicket()V
    .locals 3

    const-string v0, "BaseResultActivity"

    const-string v1, "onTicket"

    .line 277
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->cancelQueryFlight()V

    .line 279
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query_type"

    const-string v2, "query_type_ticket"

    .line 280
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const-class v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 282
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected onZwd()V
    .locals 3

    const-string v0, "BaseResultActivity"

    const-string v1, "onZwd"

    .line 310
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->cancelQueryFlight()V

    .line 313
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrain:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->getTrainName4ZWD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 316
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "train_name"

    .line 317
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    const-class v0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 319
    invoke-static {p0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected prepareIntent()Landroid/content/Intent;
    .locals 6

    const-string v0, "BaseResultActivity"

    const-string v1, "prepareIntent"

    .line 289
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 291
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    const/16 v2, 0x2d

    .line 292
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_0

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    const/4 v5, 0x6

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    const/16 v3, 0x8

    .line 294
    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 296
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "ticket_date"

    .line 298
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mStart:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mEnd:Ljava/lang/String;

    const-string v2, "ticket_arrive_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ticket_type"

    const-string v2, "\u5168\u90e8"

    .line 301
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_normal"

    .line 302
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    :goto_0
    return-object v0
.end method

.method protected abstract setFilter(I)V
.end method

.method showQiyePopMenu(Landroid/view/View;Ljava/lang/String;)V
    .locals 4

    .line 69
    new-instance v0, Landroid/support/v7/widget/PopupMenu;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 70
    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0x7f0d01c5

    .line 72
    invoke-interface {p1, v1, v2, v1, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 74
    new-instance p1, Lcom/lltskb/lltskb/result/BaseResultActivity$1;

    invoke-direct {p1, p0, p2}, Lcom/lltskb/lltskb/result/BaseResultActivity$1;-><init>(Lcom/lltskb/lltskb/result/BaseResultActivity;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/support/v7/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 83
    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->show()V

    return-void
.end method

.method protected updateDirection()V
    .locals 3

    const v0, 0x7f0901c3

    .line 170
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 171
    iget v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f0901b5

    .line 182
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_1
    const v1, 0x7f0901b4

    .line 179
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0901b6

    .line 176
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_3
    const v1, 0x7f0901b3

    .line 173
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    :goto_0
    const v0, 0x7f090278

    .line 186
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    .line 187
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected updateToolbar(Z)V
    .locals 6

    const-string v0, "BaseResultActivity"

    const-string v1, "updateToolbar"

    .line 193
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mZwd:Landroid/widget/Button;

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 195
    iget v4, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-ne v4, v1, :cond_1

    if-nez p1, :cond_1

    iget-boolean v4, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mIsFuzzy:Z

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    const v0, 0x7f090059

    .line 198
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 200
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/ResultMgr;->getJlb()Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    move-result-object v4

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-ne v4, v1, :cond_3

    if-nez p1, :cond_3

    iget-boolean v4, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mIsFuzzy:Z

    if-nez v4, :cond_3

    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    const/16 v4, 0x8

    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 204
    :cond_4
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->updateTrainInfo()V

    .line 206
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_6

    if-eqz p1, :cond_5

    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    const/4 v4, 0x4

    .line 207
    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    if-nez p1, :cond_6

    .line 209
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 213
    :cond_6
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDirection:Landroid/widget/Button;

    const/4 v4, 0x2

    if-eqz v0, :cond_8

    .line 214
    iget v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-ne v5, v4, :cond_7

    .line 215
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 216
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->updateDirection()V

    goto :goto_4

    .line 218
    :cond_7
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 219
    iput v3, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSelectedDirection:I

    .line 223
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mBook:Landroid/widget/Button;

    if-eqz v0, :cond_a

    .line 224
    iget v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-ne v5, v1, :cond_9

    if-nez p1, :cond_9

    .line 225
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    .line 227
    :cond_9
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mBook:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 231
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetFilter:Landroid/widget/Button;

    if-eqz v0, :cond_d

    .line 232
    iget v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-eqz v0, :cond_b

    if-ne v0, v4, :cond_c

    :cond_b
    if-nez p1, :cond_c

    .line 234
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetFilter:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_6

    .line 236
    :cond_c
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetFilter:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 240
    :cond_d
    :goto_6
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetSort:Landroid/widget/Button;

    if-eqz v0, :cond_f

    .line 241
    iget v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-nez v5, :cond_e

    if-nez p1, :cond_e

    iget-boolean v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mCanSort:Z

    if-eqz v5, :cond_e

    .line 242
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_7

    .line 244
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetSort:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 248
    :cond_f
    :goto_7
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetMid:Landroid/widget/Button;

    if-eqz v0, :cond_11

    .line 249
    iget v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-nez v5, :cond_10

    if-nez p1, :cond_10

    .line 250
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_8

    .line 252
    :cond_10
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mSetMid:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 256
    :cond_11
    :goto_8
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTicket:Landroid/widget/Button;

    if-eqz v0, :cond_13

    .line 257
    iget v5, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-nez v5, :cond_12

    if-nez p1, :cond_12

    const/4 p1, 0x0

    goto :goto_9

    :cond_12
    const/16 p1, 0x8

    :goto_9
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_13
    const p1, 0x7f090044

    .line 261
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    if-eqz p1, :cond_16

    .line 263
    iget v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    if-eq v0, v1, :cond_15

    if-ne v0, v4, :cond_14

    goto :goto_a

    .line 267
    :cond_14
    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_b

    .line 265
    :cond_15
    :goto_a
    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_16
    :goto_b
    return-void
.end method

.method protected updateTrainInfo()V
    .locals 5

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const v0, 0x7f0902fc

    .line 88
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/BaseResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    return-void

    .line 95
    :cond_1
    iget v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mQueryType:I

    const/16 v2, 0x8

    const/4 v3, 0x1

    if-eq v1, v3, :cond_2

    .line 96
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 101
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;

    move-result-object v0

    if-nez v0, :cond_3

    return-void

    .line 105
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrain:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->getFoodCoach(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;

    move-result-object v0

    if-nez v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 111
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0132

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->coach:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v1, p0, Lcom/lltskb/lltskb/result/BaseResultActivity;->mTrainInfoView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
