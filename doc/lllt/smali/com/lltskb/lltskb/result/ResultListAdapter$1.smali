.class Lcom/lltskb/lltskb/result/ResultListAdapter$1;
.super Landroid/os/AsyncTask;
.source "ResultListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/result/ResultListAdapter;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$item:Lcom/lltskb/lltskb/engine/ResultItem;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/result/ResultListAdapter;Landroid/content/Context;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    iput-object p2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 219
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .line 237
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result p1

    .line 238
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v0

    .line 240
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$context:Landroid/content/Context;

    const-class v3, Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    .line 243
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v2}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$100(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ticket_date"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v1

    .line 246
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v2

    .line 249
    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v3}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$100(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "-"

    const-string v5, ""

    .line 250
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    .line 254
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x30

    if-lt v6, v7, :cond_0

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x39

    if-le v6, v7, :cond_1

    .line 255
    :cond_0
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x41

    if-lt v6, v7, :cond_2

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x5a

    if-gt v6, v7, :cond_2

    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    .line 258
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    const/4 v7, 0x2

    const-string v9, "query_type"

    const-string v10, " \u5171"

    if-eqz v6, :cond_6

    .line 260
    new-instance v6, Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-direct {v6, p1, v0}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    .line 261
    invoke-virtual {v6, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    const/16 p1, 0x5b

    .line 262
    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    if-ltz p1, :cond_3

    .line 264
    invoke-virtual {v1, v5, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 265
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result p1

    invoke-virtual {v6, v2, p1}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p1

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getLX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "\u7ad9"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    const-string v3, "train_name"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getQiYe()Ljava/lang/String;

    move-result-object v3

    const-string v5, "query_result_qiye"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;

    move-result-object v2

    .line 273
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v3

    iget-object v5, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v5}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$100(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v1, v5}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->getJlb(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    .line 275
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$200(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;

    move-result-object v1

    .line 276
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v2}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$300(Lcom/lltskb/lltskb/result/ResultListAdapter;)I

    move-result v2

    if-nez v2, :cond_4

    .line 277
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 278
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {v2}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$300(Lcom/lltskb/lltskb/result/ResultListAdapter;)I

    move-result v2

    if-ne v2, v7, :cond_5

    .line 279
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$item:Lcom/lltskb/lltskb/engine/ResultItem;

    const/16 v2, 0xe

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 281
    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    const-string v3, "run_chart_station"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getRuleIndex()I

    move-result v2

    const-string v3, "run_chart_runindex"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 283
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getMinDate()Ljava/lang/String;

    move-result-object v2

    const-string v3, "run_chart_mindate"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getMaxDate()Ljava/lang/String;

    move-result-object v2

    const-string v3, "run_chart_maxdate"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 287
    :cond_6
    new-instance v2, Lcom/lltskb/lltskb/engine/QueryCZ;

    invoke-direct {v2, p1, v0}, Lcom/lltskb/lltskb/engine/QueryCZ;-><init>(ZZ)V

    .line 288
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/QueryCZ;->setDate(Ljava/lang/String;)V

    .line 289
    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->query(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " \u8d9f\u8f66"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 294
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    const-string v3, "ticket_start_station"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    :goto_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 301
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 302
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1, v8}, Lcom/lltskb/lltskb/engine/ResultMgr;->setPos(Ljava/lang/Object;)V

    return-object v8
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 219
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1

    .line 228
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 229
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 230
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {p1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$000(Lcom/lltskb/lltskb/result/ResultListAdapter;)Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    if-eqz p1, :cond_0

    .line 231
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->this$0:Lcom/lltskb/lltskb/result/ResultListAdapter;

    invoke-static {p1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->access$000(Lcom/lltskb/lltskb/result/ResultListAdapter;)Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->mIntent:Landroid/content/Intent;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 223
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter$1;->val$context:Landroid/content/Context;

    const-string v1, "\u67e5\u8be2\u4e2d"

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 224
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
