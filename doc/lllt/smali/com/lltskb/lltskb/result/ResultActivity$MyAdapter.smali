.class Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;
.super Landroid/widget/BaseAdapter;
.source "ResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/result/ResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyAdapter"
.end annotation


# instance fields
.field id_row_layout:I

.field mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/lltskb/lltskb/result/ResultActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/content/Context;I)V
    .locals 0

    .line 1084
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    .line 1085
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1086
    iput p3, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->id_row_layout:I

    .line 1087
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method clearSelect()V
    .locals 3

    .line 1092
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v2, 0x0

    .line 1094
    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    .line 1101
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1102
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 1107
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 1108
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 1118
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResultItem;

    if-nez p2, :cond_0

    .line 1120
    iget-object p3, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    monitor-enter p3

    .line 1121
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->id_row_layout:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1122
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {v0, p2, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->access$800(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 1123
    monitor-exit p3

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 1126
    :cond_0
    :goto_0
    iget-object p3, p0, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {p3, p2, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->access$900(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    if-eqz p1, :cond_1

    .line 1131
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p1

    if-eqz p1, :cond_1

    const p1, 0x7f080129

    .line 1132
    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_1
    const p1, 0x7f080128

    .line 1134
    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    return-object p2
.end method
