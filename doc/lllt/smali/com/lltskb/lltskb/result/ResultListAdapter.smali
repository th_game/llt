.class Lcom/lltskb/lltskb/result/ResultListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ResultListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ResultListAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDate:Ljava/lang/String;

.field private mEnd:Ljava/lang/String;

.field private mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

.field private mLastClick:J

.field private mQuertType:I

.field private mQueryTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResultList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectMode:Z

.field private mStart:Ljava/lang/String;

.field private mType:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .line 41
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    const-wide/16 v0, 0x0

    .line 182
    iput-wide v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mLastClick:J

    .line 42
    iput-object p4, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mDate:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mStart:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mEnd:Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    .line 46
    iput p5, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mQuertType:I

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/result/ResultListAdapter;)Landroid/content/Context;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mDate:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/result/ResultListAdapter;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mStart:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/result/ResultListAdapter;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mQuertType:I

    return p0
.end method

.method private getFlightView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .line 113
    instance-of v0, p1, Lcom/lltskb/lltskb/result/ResultLayout;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 116
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v1, 0x7f0b0041

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_1
    const p2, 0x7f0902f6

    .line 120
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->start:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\uff0d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->arrive:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f090274

    .line 123
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    const/16 v2, 0xa

    const/16 v3, 0x7c

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0902c1

    .line 127
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 128
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f090275

    .line 130
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0902c2

    .line 134
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 137
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object p1
.end method

.method private isShowFlight()Z
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$sort$0(Lcom/lltskb/lltskb/engine/ResultItem;Lcom/lltskb/lltskb/engine/ResultItem;)I
    .locals 1

    const/4 v0, 0x4

    .line 52
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .line 91
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isShowFlight()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 95
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    if-nez v2, :cond_1

    return v0

    .line 97
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    add-int/2addr v2, v0

    return v2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 p1, p1, 0x1

    if-gt v0, p1, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 146
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isShowFlight()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 148
    invoke-direct {p0, p2, p3}, Lcom/lltskb/lltskb/result/ResultListAdapter;->getFlightView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 153
    :cond_1
    instance-of p3, p2, Lcom/lltskb/lltskb/result/ResultLayout;

    if-nez p3, :cond_2

    const/4 p2, 0x0

    .line 157
    :cond_2
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/lltskb/lltskb/engine/ResultItem;

    if-nez p2, :cond_3

    .line 160
    new-instance p2, Lcom/lltskb/lltskb/result/ResultLayout;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mType:I

    invoke-direct {p2, v0, v1}, Lcom/lltskb/lltskb/result/ResultLayout;-><init>(Landroid/content/Context;I)V

    :cond_3
    if-nez p3, :cond_4

    return-object p2

    .line 165
    :cond_4
    check-cast p2, Lcom/lltskb/lltskb/result/ResultLayout;

    .line 166
    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/result/ResultLayout;->setItem(Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 168
    iget-boolean p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    if-eqz p3, :cond_5

    const/4 p1, 0x0

    .line 169
    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/result/ResultLayout;->showCheckBox(I)V

    goto :goto_0

    :cond_5
    const/16 p3, 0x8

    .line 171
    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/result/ResultLayout;->showCheckBox(I)V

    .line 172
    invoke-virtual {p2}, Lcom/lltskb/lltskb/result/ResultLayout;->getOrderButton()Landroid/widget/Button;

    move-result-object p3

    if-eqz p3, :cond_6

    .line 174
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 175
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    :goto_0
    return-object p2
.end method

.method isSelectMode()Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const-string v0, "ResultListAdapter"

    const-string v1, "onClick"

    .line 312
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    .line 313
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_1

    .line 318
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ltz p1, :cond_3

    .line 319
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    goto :goto_0

    .line 322
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    instance-of v2, v1, Lcom/lltskb/lltskb/result/ViewShowResult;

    if-eqz v2, :cond_2

    .line 323
    check-cast v1, Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 326
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v1, 0x0

    .line 327
    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 328
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 330
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    const-class v3, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mStart:Ljava/lang/String;

    const-string v3, "order_from_station"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mEnd:Ljava/lang/String;

    const-string v3, "order_to_station"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mDate:Ljava/lang/String;

    const-string v3, "order_depart_date"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "order_train_code"

    .line 334
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    const-string p1, "start order activity"

    .line 336
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    :goto_1
    const-string p1, "error in onClick"

    .line 314
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 185
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "onItemClick position="

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p4, "ResultListAdapter"

    invoke-static {p4, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mLastClick:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long p1, v0, v2

    if-gez p1, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    if-nez p1, :cond_0

    const-string p1, "onItemClick too quick"

    .line 187
    invoke-static {p4, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    add-int/lit8 p3, p3, -0x1

    .line 193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mLastClick:J

    .line 195
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->isShowFlight()Z

    move-result p1

    if-eqz p1, :cond_2

    if-nez p3, :cond_1

    .line 197
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/result/ViewShowResult;->btn_flight(Landroid/view/View;)V

    return-void

    :cond_1
    add-int/lit8 p3, p3, -0x1

    .line 203
    :cond_2
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/result/ResultListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResultItem;

    if-nez p1, :cond_3

    return-void

    .line 206
    :cond_3
    iget-boolean p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    const/4 p5, 0x1

    if-eqz p3, :cond_4

    .line 207
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p2

    xor-int/2addr p2, p5

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    .line 208
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    return-void

    .line 212
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    check-cast p3, Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/result/ViewShowResult;->cancelQueryFlight()V

    .line 214
    iget-object p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mQueryTask:Landroid/os/AsyncTask;

    if-eqz p3, :cond_5

    invoke-virtual {p3}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object p3

    sget-object v0, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq p3, v0, :cond_5

    const-string p1, "onItemClick,task is not finished"

    .line 215
    invoke-static {p4, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 218
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 219
    new-instance p3, Lcom/lltskb/lltskb/result/ResultListAdapter$1;

    invoke-direct {p3, p0, p2, p1}, Lcom/lltskb/lltskb/result/ResultListAdapter$1;-><init>(Lcom/lltskb/lltskb/result/ResultListAdapter;Landroid/content/Context;Lcom/lltskb/lltskb/engine/ResultItem;)V

    iput-object p3, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mQueryTask:Landroid/os/AsyncTask;

    .line 307
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mQueryTask:Landroid/os/AsyncTask;

    sget-object p2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array p3, p5, [Ljava/lang/String;

    const/4 p4, 0x0

    const-string p5, ""

    aput-object p5, p3, p4

    invoke-virtual {p1, p2, p3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    return-void
.end method

.method setResultList(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;I)V"
        }
    .end annotation

    .line 77
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    .line 78
    iput p2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mType:I

    .line 80
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method setSelectMode(Z)V
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mSelectMode:Z

    .line 73
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public sort()Z
    .locals 6

    .line 50
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mType:I

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 52
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/result/-$$Lambda$ResultListAdapter$ndoqL-OpJdAgTAOZgh5ZGZ5RXy0;->INSTANCE:Lcom/lltskb/lltskb/result/-$$Lambda$ResultListAdapter$ndoqL-OpJdAgTAOZgh5ZGZ5RXy0;

    .line 53
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x1

    const/4 v4, 0x1

    .line 54
    :goto_0
    iget-object v5, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 55
    iget-object v5, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 57
    :cond_1
    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 58
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59
    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultListAdapter;->mResultList:Ljava/util/List;

    return v3
.end method
