.class Lcom/lltskb/lltskb/result/ResultActivity$4;
.super Ljava/lang/Object;
.source "ResultActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/result/ResultActivity;->onShare()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/result/ResultActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/result/ResultActivity;)V
    .locals 0

    .line 835
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDenied()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onGranted()V
    .locals 3

    .line 838
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/result/ResultActivity;->access$500(Lcom/lltskb/lltskb/result/ResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 840
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/result/ResultActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 842
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {v1, v0, v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->startShareIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 843
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    .line 844
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setSelection(Z)V

    .line 845
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->clearSelect()V

    .line 847
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$4;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/result/ResultActivity;->access$600(Lcom/lltskb/lltskb/result/ResultActivity;)V

    return-void
.end method
