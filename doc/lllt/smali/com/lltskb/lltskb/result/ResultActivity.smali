.class public Lcom/lltskb/lltskb/result/ResultActivity;
.super Lcom/lltskb/lltskb/result/BaseResultActivity;
.source "ResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;,
        Lcom/lltskb/lltskb/result/ResultActivity$OnScrollChangedListenerImp;,
        Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;,
        Lcom/lltskb/lltskb/result/ResultActivity$ListViewAndHeadViewTouchLinstener;,
        Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;,
        Lcom/lltskb/lltskb/result/ResultActivity$HoldView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ResultActivity"


# instance fields
.field private iconSize:I

.field imgGetter:Landroid/text/Html$ImageGetter;

.field private mFlightTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mHead:Landroid/widget/RelativeLayout;

.field mHeadSrcrollView:Lcom/lltskb/lltskb/view/LLTHScrollView;

.field private final mPadding:I

.field mResultListView:Landroid/widget/ListView;

.field private mRunChart:Lcom/lltskb/lltskb/view/RunChartTopLayout;

.field mSelectMode:Z

.field mTask:Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;

.field private mTextSize:I

.field myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

.field private onTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 273
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/BaseResultActivity;-><init>()V

    const/4 v0, 0x4

    .line 80
    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mPadding:I

    const/16 v0, 0x10

    .line 81
    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    const/16 v0, 0xc

    .line 91
    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->iconSize:I

    .line 127
    new-instance v0, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$pUCz_g2KHOw4XnGesxY61tXkuXo;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$pUCz_g2KHOw4XnGesxY61tXkuXo;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->imgGetter:Landroid/text/Html$ImageGetter;

    .line 139
    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ResultActivity$1;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->onTouchListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/result/ResultActivity;)I
    .locals 0

    .line 73
    iget p0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->iconSize:I

    return p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->doQuery(Lcom/lltskb/lltskb/engine/ResultItem;)V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V
    .locals 0

    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/result/ResultActivity;[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->showFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V

    return-void
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/result/ResultActivity;)Ljava/lang/String;
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getMessage()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/result/ResultActivity;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->checkToolbarButtons()V

    return-void
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/result/ResultActivity;->initItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    return-void
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    return-void
.end method

.method private checkToolbarButtons()V
    .locals 2

    const-string v0, "ResultActivity"

    const-string v1, "checkToobarButtons"

    .line 584
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    iget-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    .line 586
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->updateToolbar(Z)V

    return-void
.end method

.method private doQuery(Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 4

    const-string v0, "ResultActivity"

    const-string v1, "doQuery"

    .line 562
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    .line 563
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0

    .line 564
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    return-void

    .line 567
    :cond_1
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 570
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTask:Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v1, v3, :cond_3

    const-string p1, "doQuery enter twice"

    .line 571
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 575
    :cond_3
    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTask:Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;

    .line 576
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTask:Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/lltskb/lltskb/engine/ResultItem;

    aput-object p1, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/lltskb/lltskb/result/ResultActivity$QueryAsynTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    :goto_0
    return-void
.end method

.method private getMessage()Ljava/lang/String;
    .locals 11

    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 804
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getResultTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v2, :cond_9

    .line 806
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    goto/16 :goto_4

    :cond_0
    const/4 v3, 0x0

    .line 809
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/ResultItem;

    if-nez v4, :cond_1

    const-string v0, ""

    return-object v0

    :cond_1
    const/4 v5, 0x1

    const/4 v6, 0x1

    .line 812
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_8

    .line 813
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz v7, :cond_7

    .line 814
    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v8

    if-nez v8, :cond_2

    goto :goto_3

    .line 815
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ":\n"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    const/4 v8, 0x0

    .line 818
    :goto_1
    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getCount()I

    move-result v9

    if-ge v8, v9, :cond_6

    .line 819
    invoke-virtual {v7, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    const-string v10, "-"

    .line 820
    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    goto :goto_2

    .line 822
    :cond_3
    invoke-virtual {v4, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_4

    goto :goto_2

    .line 824
    :cond_4
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ":"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 828
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 830
    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 807
    :cond_9
    :goto_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

# th init itemView
.method private initItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    if-eqz v1, :cond_a

    if-eqz v2, :cond_a

    .line 282
    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-nez v3, :cond_0

    goto/16 :goto_3

    .line 285
    :cond_0
    new-instance v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$1;)V

    .line 286
    iput-object v2, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->item:Lcom/lltskb/lltskb/engine/ResultItem;

    const v4, 0x7f0902a8

    .line 288
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 289
    iget v5, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextSize(F)V

    const/16 v5, 0x11

    .line 290
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v6, 0x1

    .line 291
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v7, 0x0

    .line 292
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x4

    invoke-static {v10, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v10

    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v13

    .line 293
    invoke-virtual {v4, v8, v10, v12, v13}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 296
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    const/4 v10, -0x1

    if-nez v8, :cond_1

    .line 298
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v12, -0x2

    invoke-direct {v8, v12, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 300
    :cond_1
    iput v10, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 302
    instance-of v10, v8, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v10, :cond_2

    .line 303
    move-object v10, v8

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v12

    iput v12, v10, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 306
    :cond_2
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    iget-object v10, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-eq v1, v10, :cond_3

    .line 309
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v10

    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/text/TextPaint;->setFlags(I)V

    .line 310
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 313
    :cond_3
    iput-object v4, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->name:Landroid/widget/TextView;

    .line 314
    iget-object v10, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-ne v1, v10, :cond_6

    const v10, -0xe7652a

    .line 316
    invoke-virtual {v1, v10}, Landroid/view/View;->setBackgroundColor(I)V

    .line 317
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    const/4 v10, 0x0

    const/4 v12, 0x0

    .line 319
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getCount()I

    move-result v13

    if-ge v10, v13, :cond_5

    .line 320
    invoke-virtual {v2, v10}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 322
    invoke-virtual {v4, v13}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v13

    const/high16 v14, 0x41200000    # 10.0f

    add-float/2addr v13, v14

    float-to-int v14, v13

    .line 323
    invoke-virtual {v2, v10, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setWidth(II)V

    int-to-float v12, v12

    add-float/2addr v12, v13

    float-to-int v12, v12

    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 328
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getScreenWidth(Landroid/content/Context;)I

    move-result v4

    add-int/lit8 v10, v12, 0xa

    if-le v4, v10, :cond_6

    add-int/lit8 v4, v4, -0xa

    int-to-float v4, v4

    int-to-float v10, v12

    div-float/2addr v4, v10

    const/4 v10, 0x0

    .line 331
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getCount()I

    move-result v12

    if-ge v10, v12, :cond_6

    .line 332
    invoke-virtual {v2, v10}, Lcom/lltskb/lltskb/engine/ResultItem;->getWidth(I)I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v4

    float-to-int v12, v12

    .line 334
    invoke-virtual {v2, v10, v12}, Lcom/lltskb/lltskb/engine/ResultItem;->setWidth(II)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_6
    const v4, 0x7f090002

    .line 339
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/lltskb/lltskb/view/LLTHScrollView;

    .line 340
    iput-object v10, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->scrollView:Landroid/view/ViewGroup;

    .line 341
    iget-object v12, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-eq v1, v12, :cond_7

    .line 342
    invoke-virtual {v12, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/view/LLTHScrollView;

    .line 343
    new-instance v12, Lcom/lltskb/lltskb/result/ResultActivity$OnScrollChangedListenerImp;

    invoke-direct {v12, v0, v10}, Lcom/lltskb/lltskb/result/ResultActivity$OnScrollChangedListenerImp;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/view/LLTHScrollView;)V

    invoke-virtual {v4, v12}, Lcom/lltskb/lltskb/view/LLTHScrollView;->AddOnScrollChangedListener(Lcom/lltskb/lltskb/view/LLTHScrollView$OnScrollChangedListener;)V

    :cond_7
    const v4, 0x7f09008c

    .line 346
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 347
    iput-object v4, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->chk:Landroid/widget/CheckBox;

    if-eqz v4, :cond_8

    .line 350
    new-instance v10, Lcom/lltskb/lltskb/result/ResultActivity$2;

    invoke-direct {v10, v0}, Lcom/lltskb/lltskb/result/ResultActivity$2;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 363
    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    :cond_8
    const v4, 0x7f090129

    .line 367
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 368
    iput-object v4, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->layout_content:Landroid/view/ViewGroup;

    .line 369
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v10

    const v12, 0x7f0600e0

    invoke-static {v10, v12}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v10

    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    const/4 v10, 0x1

    .line 371
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getCount()I

    move-result v12

    if-ge v10, v12, :cond_9

    .line 372
    new-instance v12, Landroid/widget/TextView;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v12, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 373
    invoke-virtual {v12, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 374
    invoke-virtual {v12, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 375
    iget v13, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    int-to-float v13, v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextSize(F)V

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v14

    .line 377
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v6

    .line 376
    invoke-virtual {v12, v13, v14, v15, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 379
    invoke-virtual {v12, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    invoke-virtual {v12, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 383
    invoke-virtual {v4, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v10, v10, 0x1

    const/4 v6, 0x1

    goto :goto_2

    :cond_9
    const v4, 0x7f090146

    .line 387
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 388
    iput-object v4, v3, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->layout_name:Landroid/view/ViewGroup;

    .line 389
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 390
    new-instance v2, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$1oKCMWocfyWcJuvd8xZ2bOOpUfs;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$1oKCMWocfyWcJuvd8xZ2bOOpUfs;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 410
    iget-object v2, v0, Lcom/lltskb/lltskb/result/ResultActivity;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_a
    :goto_3
    return-void
.end method

.method private initView()V
    .locals 2

    const v0, 0x7f09006c

    .line 872
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetFilter:Landroid/widget/Button;

    .line 873
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetFilter:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetFilter:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/ResultActivity$5;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/ResultActivity$5;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f09006e

    .line 885
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetSort:Landroid/widget/Button;

    .line 886
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetSort:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$k70tzTwUeJaJEDygpIGm3w7Tmz0;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$k70tzTwUeJaJEDygpIGm3w7Tmz0;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006d

    .line 891
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetMid:Landroid/widget/Button;

    .line 892
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSetMid:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$bo9FUOPlFrSK6Om_W56sQ960uas;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$bo9FUOPlFrSK6Om_W56sQ960uas;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901e1

    .line 897
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    .line 898
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 899
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$DYdAl6mfwmoF2cHFGDU0TxiCqXk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$DYdAl6mfwmoF2cHFGDU0TxiCqXk;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f090077

    .line 909
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTicket:Landroid/widget/Button;

    .line 910
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTicket:Landroid/widget/Button;

    if-nez v0, :cond_2

    return-void

    .line 912
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTicket:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$chj1J5fDLXCPqwuSe2zYcfd0EmU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$chj1J5fDLXCPqwuSe2zYcfd0EmU;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09004e

    .line 917
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDirection:Landroid/widget/Button;

    .line 918
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDirection:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$Pj6V7b0drG3Ir51wBim8kAdLYmA;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$Pj6V7b0drG3Ir51wBim8kAdLYmA;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090078

    .line 921
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mZwd:Landroid/widget/Button;

    .line 923
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mZwd:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$YVZ92ve-CDa6sjDa0k1hk_Zfw0g;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$YVZ92ve-CDa6sjDa0k1hk_Zfw0g;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090047

    .line 928
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mBook:Landroid/widget/Button;

    .line 930
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mBook:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$8TvW1CA42kpyJq02jZkLDKMT77w;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$8TvW1CA42kpyJq02jZkLDKMT77w;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 935
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mBook:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f09006f

    .line 937
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 938
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$U5Si7JoQBho8_rURvmJJVwPz0DM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$U5Si7JoQBho8_rURvmJJVwPz0DM;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090044

    .line 947
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez v0, :cond_3

    return-void

    .line 949
    :cond_3
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$VEyYtXiOeuKHO29snOiV21k2fA8;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$VEyYtXiOeuKHO29snOiV21k2fA8;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 954
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->initDirection()V

    .line 956
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->checkToolbarButtons()V

    .line 958
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->queryFlight()V

    return-void
.end method

.method private isTrain(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 429
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v1, 0x30

    if-lt p1, v1, :cond_1

    const/16 v1, 0x39

    if-le p1, v1, :cond_2

    :cond_1
    const/16 v1, 0x41

    if-lt p1, v1, :cond_3

    const/16 v1, 0x5a

    if-gt p1, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private onSetSort()V
    .locals 5

    .line 591
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 593
    sget v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 595
    :cond_0
    sget v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v4, 0x20

    if-ne v1, v4, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    .line 597
    :cond_1
    sget v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v3, 0x10

    if-ne v1, v3, :cond_2

    const/4 v1, 0x3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 601
    :goto_0
    new-instance v3, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$CyROJ-6IVOiP95OE5YjL60CB4AA;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$CyROJ-6IVOiP95OE5YjL60CB4AA;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    .line 602
    invoke-virtual {v3, v0, v1, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 611
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 612
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d027a

    .line 613
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 614
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 615
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    return-void
.end method

.method private onShare()V
    .locals 3

    .line 834
    iget-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-eqz v0, :cond_0

    .line 835
    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$4;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ResultActivity$4;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 859
    iput-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    .line 860
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->checkToolbarButtons()V

    .line 861
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz v0, :cond_1

    .line 862
    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    .line 866
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 867
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {p0, v0, v1}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    :cond_2
    return-void
.end method

.method private declared-synchronized queryFlight()V
    .locals 5

    monitor-enter p0

    .line 732
    :try_start_0
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 733
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "ResultActivity"

    const-string v1, "queryFlight"

    .line 735
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->cancelQueryFlight()V

    .line 743
    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/result/ResultActivity$3;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;

    .line 766
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mStart:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mEnd:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 767
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private showFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V
    .locals 9

    if-eqz p1, :cond_6

    .line 681
    :try_start_0
    array-length v0, p1

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    goto/16 :goto_0

    :cond_0
    const v0, 0x7f090133

    .line 683
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0902f6

    .line 685
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_1

    return-void

    .line 687
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    aget-object v5, p1, v4

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->start:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\uff0d"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, p1, v4

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->arrive:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0x8

    .line 688
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v2, 0x7f090274

    .line 690
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_2

    return-void

    .line 692
    :cond_2
    aget-object v5, p1, v4

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    const/16 v6, 0xa

    const/16 v7, 0x7c

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    .line 693
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0902c1

    .line 695
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_3

    return-void

    .line 697
    :cond_3
    aget-object v5, p1, v4

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f090275

    .line 699
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_4

    return-void

    :cond_4
    const/4 v5, 0x1

    .line 701
    aget-object v8, p1, v5

    iget-object v8, v8, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    invoke-virtual {v8, v7, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    .line 702
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0902c2

    .line 704
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_5

    return-void

    .line 706
    :cond_5
    aget-object p1, p1, v5

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 709
    new-instance p1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$ZPrJBcpzJP3roVRKJBSLNkQbIb0;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$ZPrJBcpzJP3roVRKJBSLNkQbIb0;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 713
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v2

    .line 714
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v3

    .line 713
    invoke-virtual {v0, p1, v2, v1, v3}, Landroid/view/View;->setPadding(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 716
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_6
    :goto_0
    return-void
.end method

# th update ItemView
.method private updateItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 4

    if-eqz p2, :cond_5

    if-eqz p1, :cond_5

    .line 414
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    goto :goto_2

    .line 416
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;

    if-nez v0, :cond_1

    return-void

    .line 420
    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 421
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    if-ne p1, v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_3

    .line 422
    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 423
    :cond_3
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-ne p1, v2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    invoke-direct {p0, v0, p2, v1}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V

    :cond_5
    :goto_2
    return-void
.end method

# th updateItemView
.method private updateItemView(Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    if-eqz v2, :cond_14

    if-eqz v1, :cond_14

    .line 434
    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v3, :cond_14

    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-lt v3, v4, :cond_14

    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    if-nez v3, :cond_0

    goto/16 :goto_8

    .line 437
    :cond_0
    iput-object v2, v1, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->item:Lcom/lltskb/lltskb/engine/ResultItem;

    .line 439
    iget-object v3, v1, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->chk:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    if-eqz v3, :cond_3

    .line 441
    iget-boolean v7, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    const/16 v8, 0x8

    if-eqz v7, :cond_1

    const/4 v7, 0x0

    goto :goto_0

    :cond_1
    const/16 v7, 0x8

    :goto_0
    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 442
    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 443
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v7

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    if-eqz p3, :cond_3

    .line 445
    iget-boolean v7, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-eqz v7, :cond_2

    const/4 v8, 0x4

    :cond_2
    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_3
    const/16 v3, 0xff

    if-eqz p3, :cond_4

    .line 449
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v7

    goto :goto_1

    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v7

    :goto_1
    if-eqz p3, :cond_5

    const v8, -0xe7652a

    goto :goto_2

    .line 450
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getBackColor()I

    move-result v8

    :goto_2
    if-nez p3, :cond_7

    .line 453
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v8

    if-ne v8, v4, :cond_6

    const v8, -0x231108

    goto :goto_3

    .line 456
    :cond_6
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    .line 460
    :cond_7
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isIsNextStation()Z

    move-result v3

    if-eqz v3, :cond_8

    const v7, -0x30cedb

    .line 462
    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_9

    move/from16 v17, v8

    move v8, v7

    move/from16 v7, v17

    .line 468
    :cond_9
    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v3, :cond_14

    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, v4, :cond_a

    goto/16 :goto_8

    .line 471
    :cond_a
    iget-object v3, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 472
    iget-object v9, v1, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->name:Landroid/widget/TextView;

    if-nez v9, :cond_b

    return-void

    .line 475
    :cond_b
    invoke-virtual {v2, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v10

    .line 476
    invoke-direct {v0, v10}, Lcom/lltskb/lltskb/result/ResultActivity;->isTrain(Ljava/lang/String;)Z

    move-result v10

    .line 477
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing()Z

    move-result v11

    const/4 v12, 0x0

    if-eqz v11, :cond_c

    if-nez p3, :cond_c

    if-eqz v10, :cond_c

    .line 478
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, " <img src=\'\u590d\'/>"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v13, v0, Lcom/lltskb/lltskb/result/ResultActivity;->imgGetter:Landroid/text/Html$ImageGetter;

    invoke-static {v11, v13, v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 480
    :cond_c
    invoke-virtual {v2, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    :goto_4
    invoke-virtual {v3, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getWidth(I)I

    move-result v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setWidth(I)V

    .line 485
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v11, 0x11

    .line 488
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 489
    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 490
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 491
    iget v13, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    int-to-float v13, v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setTextSize(F)V

    .line 505
    iget-object v9, v1, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->layout_content:Landroid/view/ViewGroup;

    if-nez v9, :cond_d

    return-void

    :cond_d
    const/4 v13, 0x1

    .line 507
    :goto_5
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getCount()I

    move-result v14

    if-ge v13, v14, :cond_13

    add-int/lit8 v14, v13, -0x1

    .line 508
    invoke-virtual {v9, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    if-ne v13, v4, :cond_10

    .line 510
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v15

    .line 511
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing()Z

    move-result v16

    if-eqz v16, :cond_e

    if-nez p3, :cond_e

    if-nez v10, :cond_e

    .line 512
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, " <img src=\'\u590d\'/> "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 514
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lcom/lltskb/lltskb/engine/ResultItem;->isIsNextStation()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 515
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "\u2190"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 517
    :cond_f
    iget-object v5, v0, Lcom/lltskb/lltskb/result/ResultActivity;->imgGetter:Landroid/text/Html$ImageGetter;

    invoke-static {v15, v5, v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 519
    :cond_10
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getIsHtml(I)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 520
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 522
    :cond_11
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    :goto_6
    invoke-virtual {v3, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getWidth(I)I

    move-result v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setWidth(I)V

    .line 527
    invoke-virtual {v14, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 528
    invoke-virtual {v14, v8}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 531
    invoke-virtual {v14, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 532
    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 533
    invoke-virtual {v14, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 534
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    .line 535
    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getIsHtml(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 536
    iget v5, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    mul-int/lit8 v5, v5, 0x3

    const/4 v15, 0x4

    div-int/2addr v5, v15

    int-to-float v5, v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 537
    invoke-virtual {v14, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_7

    :cond_12
    const/4 v15, 0x4

    .line 539
    iget v5, v0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    int-to-float v5, v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setTextSize(F)V

    :goto_7
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_5

    .line 554
    :cond_13
    iget-object v1, v1, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->layout_name:Landroid/view/ViewGroup;

    .line 555
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 556
    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    :cond_14
    :goto_8
    return-void
.end method


# virtual methods
.method declared-synchronized cancelQueryFlight()V
    .locals 2

    monitor-enter p0

    .line 724
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 725
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    const-string v0, "ResultActivity"

    const-string v1, "interrupt flight task"

    .line 726
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 727
    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFlightTask:Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 729
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public synthetic lambda$initItemView$1$ResultActivity(Landroid/view/View;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "ResultActivity"

    const-string v1, "On layout name click"

    .line 392
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 394
    iget-boolean v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    const v1, 0x7f09008c

    .line 396
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    if-eqz p1, :cond_2

    .line 398
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 401
    :cond_1
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->doQuery(Lcom/lltskb/lltskb/engine/ResultItem;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public synthetic lambda$initView$10$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onBook"

    .line 931
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onBook()V

    return-void
.end method

.method public synthetic lambda$initView$11$ResultActivity(Landroid/view/View;)V
    .locals 3

    const-string p1, "ResultActivity"

    const-string v0, "onShare"

    .line 939
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    :try_start_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onShare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 943
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShare exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$initView$12$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onBackPessed"

    .line 950
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onBackPressed()V

    return-void
.end method

.method public synthetic lambda$initView$4$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onSetSort"

    .line 887
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onSetSort()V

    return-void
.end method

.method public synthetic lambda$initView$5$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onSetMid"

    .line 893
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onSetMid()V

    return-void
.end method

.method public synthetic lambda$initView$6$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onSelectAll"

    .line 900
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-nez p1, :cond_0

    return-void

    .line 902
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->setSelection(Z)V

    .line 903
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz p1, :cond_1

    .line 904
    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$initView$7$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onTicket"

    .line 913
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onTicket()V

    return-void
.end method

.method public synthetic lambda$initView$8$ResultActivity(Landroid/view/View;)V
    .locals 0

    .line 918
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onDirection()V

    return-void
.end method

.method public synthetic lambda$initView$9$ResultActivity(Landroid/view/View;)V
    .locals 1

    const-string p1, "ResultActivity"

    const-string v0, "onZwd"

    .line 924
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onZwd()V

    return-void
.end method

.method public synthetic lambda$new$0$ResultActivity(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 129
    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Ljava/lang/String;)V

    .line 130
    new-instance p1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 131
    invoke-virtual {p1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06009c

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    invoke-virtual {p1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 133
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->iconSize:I

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v0

    const/4 v1, 0x0

    .line 134
    invoke-virtual {p1, v1, v1, v0, v0}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    return-object p1
.end method

.method public synthetic lambda$onCreate$13$ResultActivity(Landroid/view/View;)V
    .locals 0

    .line 970
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->onJlb()V

    return-void
.end method

.method public synthetic lambda$onCreate$14$ResultActivity(Landroid/view/View;)V
    .locals 1

    .line 1022
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTrain:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->showQiyePopMenu(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onSetSort$2$ResultActivity(Landroid/content/DialogInterface;I)V
    .locals 2

    const-string v0, "ResultActivity"

    const-string v1, "sort type onChoiceItems clicked"

    .line 604
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-nez p2, :cond_0

    .line 605
    sput v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    if-ne p2, v0, :cond_1

    .line 606
    sput v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_0

    :cond_1
    if-ne p2, v1, :cond_2

    const/16 p2, 0x20

    .line 607
    sput p2, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    const/16 p2, 0x10

    .line 608
    sput p2, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    .line 609
    :cond_3
    :goto_0
    iget p2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/result/ResultActivity;->setFilter(I)V

    .line 610
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public synthetic lambda$showFlight$3$ResultActivity(Landroid/view/View;)V
    .locals 2

    const-string p1, "ResultActivity"

    const-string v0, "onFlight click"

    .line 710
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mStart:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mEnd:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    const-string v0, "ResultActivity"

    const-string v1, "onBackPressed"

    .line 771
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 773
    iput-boolean v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    .line 774
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->checkToolbarButtons()V

    .line 775
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz v1, :cond_0

    .line 776
    invoke-virtual {v1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->clearSelect()V

    .line 777
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    .line 779
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 780
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {p0, v1, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    :cond_1
    return-void

    .line 784
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->cancelQueryFlight()V

    .line 786
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    .line 962
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/result/BaseResultActivity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "ResultActivity"

    const-string v0, "onCreate"

    .line 963
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 964
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b007a

    .line 965
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->setContentView(I)V

    const v0, 0x7f090059

    .line 967
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 969
    new-instance v1, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$KfVIvOYXIHyiB0Kn1rv_A9nIIso;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$KfVIvOYXIHyiB0Kn1rv_A9nIIso;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 974
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->getResult()Ljava/util/List;

    move-result-object v0

    .line 976
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 977
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 978
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 981
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    .line 983
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v2, "ticket_start_station"

    .line 985
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mStart:Ljava/lang/String;

    const-string v2, "ticket_arrive_station"

    .line 986
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mEnd:Ljava/lang/String;

    const-string v2, "ticket_date"

    .line 987
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "train_name"

    .line 988
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTrain:Ljava/lang/String;

    const-string v2, "result_can_sort"

    .line 989
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mCanSort:Z

    const-string v2, "query_type"

    .line 991
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    const-string v2, "query_result_cc_fuzzy"

    .line 992
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mIsFuzzy:Z

    .line 994
    iget v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-eqz v2, :cond_2

    const/16 v2, 0xff

    .line 995
    iput v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    .line 998
    :cond_2
    iget v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-ne v2, p1, :cond_3

    iget-boolean v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mIsFuzzy:Z

    if-nez v2, :cond_3

    const-string v2, "run_chart_station"

    .line 999
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "run_chart_mindate"

    .line 1000
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "run_chart_maxdate"

    .line 1001
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v2, "run_chart_runindex"

    .line 1002
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    const v2, 0x7f090165

    .line 1005
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1007
    new-instance v2, Lcom/lltskb/lltskb/view/RunChartTopLayout;

    iget-object v5, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDate:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTrain:Ljava/lang/String;

    iget-object v11, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    move-object v3, v2

    invoke-direct/range {v3 .. v11}, Lcom/lltskb/lltskb/view/RunChartTopLayout;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    iput-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mRunChart:Lcom/lltskb/lltskb/view/RunChartTopLayout;

    :cond_3
    const-string v2, "query_result_qiye"

    .line 1010
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0901c1

    .line 1011
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/16 v3, 0x8

    if-eqz v2, :cond_6

    .line 1014
    iget v4, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-ne v4, p1, :cond_4

    iget-boolean v4, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mIsFuzzy:Z

    if-nez v4, :cond_4

    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1015
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v4

    const v5, 0x7f0d012d

    if-eqz v4, :cond_5

    .line 1017
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, p1, [Ljava/lang/Object;

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1019
    :cond_5
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, p1, [Ljava/lang/Object;

    const-string v7, ""

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1021
    :goto_1
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 1022
    new-instance v4, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$4XwyjDD8gUu9uRHBw8wllQwavlM;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/result/-$$Lambda$ResultActivity$4XwyjDD8gUu9uRHBw8wllQwavlM;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1025
    :cond_6
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const v0, 0x7f0900d5

    .line 1026
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 1028
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    const v0, 0x7f0900f5

    .line 1030
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 1032
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1039
    :cond_8
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getFontSize()I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    .line 1040
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getFilterType()I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    .line 1041
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->setFilter(I)V

    .line 1043
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->initView()V

    const v0, 0x7f0900e8

    .line 1045
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    .line 1046
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 1047
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1048
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_9

    .line 1049
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {p0, p1, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->initItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 1050
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {p0, p1, v0}, Lcom/lltskb/lltskb/result/ResultActivity;->updateItemView(Landroid/view/View;Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 1053
    :cond_9
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    const v0, 0x7f090002

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/LLTHScrollView;

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHeadSrcrollView:Lcom/lltskb/lltskb/view/LLTHScrollView;

    .line 1055
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mHead:Landroid/widget/RelativeLayout;

    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$ListViewAndHeadViewTouchLinstener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/result/ResultActivity$ListViewAndHeadViewTouchLinstener;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$1;)V

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const p1, 0x7f090182

    .line 1057
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    .line 1058
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    new-instance v0, Lcom/lltskb/lltskb/result/ResultActivity$ListViewAndHeadViewTouchLinstener;

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/result/ResultActivity$ListViewAndHeadViewTouchLinstener;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$1;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1060
    new-instance p1, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    const v0, 0x7f0b007e

    invoke-direct {p1, p0, p0, v0}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;-><init>(Lcom/lltskb/lltskb/result/ResultActivity;Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    .line 1061
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected setFilter(I)V
    .locals 7

    .line 620
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mQueryType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/16 p1, 0xff

    .line 622
    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    .line 623
    iget p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mResult:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 627
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    goto/16 :goto_3

    .line 629
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->isZZQuery()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mCanSort:Z

    if-eqz v1, :cond_2

    .line 631
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultItem;->updateHouCheTime()V

    .line 632
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 634
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sort exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ResultActivity"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_2
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 641
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    const/4 v4, 0x1

    .line 642
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 643
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 644
    invoke-static {p1, v5}, Lcom/lltskb/lltskb/utils/LLTUtils;->isMatchFilter(ILcom/lltskb/lltskb/engine/ResultItem;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectedDirection:I

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/LLTUtils;->isSameDirection(ILcom/lltskb/lltskb/engine/ResultItem;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 645
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 649
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_5

    const p1, 0x7f0d010f

    const v0, 0x7f0d01eb

    const/4 v1, 0x0

    .line 650
    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 655
    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 656
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 657
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v4

    if-ne v4, v3, :cond_6

    .line 658
    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 659
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 665
    :cond_6
    iput-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mDisplayItems:Ljava/util/List;

    .line 666
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz v0, :cond_7

    .line 667
    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    .line 669
    :cond_7
    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mFilter:I

    const p1, 0x7f0901c2

    .line 670
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_8

    return-void

    .line 672
    :cond_8
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity;->mTextSize:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 674
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultActivity;->getResultTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    :cond_9
    :goto_3
    return-void
.end method
