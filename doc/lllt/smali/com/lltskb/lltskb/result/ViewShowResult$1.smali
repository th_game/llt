.class Lcom/lltskb/lltskb/result/ViewShowResult$1;
.super Landroid/os/AsyncTask;
.source "ViewShowResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/result/ViewShowResult;->queryFlight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/result/ViewShowResult;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/result/ViewShowResult;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ViewShowResult$1;->this$0:Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 108
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ViewShowResult$1;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .line 124
    new-instance v0, Lcom/lltskb/lltskb/engine/online/FlightQuery;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/FlightQuery;-><init>()V

    const/4 v1, 0x0

    .line 125
    aget-object v1, p1, v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    const/4 v3, 0x2

    aget-object p1, p1, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/FlightQuery;->getFlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    move-result-object p1

    .line 126
    sput-object p1, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mFlights:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 113
    :cond_0
    instance-of v0, p1, [Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, [Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    check-cast v0, [Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    array-length v1, v0

    if-lez v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult$1;->this$0:Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-static {v1}, Lcom/lltskb/lltskb/result/ViewShowResult;->access$000(Lcom/lltskb/lltskb/result/ViewShowResult;)Lcom/lltskb/lltskb/result/ResultListAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ViewShowResult$1;->this$0:Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-static {v1}, Lcom/lltskb/lltskb/result/ViewShowResult;->access$000(Lcom/lltskb/lltskb/result/ViewShowResult;)Lcom/lltskb/lltskb/result/ResultListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V

    .line 116
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ViewShowResult$1;->this$0:Lcom/lltskb/lltskb/result/ViewShowResult;

    invoke-static {v0}, Lcom/lltskb/lltskb/result/ViewShowResult;->access$000(Lcom/lltskb/lltskb/result/ViewShowResult;)Lcom/lltskb/lltskb/result/ResultListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/result/ResultListAdapter;->notifyDataSetChanged()V

    .line 119
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method
