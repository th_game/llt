.class public Lcom/lltskb/lltskb/result/ResultLayout;
.super Landroid/widget/LinearLayout;
.source "ResultLayout.java"


# instance fields
.field mBegEndTime:Landroid/widget/TextView;

.field mExtraInfo:Landroid/widget/TextView;

.field mFuxing:Landroid/widget/TextView;

.field mItem:Lcom/lltskb/lltskb/engine/ResultItem;

.field mItemChbox:Landroid/widget/CheckBox;

.field mOrderButton:Landroid/widget/Button;

.field mStation:Landroid/widget/TextView;

.field mStationClickListener:Landroid/view/View$OnClickListener;

.field mStationName:Landroid/widget/TextView;

.field mStopTime:Landroid/widget/TextView;

.field mTextViewType:Landroid/widget/TextView;

.field mTicket:Landroid/widget/TextView;

.field mTime:Landroid/widget/TextView;

.field mTrainName:Landroid/widget/TextView;

.field mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance p1, Lcom/lltskb/lltskb/result/ResultLayout$1;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/result/ResultLayout$1;-><init>(Lcom/lltskb/lltskb/result/ResultLayout;)V

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance p1, Lcom/lltskb/lltskb/result/ResultLayout$1;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/result/ResultLayout$1;-><init>(Lcom/lltskb/lltskb/result/ResultLayout;)V

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationClickListener:Landroid/view/View$OnClickListener;

    const/4 p1, 0x0

    .line 64
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultLayout;->setOrientation(I)V

    .line 65
    iput p2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mType:I

    .line 66
    invoke-direct {p0}, Lcom/lltskb/lltskb/result/ResultLayout;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance p1, Lcom/lltskb/lltskb/result/ResultLayout$1;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/result/ResultLayout$1;-><init>(Lcom/lltskb/lltskb/result/ResultLayout;)V

    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private init()V
    .locals 2

    .line 70
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mType:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008d

    invoke-static {v0, v1, p0}, Lcom/lltskb/lltskb/result/ResultLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0094

    invoke-static {v0, v1, p0}, Lcom/lltskb/lltskb/result/ResultLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/result/ResultLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0091

    invoke-static {v0, v1, p0}, Lcom/lltskb/lltskb/result/ResultLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_0
    const v0, 0x7f09008c

    .line 84
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    const/16 v1, 0x8

    .line 86
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 90
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    :cond_3
    const v0, 0x7f09000d

    .line 94
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTrainName:Landroid/widget/TextView;

    const v0, 0x7f090008

    .line 95
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStation:Landroid/widget/TextView;

    const v0, 0x7f09000c

    .line 96
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTime:Landroid/widget/TextView;

    const v0, 0x7f09000e

    .line 97
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTextViewType:Landroid/widget/TextView;

    const v0, 0x7f090009

    .line 98
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationName:Landroid/widget/TextView;

    const v0, 0x7f090004

    .line 100
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mBegEndTime:Landroid/widget/TextView;

    const v0, 0x7f09000a

    .line 101
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStopTime:Landroid/widget/TextView;

    const v0, 0x7f09000b

    .line 103
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTicket:Landroid/widget/TextView;

    const v0, 0x7f090005

    .line 104
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mExtraInfo:Landroid/widget/TextView;

    const v0, 0x7f090006

    .line 105
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mFuxing:Landroid/widget/TextView;

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItem:Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz v0, :cond_4

    .line 107
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->setItem(Lcom/lltskb/lltskb/engine/ResultItem;)V

    :cond_4
    const v0, 0x7f090062

    .line 108
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/result/ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mOrderButton:Landroid/widget/Button;

    return-void
.end method

.method private setItemCC(Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 8

    .line 194
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 195
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x39

    if-le v3, v4, :cond_2

    .line 196
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x41

    if-lt v3, v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5a

    if-gt v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v3, 0x1

    .line 198
    :goto_1
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 200
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_3

    const/16 v0, 0xe

    .line 202
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 205
    :cond_3
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isIsNextStation()Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "<font color=\"#ff0000\"><u>"

    .line 206
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u2190</u><br/><small>("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")</small></font>"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string v6, "<u>"

    .line 208
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</u><br/><small>("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")</small>"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationName:Landroid/widget/TextView;

    if-nez v0, :cond_5

    return-void

    .line 217
    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    if-nez v3, :cond_6

    .line 221
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 222
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 224
    :cond_6
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStationName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    .line 227
    :goto_3
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mExtraInfo:Landroid/widget/TextView;

    .line 229
    iget-object v4, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStopTime:Landroid/widget/TextView;

    if-nez v4, :cond_7

    return-void

    .line 233
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x3

    const-string v6, "<font color=\"#5fc534\"><b>"

    const/4 v7, 0x4

    if-eqz v3, :cond_8

    .line 235
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</b></font>&nbsp;\u59cb\u53d1<br/>"

    .line 237
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&nbsp;\u7ec8\u5230"

    .line 239
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStopTime:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_a

    .line 244
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v2, ""

    .line 245
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_8
    if-eqz v0, :cond_9

    .line 250
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v2, 0xf

    .line 251
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    :cond_9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</b></font>&nbsp;\u5230<br/>"

    .line 255
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&nbsp;\u5f00"

    .line 257
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStopTime:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTime:Landroid/widget/TextView;

    if-nez v0, :cond_b

    return-void

    :cond_b
    const/16 v2, 0xd

    if-eqz v3, :cond_c

    .line 268
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 270
    :cond_c
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<small><font color=\"#0066ff\">"

    .line 272
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x10

    .line 273
    invoke-static {v3}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "</font></small><br/>"

    .line 274
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isIsNextStation()Z

    move-result p1

    if-eqz p1, :cond_d

    const-string p1, "<small><font color=\"#ff0000\">"

    .line 276
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "</font></small>"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 278
    :cond_d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " \u516c\u91cc"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    :goto_6
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTicket:Landroid/widget/TextView;

    if-eqz p1, :cond_e

    const/16 v0, 0x8

    .line 285
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_e
    return-void
.end method

.method private setItemCZ(Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 4

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTrainName:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    .line 148
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b>"

    .line 149
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 150
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</b>"

    .line 151
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "<small>"

    .line 152
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&nbsp;("

    .line 153
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    .line 154
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    .line 155
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0xc

    .line 156
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")</small>"

    .line 157
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTrainName:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTrainName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStation:Landroid/widget/TextView;

    if-nez v2, :cond_1

    return-void

    .line 165
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    const-string v2, "<font color=\"#5fc534\"><b>"

    .line 167
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    .line 168
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</b></font>&nbsp;\u5230<br/>"

    .line 169
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    .line 170
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&nbsp;\u5f00"

    .line 171
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStation:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const/16 v2, 0xa

    .line 175
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTextViewType:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 179
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTextViewType:Landroid/widget/TextView;

    const/16 v1, 0xe

    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mBegEndTime:Landroid/widget/TextView;

    const/16 v0, 0x8

    if-eqz p1, :cond_4

    .line 184
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStopTime:Landroid/widget/TextView;

    if-eqz p1, :cond_5

    .line 188
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    return-void
.end method


# virtual methods
.method public getOrderButton()Landroid/widget/Button;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mOrderButton:Landroid/widget/Button;

    return-object v0
.end method

.method public setItem(Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 122
    :cond_0
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItem:Lcom/lltskb/lltskb/engine/ResultItem;

    .line 123
    iget v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mType:I

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 131
    :cond_1
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/result/ResultLayout;->setItemCZ(Lcom/lltskb/lltskb/engine/ResultItem;)V

    goto :goto_0

    .line 128
    :cond_2
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/result/ResultLayout;->setItemCC(Lcom/lltskb/lltskb/engine/ResultItem;)V

    goto :goto_0

    .line 125
    :cond_3
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/result/ResultLayout;->setItemZZ(Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItem:Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_4

    .line 137
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 138
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mFuxing:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 139
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing()Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x0

    goto :goto_1

    :cond_5
    const/16 p1, 0x8

    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    return-void
.end method

.method public setItemZZ(Lcom/lltskb/lltskb/engine/ResultItem;)V
    .locals 14

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b>"

    .line 293
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 294
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</b>"

    .line 295
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " <small>("

    .line 296
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    .line 297
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    .line 298
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0xc

    .line 299
    invoke-static {v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ")</small>"

    .line 300
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTrainName:Landroid/widget/TextView;

    if-nez v3, :cond_0

    return-void

    .line 307
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 310
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTextViewType:Landroid/widget/TextView;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/16 v4, 0xa

    .line 315
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    .line 319
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " <font color="

    .line 320
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\"#0000ff\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "><b>"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x3

    .line 321
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "</b></font> \u5f00"

    .line 322
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "<br/>"

    .line 323
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x2

    .line 324
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    .line 325
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x4

    .line 326
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5230"

    .line 327
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    iget-object v5, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mStation:Landroid/widget/TextView;

    if-nez v5, :cond_2

    return-void

    .line 334
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {v0, v6, v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v0

    if-ne v0, v4, :cond_3

    const v0, -0x231108

    .line 337
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_0

    :cond_3
    const v0, 0xffffff

    .line 339
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 342
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTime:Landroid/widget/TextView;

    if-nez v0, :cond_4

    return-void

    :cond_4
    const/4 v5, 0x5

    .line 347
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 349
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 350
    array-length v8, v5

    if-le v8, v4, :cond_5

    .line 351
    aget-object v8, v5, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u65f6"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v5, v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u5206"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v4, "\r\n"

    .line 353
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0xd

    .line 354
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u516c\u91cc"

    .line 355
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mTicket:Landroid/widget/TextView;

    if-nez v0, :cond_6

    return-void

    .line 374
    :cond_6
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\"#5c9b00\""

    const/4 v5, 0x6

    .line 377
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x7

    .line 378
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v7

    invoke-virtual {p1, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x9

    .line 379
    invoke-static {v8}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x8

    .line 380
    invoke-static {v9}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v9

    invoke-virtual {p1, v9}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 381
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 382
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x44

    const-string v12, " </font>"

    const-string v13, ">\u00a5"

    if-eq v10, v11, :cond_c

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x47

    if-eq v10, v11, :cond_c

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x43

    if-eq v10, v11, :cond_c

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x53

    if-ne v1, v3, :cond_7

    goto :goto_1

    .line 407
    :cond_7
    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "\u786c\u5ea7\uff1a<font color="

    .line 408
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_8
    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "\u8f6f\u5ea7\uff1a<font color="

    .line 411
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    :cond_9
    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "\u8f6f\u5367\uff1a<font color="

    .line 414
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    :cond_a
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "<br/>\u786c\u5367\uff1a<font color="

    .line 417
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    :cond_b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v6, v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 383
    :cond_c
    :goto_1
    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "\u4e00\u7b49\u5ea7: <font color="

    .line 384
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    :cond_d
    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "\u4e8c\u7b49\u5ea7: <font color="

    .line 387
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    :cond_e
    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "\u8f6f\u5367: <font color="

    .line 391
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    :cond_f
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, "\u52a8\u5367: <font color="

    .line 395
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "</font>"

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_10
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v6, v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method public showCheckBox(I)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 116
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItem:Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz p1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/lltskb/lltskb/result/ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method
