.class Lcom/lltskb/lltskb/result/ResultActivity$1;
.super Ljava/lang/Object;
.source "ResultActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/result/ResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private last_x:F

.field private last_y:F

.field private mAutoSelect:Z

.field private mDownTime:J

.field private final mDur:F

.field private mIsSelect:Z

.field final synthetic this$0:Lcom/lltskb/lltskb/result/ResultActivity;

.field private x:F

.field private y:F


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/result/ResultActivity;)V
    .locals 2

    .line 139
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 141
    iput-wide v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mDownTime:J

    const/high16 p1, 0x42c80000    # 100.0f

    .line 142
    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mDur:F

    const/4 p1, 0x0

    .line 143
    iput-boolean p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mIsSelect:Z

    .line 144
    iput-boolean p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mAutoSelect:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .line 149
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 152
    :cond_0
    iget-object v2, v0, Lcom/lltskb/lltskb/result/ResultActivity$HoldView;->item:Lcom/lltskb/lltskb/engine/ResultItem;

    .line 153
    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/result/ResultActivity;->mHeadSrcrollView:Lcom/lltskb/lltskb/view/LLTHScrollView;

    if-eqz v3, :cond_1

    .line 154
    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/result/ResultActivity;->mHeadSrcrollView:Lcom/lltskb/lltskb/view/LLTHScrollView;

    invoke-virtual {v3, p2}, Lcom/lltskb/lltskb/view/LLTHScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 158
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_2

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mDownTime:J

    .line 161
    iput-boolean v4, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mIsSelect:Z

    .line 162
    iput-boolean v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mAutoSelect:Z

    .line 166
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->x:F

    .line 167
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->y:F

    .line 168
    iget p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->x:F

    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_x:F

    .line 169
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_y:F

    .line 170
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_4

    .line 178
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v4, :cond_8

    .line 181
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    if-eqz v2, :cond_10

    .line 185
    iget-boolean p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mIsSelect:Z

    if-eqz p2, :cond_7

    .line 187
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 188
    iget-object p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-boolean p2, p2, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-eqz p2, :cond_4

    .line 189
    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    const p2, 0x7f09008c

    .line 190
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    if-eqz p1, :cond_3

    .line 192
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 194
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 196
    :cond_4
    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    .line 197
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {p1, v2}, Lcom/lltskb/lltskb/result/ResultActivity;->access$100(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/engine/ResultItem;)V

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 201
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-boolean p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->mSelectMode:Z

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    if-eqz p1, :cond_6

    .line 202
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->clearSelect()V

    .line 204
    :cond_6
    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    .line 205
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->myAdapter:Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity$MyAdapter;->notifyDataSetChanged()V

    :goto_0
    return v4

    .line 219
    :cond_7
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_4

    .line 223
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v3, 0x2

    if-ne p1, v3, :cond_d

    .line 224
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    .line 225
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    .line 226
    iget v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_x:F

    sub-float/2addr v3, p1

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 227
    iget v5, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_y:F

    sub-float/2addr v5, p2

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-eqz v3, :cond_9

    if-le v3, v5, :cond_9

    .line 234
    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/result/ResultActivity;->mResultListView:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 236
    :cond_9
    iput p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_x:F

    .line 237
    iput p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->last_y:F

    .line 239
    iget v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->x:F

    sub-float/2addr p1, v3

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float p1, p1, v3

    if-gtz p1, :cond_b

    iget p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->y:F

    sub-float/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    cmpl-float p1, p1, v3

    if-lez p1, :cond_a

    goto :goto_1

    .line 242
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iget-wide v5, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mDownTime:J

    sub-long/2addr p1, v5

    long-to-float p1, p1

    const/high16 p2, 0x42c80000    # 100.0f

    cmpl-float p1, p1, p2

    if-lez p1, :cond_c

    iget-boolean p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mAutoSelect:Z

    if-nez p1, :cond_c

    .line 243
    iput-boolean v4, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mIsSelect:Z

    .line 244
    iput-boolean v4, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mAutoSelect:Z

    .line 245
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResultItem;->isSelected()Z

    move-result p1

    xor-int/lit8 p2, p1, 0x1

    .line 246
    invoke-virtual {v2, p2}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    .line 247
    iget-object p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {p2, v0, v2, v1}, Lcom/lltskb/lltskb/result/ResultActivity;->access$200(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V

    .line 248
    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    goto :goto_2

    .line 241
    :cond_b
    :goto_1
    iput-boolean v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->mIsSelect:Z

    :cond_c
    :goto_2
    return v4

    .line 256
    :cond_d
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v3, 0x3

    if-eq p1, v3, :cond_f

    .line 257
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 p2, 0x4

    if-ne p1, p2, :cond_e

    goto :goto_3

    :cond_e
    const-string p1, "ResultActivity"

    const-string p2, "onTouch other"

    .line 264
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 258
    :cond_f
    :goto_3
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$1;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-static {p1, v0, v2, v1}, Lcom/lltskb/lltskb/result/ResultActivity;->access$200(Lcom/lltskb/lltskb/result/ResultActivity;Lcom/lltskb/lltskb/result/ResultActivity$HoldView;Lcom/lltskb/lltskb/engine/ResultItem;Z)V

    :cond_10
    :goto_4
    return v4
.end method
