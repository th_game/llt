.class Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;
.super Landroid/graphics/drawable/shapes/OvalShape;
.source "ResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/result/ResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TextOvalShape"
.end annotation


# instance fields
.field private height:I

.field private text:Ljava/lang/String;

.field private textPaint:Landroid/text/TextPaint;

.field final synthetic this$0:Lcom/lltskb/lltskb/result/ResultActivity;

.field private x:I


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/result/ResultActivity;Ljava/lang/String;)V
    .locals 3

    .line 106
    iput-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->this$0:Lcom/lltskb/lltskb/result/ResultActivity;

    invoke-direct {p0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    const/4 v0, 0x0

    .line 105
    iput v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->x:I

    .line 107
    iput-object p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->text:Ljava/lang/String;

    .line 108
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    .line 109
    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Lcom/lltskb/lltskb/result/ResultActivity;->access$000(Lcom/lltskb/lltskb/result/ResultActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->height:I

    .line 110
    invoke-virtual {p1}, Lcom/lltskb/lltskb/result/ResultActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result p1

    .line 111
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    int-to-float p1, p1

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 112
    iget-object p1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 113
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, p2, v0, v2, p1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 115
    iget p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->height:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    sub-int/2addr p2, p1

    div-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->x:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 4

    .line 119
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/shapes/OvalShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 122
    iget-object p2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->text:Ljava/lang/String;

    iget v0, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->x:I

    int-to-float v0, v0

    iget v1, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->height:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iget-object v3, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/lltskb/lltskb/result/ResultActivity$TextOvalShape;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method
