.class Lcom/lltskb/lltskb/order/OrderTicketActivity$7;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->checkLoginStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 886
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 900
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    return-object v0
.end method

.method public onSignStatus(Z)V
    .locals 2

    .line 890
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSignStatus ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 892
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const-class v1, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 894
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
