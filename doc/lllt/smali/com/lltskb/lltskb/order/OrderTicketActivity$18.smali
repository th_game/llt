.class Lcom/lltskb/lltskb/order/OrderTicketActivity$18;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->onTicketSearched(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 1335
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 2

    .line 1351
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$2100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1352
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 1353
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$300(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    goto :goto_0

    .line 1354
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1355
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$400(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onYes()V
    .locals 2

    .line 1339
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$2100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1340
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 1341
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$300(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    goto :goto_0

    .line 1342
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1343
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$400(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1346
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$2200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    return-void
.end method
