.class Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;
.super Landroid/os/AsyncTask;
.source "EditContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/EditContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DeletePassengerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/EditContactActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 1

    .line 708
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 709
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/EditContactActivity;Landroid/view/View;)V
    .locals 0

    .line 733
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 705
    check-cast p1, [Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->doInBackground([Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;
    .locals 1

    .line 740
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 744
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 746
    :try_start_0
    iget-object p1, p1, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->deletePassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 749
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getErrorMessage()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 752
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 753
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$EditContactActivity$DeletePassengerTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 718
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 705
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 724
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    .line 728
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-nez p1, :cond_1

    const v1, 0x7f0d00ce

    .line 730
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    .line 731
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->finish()V

    goto :goto_0

    :cond_1
    const v1, 0x7f0d010f

    .line 733
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/EditContactActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$DeletePassengerTask$qSXvsYZPb-4pV6bvYKhfqldij2M;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$DeletePassengerTask$qSXvsYZPb-4pV6bvYKhfqldij2M;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 735
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 713
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d00d0

    const/4 v2, -0x1

    .line 718
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$DeletePassengerTask$mxejkaYv16gCbOdN6bUZkMW1JaE;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$DeletePassengerTask$mxejkaYv16gCbOdN6bUZkMW1JaE;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 719
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
