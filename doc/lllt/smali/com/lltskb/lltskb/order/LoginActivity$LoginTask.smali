.class Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;
.super Landroid/os/AsyncTask;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LoginTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/LoginActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/LoginActivity;)V
    .locals 1

    .line 524
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 525
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$2(Lcom/lltskb/lltskb/order/LoginActivity;Landroid/view/View;)V
    .locals 0

    .line 565
    invoke-static {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$400(Lcom/lltskb/lltskb/order/LoginActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 521
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const-string v0, "0"

    const/4 v1, 0x0

    .line 594
    aget-object v3, p1, v1

    const/4 v2, 0x1

    .line 595
    aget-object v4, p1, v2

    const/4 v5, 0x2

    .line 596
    aget-object v5, p1, v5

    const/4 v6, 0x3

    .line 597
    aget-object v6, p1, v6

    const/4 v7, 0x4

    .line 598
    aget-object p1, p1, v7

    .line 599
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v7

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v8

    .line 601
    :try_start_0
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    move-object v2, v8

    invoke-interface/range {v2 .. v7}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->loginAysnSuggest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result p1

    if-nez p1, :cond_2

    .line 602
    invoke-interface {v8}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 605
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 606
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$LoginActivity$LoginTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 546
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->cancel(Z)Z

    return-void
.end method

.method public synthetic lambda$onPreExecute$1$LoginActivity$LoginTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 549
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 521
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 1

    .line 530
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0d0093

    .line 535
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 521
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5

    .line 556
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 558
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez v0, :cond_0

    return-void

    .line 563
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_4

    .line 565
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$VcXGdqHdVT33EZE3JmYNna4L0Rw;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$VcXGdqHdVT33EZE3JmYNna4L0Rw;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    const/4 v2, 0x0

    .line 567
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->access$302(Lcom/lltskb/lltskb/order/LoginActivity;Z)Z

    .line 568
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->clearCookie()V

    const-string v3, "\u975e\u6cd5\u8bf7\u6c42"

    .line 573
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const v4, 0x7f0d010f

    if-nez v3, :cond_3

    const-string v3, "\u5378\u8f7d"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const-string v3, "\u5bc6\u7801\u8f93\u5165\u9519\u8bef"

    .line 578
    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 579
    invoke-static {v0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$100(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 581
    :cond_2
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->access$302(Lcom/lltskb/lltskb/order/LoginActivity;Z)Z

    .line 583
    :goto_0
    invoke-virtual {v0, v4}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_2

    .line 574
    :cond_3
    :goto_1
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->access$302(Lcom/lltskb/lltskb/order/LoginActivity;Z)Z

    const p1, 0x7f0d01d1

    .line 575
    invoke-static {v0, v4, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_2
    return-void

    .line 588
    :cond_4
    invoke-static {v0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$200(Lcom/lltskb/lltskb/order/LoginActivity;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 540
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 541
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d01bf

    const/4 v2, -0x1

    .line 546
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$3_l-8Izn5o7jZ5IizMqjrUNneaA;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$3_l-8Izn5o7jZ5IizMqjrUNneaA;-><init>(Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 548
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    .line 549
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$aaz9w86O1m-F93yz2wA87ZBxZCI;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$LoginTask$aaz9w86O1m-F93yz2wA87ZBxZCI;-><init>(Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_1
    return-void
.end method
