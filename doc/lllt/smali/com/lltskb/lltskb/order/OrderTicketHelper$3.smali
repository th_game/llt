.class Lcom/lltskb/lltskb/order/OrderTicketHelper$3;
.super Ljava/lang/Object;
.source "OrderTicketHelper.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketHelper;->showBookTicketSuccess(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 3

    .line 340
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    .line 341
    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "auto_show_baoxian"

    const/4 v2, 0x0

    .line 343
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 345
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onYes()V
    .locals 3

    .line 330
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    .line 331
    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "auto_show_baoxian"

    const/4 v2, 0x1

    .line 333
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 334
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 335
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;->this$0:Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method
