.class Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;
.super Landroid/os/AsyncTask;
.source "NoCompleteOrderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CancelOrderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private activityWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;",
            ">;"
        }
    .end annotation
.end field

.field private orderId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;Ljava/lang/String;)V
    .locals 1

    .line 523
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 524
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    .line 525
    iput-object p2, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->orderId:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;Landroid/view/View;)V
    .locals 0

    .line 545
    invoke-static {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$100(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 519
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 555
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object p1

    .line 557
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->orderId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->cancelNoCompleteMyOrder(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    const-wide/16 v0, 0x3e8

    .line 559
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 561
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    const/4 p1, 0x0

    return-object p1

    .line 565
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    .line 569
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 570
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$NoCompleteOrderActivity$CancelOrderTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 533
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 519
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4

    .line 539
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 540
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-eqz v0, :cond_2

    .line 541
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    if-nez p1, :cond_1

    const v1, 0x7f0d0173

    .line 545
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelOrderTask$kqJdUscg9XwUOoY4GSu7Dz3HqK0;

    invoke-direct {v3, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelOrderTask$kqJdUscg9XwUOoY4GSu7Dz3HqK0;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    :cond_1
    const v1, 0x7f0d010f

    .line 547
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0d008d

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 550
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 529
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-eqz v0, :cond_1

    .line 530
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f0d008e

    const/4 v2, -0x1

    .line 533
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelOrderTask$E-5BRhSKHM9IMdk_5MdU-bkIJy0;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelOrderTask$E-5BRhSKHM9IMdk_5MdU-bkIJy0;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 534
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    :cond_1
    :goto_0
    return-void
.end method
