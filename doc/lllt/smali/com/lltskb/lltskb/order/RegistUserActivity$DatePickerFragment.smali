.class public Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "RegistUserActivity.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/RegistUserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DatePickerFragment"
.end annotation


# instance fields
.field private mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 229
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .line 241
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 244
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    if-eqz v0, :cond_0

    const v1, 0x7f090259

    .line 245
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 251
    :goto_0
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 252
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v1, v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 254
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 255
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 258
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    :cond_1
    :goto_1
    const/4 v0, 0x1

    .line 262
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v0, 0x2

    .line 263
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v0, 0x5

    .line 264
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 266
    new-instance p1, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    return-object p1
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1

    .line 271
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    if-nez p1, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/RegistUserActivity;

    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    .line 274
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    if-nez p1, :cond_1

    return-void

    .line 276
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    const/4 v0, 0x1

    .line 277
    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    const/4 p2, 0x2

    .line 278
    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->set(II)V

    const/4 p2, 0x5

    .line 279
    invoke-virtual {p1, p2, p4}, Ljava/util/Calendar;->set(II)V

    .line 280
    new-instance p2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    const-string p4, "yyyy-MM-dd"

    invoke-direct {p2, p4, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 281
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 282
    iget-object p2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    const p3, 0x7f090259

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    if-eqz p2, :cond_2

    .line 284
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    :cond_2
    iget-object p2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    if-eqz p2, :cond_3

    .line 286
    invoke-static {p2, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$002(Lcom/lltskb/lltskb/order/RegistUserActivity;Ljava/lang/String;)Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public setContext(Lcom/lltskb/lltskb/order/RegistUserActivity;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->mContext:Lcom/lltskb/lltskb/order/RegistUserActivity;

    return-void
.end method
