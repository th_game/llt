.class public Lcom/lltskb/lltskb/order/EditContactActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "EditContactActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/EditContactActivity$EditPassengerTask;,
        Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;,
        Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;,
        Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EditContactActivity"


# instance fields
.field mEditModel:Z

.field private mIdTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

.field private mTicketTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 51
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->doDeleteUser()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/EditContactActivity;)Z
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->updatePassengerDTO()Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->doEditUser()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->initStudentView()V

    return-void
.end method

.method private addPassenger()V
    .locals 5

    const-string v0, "EditContactActivity"

    const-string v1, "addPassenger"

    .line 442
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    .line 444
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private checkStudent()Z
    .locals 5

    const v0, 0x7f0902c6

    .line 350
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 355
    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getProvinceCode(Ljava/lang/String;)I

    move-result v0

    .line 356
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    .line 357
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0d0102

    .line 358
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_1
    const v0, 0x7f0900c6

    .line 362
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v2, 0x7f0d0105

    if-nez v0, :cond_2

    .line 364
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    .line 367
    :cond_2
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    .line 368
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_3
    const v0, 0x7f0900c9

    .line 373
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v2, 0x7f0d0106

    if-nez v0, :cond_4

    .line 375
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    .line 378
    :cond_4
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    .line 379
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 380
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_5
    const v2, 0x7f0900c8

    .line 384
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    if-nez v2, :cond_6

    return v1

    .line 387
    :cond_6
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    .line 388
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_7

    .line 389
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v4, 0x7f0d0131

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 390
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v4, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_7
    const v0, 0x7f0900be

    .line 394
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_8

    return v1

    .line 397
    :cond_8
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    .line 399
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const v2, 0x7f0d00f6

    if-eqz v0, :cond_9

    .line 400
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_9
    const v0, 0x7f0902d4

    .line 404
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_a

    return v1

    .line 407
    :cond_a
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    const v0, 0x7f090282

    .line 409
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_b

    return v1

    .line 412
    :cond_b
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    .line 415
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/lltskb/lltskb/order/EditContactActivity;->getSchoolCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_code:Ljava/lang/String;

    .line 416
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/lltskb/lltskb/order/EditContactActivity;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_code:Ljava/lang/String;

    .line 418
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/lltskb/lltskb/order/EditContactActivity;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_code:Ljava/lang/String;

    .line 421
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 422
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    .line 426
    :cond_c
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 427
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_d
    return v3
.end method

.method private doDeleteUser()V
    .locals 5

    const-string v0, "EditContactActivity"

    const-string v1, "doDeleteUser"

    .line 523
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    .line 525
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/EditContactActivity$DeletePassengerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private doEditUser()V
    .locals 5

    const-string v0, "EditContactActivity"

    const-string v1, "doEditUser"

    .line 560
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;-><init>()V

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    .line 565
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$EditPassengerTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$EditPassengerTask;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    .line 566
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/EditContactActivity$EditPassengerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getSchoolCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 546
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getSchoolByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p1

    .line 549
    :cond_0
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->teleCode:Ljava/lang/String;

    return-object p1
.end method

.method private getStationCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 553
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getCityByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p1

    .line 556
    :cond_0
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->teleCode:Ljava/lang/String;

    return-object p1
.end method

.method private initStudentView()V
    .locals 2

    const-string v0, "EditContactActivity"

    const-string v1, "initStudentView"

    .line 218
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f09015b

    .line 220
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    .line 221
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const v0, 0x7f0900c8

    .line 226
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    .line 227
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900be

    .line 229
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    .line 230
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902c6

    .line 233
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 234
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getProviceNameByCode(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900c6

    .line 236
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    .line 237
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902d4

    .line 240
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090282

    .line 243
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 244
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900c9

    .line 246
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 247
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private initView()V
    .locals 7

    const-string v0, "EditContactActivity"

    const-string v1, "initView"

    .line 93
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 95
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-eqz v3, :cond_3

    const v3, 0x7f0900c2

    .line 96
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    if-eqz v3, :cond_1

    .line 98
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    :cond_1
    const v3, 0x7f0900bf

    .line 104
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    if-eqz v3, :cond_2

    .line 106
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/LLTUtils;->getSecurePassenderId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_2

    .line 108
    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    :cond_2
    const v3, 0x7f0900c5

    .line 112
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    if-eqz v3, :cond_3

    .line 113
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v3, 0x7f09013a

    .line 117
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_4

    if-nez v0, :cond_4

    .line 120
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_4
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-eqz v3, :cond_5

    const v3, 0x7f090290

    .line 126
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_5

    .line 127
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const v3, 0x7f09015d

    .line 130
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 131
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :cond_6
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/16 v4, 0x8

    if-eqz v3, :cond_c

    const v3, 0x7f0902f2

    .line 133
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 134
    iget-object v5, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result v5

    sub-int/2addr v5, v1

    if-ltz v5, :cond_7

    .line 135
    iget-object v6, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_7

    if-eqz v3, :cond_7

    .line 136
    iget-object v6, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    const v3, 0x7f0901bd

    .line 139
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const v5, 0x7f0901bc

    .line 140
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    const v6, 0x7f0900e6

    .line 141
    invoke-virtual {p0, v6}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioGroup;

    if-eqz v0, :cond_8

    .line 144
    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setFocusable(Z)V

    .line 145
    invoke-virtual {v5, v2}, Landroid/widget/RadioButton;->setFocusable(Z)V

    .line 146
    invoke-virtual {v6, v2}, Landroid/widget/RadioGroup;->setFocusable(Z)V

    .line 147
    invoke-virtual {v3, v2}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 148
    invoke-virtual {v5, v2}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 152
    :cond_8
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    const-string v6, "F"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    if-eqz v3, :cond_a

    .line 153
    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    :cond_9
    if-eqz v5, :cond_a

    .line 156
    invoke-virtual {v5, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 160
    :cond_a
    :goto_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 162
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->startInitSchoolTask()V

    goto :goto_2

    :cond_b
    const v0, 0x7f09015b

    .line 164
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 165
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    :goto_2
    const v0, 0x7f0900c8

    .line 169
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_d

    .line 170
    new-instance v1, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_d
    const v0, 0x7f0900be

    .line 172
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_e

    .line 173
    new-instance v1, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_e
    const v0, 0x7f0900c6

    .line 175
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_f

    .line 176
    new-instance v1, Lcom/lltskb/lltskb/order/SchoolTextWatcher;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/order/SchoolTextWatcher;-><init>(Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_f
    const v0, 0x7f09014e

    .line 178
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 179
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_10
    const v0, 0x7f090155

    .line 181
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 182
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_11
    const v0, 0x7f090131

    .line 184
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 185
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_12
    const v0, 0x7f090074

    .line 187
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 188
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-boolean v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    if-eqz v1, :cond_13

    const/16 v1, 0x8

    goto :goto_3

    :cond_13
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f09004d

    .line 191
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 192
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-boolean v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    if-eqz v1, :cond_14

    const/4 v1, 0x0

    goto :goto_4

    :cond_14
    const/16 v1, 0x8

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f090051

    .line 195
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 196
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-boolean v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    if-eqz v1, :cond_15

    goto :goto_5

    :cond_15
    const/16 v2, 0x8

    :goto_5
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f090223

    .line 199
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 200
    iget-boolean v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    if-eqz v1, :cond_16

    if-eqz v0, :cond_17

    const v1, 0x7f0d00df

    .line 201
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :cond_16
    if-eqz v0, :cond_17

    const v1, 0x7f0d0042

    .line 203
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_17
    :goto_6
    const v0, 0x7f0900fb

    .line 206
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 207
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$lno7VNXXjmnO_BTP4ZvSwQmMDEY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$lno7VNXXjmnO_BTP4ZvSwQmMDEY;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_18
    return-void
.end method

.method private onDel()V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onDel"

    .line 529
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$1;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    const v1, 0x7f0d010f

    const v2, 0x7f0d00b2

    invoke-static {p0, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private onEdit()V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onEdit"

    .line 571
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$2;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$2;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    const v1, 0x7f0d02de

    const v2, 0x7f0d00b3

    invoke-static {p0, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private onSelectEnterYear()V
    .locals 4

    const-string v0, "EditContactActivity"

    const-string v1, "onSelectEnterYear"

    .line 477
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 479
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    .line 480
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    sub-int v3, v1, v2

    .line 482
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const v1, 0x7f090282

    const/4 v2, 0x0

    .line 485
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectIdType()V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onSelectIdType"

    .line 448
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    const v1, 0x7f090290

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectProvinceCode()V
    .locals 3

    .line 462
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getProvinceList()Ljava/util/List;

    move-result-object v0

    const v1, 0x7f0902c6

    const/4 v2, 0x0

    .line 463
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectSchoolSystem()V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onSelectSchoolSystem"

    .line 467
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    .line 470
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const v1, 0x7f0902d4

    const/4 v2, 0x0

    .line 473
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectTicketType()V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onSelectTicketType"

    .line 453
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$47rGUzR1eEsbSiUMzGWnvnEuViU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$47rGUzR1eEsbSiUMzGWnvnEuViU;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    const v2, 0x7f0902f2

    invoke-static {p0, v0, v2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSubmit()V
    .locals 2

    const-string v0, "EditContactActivity"

    const-string v1, "onSubmit"

    .line 434
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->updatePassengerDTO()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 438
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->addPassenger()V

    return-void
.end method

.method private startInitSchoolTask()V
    .locals 5

    .line 213
    new-instance v0, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    .line 214
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updatePassengerDTO()Z
    .locals 5

    const-string v0, "EditContactActivity"

    const-string v1, "updatePassengerDTO"

    .line 252
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0900bf

    .line 253
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 255
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    .line 258
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v2, :cond_2

    .line 259
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;-><init>()V

    iput-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    :cond_2
    const-string v2, "****"

    .line 262
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 263
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 266
    :cond_3
    iget-object v3, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 267
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 270
    :cond_4
    new-instance v3, Lcom/lltskb/lltskb/utils/IDCard;

    invoke-direct {v3}, Lcom/lltskb/lltskb/utils/IDCard;-><init>()V

    .line 272
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v4, "1"

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v3, v0}, Lcom/lltskb/lltskb/utils/IDCard;->verify(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 273
    invoke-virtual {v3}, Lcom/lltskb/lltskb/utils/IDCard;->getCodeError()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 277
    :cond_5
    iget-boolean v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    if-eqz v2, :cond_6

    .line 278
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_id_no:Ljava/lang/String;

    .line 279
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_id_type_code:Ljava/lang/String;

    .line 280
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_name:Ljava/lang/String;

    .line 283
    :cond_6
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const v0, 0x7f0900c2

    .line 285
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-nez v0, :cond_7

    return v1

    .line 287
    :cond_7
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    .line 288
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0d00f7

    .line 289
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    :cond_8
    const v0, 0x7f0901bd

    .line 293
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-nez v0, :cond_9

    return v1

    .line 295
    :cond_9
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 296
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v2, "M"

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    goto :goto_0

    .line 298
    :cond_a
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v2, "F"

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    :goto_0
    const v0, 0x7f090290

    .line 301
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_b

    return v1

    .line 303
    :cond_b
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const v0, 0x7f0d00f5

    .line 305
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    .line 308
    :cond_c
    iget-object v2, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x1

    add-int/2addr v0, v2

    const/4 v3, 0x3

    if-eq v0, v2, :cond_10

    const/4 v4, 0x2

    if-eq v0, v4, :cond_f

    if-eq v0, v3, :cond_e

    const/4 v4, 0x4

    if-eq v0, v4, :cond_d

    goto :goto_1

    .line 320
    :cond_d
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v4, "B"

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_1

    .line 317
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v4, "G"

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_1

    .line 314
    :cond_f
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v4, "C"

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_1

    .line 311
    :cond_10
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    :goto_1
    const v0, 0x7f0900c5

    .line 326
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_11

    .line 328
    iget-object v4, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    :cond_11
    const v0, 0x7f0902f2

    .line 331
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_12

    return v1

    .line 333
    :cond_12
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 334
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    const v0, 0x7f0d00e8

    .line 335
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    return v1

    .line 338
    :cond_13
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v2

    .line 339
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    .line 340
    iget-object v1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;-><init>()V

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    if-ne v0, v3, :cond_14

    .line 342
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->checkStudent()Z

    move-result v0

    return v0

    :cond_14
    return v2
.end method


# virtual methods
.method public synthetic lambda$initView$0$EditContactActivity(Landroid/view/View;)V
    .locals 0

    .line 207
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$onSelectTicketType$1$EditContactActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    const p1, 0x7f09015b

    .line 456
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x2

    if-ne p3, p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    .line 457
    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 491
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 496
    :sswitch_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSelectTicketType()V

    goto :goto_0

    .line 502
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSelectSchoolSystem()V

    goto :goto_0

    .line 499
    :sswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSelectProvinceCode()V

    goto :goto_0

    .line 493
    :sswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSelectIdType()V

    goto :goto_0

    .line 505
    :sswitch_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSelectEnterYear()V

    goto :goto_0

    .line 508
    :sswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onSubmit()V

    goto :goto_0

    .line 514
    :sswitch_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onEdit()V

    goto :goto_0

    .line 511
    :sswitch_7
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->onDel()V

    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f09004d -> :sswitch_7
        0x7f090051 -> :sswitch_6
        0x7f090074 -> :sswitch_5
        0x7f090131 -> :sswitch_4
        0x7f09013a -> :sswitch_3
        0x7f09014e -> :sswitch_2
        0x7f090155 -> :sswitch_1
        0x7f09015d -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "EditContactActivity"

    const-string v1, "onCreate"

    .line 59
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 62
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b003f

    .line 64
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->setContentView(I)V

    .line 66
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "passenger_index"

    const/4 v2, -0x1

    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 69
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object v1

    if-ltz v0, :cond_0

    .line 71
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 72
    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 73
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mEditModel:Z

    .line 76
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    .line 77
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d019f

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d01a1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d01a2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mIdTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d01a0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    .line 83
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d02a4

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d02a5

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d02ab

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity;->mTicketTypeList:Ljava/util/List;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d02a9

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->initView()V

    return-void
.end method

.method public onTextChanged(Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    return-void
.end method
