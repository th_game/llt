.class Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;
.super Landroid/os/AsyncTask;
.source "NoCompleteOrderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "QueryNoCompleteOrderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private isOrderInQueue:Z

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 1

    .line 580
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 578
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->isOrderInQueue:Z

    .line 581
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 575
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 p1, 0x0

    .line 644
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->isOrderInQueue:Z

    .line 645
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 647
    :try_start_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->queryMyOrderNoComplete()I

    move-result v1

    if-eqz v1, :cond_0

    .line 648
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 654
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v1

    .line 655
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderCacheDTO()Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    move-result-object v0

    if-eqz v1, :cond_1

    .line 656
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    if-nez v0, :cond_2

    .line 657
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d01e9

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    if-eqz v0, :cond_9

    .line 661
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 662
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->trainDate:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 663
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-le v3, v4, :cond_3

    .line 664
    invoke-virtual {v2, p1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 666
    :cond_3
    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->startTime:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 667
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x11

    if-le v4, v5, :cond_4

    const/16 v4, 0xb

    const/16 v5, 0x10

    .line 668
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 671
    :cond_4
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->message:Ljava/lang/String;

    .line 672
    iget v5, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->waitTime:I

    if-gtz v5, :cond_7

    iget v5, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->waitTime:I

    const/16 v6, -0x64

    if-ne v5, v6, :cond_5

    goto :goto_0

    .line 679
    :cond_5
    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v4, "\u51fa\u7968\u5931\u8d25\uff0c\u8bf7\u91cd\u65b0\u8d2d\u7968"

    .line 682
    :cond_6
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->isOrderInQueue:Z

    goto :goto_1

    .line 673
    :cond_7
    :goto_0
    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    const-string v4, "\u6b63\u5728\u6392\u961f\u4e2d...,\u8bf7\u8010\u5fc3\u7b49\u5f85"

    .line 676
    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n\n\u662f\u5426\u53d6\u6d88\u6392\u961f\u4e2d\u7684\u8ba2\u5355?"

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x1

    .line 677
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->isOrderInQueue:Z

    :goto_1
    const-string p1, "\u672a\u5b8c\u6210\u8ba2\u5355\u8f66\u6b21\u4fe1\u606f\n"

    .line 685
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u65e5\u671f:\t"

    .line 686
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\u540d\u79f0:\t"

    .line 688
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->stationTrainCode:Ljava/lang/String;

    .line 689
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\u53d1\u8f66\u65f6\u95f4:\t"

    .line 690
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\u51fa\u53d1\u7ad9:\t"

    .line 691
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->fromStationName:Ljava/lang/String;

    .line 692
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\u5230\u8fbe\u7ad9:\t"

    .line 693
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->toStationName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\u8ba2\u5355\u72b6\u6001:\t"

    .line 694
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_9
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 651
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 652
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$NoCompleteOrderActivity$QueryNoCompleteOrderTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 591
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 575
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 599
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 600
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-nez v0, :cond_0

    return-void

    .line 605
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$600(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Lcom/lltskb/lltskb/view/widget/XListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/widget/XListView;->stopRefresh()V

    .line 606
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$000(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    .line 607
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 608
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$700(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;->notifyDataSetChanged()V

    if-eqz p1, :cond_2

    .line 610
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0173

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 612
    iget-boolean v2, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->isOrderInQueue:Z

    if-eqz v2, :cond_1

    .line 613
    new-instance v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask$1;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask$1;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 625
    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_0
    return-void

    .line 630
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isBaoxianEnabled()Z

    move-result p1

    .line 632
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$500(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_3

    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 635
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$800(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    .line 637
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$900(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p1, :cond_4

    .line 638
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$1000(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    :cond_4
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 585
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-nez v0, :cond_0

    return-void

    .line 590
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d022d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    .line 591
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$QueryNoCompleteOrderTask$bpks8fu8LpQGE5xl5mAo4CK2r2c;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$QueryNoCompleteOrderTask$bpks8fu8LpQGE5xl5mAo4CK2r2c;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 593
    invoke-static {v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->access$500(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 594
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
