.class Lcom/lltskb/lltskb/order/OrderTicketActivity$5;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showOrderProgressDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 803
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 806
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object p1

    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$500(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/order/MonitorManager;->addTask(Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "OrderTicketActivity"

    const-string p2, "add task failed"

    .line 807
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const p2, 0x7f0d0103

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 811
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$600(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/app/ProgressDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 812
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$700(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    return-void
.end method
