.class public Lcom/lltskb/lltskb/order/MonitorTaskActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "MonitorTaskActivity.java"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

.field private mHandler:Landroid/os/Handler;

.field private mMsgView:Landroid/widget/TextView;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->refresh()V

    return-void
.end method

.method private initTimer()V
    .locals 7

    .line 68
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    .line 70
    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    .line 73
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    .line 74
    new-instance v2, Lcom/lltskb/lltskb/order/MonitorTaskActivity$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity$1;-><init>(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V

    .line 81
    iget-object v1, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x3e8

    const-wide/16 v5, 0x3e8

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method private initView()V
    .locals 4

    const/4 v0, 0x1

    .line 106
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b005c

    .line 107
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->setContentView(I)V

    const v0, 0x7f090293

    .line 108
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mMsgView:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mMsgView:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 110
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    const v0, 0x7f090172

    .line 113
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const v2, 0x7f090223

    .line 114
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0d01ce

    .line 115
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f0900fb

    .line 117
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 119
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$tsxo-tQLfdtjVeFK0H1aQG4Se7E;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$tsxo-tQLfdtjVeFK0H1aQG4Se7E;-><init>(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v2, 0x7f090067

    .line 122
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 124
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$F75PkJLEx064iX74Em61HqzVFMw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$F75PkJLEx064iX74Em61HqzVFMw;-><init>(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :cond_3
    new-instance v1, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    .line 133
    iget-object v1, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 134
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$THAVf4QCxnqCiASyNQBs9yPc0w4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$THAVf4QCxnqCiASyNQBs9yPc0w4;-><init>(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private refresh()V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$Kq8yw2HtCphCUL6HekN6Aarq28o;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$MonitorTaskActivity$Kq8yw2HtCphCUL6HekN6Aarq28o;-><init>(Lcom/lltskb/lltskb/order/MonitorTaskActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private showTaskInfo(I)V
    .locals 8

    if-ltz p1, :cond_a

    .line 138
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto/16 :goto_3

    .line 142
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/order/MonitorManager;->get(I)Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 145
    :cond_1
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getOrderConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    .line 148
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b>\u533a\u95f4:</b>"

    .line 149
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br/><b>\u8f66\u6b21:</b>"

    .line 151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br/><b>\u65e5\u671f:</b>"

    .line 152
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br/><b>\u65f6\u95f4:</b>"

    .line 153
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<br><b>\u8f66\u578b:</b>"

    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 159
    array-length v4, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_4

    aget-object v6, v1, v5

    .line 160
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getTrainTypeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 162
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 167
    :cond_4
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "<br/><b>\u4e58\u5ba2:</b>"

    .line 168
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "<br><b>\u5ea7\u5e2d:</b>"

    .line 171
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 174
    array-length v4, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_7

    aget-object v6, v1, v5

    .line 175
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 176
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 177
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_7
    const-string v1, "<br/><b>\u63d0\u793a\u97f3:</b>"

    .line 182
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_8

    const-string v1, "\u58f0\u97f3"

    .line 184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 185
    :cond_8
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_9

    const-string v1, "\u9707\u52a8"

    .line 186
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    :goto_2
    const-string v1, "<br/><b>\u67e5\u8be2\u9891\u7387:</b>"

    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f0d022a

    .line 191
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 192
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v4, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 193
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    const/4 v0, 0x0

    const-string v1, "\u4efb\u52a1\u8be6\u60c5"

    invoke-static {p0, v1, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :cond_a
    :goto_3
    return-void
.end method

.method private updateBgTask()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mMsgView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 95
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 99
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideBgTaskNotify(Landroid/content/Context;)V

    goto :goto_1

    .line 101
    :cond_2
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showBgTaskNotify(Landroid/content/Context;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$1$MonitorTaskActivity(Landroid/view/View;)V
    .locals 0

    .line 119
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$initView$2$MonitorTaskActivity(Landroid/view/View;)V
    .locals 0

    .line 126
    iget-object p1, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p1}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$initView$3$MonitorTaskActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 134
    invoke-direct {p0, p3}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->showTaskInfo(I)V

    return-void
.end method

.method public synthetic lambda$refresh$0$MonitorTaskActivity()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->notifyDataSetChanged()V

    .line 89
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->updateBgTask()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 43
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->initView()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 59
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onPause()V

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mTimer:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 50
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onResume()V

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/TaskMonitorListAdapter;->notifyDataSetChanged()V

    .line 54
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/MonitorTaskActivity;->initTimer()V

    return-void
.end method
