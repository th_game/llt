.class Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;
.super Landroid/os/AsyncTask;
.source "HeyanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/HeyanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BindTelTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/HeyanActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 1

    .line 357
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 358
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/HeyanActivity;Landroid/view/View;)V
    .locals 0

    .line 388
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 355
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 395
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->bindTel()I

    move-result p1

    if-eqz p1, :cond_0

    .line 396
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 399
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 400
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$HeyanActivity$BindTelTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 370
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 355
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 376
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 377
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 379
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 385
    invoke-static {v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$600(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f0d010f

    .line 388
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$BindTelTask$6JyxyQsZQQRBOSWFPqaqmdGVnIw;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$BindTelTask$6JyxyQsZQQRBOSWFPqaqmdGVnIw;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    .line 387
    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 363
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 364
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d01a6

    const/4 v2, -0x1

    .line 368
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$BindTelTask$otrWpudJ1daQ-_NEsS5AItyrNN8;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$BindTelTask$otrWpudJ1daQ-_NEsS5AItyrNN8;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
