.class public Lcom/lltskb/lltskb/order/HeyanActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "HeyanActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;,
        Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;,
        Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;,
        Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeyanActivity"


# instance fields
.field private mHYBtn:Landroid/widget/Button;

.field private mIsUseNewMobile:Z

.field private mMobileET:Landroid/widget/EditText;

.field private mMobileStatusTV:Landroid/widget/TextView;

.field private mOldMobileTV:Landroid/widget/TextView;

.field private mPassword:Landroid/widget/EditText;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 53
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/HeyanActivity;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity;->checkMobileCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->startBindTel()V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/HeyanActivity;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->startLogin()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/HeyanActivity;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->getUsedMobile()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$502(Lcom/lltskb/lltskb/order/HeyanActivity;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    return p1
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->onBindTel()V

    return-void
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->showRandCodeDlg()V

    return-void
.end method

.method private checkMobileCode(Ljava/lang/String;)V
    .locals 4

    .line 201
    new-instance v0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;Ljava/lang/String;)V

    .line 202
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private checkUser()V
    .locals 5

    const-string v0, "HeyanActivity"

    const-string v1, "checkUser"

    .line 206
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    new-instance v0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    .line 208
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getUsedMobile()Ljava/lang/String;
    .locals 1

    .line 176
    iget-boolean v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileET:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mOldMobileTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initView()V
    .locals 3

    const-string v0, "HeyanActivity"

    const-string v1, "initView"

    .line 77
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 78
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b0097

    .line 80
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->setContentView(I)V

    const/4 v0, 0x0

    .line 82
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    const v1, 0x7f090223

    .line 83
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0d01cb

    .line 84
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f0900fb

    .line 86
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 88
    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$HKqJonTb6nFWJSQl9uU9OyXoj1s;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$HKqJonTb6nFWJSQl9uU9OyXoj1s;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v1, 0x7f0902af

    .line 90
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 92
    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$HFhciq2Uf_88-s3ndof0Z1SSk5A;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$HFhciq2Uf_88-s3ndof0Z1SSk5A;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v1, 0x7f090067

    .line 95
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 96
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 97
    new-instance v0, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$mgPSGvmKfhROZ3GvkMixOe9mVHw;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$mgPSGvmKfhROZ3GvkMixOe9mVHw;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900c3

    .line 99
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileET:Landroid/widget/EditText;

    const v0, 0x7f0902a1

    .line 100
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mOldMobileTV:Landroid/widget/TextView;

    const v0, 0x7f0902a2

    .line 101
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileStatusTV:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileStatusTV:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0900c4

    .line 104
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mPassword:Landroid/widget/EditText;

    const v0, 0x7f090055

    .line 106
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mHYBtn:Landroid/widget/Button;

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mHYBtn:Landroid/widget/Button;

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$o3XGGMNvLwI8PGJwG_5-MAB7gJY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$o3XGGMNvLwI8PGJwG_5-MAB7gJY;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090061

    .line 113
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 114
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$PEoPnCbG-EWkYzXQeQnqb04vVTc;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$PEoPnCbG-EWkYzXQeQnqb04vVTc;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private onBindTel()V
    .locals 5

    .line 156
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getOldMobileNo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-nez v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mOldMobileTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getOldMobileNo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileStatusTV:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mHYBtn:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 163
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getMobileNo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mOldMobileTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getMobileNo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileStatusTV:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mHYBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v3, 0x1

    :cond_1
    if-eqz v3, :cond_2

    .line 171
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->showRandCodeDlg()V

    :cond_2
    return-void
.end method

.method private onEditTel()V
    .locals 5

    .line 122
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mMobileET:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "\u7535\u8bdd\u53f7\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 125
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 129
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isMobileNO(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "\u7535\u8bdd\u53f7\u7801\u683c\u5f0f\u4e0d\u6b63\u786e"

    .line 130
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 137
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void

    .line 141
    :cond_2
    new-instance v2, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;

    invoke-direct {v2, p0, v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v1, v3

    invoke-virtual {v2, v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private onHeyan()V
    .locals 0

    .line 242
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->showRandCodeDlg()V

    return-void
.end method

.method private register()V
    .locals 3

    const-string v0, "HeyanActivity"

    const-string v1, "register"

    .line 212
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 214
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215
    new-instance v1, Lcom/lltskb/lltskb/order/HeyanActivity$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/HeyanActivity$2;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 229
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    .line 230
    iget-object v2, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private showRandCodeDlg()V
    .locals 2

    .line 184
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->getUsedMobile()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/HeyanActivity$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/HeyanActivity$1;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-static {p0, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showRandCodeDlg(Landroid/content/Context;Ljava/lang/String;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V

    return-void
.end method

.method private startBindTel()V
    .locals 5

    .line 149
    new-instance v0, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    .line 151
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/HeyanActivity$BindTelTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private startLogin()V
    .locals 2

    const-string v0, "HeyanActivity"

    const-string v1, "startLogin"

    .line 234
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->register()V

    .line 236
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 238
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$HeyanActivity(Landroid/view/View;)V
    .locals 0

    .line 88
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$initView$1$HeyanActivity(Landroid/view/View;)V
    .locals 0

    const-string p1, "https://kyfw.12306.cn/otn/userSecurity/bindTel"

    .line 92
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$2$HeyanActivity(Landroid/view/View;)V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->checkUser()V

    return-void
.end method

.method public synthetic lambda$initView$3$HeyanActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x0

    .line 109
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    .line 110
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->onHeyan()V

    return-void
.end method

.method public synthetic lambda$initView$4$HeyanActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 115
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/HeyanActivity;->mIsUseNewMobile:Z

    .line 116
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->onEditTel()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 58
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "HeyanActivity"

    const-string v0, "onCreate"

    .line 59
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->initView()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "HeyanActivity"

    const-string v1, "onStart"

    .line 65
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onStart()V

    .line 67
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->checkUser()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 72
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onStop()V

    const-string v0, "HeyanActivity"

    const-string v1, "onStop"

    .line 73
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
