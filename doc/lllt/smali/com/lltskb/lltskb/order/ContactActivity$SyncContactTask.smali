.class Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;
.super Landroid/os/AsyncTask;
.source "ContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/ContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SyncContactTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/ContactActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/ContactActivity;)V
    .locals 1

    .line 200
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 201
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/ContactActivity;Landroid/view/View;)V
    .locals 0

    .line 229
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 197
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 244
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    const/4 v0, 0x1

    .line 246
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengers(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getErrorMessage()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 250
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 251
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$ContactActivity$SyncContactTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 212
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 197
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 218
    iget-object v0, p0, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/ContactActivity;

    if-nez v0, :cond_0

    return-void

    .line 222
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_2

    .line 224
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d010e

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/lltskb/lltskb/order/ContactActivity;->access$200(Lcom/lltskb/lltskb/order/ContactActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    invoke-static {v0}, Lcom/lltskb/lltskb/order/ContactActivity;->access$300(Lcom/lltskb/lltskb/order/ContactActivity;)V

    const/4 p1, 0x0

    .line 226
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/order/ContactActivity;->access$202(Lcom/lltskb/lltskb/order/ContactActivity;Z)Z

    return-void

    :cond_1
    const v1, 0x7f0d010f

    .line 229
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/ContactActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$SyncContactTask$sxkIoM4RKyJJim3Ie97jAsZxuIc;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$SyncContactTask$sxkIoM4RKyJJim3Ie97jAsZxuIc;-><init>(Lcom/lltskb/lltskb/order/ContactActivity;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 231
    :cond_2
    invoke-static {v0}, Lcom/lltskb/lltskb/order/ContactActivity;->access$400(Lcom/lltskb/lltskb/order/ContactActivity;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 232
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 205
    iget-object v0, p0, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/ContactActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d0228

    const/4 v2, -0x1

    .line 210
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$SyncContactTask$oyzg7sE4SfeXMQsspDXfGzsAumc;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$SyncContactTask$oyzg7sE4SfeXMQsspDXfGzsAumc;-><init>(Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 213
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
