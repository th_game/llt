.class public Lcom/lltskb/lltskb/order/MonitorManager;
.super Ljava/lang/Object;
.source "MonitorManager.java"


# static fields
.field private static mInstance:Lcom/lltskb/lltskb/order/MonitorManager;


# instance fields
.field private mOrderParameters:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

.field private mTasks:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    return-void
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/order/MonitorManager;Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mOrderParameters:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    return-object p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/MonitorManager;)Ljava/util/Vector;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    return-object p0
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/order/MonitorManager;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/order/MonitorManager;

    monitor-enter v0

    .line 27
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/order/MonitorManager;->mInstance:Lcom/lltskb/lltskb/order/MonitorManager;

    if-nez v1, :cond_0

    .line 28
    new-instance v1, Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-direct {v1}, Lcom/lltskb/lltskb/order/MonitorManager;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/order/MonitorManager;->mInstance:Lcom/lltskb/lltskb/order/MonitorManager;

    .line 30
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/order/MonitorManager;->mInstance:Lcom/lltskb/lltskb/order/MonitorManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method addTask(Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 47
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    new-instance v2, Lcom/lltskb/lltskb/order/MonitorManager$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/MonitorManager$1;-><init>(Lcom/lltskb/lltskb/order/MonitorManager;)V

    invoke-direct {v1, v2}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;)V

    .line 72
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getOrderConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 73
    iget-object v3, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    if-nez v3, :cond_1

    goto :goto_0

    .line 77
    :cond_1
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;-><init>()V

    .line 78
    invoke-virtual {v3, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->from(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const/4 v2, 0x1

    .line 79
    invoke-virtual {v3, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setIsIgnoreNetworkErr(Z)V

    .line 81
    iget-object v4, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v5, v2, [Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    aput-object v3, v5, v0

    invoke-virtual {v1, v4, v5}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 84
    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->cancel(Z)Z

    return v2

    :cond_2
    :goto_0
    return v0
.end method

.method public clear()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public get(I)Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    return-object p1
.end method

.method getmOrderParameters()Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mOrderParameters:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    return-object v0
.end method

.method public removeTask(I)V
    .locals 1

    if-ltz p1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public size()I
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager;->mTasks:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method
