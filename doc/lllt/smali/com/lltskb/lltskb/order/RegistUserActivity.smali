.class public Lcom/lltskb/lltskb/order/RegistUserActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "RegistUserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RegistUserActivity"


# instance fields
.field private mBirthday:Ljava/lang/String;

.field private mIdTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

.field private mTicketTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserName:Ljava/lang/String;

.field private mUserPass:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mBirthday:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lcom/lltskb/lltskb/order/RegistUserActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mBirthday:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/RegistUserActivity;I)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onIdTypeSelected(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/RegistUserActivity;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->startInitSchoolTask()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/RegistUserActivity;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->showRandCodeDlg()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/order/RegistUserActivity;)Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/order/RegistUserActivity;Ljava/lang/String;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->subDetail(Ljava/lang/String;)V

    return-void
.end method

.method private checkStudent()Z
    .locals 5

    const v0, 0x7f0902c6

    .line 785
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 790
    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getProvinceCode(Ljava/lang/String;)I

    move-result v0

    .line 791
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    .line 792
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "\u5b66\u6821\u7701\u4efd\u4e0d\u80fd\u4e3a\u7a7a!"

    .line 793
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_1
    const v0, 0x7f0900c6

    .line 797
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v2, "\u5b66\u6821\u540d\u79f0\u4e0d\u80fd\u4e3a\u7a7a!"

    if-nez v0, :cond_2

    .line 799
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 802
    :cond_2
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    .line 803
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 804
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_3
    const v0, 0x7f0900c9

    .line 808
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v2, "\u5b66\u53f7\u4e0d\u80fd\u4e3a\u7a7a!"

    if-nez v0, :cond_4

    .line 810
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 813
    :cond_4
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    .line 814
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 815
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_5
    const v2, 0x7f0900c8

    .line 819
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    if-nez v2, :cond_6

    return v1

    .line 822
    :cond_6
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    .line 823
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 824
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4f18\u60e0\u533a\u95f4 \u8f66\u7ad9"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u6ca1\u6709\u627e\u5230\uff01"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_7
    const v0, 0x7f0900be

    .line 828
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_8

    return v1

    .line 831
    :cond_8
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    .line 833
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "\u4f18\u60e0\u533a\u95f4\u4e0d\u80fd\u4e3a\u7a7a!"

    .line 834
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_9
    const v0, 0x7f0902d4

    .line 838
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_a

    return v1

    .line 841
    :cond_a
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    const v0, 0x7f090282

    .line 843
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_b

    return v1

    .line 846
    :cond_b
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    .line 849
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->getSchoolCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_code:Ljava/lang/String;

    .line 850
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_code:Ljava/lang/String;

    .line 852
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_code:Ljava/lang/String;

    .line 855
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "\u4f18\u60e0\u533a\u95f4\u65e0\u6cd5\u8bc6\u522b!"

    if-eqz v0, :cond_c

    .line 856
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 860
    :cond_c
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 861
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_d
    const/4 v0, 0x1

    return v0
.end method

.method private getSchoolCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 868
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getSchoolByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p1

    .line 871
    :cond_0
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->teleCode:Ljava/lang/String;

    return-object p1
.end method

.method private getStationCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 875
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getCityByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p1

    .line 878
    :cond_0
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->teleCode:Ljava/lang/String;

    return-object p1
.end method

.method private initView()V
    .locals 3

    const-string v0, "RegistUserActivity"

    const-string v1, "initView"

    .line 98
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f09013a

    .line 100
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f09015d

    .line 104
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f0900b8

    .line 108
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900cb

    .line 111
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 112
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900ba

    .line 114
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900c2

    .line 117
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 118
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900bf

    .line 120
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900bc

    .line 123
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 124
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0900c5

    .line 126
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v0, 0x7f0902f2

    .line 129
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 132
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const v0, 0x7f09015b

    .line 135
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v2, 0x8

    .line 137
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const v0, 0x7f0900c8

    .line 139
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_4

    .line 141
    new-instance v2, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_4
    const v0, 0x7f0900be

    .line 143
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_5

    .line 145
    new-instance v2, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_5
    const v0, 0x7f0900c6

    .line 147
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_6

    .line 149
    new-instance v2, Lcom/lltskb/lltskb/order/SchoolTextWatcher;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/SchoolTextWatcher;-><init>(Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_6
    const v0, 0x7f09012a

    .line 151
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 153
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    const v0, 0x7f090121

    .line 155
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 157
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    const v0, 0x7f09014e

    .line 159
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 161
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    const v0, 0x7f090155

    .line 163
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 165
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    const v0, 0x7f090131

    .line 167
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 169
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    const v0, 0x7f09005e

    .line 171
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 172
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09004a

    .line 174
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 175
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 176
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v2, "\u7f51\u9875\u7248"

    .line 177
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090223

    .line 179
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_c

    const v2, 0x7f0d0238

    .line 181
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_c
    const v0, 0x7f0900fb

    .line 183
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 185
    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$RegistUserActivity$gcIeTX9cw86e7YZaaJUA4vWERvs;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$RegistUserActivity$gcIeTX9cw86e7YZaaJUA4vWERvs;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    :cond_d
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onIdTypeSelected(I)V

    return-void
.end method

.method private static isHKMacao(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "^[HMhm]{1}([0-9]{10}|[0-9]{8})$"

    .line 306
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 307
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 308
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    return p0
.end method

.method private static isPassport(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "^[a-zA-Z]{5,17}$"

    .line 314
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "^[a-zA-Z0-9]{5,17}$"

    .line 315
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 317
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 318
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 319
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static isPassword(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "(?![a-z]+$|[0-9]+$|_+$)^[a-zA-Z0-9_]{6,}$"

    .line 334
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 335
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 336
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    return p0
.end method

.method private static isTaiw(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "^[0-9]{8}$"

    .line 325
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "^[0-9]{10}$"

    .line 326
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 328
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 329
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 330
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static isUserName(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "^[A-Za-z]{1}([A-Za-z0-9]|[_]){0,29}$"

    .line 298
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 299
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 300
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    return p0
.end method

.method private onIdTypeSelected(I)V
    .locals 6

    const v0, 0x7f090157

    .line 352
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f09012a

    .line 355
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const v2, 0x7f090121

    .line 359
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    :cond_2
    const/16 v3, 0x8

    if-nez p1, :cond_3

    .line 364
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 365
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 366
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne p1, v4, :cond_4

    .line 368
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 369
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 370
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const/4 v4, 0x2

    if-ne p1, v4, :cond_5

    .line 372
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 373
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 374
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    const/4 v3, 0x3

    if-ne p1, v3, :cond_6

    .line 376
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 377
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 378
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    :goto_0
    return-void
.end method

.method private onNext()V
    .locals 6

    .line 481
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->verifyInput()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 484
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$4;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    .line 523
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ge v1, v2, :cond_1

    new-array v1, v5, [Ljava/lang/String;

    aput-object v3, v1, v4

    .line 524
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 526
    :cond_1
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void
.end method

.method private onSelectBirthday()V
    .locals 3

    .line 291
    new-instance v0, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;-><init>()V

    .line 292
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "datePicker"

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/RegistUserActivity$DatePickerFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private onSelectCountry()V
    .locals 3

    .line 383
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getCountryList()Ljava/util/List;

    move-result-object v0

    const v1, 0x7f09026c

    const/4 v2, 0x0

    .line 384
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectEnterYear()V
    .locals 4

    const-string v0, "RegistUserActivity"

    const-string v1, "onSelectEnterYear"

    .line 468
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 470
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    .line 471
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    sub-int v3, v1, v2

    .line 473
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const v1, 0x7f090282

    const/4 v2, 0x0

    .line 476
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectIdType()V
    .locals 3

    const-string v0, "RegistUserActivity"

    const-string v1, "onSelectIdType"

    .line 340
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    new-instance v1, Lcom/lltskb/lltskb/order/RegistUserActivity$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$1;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    const v2, 0x7f090290

    invoke-static {p0, v0, v2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectProvinceCode()V
    .locals 3

    .line 453
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getProvinceList()Ljava/util/List;

    move-result-object v0

    const v1, 0x7f0902c6

    const/4 v2, 0x0

    .line 454
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectSchoolSystem()V
    .locals 3

    const-string v0, "RegistUserActivity"

    const-string v1, "onSelectSchoolSystem"

    .line 458
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    .line 461
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const v1, 0x7f0902d4

    const/4 v2, 0x0

    .line 464
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onSelectTicketType()V
    .locals 3

    const-string v0, "RegistUserActivity"

    const-string v1, "onSelectTicketType"

    .line 388
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    new-instance v1, Lcom/lltskb/lltskb/order/RegistUserActivity$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$2;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    const v2, 0x7f0902f2

    invoke-static {p0, v0, v2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showSelectDialog(Lcom/lltskb/lltskb/BaseActivity;Ljava/util/List;ILandroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private showRandCodeDlg()V
    .locals 2

    .line 532
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    new-instance v1, Lcom/lltskb/lltskb/order/RegistUserActivity$5;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$5;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    invoke-static {p0, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showRandCodeDlg(Landroid/content/Context;Ljava/lang/String;Lcom/lltskb/lltskb/utils/LLTUIUtils$RandCodeListener;)V

    return-void
.end method

.method private startInitSchoolTask()V
    .locals 5

    .line 409
    new-instance v0, Lcom/lltskb/lltskb/order/RegistUserActivity$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$3;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    .line 450
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private subDetail(Ljava/lang/String;)V
    .locals 4

    .line 549
    new-instance v0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$6;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity;Ljava/lang/String;)V

    .line 591
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private verifyInput()Z
    .locals 12

    const v0, 0x7f0900b8

    .line 597
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 598
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    .line 599
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "\u7528\u6237\u540d\u4e0d\u80fd\u4e3a\u7a7a\uff01"

    .line 600
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    .line 605
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x6

    if-ge v0, v2, :cond_1

    const-string v0, "\u7528\u6237\u540d\u957f\u5ea6\u4e0d\u80fd\u5c11\u4e8e6\u4e2a\u5b57\u7b26\uff01"

    .line 606
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x1e

    if-lt v0, v3, :cond_2

    const-string v0, "\u7528\u6237\u540d\u957f\u5ea6\u4e0d\u80fd\u591a\u4e8e30\u4e2a\u5b57\u7b26\uff01"

    .line 610
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 613
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "\u8f93\u5165\u7684\u7528\u6237\u540d\u4e0d\u80fd\u5305\u542b\u7a7a\u683c"

    .line 614
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 617
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->isUserName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "\u7528\u6237\u540d\u53ea\u80fd\u7531\u5b57\u6bcd\u3001\u6570\u5b57\u6216_\u7ec4\u6210\uff01"

    .line 618
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_4
    const v0, 0x7f0900cb

    .line 622
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 623
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    .line 624
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "\u7528\u6237\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 625
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 629
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "\u7528\u6237\u5bc6\u7801\u4e0d\u80fd\u5305\u542b\u7a7a\u683c"

    .line 630
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 634
    :cond_6
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "\u7528\u6237\u5bc6\u7801\u4e0d\u80fd\u548c\u7528\u6237\u540d\u79f0\u76f8\u540c"

    .line 635
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 639
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_8

    const-string v0, "\u7528\u6237\u5bc6\u7801\u4e0d\u80fd\u5c11\u4e8e6\u4e2a\u5b57\u7b26"

    .line 640
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 644
    :cond_8
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->isPassword(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "\u7528\u6237\u5bc6\u7801\u683c\u5f0f\u9519\u8bef\uff0c\u5fc5\u987b\u4e14\u53ea\u80fd\u5305\u542b\u5b57\u6bcd\uff0c\u6570\u5b57\uff0c\u4e0b\u5212\u7ebf\u4e2d\u7684\u4e24\u79cd\u6216\u4e24\u79cd\u4ee5\u4e0a\uff01"

    .line 645
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 649
    :cond_9
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    const/16 v3, 0x27

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    const/16 v3, 0x3e

    .line 650
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    goto/16 :goto_5

    :cond_a
    const v0, 0x7f0900ba

    .line 655
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 656
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mUserPass:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "\u4e24\u6b21\u5bc6\u7801\u8f93\u5165\u4e0d\u4e00\u81f4"

    .line 657
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 661
    :cond_b
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 662
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;-><init>()V

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    .line 663
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v3, ""

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->email:Ljava/lang/String;

    const v0, 0x7f0900bc

    .line 665
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 666
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 667
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 668
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmail(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v0, "\u90ae\u4ef6\u5730\u5740\u683c\u5f0f\u4e0d\u6b63\u786e"

    .line 669
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 672
    :cond_c
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->email:Ljava/lang/String;

    :cond_d
    const v0, 0x7f0900c2

    .line 675
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 676
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    .line 677
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "\u59d3\u540d\u4e0d\u80fd\u4e3a\u7a7a"

    .line 678
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 682
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    const v0, 0x7f0900c5

    .line 683
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 684
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    .line 685
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "\u624b\u673a\u53f7\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 686
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 690
    :cond_f
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isMobileNO(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "\u60a8\u8f93\u5165\u7684\u624b\u673a\u53f7\u7801\u4e0d\u662f\u6709\u6548\u7684\u683c\u5f0f\uff01"

    .line 691
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    :cond_10
    const v0, 0x7f090290

    .line 695
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 696
    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v3, 0x1

    add-int/2addr v0, v3

    const-string v4, "G"

    const-string v5, "C"

    const/4 v6, 0x3

    const-string v7, "B"

    const/4 v8, 0x2

    const-string v9, "1"

    if-eq v0, v3, :cond_14

    if-eq v0, v8, :cond_13

    if-eq v0, v6, :cond_12

    const/4 v10, 0x4

    if-eq v0, v10, :cond_11

    goto :goto_0

    .line 708
    :cond_11
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v7, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_0

    .line 705
    :cond_12
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_0

    .line 702
    :cond_13
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    goto :goto_0

    .line 699
    :cond_14
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v9, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    :goto_0
    const v0, 0x7f0900bf

    .line 713
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 714
    iget-object v10, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 715
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "\u8bc1\u4ef6\u53f7\u7801\u4e0d\u80fd\u4e3a\u7a7a"

    .line 716
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 719
    :cond_15
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v10, "F"

    const-string v11, "M"

    if-eqz v0, :cond_18

    .line 720
    new-instance v0, Lcom/lltskb/lltskb/utils/IDCard;

    invoke-direct {v0}, Lcom/lltskb/lltskb/utils/IDCard;-><init>()V

    .line 721
    iget-object v4, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/lltskb/lltskb/utils/IDCard;->verify(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 722
    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/IDCard;->getCodeError()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 726
    :cond_16
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v8

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 727
    iget-object v4, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    add-int/lit8 v0, v0, -0x30

    rem-int/2addr v0, v8

    if-nez v0, :cond_17

    move-object v0, v10

    goto :goto_1

    :cond_17
    move-object v0, v11

    :goto_1
    iput-object v0, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    goto :goto_2

    .line 728
    :cond_18
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 729
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->isHKMacao(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "\u8bf7\u8f93\u5165\u6709\u6548\u7684\u6e2f\u6fb3\u5c45\u6c11\u901a\u884c\u8bc1\u53f7\u7801\uff01"

    .line 730
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 733
    :cond_19
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 734
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->isTaiw(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "\u8bf7\u8f93\u5165\u6709\u6548\u7684\u53f0\u6e7e\u5c45\u6c11\u901a\u884c\u8bc1\u53f7\u7801\uff01"

    .line 735
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 738
    :cond_1a
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 739
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->isPassport(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "\u8bf7\u8f93\u5165\u6709\u6548\u7684\u62a4\u7167\u53f7\u7801\uff01"

    .line 740
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 745
    :cond_1b
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    const v0, 0x7f0901bd

    .line 746
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-nez v0, :cond_1c

    return v1

    .line 748
    :cond_1c
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 749
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v11, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    goto :goto_3

    .line 751
    :cond_1d
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iput-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    .line 756
    :cond_1e
    :goto_3
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 757
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v4, "CN"

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->country_code:Ljava/lang/String;

    .line 758
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 759
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0xa

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0xc

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0xe

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mBirthday:Ljava/lang/String;

    goto :goto_4

    .line 760
    :cond_1f
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    const v0, 0x7f09026c

    .line 761
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 762
    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->country_code:Ljava/lang/String;

    :cond_20
    :goto_4
    const v0, 0x7f0902f2

    .line 765
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_21

    return v1

    .line 768
    :cond_21
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 769
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    const-string v0, "\u4e58\u5ba2\u7c7b\u578b\u4e0d\u80fd\u4e3a\u7a7a!"

    .line 770
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1

    .line 773
    :cond_22
    iget-object v1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v3

    .line 774
    iget-object v1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    if-ne v0, v6, :cond_23

    .line 777
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->checkStudent()Z

    move-result v0

    return v0

    :cond_23
    return v3

    :cond_24
    :goto_5
    const-string v0, "\u5bc6\u7801\u5305\u542b\u975e\u6cd5\u5b57\u7b26"

    .line 651
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return v1
.end method


# virtual methods
.method public synthetic lambda$initView$0$RegistUserActivity(Landroid/view/View;)V
    .locals 0

    .line 185
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 192
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 197
    :sswitch_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectTicketType()V

    goto :goto_0

    .line 203
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectSchoolSystem()V

    goto :goto_0

    .line 200
    :sswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectProvinceCode()V

    goto :goto_0

    .line 194
    :sswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectIdType()V

    goto :goto_0

    .line 206
    :sswitch_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectEnterYear()V

    goto :goto_0

    .line 209
    :sswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectCountry()V

    goto :goto_0

    .line 212
    :sswitch_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onSelectBirthday()V

    goto :goto_0

    .line 218
    :sswitch_7
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->onNext()V

    goto :goto_0

    :sswitch_8
    const-string p1, "https://kyfw.12306.cn/otn/regist/init"

    .line 215
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f09004a -> :sswitch_8
        0x7f09005e -> :sswitch_7
        0x7f090121 -> :sswitch_6
        0x7f09012a -> :sswitch_5
        0x7f090131 -> :sswitch_4
        0x7f09013a -> :sswitch_3
        0x7f09014e -> :sswitch_2
        0x7f090155 -> :sswitch_1
        0x7f09015d -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 77
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 78
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->requestWindowFeature(I)Z

    const p1, 0x7f0b007c

    .line 79
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->setContentView(I)V

    .line 81
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    .line 82
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    const-string v0, "\u4e8c\u4ee3\u8eab\u4efd\u8bc1"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    const-string v0, "\u6e2f\u6fb3\u901a\u884c\u8bc1"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    const-string v0, "\u53f0\u6e7e\u901a\u884c\u8bc1"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mIdTypeList:Ljava/util/List;

    const-string v0, "\u62a4\u7167"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    .line 88
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    const-string v0, "\u6210\u4eba\u7968"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    const-string v0, "\u513f\u7ae5\u7968"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    const-string v0, "\u5b66\u751f\u7968"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity;->mTicketTypeList:Ljava/util/List;

    const-string v0, "\u6b8b\u519b\u7968"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->initView()V

    return-void
.end method

.method public onTextChanged(Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    return-void
.end method
