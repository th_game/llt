.class Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;
.super Landroid/os/AsyncTask;
.source "NoCompleteOrderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CancelNoCompleteOrderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 1

    .line 705
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 706
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Ljava/lang/String;Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;Landroid/view/View;)V
    .locals 0

    if-nez p0, :cond_0

    .line 751
    invoke-virtual {p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->onRefresh()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 703
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 710
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object p1

    .line 712
    :try_start_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->cancelQueueNoCompleteMyOrder()Z

    move-result p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 720
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d008d

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 717
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 718
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$NoCompleteOrderActivity$CancelNoCompleteOrderTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 732
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 703
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4

    .line 737
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 738
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-nez p1, :cond_0

    .line 741
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d0092

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 744
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-nez v1, :cond_1

    return-void

    .line 749
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d008c

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 750
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelNoCompleteOrderTask$mbq_E_dE4iwmw8bqicFh2VY4kDM;

    invoke-direct {v3, p1, v1}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelNoCompleteOrderTask$mbq_E_dE4iwmw8bqicFh2VY4kDM;-><init>(Ljava/lang/String;Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {v1, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 725
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 726
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    if-nez v0, :cond_0

    return-void

    .line 731
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0091

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    .line 732
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelNoCompleteOrderTask$gE_4c7M4e2sO6syXsWMHCZcwq7k;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$CancelNoCompleteOrderTask$gE_4c7M4e2sO6syXsWMHCZcwq7k;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
