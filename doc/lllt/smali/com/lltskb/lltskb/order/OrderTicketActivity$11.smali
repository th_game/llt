.class Lcom/lltskb/lltskb/order/OrderTicketActivity$11;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showFreqDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

.field final synthetic val$fmt:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;Ljava/lang/String;)V
    .locals 0

    .line 1067
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    iput-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->val$fmt:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .line 1070
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->val$fmt:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    add-int/2addr p2, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1071
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1700(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1072
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1700(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    .line 1073
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setQueryFreq(Ljava/lang/Integer;)V

    .line 1074
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
