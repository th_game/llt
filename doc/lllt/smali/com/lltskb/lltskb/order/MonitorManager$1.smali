.class Lcom/lltskb/lltskb/order/MonitorManager$1;
.super Ljava/lang/Object;
.source "MonitorManager.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/MonitorManager;->addTask(Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/MonitorManager;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/MonitorManager;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/lltskb/lltskb/order/MonitorManager$1;->this$0:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchTicketProgress(I)V
    .locals 0

    return-void
.end method

.method public onTicketSearched(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager$1;->this$0:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/order/MonitorManager;->access$002(Lcom/lltskb/lltskb/order/MonitorManager;Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/order/MonitorManager$1;->this$0:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->access$100(Lcom/lltskb/lltskb/order/MonitorManager;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    .line 58
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->getOrderConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v2

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-ne v2, v3, :cond_0

    .line 59
    iget-object p1, p0, Lcom/lltskb/lltskb/order/MonitorManager$1;->this$0:Lcom/lltskb/lltskb/order/MonitorManager;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/MonitorManager;->access$100(Lcom/lltskb/lltskb/order/MonitorManager;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 64
    :cond_1
    new-instance p1, Landroid/content/Intent;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x4

    const-string v1, "order_from_flag"

    .line 65
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    .line 66
    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 68
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/AppContext;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
