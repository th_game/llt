.class Lcom/lltskb/lltskb/order/OrderTicketActivity$14;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTrainTypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

.field final synthetic val$trainType:[Ljava/lang/String;

.field final synthetic val$trainTypeChecked:[Z


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;[Z)V
    .locals 0

    .line 1113
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    iput-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->val$trainType:[Ljava/lang/String;

    iput-object p3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->val$trainTypeChecked:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 1

    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    const/4 p1, 0x1

    .line 1118
    :goto_0
    iget-object p3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->val$trainType:[Ljava/lang/String;

    array-length p3, p3

    if-ge p1, p3, :cond_0

    .line 1119
    iget-object p3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->val$trainTypeChecked:[Z

    const/4 v0, 0x0

    aput-boolean v0, p3, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1121
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->val$trainType:[Ljava/lang/String;

    aget-object p2, p3, p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1122
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1123
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    .line 1125
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1600(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    return-void
.end method
