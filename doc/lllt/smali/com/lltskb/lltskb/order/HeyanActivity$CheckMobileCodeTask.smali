.class Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;
.super Landroid/os/AsyncTask;
.source "HeyanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/HeyanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CheckMobileCodeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private randCode:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/HeyanActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/HeyanActivity;Ljava/lang/String;)V
    .locals 1

    .line 294
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 295
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    .line 296
    iput-object p2, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->randCode:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/HeyanActivity;Landroid/view/View;)V
    .locals 0

    .line 322
    invoke-static {p0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$100(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    return-void
.end method

.method static synthetic lambda$onPostExecute$2(Landroid/view/View;)V
    .locals 0

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 291
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 335
    iget-object p1, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 341
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$400(Lcom/lltskb/lltskb/order/HeyanActivity;)Ljava/lang/String;

    move-result-object v0

    .line 342
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->randCode:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->checkMobileCode(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v0, 0x0

    .line 345
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$502(Lcom/lltskb/lltskb/order/HeyanActivity;Z)Z
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 347
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 348
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$HeyanActivity$CheckMobileCodeTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 306
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 291
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 313
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 315
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    const p1, 0x7f0d0073

    .line 321
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0d0173

    .line 322
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$BNe6LrwJGZ7EHVYglssXv9vt5r0;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$BNe6LrwJGZ7EHVYglssXv9vt5r0;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    :cond_1
    const v1, 0x7f0d010f

    .line 325
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$5ANMv6_fWqD_oqK6gCBu9DMDIJo;->INSTANCE:Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$5ANMv6_fWqD_oqK6gCBu9DMDIJo;

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 329
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 301
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, -0x1

    .line 306
    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$H8lXyt-nLFytrwwpkGP9hodjed0;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckMobileCodeTask$H8lXyt-nLFytrwwpkGP9hodjed0;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity$CheckMobileCodeTask;)V

    const-string v3, "\u6b63\u5728\u6821\u9a8c\u624b\u673a"

    invoke-static {v0, v3, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 308
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
