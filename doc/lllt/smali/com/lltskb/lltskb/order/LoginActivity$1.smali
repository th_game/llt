.class Lcom/lltskb/lltskb/order/LoginActivity$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/LoginActivity;->clearUser()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/LoginActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/LoginActivity;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity$1;->this$0:Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNo()V
    .locals 0

    return-void
.end method

.method public onYes()V
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$1;->this$0:Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$000(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 241
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->removeUser(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->save()V

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$1;->this$0:Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$000(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$1;->this$0:Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/LoginActivity;->access$100(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
