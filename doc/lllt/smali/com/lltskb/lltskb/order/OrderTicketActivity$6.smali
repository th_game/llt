.class Lcom/lltskb/lltskb/order/OrderTicketActivity$6;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showDateDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 843
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(IIILjava/lang/String;)V
    .locals 4

    .line 847
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$802(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I

    .line 848
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$902(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I

    .line 849
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1, p3}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1002(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I

    .line 851
    invoke-static {p4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    const/4 p2, 0x3

    const/4 p3, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    .line 852
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array p2, p2, [Ljava/lang/Object;

    iget-object p4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    .line 853
    invoke-static {p4}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result p4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p2, v0

    iget-object p4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p4}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result p4

    add-int/2addr p4, v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p2, v1

    iget-object p4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p4}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result p4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p2, p3

    const-string p3, "%04d-%02d-%02d"

    .line 852
    invoke-static {p1, p3, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 854
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 856
    :cond_0
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    .line 857
    invoke-static {v3}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, p3

    aput-object p4, v2, p2

    const-string p2, "%04d-%02d-%02d %s"

    .line 856
    invoke-static {p1, p2, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 858
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 861
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method
