.class public Lcom/lltskb/lltskb/order/OrderTicketHelper;
.super Ljava/lang/Object;
.source "OrderTicketHelper.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;


# static fields
.field private static final TAG:Ljava/lang/String; = "OrderTicketHelper"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsStuAlerted:Z

.field private mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mIsStuAlerted:Z

    .line 50
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/OrderTicketHelper;)Landroid/content/Context;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private bookTicket()V
    .locals 4

    const-string v0, "OrderTicketHelper"

    const-string v1, "bookTicket"

    .line 253
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    .line 255
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 257
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$eW7oH6wOZ5zDm_fRWZc0TVjxP-0;

    invoke-direct {v2, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$eW7oH6wOZ5zDm_fRWZc0TVjxP-0;-><init>(Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;)V

    const v0, 0x7f0d00b5

    const/4 v3, -0x1

    invoke-static {v1, v0, v3, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private checkOrderInfo()V
    .locals 5

    .line 149
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTourFlag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$3lOYg5i4CP1XhaT4DJ9zijEnfWk;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$3lOYg5i4CP1XhaT4DJ9zijEnfWk;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;)V

    .line 164
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private checkShowChooseSeats()V
    .locals 5

    .line 195
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setChooseSeats(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseSeats()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    const-string v1, "\u5ea7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 209
    new-instance v4, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$F_mxtdf6cQJxFbAk3b4Pn4sikrs;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$F_mxtdf6cQJxFbAk3b4Pn4sikrs;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {v0, v3, v1, v2, v4}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->newInstance(IILjava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/view/ChooseSeatsDialog$Listener;)Lcom/lltskb/lltskb/view/ChooseSeatsDialog;

    move-result-object v0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/lltskb/lltskb/BaseActivity;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "chooseSeatsDialog"

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/view/ChooseSeatsDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 221
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketHelper"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 225
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->bookTicket()V

    :goto_1
    return-void
.end method

.method static synthetic lambda$bookTicket$5(Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 258
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->cancel(Z)Z

    return-void
.end method

.method static synthetic lambda$submitOrder$1(Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;Landroid/view/View;)V
    .locals 3

    .line 136
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private parseInt(Ljava/lang/String;)I
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 170
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 174
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 176
    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 178
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x39

    const/16 v5, 0x30

    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v5, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-le v3, v4, :cond_2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    :cond_2
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v5, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v3, v4, :cond_3

    .line 182
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 186
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-lez p1, :cond_4

    .line 187
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result v0

    :cond_4
    :goto_2
    return v0
.end method

.method private showBookTicketSuccess(Ljava/lang/String;)V
    .locals 3

    .line 320
    invoke-static {}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isBaoxianEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "baoxian_hint"

    .line 321
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const v1, 0x7f0d0203

    if-eqz v0, :cond_1

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\n"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f0d0066

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 325
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper$3;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_1

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$Um-HtqgDA2Z4WtIhhxn_yfETevQ;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$Um-HtqgDA2Z4WtIhhxn_yfETevQ;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_1
    return-void
.end method

.method private showPassCodeDialog()V
    .locals 4

    const-string v0, "OrderTicketHelper"

    const-string v1, "showPassCodeDialog"

    .line 233
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    new-instance v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00b3

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/PassCodeDialog;-><init>(Landroid/content/Context;II)V

    .line 237
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$DycuY5Gx9ZxV23DqqD8F5vJcmrY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$DycuY5Gx9ZxV23DqqD8F5vJcmrY;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->setListener(Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;)V

    .line 242
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->show()V

    .line 243
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f0e00bd

    .line 245
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$checkOrderInfo$2$OrderTicketHelper(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d010f

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 154
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getIfShowPassCode()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Y"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 156
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->showPassCodeDialog()V

    goto :goto_0

    .line 158
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setRangeCode(Ljava/lang/String;)V

    .line 159
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->checkShowChooseSeats()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$checkShowChooseSeats$3$OrderTicketHelper(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setChooseSeats(Ljava/lang/String;)V

    .line 215
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->bookTicket()V

    return-void
.end method

.method public synthetic lambda$onSubmitOrderResult$6$OrderTicketHelper(Landroid/view/View;)V
    .locals 0

    .line 289
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->checkOrderInfo()V

    return-void
.end method

.method public synthetic lambda$onSubmitOrderResult$7$OrderTicketHelper(Landroid/view/View;)V
    .locals 0

    .line 301
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->submitOrder()V

    return-void
.end method

.method public synthetic lambda$showBookTicketSuccess$8$OrderTicketHelper(Landroid/view/View;)V
    .locals 2

    .line 351
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const-class v1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "auto_show_baoxian"

    const/4 v1, 0x0

    .line 354
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 355
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 356
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public synthetic lambda$showPassCodeDialog$4$OrderTicketHelper(Ljava/lang/String;)V
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setRangeCode(Ljava/lang/String;)V

    .line 240
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->checkShowChooseSeats()V

    return-void
.end method

.method public synthetic lambda$submitOrder$0$OrderTicketHelper(ILjava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->checkOrderInfo()V

    goto :goto_0

    :cond_0
    const/4 v0, -0x8

    if-ne p1, v0, :cond_1

    .line 101
    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const-class v0, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {p1, p2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const/4 v0, -0x6

    const v1, 0x7f0d010f

    if-ne p1, v0, :cond_2

    .line 104
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketHelper$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper$1;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    .line 116
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 119
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketHelper$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketHelper$2;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :goto_0
    return-void
.end method

.method public onSubmitOrderResult(ILjava/lang/String;)V
    .locals 3

    const-string v0, "OrderTicketHelper"

    const-string v1, "onSubmitOrderResult"

    .line 263
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, " activity is finishing"

    .line 265
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 269
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const/4 v0, 0x0

    const-string v1, "\u975e\u6cd5\u8bf7\u6c42"

    .line 272
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p2, :cond_2

    const-string v1, "\u5378\u8f7d"

    .line 273
    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    const-string p2, "\u63d0\u4ea4\u8ba2\u5355\u5931\u8d25\uff0c\u8bf7\u518d\u8bd5\u4e00\u6b21\u3002"

    .line 278
    :cond_2
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    const-string p2, "\u672a\u77e5\u9519\u8bef"

    :cond_3
    const/4 v1, -0x5

    const v2, 0x7f0d010f

    if-eq p1, v1, :cond_7

    const/4 v1, -0x4

    if-eq p1, v1, :cond_6

    const/4 v1, -0x2

    if-eq p1, v1, :cond_7

    if-eqz p1, :cond_5

    if-nez v0, :cond_4

    .line 296
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 298
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$W51ekSdYLJlRkuBzdpZ_CvgTgtw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$W51ekSdYLJlRkuBzdpZ_CvgTgtw;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 284
    :cond_5
    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->showBookTicketSuccess(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string p2, "bookticket"

    const-string v0, "\u6210\u529f"

    invoke-static {p1, p2, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 292
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const-string p2, "\u7528\u6237\u53d6\u6d88\u4e86"

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 289
    :cond_7
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$Jwe6L5Tsrw6IO8Zer2ru-I0V0Cs;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$Jwe6L5Tsrw6IO8Zer2ru-I0V0Cs;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-static {p1, v0, p2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_0
    return-void
.end method

.method public publishProgress(Ljava/lang/String;)V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->setLoadingDialogMsg(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;

    return-void
.end method

.method submitOrder()V
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    if-nez v0, :cond_0

    return-void

    .line 58
    :cond_0
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v0, :cond_1

    return-void

    .line 61
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 64
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    .line 68
    :cond_2
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 74
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const v3, 0x7f0d01ea

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 77
    :cond_3
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 79
    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    const-string v7, "3"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v5, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    :cond_5
    if-eqz v4, :cond_6

    const-string v1, "0X00"

    .line 88
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setPurpose(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setPurpose(Ljava/lang/String;)V

    .line 92
    :cond_6
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->setOrderParams(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V

    .line 95
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const-string v4, "bookticket"

    const-string v6, "\u8bf7\u6c42"

    invoke-static {v1, v4, v6}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTourFlag()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$BginfyaDmQp9uo3-83oYXREmza4;

    invoke-direct {v7, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$BginfyaDmQp9uo3-83oYXREmza4;-><init>(Lcom/lltskb/lltskb/order/OrderTicketHelper;)V

    invoke-direct {v1, v4, v6, v7}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;)V

    if-eqz v5, :cond_7

    .line 134
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canBuyStudentTicket()Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mIsStuAlerted:Z

    if-nez v0, :cond_7

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0173

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0088

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$KpSuZLTvygS3WwBh5NcPnDjMMRM;

    invoke-direct {v5, v1}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketHelper$KpSuZLTvygS3WwBh5NcPnDjMMRM;-><init>(Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;)V

    invoke-static {v0, v2, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 137
    iput-boolean v3, p0, Lcom/lltskb/lltskb/order/OrderTicketHelper;->mIsStuAlerted:Z

    goto :goto_1

    .line 139
    :cond_7
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v2

    invoke-virtual {v1, v0, v3}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void
.end method
