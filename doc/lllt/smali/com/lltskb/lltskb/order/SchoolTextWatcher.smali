.class public Lcom/lltskb/lltskb/order/SchoolTextWatcher;
.super Ljava/lang/Object;
.source "SchoolTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private mTextView:Landroid/widget/AutoCompleteTextView;


# direct methods
.method constructor <init>(Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/lltskb/lltskb/order/SchoolTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    .line 15
    invoke-virtual {p1}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 26
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 29
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/adapters/SchoolAdapter;

    iget-object p2, p0, Lcom/lltskb/lltskb/order/SchoolTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/adapters/SchoolAdapter;-><init>(Landroid/content/Context;)V

    .line 30
    iget-object p2, p0, Lcom/lltskb/lltskb/order/SchoolTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2, p1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
