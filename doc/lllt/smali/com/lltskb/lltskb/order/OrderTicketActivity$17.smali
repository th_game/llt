.class Lcom/lltskb/lltskb/order/OrderTicketActivity$17;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showSeatDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

.field final synthetic val$selectSeats:[Z


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Z)V
    .locals 0

    .line 1191
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    iput-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;->val$selectSeats:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1195
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 p2, 0x0

    .line 1196
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;->val$selectSeats:[Z

    array-length v1, v0

    if-ge p2, v1, :cond_1

    .line 1197
    aget-boolean v0, v0, p2

    if-eqz v0, :cond_0

    .line 1198
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1201
    :cond_1
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$2000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1202
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$2000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    return-void
.end method
