.class public Lcom/lltskb/lltskb/order/StationTextWatcher;
.super Ljava/lang/Object;
.source "StationTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;

.field private mTextView:Landroid/widget/AutoCompleteTextView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mListener:Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;

    .line 21
    iput-object p2, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 42
    iget-object p1, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mListener:Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;

    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;->onTextChanged(Landroid/widget/AutoCompleteTextView;)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 31
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 34
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/adapters/StationAdapter;

    iget-object p2, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/adapters/StationAdapter;-><init>(Landroid/content/Context;)V

    .line 35
    iget-object p2, p0, Lcom/lltskb/lltskb/order/StationTextWatcher;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2, p1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
