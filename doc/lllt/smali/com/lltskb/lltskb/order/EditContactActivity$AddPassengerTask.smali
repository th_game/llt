.class Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;
.super Landroid/os/AsyncTask;
.source "EditContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/EditContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AddPassengerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/EditContactActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 1

    .line 655
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 656
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Lcom/lltskb/lltskb/order/EditContactActivity;Landroid/view/View;)V
    .locals 0

    .line 679
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/EditContactActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 652
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 686
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 690
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 692
    :try_start_0
    iget-object p1, p1, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->addPassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 695
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getErrorMessage()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 699
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 700
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$EditContactActivity$AddPassengerTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 664
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->cancel(Z)Z

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .line 670
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    .line 674
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-nez p1, :cond_1

    const v1, 0x7f0d0041

    .line 676
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;I)V

    .line 677
    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->finish()V

    goto :goto_0

    :cond_1
    const v1, 0x7f0d010f

    .line 679
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/EditContactActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$AddPassengerTask$EpThjZpzwV1i3zQsAlqCGgYaJ28;

    invoke-direct {v3, v0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$AddPassengerTask$EpThjZpzwV1i3zQsAlqCGgYaJ28;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 681
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 660
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d0043

    const/4 v2, -0x1

    .line 664
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$AddPassengerTask$GGi8_As8kGAb7nVV6H0BFvE1Scg;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$AddPassengerTask$GGi8_As8kGAb7nVV6H0BFvE1Scg;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity$AddPassengerTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 665
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
