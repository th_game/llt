.class public Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "NoCompleteOrderActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;,
        Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;,
        Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NoCompleteOrderActivity"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

.field private mAutoShowBaoxian:Z

.field private mBaoxianLayout:Landroid/view/View;

.field private mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTicketNumView:Landroid/widget/TextView;

.field private mTimeView:Landroid/widget/TextView;

.field mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 56
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAutoShowBaoxian:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->updateHeader()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->queryNoCompleteOrder()V

    return-void
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->showBaoxianFragment()V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->doCancelOrder()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->payMyOrder()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;Z)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->cancelQueueMyOrderNoComplete(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Landroid/view/View;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mBaoxianLayout:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Lcom/lltskb/lltskb/view/widget/XListView;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->startUpdateTimer()V

    return-void
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)Z
    .locals 0

    .line 54
    iget-boolean p0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAutoShowBaoxian:Z

    return p0
.end method

.method private cancelQueueMyOrderNoComplete(Z)V
    .locals 6

    .line 446
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderCacheDTO()Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 448
    :cond_0
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->trainDate:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 449
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-le v3, v4, :cond_1

    .line 450
    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 452
    :cond_1
    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->startTime:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 453
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x11

    if-le v4, v5, :cond_2

    const/16 v4, 0xb

    const/16 v5, 0x10

    .line 454
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 457
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u8bf7\u518d\u6b21\u786e\u8ba4\u662f\u5426\u53d6\u6d88\u6392\u961f\u4e2d\u7684\u8ba2\u5355\n"

    .line 458
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u65e5\u671f\t"

    .line 459
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 460
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\u540d\u79f0:\t"

    .line 461
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->stationTrainCode:Ljava/lang/String;

    .line 462
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\u53d1\u8f66\u65f6\u95f4:\t"

    .line 463
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\u51fa\u53d1\u7ad9:\t"

    .line 464
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->fromStationName:Ljava/lang/String;

    .line 465
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\u5230\u8fbe\u7ad9:\t"

    .line 466
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->toStationName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_3

    .line 469
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0d0173

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 470
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$6;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$6;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void

    .line 484
    :cond_3
    new-instance p1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    .line 486
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelNoCompleteOrderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private checkLoginStatus()V
    .locals 5

    const-string v0, "NoCompleteOrderActivity"

    const-string v1, "checkLoginStatus"

    .line 367
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;

    new-instance v1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$5;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$5;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;)V

    .line 387
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private doCancelOrder()V
    .locals 5

    const-string v0, "NoCompleteOrderActivity"

    const-string v1, "doCancelOrder"

    .line 294
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 301
    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    .line 303
    new-instance v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;Ljava/lang/String;)V

    .line 305
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$CancelOrderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_0
    return-void
.end method

.method private getPassengers()Ljava/util/Vector;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation

    .line 220
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    .line 222
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_3

    :cond_0
    const/4 v2, 0x0

    .line 226
    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    if-eqz v0, :cond_7

    .line 228
    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    if-nez v3, :cond_1

    goto :goto_3

    .line 230
    :cond_1
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v3, 0x0

    .line 232
    :goto_0
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 233
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    if-nez v4, :cond_2

    goto :goto_2

    .line 235
    :cond_2
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x12

    if-eq v5, v6, :cond_3

    goto :goto_2

    .line 240
    :cond_3
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 241
    iget-object v7, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v5, 0x1

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    :goto_1
    if-nez v5, :cond_6

    .line 247
    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_6
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_7
    :goto_3
    return-object v1
.end method

.method private initView()V
    .locals 4

    const/4 v0, 0x1

    .line 125
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->requestWindowFeature(I)Z

    const v1, 0x7f0b0061

    .line 126
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->setContentView(I)V

    .line 127
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->checkLoginStatus()V

    .line 129
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "auto_show_baoxian"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAutoShowBaoxian:Z

    :cond_0
    const v1, 0x7f090223

    .line 134
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0d01f3

    .line 135
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f0900fb

    .line 137
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 138
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$HcUNlUA7St1167ALkqaMxlS_kbk;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$HcUNlUA7St1167ALkqaMxlS_kbk;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090067

    .line 140
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 141
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 142
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$5SQ5uO2Hjy8D0K74xti4h73WGSM;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$5SQ5uO2Hjy8D0K74xti4h73WGSM;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f09004b

    .line 144
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 145
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$wfiV5TDwnsAl9crkMu8Tkn0Tze4;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$wfiV5TDwnsAl9crkMu8Tkn0Tze4;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090049

    .line 147
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 148
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$rYAgNheX2h1tr-GrSGqe8PWMAwI;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$rYAgNheX2h1tr-GrSGqe8PWMAwI;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    new-instance v1, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

    const v1, 0x7f090187

    .line 151
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 152
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 153
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const-string v3, ""

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 155
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    .line 156
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, p0}, Lcom/lltskb/lltskb/view/widget/XListView;->setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V

    .line 157
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAdapter:Lcom/lltskb/lltskb/adapters/NoCompleteListAdapter;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 160
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    new-instance v1, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$2;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 176
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 177
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    const v0, 0x7f090120

    .line 180
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mBaoxianLayout:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mBaoxianLayout:Landroid/view/View;

    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$2emwhZLaiQsuinKVqq4AeiM02c0;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$NoCompleteOrderActivity$2emwhZLaiQsuinKVqq4AeiM02c0;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mBaoxianLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private payMyOrder()V
    .locals 4

    .line 358
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 359
    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-direct {v1}, Lcom/lltskb/lltskb/fragment/SelectBankFragment;-><init>()V

    const v2, 0x7f010013

    const v3, 0x7f010014

    .line 360
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 361
    const-class v2, Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0900de

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 363
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private queryNoCompleteOrder()V
    .locals 5

    const-string v0, "NoCompleteOrderActivity"

    const-string v1, "queryNoCompleteOrder"

    .line 439
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    new-instance v0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    .line 442
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$QueryNoCompleteOrderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private showBaoxianFragment()V
    .locals 4

    const/4 v0, 0x0

    .line 198
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mAutoShowBaoxian:Z

    const-string v0, "kuntao_h5"

    .line 199
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->showXindanH5()V

    return-void

    .line 204
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getPassengers()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 205
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 209
    new-instance v2, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;-><init>()V

    .line 210
    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->setPassengers(Ljava/util/Vector;)V

    const v0, 0x7f010013

    const v3, 0x7f010014

    .line 211
    invoke-virtual {v1, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    const v0, 0x7f0900de

    .line 212
    const-class v3, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 214
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_2
    :goto_0
    return-void
.end method

.method private showCancelOrderConfirm()V
    .locals 7

    const-string v0, "NoCompleteOrderActivity"

    const-string v1, "showCancelOrderConfirm"

    .line 259
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 263
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 267
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 268
    new-instance v2, Landroid/support/v7/app/AppCompatDialog;

    const v3, 0x7f0e0003

    invoke-direct {v2, p0, v3}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f0b001f

    .line 269
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    const/4 v3, 0x1

    .line 270
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    .line 272
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v4, 0x7f0d0127

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    aput-object v6, v5, v1

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->train_code_page:Ljava/lang/String;

    aput-object v1, v5, v3

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->start_train_date_page:Ljava/lang/String;

    aput-object v0, v5, v1

    invoke-static {v4, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d0173

    .line 275
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$3;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$3;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {p0, v1, v0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private showXindanH5()V
    .locals 3

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "web_url"

    const-string v2, "http://wap.lltskb.com/ktzx.html"

    .line 188
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "web_post"

    .line 189
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "web_closeonback"

    .line 190
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 191
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startUpdateTimer()V
    .locals 7

    .line 70
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    .line 72
    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    .line 75
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    .line 76
    new-instance v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$1;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    .line 83
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x3e8

    const-wide/16 v5, 0x3e8

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method private updateHeader()V
    .locals 4

    .line 395
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimeView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const v0, 0x7f0902b1

    .line 396
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimeView:Landroid/widget/TextView;

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTicketNumView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const v0, 0x7f0902ed

    .line 399
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTicketNumView:Landroid/widget/TextView;

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimeView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTicketNumView:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 407
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v0, 0x0

    .line 408
    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 410
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_date:Ljava/lang/String;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    .line 413
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_date:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 415
    :cond_2
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_date:Ljava/lang/String;

    .line 418
    :goto_0
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 419
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    .line 420
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u5269\u4f59\u65f6\u95f4 <b><font color=\'red\'>"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->pay_limit_time:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getTimeLeft(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</font></b>"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 423
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimeView:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTicketNumView:Landroid/widget/TextView;

    iget v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_totalnum:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 426
    :cond_4
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderCacheDTO()Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 427
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->message:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 428
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimeView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->message:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTicketNumView:Landroid/widget/TextView;

    iget v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->ticketCount:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_1
    return-void
.end method


# virtual methods
.method protected continuePay()V
    .locals 6

    const-string v0, "NoCompleteOrderActivity"

    const-string v1, "continuePay"

    .line 309
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 312
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 313
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 331
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    .line 339
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d0147

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 340
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    aput-object v5, v4, v1

    const/4 v1, 0x1

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->train_code_page:Ljava/lang/String;

    aput-object v5, v4, v1

    const/4 v1, 0x2

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->start_train_date_page:Ljava/lang/String;

    aput-object v5, v4, v1

    const/4 v1, 0x3

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_total_price_page:Ljava/lang/String;

    aput-object v0, v4, v1

    invoke-static {v3, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d0173

    .line 342
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    new-instance v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$4;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity$4;-><init>(Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;)V

    invoke-static {p0, v1, v0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$initView$0$NoCompleteOrderActivity(Landroid/view/View;)V
    .locals 0

    .line 138
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$initView$1$NoCompleteOrderActivity(Landroid/view/View;)V
    .locals 0

    .line 142
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->checkLoginStatus()V

    return-void
.end method

.method public synthetic lambda$initView$2$NoCompleteOrderActivity(Landroid/view/View;)V
    .locals 0

    .line 145
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->continuePay()V

    return-void
.end method

.method public synthetic lambda$initView$3$NoCompleteOrderActivity(Landroid/view/View;)V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->showCancelOrderConfirm()V

    return-void
.end method

.method public synthetic lambda$initView$4$NoCompleteOrderActivity(Landroid/view/View;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->showBaoxianFragment()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 107
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->initView()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 115
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 493
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/fragment/SelectBankFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/BaseFragment;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 494
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->dismiss()V

    return v1

    .line 499
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/BaseFragment;

    if-eqz v0, :cond_1

    .line 500
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 501
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->dismiss()V

    return v1

    .line 506
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLoadMore()V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 60
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onPause()V

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->mTimer:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .line 511
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->queryNoCompleteOrder()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 88
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onResume()V

    .line 89
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getMyOrderNoComplete()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;->startUpdateTimer()V

    :cond_0
    return-void
.end method
