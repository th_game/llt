.class Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;
.super Landroid/os/AsyncTask;
.source "EditContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/EditContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InitSchoolTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/EditContactActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/EditContactActivity;)V
    .locals 1

    .line 597
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 598
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 594
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 627
    iget-object p1, p0, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 633
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getAllCitys()V

    .line 634
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getAllSchools()V

    .line 636
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 637
    iget-object p1, p1, Lcom/lltskb/lltskb/order/EditContactActivity;->mPassenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->showPassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "EditContactActivity"

    const-string v0, "error show passenger"

    .line 638
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 642
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 643
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$EditContactActivity$InitSchoolTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 607
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 594
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1

    .line 612
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 613
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    .line 618
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_1

    .line 620
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 622
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/order/EditContactActivity;->access$300(Lcom/lltskb/lltskb/order/EditContactActivity;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 602
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 603
    iget-object v0, p0, Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/EditContactActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d01a8

    const/high16 v2, -0x1000000

    .line 607
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$InitSchoolTask$8Oqt8Fr7Xvb6pVebeY8yXwd65Pg;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$EditContactActivity$InitSchoolTask$8Oqt8Fr7Xvb6pVebeY8yXwd65Pg;-><init>(Lcom/lltskb/lltskb/order/EditContactActivity$InitSchoolTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
