.class Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;
.super Landroid/os/AsyncTask;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InitLoginTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/LoginActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/LoginActivity;)V
    .locals 1

    .line 467
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 468
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 464
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 472
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/LoginActivity;

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 477
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/utils/JSEngine;->setSignValue(Ljava/lang/String;)V

    .line 478
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v1

    invoke-interface {v1}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->loginInit()Z

    move-result v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/order/LoginActivity;->access$302(Lcom/lltskb/lltskb/order/LoginActivity;Z)Z

    return-object v0
.end method

.method public synthetic lambda$onPreExecute$0$LoginActivity$InitLoginTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 501
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->cancel(Z)Z

    return-void
.end method

.method public synthetic lambda$onPreExecute$1$LoginActivity$InitLoginTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 504
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 464
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 1

    .line 485
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0d0093

    .line 489
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 464
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0

    .line 510
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 511
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 512
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez p1, :cond_0

    return-void

    .line 517
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/order/LoginActivity;->access$400(Lcom/lltskb/lltskb/order/LoginActivity;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 494
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 495
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/LoginActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d01bf

    const/4 v2, -0x1

    .line 500
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$InitLoginTask$fWpjOJoZnSDLCBT4vM_7OIW2kO8;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$InitLoginTask$fWpjOJoZnSDLCBT4vM_7OIW2kO8;-><init>(Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 503
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    .line 504
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$InitLoginTask$UPCATHXQMtQlQAB7CAAWvfuvrSg;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$InitLoginTask$UPCATHXQMtQlQAB7CAAWvfuvrSg;-><init>(Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_1
    return-void
.end method
