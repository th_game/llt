.class public Lcom/lltskb/lltskb/order/CompleteOrderActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "CompleteOrderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final AUTO_QUERY:Ljava/lang/String; = "auto_query"

.field private static final DATE_TYPE_END:I = 0x1

.field private static final DATE_TYPE_START:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CompleteOrderView"


# instance fields
.field private mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private mDateType:I

.field private mEndDate:Ljava/util/Date;

.field private mStartDate:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 198
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    .line 48
    new-instance v0, Lcom/lltskb/lltskb/order/-$$Lambda$CompleteOrderActivity$NLuHzSsUFL-OjEuWHWRNg3C6SwM;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$CompleteOrderActivity$NLuHzSsUFL-OjEuWHWRNg3C6SwM;-><init>(Lcom/lltskb/lltskb/order/CompleteOrderActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    return-void
.end method

.method private checkEndDate()V
    .locals 3

    .line 93
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 95
    iget-object v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0901ba

    .line 97
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const v2, 0x7f09027f

    if-eqz v1, :cond_2

    .line 98
    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 99
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->afterToday(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isToday(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 103
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 100
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->updateEndDate(Z)V

    goto :goto_1

    .line 106
    :cond_2
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 107
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private onBaoXian()V
    .locals 5

    const-string v0, "CompleteOrderView"

    const-string v1, "onBaoXian"

    .line 237
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragment_mybaoxian_query"

    .line 239
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 241
    new-instance v2, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/QueryBaoxianFragment;-><init>()V

    .line 243
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v3, 0x7f010018

    const v4, 0x7f010019

    .line 244
    invoke-virtual {v0, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    const v3, 0x7f0900de

    .line 245
    invoke-virtual {v0, v3, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 246
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private onQuery()V
    .locals 5

    const-string v0, "CompleteOrderView"

    const-string v1, "onQuery"

    .line 250
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragment_myorder_query"

    .line 252
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 254
    new-instance v2, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/MyOrderListFragment;-><init>()V

    .line 256
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v3, 0x7f010018

    const v4, 0x7f010019

    .line 257
    invoke-virtual {v0, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    const v3, 0x7f0900de

    .line 258
    invoke-virtual {v0, v3, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    const/4 v1, 0x0

    .line 259
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 260
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private retResign()V
    .locals 1

    const-string v0, "https://kyfw.12306.cn/otn/queryOrder/init"

    .line 232
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private updateDateDisplay(III)V
    .locals 3

    .line 58
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    add-int/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v1, p1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v1, p2

    const-string p1, "%04d-%02d-%02d"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 59
    new-instance p2, Ljava/text/SimpleDateFormat;

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v0, "yyyy-MM-dd"

    invoke-direct {p2, v0, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 62
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 64
    invoke-virtual {p2}, Ljava/text/ParseException;->printStackTrace()V

    const/4 p2, 0x0

    .line 66
    :goto_0
    iget p3, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateType:I

    if-nez p3, :cond_0

    .line 67
    iput-object p2, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mStartDate:Ljava/util/Date;

    const p2, 0x7f0902e3

    .line 68
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 69
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 71
    :cond_0
    iput-object p2, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    const p2, 0x7f0901ba

    .line 72
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/RadioButton;

    const p3, 0x7f09027f

    if-eqz p2, :cond_3

    .line 73
    invoke-virtual {p2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result p2

    if-nez p2, :cond_3

    .line 74
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->afterToday(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_2

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->isToday(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 78
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 75
    :cond_2
    :goto_1
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->updateEndDate(Z)V

    goto :goto_2

    .line 81
    :cond_3
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 82
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private updateEndDate(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 171
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    goto :goto_0

    .line 173
    :cond_0
    new-instance p1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    sub-long/2addr v0, v2

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object p1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    .line 175
    :goto_0
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {p1, v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const v0, 0x7f09027f

    .line 177
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    invoke-virtual {p1, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected dismissFragment(Ljava/lang/String;)Z
    .locals 3

    .line 279
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 280
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 282
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 283
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 284
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$new$0$CompleteOrderActivity(Landroid/widget/DatePicker;III)V
    .locals 0

    .line 48
    invoke-direct {p0, p2, p3, p4}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->updateDateDisplay(III)V

    return-void
.end method

.method public synthetic lambda$onCreate$1$CompleteOrderActivity(Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-nez p2, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->checkEndDate()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 164
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->updateEndDate(Z)V

    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/4 p1, 0x0

    .line 206
    iput p1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateType:I

    .line 207
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->showDateDialog()V

    goto :goto_0

    .line 220
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->retResign()V

    goto :goto_0

    :sswitch_2
    const/4 p1, 0x1

    .line 210
    iput p1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateType:I

    .line 211
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->showDateDialog()V

    goto :goto_0

    .line 226
    :sswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->onBaoXian()V

    goto :goto_0

    .line 214
    :sswitch_4
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->finish()V

    goto :goto_0

    .line 217
    :sswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->onQuery()V

    :goto_0
    :sswitch_6
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f090066 -> :sswitch_5
        0x7f0900fb -> :sswitch_4
        0x7f090149 -> :sswitch_6
        0x7f090258 -> :sswitch_3
        0x7f09027f -> :sswitch_2
        0x7f0902d0 -> :sswitch_1
        0x7f0902e3 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 113
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 114
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b002a

    .line 115
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->setContentView(I)V

    const v0, 0x7f090223

    .line 117
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d00b0

    .line 118
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0900fb

    .line 120
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    sub-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    .line 127
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide v3, 0x9a7ec800L

    sub-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mStartDate:Ljava/util/Date;

    .line 128
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const v1, 0x7f0902e3

    .line 130
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 131
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v2, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mStartDate:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->updateEndDate(Z)V

    const v0, 0x7f090066

    .line 136
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902d0

    .line 139
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 140
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setFlags(I)V

    .line 142
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 144
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/baoxian/LLTBaoXian;->initBaoxian(Landroid/content/Context;)Z

    const v0, 0x7f090258

    .line 146
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 147
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setFlags(I)V

    .line 150
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 152
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    const-string v1, "auto_query"

    .line 154
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->onQuery()V

    :cond_0
    const p1, 0x7f0901ba

    .line 159
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    .line 160
    new-instance v0, Lcom/lltskb/lltskb/order/-$$Lambda$CompleteOrderActivity$SuHBWmIRuOH4EONVuajd-8YE9dE;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$CompleteOrderActivity$SuHBWmIRuOH4EONVuajd-8YE9dE;-><init>(Lcom/lltskb/lltskb/order/CompleteOrderActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const-string v0, "fragment_myorder_complete_detail"

    .line 266
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->dismissFragment(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "fragment_myorder_query"

    .line 270
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->dismissFragment(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 275
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected showDateDialog()V
    .locals 8

    .line 183
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 184
    iget v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateType:I

    if-nez v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mStartDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mEndDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 190
    :goto_0
    new-instance v1, Landroid/app/DatePickerDialog;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/CompleteOrderActivity;->mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/4 v2, 0x5

    .line 191
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v7

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 192
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method
