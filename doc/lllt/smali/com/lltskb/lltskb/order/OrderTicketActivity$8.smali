.class Lcom/lltskb/lltskb/order/OrderTicketActivity$8;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTrainDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 937
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelected(Ljava/lang/String;)V
    .locals 3

    .line 941
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const v1, 0x7f090242

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    .line 944
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 946
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderDate(Ljava/lang/String;)V

    .line 947
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 949
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 951
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    .line 952
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 953
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 954
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$802(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I

    .line 955
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$902(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I

    .line 956
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1002(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 959
    invoke-virtual {p1}, Ljava/text/ParseException;->printStackTrace()V

    :goto_0
    return-void
.end method
