.class Lcom/lltskb/lltskb/order/RegistUserActivity$4;
.super Landroid/os/AsyncTask;
.source "RegistUserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/RegistUserActivity;->onNext()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 484
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 506
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$400(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->checkUserName(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    .line 507
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 509
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v0}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$400(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$500(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v2}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$000(Lcom/lltskb/lltskb/order/RegistUserActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v3}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$600(Lcom/lltskb/lltskb/order/RegistUserActivity;)Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getRandCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)I

    move-result p1

    if-eqz p1, :cond_1

    .line 510
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 514
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 515
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 484
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 494
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 495
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_0

    .line 497
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    const/4 v1, 0x0

    const-string v2, "\u9519\u8bef"

    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 500
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/RegistUserActivity;->access$300(Lcom/lltskb/lltskb/order/RegistUserActivity;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 488
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 489
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$4;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    const v1, 0x7f0d01a6

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
