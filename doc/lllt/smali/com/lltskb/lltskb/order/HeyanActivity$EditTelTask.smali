.class Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;
.super Landroid/os/AsyncTask;
.source "HeyanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/HeyanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditTelTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mobile:Ljava/lang/String;

.field private pass:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/HeyanActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/HeyanActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 411
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 412
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    .line 413
    iput-object p2, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->mobile:Ljava/lang/String;

    .line 414
    iput-object p3, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->pass:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPostExecute$1(Landroid/view/View;)V
    .locals 0

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 407
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 456
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->mobile:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->pass:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->editTel(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    .line 457
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 460
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 461
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$HeyanActivity$EditTelTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 427
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 407
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 433
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 434
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 436
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 442
    invoke-static {v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$700(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    goto :goto_0

    :cond_1
    const-string v1, "\u7528\u6237\u672a\u767b\u5f55"

    .line 443
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 444
    invoke-static {v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$300(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0d010f

    .line 447
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$EditTelTask$B_iqXGNAx2CEOnBJU58Ukois9rc;->INSTANCE:Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$EditTelTask$B_iqXGNAx2CEOnBJU58Ukois9rc;

    .line 446
    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 419
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 420
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d01a6

    const/4 v2, -0x1

    .line 425
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$EditTelTask$mjy8d7JLiISowGWrHNC16vdugog;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$EditTelTask$mjy8d7JLiISowGWrHNC16vdugog;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity$EditTelTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
