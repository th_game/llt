.class Lcom/lltskb/lltskb/order/RegistUserActivity$6;
.super Landroid/os/AsyncTask;
.source "RegistUserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/RegistUserActivity;->subDetail(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

.field final synthetic val$randCode:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/RegistUserActivity;Ljava/lang/String;)V
    .locals 0

    .line 549
    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    iput-object p2, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->val$randCode:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 549
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 578
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->val$randCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->subDetail(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    .line 579
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->get()Lcom/lltskb/lltskb/engine/online/RegistUserModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/RegistUserModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 583
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 584
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 549
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 559
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 560
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_0

    .line 562
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    const/4 v1, 0x0

    const-string v2, "\u9519\u8bef"

    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 565
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    new-instance v0, Lcom/lltskb/lltskb/order/RegistUserActivity$6$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$6$1;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity$6;)V

    const-string v1, "\u63d0\u793a"

    const-string v2, "\u60a8\u5df2\u5b8c\u6210\u6ce8\u518c\uff0c12306\u7f51\u7ad9\u5c06\u572824\u5c0f\u65f6\u5185\u5bf9\u60a8\u7684\u8eab\u4efd\u4fe1\u606f\u8fdb\u884c\u6838\u9a8c\uff0c\u901a\u8fc7\u540e\u5373\u53ef\u8d2d\u7968\u3002"

    invoke-static {p1, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 553
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 554
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$6;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    const v1, 0x7f0d01a6

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
