.class public Lcom/lltskb/lltskb/order/LoginActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;,
        Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;,
        Lcom/lltskb/lltskb/order/LoginActivity$CheckUserTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LoginActivity"


# instance fields
.field private mIsLoginOnline:Z

.field private mLoginInit:Z

.field private mRandCode:Ljava/lang/String;

.field private mRememberName:Landroid/widget/CheckBox;

.field private mRememberPass:Landroid/widget/CheckBox;

.field private mUserName:Landroid/widget/AutoCompleteTextView;

.field private mUserPass:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 64
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mIsLoginOnline:Z

    .line 80
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mLoginInit:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/LoginActivity;)Landroid/widget/EditText;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/LoginActivity;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->onLoginSuccess()V

    return-void
.end method

.method static synthetic access$302(Lcom/lltskb/lltskb/order/LoginActivity;Z)Z
    .locals 0

    .line 49
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mLoginInit:Z

    return p1
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/LoginActivity;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->login()V

    return-void
.end method

.method private checkUser()V
    .locals 5

    const-string v0, "LoginActivity"

    const-string v1, "checkUser"

    .line 225
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Lcom/lltskb/lltskb/order/LoginActivity$CheckUserTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/LoginActivity$CheckUserTask;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    .line 228
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/LoginActivity$CheckUserTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private clearUser()V
    .locals 3

    const-string v0, "LoginActivity"

    const-string v1, "clearuser"

    .line 233
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0d00aa

    .line 234
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d0173

    .line 235
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/order/LoginActivity$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/LoginActivity$1;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-static {p0, v1, v0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method private initLogin()V
    .locals 5

    .line 316
    new-instance v0, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    .line 318
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/LoginActivity$InitLoginTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private initUser()V
    .locals 4

    const-string v0, "LoginActivity"

    const-string v1, "initUser"

    .line 256
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 258
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    .line 259
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 260
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUser(I)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 261
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v2

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    .line 267
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 268
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberPass()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 272
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 275
    :goto_2
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberPass()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 276
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberName:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_3
    return-void
.end method

.method private initView()V
    .locals 5

    const-string v0, "LoginActivity"

    const-string v1, "initView"

    .line 94
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 95
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->requestWindowFeature(I)Z

    const v1, 0x7f0b0051

    .line 97
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/LoginActivity;->setContentView(I)V

    const/4 v1, 0x0

    .line 99
    iput-boolean v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mIsLoginOnline:Z

    .line 100
    iput-boolean v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mLoginInit:Z

    .line 101
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/lltskb/lltskb/utils/JSEngine;->setContext(Landroid/content/Context;)V

    .line 102
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/utils/JSEngine;->setSignValue(Ljava/lang/String;)V

    const v2, 0x7f090304

    .line 104
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    iput-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    .line 105
    iget-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v2, 0x7f0900c4

    .line 106
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    .line 107
    iget-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    const v2, 0x7f09008b

    .line 108
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    const v2, 0x7f09008a

    .line 109
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberName:Landroid/widget/CheckBox;

    const v2, 0x7f090087

    .line 110
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 111
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 112
    iget-object v3, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberName:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 113
    iget-object v3, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 115
    iget-object v3, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberName:Landroid/widget/CheckBox;

    new-instance v4, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$ZmOxe7OR7N4PNsCO5Weqh3aSTbE;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$ZmOxe7OR7N4PNsCO5Weqh3aSTbE;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const-string v3, "uamtk"

    .line 122
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v3

    xor-int/2addr v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    sget-object v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$M79F_1qBV12jXEvdcH0QvyXpIyY;->INSTANCE:Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$M79F_1qBV12jXEvdcH0QvyXpIyY;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v2, 0x7f090223

    .line 129
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0d01be

    .line 130
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 132
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->updateMaxDateInfo()V

    const v2, 0x7f0900fb

    .line 134
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 135
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$292aQYcBFAbddQB0YvvqjeL0Trg;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$292aQYcBFAbddQB0YvvqjeL0Trg;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0902cf

    .line 142
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 143
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$D_XEk1ITRKMCG6hVxht7PwfYeO0;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$D_XEk1ITRKMCG6hVxht7PwfYeO0;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setFlags(I)V

    .line 145
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 147
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setFlags(I)V

    .line 148
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    const v2, 0x7f0902b0

    .line 150
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 151
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$3v-TXEGWMnyGVFKX0-Z2Ygy8COY;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$3v-TXEGWMnyGVFKX0-Z2Ygy8COY;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setFlags(I)V

    .line 156
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    const v2, 0x7f090286

    .line 159
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 160
    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$_LBriSAP4ON6RNRu67u0Q3FoZHk;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$_LBriSAP4ON6RNRu67u0Q3FoZHk;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setFlags(I)V

    .line 162
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    const v0, 0x7f09004a

    .line 164
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v2, 0x7f0d00a8

    .line 165
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 166
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 167
    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    const/16 v2, 0x46

    .line 169
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 170
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$eEgaoW4oymHcc3f-ajoZYbfFy3A;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$eEgaoW4oymHcc3f-ajoZYbfFy3A;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09005c

    .line 174
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 175
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$dxjcw4-X3KkENjfUH_n1RuyqX3w;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$dxjcw4-X3KkENjfUH_n1RuyqX3w;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006b

    .line 177
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 178
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$KYY8QqZaHKNjtcfZ-U4t8FcRGjI;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$KYY8QqZaHKNjtcfZ-U4t8FcRGjI;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->initUser()V

    .line 184
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    .line 186
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    const/4 v1, 0x6

    if-ge v0, v1, :cond_2

    .line 188
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$JJao_DJRK8Nq3K6XgXV2MWv6Th0;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$JJao_DJRK8Nq3K6XgXV2MWv6Th0;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method static synthetic lambda$initView$1(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 124
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "use uamtk oauth ="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    xor-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "LoginActivity"

    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    xor-int/lit8 p0, p1, 0x1

    .line 125
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    const-string p1, "uamtk"

    invoke-static {p1, p0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->setFeatureEnabled(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method private login()V
    .locals 3

    .line 297
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->clearCookie()V

    .line 298
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 301
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 305
    :cond_0
    iget-boolean v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mLoginInit:Z

    if-nez v0, :cond_1

    .line 306
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->initLogin()V

    goto :goto_0

    .line 308
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->showPassCodeDialog()V

    :goto_0
    return-void

    :cond_2
    :goto_1
    const v0, 0x7f0d010f

    .line 302
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d01d2

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private onLoginSuccess()V
    .locals 3

    const-string v0, "LoginActivity"

    const-string v1, "onLoginSuccess"

    .line 281
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->save()V

    const v0, 0x7f0d01c0

    .line 283
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 285
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.lltskb.lltskb.order.login.result"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login.result"

    const/4 v2, 0x0

    .line 286
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 287
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 288
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->reset()V

    .line 290
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->finish()V

    return-void
.end method

.method private performLogin()V
    .locals 9

    const-string v0, "LoginActivity"

    const-string v1, "login"

    .line 325
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 328
    iget-object v1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserPass:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 329
    iget-object v2, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRandCode:Ljava/lang/String;

    .line 330
    iget-object v3, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    const-string v4, "1"

    const-string v5, "0"

    if-eqz v3, :cond_0

    move-object v3, v4

    goto :goto_0

    :cond_0
    move-object v3, v5

    .line 331
    :goto_0
    iget-object v6, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberName:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    move-object v4, v5

    .line 333
    :goto_1
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_2

    .line 338
    :cond_2
    new-instance v5, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    .line 340
    sget-object v6, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    aput-object v1, v7, v0

    const/4 v0, 0x2

    aput-object v2, v7, v0

    const/4 v0, 0x3

    aput-object v4, v7, v0

    const/4 v0, 0x4

    aput-object v3, v7, v0

    invoke-virtual {v5, v6, v7}, Lcom/lltskb/lltskb/order/LoginActivity$LoginTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_3
    :goto_2
    const v0, 0x7f0d010f

    .line 334
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d01d2

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private showPassCodeDialog()V
    .locals 3

    .line 348
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 352
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 353
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 358
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/view/PassCodeDialog;

    const v1, 0x7f0e00b3

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/lltskb/lltskb/view/PassCodeDialog;-><init>(Landroid/content/Context;II)V

    .line 360
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$obLovq2TXrRnI08DBXem4lAwSnY;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$obLovq2TXrRnI08DBXem4lAwSnY;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/PassCodeDialog;->setListener(Lcom/lltskb/lltskb/view/PassCodeDialog$Listener;)V

    .line 364
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->show()V

    .line 365
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/PassCodeDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    const v1, 0x7f0e00bd

    .line 367
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private showRegist()V
    .locals 2

    .line 215
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 216
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private updateMaxDateInfo()V
    .locals 10

    .line 194
    sget-object v0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mStudentMaxDate:Ljava/lang/String;

    .line 195
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxStudentDays()J

    move-result-wide v0

    long-to-int v1, v0

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->getDate(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mOtherMaxdate:Ljava/lang/String;

    .line 199
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 200
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxOtherDays()J

    move-result-wide v1

    long-to-int v2, v1

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->getDate(I)Ljava/lang/String;

    move-result-object v1

    .line 203
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxOtherDays()J

    move-result-wide v2

    long-to-int v3, v2

    const/4 v2, 0x2

    sub-int/2addr v3, v2

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->getDate(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f090293

    .line 205
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/order/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0d0202

    .line 206
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/order/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 207
    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {}, Lcom/lltskb/lltskb/utils/StringUtils;->getTodayShort()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v1, v7, v8

    aput-object v3, v7, v2

    const/4 v1, 0x3

    aput-object v0, v7, v1

    invoke-static {v6, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$LoginActivity(Landroid/widget/CompoundButton;Z)V
    .locals 1

    if-nez p2, :cond_0

    .line 117
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRememberPass:Landroid/widget/CheckBox;

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$initView$2$LoginActivity(Landroid/view/View;)V
    .locals 2

    .line 136
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->finish()V

    .line 137
    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.lltskb.lltskb.order.login.result"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "login.result"

    const/4 v1, -0x3

    .line 138
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 139
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method

.method public synthetic lambda$initView$3$LoginActivity(Landroid/view/View;)V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->showRegist()V

    return-void
.end method

.method public synthetic lambda$initView$4$LoginActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 152
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mIsLoginOnline:Z

    const-string p1, "https://kyfw.12306.cn/otn/login/init"

    .line 153
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$5$LoginActivity(Landroid/view/View;)V
    .locals 0

    const-string p1, "https://kyfw.12306.cn/otn/forgetPassword/initforgetMyPassword"

    .line 160
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$6$LoginActivity(Landroid/view/View;)V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->clearUser()V

    return-void
.end method

.method public synthetic lambda$initView$7$LoginActivity(Landroid/view/View;)V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->login()V

    return-void
.end method

.method public synthetic lambda$initView$8$LoginActivity(Landroid/view/View;)V
    .locals 0

    .line 178
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->showSelectUserDialog()V

    return-void
.end method

.method public synthetic lambda$initView$9$LoginActivity()V
    .locals 2

    const v0, 0x7f0d02de

    const v1, 0x7f0d0299

    .line 188
    invoke-static {p0, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method public synthetic lambda$showPassCodeDialog$10$LoginActivity(Ljava/lang/String;)V
    .locals 0

    .line 361
    iput-object p1, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mRandCode:Ljava/lang/String;

    .line 362
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->performLogin()V

    return-void
.end method

.method public synthetic lambda$showSelectUserDialog$11$LoginActivity([Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 405
    aget-object p1, p1, p3

    .line 406
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p3

    invoke-virtual {p3, p1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    .line 407
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->initUser()V

    .line 408
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 84
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "LoginActivity"

    const-string v0, "onCreate"

    .line 85
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->initView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.lltskb.lltskb.order.login.result"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, -0x4

    const-string v2, "login.result"

    .line 69
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 70
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 72
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onResume()V
    .locals 1

    .line 54
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onResume()V

    .line 56
    iget-boolean v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mIsLoginOnline:Z

    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/LoginActivity;->checkUser()V

    :cond_0
    const/4 v0, 0x0

    .line 60
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mIsLoginOnline:Z

    return-void
.end method

.method public showSelectUserDialog()V
    .locals 6

    const-string v0, "LoginActivity"

    const-string v1, "showSelectUserDialog"

    .line 375
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 380
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 381
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUser(I)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v3

    .line 382
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const v1, 0x7f0d025b

    if-nez v2, :cond_2

    .line 388
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d01e0

    .line 389
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d01f4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    return-void

    .line 392
    :cond_2
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 395
    :goto_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 396
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUser(I)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v4

    .line 397
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v5

    if-nez v5, :cond_3

    goto :goto_3

    .line 398
    :cond_3
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    .line 399
    iget-object v5, p0, Lcom/lltskb/lltskb/order/LoginActivity;->mUserName:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v5}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v3, v0

    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 403
    :cond_5
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$66s3FFVUfKHO5incic0IFjdE740;

    invoke-direct {v4, p0, v2}, Lcom/lltskb/lltskb/order/-$$Lambda$LoginActivity$66s3FFVUfKHO5incic0IFjdE740;-><init>(Lcom/lltskb/lltskb/order/LoginActivity;[Ljava/lang/String;)V

    .line 404
    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x108000a

    .line 410
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 411
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 414
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void
.end method
