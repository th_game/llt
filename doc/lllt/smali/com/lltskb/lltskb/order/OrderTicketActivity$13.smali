.class Lcom/lltskb/lltskb/order/OrderTicketActivity$13;
.super Ljava/lang/Object;
.source "OrderTicketActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTrainTypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

.field final synthetic val$trainType:[Ljava/lang/String;

.field final synthetic val$trainTypeChecked:[Z


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;[Z)V
    .locals 0

    .line 1129
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    iput-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->val$trainType:[Ljava/lang/String;

    iput-object p3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->val$trainTypeChecked:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1133
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 p2, 0x1

    .line 1134
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->val$trainType:[Ljava/lang/String;

    array-length v1, v0

    if-ge p2, v1, :cond_1

    .line 1135
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->val$trainTypeChecked:[Z

    aget-boolean v1, v1, p2

    if-eqz v1, :cond_0

    .line 1136
    aget-object v0, v0, p2

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1139
    :cond_1
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->val$trainTypeChecked:[Z

    const/4 v0, 0x0

    aput-boolean v0, p2, v0

    .line 1140
    iget-object p2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1141
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1142
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;->this$0:Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-static {p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->access$1900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method
