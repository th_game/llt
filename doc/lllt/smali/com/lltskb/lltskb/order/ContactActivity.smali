.class public Lcom/lltskb/lltskb/order/ContactActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "ContactActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactActivity"


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mAutoSignIn:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x1

    .line 51
    iput-boolean v0, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAutoSignIn:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/ContactActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->syncContact()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/ContactActivity;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/ContactActivity;)Z
    .locals 0

    .line 40
    iget-boolean p0, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAutoSignIn:Z

    return p0
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/order/ContactActivity;Z)Z
    .locals 0

    .line 40
    iput-boolean p1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAutoSignIn:Z

    return p1
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/ContactActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->startLogin()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/ContactActivity;)Landroid/widget/BaseAdapter;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAdapter:Landroid/widget/BaseAdapter;

    return-object p0
.end method

.method private addContact()V
    .locals 2

    const-string v0, "ContactActivity"

    const-string v1, "addContact"

    .line 174
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/EditContactActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    .line 177
    invoke-virtual {p0, v0, v1}, Lcom/lltskb/lltskb/order/ContactActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private initView()V
    .locals 2

    const-string v0, "ContactActivity"

    const-string v1, "initView"

    .line 76
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 77
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b0078

    .line 78
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->setContentView(I)V

    const v0, 0x7f090223

    .line 79
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d00ba

    .line 80
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0900fb

    .line 82
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 83
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090076

    .line 85
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 86
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090041

    .line 88
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 89
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090181

    .line 91
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 92
    new-instance v1, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/adapters/PassengerListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAdapter:Landroid/widget/BaseAdapter;

    .line 93
    iget-object v1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$9wthi2ACQ-AemAbVInqXh7_KJ9I;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$9wthi2ACQ-AemAbVInqXh7_KJ9I;-><init>(Lcom/lltskb/lltskb/order/ContactActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f090120

    .line 97
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 98
    invoke-static {}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isBaoxianEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 99
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$BGumBXHgxm2GXKTfD-g9-HqNLsM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$ContactActivity$BGumBXHgxm2GXKTfD-g9-HqNLsM;-><init>(Lcom/lltskb/lltskb/order/ContactActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private onEditItem(I)V
    .locals 2

    const-string v0, "ContactActivity"

    const-string v1, "onEditItem"

    .line 131
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/EditContactActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "passenger_index"

    .line 133
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x0

    .line 134
    invoke-virtual {p0, v0, p1}, Lcom/lltskb/lltskb/order/ContactActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private register()V
    .locals 3

    const-string v0, "ContactActivity"

    const-string v1, "register"

    .line 138
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 140
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    new-instance v1, Lcom/lltskb/lltskb/order/ContactActivity$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/ContactActivity$1;-><init>(Lcom/lltskb/lltskb/order/ContactActivity;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 155
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    .line 156
    iget-object v2, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private showBaoxianFragment()V
    .locals 4

    const-string v0, "kuntao_h5"

    .line 103
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->showXindanH5()V

    return-void

    .line 107
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 113
    new-instance v2, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;-><init>()V

    .line 114
    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;->setPassengers(Ljava/util/Vector;)V

    const v0, 0x7f010013

    const v3, 0x7f010014

    .line 115
    invoke-virtual {v1, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    const v0, 0x7f0900de

    .line 116
    const-class v3, Lcom/lltskb/lltskb/fragment/DrawBaoxianFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 118
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_2
    :goto_0
    return-void
.end method

.method private showXindanH5()V
    .locals 3

    .line 123
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/view/WebBrowser;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "web_url"

    const-string v2, "http://wap.lltskb.com/ktzx.html"

    .line 124
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "web_post"

    .line 125
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "web_closeonback"

    .line 126
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/ContactActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startLogin()V
    .locals 2

    const-string v0, "ContactActivity"

    const-string v1, "startLogin"

    .line 160
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->register()V

    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private syncContact()V
    .locals 5

    const-string v0, "ContactActivity"

    const-string v1, "syncContact"

    .line 167
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;-><init>(Lcom/lltskb/lltskb/order/ContactActivity;)V

    .line 170
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/order/ContactActivity$SyncContactTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$ContactActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 95
    invoke-direct {p0, p3}, Lcom/lltskb/lltskb/order/ContactActivity;->onEditItem(I)V

    return-void
.end method

.method public synthetic lambda$initView$1$ContactActivity(Landroid/view/View;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->showBaoxianFragment()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/lltskb/lltskb/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->syncContact()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f090041

    const/4 v1, 0x1

    if-eq p1, v0, :cond_2

    const v0, 0x7f090076

    if-eq p1, v0, :cond_1

    const v0, 0x7f0900fb

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->finish()V

    goto :goto_0

    .line 187
    :cond_1
    iput-boolean v1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAutoSignIn:Z

    .line 188
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->syncContact()V

    goto :goto_0

    .line 191
    :cond_2
    iput-boolean v1, p0, Lcom/lltskb/lltskb/order/ContactActivity;->mAutoSignIn:Z

    .line 192
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->addContact()V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "ContactActivity"

    const-string v1, "onCreate"

    .line 64
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->initView()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    .line 71
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onStart()V

    .line 72
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/ContactActivity;->syncContact()V

    return-void
.end method
