.class Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;
.super Landroid/os/AsyncTask;
.source "HeyanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/order/HeyanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CheckUserTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/order/HeyanActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/HeyanActivity;)V
    .locals 1

    .line 248
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 249
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 246
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 283
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "1"

    return-object p1

    :cond_0
    const-string p1, "0"

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$0$HeyanActivity$CheckUserTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 260
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 246
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 266
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 267
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 269
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "1"

    .line 274
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 275
    invoke-static {v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$100(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    goto :goto_0

    .line 277
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/order/HeyanActivity;->access$300(Lcom/lltskb/lltskb/order/HeyanActivity;)V

    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 254
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 256
    iget-object v0, p0, Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/order/HeyanActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d009c

    .line 260
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/order/HeyanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckUserTask$4sEAwT5U1TBVlPs593KnsTjf6gw;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$HeyanActivity$CheckUserTask$4sEAwT5U1TBVlPs593KnsTjf6gw;-><init>(Lcom/lltskb/lltskb/order/HeyanActivity$CheckUserTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
