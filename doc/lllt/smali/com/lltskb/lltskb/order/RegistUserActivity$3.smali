.class Lcom/lltskb/lltskb/order/RegistUserActivity$3;
.super Landroid/os/AsyncTask;
.source "RegistUserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/order/RegistUserActivity;->startInitSchoolTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/order/RegistUserActivity;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$3;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 409
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$3;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 437
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getAllCitys()V

    .line 438
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->getAllSchools()V
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    .line 441
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 442
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 409
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/RegistUserActivity$3;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1

    .line 426
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 427
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    if-eqz p1, :cond_0

    .line 429
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$3;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 413
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 414
    iget-object v0, p0, Lcom/lltskb/lltskb/order/RegistUserActivity$3;->this$0:Lcom/lltskb/lltskb/order/RegistUserActivity;

    new-instance v1, Lcom/lltskb/lltskb/order/RegistUserActivity$3$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/RegistUserActivity$3$1;-><init>(Lcom/lltskb/lltskb/order/RegistUserActivity$3;)V

    const-string v2, "\u6b63\u5728\u521d\u59cb\u5316"

    const/high16 v3, -0x1000000

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
