.class public Lcom/lltskb/lltskb/order/OrderTicketActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "OrderTicketActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;


# static fields
.field private static final TAG:Ljava/lang/String; = "OrderTicketActivity"


# instance fields
.field private mAlertType:Landroid/widget/TextView;

.field private mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

.field private mDay:I

.field private mFromStation:Landroid/widget/AutoCompleteTextView;

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMonth:I

.field private mOldValume:I

.field private mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

.field private mOrderFromFlag:I

.field private mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

.field private mOrderUser:Landroid/widget/TextView;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mQueryFreq:Landroid/widget/TextView;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSeatType:Landroid/widget/TextView;

.field private mSelectAlertDialog:Landroid/support/v7/app/AppCompatDialog;

.field private mSelectSeatDialog:Landroid/support/v7/app/AppCompatDialog;

.field private mSelectTimeDialog:Landroid/support/v7/app/AppCompatDialog;

.field private mSelectTrainTypeDialog:Landroid/support/v7/app/AppCompatDialog;

.field private mStartDate:Landroid/widget/TextView;

.field private mTicketTask:Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

.field private mToStation:Landroid/widget/AutoCompleteTextView;

.field private mTrainNo:Landroid/widget/TextView;

.field private mTrainTime:Landroid/widget/TextView;

.field private mTrainType:Landroid/widget/TextView;

.field private mVibrator:Landroid/os/Vibrator;

.field private mYear:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 521
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mVibrator:Landroid/os/Vibrator;

    const/4 v0, 0x0

    .line 112
    iput v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderFromFlag:I

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initTitle()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initOrderConfig()V

    return-void
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I
    .locals 0

    .line 86
    iget p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    return p0
.end method

.method static synthetic access$1002(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I
    .locals 0

    .line 86
    iput p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    return p1
.end method

.method static synthetic access$1100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->onUserSelected()V

    return-void
.end method

.method static synthetic access$1400(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTimeDialog:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->resetOrderTrainNo()V

    return-void
.end method

.method static synthetic access$1700(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mQueryFreq:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTrainTypeDialog:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2100(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->hideNotify()V

    return-void
.end method

.method static synthetic access$2200(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->prepareOrder()V

    return-void
.end method

.method static synthetic access$2300(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/widget/TextView;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2400(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/support/v7/app/AppCompatDialog;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectAlertDialog:Landroid/support/v7/app/AppCompatDialog;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->stopAlertSound()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->stopVibrate()V

    return-void
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTicketTask:Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/order/OrderTicketActivity;)Landroid/app/ProgressDialog;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updateBgTask()V

    return-void
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I
    .locals 0

    .line 86
    iget p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    return p0
.end method

.method static synthetic access$802(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I
    .locals 0

    .line 86
    iput p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    return p1
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/order/OrderTicketActivity;)I
    .locals 0

    .line 86
    iget p0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    return p0
.end method

.method static synthetic access$902(Lcom/lltskb/lltskb/order/OrderTicketActivity;I)I
    .locals 0

    .line 86
    iput p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    return p1
.end method

.method private beginVibrate()V
    .locals 3

    .line 1550
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    const-string v0, "vibrator"

    .line 1551
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mVibrator:Landroid/os/Vibrator;

    .line 1553
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1

    const/4 v1, 0x5

    new-array v1, v1, [J

    .line 1554
    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    :cond_1
    return-void

    nop

    :array_0
    .array-data 8
        0x3e8
        0x96
        0x96
        0x12c
        0x32
    .end array-data
.end method

.method private checkLoginStatus()V
    .locals 5

    const-string v0, "OrderTicketActivity"

    const-string v1, "checkLoginStatus"

    .line 885
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$7;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;)V

    .line 904
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getConfigInfo()Z
    .locals 8

    const-string v0, "OrderTicketActivity"

    const-string v1, "getConfigInfo"

    .line 1238
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v0, :cond_0

    .line 1241
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 1243
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1244
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "\u63d0\u793a"

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    const-string v0, "\u8bf7\u8f93\u5165\u59cb\u53d1\u5730\uff01"

    .line 1245
    invoke-static {p0, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 1246
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    return v4

    .line 1250
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setFromStationName(Ljava/lang/String;)V

    .line 1251
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setFromStation(Ljava/lang/String;)V

    .line 1253
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1254
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "\u8bf7\u8f93\u5165\u76ee\u7684\u5730\uff01"

    .line 1255
    invoke-static {p0, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 1256
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    return v4

    .line 1260
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setToStationName(Ljava/lang/String;)V

    .line 1261
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setToStation(Ljava/lang/String;)V

    .line 1263
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1264
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v5, 0xa

    if-ge v1, v5, :cond_3

    goto/16 :goto_2

    .line 1268
    :cond_3
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1269
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->afterToday(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v0, "\u4e58\u8f66\u65e5\u671f\u4e0d\u6b63\u786e\uff01"

    .line 1270
    invoke-static {p0, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v4

    .line 1273
    :cond_4
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderDate(Ljava/lang/String;)V

    .line 1274
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1275
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "\u8bf7\u9009\u62e9\u51fa\u53d1\u65f6\u95f4\uff01"

    .line 1276
    invoke-static {p0, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v4

    .line 1279
    :cond_5
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderTime(Ljava/lang/String;)V

    .line 1280
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1281
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v0, 0x7f0d0173

    const v1, 0x7f0d0186

    .line 1282
    invoke-static {p0, v0, v1, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v4

    :cond_6
    const-string v1, "\u5168\u90e8"

    .line 1287
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_7

    .line 1288
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "QB"

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainClass([Ljava/lang/String;)V

    goto :goto_1

    .line 1290
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    return v4

    .line 1293
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1294
    array-length v5, v0

    :goto_0
    if-ge v4, v5, :cond_9

    aget-object v6, v0, v4

    .line 1295
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getTrainTypeMap()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1297
    :cond_9
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainClass([Ljava/lang/String;)V

    :goto_1
    return v1

    :cond_a
    :goto_2
    const-string v0, "\u8bf7\u9009\u62e9\u4e58\u8f66\u65e5\u671f\uff01"

    .line 1265
    invoke-static {p0, v2, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v4
.end method

.method private getMaxDays()J
    .locals 2

    .line 825
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->isStudentPurpose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxStudentDays()J

    move-result-wide v0

    return-wide v0

    .line 828
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getMaxOtherDays()J

    move-result-wide v0

    return-wide v0
.end method

.method private hideNotify()V
    .locals 2

    const-string v0, "notification"

    .line 1490
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 1492
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_0
    return-void
.end method

.method private initFromIntent()Z
    .locals 11

    const-string v0, "OrderTicketActivity"

    const-string v1, "initFromIntent"

    .line 253
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_13

    .line 255
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v2, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v2, "order_from_station"

    .line 258
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "order_to_station"

    .line 259
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "order_depart_date"

    .line 260
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "order_train_code"

    .line 261
    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "order_seat_type"

    .line 262
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "tour_flag"

    .line 263
    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 264
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v8

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/LltSettings;->getPurpose()Ljava/lang/String;

    move-result-object v8

    const-string v9, "order_from_flag"

    .line 265
    invoke-virtual {v0, v9, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderFromFlag:I

    .line 268
    iget v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderFromFlag:I

    const/4 v9, 0x4

    if-ne v0, v9, :cond_1

    .line 269
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->getmOrderParameters()Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 270
    iget-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v10, :cond_1

    .line 271
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 275
    :cond_1
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setFromStationName(Ljava/lang/String;)V

    .line 279
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    .line 281
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setToStationName(Ljava/lang/String;)V

    .line 283
    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x8

    if-ge v0, v2, :cond_4

    goto/16 :goto_2

    :cond_4
    const-string v0, "-"

    .line 286
    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 287
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v10, 0x6

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    invoke-virtual {v4, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 290
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderDate(Ljava/lang/String;)V

    .line 292
    invoke-direct {p0, v8}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setPurpose(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 295
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    :cond_6
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 299
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 303
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    :cond_8
    invoke-static {v7}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 307
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v7}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTourFlag(Ljava/lang/String;)V

    .line 310
    :cond_9
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v2, ""

    if-nez v0, :cond_b

    .line 311
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    if-eqz v0, :cond_a

    .line 312
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :cond_a
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v6}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderSeat(Ljava/lang/String;)V

    goto :goto_0

    .line 315
    :cond_b
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    .line 316
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :cond_c
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderSeat(Ljava/lang/String;)V

    .line 320
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    const-string v3, "\u5168\u90e8"

    if-eqz v0, :cond_d

    .line 321
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object v3, v4, v1

    .line 324
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainClass([Ljava/lang/String;)V

    .line 326
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v4, "00:00--24:00"

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderTime(Ljava/lang/String;)V

    .line 328
    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 329
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainNo:Landroid/widget/TextView;

    if-eqz v3, :cond_e

    .line 330
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_e
    const-string v3, "("

    .line 331
    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_f

    .line 333
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 334
    :cond_f
    iget-object v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 335
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    goto :goto_1

    .line 337
    :cond_10
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainNo:Landroid/widget/TextView;

    if-eqz v1, :cond_11

    .line 338
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    :cond_11
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 340
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    .line 343
    :goto_1
    iget v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderFromFlag:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_12

    .line 344
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initTrainInfo()V

    :cond_12
    return v0

    :cond_13
    :goto_2
    return v1
.end method

.method private initOrderConfig()V
    .locals 9

    const-string v0, "OrderTicketActivity"

    const-string v1, "initOrderConfig"

    .line 350
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 354
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v1

    .line 355
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 354
    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderConfigMgr(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->getLastConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v0, :cond_1

    .line 360
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v1, "dc"

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTourFlag(Ljava/lang/String;)V

    .line 365
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initFromIntent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 366
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainNo()Ljava/lang/String;

    move-result-object v1

    .line 368
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 369
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    :cond_2
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 373
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    :cond_3
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 377
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 383
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setPurpose(Ljava/lang/String;)V

    .line 390
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const/4 v2, 0x0

    if-eqz v0, :cond_8

    .line 392
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    array-length v4, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_7

    aget-object v6, v0, v5

    .line 394
    invoke-static {v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getTrainTypeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 396
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 400
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 401
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    :cond_8
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-nez v0, :cond_9

    .line 406
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v0

    .line 407
    iget-object v5, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    new-instance v5, Ljava/text/SimpleDateFormat;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "yyyy-MM-dd"

    invoke-direct {v5, v7, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 410
    :try_start_0
    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 411
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 412
    invoke-virtual {v5, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 413
    invoke-virtual {v5, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    .line 414
    invoke-virtual {v5, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    const/4 v0, 0x5

    .line 415
    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 418
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 422
    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 423
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainNo:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    :cond_a
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object v0

    .line 427
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 428
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 430
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 431
    array-length v6, v0

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v6, :cond_c

    aget-object v8, v0, v7

    .line 432
    invoke-static {v8}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 434
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 437
    :cond_c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 438
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    :cond_d
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 444
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderUser:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_f

    .line 449
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    const v1, 0x7f0d027b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 450
    :cond_f
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_10

    .line 451
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    const v1, 0x7f0d02da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 452
    :cond_10
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_11

    .line 453
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    const v1, 0x7f0d0276

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_11
    :goto_3
    const v0, 0x7f0d022a

    .line 456
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 457
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mQueryFreq:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    iget v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderFromFlag:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_12

    .line 461
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->getmOrderParameters()Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 462
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v1, :cond_12

    .line 463
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 464
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->onTicketSearched(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V

    :cond_12
    return-void
.end method

.method private initTitle()V
    .locals 4

    const-string v0, "OrderTicketActivity"

    const-string v1, "initTitle"

    .line 871
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090223

    .line 872
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d0075

    .line 876
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 877
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 878
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 879
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 881
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private initTrainInfo()V
    .locals 9

    const v0, 0x7f090105

    .line 156
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v0, 0x7f09015a

    .line 158
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v0, 0x7f090273

    .line 161
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 163
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const v0, 0x7f090287

    .line 166
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 168
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v0, 0x7f0902f7

    .line 171
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 173
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const v0, 0x7f0902fe

    .line 176
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 178
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const v0, 0x7f0902d7

    .line 181
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_6

    .line 183
    iget-object v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_6
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_7

    return-void

    :cond_7
    const-string v4, "order_seat_price"

    .line 190
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 193
    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 194
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    const v6, 0x7f0d0128

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 195
    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v1

    const/4 v1, 0x1

    aput-object v4, v7, v1

    invoke-static {v6, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    const-string v0, "order_start_time"

    .line 198
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0902e5

    .line 200
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_9

    .line 202
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    const-string v0, "order_arrive_time"

    .line 205
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f09024d

    .line 207
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_a

    .line 209
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    const-string v0, "order_duration"

    .line 212
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f09027a

    .line 213
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_b

    .line 215
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    const v0, 0x7f09015f

    .line 219
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 220
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    const v0, 0x7f090166

    .line 221
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 222
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_d
    const v0, 0x7f09012b

    .line 223
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 224
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_e
    const v0, 0x7f090162

    .line 225
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 226
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_f
    return-void
.end method

.method private initView()V
    .locals 8

    const-string v0, "OrderTicketActivity"

    const-string v1, "initView"

    .line 526
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 527
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->requestWindowFeature(I)Z

    const v1, 0x7f0b0073

    .line 528
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setContentView(I)V

    const v1, 0x7f0900fb

    .line 530
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 532
    new-instance v2, Lcom/lltskb/lltskb/order/OrderTicketActivity$2;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$2;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v1, 0x7f090045

    .line 549
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 551
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updateBgTask()V

    .line 552
    new-instance v2, Lcom/lltskb/lltskb/order/OrderTicketActivity$3;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$3;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v1, 0x7f090241

    .line 561
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    .line 562
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_2

    .line 563
    new-instance v2, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v2, p0, v1}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_2
    const v1, 0x7f09023d

    .line 566
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    .line 567
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_3

    .line 568
    new-instance v2, Lcom/lltskb/lltskb/order/StationTextWatcher;

    invoke-direct {v2, p0, v1}, Lcom/lltskb/lltskb/order/StationTextWatcher;-><init>(Lcom/lltskb/lltskb/order/StationTextWatcher$Listener;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_3
    const v1, 0x7f090243

    .line 571
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    .line 572
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    const-string v3, "00:00--24:00"

    .line 573
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 574
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_4
    const v1, 0x7f09015f

    .line 577
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 579
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v1, 0x7f09020c

    .line 581
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 583
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    const v1, 0x7f090244

    .line 585
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    .line 586
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    if-eqz v1, :cond_7

    const v3, 0x7f0d004c

    .line 587
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 588
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_7
    const v1, 0x7f090166

    .line 591
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 593
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    const v1, 0x7f090240

    .line 595
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    .line 596
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    if-eqz v1, :cond_9

    .line 597
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_9
    const v1, 0x7f09012b

    .line 598
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 600
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602
    :cond_a
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getLastTakeDate()J

    move-result-wide v3

    .line 603
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v1

    .line 604
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-gez v7, :cond_b

    .line 605
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    mul-int/lit8 v1, v1, 0x18

    mul-int/lit16 v1, v1, 0xe10

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v5, v1

    add-long/2addr v3, v5

    .line 607
    :cond_b
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 608
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 609
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    const/4 v3, 0x2

    .line 610
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iput v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    const/4 v4, 0x5

    .line 611
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    .line 613
    iget v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    iget v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    iget v5, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    invoke-static {p0, v1, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoliday(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v1

    .line 615
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    add-int/2addr v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    iget v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v3

    const-string v0, "%04d-%02d-%02d"

    invoke-static {v4, v0, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 617
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 618
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 621
    :cond_c
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mStartDate:Landroid/widget/TextView;

    if-eqz v1, :cond_d

    .line 622
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    const v0, 0x7f09023f

    .line 624
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    .line 625
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    if-eqz v0, :cond_e

    .line 626
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_e
    const v0, 0x7f090156

    .line 628
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 630
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_f
    const v0, 0x7f090242

    .line 632
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainNo:Landroid/widget/TextView;

    .line 633
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainNo:Landroid/widget/TextView;

    if-eqz v0, :cond_10

    .line 634
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_10
    const v0, 0x7f090162

    .line 636
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 638
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_11
    const v0, 0x7f09023e

    .line 640
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderUser:Landroid/widget/TextView;

    .line 641
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderUser:Landroid/widget/TextView;

    if-eqz v0, :cond_12

    .line 642
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_12
    const v0, 0x7f09014b

    .line 644
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 646
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_13
    const v0, 0x7f09023c

    .line 648
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    .line 649
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    if-eqz v0, :cond_14

    .line 650
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_14
    const v0, 0x7f090111

    .line 652
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 654
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_15
    const v0, 0x7f0902ca

    .line 656
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mQueryFreq:Landroid/widget/TextView;

    .line 657
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mQueryFreq:Landroid/widget/TextView;

    if-eqz v0, :cond_16

    .line 658
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    :cond_16
    const v0, 0x7f090134

    .line 660
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 662
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_17
    const v0, 0x7f090062

    .line 665
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_18

    .line 667
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$I_Y-HlaIDKUyZJAkCPb5pguO2Uk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$I_Y-HlaIDKUyZJAkCPb5pguO2Uk;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_18
    const v0, 0x7f090073

    .line 670
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_19

    .line 672
    new-instance v1, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$Cv92WYtCwwY1gi5LyUtpgyAaElg;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$Cv92WYtCwwY1gi5LyUtpgyAaElg;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_19
    const v0, 0x7f0901b7

    .line 675
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setPurposeChangeListener(I)V

    const v0, 0x7f0901b8

    .line 676
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setPurposeChangeListener(I)V

    return-void
.end method

.method private isStudentPurpose()Z
    .locals 2

    .line 821
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0X00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private onUserSelected()V
    .locals 5

    .line 908
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 909
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 911
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 912
    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 913
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 914
    iget-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-eqz v4, :cond_0

    const-string v4, "[\u7ae5]"

    .line 915
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v4, "-"

    .line 916
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    .line 917
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 919
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderPerson(Ljava/lang/String;)V

    .line 920
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderUser:Landroid/widget/TextView;

    .line 921
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getPersonDisplayText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 922
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderUser:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    return-void
.end method

.method private orderTicket()V
    .locals 8

    const-string v0, "OrderTicketActivity"

    const-string v1, "orderTicket"

    .line 702
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getConfigInfo()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 707
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-nez v1, :cond_1

    return-void

    .line 709
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updatePurpose()V

    .line 711
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v1

    .line 712
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    const v4, 0x7f0d0173

    if-eqz v2, :cond_2

    const v0, 0x7f0d0184

    .line 713
    invoke-static {p0, v4, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    :cond_2
    const-string v2, ","

    const/4 v5, 0x0

    .line 717
    invoke-static {v1, v2, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 718
    array-length v1, v1

    const/4 v6, 0x5

    if-le v1, v6, :cond_3

    const v0, 0x7f0d017d

    .line 719
    invoke-static {p0, v4, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 725
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 726
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x7f0d0185

    .line 727
    invoke-static {p0, v4, v0, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 731
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 732
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 734
    array-length v4, v3

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v4, :cond_5

    aget-object v7, v3, v6

    .line 735
    invoke-static {v7}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 739
    :cond_5
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setOrderSeat(Ljava/lang/String;)V

    .line 741
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 742
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmail(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_6

    .line 743
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setAlertType(Ljava/lang/Integer;)V

    goto :goto_1

    :cond_6
    const v2, 0x7f0d027b

    .line 745
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 746
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setAlertType(Ljava/lang/Integer;)V

    goto :goto_1

    :cond_7
    const v2, 0x7f0d02da

    .line 747
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 748
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setAlertType(Ljava/lang/Integer;)V

    goto :goto_1

    :cond_8
    const v2, 0x7f0d0276

    .line 749
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 750
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setAlertType(Ljava/lang/Integer;)V

    .line 753
    :cond_9
    :goto_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->saveOrderConfig()V

    .line 754
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showOrderProgressDialog()V

    .line 755
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTicketTask:Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    if-eqz v1, :cond_a

    .line 756
    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->cancel(Z)Z

    .line 758
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "orderTicket config ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTicketTask:Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    .line 760
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTicketTask:Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;

    new-array v1, v3, [Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private playAlertSound()V
    .locals 5

    const/4 v0, 0x4

    .line 1580
    :try_start_0
    invoke-static {v0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v1

    .line 1581
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->stopAlertSound()V

    .line 1582
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1583
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p0, v1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    const-string v1, "audio"

    .line 1584
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 1586
    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 1588
    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v4

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    iput v4, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOldValume:I

    if-eqz v1, :cond_2

    .line 1590
    invoke-virtual {v1, v0, v3, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1592
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1593
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 1594
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 1595
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 1597
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    return-void
.end method

.method private prepareOrder()V
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "prepareOrder"

    .line 1481
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1482
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 1484
    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketHelper;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketHelper;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V

    .line 1485
    invoke-virtual {v1}, Lcom/lltskb/lltskb/order/OrderTicketHelper;->submitOrder()V

    return-void
.end method

.method private resetOrderTrainNo()V
    .locals 2

    const v0, 0x7f090242

    .line 1607
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 1609
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1611
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_1

    .line 1612
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 1613
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private saveOrderConfig()V
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "saveOrderConfig"

    .line 473
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 476
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 477
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderConfigMgr(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    move-result-object v0

    .line 478
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->addConfig(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V

    .line 479
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->save()V

    :cond_0
    return-void
.end method

.method private setPurpose(Ljava/lang/String;)V
    .locals 4

    const v0, 0x7f0901b7

    .line 230
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v1, 0x7f0901b8

    .line 231
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 232
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_1

    .line 233
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setPurpose(Ljava/lang/String;)V

    const-string v2, "ADULT"

    .line 235
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz v0, :cond_2

    .line 237
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_2

    .line 241
    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method private setPurposeChangeListener(I)V
    .locals 1

    .line 680
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    if-eqz p1, :cond_0

    .line 683
    new-instance v0, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$UqODGltaCyl8XJKbWXFTxWYGx_Q;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/-$$Lambda$OrderTicketActivity$UqODGltaCyl8XJKbWXFTxWYGx_Q;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-void
.end method

.method private showAlertDialog()V
    .locals 5

    const-string v0, "OrderTicketActivity"

    const-string v1, "showAlertDialog"

    .line 1428
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectAlertDialog:Landroid/support/v7/app/AppCompatDialog;

    if-nez v0, :cond_2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f0d027b

    .line 1430
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f0d02da

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v4, 0x7f0d0276

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 1431
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mAlertType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1433
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1434
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_1

    .line 1435
    aget-object v4, v0, v2

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1440
    :cond_1
    :goto_1
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$21;

    invoke-direct {v4, p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$21;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;)V

    .line 1441
    invoke-virtual {v1, v0, v2, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 1450
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1451
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0183

    .line 1452
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1453
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectAlertDialog:Landroid/support/v7/app/AppCompatDialog;

    .line 1454
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectAlertDialog:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    goto :goto_2

    .line 1456
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    :goto_2
    return-void
.end method

.method private showFreqDialog()V
    .locals 8

    const-string v0, "OrderTicketActivity"

    const-string v1, "showFreqDialog"

    .line 1052
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 1054
    fill-array-data v0, :array_0

    const v1, 0x7f0d022a

    .line 1055
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1056
    array-length v2, v0

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1058
    :goto_0
    array-length v5, v0

    const/4 v6, 0x1

    if-ge v4, v5, :cond_0

    .line 1059
    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v6, v6, [Ljava/lang/Object;

    aget v7, v0, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v1, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1062
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v0, v6

    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 1065
    :cond_1
    new-instance v3, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;

    invoke-direct {v4, p0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity$11;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;Ljava/lang/String;)V

    .line 1066
    invoke-virtual {v3, v2, v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 1076
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1077
    invoke-virtual {v0, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "\u9009\u62e9\u67e5\u8be2\u9891\u7387"

    .line 1078
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1079
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 1080
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method private showNotify(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 6

    const-string v0, "notification"

    .line 1499
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1505
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    .line 1506
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.category.LAUNCHER"

    .line 1507
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    .line 1509
    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1512
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1513
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0800e4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 1514
    invoke-virtual {v3, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object p1

    .line 1516
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, 0x1

    const/16 v4, 0x1a

    if-lt p2, v4, :cond_1

    const/4 p2, 0x2

    .line 1520
    new-instance v4, Landroid/app/NotificationChannel;

    const-string v5, "order_ticket"

    invoke-direct {v4, v5, v5, p2}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 1522
    invoke-virtual {v4, v3}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const/high16 p2, -0x10000

    .line 1523
    invoke-virtual {v4, p2}, Landroid/app/NotificationChannel;->setLightColor(I)V

    if-eqz v0, :cond_0

    .line 1529
    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 1531
    :cond_0
    invoke-virtual {p1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setChannelId(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1533
    :cond_1
    invoke-virtual {p1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1534
    invoke-virtual {p1, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1536
    invoke-virtual {p1, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v0, :cond_2

    .line 1539
    invoke-virtual {p1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1542
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_3

    .line 1543
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->playAlertSound()V

    goto :goto_0

    .line 1544
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v3, :cond_4

    .line 1545
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->beginVibrate()V

    :cond_4
    :goto_0
    return-void
.end method

.method private showOrderProgressDialog()V
    .locals 6

    const-string v0, "OrderTicketActivity"

    const-string v1, "showOrderProgressDialog"

    .line 765
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    new-instance v0, Landroid/app/ProgressDialog;

    const v1, 0x7f0e000c

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 769
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v0, 0x7f0d01a5

    .line 774
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 775
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<font color=\"#303f9f\"><big>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</big></font>"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 778
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f0d0173

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 782
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 784
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    const v0, 0x7f0d028f

    .line 786
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 788
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<big>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</big>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 790
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$4;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$4;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    const/4 v5, -0x2

    invoke-virtual {v3, v5, v1, v4}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 802
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v2, 0x7f0d0059

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 803
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$5;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    const/4 v3, -0x1

    invoke-virtual {v1, v3, v0, v2}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 817
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method private showPassengerDialog()V
    .locals 7

    const-string v0, "OrderTicketActivity"

    const-string v1, "showPassengerDialog"

    .line 978
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->clearSelectedStatus()V

    .line 982
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    .line 983
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;)V

    .line 985
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010019

    const v2, 0x7f010018

    .line 986
    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 987
    new-instance v3, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    invoke-direct {v3}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;-><init>()V

    .line 989
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 990
    iget-object v5, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v5

    const-string v6, "purpose"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->setArguments(Landroid/os/Bundle;)V

    .line 993
    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$9;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$9;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->setListener(Lcom/lltskb/lltskb/fragment/SelectPassengersFragment$Listener;)V

    .line 1001
    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 1002
    const-class v1, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    .line 1003
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0900e1

    .line 1002
    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1007
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private showSeatDialog()V
    .locals 9

    const-string v0, "OrderTicketActivity"

    const-string v1, "showSeatDialog"

    .line 1165
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectSeatDialog:Landroid/support/v7/app/AppCompatDialog;

    if-nez v0, :cond_3

    .line 1167
    sget-object v0, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    .line 1168
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    .line 1169
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSeatType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ","

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1171
    array-length v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_2

    aget-object v6, v1, v5

    const/4 v7, 0x0

    .line 1172
    :goto_1
    sget-object v8, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    array-length v8, v8

    if-ge v7, v8, :cond_1

    .line 1173
    sget-object v8, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    aget-object v8, v8, v7

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1174
    aput-boolean v2, v0, v7

    goto :goto_2

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1181
    :cond_2
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/lltskb/lltskb/utils/LLTUIUtils;->seats:[Ljava/lang/String;

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$15;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$15;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1182
    invoke-virtual {v1, v3, v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x108000a

    .line 1188
    invoke-virtual {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0d0186

    .line 1189
    invoke-virtual {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0d01f4

    new-instance v5, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;

    invoke-direct {v5, p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$17;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Z)V

    .line 1190
    invoke-virtual {v3, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0d008a

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$16;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$16;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1206
    invoke-virtual {v0, v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1213
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1214
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectSeatDialog:Landroid/support/v7/app/AppCompatDialog;

    .line 1215
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectSeatDialog:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    goto :goto_3

    .line 1217
    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    :goto_3
    return-void
.end method

.method private showTimeDialog()V
    .locals 5

    const-string v0, "OrderTicketActivity"

    const-string v1, "showTimeDialog"

    .line 1014
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTimeDialog:Landroid/support/v7/app/AppCompatDialog;

    if-nez v0, :cond_2

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "00:00--24:00"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "00:00--06:00"

    aput-object v3, v0, v2

    const/4 v3, 0x2

    const-string v4, "06:00--12:00"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string v4, "12:00--18:00"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string v4, "18:00--24:00"

    aput-object v4, v0, v3

    .line 1018
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainTime:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1020
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1021
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 1022
    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1027
    :cond_1
    :goto_1
    new-instance v3, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/lltskb/lltskb/order/OrderTicketActivity$10;

    invoke-direct {v4, p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$10;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;)V

    .line 1028
    invoke-virtual {v3, v0, v1, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 1038
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1039
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "\u9009\u62e9\u65f6\u95f4\u6bb5"

    .line 1040
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1041
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTimeDialog:Landroid/support/v7/app/AppCompatDialog;

    .line 1042
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTimeDialog:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    goto :goto_2

    .line 1044
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    :goto_2
    return-void
.end method

.method private showTrainDialog()V
    .locals 4

    const-string v0, "OrderTicketActivity"

    const-string v1, "showTrainDialog"

    .line 930
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 933
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 935
    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    invoke-direct {v1}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;-><init>()V

    .line 936
    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->setOrderConfig(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V

    .line 937
    new-instance v2, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$8;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;->setListener(Lcom/lltskb/lltskb/fragment/SelectTrainFragment$Listener;)V

    .line 965
    const-class v2, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    .line 966
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0900e1

    .line 965
    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 970
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private showTrainTypeDialog()V
    .locals 10

    const-string v0, "OrderTicketActivity"

    const-string v1, "showTrainTypeDialog"

    .line 1087
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTrainTypeDialog:Landroid/support/v7/app/AppCompatDialog;

    if-eqz v0, :cond_0

    .line 1089
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void

    :cond_0
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\u5168\u90e8"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "\u9ad8\u94c1/\u57ce\u9645"

    aput-object v3, v0, v2

    const/4 v3, 0x2

    const-string v4, "\u52a8\u8f66"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string v4, "Z\u5b57\u5934"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string v4, "T\u5b57\u5934"

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const-string v4, "K\u5b57\u5934"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "\u5176\u5b83"

    aput-object v4, v0, v3

    .line 1095
    iget-object v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1096
    array-length v4, v0

    new-array v4, v4, [Z

    .line 1097
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, ","

    .line 1098
    invoke-static {v3, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1100
    array-length v5, v3

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_3

    aget-object v7, v3, v6

    const/4 v8, 0x0

    .line 1101
    :goto_1
    array-length v9, v0

    if-ge v8, v9, :cond_2

    .line 1102
    aget-object v9, v0, v8

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1103
    aput-boolean v2, v4, v8

    goto :goto_2

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1111
    :cond_3
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;

    invoke-direct {v3, p0, v0, v4}, Lcom/lltskb/lltskb/order/OrderTicketActivity$14;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;[Z)V

    .line 1112
    invoke-virtual {v1, v0, v4, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0d01f4

    new-instance v5, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;

    invoke-direct {v5, p0, v0, v4}, Lcom/lltskb/lltskb/order/OrderTicketActivity$13;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;[Ljava/lang/String;[Z)V

    .line 1128
    invoke-virtual {v1, v3, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d008a

    new-instance v3, Lcom/lltskb/lltskb/order/OrderTicketActivity$12;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$12;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    .line 1146
    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 1154
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1155
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0188

    .line 1156
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1157
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTrainTypeDialog:Landroid/support/v7/app/AppCompatDialog;

    .line 1158
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mSelectTrainTypeDialog:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void
.end method

.method private stopAlertSound()V
    .locals 4

    .line 1565
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1566
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    const-string v0, "audio"

    .line 1567
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    .line 1569
    iget v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOldValume:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_0
    return-void
.end method

.method private stopVibrate()V
    .locals 1

    .line 1559
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    .line 1560
    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    :cond_0
    return-void
.end method

.method private switchStation()V
    .locals 3

    const-string v0, "OrderTicketActivity"

    const-string v1, "switchStation"

    .line 1464
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1465
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 1466
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 1467
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1468
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1469
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1471
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mFromStation:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 1472
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mToStation:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    return-void
.end method

.method private updateBgTask()V
    .locals 5

    const v0, 0x7f090045

    .line 1222
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 1224
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1225
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, "\u540e\u53f0[%d]"

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1226
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1228
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1229
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideBgTaskNotify(Landroid/content/Context;)V

    goto :goto_1

    .line 1231
    :cond_2
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showBgTaskNotify(Landroid/content/Context;)V

    :goto_1
    return-void
.end method

.method private updatePurpose()V
    .locals 2

    const v0, 0x7f0901b8

    .line 688
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 690
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v1, "0X00"

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setPurpose(Ljava/lang/String;)V

    goto :goto_0

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    const-string v1, "ADULT"

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setPurpose(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$OrderTicketActivity(Landroid/view/View;)V
    .locals 0

    .line 667
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->orderTicket()V

    return-void
.end method

.method public synthetic lambda$initView$1$OrderTicketActivity(Landroid/view/View;)V
    .locals 0

    .line 672
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->orderTicket()V

    return-void
.end method

.method public synthetic lambda$setPurposeChangeListener$2$OrderTicketActivity(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 683
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updatePurpose()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 1387
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1411
    :sswitch_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->switchStation()V

    goto :goto_0

    .line 1392
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTrainTypeDialog()V

    goto :goto_0

    .line 1401
    :sswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getConfigInfo()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1404
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTrainDialog()V

    goto :goto_0

    .line 1389
    :sswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showTimeDialog()V

    goto :goto_0

    .line 1398
    :sswitch_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showSeatDialog()V

    goto :goto_0

    .line 1408
    :sswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showPassengerDialog()V

    goto :goto_0

    .line 1417
    :sswitch_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showFreqDialog()V

    goto :goto_0

    .line 1395
    :sswitch_7
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showDateDialog()V

    goto :goto_0

    .line 1414
    :sswitch_8
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showAlertDialog()V

    :cond_0
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f090111 -> :sswitch_8
        0x7f09012b -> :sswitch_7
        0x7f090134 -> :sswitch_6
        0x7f09014b -> :sswitch_5
        0x7f090156 -> :sswitch_4
        0x7f09015f -> :sswitch_3
        0x7f090162 -> :sswitch_2
        0x7f090166 -> :sswitch_1
        0x7f09020c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "onCreate"

    .line 485
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 488
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initView()V

    .line 490
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/lltskb/lltskb/utils/JSEngine;->setContext(Landroid/content/Context;)V

    .line 491
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/utils/JSEngine;->setSignValue(Ljava/lang/String;)V

    .line 493
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.lltskb.lltskb.order.login.result"

    .line 494
    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 495
    new-instance v0, Lcom/lltskb/lltskb/order/OrderTicketActivity$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$1;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 512
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 513
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 515
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initTitle()V

    .line 516
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initOrderConfig()V

    .line 518
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->checkLoginStatus()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "onDestroy"

    .line 116
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 118
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->stopAlertSound()V

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getAlertType()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 121
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->stopVibrate()V

    .line 123
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    .line 125
    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 127
    :cond_2
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "onKeyDown"

    .line 1619
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 1621
    const-class v0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->dismissFragment(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 1624
    :cond_0
    const-class v0, Lcom/lltskb/lltskb/fragment/SelectTrainFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->dismissFragment(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 1629
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "OrderTicketActivity"

    const-string v1, "onNewIntent"

    .line 132
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 134
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->setIntent(Landroid/content/Intent;)V

    .line 135
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->initOrderConfig()V

    return-void
.end method

.method public onSearchTicketProgress(I)V
    .locals 3

    .line 1305
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    const v0, 0x7f0d014b

    .line 1307
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    .line 1306
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1309
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=\"#303f9f\"><big>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "</big></font>"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 4

    const-string v0, "OrderTicketActivity"

    const-string v1, "onStart"

    .line 140
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;-><init>(Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 144
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 146
    :cond_1
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onStart()V

    .line 148
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updateBgTask()V

    return-void
.end method

.method public onTextChanged(Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    .line 1603
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->resetOrderTrainNo()V

    return-void
.end method

.method public onTicketSearched(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
    .locals 3

    const-string v0, "OrderTicketActivity"

    const-string v1, "onTicketSearched"

    .line 1315
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, " activity is finishing"

    .line 1317
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1321
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1322
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1324
    :cond_1
    iput-object p1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    .line 1333
    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    const/16 v1, 0xc

    const v2, 0x7f0d0173

    if-ne v0, v1, :cond_2

    const v0, 0x7f0d0161

    .line 1334
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->showNotify(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1335
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "<br/><br/>\u786e\u5b9a\u73b0\u5728\u4e0b\u5355\u5417?"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$18;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_0

    .line 1360
    :cond_2
    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1361
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketActivity$19;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$19;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 1369
    :cond_3
    iget v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    .line 1370
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketActivity$20;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$20;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    goto :goto_0

    .line 1379
    :cond_4
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 1382
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->updateBgTask()V

    return-void
.end method

.method protected showDateDialog()V
    .locals 4

    const-string v0, "OrderTicketActivity"

    const-string v1, "showDateDialog"

    .line 833
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 836
    iget-object v0, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->requestWindowFeature(I)Z

    .line 837
    new-instance v0, Lcom/lltskb/lltskb/view/CalendarView;

    invoke-direct {p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity;->getMaxDays()J

    move-result-wide v1

    invoke-direct {v0, p0, v1, v2}, Lcom/lltskb/lltskb/view/CalendarView;-><init>(Landroid/content/Context;J)V

    .line 839
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    .line 840
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const-string v2, "\u5e74\u6708\u65e5"

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 841
    iget-object v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 842
    iget v1, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mYear:I

    iget v2, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mMonth:I

    iget v3, p0, Lcom/lltskb/lltskb/order/OrderTicketActivity;->mDay:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/CalendarView;->setSelectedDate(III)V

    .line 843
    new-instance v1, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/order/OrderTicketActivity$6;-><init>(Lcom/lltskb/lltskb/order/OrderTicketActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/CalendarView;->setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V

    return-void
.end method
