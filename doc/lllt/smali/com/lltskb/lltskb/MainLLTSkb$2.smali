.class Lcom/lltskb/lltskb/MainLLTSkb$2;
.super Ljava/lang/Object;
.source "MainLLTSkb.java"

# interfaces
.implements Lcom/baidu/mobads/SplashAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/MainLLTSkb;->showBaiduSSPAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/MainLLTSkb;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onAdDismissed$0$MainLLTSkb$2()V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    return-void
.end method

.method public synthetic lambda$onAdFailed$1$MainLLTSkb$2()V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    return-void
.end method

.method public onAdClick()V
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "baidu onAdClick"

    .line 269
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setLastUpdateTime(J)V

    .line 272
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "baidussp"

    const-string v2, "\u70b9\u51fb"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onAdDismissed()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "baidu onAdDismissed "

    .line 241
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$2$cfvZWmOcgioGstGZuYdk1AvBQkc;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$2$cfvZWmOcgioGstGZuYdk1AvBQkc;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "baidu onAdFailed msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "MainLLTSkb"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 251
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {p1}, Lcom/lltskb/lltskb/MainLLTSkb;->access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$2$5jqABvwC_lXRO4c6gvoZ7BwOn8U;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$2$5jqABvwC_lXRO4c6gvoZ7BwOn8U;-><init>(Lcom/lltskb/lltskb/MainLLTSkb$2;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 253
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setLastUpdateTime(J)V

    return-void
.end method

.method public onAdPresent()V
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "baidu onAdPresent"

    .line 258
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setLastUpdateTime(J)V

    .line 260
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    .line 261
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$200(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 263
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$2;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    invoke-static {v0}, Lcom/lltskb/lltskb/MainLLTSkb;->access$300(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 264
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "baidussp"

    const-string v2, "\u63a5\u6536"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
