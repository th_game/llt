.class public Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;
.super Ljava/lang/Object;
.source "LeftTicketClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LeftTicketClient"

.field public static curl:Ljava/lang/String;

.field public static otherMaxdate:Ljava/lang/String;

.field public static otherMindate:Ljava/lang/String;

.field private static saveQueryLog:Ljava/lang/Boolean;

.field public static studentMaxDate:Ljava/lang/String;

.field public static studentMinDate:Ljava/lang/String;


# instance fields
.field private date:Ljava/lang/String;

.field private from:Ljava/lang/String;

.field private message:Ljava/lang/String;

.field private purpose:Ljava/lang/String;

.field private result:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field

.field private to:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    .line 49
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->saveQueryLog:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calcMaxDate()V
    .locals 10

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calcMaxDate stu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->studentMaxDate:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",other="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->otherMaxdate:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "LeftTicketClient"

    invoke-static {v3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    sget-object v0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->studentMaxDate:Ljava/lang/String;

    const-string v4, "yyyy-MM-dd"

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->str2Date(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 168
    sget-object v5, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->otherMaxdate:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->str2Date(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 169
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 170
    invoke-static {v5, v0}, Lcom/lltskb/lltskb/utils/DateUtils;->getDayDiff(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v6

    .line 171
    invoke-static {v5, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->getDayDiff(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    .line 173
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/lltskb/lltskb/engine/LltSettings;->setMaxStudentDays(J)V

    .line 174
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/lltskb/lltskb/engine/LltSettings;->setMaxOtherDays(J)V

    .line 176
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 65
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 68
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-lez p1, :cond_1

    add-int/lit8 p1, p1, 0x1

    const-string v0, "\'"

    .line 70
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    .line 71
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    .line 73
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    return-object v1
.end method

.method public static initCURL()Z
    .locals 7

    const-string v0, "LeftTicketClient"

    .line 96
    sget-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 99
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v1

    const-string v3, "https://kyfw.12306.cn/otn/leftTicket/init"

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/utils/JSEngine;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 101
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 107
    :try_start_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    invoke-static {}, Lcom/lltskb/lltskb/client/NetworkUtils;->clearCookie()V

    .line 111
    :cond_1
    new-instance v4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient$1;

    invoke-direct {v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient$1;-><init>()V

    const/4 v5, 0x0

    :goto_1
    if-nez v5, :cond_2

    if-lez v1, :cond_2

    .line 135
    invoke-static {v4}, Lcom/lltskb/lltskb/client/NetworkClient;->requestString(Lcom/lltskb/lltskb/client/IRequest;)Lcom/lltskb/lltskb/client/IResponse;

    move-result-object v5

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_3

    .line 140
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    const-string v1, "Y"

    .line 143
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v6, "isSaveQueryLog"

    invoke-static {v4, v6}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->saveQueryLog:Ljava/lang/Boolean;

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSaveQueryLog="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->saveQueryLog:Ljava/lang/Boolean;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v4, "CLeftTicketUrl"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CLeftTicketUrl="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v4, "studentMindate"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->studentMinDate:Ljava/lang/String;

    .line 149
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v4, "studentMaxdate"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->studentMaxDate:Ljava/lang/String;

    .line 150
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v4, "otherMindate"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->otherMindate:Ljava/lang/String;

    .line 151
    invoke-interface {v5}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v4, "otherMaxdate"

    invoke-static {v1, v4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->otherMaxdate:Ljava/lang/String;

    .line 152
    invoke-static {}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->calcMaxDate()V

    .line 154
    sget-object v1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    xor-int/2addr v0, v2

    return v0

    :catch_1
    move-exception v1

    .line 157
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 158
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initCURL="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return v3
.end method

.method private parseQueryResult(Ljava/lang/String;)I
    .locals 12

    const-string v0, "error_msg"

    const-string v1, "messages"

    const-string v2, "data"

    const-string v3, "c_url"

    const-string v4, "httpstatus"

    .line 284
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseQueryResult ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "LeftTicketClient"

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x2

    const-string v7, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    if-eqz p1, :cond_21

    .line 285
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x6

    if-ge v8, v9, :cond_0

    goto/16 :goto_14

    .line 290
    :cond_0
    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-gtz v8, :cond_20

    const-string v8, "<"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    goto/16 :goto_13

    :cond_1
    const/4 v8, -0x3

    .line 294
    :try_start_0
    new-instance v10, Lorg/json/JSONTokener;

    invoke-direct {v10, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 298
    invoke-virtual {v10}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    const/4 v10, -0x1

    if-eqz p1, :cond_1f

    .line 299
    instance-of v11, p1, Lorg/json/JSONObject;

    if-nez v11, :cond_2

    goto/16 :goto_11

    .line 303
    :cond_2
    check-cast p1, Lorg/json/JSONObject;

    .line 304
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 305
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 306
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    .line 307
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "parseQueryResult new c_url = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v6, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v5

    .line 311
    :cond_3
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_4

    .line 313
    iput-object v7, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    :cond_4
    const/4 v3, 0x0

    .line 316
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_6

    .line 317
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "result"

    if-eqz v0, :cond_5

    .line 319
    :try_start_1
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    if-nez v3, :cond_9

    const-string p1, "\u683c\u5f0f\u9519\u8bef"

    .line 321
    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return v8

    .line 325
    :cond_5
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "message"

    .line 326
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    goto :goto_1

    .line 329
    :cond_6
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 330
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    const-string v0, ""

    .line 331
    iput-object v0, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    const/4 v0, 0x0

    .line 332
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_7
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 336
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string p1, "\u672a\u77e5\u9519\u8bef"

    .line 339
    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    :cond_9
    :goto_1
    if-nez v3, :cond_a

    return v8

    :cond_a
    const/4 p1, 0x0

    .line 345
    :goto_2
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_1e

    .line 346
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 347
    invoke-virtual {v3, p1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_b

    goto/16 :goto_10

    :cond_b
    const-string v2, "\\|"

    .line 350
    invoke-static {v1, v2, v10}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1d

    .line 352
    array-length v2, v1

    const/16 v4, 0x24

    if-ge v2, v4, :cond_c

    goto/16 :goto_10

    .line 403
    :cond_c
    iget-object v2, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->date:Ljava/lang/String;

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    .line 404
    invoke-static {}, Lcom/lltskb/lltskb/utils/StringUtils;->getToday()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->back_take_date:Ljava/lang/String;

    .line 408
    aget-object v2, v1, v5

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const/4 v2, 0x1

    .line 409
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    const/4 v2, 0x2

    .line 410
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const/4 v2, 0x3

    .line 411
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const/4 v2, 0x4

    .line 412
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const/4 v2, 0x5

    .line 413
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    .line 414
    aget-object v2, v1, v9

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const/4 v2, 0x7

    .line 415
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const/16 v2, 0x8

    .line 416
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const/16 v2, 0x9

    .line 417
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const/16 v2, 0xa

    .line 418
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const/16 v2, 0xb

    .line 419
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const/16 v2, 0xc

    .line 420
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const/16 v2, 0xd

    .line 421
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const/16 v2, 0xe

    .line 422
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const/16 v2, 0xf

    .line 423
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const/16 v2, 0x10

    .line 424
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const/16 v2, 0x11

    .line 425
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const/16 v2, 0x12

    .line 426
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const/16 v2, 0x13

    .line 427
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const/16 v2, 0x14

    .line 428
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v4, "--"

    if-eqz v2, :cond_d

    const/16 v2, 0x14

    :try_start_2
    aget-object v2, v1, v2

    goto :goto_3

    :cond_d
    move-object v2, v4

    :goto_3
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const/16 v2, 0x15

    .line 429
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x15

    aget-object v2, v1, v2

    goto :goto_4

    :cond_e
    move-object v2, v4

    :goto_4
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const/16 v2, 0x16

    .line 430
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x16

    aget-object v2, v1, v2

    goto :goto_5

    :cond_f
    move-object v2, v4

    :goto_5
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const/16 v2, 0x17

    .line 431
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x17

    aget-object v2, v1, v2

    goto :goto_6

    :cond_10
    move-object v2, v4

    :goto_6
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const/16 v2, 0x18

    .line 432
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x18

    aget-object v2, v1, v2

    goto :goto_7

    :cond_11
    move-object v2, v4

    :goto_7
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const/16 v2, 0x19

    .line 433
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x19

    aget-object v2, v1, v2

    goto :goto_8

    :cond_12
    move-object v2, v4

    :goto_8
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const/16 v2, 0x1a

    .line 434
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x1a

    aget-object v2, v1, v2

    goto :goto_9

    :cond_13
    move-object v2, v4

    :goto_9
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const/16 v2, 0x1b

    .line 435
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x1b

    aget-object v2, v1, v2

    goto :goto_a

    :cond_14
    move-object v2, v4

    :goto_a
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const/16 v2, 0x1c

    .line 436
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x1c

    aget-object v2, v1, v2

    goto :goto_b

    :cond_15
    move-object v2, v4

    :goto_b
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const/16 v2, 0x1d

    .line 437
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x1d

    aget-object v2, v1, v2

    goto :goto_c

    :cond_16
    move-object v2, v4

    :goto_c
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const/16 v2, 0x1e

    .line 438
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x1e

    aget-object v2, v1, v2

    goto :goto_d

    :cond_17
    move-object v2, v4

    :goto_d
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const/16 v2, 0x1f

    .line 439
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x1f

    aget-object v2, v1, v2

    goto :goto_e

    :cond_18
    move-object v2, v4

    :goto_e
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const/16 v2, 0x20

    .line 440
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x20

    aget-object v2, v1, v2

    goto :goto_f

    :cond_19
    move-object v2, v4

    :goto_f
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const/16 v2, 0x21

    .line 441
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x21

    aget-object v4, v1, v2

    :cond_1a
    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    const/16 v2, 0x22

    .line 442
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const/16 v2, 0x23

    .line 444
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    .line 445
    array-length v2, v1

    const/16 v4, 0x26

    if-le v2, v4, :cond_1b

    const/16 v2, 0x24

    .line 446
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->exchange_train_flag:Ljava/lang/String;

    const/16 v2, 0x25

    .line 447
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    const/16 v2, 0x26

    .line 448
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    .line 451
    :cond_1b
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    aget-object v4, v1, v9

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    .line 452
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    const/4 v4, 0x7

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    .line 453
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    .line 454
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    .line 456
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const-string v1, "\u9884\u8ba2"

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 457
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    .line 459
    :cond_1c
    iget-object v1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->result:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1d
    :goto_10
    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_2

    :cond_1e
    return v5

    :cond_1f
    :goto_11
    return v10

    :catch_0
    move-exception p1

    .line 467
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    :catch_1
    move-exception p1

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_12
    return v8

    :cond_20
    :goto_13
    return v5

    .line 286
    :cond_21
    :goto_14
    iput-object v7, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return v5
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->result:Ljava/util/Vector;

    return-object v0
.end method

.method public queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    const-string v0, "utf-8"

    .line 180
    iput-object p3, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->date:Ljava/lang/String;

    .line 181
    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->from:Ljava/lang/String;

    .line 182
    iput-object p2, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->to:Ljava/lang/String;

    .line 183
    iput-object p4, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->purpose:Ljava/lang/String;

    .line 185
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "\u3011\u65e0\u6cd5\u627e\u5230"

    const-string v5, "\u8f66\u7ad9\u3010"

    const/4 v6, 0x0

    if-eqz v3, :cond_0

    .line 189
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return v6

    .line 193
    :cond_0
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 194
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return v6

    .line 202
    :cond_1
    :try_start_0
    invoke-static {p3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 203
    invoke-static {p4, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 205
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 208
    :goto_0
    sget-object p1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 p2, 0x4

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p3, p2, v6

    const/4 p3, 0x1

    aput-object v1, p2, p3

    const/4 v0, 0x2

    aput-object v2, p2, v0

    const/4 v1, 0x3

    aput-object p4, p2, v1

    const-string p4, "leftTicketDTO.train_date=%s&leftTicketDTO.from_station=%s&leftTicketDTO.to_station=%s&purpose_codes=%s"

    invoke-static {p1, p4, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    :goto_1
    if-ge p2, v0, :cond_7

    add-int/lit8 p2, p2, 0x1

    .line 216
    sget-object p4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    if-nez p4, :cond_2

    .line 217
    invoke-static {}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->initCURL()Z

    .line 219
    :cond_2
    sget-object p4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    if-nez p4, :cond_3

    const-string p4, "leftTicket/query"

    .line 220
    sput-object p4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    .line 223
    :cond_3
    new-instance p4, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient$2;

    invoke-direct {p4, p0, p1}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient$2;-><init>(Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;Ljava/lang/String;)V

    .line 244
    invoke-static {p4}, Lcom/lltskb/lltskb/client/NetworkClient;->requestString(Lcom/lltskb/lltskb/client/IRequest;)Lcom/lltskb/lltskb/client/IResponse;

    move-result-object p4

    if-nez p4, :cond_4

    const-string p1, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    .line 246
    iput-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->message:Ljava/lang/String;

    return v6

    .line 250
    :cond_4
    invoke-interface {p4}, Lcom/lltskb/lltskb/client/IResponse;->getBody()Ljava/lang/String;

    move-result-object p4

    invoke-direct {p0, p4}, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->parseQueryResult(Ljava/lang/String;)I

    move-result p4

    const/4 v1, -0x2

    if-ne p4, v1, :cond_5

    goto :goto_1

    .line 256
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->result:Ljava/util/Vector;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    goto :goto_2

    :cond_6
    const/4 p3, 0x0

    :cond_7
    :goto_2
    return p3
.end method
