.class public Lcom/lltskb/lltskb/client/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field private static final PUB_KEY_12306:Ljava/lang/String; = "bc0b1973f95ff82a4524f184f1571ce28bbc69da064f5aeb95062c10ea2c0bf7c8adef958d1a260251ab035f2dcef3063e3ed645be010a9291ea43553ab9e9a21d2b6d8544b5c5306c53f4ee5c5e801dcfa876e3facc218a7149c744092c45bf01192833040fd7dc1f4250a9d86bd600d8404861c72bcc887a6910230c76ef61"

.field private static final PUB_KEY_ST12306:Ljava/lang/String; = "b963f4ccf401792a6c21f7aa61499c8ea90da7ec39bb3a9f7d70b6fab8480b36e3a1034e95ef41c0ba0b4dd3ce377c696de7d9bb03ae8acc76b2658208785cbdce014f02d2ff66d0da2644d493296373df6000c0a48a626cb973873bf938af43244f538eb260d1f524afce16f1a506ddaad1102ff9ed82890697eda343198ad738f8e9935cb699ecef1bd14c661c056e83f134f305c7ecf123db6b20b0b89d69c9e15a3b31e975f368174a1b1ad366d3fb118c4cb2f570536cc7920c25d929b8fe8cf423d0e7e7869627a7b944c1409e8ce27a5c84ce6f62c8da24dc36f05a7e33903e6e02ce6db8474f8c37db59a283a11de0f8219bada35fcf501781062f9d"

.field private static final TAG:Ljava/lang/String; = "NetworkUtils"

.field private static mCookieManager:Ljava/net/CookieHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearCookie()V
    .locals 2

    const-string v0, "NetworkUtils"

    const-string v1, "clearCookie"

    .line 113
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 115
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    .line 116
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 117
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    const/4 v0, 0x0

    .line 118
    sput-object v0, Lcom/lltskb/lltskb/client/ticket/LeftTicketClient;->curl:Ljava/lang/String;

    return-void
.end method

.method private static disableSSLCertificateChecking()V
    .locals 6

    const-string v0, "NetworkUtils"

    .line 39
    new-instance v1, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v1}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    const/4 v1, 0x0

    .line 42
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :try_start_1
    const-string v2, "TLS"

    .line 46
    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    .line 48
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    :goto_0
    if-nez v2, :cond_0

    const-string v1, "HttpsClient sc == null"

    .line 53
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v3, 0x1

    new-array v3, v3, [Ljavax/net/ssl/X509TrustManager;

    const/4 v4, 0x0

    .line 56
    new-instance v5, Lcom/lltskb/lltskb/client/NetworkUtils$1;

    invoke-direct {v5}, Lcom/lltskb/lltskb/client/NetworkUtils$1;-><init>()V

    aput-object v5, v3, v4

    .line 97
    :try_start_2
    new-instance v4, Ljava/security/SecureRandom;

    invoke-direct {v4}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2, v1, v3, v4}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_2
    .catch Ljava/security/KeyManagementException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    .line 99
    invoke-virtual {v1}, Ljava/security/KeyManagementException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {v1}, Ljava/security/KeyManagementException;->printStackTrace()V

    .line 102
    :goto_1
    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    return-void
.end method

.method public static init()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 106
    invoke-static {}, Lcom/lltskb/lltskb/client/NetworkUtils;->disableSSLCertificateChecking()V

    .line 108
    new-instance v0, Lcom/lltskb/lltskb/utils/WebkitCookieManagerProxy;

    sget-object v1, Ljava/net/CookiePolicy;->ACCEPT_ALL:Ljava/net/CookiePolicy;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/lltskb/lltskb/utils/WebkitCookieManagerProxy;-><init>(Ljava/net/CookieStore;Ljava/net/CookiePolicy;)V

    sput-object v0, Lcom/lltskb/lltskb/client/NetworkUtils;->mCookieManager:Ljava/net/CookieHandler;

    .line 109
    sget-object v0, Lcom/lltskb/lltskb/client/NetworkUtils;->mCookieManager:Ljava/net/CookieHandler;

    invoke-static {v0}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    return-void
.end method
