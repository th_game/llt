.class public Lcom/lltskb/lltskb/engine/DataMgr;
.super Ljava/lang/Object;
.source "DataMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;
    }
.end annotation


# static fields
.field static final GUARD:I = 0x80

.field private static final TAG:Ljava/lang/String; = "DataMgr"


# instance fields
.field protected data:[B

.field private tsPriceIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private tsPriceVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;",
            ">;"
        }
    .end annotation
.end field

.field private xwIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->xwIndex:Ljava/util/Map;

    .line 38
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;->init(Ljava/lang/String;)V

    return-void
.end method

.method private getBitCount(I)I
    .locals 2

    and-int/lit16 p1, p1, 0xff

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x3

    :cond_2
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_3

    add-int/lit8 v0, v0, 0x2

    :cond_3
    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    and-int/lit8 p1, p1, 0x20

    if-eqz p1, :cond_5

    add-int/lit8 v0, v0, 0x1

    :cond_5
    return v0
.end method

.method private hasTsPrice(I)Z
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceVector:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 400
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/DataMgr;->initTsPrice()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceIndex:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private init(Ljava/lang/String;)V
    .locals 12

    const-string v0, " "

    const-string v1, "init "

    .line 154
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 155
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    if-eqz v2, :cond_0

    return-void

    .line 158
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, "DataMgr"

    if-nez v3, :cond_1

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not exists"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 165
    :cond_1
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-wide/16 v5, 0x7530

    .line 167
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    cmp-long v9, v5, v7

    if-gez v9, :cond_2

    .line 168
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v11, v7, v9

    if-lez v11, :cond_3

    .line 171
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    :cond_3
    :goto_0
    long-to-int v2, v5

    .line 174
    new-array v2, v2, [B

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 176
    :try_start_1
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    invoke-virtual {v3, v2}, Ljava/io/FileInputStream;->read([B)I

    .line 177
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v2

    .line 179
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    .line 182
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private initTsPrice()V
    .locals 5

    .line 450
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceVector:Ljava/util/Vector;

    .line 451
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceIndex:Ljava/util/Map;

    .line 453
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 459
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 460
    new-instance v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;-><init>(Lcom/lltskb/lltskb/engine/DataMgr;)V

    .line 461
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v3, v3, 0xff

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->train:I

    add-int/lit8 v0, v0, 0x2

    .line 463
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->start_date:I

    add-int/lit8 v0, v0, 0x4

    .line 467
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->end_date:I

    add-int/lit8 v0, v0, 0x4

    .line 471
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->rule1:I

    add-int/lit8 v0, v0, 0x2

    .line 474
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->rule2:I

    add-int/lit8 v0, v0, 0x2

    .line 477
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v3, v3, 0xff

    add-int/lit8 v4, v0, 0x1

    aget-byte v2, v2, v4

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v3, v2

    iput v3, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->price_no:I

    add-int/lit8 v0, v0, 0x2

    .line 479
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceVector:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 480
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceIndex:Ljava/util/Map;

    iget v1, v1, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->train:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    return-void
.end method

.method private initXwIndex()V
    .locals 5

    .line 290
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    if-nez v0, :cond_0

    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->xwIndex:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    .line 293
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x4

    if-ge v0, v1, :cond_1

    .line 294
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 295
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v3, v3, 0x80

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v4, v0, 0x1

    .line 297
    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v4

    .line 299
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->xwIndex:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private pow(JJ)J
    .locals 10

    const/4 v0, 0x0

    move-wide v1, p1

    :goto_0
    int-to-long v3, v0

    const-wide/16 v5, 0x1

    sub-long v7, p3, v5

    cmp-long v9, v3, v7

    if-gez v9, :cond_0

    mul-long v1, v1, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    cmp-long v0, p3, p1

    if-nez v0, :cond_1

    move-wide v1, v5

    :cond_1
    return-wide v1
.end method


# virtual methods
.method getCancelTrainData(II)[B
    .locals 7

    .line 342
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const-string p1, "DataMgr"

    const-string p2, "getCancelTrainData data is null"

    .line 343
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const/16 p2, 0x4000

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 349
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v4, v3

    if-ge v2, v4, :cond_7

    .line 350
    aget-byte v4, v3, v2

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v5, v2, 0x1

    .line 351
    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    mul-int/lit16 v4, v4, 0x80

    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x2

    .line 356
    aget-byte v5, v3, v2

    and-int/lit16 v5, v5, 0xff

    add-int/lit8 v6, v2, 0x1

    .line 357
    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    mul-int/lit16 v5, v5, 0x80

    add-int/2addr v5, v6

    add-int/lit8 v2, v2, 0x2

    .line 361
    aget-byte v3, v3, v2

    and-int/lit16 v3, v3, 0xff

    if-ne p1, v4, :cond_4

    if-ne v5, p2, :cond_4

    add-int/lit8 p1, v2, 0x1

    move p2, p1

    const/4 p1, 0x0

    :goto_1
    if-ge p1, v3, :cond_2

    .line 368
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v0, v0, p2

    if-nez v0, :cond_1

    add-int/lit8 p2, p2, 0xc

    goto :goto_2

    :cond_1
    add-int/lit8 p2, p2, 0x9

    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    sub-int/2addr p2, v2

    .line 376
    new-array p1, p2, [B

    .line 377
    array-length p2, p1

    .line 378
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v3, v0

    sub-int/2addr v3, v2

    if-le p2, v3, :cond_3

    .line 379
    array-length p2, v0

    sub-int/2addr p2, v2

    .line 381
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    invoke-static {v0, v2, p1, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    move v4, v2

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_6

    .line 386
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v5, v5, v4

    if-nez v5, :cond_5

    add-int/lit8 v4, v4, 0xc

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, 0x9

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move v2, v4

    goto :goto_0

    :cond_7
    return-object v0
.end method

.method getPkPriceUnit(II)[B
    .locals 9

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 109
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v4, v3

    if-ge v2, v4, :cond_5

    .line 110
    invoke-virtual {p0, v3, v2}, Lcom/lltskb/lltskb/engine/DataMgr;->readShort([BI)I

    move-result v3

    .line 111
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    add-int/lit8 v5, v2, 0x2

    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/DataMgr;->readShort([BI)I

    # v4
    move-result v4

    add-int/lit8 v2, v2, 0x4

    .line 114
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v5, v5, v2

    # v5 = read()
    and-int/lit16 v5, v5, 0xff

    add-int/lit8 v6, v2, 0x1

    move v7, v6

    const/4 v6, 0x0

    # this.pos >= v5
    :goto_1
    if-ge v6, v5, :cond_1

    .line 117
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    invoke-virtual {p0, v8, v7}, Lcom/lltskb/lltskb/engine/DataMgr;->readShort([BI)I

    add-int/lit8 v7, v7, 0x2

    .line 119
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v8, v8, v7

    and-int/lit16 v8, v8, 0xff

    add-int/lit8 v7, v7, 0x1

    .line 121
    invoke-direct {p0, v8}, Lcom/lltskb/lltskb/engine/DataMgr;->getBitCount(I)I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    if-ne v3, p1, :cond_2

    if-eq v4, p2, :cond_3

    :cond_2
    if-ne v3, p2, :cond_4

    if-ne v4, p1, :cond_4

    # v7 = v7 - v2
    :cond_3
    sub-int/2addr v7, v2

    .line 125
    new-array p1, v7, [B

    .line 126
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v1, p1

    invoke-static {p2, v2, p1, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_4
    move v2, v7

    goto :goto_0

    :cond_5
    return-object v1
.end method

.method getPriceUnit(II)[B
    .locals 7

    .line 134
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 137
      :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v4, v3

    if-ge v2, v4, :cond_2

    .line 138
    aget-byte v4, v3, v2

    and-int/lit16 v4, v4, 0xff

    mul-int/lit16 v4, v4, 0x80

    add-int/lit8 v5, v2, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    # v4 = int1
    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x2

    .line 140
    aget-byte v5, v3, v2

    and-int/lit16 v5, v5, 0xff

    mul-int/lit16 v5, v5, 0x80

    add-int/lit8 v6, v2, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v5, v6

    add-int/lit8 v2, v2, 0x2

    .line 142
    aget-byte v6, v3, v2

    and-int/lit16 v6, v6, 0xff

    if-ne p1, v4, :cond_1

    if-ne p2, v5, :cond_1

    mul-int/lit8 v6, v6, 0x6

    .line 144
    new-array p1, v6, [B

    .line 145
    invoke-static {v3, v2, p1, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_1
    mul-int/lit8 v6, v6, 0x6

    add-int/lit8 v6, v6, -0x1

    add-int/2addr v2, v6

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method getScheduleData(II)[B
    .locals 7

    .line 230
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "DataMgr"

    const-string p2, "getScheduleData data is null"

    .line 231
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 237
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v4, v3

    if-ge v2, v4, :cond_7

    .line 238
    aget-byte v4, v3, v2

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v5, v2, 0x1

    .line 239
    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    mul-int/lit16 v4, v4, 0x80

    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x2

    .line 244
    aget-byte v5, v3, v2

    and-int/lit16 v5, v5, 0xff

    add-int/lit8 v6, v2, 0x1

    .line 245
    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    mul-int/lit16 v5, v5, 0x80

    add-int/2addr v5, v6

    add-int/lit8 v2, v2, 0x2

    .line 249
    aget-byte v3, v3, v2

    and-int/lit16 v3, v3, 0xff

    if-ne p1, v4, :cond_4

    if-ne p2, v5, :cond_4

    add-int/lit8 p1, v2, 0x1

    move p2, p1

    const/4 p1, 0x0

    :goto_1
    if-ge p1, v3, :cond_2

    .line 256
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v1, v1, p2

    if-nez v1, :cond_1

    add-int/lit8 p2, p2, 0xc

    goto :goto_2

    :cond_1
    add-int/lit8 p2, p2, 0x9

    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    sub-int/2addr p2, v2

    .line 264
    new-array p1, p2, [B

    .line 265
    array-length p2, p1

    .line 266
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v3, v1

    sub-int/2addr v3, v2

    if-le p2, v3, :cond_3

    .line 267
    array-length p2, v1

    sub-int/2addr p2, v2

    .line 269
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    invoke-static {v1, v2, p1, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    move v4, v2

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_6

    .line 274
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v5, v5, v4

    if-nez v5, :cond_5

    add-int/lit8 v4, v4, 0xc

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, 0x9

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move v2, v4

    goto :goto_0

    :cond_7
    return-object v1
.end method

.method getTsPriceNo(III)I
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 407
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceVector:Ljava/util/Vector;

    if-nez v2, :cond_0

    .line 408
    invoke-direct/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/DataMgr;->initTsPrice()V

    .line 410
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/lltskb/lltskb/engine/DataMgr;->hasTsPrice(I)Z

    move-result v2

    if-nez v2, :cond_1

    return p3

    .line 413
    :cond_1
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/DataMgr;->tsPriceVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;

    .line 414
    iget v4, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->train:I

    move/from16 v5, p1

    if-eq v4, v5, :cond_3

    goto :goto_0

    .line 418
    :cond_3
    iget v4, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->start_date:I

    if-gt v4, v1, :cond_2

    iget v4, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->end_date:I

    if-ge v4, v1, :cond_4

    goto :goto_0

    .line 422
    :cond_4
    iget v4, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->start_date:I

    invoke-static {v4, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateDiff(II)J

    move-result-wide v6

    .line 423
    iget v4, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->rule1:I

    .line 424
    iget v8, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->rule2:I

    const/4 v9, 0x1

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :cond_5
    int-to-long v10, v8

    const-wide/16 v12, 0x2

    int-to-long v14, v4

    .line 426
    rem-long/2addr v6, v14

    invoke-direct {v0, v12, v13, v6, v7}, Lcom/lltskb/lltskb/engine/DataMgr;->pow(JJ)J

    move-result-wide v6

    and-long/2addr v6, v10

    if-eq v4, v9, :cond_6

    const-wide/16 v8, 0x1

    cmp-long v4, v6, v8

    if-gez v4, :cond_6

    goto :goto_0

    .line 432
    :cond_6
    iget v1, v3, Lcom/lltskb/lltskb/engine/DataMgr$TsPrice;->price_no:I

    return v1

    :cond_7
    return p3
.end method

.method getUnit(IZ)[B
    .locals 8

    .line 42
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    if-gez p1, :cond_0

    goto :goto_3

    .line 47
    :cond_0
    array-length v0, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_6

    .line 49
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    add-int/lit8 v5, v3, 0x1

    aget-byte v3, v4, v3

    and-int/lit16 v3, v3, 0xff

    const/16 v6, 0x80

    mul-int/lit16 v3, v3, 0x80

    add-int/lit8 v7, v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    if-eqz p2, :cond_1

    add-int/lit8 v4, v7, 0xc

    goto :goto_1

    :cond_1
    move v4, v7

    :goto_1
    if-ge v4, v0, :cond_3

    .line 53
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v5, v5, v4

    and-int/lit16 v5, v5, 0xff

    if-eq v5, v6, :cond_3

    if-eqz p2, :cond_2

    add-int/lit8 v4, v4, 0x7

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    if-ne v3, p1, :cond_5

    sub-int/2addr v4, v7

    .line 60
    new-array p1, v4, [B

    .line 67
    array-length p2, p1

    :goto_2
    add-int v0, p2, v7

    .line 68
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v3, v1

    if-le v0, v3, :cond_4

    add-int/lit8 p2, p2, -0x1

    goto :goto_2

    .line 71
    :cond_4
    invoke-static {v1, v7, p1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_5
    add-int/lit8 v3, v4, 0x1

    goto :goto_0

    :cond_6
    :goto_3
    return-object v1
.end method

.method getXwData(I)[B
    .locals 4

    .line 310
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "DataMgr"

    const-string v0, "getXwData data is null"

    .line 311
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->xwIndex:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->xwIndex:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_1

    .line 318
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/lit8 p1, p1, 0x4

    .line 321
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    add-int/lit8 v1, p1, 0x1

    aget-byte p1, v0, p1

    and-int/lit16 p1, p1, 0xff

    mul-int/lit8 p1, p1, 0x3

    add-int/lit8 p1, p1, 0x3

    .line 322
    new-array p1, p1, [B

    add-int/lit8 v1, v1, -0x3

    const/4 v2, 0x0

    .line 323
    array-length v3, p1

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_1
    return-object v1

    .line 331
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/DataMgr;->initXwIndex()V

    .line 333
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;->getXwData(I)[B

    move-result-object p1

    return-object p1
.end method

.method hasScheduleData(I)Z
    .locals 5

    .line 190
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "DataMgr"

    const-string v0, "hasScheduleData data is null"

    .line 191
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    const/4 v0, 0x0

    .line 197
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v3, v2

    if-ge v0, v3, :cond_4

    .line 198
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    add-int/lit8 v4, v0, 0x1

    .line 199
    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    mul-int/lit16 v3, v3, 0x80

    add-int/2addr v3, v4

    add-int/lit8 v0, v0, 0x2

    .line 204
    aget-byte v4, v2, v0

    add-int/lit8 v4, v0, 0x1

    .line 205
    aget-byte v4, v2, v4

    add-int/lit8 v0, v0, 0x2

    .line 209
    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    if-ne p1, v3, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    move v3, v0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    .line 216
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    aget-byte v4, v4, v3

    if-nez v4, :cond_2

    add-int/lit8 v3, v3, 0xc

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x9

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_0

    :cond_4
    return v1
.end method

.method protected readInt(I)I
    .locals 3

    add-int/lit8 v0, p1, 0x4

    .line 485
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/DataMgr;->data:[B

    array-length v2, v1

    if-lt v0, v2, :cond_0

    const/4 p1, 0x0

    return p1

    .line 488
    :cond_0
    aget-byte v0, v1, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, v1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    add-int/2addr v0, v2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, v1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    add-int/lit8 p1, p1, 0x3

    aget-byte p1, v1, p1

    and-int/lit16 p1, p1, 0xff

    add-int/2addr v0, p1

    return v0
.end method

.method protected readShort([BI)I
    .locals 1

    .line 82
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    if-lt p2, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 84
    :cond_0
    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    mul-int/lit16 v0, v0, 0xff

    add-int/lit8 p2, p2, 0x1

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    add-int/2addr v0, p1

    return v0
.end method
