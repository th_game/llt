.class public Lcom/lltskb/lltskb/engine/LltSettings;
.super Ljava/lang/Object;
.source "LltSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;
    }
.end annotation


# static fields
.field private static final BAIDU_CRASHED:Ljava/lang/String; = "baidu_crashed"

.field private static final BOOK_PURPOSE:Ljava/lang/String; = "book_purpose"

.field private static final LAST_CHECK_PERMISSION_TIME:Ljava/lang/String; = "last_check_permission_time"

.field private static final LAST_TAKE_DATE:Ljava/lang/String; = "last_take_date"

.field private static final LAST_UPDATE_TIME:Ljava/lang/String; = "last_update_time"

.field private static final LAST_UPDATE_VERSION:Ljava/lang/String; = "last_update_ver"

.field private static final MAX_OTHER_DATE:Ljava/lang/String; = "max_other_date"

.field private static final MAX_STUDENT_DATE:Ljava/lang/String; = "max_student_date"

.field private static final SETTING_CLASSIC_STYLE:Ljava/lang/String; = "classic_style"

.field private static final SETTING_DEFAULT_QUERY_DATE:Ljava/lang/String; = "default_query_date"

.field private static final SETTING_END_STATION:Ljava/lang/String; = "end_point"

.field private static final SETTING_FILTER_TYPE:Ljava/lang/String; = "filter_type"

.field private static final SETTING_FONT_SIZE:Ljava/lang/String; = "font-size"

.field private static final SETTING_FUZZY_QUERY:Ljava/lang/String; = "query-fuzzy"

.field private static final SETTING_HAS_NEWVERSION:Ljava/lang/String; = "has_new_version"

.field private static final SETTING_HIDE_TRAIN:Ljava/lang/String; = "hide-train"

.field private static final SETTING_QUERY_HISTROY:Ljava/lang/String; = "query_histroy"

.field private static final SETTING_SHOW_RUNCHART:Ljava/lang/String; = "show_runchart"

.field private static final SETTING_START_STATION:Ljava/lang/String; = "start_point"

.field private static final SETTING_STATION:Ljava/lang/String; = "station"

.field private static final SETTING_TRAIN:Ljava/lang/String; = "train"

.field private static final TAG:Ljava/lang/String; = "LltSettings"

.field private static final ZWD_QUERY_TYPE:Ljava/lang/String; = "zwd_query_type"

.field private static mInstance:Lcom/lltskb/lltskb/engine/LltSettings;


# instance fields
.field private mBaiduCrashed:Z

.field private mClassicStyle:Z

.field private mDefaultQueryDate:I

.field private mEndStation:Ljava/lang/String;

.field private mFilterType:I

.field private mFontSize:I

.field private mFuzzyQuery:Z

.field private mHasNewVersion:Z

.field private mHideTrain:Z

.field private mHistroy:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsShowRunchart:Z

.field private mLastCheckPermissionTime:J

.field private mLastTakeDate:J

.field private mLastUpdate:J

.field private mMaxOtherDays:J

.field private mMaxStudentDays:J

.field private mPurpose:Ljava/lang/String;

.field private mStartStation:Ljava/lang/String;

.field private mStations:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTrains:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateVer:Ljava/lang/String;

.field private mZwdQueryType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 76
    new-instance v0, Lcom/lltskb/lltskb/engine/LltSettings;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/LltSettings;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/LltSettings;->mInstance:Lcom/lltskb/lltskb/engine/LltSettings;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mIsShowRunchart:Z

    .line 29
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHideTrain:Z

    .line 30
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFuzzyQuery:Z

    .line 31
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mClassicStyle:Z

    const/16 v1, 0x10

    .line 32
    iput v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFontSize:I

    const-wide/16 v1, 0x0

    .line 33
    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastUpdate:J

    const/4 v3, -0x1

    .line 34
    iput v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFilterType:I

    .line 35
    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastTakeDate:J

    const/4 v1, 0x1

    .line 36
    iput v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mDefaultQueryDate:I

    .line 37
    iput v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mZwdQueryType:I

    const-wide/16 v1, 0x3c

    .line 38
    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    const-wide/16 v1, 0x1e

    .line 39
    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    const-string v1, "ADULT"

    .line 41
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    .line 43
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mBaiduCrashed:Z

    .line 45
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHasNewVersion:Z

    .line 93
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    .line 94
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    return-void
.end method

.method private containsHistroy(Ljava/lang/String;Ljava/lang/String;ZZI)Z
    .locals 2

    .line 275
    iget-object p3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    const/4 p4, 0x0

    if-nez p3, :cond_0

    return p4

    :cond_0
    const/4 p3, 0x0

    .line 277
    :goto_0
    iget-object p5, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p5}, Ljava/util/Vector;->size()I

    move-result p5

    if-ge p4, p5, :cond_2

    .line 278
    iget-object p5, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p5, p4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    .line 279
    iget-object v0, p5, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object p5, p5, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    invoke-virtual {p5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p5

    if-eqz p5, :cond_1

    .line 281
    iget-object p3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p3, p4}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    add-int/lit8 p4, p4, -0x1

    const/4 p3, 0x1

    :cond_1
    add-int/2addr p4, v1

    goto :goto_0

    :cond_2
    return p3
.end method

.method private genHistroy()Ljava/lang/String;
    .locals 7

    .line 195
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 196
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 197
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 198
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    .line 199
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    iget-boolean v4, v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bOnline:Z

    const-string v5, "0"

    const/4 v6, 0x1

    if-eqz v4, :cond_1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v5

    :goto_1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-boolean v4, v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bTicket:Z

    if-eqz v4, :cond_2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :cond_2
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    iget v2, v2, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->filter:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 204
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    sub-int/2addr v2, v6

    if-ge v1, v2, :cond_3

    const-string v2, ";"

    .line 205
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static get()Lcom/lltskb/lltskb/engine/LltSettings;
    .locals 1

    .line 89
    sget-object v0, Lcom/lltskb/lltskb/engine/LltSettings;->mInstance:Lcom/lltskb/lltskb/engine/LltSettings;

    return-object v0
.end method

.method private getDefaultStuDays()J
    .locals 5

    const-string v0, "20170226"

    .line 164
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 165
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 166
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long/2addr v2, v0

    const-wide/32 v0, 0x5265c00

    div-long/2addr v2, v0

    const-wide/16 v0, 0x1d

    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    move-wide v0, v2

    :goto_0
    return-wide v0
.end method

.method private historyToVector(Ljava/lang/String;Ljava/util/Vector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 102
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 103
    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->clear()V

    const-string v0, ";"

    .line 104
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 106
    :cond_1
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private loadHistroy(Ljava/lang/String;)V
    .locals 7

    .line 175
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    .line 176
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, ";"

    .line 178
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 179
    array-length v0, p1

    if-nez v0, :cond_1

    return-void

    .line 181
    :cond_1
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_5

    aget-object v3, p1, v2

    const-string v4, ","

    .line 182
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 183
    array-length v4, v3

    const/4 v5, 0x5

    if-ge v4, v5, :cond_2

    goto :goto_3

    .line 184
    :cond_2
    new-instance v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;-><init>(Lcom/lltskb/lltskb/engine/LltSettings;)V

    .line 185
    aget-object v5, v3, v1

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    const/4 v5, 0x1

    .line 186
    aget-object v6, v3, v5

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    const/4 v6, 0x2

    .line 187
    aget-object v6, v3, v6

    invoke-static {v6, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_1
    iput-boolean v6, v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bOnline:Z

    const/4 v6, 0x3

    .line 188
    aget-object v6, v3, v6

    invoke-static {v6, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_4

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    iput-boolean v5, v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bTicket:Z

    const/4 v5, 0x4

    .line 189
    aget-object v3, v3, v5

    invoke-static {v3, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v4, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->filter:I

    .line 190
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {v3, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method private save()V
    .locals 5

    .line 337
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "query"

    .line 338
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    .line 341
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 342
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStartStation:Ljava/lang/String;

    const-string v4, "start_point"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 343
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mEndStation:Ljava/lang/String;

    const-string v4, "end_point"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 345
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-direct {p0, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->vectorToHistroy(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "station"

    .line 346
    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 347
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-direct {p0, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->vectorToHistroy(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "train"

    .line 348
    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 349
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v2, "setting"

    .line 351
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 353
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 354
    iget v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFontSize:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "font-size"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 355
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHideTrain:Z

    const-string v2, "hide-train"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 356
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFuzzyQuery:Z

    const-string v2, "query-fuzzy"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 357
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mClassicStyle:Z

    const-string v2, "classic_style"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 358
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastUpdate:J

    const-string v3, "last_update_time"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 359
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastTakeDate:J

    const-string v3, "last_take_date"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 360
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mUpdateVer:Ljava/lang/String;

    const-string v2, "last_update_ver"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 361
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->genHistroy()Ljava/lang/String;

    move-result-object v1

    const-string v2, "query_histroy"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 362
    iget v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mZwdQueryType:I

    const-string v2, "zwd_query_type"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 363
    iget v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFilterType:I

    const-string v2, "filter_type"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 364
    iget v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mDefaultQueryDate:I

    const-string v2, "default_query_date"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 365
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHasNewVersion:Z

    const-string v2, "has_new_version"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 366
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mIsShowRunchart:Z

    const-string v2, "show_runchart"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 367
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    const-string v3, "max_student_date"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 368
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    const-string v3, "max_other_date"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 370
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    const-string v2, "book_purpose"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 372
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mBaiduCrashed:Z

    const-string v2, "baidu_crashed"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 373
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastCheckPermissionTime:J

    const-string v3, "last_check_permission_time"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 376
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private vectorToHistroy(Ljava/util/Vector;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public addHistroy(Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    .line 302
    :cond_0
    invoke-direct/range {p0 .. p5}, Lcom/lltskb/lltskb/engine/LltSettings;->containsHistroy(Ljava/lang/String;Ljava/lang/String;ZZI)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "LltSettings"

    const-string v1, "history already added"

    .line 303
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;-><init>(Lcom/lltskb/lltskb/engine/LltSettings;)V

    .line 307
    iput-object p1, v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    .line 308
    iput-object p2, v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    .line 309
    iput-boolean p3, v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bOnline:Z

    .line 310
    iput-boolean p4, v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->bTicket:Z

    .line 311
    iput p5, v0, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->filter:I

    .line 313
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    const/16 p2, 0xa

    if-le p1, p2, :cond_2

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_0

    .line 314
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    const/4 p2, 0x0

    invoke-virtual {p1, v0, p2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    return-void
.end method

.method public addStation(Ljava/lang/String;)V
    .locals 2

    .line 461
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 464
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 466
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    const/16 v0, 0xa

    if-le p1, v0, :cond_2

    .line 467
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 469
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public addTrain(Ljava/lang/String;)V
    .locals 2

    .line 492
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    .line 493
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 494
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 495
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 497
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    const/16 v0, 0xa

    if-le p1, v0, :cond_2

    .line 498
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 500
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public delStation(I)V
    .locals 1

    .line 473
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    :cond_1
    if-ltz p1, :cond_2

    .line 476
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 477
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 479
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public delTrain(I)V
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 506
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    :cond_1
    if-ltz p1, :cond_2

    .line 507
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 510
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public deleteHistroy(I)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    if-gez p1, :cond_1

    .line 325
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->removeAllElements()V

    goto :goto_0

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 328
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    :cond_2
    :goto_1
    return-void
.end method

.method public getBaiduCrashed()Z
    .locals 1

    .line 627
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mBaiduCrashed:Z

    return v0
.end method

.method public getDefaultMaxDays()J
    .locals 2

    .line 406
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    const-string v1, "0X00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    return-wide v0

    .line 410
    :cond_0
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    return-wide v0
.end method

.method public getDefaultQueryDate()I
    .locals 1

    .line 235
    iget v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mDefaultQueryDate:I

    return v0
.end method

.method public getEndStation()Ljava/lang/String;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mEndStation:Ljava/lang/String;

    return-object v0
.end method

.method public getFilterType()I
    .locals 1

    .line 397
    iget v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFilterType:I

    return v0
.end method

.method public getFontSize()I
    .locals 1

    .line 571
    iget v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFontSize:I

    return v0
.end method

.method public getHistroy()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;",
            ">;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHistroy:Ljava/util/Vector;

    return-object v0
.end method

.method public getLastTakeDate()J
    .locals 2

    .line 601
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastTakeDate:J

    return-wide v0
.end method

.method public getLastUpdate()J
    .locals 2

    .line 586
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastUpdate:J

    return-wide v0
.end method

.method public getMaxOtherDays()J
    .locals 2

    .line 427
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    return-wide v0
.end method

.method public getMaxStudentDays()J
    .locals 2

    .line 418
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    return-wide v0
.end method

.method public getPurpose()Ljava/lang/String;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    return-object v0
.end method

.method public getStartStation()Ljava/lang/String;
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStartStation:Ljava/lang/String;

    return-object v0
.end method

.method public getStations()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 454
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    return-object v0
.end method

.method public getTrains()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 485
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    return-object v0
.end method

.method public getUpdateVer()Ljava/lang/String;
    .locals 1

    .line 613
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mUpdateVer:Ljava/lang/String;

    return-object v0
.end method

.method public getZwdQueryType()I
    .locals 1

    .line 253
    iget v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mZwdQueryType:I

    return v0
.end method

.method public hasNewVersion()Z
    .locals 1

    .line 226
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHasNewVersion:Z

    return v0
.end method

.method public isClassicStyle()Z
    .locals 1

    .line 556
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mClassicStyle:Z

    return v0
.end method

.method public isFuzzyQuery()Z
    .locals 1

    .line 541
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFuzzyQuery:Z

    return v0
.end method

.method public isHideTrain()Z
    .locals 1

    .line 517
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHideTrain:Z

    return v0
.end method

.method public isShowRunchart()Z
    .locals 1

    .line 534
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mIsShowRunchart:Z

    return v0
.end method

.method public load()V
    .locals 7

    .line 120
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "query"

    .line 122
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    const-string v3, "start_point"

    const-string v4, "\u4e0a\u6d77"

    .line 124
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStartStation:Ljava/lang/String;

    const-string v3, "\u5317\u4eac"

    const-string v4, "end_point"

    .line 125
    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mEndStation:Ljava/lang/String;

    const-string v4, "station"

    .line 128
    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 129
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStations:Ljava/util/Vector;

    invoke-direct {p0, v3, v4}, Lcom/lltskb/lltskb/engine/LltSettings;->historyToVector(Ljava/lang/String;Ljava/util/Vector;)V

    const-string v3, "train"

    const-string v4, "T64"

    .line 130
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mTrains:Ljava/util/Vector;

    invoke-direct {p0, v2, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->historyToVector(Ljava/lang/String;Ljava/util/Vector;)V

    const-string v2, "setting"

    .line 133
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v2, "font-size"

    const-string v3, "16"

    .line 135
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFontSize:I

    const-string v2, "hide-train"

    .line 137
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHideTrain:Z

    const/4 v2, 0x1

    const-string v3, "show_runchart"

    .line 138
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mIsShowRunchart:Z

    const-string v3, "query-fuzzy"

    .line 139
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFuzzyQuery:Z

    const-string v3, "classic_style"

    .line 140
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mClassicStyle:Z

    const-wide/16 v3, 0x0

    const-string v5, "last_update_time"

    .line 141
    invoke-interface {v0, v5, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastUpdate:J

    const-string v5, "last_take_date"

    .line 142
    invoke-interface {v0, v5, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastTakeDate:J

    const/4 v5, -0x1

    const-string v6, "filter_type"

    .line 143
    invoke-interface {v0, v6, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFilterType:I

    const-string v5, "default_query_date"

    .line 144
    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mDefaultQueryDate:I

    const-string v2, "zwd_query_type"

    .line 145
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mZwdQueryType:I

    const-string v2, "last_update_ver"

    const-string v5, "1.9.5"

    .line 146
    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mUpdateVer:Ljava/lang/String;

    const-string v2, "baidu_crashed"

    .line 148
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mBaiduCrashed:Z

    const-string v2, "has_new_version"

    .line 150
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHasNewVersion:Z

    .line 152
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultStuDays()J

    move-result-wide v1

    const-string v5, "max_student_date"

    invoke-interface {v0, v5, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    const-wide/16 v1, 0x1d

    const-string v5, "max_other_date"

    .line 153
    invoke-interface {v0, v5, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    const-string v1, "book_purpose"

    const-string v2, "ADULT"

    .line 154
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    const-string v1, "last_check_permission_time"

    .line 157
    invoke-interface {v0, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastCheckPermissionTime:J

    const-string v1, "query_histroy"

    const-string v2, ""

    .line 159
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->loadHistroy(Ljava/lang/String;)V

    return-void
.end method

.method public setBaiduCrashed(Z)V
    .locals 0

    .line 622
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mBaiduCrashed:Z

    .line 623
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setClassicStyle(Z)V
    .locals 0

    .line 563
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mClassicStyle:Z

    .line 564
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setContext()V
    .locals 0

    return-void
.end method

.method public setDefaultQueryDate(I)V
    .locals 0

    .line 230
    iput p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mDefaultQueryDate:I

    .line 231
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setEndStation(Ljava/lang/String;)V
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mEndStation:Ljava/lang/String;

    .line 447
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setFilterType(I)V
    .locals 0

    .line 401
    iput p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFilterType:I

    .line 402
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setFontSize(I)V
    .locals 0

    .line 578
    iput p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFontSize:I

    .line 579
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setFuzzyQuery(Z)V
    .locals 0

    .line 548
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mFuzzyQuery:Z

    .line 549
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setHasNewVersion(Z)V
    .locals 0

    .line 221
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHasNewVersion:Z

    .line 222
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setHideTrain(Z)V
    .locals 0

    .line 524
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mHideTrain:Z

    .line 525
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setLastCheckPermissionTime()V
    .locals 2

    .line 244
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastCheckPermissionTime:J

    .line 245
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setLastTakeDate(J)V
    .locals 0

    .line 608
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastTakeDate:J

    .line 609
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setLastUpdate(J)V
    .locals 0

    .line 593
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastUpdate:J

    .line 594
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setMaxOtherDays(J)V
    .locals 0

    .line 431
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxOtherDays:J

    .line 432
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setMaxStudentDays(J)V
    .locals 0

    .line 422
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mMaxStudentDays:J

    .line 423
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setPurpose(Ljava/lang/String;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mPurpose:Ljava/lang/String;

    .line 217
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setShowRunChart(Z)V
    .locals 0

    .line 529
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mIsShowRunchart:Z

    .line 530
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setStartStation(Ljava/lang/String;)V
    .locals 0

    .line 392
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mStartStation:Ljava/lang/String;

    .line 393
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setUpdateVer(Ljava/lang/String;)V
    .locals 0

    .line 617
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mUpdateVer:Ljava/lang/String;

    .line 618
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public setZwdQueryType(I)V
    .locals 0

    .line 239
    iput p1, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mZwdQueryType:I

    .line 240
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/LltSettings;->save()V

    return-void
.end method

.method public shouldCheckPermissionTime()Z
    .locals 5

    .line 249
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/lltskb/lltskb/engine/LltSettings;->mLastCheckPermissionTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
