.class public Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "LeftTicketQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LeftTicketQuery"


# instance fields
.field private mDate:Ljava/lang/String;

.field private mErrorMsg:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mPurpose:Ljava/lang/String;

.field private mResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mTo:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 32
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method private parseQueryResult(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "datas"

    const-string v1, "data"

    .line 134
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0xa

    if-ge v2, v4, :cond_0

    goto/16 :goto_3

    :cond_0
    const-string v2, "queryLeftNewDTO"

    .line 139
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    .line 141
    :try_start_0
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->parseQueryResult1(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    return v4

    :catch_0
    move-exception p1

    .line 144
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    return v3

    .line 151
    :cond_1
    :try_start_1
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 155
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_2

    goto/16 :goto_2

    .line 159
    :cond_2
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 161
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    const/16 v5, 0xc8

    if-eq v2, v5, :cond_3

    return v3

    :cond_3
    const/4 v2, 0x0

    .line 166
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const-string v6, "message"

    if-eqz v5, :cond_8

    .line 167
    :try_start_2
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    if-eqz p1, :cond_5

    .line 169
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 170
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    goto :goto_0

    .line 172
    :cond_4
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v3

    :cond_5
    :goto_0
    if-nez v2, :cond_6

    return v3

    .line 184
    :cond_6
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    const/4 p1, 0x0

    .line 185
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_7

    .line 186
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 187
    invoke-virtual {v2, p1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v5, "train_no"

    .line 198
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const-string v5, "station_train_code"

    .line 199
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const-string v5, "start_station_telecode"

    .line 200
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const-string v5, "start_station_name"

    .line 201
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    const-string v5, "end_station_telecode"

    .line 202
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    const-string v5, "end_station_name"

    .line 203
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    const-string v5, "from_station_telecode"

    .line 204
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const-string v5, "from_station_name"

    .line 205
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    const-string v5, "to_station_telecode"

    .line 206
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const-string v5, "to_station_name"

    .line 207
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const-string v5, "start_time"

    .line 208
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v5, "arrive_time"

    .line 209
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const-string v5, "day_difference"

    .line 210
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->day_difference:Ljava/lang/String;

    const-string v5, "train_class_name"

    .line 211
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_class_name:Ljava/lang/String;

    const-string v5, "lishi"

    .line 212
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v5, "canWebBuy"

    .line 213
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const-string v5, "lishiValue"

    .line 214
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishiValue:Ljava/lang/String;

    const-string v5, "yp_info"

    .line 215
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v5, "control_train_day"

    .line 216
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_train_day:Ljava/lang/String;

    const-string v5, "start_train_date"

    .line 217
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const-string v5, "seat_feature"

    .line 218
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_feature:Ljava/lang/String;

    const-string v5, "yp_ex"

    .line 219
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const-string v5, "train_seat_feature"

    .line 220
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const-string v5, "seat_types"

    .line 221
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    const-string v5, "location_code"

    .line 222
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const-string v5, "from_station_no"

    .line 223
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const-string v5, "to_station_no"

    .line 224
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const-string v5, "control_day"

    .line 225
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_day:Ljava/lang/String;

    const-string v5, "sale_time"

    .line 226
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->sale_time:Ljava/lang/String;

    const-string v5, "is_support_card"

    .line 227
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const-string v5, "controlled_train_flag"

    .line 228
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const-string v5, "controlled_train_message"

    .line 229
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    const-string v5, "note"

    .line 230
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->note:Ljava/lang/String;

    const-string v5, "gg_num"

    .line 232
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const-string v5, "gr_num"

    .line 233
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const-string v5, "qt_num"

    .line 234
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const-string v5, "rw_num"

    .line 235
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const-string v5, "rz_num"

    .line 236
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const-string v5, "tz_num"

    .line 237
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const-string v5, "wz_num"

    .line 238
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const-string v5, "yb_num"

    .line 239
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const-string v5, "yw_num"

    .line 240
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const-string v5, "yz_num"

    .line 241
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v5, "ze_num"

    .line 242
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const-string v5, "zy_num"

    .line 243
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const-string v5, "swz_num"

    .line 244
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const-string v5, "secretStr"

    .line 246
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const-string v5, "buttonTextInfo"

    .line 247
    invoke-virtual {p0, v1, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    .line 249
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_1

    :cond_7
    return v4

    .line 176
    :cond_8
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 177
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_9
    :goto_2
    return v3

    :catch_1
    move-exception p1

    .line 258
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 259
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_3

    :catch_2
    move-exception p1

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LeftTicketQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 256
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    :cond_a
    :goto_3
    return v3
.end method

.method private parseQueryResult1(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "data"

    const/4 v1, 0x0

    .line 266
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 269
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 270
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_0

    goto/16 :goto_1

    .line 274
    :cond_0
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 276
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_5

    .line 281
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "message"

    if-eqz v2, :cond_3

    .line 282
    :try_start_1
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 283
    instance-of v2, v2, Lorg/json/JSONArray;

    if-eqz v2, :cond_2

    .line 284
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    .line 298
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    .line 299
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 300
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 301
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "queryLeftNewDTO"

    .line 302
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "train_no"

    .line 304
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const-string v6, "station_train_code"

    .line 305
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const-string v6, "start_station_telecode"

    .line 306
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const-string v6, "start_station_name"

    .line 307
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    const-string v6, "end_station_telecode"

    .line 308
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    const-string v6, "end_station_name"

    .line 309
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    const-string v6, "from_station_telecode"

    .line 310
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const-string v6, "from_station_name"

    .line 311
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    const-string v6, "to_station_telecode"

    .line 312
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const-string v6, "to_station_name"

    .line 313
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const-string v6, "start_time"

    .line 314
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v6, "arrive_time"

    .line 315
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const-string v6, "day_difference"

    .line 316
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->day_difference:Ljava/lang/String;

    const-string v6, "train_class_name"

    .line 317
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_class_name:Ljava/lang/String;

    const-string v6, "lishi"

    .line 318
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v6, "canWebBuy"

    .line 319
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const-string v6, "lishiValue"

    .line 320
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishiValue:Ljava/lang/String;

    const-string v6, "yp_info"

    .line 321
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v6, "control_train_day"

    .line 322
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_train_day:Ljava/lang/String;

    const-string v6, "start_train_date"

    .line 323
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const-string v6, "seat_feature"

    .line 324
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_feature:Ljava/lang/String;

    const-string v6, "yp_ex"

    .line 325
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const-string v6, "train_seat_feature"

    .line 326
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const-string v6, "seat_types"

    .line 327
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    const-string v6, "location_code"

    .line 328
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const-string v6, "from_station_no"

    .line 329
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const-string v6, "to_station_no"

    .line 330
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const-string v6, "control_day"

    .line 331
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_day:Ljava/lang/String;

    const-string v6, "sale_time"

    .line 332
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->sale_time:Ljava/lang/String;

    const-string v6, "is_support_card"

    .line 333
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const-string v6, "controlled_train_flag"

    .line 334
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const-string v6, "controlled_train_message"

    .line 335
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    const-string v6, "note"

    .line 336
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->note:Ljava/lang/String;

    const-string v6, "gg_num"

    .line 338
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const-string v6, "gr_num"

    .line 339
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const-string v6, "qt_num"

    .line 340
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const-string v6, "rw_num"

    .line 341
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const-string v6, "rz_num"

    .line 342
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const-string v6, "tz_num"

    .line 343
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const-string v6, "wz_num"

    .line 344
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const-string v6, "yb_num"

    .line 345
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const-string v6, "yw_num"

    .line 346
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const-string v6, "yz_num"

    .line 347
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v6, "ze_num"

    .line 348
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const-string v6, "zy_num"

    .line 349
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const-string v6, "swz_num"

    .line 350
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const-string v5, "secretStr"

    .line 352
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const-string v5, "buttonTextInfo"

    .line 353
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    .line 355
    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    return-object v0

    .line 286
    :cond_2
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 287
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 288
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_3
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 292
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 293
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_4
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v0, "\u672a\u77e5\u9519\u8bef"

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 278
    :cond_5
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v0, "\u7f51\u7edc\u95ee\u9898"

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_6
    :goto_1
    return-object v1

    :catch_0
    move-exception p1

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "LeftTicketQuery"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return-object v1
.end method

.method private queryTicket()Z
    .locals 6

    .line 70
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mTo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "\u3011\u65e0\u6cd5\u627e\u5230"

    const-string v4, "\u8f66\u7ad9\u3010"

    const/4 v5, 0x0

    if-eqz v2, :cond_0

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v5

    .line 78
    :cond_0
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mTo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v5

    .line 89
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "purpose_codes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mPurpose:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&queryDate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&from_station="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&to_station="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueryLeftTicket url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LeftTicketQuery"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v3, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->parseQueryResult(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "QueryLeftTicket ret="

    if-nez v1, :cond_2

    .line 109
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :catch_0
    move-exception v0

    .line 101
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QueryLeftTicket message="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v5
.end method


# virtual methods
.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    return-object v0
.end method

.method public queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 44
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mFrom:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mTo:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mDate:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mPurpose:Ljava/lang/String;

    const/4 p1, 0x0

    .line 48
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    .line 49
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->queryTicket()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 50
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result p2

    if-nez p2, :cond_1

    .line 51
    :cond_0
    new-instance p2, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;

    invoke-direct {p2}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;-><init>()V

    .line 53
    :try_start_0
    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mFrom:Ljava/lang/String;

    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mTo:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mDate:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mPurpose:Ljava/lang/String;

    invoke-virtual {p2, p3, p4, v0, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p3

    .line 56
    invoke-virtual {p3}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 57
    invoke-virtual {p3}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    .line 59
    :goto_0
    invoke-virtual {p2}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->mResult:Ljava/util/Vector;

    :cond_1
    return p1
.end method
