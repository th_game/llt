.class public Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;
.super Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;
.source "TrainOldZwdQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TrainOldZwdQuery"


# instance fields
.field private isCancelled:Z

.field private mSelectedRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStation:Ljava/lang/String;

.field private mTicketList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTrain:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;ILjava/lang/String;)V
    .locals 1

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    const/4 p1, 0x0

    .line 34
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->isCancelled:Z

    .line 41
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    .line 42
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    const/16 v0, 0x2f

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result p2

    if-ltz p2, :cond_0

    .line 44
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    .line 46
    :cond_0
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mType:Ljava/lang/String;

    .line 47
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->isCancelled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->isCancelled:Z

    return p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->refreshList()V

    return-void
.end method

.method private refreshList()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$1;-><init>(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 237
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->isCancelled:Z

    return-void
.end method

.method public doQuery()Ljava/lang/String;
    .locals 7

    .line 78
    :try_start_0
    new-instance v0, Lcom/lltskb/lltskb/engine/QueryCC;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    .line 79
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 81
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit16 v4, v4, 0x2710

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/2addr v5, v2

    mul-int/lit8 v5, v5, 0x64

    add-int/2addr v4, v5

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v4, v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 82
    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    .line 84
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    sget v4, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v0, v3, v4}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 85
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v2, :cond_1

    .line 86
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 87
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 88
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;-><init>()V

    .line 89
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 90
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_train_code:Ljava/lang/String;

    const/16 v6, 0xe

    .line 91
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    const/4 v6, 0x4

    .line 92
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const/4 v6, 0x3

    .line 93
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    const/16 v6, 0xd

    .line 94
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    .line 95
    invoke-virtual {v3, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    goto :goto_1

    .line 99
    :cond_1
    invoke-super {p0}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->doQuery()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    .line 104
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mDisplayResult:Ljava/util/Vector;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u6b21\u6b63\u665a\u70b9\u4fe1\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onItemClicked(I)V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 248
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 250
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mSelectedRow:Ljava/util/Map;

    .line 251
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mStation:Ljava/lang/String;

    .line 253
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mType:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->queryZWD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "*>;ZZ)",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 121
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 122
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 124
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    .line 126
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->isSelected:Z

    if-nez v3, :cond_1

    goto/16 :goto_4

    .line 128
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 129
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    const-string v5, "station"

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const-string v5, "--"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v6, ""

    if-nez v4, :cond_2

    .line 133
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " \u5230"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    move-object v4, v6

    .line 135
    :goto_1
    iget-object v7, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 136
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 137
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_3

    const-string v4, "<br/>"

    goto :goto_2

    :cond_3
    const-string v4, "\n"

    :goto_2
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 138
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " \u5f00"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_5
    const-string v7, "stoptime"

    .line 140
    invoke-interface {v3, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 143
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 144
    array-length v5, v4

    const/4 v7, 0x1

    if-le v5, v7, :cond_6

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v4, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u65f6"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v7

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u5206"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_3

    .line 147
    :cond_6
    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    :cond_7
    :goto_3
    const-string v2, "time"

    .line 151
    invoke-interface {v3, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ticket"

    const-string v4, "\u6b63\u5728\u67e5\u8be2\u4e2d"

    .line 153
    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 157
    :cond_8
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->onItemClicked(I)V

    .line 159
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    return-object p1
.end method

.method public queryZWD(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v0, 0x0

    .line 241
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->isCancelled:Z

    .line 242
    new-instance v0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mSelectedRow:Ljava/util/Map;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mStation:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTrain:Ljava/lang/String;

    move-object v1, v0

    move-object v2, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;-><init>(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->start()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .line 258
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 260
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    .line 261
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_1

    return-object v1

    .line 263
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    .line 264
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 265
    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    if-eqz v4, :cond_3

    .line 266
    iget-boolean v4, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->isSelected:Z

    if-nez v4, :cond_2

    goto :goto_1

    .line 268
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    const-string v5, "station"

    .line 269
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "stoptime"

    .line 270
    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v7, "<br/>"

    const-string v8, " "

    .line 271
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 273
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "ticket"

    .line 274
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n\n"

    .line 275
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 277
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
