.class public final Lcom/lltskb/lltskb/engine/online/dto/QueueNum;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0014\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0008J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J7\u0010\u0019\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR \u0010\u0007\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u000e\"\u0004\u0008\u0012\u0010\u0010R \u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u000e\"\u0004\u0008\u0014\u0010\u0010\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/QueueNum;",
        "",
        "station_train_code",
        "",
        "train_date",
        "queue_num",
        "",
        "seat_type_code",
        "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V",
        "getQueue_num",
        "()I",
        "setQueue_num",
        "(I)V",
        "getSeat_type_code",
        "()Ljava/lang/String;",
        "setSeat_type_code",
        "(Ljava/lang/String;)V",
        "getStation_train_code",
        "setStation_train_code",
        "getTrain_date",
        "setTrain_date",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private queue_num:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "queue_num"
    .end annotation
.end field

.field private seat_type_code:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "seat_type_code"
    .end annotation
.end field

.field private station_train_code:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "station_train_code"
    .end annotation
.end field

.field private train_date:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "train_date"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    iput p3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/QueueNum;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/QueueNum;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->copy(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueueNum;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueueNum;
    .locals 1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    iget v3, p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public final getQueue_num()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    return v0
.end method

.method public final getSeat_type_code()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    return-object v0
.end method

.method public final getStation_train_code()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrain_date()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final setQueue_num(I)V
    .locals 0

    .line 73
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    return-void
.end method

.method public final setSeat_type_code(Ljava/lang/String;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    return-void
.end method

.method public final setStation_train_code(Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    return-void
.end method

.method public final setTrain_date(Ljava/lang/String;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QueueNum(station_train_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", train_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->train_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", queue_num="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->queue_num:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", seat_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueueNum;->seat_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
