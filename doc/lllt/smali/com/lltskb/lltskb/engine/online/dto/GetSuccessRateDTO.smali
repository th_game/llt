.class public final Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J/\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0008H\u00d6\u0001R \u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR$\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;",
        "",
        "status",
        "",
        "data",
        "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;",
        "messages",
        "",
        "",
        "(ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;)V",
        "getData",
        "()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;",
        "setData",
        "(Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;)V",
        "getMessages",
        "()Ljava/util/List;",
        "setMessages",
        "(Ljava/util/List;)V",
        "getStatus",
        "()Z",
        "setStatus",
        "(Z)V",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field private messages:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "messages"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private status:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messages"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->copy(ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;)Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    return v0
.end method

.method public final component2()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    return-object v0
.end method

.method public final copy(ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;)Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;"
        }
    .end annotation

    const-string v0, "messages"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    invoke-direct {v0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;-><init>(ZLcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    iget-boolean v3, p1, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public final getData()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    return-object v0
.end method

.method public final getMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    return-object v0
.end method

.method public final getStatus()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public final setData(Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;)V
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    return-void
.end method

.method public final setMessages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    return-void
.end method

.method public final setStatus(Z)V
    .locals 0

    .line 8
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetSuccessRateDTO(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->status:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->data:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->messages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
