.class public Lcom/lltskb/lltskb/engine/online/dto/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mRemeberName:Z

.field private mRemeberPass:Z

.field private mSignKey:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberPass:Z

    .line 14
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberName:Z

    return-void
.end method


# virtual methods
.method public getAccount()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getSignKey()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mSignKey:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public initAccount(Lorg/json/JSONObject;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "userId"

    .line 87
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mAccount:Ljava/lang/String;

    const-string v0, "userName"

    .line 88
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mUserName:Ljava/lang/String;

    const-string v0, "userPwd"

    .line 89
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    const-string v0, "remPass"

    .line 90
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberPass:Z

    const-string v0, "remName"

    .line 91
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberName:Z

    const-string v0, "signKey"

    .line 92
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mSignKey:Ljava/lang/String;

    .line 93
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v0, "adfja3290alks*&^t83qelkasd823rhl"

    .line 94
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/CyptoUtils;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public isRemeberName()Z
    .locals 1

    .line 58
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberName:Z

    return v0
.end method

.method public isRemeberPass()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberPass:Z

    return v0
.end method

.method public setAccount(Ljava/lang/String;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mAccount:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    return-void
.end method

.method public setRemeberName(Z)V
    .locals 0

    .line 62
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberName:Z

    return-void
.end method

.method public setRemeberPass(Z)V
    .locals 0

    .line 54
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberPass:Z

    return-void
.end method

.method public setSignKey(Ljava/lang/String;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mSignKey:Ljava/lang/String;

    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mUserName:Ljava/lang/String;

    return-void
.end method

.method public toJSONObject()Lorg/json/JSONObject;
    .locals 3

    .line 68
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "userId"

    .line 69
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "userName"

    .line 70
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mUserName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "adfja3290alks*&^t83qelkasd823rhl"

    .line 71
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mPassword:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/CyptoUtils;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "userPwd"

    .line 72
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "remPass"

    .line 73
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberPass:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "remName"

    .line 74
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mRemeberName:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "signKey"

    .line 75
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->mSignKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
