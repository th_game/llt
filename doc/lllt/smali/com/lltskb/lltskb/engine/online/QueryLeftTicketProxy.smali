.class public Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;
.super Ljava/lang/Object;
.source "QueryLeftTicketProxy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryLeftTicketProxy"

.field private static volatile mMethod:I = -0x1


# instance fields
.field private mDate:Ljava/lang/String;

.field private mErrMsg:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private volatile mHasResult:Z

.field private volatile mNormalReturned:Z

.field private mOrderQueryFirst:Z

.field private volatile mOrderReturned:Z

.field private mPurpose:Ljava/lang/String;

.field private mResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mTo:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 23
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    .line 24
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    const/4 v0, 0x1

    .line 33
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderQueryFirst:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;)Z
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryWeiXinMethod()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;)Z
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryOrderMethod()Z

    move-result p0

    return p0
.end method

.method private queryNormalMethod()Z
    .locals 6

    const/4 v0, 0x0

    .line 147
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    .line 148
    new-instance v1, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;-><init>()V

    .line 149
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mFrom:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mTo:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mPurpose:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 152
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->isToday(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderQueryFirst:Z

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x5dc

    .line 154
    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    .line 156
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 160
    :cond_0
    :goto_0
    monitor-enter p0

    const/4 v3, 0x1

    .line 161
    :try_start_1
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    .line 162
    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v4, :cond_7

    if-eqz v2, :cond_7

    .line 164
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    .line 165
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    if-eqz v2, :cond_3

    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 167
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 168
    sput v0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    goto :goto_2

    .line 169
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 170
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    :cond_5
    :goto_2
    const-string v1, "QueryLeftTicketProxy"

    .line 172
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queryNormalMethod count="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " hasResult="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 174
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    monitor-exit p0

    return v0

    .line 175
    :cond_7
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v2, :cond_9

    .line 176
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 178
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 179
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 181
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 182
    monitor-exit p0

    return v0

    .line 183
    :cond_9
    sget v2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    if-nez v2, :cond_a

    .line 184
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryOrderMethod()Z

    move-result v0

    monitor-exit p0

    return v0

    .line 186
    :cond_a
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 187
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 189
    :cond_b
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 191
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private queryOrderMethod()Z
    .locals 6

    const/4 v0, 0x0

    .line 195
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    .line 196
    new-instance v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;-><init>()V

    .line 199
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mFrom:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mTo:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mPurpose:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 202
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 204
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    const/4 v2, 0x0

    .line 208
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->isToday(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderQueryFirst:Z

    if-nez v3, :cond_1

    :cond_0
    const-wide/16 v3, 0xdac

    .line 210
    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    .line 212
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 216
    :cond_1
    :goto_1
    monitor-enter p0

    const/4 v3, 0x1

    .line 217
    :try_start_2
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    .line 218
    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v4, :cond_8

    if-eqz v2, :cond_8

    .line 220
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    .line 221
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    if-eqz v2, :cond_4

    :cond_3
    const/4 v2, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 222
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 223
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 224
    sput v3, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    goto :goto_3

    .line 225
    :cond_5
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 226
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    :cond_6
    :goto_3
    const-string v1, "QueryLeftTicketProxy"

    .line 229
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queryOrderMethod count="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " hasResult="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v0, 0x1

    :cond_7
    monitor-exit p0

    return v0

    .line 231
    :cond_8
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    if-eqz v2, :cond_a

    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v2, :cond_a

    .line 232
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 233
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 234
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 236
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 237
    monitor-exit p0

    return v0

    .line 238
    :cond_a
    sget v2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    if-ne v2, v3, :cond_b

    .line 239
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryNormalMethod()Z

    move-result v0

    monitor-exit p0

    return v0

    .line 241
    :cond_b
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 242
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 245
    :cond_c
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 247
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private queryWeiXinMethod()Z
    .locals 6

    const/4 v0, 0x0

    .line 100
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    .line 101
    new-instance v1, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;-><init>()V

    .line 102
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mFrom:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mTo:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mPurpose:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 105
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->isToday(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderQueryFirst:Z

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x5dc

    .line 107
    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    .line 109
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 113
    :cond_0
    :goto_0
    monitor-enter p0

    const/4 v3, 0x1

    .line 114
    :try_start_1
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mNormalReturned:Z

    .line 115
    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v4, :cond_7

    if-eqz v2, :cond_7

    .line 117
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    .line 118
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    if-eqz v2, :cond_3

    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 120
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 121
    sput v0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    goto :goto_2

    .line 122
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 123
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    :cond_5
    :goto_2
    const-string v1, "QueryLeftTicketProxy"

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queryNormalMethod count="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " hasResult="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 127
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    monitor-exit p0

    return v0

    .line 128
    :cond_7
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mOrderReturned:Z

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez v2, :cond_9

    .line 129
    iput-boolean v3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    .line 131
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 132
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 134
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 135
    monitor-exit p0

    return v0

    .line 136
    :cond_9
    sget v2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    if-nez v2, :cond_a

    .line 137
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryOrderMethod()Z

    move-result v0

    monitor-exit p0

    return v0

    .line 139
    :cond_a
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 140
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    .line 142
    :cond_b
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 144
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mErrMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    return-object v0
.end method

.method public queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mFrom:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mTo:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mDate:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mPurpose:Ljava/lang/String;

    const/4 p1, 0x0

    .line 49
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    const/4 p2, -0x1

    .line 51
    sput p2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    .line 52
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "queryLeftTicket method="

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p4, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string p4, "QueryLeftTicketProxy"

    invoke-static {p4, p3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    sget p3, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    const/4 p4, 0x1

    if-ne p3, p2, :cond_2

    .line 54
    new-instance p2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy$1;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy$1;-><init>(Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;)V

    .line 65
    new-instance p3, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy$2;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy$2;-><init>(Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;)V

    .line 75
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V

    .line 76
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    .line 78
    :goto_0
    iget-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mHasResult:Z

    if-nez p2, :cond_0

    .line 80
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 82
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p2
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p2

    .line 86
    invoke-virtual {p2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 90
    :cond_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mResult:Ljava/util/Vector;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/Vector;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    const/4 p1, 0x1

    :cond_1
    return p1

    .line 91
    :cond_2
    sget p2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    if-nez p2, :cond_3

    .line 92
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryWeiXinMethod()Z

    move-result p1

    return p1

    .line 93
    :cond_3
    sget p2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->mMethod:I

    if-ne p2, p4, :cond_4

    .line 94
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryOrderMethod()Z

    move-result p1

    :cond_4
    return p1
.end method
