.class public Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "BigScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;
    }
.end annotation


# static fields
.field private static final BIG_SCREEN_FRAGMENT_TAG:Ljava/lang/String; = "BIG_SCREEN_FRAGMENT_TAG"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private station:Ljava/lang/String;

.field private stationView:Landroid/widget/Button;

.field private switchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

.field private tableLayout:Landroid/widget/TableLayout;

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    const-class v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->type:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->showResult()V

    return-void
.end method

.method private clearContent()V
    .locals 6

    .line 152
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->type:Ljava/lang/String;

    const-string v1, "D"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 153
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableRow;

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    .line 155
    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 156
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d0054

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x2

    .line 158
    invoke-virtual {v1, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 159
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    if-eqz v0, :cond_0

    const v5, 0x7f0d0282

    goto :goto_0

    :cond_0
    const v5, 0x7f0d0056

    :goto_0
    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x3

    .line 161
    invoke-virtual {v1, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    if-eqz v0, :cond_1

    const v0, 0x7f0d02dc

    goto :goto_1

    :cond_1
    const v0, 0x7f0d0113

    :goto_1
    invoke-virtual {v3, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_3

    .line 165
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->removeViewAt(I)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method public static synthetic lambda$ZK3OO0yOjBTkiDgx5WC-anNWmUA(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->selectStation()V

    return-void
.end method

.method private refresh()V
    .locals 4

    .line 144
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "refresh"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->switchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "A"

    goto :goto_0

    :cond_0
    const-string v0, "D"

    :goto_0
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->type:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 147
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    .line 148
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private selectStation()V
    .locals 3

    .line 112
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "selectStation"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getStationMap()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 115
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 126
    :cond_0
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 128
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0d0263

    .line 129
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 130
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$1ZNz5d9V7hYGo70YsdDAxT2Np-4;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$1ZNz5d9V7hYGo70YsdDAxT2Np-4;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;[Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 136
    :try_start_0
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 138
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    .line 116
    :cond_1
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$xQyTUL9aZ8H1YOrJxbN7fE4MyhE;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$xQyTUL9aZ8H1YOrJxbN7fE4MyhE;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private showResult()V
    .locals 16

    move-object/from16 v0, p0

    .line 170
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getBigScreenDataList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_e

    .line 172
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    goto/16 :goto_b

    .line 180
    :cond_0
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->type:Ljava/lang/String;

    const-string v5, "D"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 184
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v5, v3}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TableRow;

    const/4 v6, 0x3

    const/4 v7, 0x2

    if-eqz v5, :cond_3

    .line 186
    invoke-virtual {v5, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 187
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v9

    const v10, 0x7f0d0054

    invoke-virtual {v9, v10}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    invoke-virtual {v5, v7}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 190
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v9

    if-eqz v4, :cond_1

    const v10, 0x7f0d0282

    goto :goto_0

    :cond_1
    const v10, 0x7f0d0056

    :goto_0
    invoke-virtual {v9, v10}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 193
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v8

    if-eqz v4, :cond_2

    const v9, 0x7f0d02dc

    goto :goto_1

    :cond_2
    const v9, 0x7f0d0113

    :goto_1
    invoke-virtual {v8, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const/4 v5, 0x5

    .line 196
    invoke-static {v0, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v8

    .line 197
    :goto_2
    iget-object v9, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v9}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    add-int/2addr v10, v2

    const v11, 0x7f060021

    if-ge v9, v10, :cond_5

    .line 198
    new-instance v9, Landroid/widget/TableRow;

    invoke-direct {v9, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    const/16 v10, 0x11

    .line 199
    invoke-virtual {v9, v10}, Landroid/widget/TableRow;->setGravity(I)V

    .line 200
    invoke-virtual {v9, v8, v8, v8, v8}, Landroid/widget/TableRow;->setPadding(IIII)V

    const v12, 0x7f06008a

    .line 201
    invoke-static {v0, v12}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v12

    invoke-virtual {v9, v12}, Landroid/widget/TableRow;->setBackgroundColor(I)V

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v5, :cond_4

    .line 203
    new-instance v13, Landroid/widget/TextView;

    invoke-direct {v13, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070090

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v13, v3, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 205
    invoke-static {v0, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 206
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 207
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 208
    invoke-virtual {v9, v13}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 211
    :cond_4
    iget-object v10, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v10, v9}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 214
    :cond_5
    :goto_4
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v5}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v8, v2

    if-le v5, v8, :cond_6

    .line 215
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v5}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v8

    sub-int/2addr v8, v2

    invoke-virtual {v5, v8}, Landroid/widget/TableLayout;->removeViewAt(I)V

    goto :goto_4

    :cond_6
    const v5, 0x7f0600de

    .line 218
    invoke-static {v0, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v5

    const v8, 0x7f060067

    .line 219
    invoke-static {v0, v8}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v8

    const v9, 0x7f06009c

    .line 220
    invoke-static {v0, v9}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v9

    const v10, 0x7f060072

    .line 221
    invoke-static {v0, v10}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v10

    .line 222
    invoke-static {v0, v11}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result v11

    const/4 v12, 0x0

    .line 223
    :goto_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v12, v13, :cond_d

    .line 224
    iget-object v13, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    add-int/lit8 v14, v12, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TableRow;

    .line 225
    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;

    if-eqz v13, :cond_c

    .line 227
    rem-int/lit8 v12, v12, 0x2

    if-nez v12, :cond_7

    move v12, v5

    goto :goto_6

    :cond_7
    move v12, v8

    :goto_6
    invoke-virtual {v13, v12}, Landroid/widget/TableRow;->setBackgroundColor(I)V

    .line 228
    invoke-virtual {v13, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 229
    iget-object v3, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->dispTrainCode:Ljava/lang/String;

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {v13, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 231
    iget-object v12, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->endStationName:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    invoke-virtual {v13, v7}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v4, :cond_8

    .line 236
    iget-object v12, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->dTime:Ljava/lang/String;

    goto :goto_7

    :cond_8
    iget-object v12, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->aTime:Ljava/lang/String;

    :goto_7
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    invoke-virtual {v13, v6}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 239
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v12

    const/16 v6, 0x50

    invoke-static {v12, v6}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->dip2px(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setMaxWidth(I)V

    const/16 v6, 0x8

    if-eqz v4, :cond_a

    .line 241
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->waitingRoom:Ljava/lang/String;

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "/"

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->wicket:Ljava/lang/String;

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 242
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v12

    if-le v12, v6, :cond_9

    const/4 v12, 0x0

    .line 243
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_8

    :cond_9
    const/4 v12, 0x0

    .line 245
    :goto_8
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    :cond_a
    const/4 v12, 0x0

    .line 247
    iget-object v7, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->platform:Ljava/lang/String;

    .line 248
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v6, :cond_b

    .line 250
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setSingleLine(Z)V

    :cond_b
    :goto_9
    const/4 v3, 0x4

    .line 254
    invoke-virtual {v13, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 255
    iget v6, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->status:I

    packed-switch v6, :pswitch_data_0

    .line 283
    iget-object v6, v15, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->statusInfo:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 278
    :pswitch_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d0289

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 279
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_a

    .line 273
    :pswitch_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d0288

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 274
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_a

    .line 269
    :pswitch_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d028b

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_a

    .line 265
    :pswitch_3
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d028c

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_a

    .line 261
    :pswitch_4
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d0287

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_a

    .line 257
    :pswitch_5
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d028d

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_c
    :goto_a
    move v12, v14

    const/4 v3, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x2

    goto/16 :goto_5

    :cond_d
    return-void

    .line 173
    :cond_e
    :goto_b
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v3, 0x7f0d0146

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    const v4, 0x7f0d02de

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v2, v6

    invoke-static {v4, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 175
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    .line 176
    invoke-direct/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->clearContent()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showSelectStation()V
    .locals 4

    .line 92
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "showSelectStation"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getStationMap()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;

    invoke-direct {v1}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;-><init>()V

    .line 101
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$dRJVGrWpUK1rM4myHJsQJi-1P-M;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$dRJVGrWpUK1rM4myHJsQJi-1P-M;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment;->setListener(Lcom/lltskb/lltskb/fragment/BigScreenSelectStationFragment$Listener;)V

    const v2, 0x7f0900de

    const-string v3, "BIG_SCREEN_FRAGMENT_TAG"

    .line 106
    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    const/4 v1, 0x0

    .line 107
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 108
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreate$0$BigScreenActivity(Landroid/view/View;)V
    .locals 0

    .line 54
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$onCreate$1$BigScreenActivity(Landroid/view/View;)V
    .locals 2

    .line 70
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "bigscreen"

    const-string v1, "\u9009\u62e9\u8f66\u7ad9"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->showSelectStation()V

    return-void
.end method

.method public synthetic lambda$onCreate$2$BigScreenActivity(Landroid/view/View;)V
    .locals 2

    .line 76
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "bigscreen"

    const-string v1, "\u5237\u65b0"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->refresh()V

    return-void
.end method

.method public synthetic lambda$onCreate$3$BigScreenActivity()V
    .locals 3

    .line 83
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "bigscreen"

    const-string v2, "\u5207\u6362"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->refresh()V

    return-void
.end method

.method public synthetic lambda$selectStation$5$BigScreenActivity(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V
    .locals 1

    .line 117
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->loadStStationList()V

    .line 118
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getStationMap()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 119
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 120
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$ZK3OO0yOjBTkiDgx5WC-anNWmUA;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$ZK3OO0yOjBTkiDgx5WC-anNWmUA;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$selectStation$6$BigScreenActivity([Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 131
    aget-object p1, p1, p3

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    .line 133
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->refresh()V

    return-void
.end method

.method public synthetic lambda$showSelectStation$4$BigScreenActivity(Ljava/lang/String;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    .line 103
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->refresh()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 47
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0b0025

    .line 48
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->setContentView(I)V

    const p1, 0x7f090223

    .line 50
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0d0071

    .line 51
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    const p1, 0x7f0900fb

    .line 53
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 54
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$xDQYoq35fNnR-UoXKraEtLxx9Vo;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$xDQYoq35fNnR-UoXKraEtLxx9Vo;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090227

    .line 56
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TableLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->tableLayout:Landroid/widget/TableLayout;

    .line 58
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "station"

    .line 60
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p1, "\u5317\u4eac\u897f"

    .line 62
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    :goto_0
    const-string p1, "D"

    .line 65
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->type:Ljava/lang/String;

    const p1, 0x7f090071

    .line 67
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    .line 68
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->station:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->stationView:Landroid/widget/Button;

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$Ljnf-eAddZQV1Sa9iqx4ZP7o57k;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$Ljnf-eAddZQV1Sa9iqx4ZP7o57k;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090068

    .line 74
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 75
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$VDsJB_Fd7k2z3Prdy5UzGwCxKv8;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$VDsJB_Fd7k2z3Prdy5UzGwCxKv8;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f09020d

    .line 80
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/widget/SwitchView;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->switchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    .line 81
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->switchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setChecked(Z)V

    .line 82
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->switchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$yDe6o3lsj6KHgIxlGS479G0OBQI;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$yDe6o3lsj6KHgIxlGS479G0OBQI;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setOnClickCheckedListener(Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;)V

    .line 88
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->refresh()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 295
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BIG_SCREEN_FRAGMENT_TAG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/BaseFragment;

    if-eqz v0, :cond_1

    .line 296
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 297
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->onBackPressed()Z

    move-result p1

    if-nez p1, :cond_0

    .line 298
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/BaseFragment;->dismiss()V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 304
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method
