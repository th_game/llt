.class public Lcom/lltskb/lltskb/engine/online/CitySchoolModel;
.super Ljava/lang/Object;
.source "CitySchoolModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;
    }
.end annotation


# static fields
.field private static final CITY_FILE:Ljava/lang/String; = "citys.txt"

.field private static final SCHOOL_FILE:Ljava/lang/String; = "schools.txt"

.field private static final TAG:Ljava/lang/String; = "CitySchoolModel"

.field private static instance:Lcom/lltskb/lltskb/engine/online/CitySchoolModel;


# instance fields
.field private mAllCitys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mSchoolNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mAllCitys:Ljava/util/Map;

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    .line 60
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/engine/online/CitySchoolModel;
    .locals 1

    .line 49
    sget-object v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->instance:Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->instance:Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    .line 52
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->instance:Lcom/lltskb/lltskb/engine/online/CitySchoolModel;

    return-object v0
.end method

.method private parseAllStations(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    if-eqz p1, :cond_9

    .line 141
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto/16 :goto_3

    .line 145
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->clear()V

    .line 147
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 150
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 151
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto/16 :goto_2

    .line 155
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 156
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    const-string v2, ""

    const/4 v3, 0x0

    move-object v4, v2

    const/4 v2, 0x0

    .line 158
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 159
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "status"

    .line 162
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    return-void

    .line 167
    :cond_3
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    return-void

    .line 171
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-nez p1, :cond_5

    return-void

    .line 174
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 175
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 176
    new-instance v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;-><init>(Lcom/lltskb/lltskb/engine/online/CitySchoolModel;)V

    const-string v2, "chineseName"

    .line 178
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    const-string v2, "simplePin"

    .line 179
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->pin:Ljava/lang/String;

    const-string v2, "stationTelecode"

    .line 180
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->teleCode:Ljava/lang/String;

    .line 181
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 182
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_7
    const-string p1, "CitySchoolModel"

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "station name size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :cond_8
    :goto_2
    return-void

    :catch_0
    move-exception p1

    .line 188
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_9
    :goto_3
    return-void
.end method

.method private readFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    invoke-direct {v1, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/high16 p1, 0x20000

    new-array p1, p1, [C

    .line 218
    :cond_0
    invoke-virtual {v1, p1}, Ljava/io/FileReader;->read([C)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v3, 0x0

    .line 220
    invoke-virtual {v0, p1, v3, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 223
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V

    .line 224
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 231
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 228
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private writeToFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 196
    :try_start_0
    new-instance v0, Ljava/io/FileWriter;

    invoke-direct {v0, p2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0}, Ljava/io/FileWriter;->flush()V

    .line 199
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 201
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "writeToFile:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "CitySchoolModel"

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getAllCitys()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mAllCitys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "citys.txt"

    .line 241
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 242
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x64

    if-ge v2, v3, :cond_2

    :cond_1
    const-string v1, "station_name="

    .line 245
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->ALLCITYS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v2, v3, v1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CitySchoolModel"

    .line 246
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "city="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-direct {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mAllCitys:Ljava/util/Map;

    invoke-direct {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->parseAllStations(Ljava/lang/String;Ljava/util/Map;)V

    return-void

    :catch_0
    move-exception v0

    .line 249
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 250
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_3

    .line 251
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 252
    :cond_3
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u7f51\u7edc\u9519\u8bef"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAllSchools()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "schools.txt"

    .line 70
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 72
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x64

    if-ge v2, v3, :cond_2

    :cond_1
    const-string v1, "provinceCode=11"

    .line 75
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SCHOOL_NAMES:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v2, v3, v1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CitySchoolModel"

    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-direct {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->parseAllStations(Ljava/lang/String;Ljava/util/Map;)V

    return-void

    :catch_0
    move-exception v0

    .line 80
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 81
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_3

    .line 82
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 83
    :cond_3
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u7f51\u7edc\u9519\u8bef"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCityByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mAllCitys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 120
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mAllCitys:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    .line 121
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getSchoolByName(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    return-object p1
.end method

.method public getSchoolsByName(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;",
            ">;"
        }
    .end annotation

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 109
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    .line 110
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getSchoolsByPin(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;",
            ">;"
        }
    .end annotation

    .line 95
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 98
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/CitySchoolModel;->mSchoolNames:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;

    .line 99
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/CitySchoolModel$StationName;->pin:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
