.class public Lcom/lltskb/lltskb/engine/online/DynamicJs;
.super Ljava/lang/Object;
.source "DynamicJs.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DynamicJs"


# instance fields
.field private mDynamicJs:Ljava/lang/String;

.field private mDynamicParam:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->parseDynamicJs(Ljava/lang/String;)V

    return-void
.end method

.method private handleUrl(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 36
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "url"

    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x3

    .line 45
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_2

    return-void

    :cond_2
    const-string v1, "\'"

    .line 51
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_3

    return-void

    :cond_3
    add-int/lit8 v2, v0, 0x3

    .line 53
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_4

    return-void

    :cond_4
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x5

    .line 57
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dynamic url = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DynamicJs"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :try_start_0
    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EMPTY_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 63
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->setPath(Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object p1

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 67
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 68
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_5

    .line 69
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 70
    :cond_5
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v0, "\u7f51\u7edc\u95ee\u9898"

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private parseDynamicJs(Ljava/lang/String;)V
    .locals 2

    const-string v0, "DynamicJs"

    const-string v1, "parseDynamicJs"

    .line 21
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    .line 24
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "/otn/dynamicJs/"

    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const-string v1, "\""

    .line 28
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-lez v0, :cond_1

    if-lez v1, :cond_1

    add-int/lit8 v0, v0, 0x5

    .line 31
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    :cond_1
    return-void
.end method


# virtual methods
.method public getDynamicParam()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicParam:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicParam:Ljava/lang/String;

    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "DynamicJs"

    if-eqz v0, :cond_1

    const-string v0, "DynamicJs is null"

    .line 79
    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 87
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDynamicParam = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :try_start_0
    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EMPTY_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 90
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->setPath(Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v0, v4}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->handleUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get DynamicJs return null "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicJs:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_2
    const-string v1, "function gc(){var"

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    add-int/lit8 v1, v1, 0x11

    const-string v3, "\'"

    .line 110
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_3

    add-int/lit8 v1, v1, 0x1

    .line 113
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_3

    .line 115
    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/DynamicJsUtil;->getRandomParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicParam:Ljava/lang/String;

    .line 125
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDynamic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicParam:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x64

    .line 127
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->sleep(J)V

    .line 129
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/DynamicJs;->mDynamicParam:Ljava/lang/String;

    return-object v0

    :catch_0
    move-exception v0

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 95
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_4

    .line 96
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 97
    :cond_4
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u7f51\u7edc\u95ee\u9898"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
