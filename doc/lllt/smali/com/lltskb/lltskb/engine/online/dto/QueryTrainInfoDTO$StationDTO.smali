.class public Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;
.source "QueryTrainInfoDTO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StationDTO"
.end annotation


# instance fields
.field arrive_day_diff:Ljava/lang/String;

.field arrive_day_str:Ljava/lang/String;

.field public arrive_time:Ljava/lang/String;

.field end_station_name:Ljava/lang/String;

.field is_start:Ljava/lang/String;

.field public running_time:Ljava/lang/String;

.field service_type:Ljava/lang/String;

.field start_station_name:Ljava/lang/String;

.field public start_time:Ljava/lang/String;

.field public station_name:Ljava/lang/String;

.field station_no:Ljava/lang/String;

.field public station_train_code:Ljava/lang/String;

.field public train_class_name:Ljava/lang/String;

.field wz_num:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;-><init>()V

    return-void
.end method
