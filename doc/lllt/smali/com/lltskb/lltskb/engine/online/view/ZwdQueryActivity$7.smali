.class Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;
.super Ljava/lang/Object;
.source "ZwdQueryActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->queryTrain()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 252
    new-instance v0, Lcom/lltskb/lltskb/engine/QueryCC;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    .line 253
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 255
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit16 v4, v4, 0x2710

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/2addr v5, v2

    mul-int/lit8 v5, v5, 0x64

    add-int/2addr v4, v5

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v4, v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 256
    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    .line 257
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$502(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;Ljava/util/Vector;)Ljava/util/Vector;

    .line 258
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v3}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$400(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v0, v3, v4}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 261
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 262
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;-><init>()V

    .line 263
    iput v1, v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->status:I

    .line 264
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->train:Ljava/lang/String;

    const/16 v5, 0xe

    .line 265
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    const/4 v5, 0x4

    .line 266
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->arrive_time:Ljava/lang/String;

    const/4 v5, 0x3

    .line 267
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->start_time:Ljava/lang/String;

    .line 268
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v3}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$500(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 272
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
