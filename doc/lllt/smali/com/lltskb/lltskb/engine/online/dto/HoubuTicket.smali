.class public final Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
        "",
        "tickets",
        "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
        "seatType",
        "",
        "successRate",
        "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;",
        "(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V",
        "getSeatType",
        "()Ljava/lang/String;",
        "setSeatType",
        "(Ljava/lang/String;)V",
        "getSuccessRate",
        "()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;",
        "setSuccessRate",
        "(Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V",
        "getTickets",
        "()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
        "setTickets",
        "(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)V",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private seatType:Ljava/lang/String;

.field private successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

.field private tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V
    .locals 1

    const-string v0, "tickets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "seatType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "successRate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->copy(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    return-object v0
.end method

.method public final copy(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;
    .locals 1

    const-string v0, "tickets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "seatType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "successRate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    invoke-direct {v0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;-><init>(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 34
    :cond_0
    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    if-nez v1, :cond_1

    return v0

    .line 38
    :cond_1
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    .line 40
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    if-nez v1, :cond_2

    goto :goto_0

    .line 44
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 48
    :cond_3
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v0

    :cond_4
    const/4 p1, 0x1

    return p1

    :cond_5
    :goto_0
    return v0
.end method

.method public final getSeatType()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuccessRate()Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    return-object v0
.end method

.method public final getTickets()Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 57
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final setSeatType(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    return-void
.end method

.method public final setSuccessRate(Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    return-void
.end method

.method public final setTickets(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HoubuTicket(tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->tickets:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", seatType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->seatType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", successRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;->successRate:Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
