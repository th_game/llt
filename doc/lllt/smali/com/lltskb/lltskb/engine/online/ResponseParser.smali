.class public Lcom/lltskb/lltskb/engine/online/ResponseParser;
.super Ljava/lang/Object;
.source "ResponseParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseTicketLeftInfo(Ljava/lang/String;)Ljava/util/Vector;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;,
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 16
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const-string v1, "&nbsp;"

    const-string v2, ""

    .line 17
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v1, ","

    .line 18
    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 19
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_3

    .line 20
    aget-object v3, p0, v2

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, "<span"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 21
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;-><init>()V

    add-int/lit8 v4, v2, 0x1

    .line 22
    aget-object v2, p0, v2

    .line 23
    invoke-static {v2, v3}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserSpan(Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;)V

    add-int/lit8 v2, v4, 0x1

    .line 24
    aget-object v4, p0, v4

    const-string v5, "<br>"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 25
    aget-object v6, v4, v1

    const-string v7, "<img"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    const-string v8, ">"

    const/4 v9, 0x1

    if-eqz v6, :cond_0

    .line 26
    aget-object v6, v4, v1

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v9

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->startStation:Ljava/lang/String;

    goto :goto_1

    .line 28
    :cond_0
    aget-object v6, v4, v1

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->startStation:Ljava/lang/String;

    .line 30
    :goto_1
    aget-object v4, v4, v9

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->startTime:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 31
    aget-object v2, p0, v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 32
    aget-object v5, v2, v1

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 33
    aget-object v5, v2, v1

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->arriveStation:Ljava/lang/String;

    goto :goto_2

    .line 35
    :cond_1
    aget-object v5, v2, v1

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->arriveStation:Ljava/lang/String;

    .line 37
    :goto_2
    aget-object v2, v2, v9

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->arriveTime:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 38
    aget-object v4, p0, v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->runPeriod:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 39
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->shangwu:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 40
    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->tedeng:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 41
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->yideng:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 42
    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->erdeng:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 43
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->gaojiruanwo:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 44
    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->ruanwo:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 45
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->yingwo:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 46
    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->ruanzuo:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 47
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->yingzuo:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    .line 48
    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->wuzuo:Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 49
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/ResponseParser;->parserFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;->qitazuo:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_3
    return-object v0
.end method

.method private static parserFont(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "<font"

    .line 65
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\'>"

    .line 66
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string v1, "<\\/"

    .line 67
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 68
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private static parserSpan(Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/htmlparser/util/ParserException;
        }
    .end annotation

    .line 81
    new-instance v0, Lorg/htmlparser/Parser;

    invoke-direct {v0}, Lorg/htmlparser/Parser;-><init>()V

    .line 82
    invoke-virtual {v0, p0}, Lorg/htmlparser/Parser;->setInputHTML(Ljava/lang/String;)V

    .line 83
    new-instance p0, Lcom/lltskb/lltskb/engine/online/ResponseParser$1;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/ResponseParser$1;-><init>(Lcom/lltskb/lltskb/engine/online/dto/TicketLeftInfo;)V

    .line 103
    invoke-virtual {v0, p0}, Lorg/htmlparser/Parser;->visitAllNodesWith(Lorg/htmlparser/visitors/NodeVisitor;)V

    return-void
.end method
