.class public Lcom/lltskb/lltskb/engine/online/dto/PayInfo;
.super Ljava/lang/Object;
.source "PayInfo.java"


# instance fields
.field private appId:Ljava/lang/String;

.field private bankId:Ljava/lang/String;

.field private businessType:Ljava/lang/String;

.field private channelId:Ljava/lang/String;

.field private interfaceName:Ljava/lang/String;

.field private interfaceVersion:Ljava/lang/String;

.field private lastTime:Ljava/lang/String;

.field private merCustomIp:Ljava/lang/String;

.field private merSignMsg:Ljava/lang/String;

.field private orderTimeoutDate:Ljava/lang/String;

.field private payMoney:Ljava/lang/String;

.field private payOrderId:Ljava/lang/String;

.field private payUrl:Ljava/lang/String;

.field private paymentType:Ljava/lang/String;

.field private tranData:Ljava/lang/String;

.field private transType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getBankId()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->bankId:Ljava/lang/String;

    return-object v0
.end method

.method public getBusinessType()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->businessType:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->channelId:Ljava/lang/String;

    return-object v0
.end method

.method public getInterfaceName()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->interfaceName:Ljava/lang/String;

    return-object v0
.end method

.method public getInterfaceVersion()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->interfaceVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getLastTime()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->lastTime:Ljava/lang/String;

    return-object v0
.end method

.method public getMerCustomIp()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->merCustomIp:Ljava/lang/String;

    return-object v0
.end method

.method public getMerSignMsg()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->merSignMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderTimeoutDate()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->orderTimeoutDate:Ljava/lang/String;

    return-object v0
.end method

.method public getPayMoney()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payMoney:Ljava/lang/String;

    return-object v0
.end method

.method public getPayOrderId()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payOrderId:Ljava/lang/String;

    return-object v0
.end method

.method public getPayUrl()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentType()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->paymentType:Ljava/lang/String;

    return-object v0
.end method

.method public getTranData()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->tranData:Ljava/lang/String;

    return-object v0
.end method

.method public getTransType()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->transType:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->appId:Ljava/lang/String;

    return-void
.end method

.method public setBankId(Ljava/lang/String;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->bankId:Ljava/lang/String;

    return-void
.end method

.method public setBusinessType(Ljava/lang/String;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->businessType:Ljava/lang/String;

    return-void
.end method

.method public setChannelId(Ljava/lang/String;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->channelId:Ljava/lang/String;

    return-void
.end method

.method public setInterfaceName(Ljava/lang/String;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->interfaceName:Ljava/lang/String;

    return-void
.end method

.method public setInterfaceVersion(Ljava/lang/String;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->interfaceVersion:Ljava/lang/String;

    return-void
.end method

.method public setLastTime(Ljava/lang/String;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->lastTime:Ljava/lang/String;

    return-void
.end method

.method public setMerCustomIp(Ljava/lang/String;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->merCustomIp:Ljava/lang/String;

    return-void
.end method

.method public setMerSignMsg(Ljava/lang/String;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->merSignMsg:Ljava/lang/String;

    return-void
.end method

.method public setOrderTimeoutDate(Ljava/lang/String;)V
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->orderTimeoutDate:Ljava/lang/String;

    return-void
.end method

.method public setPayMoney(Ljava/lang/String;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payMoney:Ljava/lang/String;

    return-void
.end method

.method public setPayOrderId(Ljava/lang/String;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payOrderId:Ljava/lang/String;

    return-void
.end method

.method public setPayUrl(Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->payUrl:Ljava/lang/String;

    return-void
.end method

.method public setPaymentType(Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->paymentType:Ljava/lang/String;

    return-void
.end method

.method public setTranData(Ljava/lang/String;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->tranData:Ljava/lang/String;

    return-void
.end method

.method public setTransType(Ljava/lang/String;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->transType:Ljava/lang/String;

    return-void
.end method
