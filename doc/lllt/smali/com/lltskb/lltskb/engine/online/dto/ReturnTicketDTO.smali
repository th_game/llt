.class public Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;
.source "ReturnTicketDTO.java"


# instance fields
.field public batch_no:Ljava/lang/String;

.field public coach_name:Ljava/lang/String;

.field public coach_no:Ljava/lang/String;

.field public passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

.field public rate:Ljava/lang/String;

.field public return_cost:D

.field public return_price:D

.field public return_rate:Ljava/lang/String;

.field public seat_name:Ljava/lang/String;

.field public seat_no:Ljava/lang/String;

.field public seat_type_name:Ljava/lang/String;

.field public sequence_no:Ljava/lang/String;

.field public start_train_date_page:Ljava/lang/String;

.field public stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

.field public ticket_no:Ljava/lang/String;

.field public ticket_price:D

.field public train_date:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;-><init>()V

    return-void
.end method
