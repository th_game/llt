.class public interface abstract Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;
.super Ljava/lang/Object;
.source "IHoubuTicketModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\u0003H&J\u0010\u0010\r\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\u0003H&J\u001a\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0003H&J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0003H&J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u000c\u001a\u00020\u0003H&J\u0010\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0003H&J\u0008\u0010\u001d\u001a\u00020\u001bH&J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u001fH&J\n\u0010 \u001a\u0004\u0018\u00010!H&J\u001a\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0003H&J\u0010\u0010%\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u0003H&J\u0018\u0010&\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\'\u001a\u00020\u0003H&J\u0018\u0010(\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\'\u001a\u00020\u0003H&J\u0008\u0010)\u001a\u00020\u0007H&J\n\u0010*\u001a\u0004\u0018\u00010+H&J\n\u0010,\u001a\u0004\u0018\u00010-H&J\n\u0010.\u001a\u0004\u0018\u00010/H&J\n\u00100\u001a\u0004\u0018\u000101H&J\u0012\u00102\u001a\u0004\u0018\u0001032\u0006\u00104\u001a\u00020\u0003H&J\u0012\u00105\u001a\u0004\u0018\u0001062\u0006\u00104\u001a\u00020\u0003H&J\n\u00107\u001a\u0004\u0018\u000108H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u00069"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;",
        "",
        "message",
        "",
        "getMessage",
        "()Ljava/lang/String;",
        "addHoubuTicket",
        "",
        "ticket",
        "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
        "cancelNotComplete",
        "Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;",
        "reserve_no",
        "cancelWaitingHOrder",
        "chechFace",
        "Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;",
        "secretList",
        "seatType",
        "confirmHB",
        "Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;",
        "jzParam",
        "continuePayNoCompleteMyOrder",
        "Lcom/lltskb/lltskb/engine/online/dto/ContinuePayNoCompleteDTO;",
        "delHoubuTicket",
        "getDeadlineTime",
        "Ljava/util/Date;",
        "getHoubuCountInDate",
        "",
        "date",
        "getHoubuTicketCount",
        "getHoubuTicketList",
        "",
        "getQueueNum",
        "Lcom/lltskb/lltskb/engine/online/dto/GetQueueNumDTO;",
        "getSuccessRate",
        "Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;",
        "successSecret",
        "isAdjacentDate",
        "isAllowedDeadlineTime",
        "time",
        "isAllowedTime",
        "payOrderInit",
        "paycheck",
        "Lcom/lltskb/lltskb/engine/online/dto/PayCheckDTO;",
        "queryProcessedHOrder",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryProcessedHOrderDTO;",
        "queryQueue",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;",
        "queryUnHonourHOrder",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderDTO;",
        "reserveReturn",
        "Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;",
        "sequence_no",
        "reserveReturnCheck",
        "Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;",
        "submitOrderRequest",
        "Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# virtual methods
.method public abstract addHoubuTicket(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)Z
.end method

.method public abstract cancelNotComplete(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CancelNotCompleteDTO;
.end method

.method public abstract cancelWaitingHOrder(Ljava/lang/String;)Z
.end method

.method public abstract chechFace(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;
.end method

.method public abstract confirmHB(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ConfirmHBDTO;
.end method

.method public abstract continuePayNoCompleteMyOrder(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ContinuePayNoCompleteDTO;
.end method

.method public abstract delHoubuTicket(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)Z
.end method

.method public abstract getDeadlineTime()Ljava/util/Date;
.end method

.method public abstract getHoubuCountInDate(Ljava/lang/String;)I
.end method

.method public abstract getHoubuTicketCount()I
.end method

.method public abstract getHoubuTicketList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getQueueNum()Lcom/lltskb/lltskb/engine/online/dto/GetQueueNumDTO;
.end method

.method public abstract getSuccessRate(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;
.end method

.method public abstract isAdjacentDate(Ljava/lang/String;)Z
.end method

.method public abstract isAllowedDeadlineTime(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract isAllowedTime(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract payOrderInit()Z
.end method

.method public abstract paycheck()Lcom/lltskb/lltskb/engine/online/dto/PayCheckDTO;
.end method

.method public abstract queryProcessedHOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryProcessedHOrderDTO;
.end method

.method public abstract queryQueue()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueDTO;
.end method

.method public abstract queryUnHonourHOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryUnHonourHOrderDTO;
.end method

.method public abstract reserveReturn(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnDTO;
.end method

.method public abstract reserveReturnCheck(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnCheckDTO;
.end method

.method public abstract submitOrderRequest()Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;
.end method
