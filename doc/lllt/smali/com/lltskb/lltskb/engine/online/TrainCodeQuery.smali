.class public Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseQuery;
.source "TrainCodeQuery.java"


# instance fields
.field protected mDisplayResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/BaseQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    .line 31
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mDisplayResult:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public doQuery()Ljava/lang/String;
    .locals 3

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    if-nez v0, :cond_0

    const-string v0, "\u7a0b\u5e8f\u9519\u8bef\uff01"

    return-object v0

    .line 76
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getTrainSearchModel()Lcom/lltskb/lltskb/engine/online/ITrainSearch;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainCode:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/ITrainSearch;->queryTrain(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "\u67e5\u8be2\u5931\u8d25"

    return-object v0

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->getData()Ljava/util/Vector;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
    .locals 10

    .line 96
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 99
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const v6, 0x7f0b0094

    const/4 v3, 0x5

    new-array v7, v3, [Ljava/lang/String;

    const-string v8, "station"

    aput-object v8, v7, v2

    const-string v2, "stoptime"

    aput-object v2, v7, v1

    const/4 v1, 0x2

    const-string v2, "time"

    aput-object v2, v7, v1

    const/4 v1, 0x3

    const-string v2, "ticket"

    aput-object v2, v7, v1

    const/4 v1, 0x4

    const-string v2, "extra"

    aput-object v2, v7, v1

    new-array v8, v3, [I

    fill-array-data v8, :array_0

    move-object v3, v0

    move-object v9, p0

    invoke-direct/range {v3 .. v9}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILcom/lltskb/lltskb/engine/online/BaseQuery;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    return-object v0

    :array_0
    .array-data 4
        0x7f090009
        0x7f09000a
        0x7f09000c
        0x7f09000b
        0x7f090005
    .end array-data
.end method

.method public getDisplayResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mDisplayResult:Ljava/util/Vector;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u6b21\u4fe1\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onItemClicked(I)V
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 188
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    return-void

    .line 189
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 190
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 191
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "ticket_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_type"

    const-string v2, "query_type_station"

    .line 193
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ticket_type"

    const-string v2, "\u5168\u90e8"

    .line 194
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "station_code"

    .line 195
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "query_method"

    const-string v1, "query_method_skbcx"

    .line 196
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "*>;ZZ)",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 130
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_b

    .line 131
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 133
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    .line 135
    iget-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->isSelected:Z

    if-nez v4, :cond_1

    goto/16 :goto_7

    .line 137
    :cond_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 138
    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    const-string v6, "<br/>"

    const-string v7, "\n"

    if-eqz p3, :cond_2

    .line 140
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 142
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 144
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v8, "station"

    .line 145
    invoke-interface {v4, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const-string v8, "--"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    const-string v9, " "

    const-string v10, ""

    if-nez v5, :cond_5

    if-eqz p3, :cond_3

    .line 150
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "<font color=\"#5fc534\"><b>"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_3
    move-object v5, v10

    .line 152
    :goto_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz p3, :cond_4

    .line 154
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "</b></font>&nbsp;"

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 156
    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 158
    :goto_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u5230"

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_5
    move-object v5, v10

    .line 160
    :goto_4
    iget-object v11, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 161
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_7

    .line 162
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_6

    goto :goto_5

    :cond_6
    move-object v6, v7

    :goto_5
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 163
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5f00"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_8
    const-string v6, "stoptime"

    .line 165
    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 168
    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 169
    array-length v6, v5

    const/4 v7, 0x1

    if-le v6, v7, :cond_9

    .line 170
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v8, v5, v1

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u65f6"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v5, v7

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u5206"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_6

    .line 172
    :cond_9
    iget-object v10, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    :cond_a
    :goto_6
    const-string v5, "time"

    .line 176
    invoke-interface {v4, v5, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->train_class_name:Ljava/lang/String;

    const-string v5, "extra"

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_b
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v0

    .line 110
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 111
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 112
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string v4, "station"

    .line 113
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "stoptime"

    .line 114
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "time"

    .line 115
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "ticket"

    .line 116
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "\n\n"

    .line 117
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
