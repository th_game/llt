.class public Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;
.super Ljava/lang/Object;
.source "OrderConfigMgr.java"


# instance fields
.field private mLastConfig:Ljava/lang/String;

.field private mOrderHistroy:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    return-void
.end method

.method private setLastConfig(Ljava/lang/String;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mLastConfig:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addConfig(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V
    .locals 2

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->setLastConfig(Ljava/lang/String;)V

    return-void
.end method

.method public getLastConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mLastConfig:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-object v0
.end method

.method public initOrderConfigMgr(Lorg/json/JSONObject;)V
    .locals 6

    .line 36
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "last_config"

    const-string v1, ""

    .line 38
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mLastConfig:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    .line 41
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 44
    :try_start_0
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    .line 46
    :cond_2
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    if-nez v4, :cond_3

    return-void

    .line 48
    :cond_3
    new-instance v5, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-direct {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;-><init>()V

    .line 49
    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->initConfig(Lorg/json/JSONObject;)V

    .line 50
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 52
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public toJSONObject()Lorg/json/JSONObject;
    .locals 4

    .line 59
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "last_config"

    .line 61
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mLastConfig:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 63
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->mOrderHistroy:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 64
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v3

    .line 65
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 69
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    return-object v0
.end method
