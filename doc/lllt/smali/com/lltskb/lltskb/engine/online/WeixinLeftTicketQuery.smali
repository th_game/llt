.class public Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "WeixinLeftTicketQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WeixinLeftTicketQuery"


# instance fields
.field private mDate:Ljava/lang/String;

.field private mErrorMsg:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private mPurpose:Ljava/lang/String;

.field private mResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mTo:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 131
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    return-void
.end method

.method private parseQueryResult(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "message"

    const-string v1, "data"

    .line 221
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0xa

    if-ge v2, v4, :cond_0

    goto/16 :goto_1

    .line 226
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 231
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    const/16 v4, 0xc8

    if-eq v2, v4, :cond_1

    return v3

    .line 236
    :cond_1
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 237
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-nez p1, :cond_2

    return v3

    .line 246
    :cond_2
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mResult:Ljava/util/Vector;

    const/4 v0, 0x0

    .line 247
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 248
    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 249
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "train_no"

    .line 250
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const-string v4, "station_train_code"

    .line 251
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const-string v4, "start_station_telecode"

    .line 252
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const-string v4, "start_station_name"

    .line 253
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    const-string v4, "end_station_telecode"

    .line 254
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    const-string v4, "end_station_name"

    .line 255
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    const-string v4, "from_station_telecode"

    .line 256
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const-string v4, "from_station_name"

    .line 257
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    const-string v4, "to_station_telecode"

    .line 258
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const-string v4, "to_station_name"

    .line 259
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const-string v4, "start_time"

    .line 260
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v4, "arrive_time"

    .line 261
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const-string v4, "day_difference"

    .line 262
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->day_difference:Ljava/lang/String;

    const-string v4, "train_class_name"

    .line 263
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_class_name:Ljava/lang/String;

    const-string v4, "lishi"

    .line 264
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v4, "canWebBuy"

    .line 265
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const-string v4, "lishiValue"

    .line 266
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishiValue:Ljava/lang/String;

    const-string v4, "yp_info"

    .line 267
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v4, "control_train_day"

    .line 268
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_train_day:Ljava/lang/String;

    const-string v4, "start_train_date"

    .line 269
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const-string v4, "seat_feature"

    .line 270
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_feature:Ljava/lang/String;

    const-string v4, "yp_ex"

    .line 271
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const-string v4, "train_seat_feature"

    .line 272
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const-string v4, "seat_types"

    .line 273
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    .line 274
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 275
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    :cond_3
    const-string v4, "location_code"

    .line 277
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const-string v4, "from_station_no"

    .line 278
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const-string v4, "to_station_no"

    .line 279
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const-string v4, "control_day"

    .line 280
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_day:Ljava/lang/String;

    const-string v4, "sale_time"

    .line 281
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->sale_time:Ljava/lang/String;

    const-string v4, "is_support_card"

    .line 282
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const-string v4, "controlled_train_flag"

    .line 283
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const-string v4, "controlled_train_message"

    .line 284
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    const-string v4, "note"

    .line 285
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->note:Ljava/lang/String;

    const-string v4, "gg_num"

    .line 287
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const-string v4, "gr_num"

    .line 288
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const-string v4, "qt_num"

    .line 289
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const-string v4, "rw_num"

    .line 290
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const-string v4, "rz_num"

    .line 291
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const-string v4, "tz_num"

    .line 292
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const-string v4, "wz_num"

    .line 293
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const-string v4, "yb_num"

    .line 294
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const-string v4, "yw_num"

    .line 295
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const-string v4, "yz_num"

    .line 296
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v4, "ze_num"

    .line 297
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const-string v4, "zy_num"

    .line 298
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const-string v4, "swz_num"

    .line 299
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const-string v4, "--"

    .line 300
    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    const-string v4, "secretStr"

    .line 302
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const-string v4, "buttonTextInfo"

    .line 303
    invoke-virtual {p0, v2, v4}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    .line 305
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mResult:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_4
    const/4 p1, 0x1

    return p1

    .line 238
    :cond_5
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 239
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    return v3

    :catch_0
    move-exception p1

    .line 314
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 315
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception p1

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WeixinLeftTicketQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 312
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    :cond_7
    :goto_1
    return v3
.end method

.method private queryTicket()Z
    .locals 7

    const-string v0, "utf-8"

    .line 166
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mFrom:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mTo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "\u3011\u65e0\u6cd5\u627e\u5230"

    const-string v5, "\u8f66\u7ad9\u3010"

    const/4 v6, 0x0

    if-eqz v3, :cond_0

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v6

    .line 174
    :cond_0
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mTo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v6

    .line 182
    :cond_1
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mDate:Ljava/lang/String;

    .line 183
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mPurpose:Ljava/lang/String;

    .line 185
    :try_start_0
    invoke-static {v3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 186
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mPurpose:Ljava/lang/String;

    invoke-static {v5, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 188
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 191
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leftTicketDTO.train_date="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&leftTicketDTO.from_station="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&leftTicketDTO.to_station="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&purpose_codes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueryLeftTicket url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WeixinLeftTicketQuery"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :try_start_1
    new-instance v1, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v1}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    .line 201
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://mobile.12306.cn/weixin/leftTicket/query?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queryTicket ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/16 v4, 0xbb8

    .line 203
    invoke-virtual {v1, v0, v3, v4}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 211
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->parseQueryResult(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "QueryLeftTicket ret="

    if-nez v1, :cond_2

    .line 213
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 215
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v1

    :catch_1
    move-exception v0

    .line 205
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v6
.end method


# virtual methods
.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mResult:Ljava/util/Vector;

    return-object v0
.end method

.method queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mFrom:Ljava/lang/String;

    .line 145
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mTo:Ljava/lang/String;

    .line 146
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mDate:Ljava/lang/String;

    .line 147
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mPurpose:Ljava/lang/String;

    const/4 p1, 0x0

    .line 148
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mResult:Ljava/util/Vector;

    .line 149
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->queryTicket()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 150
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/WeixinLeftTicketQuery;->mResult:Ljava/util/Vector;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    :cond_0
    return p1
.end method
