.class public final Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u001d\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\"\u001a\u00020\nH\u00c6\u0003J;\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010$\u001a\u00020\u00032\u0008\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\nH\u00d6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u001e\u0010\u0008\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\r\"\u0004\u0008\u0015\u0010\u000fR\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001d\u00a8\u0006)"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;",
        "",
        "flag",
        "",
        "order",
        "Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "beginTime",
        "",
        "loseTime",
        "status",
        "",
        "(ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJI)V",
        "getBeginTime",
        "()J",
        "setBeginTime",
        "(J)V",
        "getFlag",
        "()Z",
        "setFlag",
        "(Z)V",
        "getLoseTime",
        "setLoseTime",
        "getOrder",
        "()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;",
        "setOrder",
        "(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V",
        "getStatus",
        "()I",
        "setStatus",
        "(I)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private beginTime:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "beginTime"
    .end annotation
.end field

.field private flag:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "flag"
    .end annotation
.end field

.field private loseTime:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "loseTime"
    .end annotation
.end field

.field private order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "order"
    .end annotation
.end field

.field private status:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJI)V
    .locals 1

    const-string v0, "order"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    iput-wide p3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    iput-wide p5, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    iput p7, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJIILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-wide p5, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    :cond_3
    move-wide v2, p5

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p7, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    :cond_4
    move v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move-wide p5, v0

    move-wide p7, v2

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->copy(ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJI)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    return v0
.end method

.method public final component2()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    return-wide v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    return v0
.end method

.method public final copy(ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJI)Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;
    .locals 9

    const-string v0, "order"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    move-object v1, v0

    move v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;-><init>(ZLcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;JJI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_5

    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    iget-boolean v3, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    iget-wide v5, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    iget-wide v5, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    iget p1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    if-ne v1, p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    return v2

    :cond_5
    :goto_4
    return v0
.end method

.method public final getBeginTime()J
    .locals 2

    .line 162
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    return-wide v0
.end method

.method public final getFlag()Z
    .locals 1

    .line 160
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    return v0
.end method

.method public final getLoseTime()J
    .locals 2

    .line 163
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    return-wide v0
.end method

.method public final getOrder()Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    return-object v0
.end method

.method public final getStatus()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    const/16 v3, 0x20

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v2, v1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v2, v1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final setBeginTime(J)V
    .locals 0

    .line 162
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    return-void
.end method

.method public final setFlag(Z)V
    .locals 0

    .line 160
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    return-void
.end method

.method public final setLoseTime(J)V
    .locals 0

    .line 163
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    return-void
.end method

.method public final setOrder(Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    return-void
.end method

.method public final setStatus(I)V
    .locals 0

    .line 164
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QueryQueueData(flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->flag:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->order:Lcom/lltskb/lltskb/engine/online/dto/QueryQueueOrder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", beginTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->beginTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", loseTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->loseTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryQueueData;->status:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
