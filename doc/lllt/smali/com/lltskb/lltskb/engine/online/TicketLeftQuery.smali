.class public Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseQuery;
.source "TicketLeftQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TicketLeftQuery"


# instance fields
.field private arriveStation:Ljava/lang/String;

.field private mDisplayResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

.field private startStation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/BaseQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    .line 60
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    return-void
.end method

.method private formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;
    .locals 3

    .line 165
    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "--"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    if-nez p3, :cond_1

    .line 170
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const-string p3, "\u65e0"

    .line 173
    invoke-virtual {p2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string v0, "</font>"

    if-eqz p3, :cond_5

    .line 174
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p3

    invoke-virtual {p3}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object p3

    iget-object v1, p5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    iget-object v2, p5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-interface {p3, v1, v2}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->isAllowedTime(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p3

    .line 175
    iget-object v1, p5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    iget-object p5, p5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    invoke-static {p4, v1, p5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p4

    const-string p5, "<font color=\"#ababab\">"

    if-eqz p4, :cond_4

    if-nez p3, :cond_2

    goto :goto_0

    :cond_2
    const/4 p2, -0x1

    const p3, 0x7f0d0197

    if-ne p4, p2, :cond_3

    .line 181
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 184
    :cond_3
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "<font color=\"#FC8A28\">"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 177
    :cond_4
    :goto_0
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_5
    const-string p3, "\u6709"

    .line 185
    invoke-virtual {p2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string p4, "<font color=\"#5c9f00\">"

    if-eqz p3, :cond_6

    .line 186
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 188
    :cond_6
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u5f20</font>"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_7
    :goto_1
    const-string p1, ""

    return-object p1
.end method

.method private getPrice(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;
    .locals 14

    const-string v0, ""

    if-eqz p1, :cond_e

    .line 197
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_5

    .line 198
    :cond_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const/4 v2, 0x0

    .line 199
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x44

    const/4 v4, 0x1

    if-eq v1, v3, :cond_2

    const/16 v3, 0x47

    if-eq v1, v3, :cond_2

    const/16 v3, 0x43

    if-eq v1, v3, :cond_2

    const/16 v3, 0x53

    if-ne v1, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 202
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v9

    .line 203
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v10

    .line 204
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    iget-object v5, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndex(Ljava/lang/String;)I

    move-result v7

    .line 206
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v3

    if-eqz v3, :cond_e

    if-ltz v9, :cond_e

    if-gez v10, :cond_3

    goto/16 :goto_5

    .line 209
    :cond_3
    aget-byte v5, v3, v4

    and-int/lit16 v5, v5, 0xff

    mul-int/lit16 v5, v5, 0xff

    const/4 v12, 0x2

    aget-byte v6, v3, v12

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v5, v6

    const-string v13, "-"

    if-eqz v1, :cond_4

    .line 212
    invoke-static {v5, v9, v10}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_gt(III)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_4

    .line 214
    :cond_4
    invoke-static {v7, v9, v10}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_pk(III)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 215
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_c

    .line 216
    :cond_5
    new-instance v6, Lcom/lltskb/lltskb/engine/QueryZZ;

    invoke-direct {v6, v2, v2}, Lcom/lltskb/lltskb/engine/QueryZZ;-><init>(ZZ)V

    const/4 v11, 0x0

    move-object v8, v3

    .line 219
    :try_start_0
    invoke-virtual/range {v6 .. v11}, Lcom/lltskb/lltskb/engine/QueryZZ;->getTrainTimeNew(I[BIIZ)[B

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v5

    .line 221
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_c

    const/16 v6, 0x9

    .line 225
    aget-byte v6, v5, v6

    mul-int/lit16 v6, v6, 0x80

    const/16 v7, 0xa

    aget-byte v7, v5, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    aget-byte v7, v5, v7

    mul-int/lit16 v7, v7, 0x80

    const/16 v8, 0x8

    aget-byte v5, v5, v8

    add-int/2addr v7, v5

    sub-int/2addr v6, v7

    .line 226
    aget-byte v3, v3, v2

    invoke-static {v3, v6}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_normal(II)Ljava/lang/String;

    move-result-object v3

    .line 227
    invoke-static {v3, v13}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 228
    array-length v5, v3

    const/4 v6, 0x4

    if-ne v5, v6, :cond_c

    .line 230
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    const-string v5, "--"

    if-eqz v1, :cond_6

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, v3, v2

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_6
    move-object v1, v0

    .line 233
    :goto_3
    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    invoke-static {v6}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 234
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_7

    .line 235
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v3, v4

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    :cond_8
    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v4

    const-string v6, "/"

    if-eqz v4, :cond_a

    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 239
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_9

    .line 240
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 241
    :cond_9
    aget-object v4, v3, v12

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v3, v12

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 245
    :cond_a
    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_c

    .line 246
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_b

    .line 247
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_b
    const/4 p1, 0x3

    .line 248
    aget-object v4, v3, p1

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 249
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, v3, p1

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_c
    move-object p1, v1

    .line 256
    :goto_4
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    return-object v0

    .line 257
    :cond_d
    invoke-static {p1, v13}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_e

    .line 258
    array-length v1, p1

    if-lez v1, :cond_e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b><font color=\"#f57902\">\uffe5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, p1, v2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "</font></b>\u8d77"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_e
    :goto_5
    return-object v0
.end method

.method public static synthetic lambda$rKn5vJLMh3aIGFWZkN8-PZ-saVo(Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;)V
    .locals 0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->queryFlight()V

    return-void
.end method

.method private prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "*>;ZZ)",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p1

    const-string v1, "TicketLeftQuery"

    const-string v2, "prepareResult"

    .line 264
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v2, v7, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 268
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 269
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 270
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 272
    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 275
    :cond_1
    :try_start_0
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v2, v0

    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sort:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v10, 0x0

    .line 282
    :goto_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-ge v10, v1, :cond_f

    .line 283
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 286
    iget-object v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->isNotMatchFilter(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto/16 :goto_b

    .line 287
    :cond_2
    iget-object v1, v7, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v1, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_3

    .line 289
    iget-boolean v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->isSelected:Z

    if-nez v1, :cond_3

    goto/16 :goto_b

    .line 291
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, ""

    if-eqz p3, :cond_4

    const-string v2, "<b>"

    goto :goto_3

    :cond_4
    move-object v2, v12

    .line 292
    :goto_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_6

    .line 294
    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "<font color=\"#ff0000\">"

    .line 295
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_5
    const-string v2, "<font color=\"#0000ff\">"

    .line 297
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :cond_6
    :goto_4
    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_7

    const-string v2, "</font>"

    .line 303
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    if-eqz p3, :cond_8

    const-string v2, "</b>"

    goto :goto_5

    :cond_8
    move-object v2, v12

    .line 305
    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 308
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "train"

    invoke-interface {v13, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    const-string v2, "type"

    invoke-interface {v13, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    if-eqz p3, :cond_9

    const-string v3, "<b><font color=\"#0000ff\">&nbsp;"

    goto :goto_6

    :cond_9
    move-object v3, v2

    :goto_6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_a

    const-string v3, "</font></b>"

    goto :goto_7

    :cond_a
    move-object v3, v12

    :goto_7
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " \u5f00"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "<br/>"

    if-eqz p3, :cond_b

    move-object v3, v14

    goto :goto_8

    :cond_b
    const-string v3, "\n"

    :goto_8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u5230"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "station"

    .line 318
    invoke-interface {v13, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 321
    iget-object v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 322
    array-length v2, v1

    const-string v3, "time"

    const/4 v4, 0x1

    if-le v2, v4, :cond_c

    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v1, v9

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u65f6"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u5206"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v13, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 325
    :cond_c
    iget-object v1, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    invoke-interface {v13, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :cond_d
    :goto_9
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const-string v2, " \u5546\u52a1:"

    const-string v5, "9"

    move-object/from16 v1, p0

    move/from16 v4, p3

    move-object v6, v11

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const-string v2, " \u7279\u7b49:"

    const-string v5, "P"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const-string v2, " \u4e00\u7b49:"

    const-string v5, "M"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const-string v2, " \u4e8c\u7b49:"

    const-string v5, "O"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    const-string v2, " \u52a8\u5367:"

    const-string v5, "F"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const-string v2, " \u9ad8\u7ea7\u8f6f\u5367:"

    const-string v5, "6"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const-string v2, " \u8f6f\u5367:"

    const-string v5, "4"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const-string v2, " \u786c\u5367:"

    const-string v5, "3"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const-string v2, " \u8f6f\u5ea7:"

    const-string v5, "2"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v2, " \u786c\u5ea7:"

    const-string v5, "1"

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    iget-object v3, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const-string v2, " \u65e0\u5ea7:"

    const-string v5, ""

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->formatTicket(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    invoke-virtual {v11}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->getNote()Ljava/lang/String;

    move-result-object v1

    .line 343
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "price"

    const-string v4, "ticket"

    if-eqz v2, :cond_e

    .line 344
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v13, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    invoke-interface {v13, v3, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 348
    :cond_e
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v13, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    invoke-direct {v7, v11}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->getPrice(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v13, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    :goto_a
    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_b
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    :cond_f
    return-object v0
.end method

.method private queryFlight()V
    .locals 5

    const-string v0, "TicketLeftQuery"

    const-string v1, "query flight begin"

    .line 101
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v1, Lcom/lltskb/lltskb/engine/online/FlightQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/FlightQuery;-><init>()V

    .line 103
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->startStation:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->arriveStation:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/lltskb/lltskb/engine/online/FlightQuery;->getFlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    const-string v1, "query flight end"

    .line 104
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/-$$Lambda$TicketLeftQuery$d4gjzBUb9aSwfsJsm6ZMlCg2Y-w;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/-$$Lambda$TicketLeftQuery$d4gjzBUb9aSwfsJsm6ZMlCg2Y-w;-><init>(Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public doQuery()Ljava/lang/String;
    .locals 7

    .line 66
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    if-nez v0, :cond_0

    const-string v0, "\u9519\u8bef"

    return-object v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->startStation:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->arriveStation:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->startStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->arriveStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TicketLeftQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    const/4 v0, 0x0

    .line 80
    :try_start_0
    new-instance v2, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;-><init>()V

    .line 81
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->startStation:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->arriveStation:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/LltSettings;->getPurpose()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 83
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getResult()Ljava/util/Vector;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    :cond_1
    if-nez v3, :cond_2

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "query result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getErrorMsg()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const-string v3, "zzquery"

    const-string v4, "\u4f59\u7968\u67e5\u8be2\u5931\u8d25"

    invoke-static {v1, v3, v4}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 91
    :cond_2
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/lltskb/lltskb/engine/online/-$$Lambda$TicketLeftQuery$rKn5vJLMh3aIGFWZkN8-PZ-saVo;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/-$$Lambda$TicketLeftQuery$rKn5vJLMh3aIGFWZkN8-PZ-saVo;-><init>(Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    .line 95
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 96
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
    .locals 11

    const-string v0, "TicketLeftQuery"

    const-string v1, "getAdapter"

    .line 120
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3, v2}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_1

    return-object v1

    .line 126
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const v7, 0x7f0b0086

    const/4 v1, 0x5

    new-array v8, v1, [Ljava/lang/String;

    const-string v4, "train"

    aput-object v4, v8, v3

    const-string v4, "station"

    aput-object v4, v8, v2

    const/4 v4, 0x2

    const-string v9, "time"

    aput-object v9, v8, v4

    const/4 v4, 0x3

    const-string v9, "ticket"

    aput-object v9, v8, v4

    const/4 v4, 0x4

    const-string v9, "price"

    aput-object v9, v8, v4

    new-array v9, v1, [I

    fill-array-data v9, :array_0

    move-object v4, v0

    move-object v10, p0

    invoke-direct/range {v4 .. v10}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILcom/lltskb/lltskb/engine/online/BaseQuery;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setShowBookButton(Z)V

    .line 133
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setShowPrice(Z)V

    .line 134
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x7f09000d
        0x7f090008
        0x7f09000c
        0x7f09000b
        0x7f090007
    .end array-data
.end method

.method public getDisplayResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 4

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-string v1, ""

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 53
    array-length v2, v0

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    return-object v1

    .line 54
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " \u2192 "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u6708"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u65e5\u4f59\u7968"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    :goto_0
    return-object v1
.end method

.method public synthetic lambda$queryFlight$0$TicketLeftQuery()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onItemClicked(I)V
    .locals 3

    const-string v0, "TicketLeftQuery"

    const-string v1, "onItemClicked"

    .line 423
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    if-eqz v1, :cond_1

    .line 425
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-le v2, p1, :cond_1

    if-gez p1, :cond_0

    goto :goto_0

    .line 429
    :cond_0
    invoke-virtual {v1, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 432
    sput-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    const/4 v0, 0x0

    .line 433
    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    .line 435
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 449
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const-string v2, "ticket_arrive_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 451
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const-string v2, "train_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 452
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const-string v2, "train_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "ticket_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 454
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const-string v1, "start_train_date"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 456
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 457
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "onItemClicked error"

    .line 426
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onOrderClicked(I)V
    .locals 3

    .line 408
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mDisplayResult:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 409
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    goto :goto_0

    .line 410
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u2192"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 413
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-class v2, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 414
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->startStation:Ljava/lang/String;

    const-string v2, "order_from_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 415
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->arriveStation:Ljava/lang/String;

    const-string v2, "order_to_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "order_depart_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "order_train_code"

    .line 417
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 418
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 140
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    if-nez v0, :cond_0

    const-string v0, "\u9519\u8bef"

    return-object v0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 146
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 148
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string v4, "train"

    .line 149
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "station"

    .line 151
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u4f59\u7968\uff1a"

    .line 153
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "ticket"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n\n"

    .line 154
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
