.class public Lcom/lltskb/lltskb/engine/online/BigScreenModel;
.super Ljava/lang/Object;
.source "BigScreenModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;,
        Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BSModel"

.field private static instance:Lcom/lltskb/lltskb/engine/online/BigScreenModel;


# instance fields
.field private bigScreenDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final fullBigSreenTrainId:Ljava/lang/String;

.field private final getBigScreenByStationCode:Ljava/lang/String;

.field private mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mUserAgent:Ljava/lang/String;

.field private stationMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->instance:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mUserAgent:Ljava/lang/String;

    const-string v0, "aHR0cHM6Ly93aWZpLjEyMzA2LmNuL29wZXJhdGVtb25pdC93aWZpYXBwcy9hcHBGcm9udEVuZC92Mi9rcEJpZ1NjcmVlbi9nZXRGdWxsQmlnU2NyZWVuVHJhaW5JZA=="

    .line 35
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->fullBigSreenTrainId:Ljava/lang/String;

    const-string v0, "aHR0cHM6Ly93aWZpLjEyMzA2LmNuL29wZXJhdGVtb25pdC93aWZpYXBwcy9hcHBGcm9udEVuZC92Mi9rcEJpZ1NjcmVlbi9nZXRCaWdTY3JlZW5CeVN0YXRpb25Db2RlQW5kVHJhaW5EYXRlP3N0YXRpb25Db2RlPSVzJnN0YXRpb25OYW1lPSVzJnRyYWluRGF0ZT0lcyZ0eXBlPSVz"

    .line 38
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getBigScreenByStationCode:Ljava/lang/String;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    .line 93
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    .line 94
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mUserAgent:Ljava/lang/String;

    return-void
.end method

.method public static getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;
    .locals 1

    .line 89
    sget-object v0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->instance:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    return-object v0
.end method

.method private getStatus(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "\u6b63\u5728\u5019\u8f66"

    .line 276
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_1
    const-string v0, "\u6b63\u5728\u68c0\u7968"

    .line 279
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x2

    return p1

    :cond_2
    const-string v0, "\u505c\u6b62\u68c0\u7968"

    .line 282
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p1, 0x3

    return p1

    :cond_3
    const-string v0, "\u6b63\u70b9"

    .line 286
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 p1, 0x4

    return p1

    :cond_4
    const-string v0, "\u665a\u70b9"

    .line 290
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 p1, 0x5

    return p1

    :cond_5
    const-string v0, "\u65e9\u70b9"

    .line 294
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p1, 0x6

    return p1

    :cond_6
    const/4 p1, -0x1

    return p1
.end method

.method private parseBigScreenData(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "BSModel"

    .line 329
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 334
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "data"

    .line 335
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    .line 338
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 339
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 341
    new-instance v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;-><init>(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V

    const-string v4, "arriveTime"

    const-wide/16 v5, 0x0

    .line 342
    invoke-virtual {v2, v4, v5, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v7

    iput-wide v7, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->arriveTime:J

    const-string v4, "currentStationName"

    .line 343
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->currentStationName:Ljava/lang/String;

    const-string v4, "delay"

    .line 344
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->delay:I

    const-string v4, "departTime"

    .line 345
    invoke-virtual {v2, v4, v5, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->departTime:J

    const-string v4, "dispTrainCode"

    .line 346
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->dispTrainCode:Ljava/lang/String;

    const-string v4, "endStationName"

    .line 347
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->endStationName:Ljava/lang/String;

    const-string v4, "exit"

    .line 348
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->exit:Ljava/lang/String;

    const-string v4, "platform"

    .line 349
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->platform:Ljava/lang/String;

    const-string v4, "startStationName"

    .line 350
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->startStationName:Ljava/lang/String;

    const-string v4, "startTrainCode"

    .line 351
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->startTrainCode:Ljava/lang/String;

    const-string v4, "stationTrainCode"

    .line 352
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->stationTrainCode:Ljava/lang/String;

    const-string v4, "status"

    .line 353
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->status:I

    const-string v4, "type"

    .line 354
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->type:Ljava/lang/String;

    const-string v4, "updateTime"

    .line 355
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->updateTime:J

    const-string v4, "waitingRoom"

    .line 356
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->waitingRoom:Ljava/lang/String;

    const-string v4, "wicket"

    .line 357
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->wicket:Ljava/lang/String;

    .line 358
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception p1

    .line 364
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 365
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " size="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    return-object p1
.end method

.method private parseSTBigScreenData(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "startstation"

    const-string v1, "trainname"

    const-string v2, "-"

    const-string v3, "BSModel"

    const-string v4, "exitStation"

    .line 220
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 225
    :cond_0
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "data"

    .line 226
    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 v5, 0x0

    .line 229
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 230
    invoke-virtual {p1, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 238
    new-instance v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;

    invoke-direct {v7, p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;-><init>(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V

    const-string v8, "aTime"

    .line 239
    invoke-virtual {v6, v8, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->aTime:Ljava/lang/String;

    const-string v8, "station"

    .line 240
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->currentStationName:Ljava/lang/String;

    const-string v8, "delay"

    .line 241
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->delay:I

    const-string v8, "dTime"

    .line 242
    invoke-virtual {v6, v8, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->dTime:Ljava/lang/String;

    .line 243
    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->dispTrainCode:Ljava/lang/String;

    const-string v8, "endstation"

    .line 244
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->endStationName:Ljava/lang/String;

    .line 245
    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->exit:Ljava/lang/String;

    .line 246
    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->platform:Ljava/lang/String;

    .line 247
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->startStationName:Ljava/lang/String;

    .line 248
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->startTrainCode:Ljava/lang/String;

    .line 249
    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->stationTrainCode:Ljava/lang/String;

    const-string v8, "statusInfo"

    .line 251
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->statusInfo:Ljava/lang/String;

    .line 252
    iget-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->statusInfo:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getStatus(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->status:I

    .line 255
    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->waitingRoom:Ljava/lang/String;

    const-string v8, "ticketCheck"

    .line 256
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;->wicket:Ljava/lang/String;

    .line 257
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception p1

    .line 263
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 264
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " size="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    return-object p1
.end method

.method private parseStStationListResult(Ljava/lang/String;)V
    .locals 6

    const-string v0, "BSModel"

    const-string v1, ""

    .line 123
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    .line 128
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "data"

    .line 129
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 131
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    const/4 v2, 0x0

    .line 132
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 133
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 135
    new-instance v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;-><init>(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V

    .line 136
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationPinyin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    .line 137
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationPinyinShort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyinShort:Ljava/lang/String;

    .line 138
    iput-object v1, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->presaleTime:Ljava/lang/String;

    .line 139
    iput-object v1, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->source:Ljava/lang/String;

    .line 140
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationCode:Ljava/lang/String;

    .line 141
    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    .line 142
    iput-object v1, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationOrder:Ljava/lang/String;

    .line 143
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 149
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 150
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "laod station count="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private parseStationListResult(Ljava/lang/String;)V
    .locals 5

    const-string v0, "BSModel"

    .line 163
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 168
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "data"

    .line 169
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 171
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    const/4 v1, 0x0

    .line 172
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 173
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 175
    new-instance v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;-><init>(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V

    const-string v4, "pinyin"

    .line 176
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    const-string v4, "pinyinShort"

    .line 177
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyinShort:Ljava/lang/String;

    const-string v4, "presaleTime"

    .line 178
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->presaleTime:Ljava/lang/String;

    const-string v4, "source"

    .line 179
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->source:Ljava/lang/String;

    const-string v4, "stationCode"

    .line 180
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationCode:Ljava/lang/String;

    const-string v4, "stationName"

    .line 181
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    const-string v4, "stationOrder"

    .line 182
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationOrder:Ljava/lang/String;

    .line 183
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationName:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 189
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "laod station count="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getBigScreen(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation

    .line 303
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 304
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 305
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "yyyyMMdd"

    invoke-direct {v1, v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 306
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    if-nez v1, :cond_0

    .line 311
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->loadStationList()V

    .line 312
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    .line 315
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_2

    if-nez v1, :cond_1

    goto :goto_0

    .line 319
    :cond_1
    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->stationCode:Ljava/lang/String;

    .line 321
    new-instance v2, Ljava/lang/String;

    const-string v3, "aHR0cHM6Ly93aWZpLjEyMzA2LmNuL29wZXJhdGVtb25pdC93aWZpYXBwcy9hcHBGcm9udEVuZC92Mi9rcEJpZ1NjcmVlbi9nZXRCaWdTY3JlZW5CeVN0YXRpb25Db2RlQW5kVHJhaW5EYXRlP3N0YXRpb25Db2RlPSVzJnN0YXRpb25OYW1lPSVzJnRyYWluRGF0ZT0lcyZ0eXBlPSVz"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/Base64;->decode([BI)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 323
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v4

    const/4 v1, 0x1

    aput-object p1, v5, v1

    const/4 p1, 0x2

    aput-object v0, v5, p1

    const/4 p1, 0x3

    aput-object p2, v5, p1

    invoke-static {v3, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 324
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mUserAgent:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getWithUserAgent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 325
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->parseBigScreenData(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getBigScreenDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    return-object v0
.end method

.method public getSTBigScreen(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$BigScreenData;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->bigScreenDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 198
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->loadStStationList()V

    :cond_0
    const/4 v0, 0x0

    .line 202
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "station="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "utf-8"

    invoke-static {p1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "A"

    .line 204
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v1, "https://g.xiuxiu365.cn//train_api/screen/msg/endTrainInfo"

    invoke-virtual {p2, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string v1, "D"

    .line 206
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 207
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v1, "https://g.xiuxiu365.cn//train_api/screen/msg/startTrainInfo"

    invoke-virtual {p2, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_0
    const-string p2, "BSModel"

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSTBigScreen result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->parseSTBigScreenData(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 212
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v0
.end method

.method public getStationMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
            ">;"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->stationMap:Ljava/util/Map;

    return-object v0
.end method

.method public loadStStationList()V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v1, "https://g.xiuxiu365.cn/train_api/sh/screen/station"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadStStationList result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BSModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->parseStStationListResult(Ljava/lang/String;)V

    return-void
.end method

.method public loadStationList()V
    .locals 3

    .line 156
    new-instance v0, Ljava/lang/String;

    const-string v1, "aHR0cHM6Ly93aWZpLjEyMzA2LmNuL29wZXJhdGVtb25pdC93aWZpYXBwcy9hcHBGcm9udEVuZC92Mi9rcEJpZ1NjcmVlbi9nZXRGdWxsQmlnU2NyZWVuVHJhaW5JZA=="

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Base64;->decode([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 158
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getWithUserAgent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->parseStationListResult(Ljava/lang/String;)V

    return-void
.end method
