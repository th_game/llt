.class public Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "ZwdQueryActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ZwdQueryActivity"


# instance fields
.field private mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

.field private mStationList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mSwitchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

.field private mTrain:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onHotel()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onFlight()V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onShare()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onSelectAll()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/lang/String;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mTrain:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/util/Vector;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mStationList:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$502(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mStationList:Ljava/util/Vector;

    return-object p1
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Lcom/lltskb/lltskb/adapters/ZwdListAdapter;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    return-object p0
.end method

.method private getZwdType()Ljava/lang/String;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mSwitchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    const-string v1, "0"

    if-nez v0, :cond_0

    return-object v1

    .line 53
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/SwitchView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "1"

    :cond_1
    return-object v1
.end method

.method private initToolbar()V
    .locals 2

    const v0, 0x7f0901e1

    .line 220
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 222
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 223
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 225
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private initView()V
    .locals 8

    const-string v0, "ZwdQueryActivity"

    const-string v1, "initView"

    .line 56
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 57
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->requestWindowFeature(I)Z

    const v1, 0x7f0b00a0

    .line 58
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const v2, 0x7f090186

    .line 65
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    const-string v3, "train_name"

    .line 66
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mTrain:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "query_jpk"

    .line 67
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "query_date"

    .line 68
    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v5, 0x7f09020d

    .line 70
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/view/widget/SwitchView;

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mSwitchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    .line 72
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mSwitchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    const/16 v6, 0x8

    if-eqz v5, :cond_2

    .line 73
    new-instance v7, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$ZwdQueryActivity$HK3kRiGdJtyscZGgbXdUC1RmPwE;

    invoke-direct {v7, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$ZwdQueryActivity$HK3kRiGdJtyscZGgbXdUC1RmPwE;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v5, v7}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setOnClickCheckedListener(Lcom/lltskb/lltskb/view/widget/SwitchView$onClickCheckedListener;)V

    .line 79
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mSwitchView:Lcom/lltskb/lltskb/view/widget/SwitchView;

    if-eqz v4, :cond_1

    const/16 v7, 0x8

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v5, v7}, Lcom/lltskb/lltskb/view/widget/SwitchView;->setVisibility(I)V

    .line 82
    :cond_2
    new-instance v5, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->getZwdType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, p0, v4, v1, v7}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;-><init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    .line 83
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v1, 0x7f0901c2

    .line 85
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_4

    if-eqz v4, :cond_3

    .line 88
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d013b

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mTrain:Ljava/lang/String;

    aput-object v5, v0, v3

    invoke-static {v2, v4, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 91
    :cond_3
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d0159

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mTrain:Ljava/lang/String;

    aput-object v5, v0, v3

    invoke-static {v2, v4, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    const v0, 0x7f0900f5

    .line 97
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 99
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v0, 0x7f090044

    .line 107
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 109
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 110
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$2;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$2;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    const v0, 0x7f0900d5

    .line 118
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 120
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$3;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$3;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    const v0, 0x7f09006c

    .line 128
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 130
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    const v0, 0x7f09006e

    .line 133
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 135
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    const v0, 0x7f09006f

    .line 138
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 140
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$4;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    const v0, 0x7f0901e1

    .line 148
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_b

    .line 150
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$5;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$5;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_b
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->initToolbar()V

    .line 159
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->queryTrain()V

    return-void
.end method

.method private onFlight()V
    .locals 1

    const/4 v0, 0x0

    .line 244
    invoke-static {p0, v0, v0, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onHotel()V
    .locals 1

    const/4 v0, 0x0

    .line 240
    invoke-static {p0, v0, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onSelectAll()V
    .locals 3

    const v0, 0x7f0901e1

    .line 210
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    return-void

    .line 212
    :cond_0
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const/4 v1, 0x0

    .line 213
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mStationList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 214
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mStationList:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    iput-boolean v0, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->isSelected:Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mStationList:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->setData(Ljava/util/Vector;)V

    return-void
.end method

.method private onShare()V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->setSelectMode(Z)V

    .line 165
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->initToolbar()V

    return-void

    .line 169
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    .line 205
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->setSelectMode(Z)V

    .line 206
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->initToolbar()V

    return-void
.end method

.method private queryTrain()V
    .locals 2

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query train ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZwdQueryActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$7;-><init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public btn_flight(Landroid/view/View;)V
    .locals 0

    .line 232
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onFlight()V

    return-void
.end method

.method public btn_hotel(Landroid/view/View;)V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onHotel()V

    return-void
.end method

.method public synthetic lambda$initView$0$ZwdQueryActivity()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->mListAdapter:Lcom/lltskb/lltskb/adapters/ZwdListAdapter;

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->getZwdType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/ZwdListAdapter;->setZWDType(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 45
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->initView()V

    return-void
.end method
