.class public Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;
.super Ljava/lang/Object;
.source "PassengerInfo.java"


# instance fields
.field private accountName:Ljava/lang/String;

.field private first_letter:Ljava/lang/String;

.field private index:Ljava/lang/Integer;

.field private isUserSelf:Ljava/lang/String;

.field private mobile_no:Ljava/lang/String;

.field private old_passenger_id_no:Ljava/lang/String;

.field private old_passenger_id_type_code:Ljava/lang/String;

.field private old_passenger_name:Ljava/lang/String;

.field private passenger_flag:Ljava/lang/String;

.field private passenger_id_no:Ljava/lang/String;

.field private passenger_id_type_code:Ljava/lang/String;

.field private passenger_id_type_name:Ljava/lang/String;

.field private passenger_name:Ljava/lang/String;

.field private passenger_type:Ljava/lang/String;

.field private passenger_type_name:Ljava/lang/String;

.field private recordCount:Ljava/lang/String;

.field private sex_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public getFirst_letter()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->first_letter:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()Ljava/lang/Integer;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->index:Ljava/lang/Integer;

    return-object v0
.end method

.method public getIsUserSelf()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->isUserSelf:Ljava/lang/String;

    return-object v0
.end method

.method public getMobile_no()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->mobile_no:Ljava/lang/String;

    return-object v0
.end method

.method public getOld_passenger_id_no()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_id_no:Ljava/lang/String;

    return-object v0
.end method

.method public getOld_passenger_id_type_code()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_id_type_code:Ljava/lang/String;

    return-object v0
.end method

.method public getOld_passenger_name()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_flag()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_flag:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_id_no()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_no:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_id_type_code()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_type_code:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_id_type_name()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_type_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_name()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_type()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_type:Ljava/lang/String;

    return-object v0
.end method

.method public getPassenger_type_name()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_type_name:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordCount()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->recordCount:Ljava/lang/String;

    return-object v0
.end method

.method public getSex_name()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->sex_name:Ljava/lang/String;

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)V
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->accountName:Ljava/lang/String;

    return-void
.end method

.method public setFirst_letter(Ljava/lang/String;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->first_letter:Ljava/lang/String;

    return-void
.end method

.method public setIndex(Ljava/lang/Integer;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->index:Ljava/lang/Integer;

    return-void
.end method

.method public setIsUserSelf(Ljava/lang/String;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->isUserSelf:Ljava/lang/String;

    return-void
.end method

.method public setMobile_no(Ljava/lang/String;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->mobile_no:Ljava/lang/String;

    return-void
.end method

.method public setOld_passenger_id_no(Ljava/lang/String;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_id_no:Ljava/lang/String;

    return-void
.end method

.method public setOld_passenger_id_type_code(Ljava/lang/String;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_id_type_code:Ljava/lang/String;

    return-void
.end method

.method public setOld_passenger_name(Ljava/lang/String;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->old_passenger_name:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_flag(Ljava/lang/String;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_flag:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_id_no(Ljava/lang/String;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_no:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_id_type_code(Ljava/lang/String;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_type_code:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_id_type_name(Ljava/lang/String;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_id_type_name:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_name(Ljava/lang/String;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_name:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_type(Ljava/lang/String;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_type:Ljava/lang/String;

    return-void
.end method

.method public setPassenger_type_name(Ljava/lang/String;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->passenger_type_name:Ljava/lang/String;

    return-void
.end method

.method public setRecordCount(Ljava/lang/String;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->recordCount:Ljava/lang/String;

    return-void
.end method

.method public setSex_name(Ljava/lang/String;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerInfo;->sex_name:Ljava/lang/String;

    return-void
.end method
