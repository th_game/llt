.class public Lcom/lltskb/lltskb/engine/online/UserMgr;
.super Ljava/lang/Object;
.source "UserMgr.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountMgr"

.field private static instance:Lcom/lltskb/lltskb/engine/online/UserMgr; = null

.field private static mUsers:Ljava/util/Vector; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final prefName:Ljava/lang/String; = "account"


# instance fields
.field private mLastUserId:Ljava/lang/String;

.field private mPref:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/UserMgr;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/UserMgr;

    monitor-enter v0

    .line 29
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->instance:Lcom/lltskb/lltskb/engine/online/UserMgr;

    if-nez v1, :cond_0

    .line 30
    new-instance v1, Lcom/lltskb/lltskb/engine/online/UserMgr;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->instance:Lcom/lltskb/lltskb/engine/online/UserMgr;

    .line 32
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->instance:Lcom/lltskb/lltskb/engine/online/UserMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getJsonString()Ljava/lang/String;
    .locals 4

    .line 155
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 156
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    if-nez v1, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 157
    :goto_0
    sget-object v2, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 158
    sget-object v2, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    .line 159
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    .line 161
    :cond_1
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v2

    .line 162
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUserInfo(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;
    .locals 4

    .line 56
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    .line 57
    :goto_0
    sget-object v2, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 58
    sget-object v2, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    .line 59
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private parseUsers(Ljava/lang/String;)V
    .locals 3

    .line 130
    sget-object v0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 131
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 134
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    if-nez p1, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 139
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 140
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 141
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;-><init>()V

    .line 142
    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->initAccount(Lorg/json/JSONObject;)V

    .line 143
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1

    .line 145
    :cond_2
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/online/UserMgr;->addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 149
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    return-void
.end method


# virtual methods
.method public addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->removeUser(Ljava/lang/String;)V

    .line 70
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;-><init>()V

    .line 71
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setAccount(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberPass()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberPass(Z)V

    .line 74
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->isRemeberName()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberName(Z)V

    .line 75
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getUserName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setUserName(Ljava/lang/String;)V

    .line 77
    sget-object p1, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserInfo(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v0

    return-object v0
.end method

.method public getUser(I)Lcom/lltskb/lltskb/engine/online/dto/UserInfo;
    .locals 1

    .line 123
    sget-object v0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 124
    sget-object v0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getUserCount()I
    .locals 1

    .line 119
    sget-object v0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const-string v1, "account"

    .line 96
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    .line 97
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-string v0, ""

    const-string v1, "account_list"

    .line 98
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 99
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    const-string v2, "lastUser"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    const-string v2, "sessionId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->setSessionId(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->parseUsers(Ljava/lang/String;)V

    .line 103
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "user count="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getUserCount()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AccountMgr"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public removeUser(Ljava/lang/String;)V
    .locals 2

    .line 81
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 82
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 83
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    .line 84
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    sget-object v1, Lcom/lltskb/lltskb/engine/online/UserMgr;->mUsers:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 86
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ""

    .line 87
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public save()V
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 112
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getJsonString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_list"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 113
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    const-string v2, "lastUser"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getSessionId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sessionId"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 115
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setLastUser(Ljava/lang/String;)V
    .locals 2

    .line 40
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    .line 41
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mPref:Landroid/content/SharedPreferences;

    if-nez p1, :cond_0

    return-void

    .line 42
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 43
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/UserMgr;->mLastUserId:Ljava/lang/String;

    const-string v1, "lastUser"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 44
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "sessionId"

    .line 46
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 48
    :cond_1
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
