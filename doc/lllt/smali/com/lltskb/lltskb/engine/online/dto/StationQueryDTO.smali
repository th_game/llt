.class public Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;
.source "StationQueryDTO.java"


# instance fields
.field public arrive_day_diff:Ljava/lang/String;

.field public arrive_time:Ljava/lang/String;

.field public bureau_code:Ljava/lang/String;

.field public city_code:Ljava/lang/String;

.field public display_station_no:Ljava/lang/String;

.field public end_arrive_time:Ljava/lang/String;

.field public end_station_name:Ljava/lang/String;

.field public end_station_telecode:Ljava/lang/String;

.field public running_time:Ljava/lang/String;

.field public same_city_code:Ljava/lang/String;

.field public seat_types:Ljava/lang/String;

.field public service_type:Ljava/lang/String;

.field public start_date:Ljava/lang/String;

.field public start_day_diff:Ljava/lang/String;

.field public start_start_time:Ljava/lang/String;

.field public start_station_name:Ljava/lang/String;

.field public start_station_telecode:Ljava/lang/String;

.field public start_time:Ljava/lang/String;

.field public start_train_date:Ljava/lang/String;

.field public station_name:Ljava/lang/String;

.field public station_no:Ljava/lang/String;

.field public station_telecode:Ljava/lang/String;

.field public station_train_code:Ljava/lang/String;

.field public station_train_date:Ljava/lang/String;

.field public stop_date:Ljava/lang/String;

.field public stopover_time:Ljava/lang/String;

.field public train_class_code:Ljava/lang/String;

.field public train_class_name:Ljava/lang/String;

.field public train_no:Ljava/lang/String;

.field public train_type_code:Ljava/lang/String;

.field public train_type_name:Ljava/lang/String;

.field public update_arrive_time:Ljava/lang/String;

.field public update_start_time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;-><init>()V

    return-void
.end method
