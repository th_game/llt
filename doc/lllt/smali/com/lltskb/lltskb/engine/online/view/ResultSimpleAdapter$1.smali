.class Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$1;
.super Ljava/lang/Object;
.source "ResultSimpleAdapter.java"

# interfaces
.implements Landroid/widget/SimpleAdapter$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$1;->this$0:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setViewValue(Landroid/view/View;Ljava/lang/Object;Ljava/lang/String;)Z
    .locals 1

    .line 153
    instance-of p3, p1, Landroid/widget/TextView;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    instance-of p3, p2, Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 154
    check-cast p1, Landroid/widget/TextView;

    .line 155
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method
