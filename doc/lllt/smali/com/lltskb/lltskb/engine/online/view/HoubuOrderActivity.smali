.class public final Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "HoubuOrderActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0016B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0008\u0010\u0011\u001a\u00020\u0010H\u0002J\u0012\u0010\u0012\u001a\u00020\u00102\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0008\u0010\u0015\u001a\u00020\u0010H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0018\u00010\u0006R\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;",
        "Lcom/lltskb/lltskb/BaseActivity;",
        "()V",
        "mOrderFragment",
        "Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;",
        "mPagerAdapter",
        "Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;",
        "mProcessedOrderFragment",
        "Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;",
        "mTabLayout",
        "Landroid/support/design/widget/TabLayout;",
        "mViewPager",
        "Landroid/support/v4/view/ViewPager;",
        "mWaitingOrderFragment",
        "Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;",
        "initView",
        "",
        "initViewPagerFragment",
        "onCreate",
        "arg0",
        "Landroid/os/Bundle;",
        "onRefresh",
        "PagerAdapter",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

.field private mPagerAdapter:Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

.field private mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

.field private mTabLayout:Landroid/support/design/widget/TabLayout;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    .line 30
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    .line 31
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    .line 33
    new-instance v0, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    invoke-direct {v0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    return-void
.end method

.method public static final synthetic access$getMOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    return-object p0
.end method

.method public static final synthetic access$getMProcessedOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    return-object p0
.end method

.method public static final synthetic access$getMWaitingOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    return-object p0
.end method

.method public static final synthetic access$onRefresh(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->onRefresh()V

    return-void
.end method

.method public static final synthetic access$setMOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    return-void
.end method

.method public static final synthetic access$setMProcessedOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    return-void
.end method

.method public static final synthetic access$setMWaitingOrderFragment$p(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    return-void
.end method

.method private final initView()V
    .locals 2

    const v0, 0x7f090016

    .line 38
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    const v0, 0x7f09001a

    .line 39
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    const v0, 0x7f090067

    .line 40
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.btn_refresh)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    .line 41
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 42
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$initView$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$initView$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->initViewPagerFragment()V

    .line 47
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->onRefresh()V

    return-void
.end method

.method private final initViewPagerFragment()V
    .locals 3

    .line 65
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "supportFragmentManager"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;-><init>(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mPagerAdapter:Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const v1, 0x7f0d016b

    .line 68
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0d016c

    .line 69
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0d016a

    .line 70
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 74
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mPagerAdapter:Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

    if-eqz v2, :cond_0

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;->setTitles(Ljava/util/ArrayList;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mPagerAdapter:Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

    if-eqz v0, :cond_1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;->setFragments(Ljava/util/List;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mPagerAdapter:Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$PagerAdapter;

    check-cast v1, Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$initViewPagerFragment$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity$initViewPagerFragment$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;)V

    check-cast v1, Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 106
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 109
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setTabMode(I)V

    :cond_6
    const v0, 0x7f090223

    .line 111
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 112
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d01ba

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    return-void
.end method

.method private final onRefresh()V
    .locals 3

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    .line 52
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_2

    .line 53
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuNotCompleteOrderFragment;->refreshOrder()V

    goto :goto_3

    :cond_2
    :goto_1
    if-nez v0, :cond_3

    goto :goto_2

    .line 55
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 56
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mWaitingOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuWaitingOrderFragment;->refreshOrder()V

    goto :goto_3

    :cond_4
    :goto_2
    if-nez v0, :cond_5

    goto :goto_3

    .line 58
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->mProcessedOrderFragment:Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuProcessedOrderFragment;->refreshOrder()V

    :cond_6
    :goto_3
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 23
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0b0048

    .line 24
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->setContentView(I)V

    .line 25
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;->initView()V

    return-void
.end method
