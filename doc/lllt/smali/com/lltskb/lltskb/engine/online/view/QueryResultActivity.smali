.class public Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "QueryResultActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;


# static fields
.field public static final MENU_BACK_ID:I = 0x1

.field public static final MENU_SMS_ID:I = 0x2

.field protected static final TAG:Ljava/lang/String; = "QueryResultActivity"


# instance fields
.field private mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

.field public mArriveStation:Ljava/lang/String;

.field private mBook:Landroid/widget/Button;

.field private mBottombar:Landroid/view/View;

.field private mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

.field public mDate:Ljava/lang/String;

.field private mErrorTxt:Landroid/widget/TextView;

.field private mFilter:I

.field private mLastScrollPos:I

.field protected mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mLoadingLayout:Landroid/view/View;

.field public mPassCode:Ljava/lang/String;

.field private mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

.field private mPassEdt:Landroid/widget/EditText;

.field private mPassImage:Landroid/widget/ImageView;

.field private mProgBar:Landroid/widget/ProgressBar;

.field private mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

.field private mQueryType:Ljava/lang/String;

.field public mResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation
.end field

.field private mSelectAll:Landroid/widget/CheckBox;

.field private mSetFilter:Landroid/widget/Button;

.field private mSetSort:Landroid/widget/Button;

.field public mStartStation:Ljava/lang/String;

.field public mStation:Ljava/lang/String;

.field private mTimeView:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field public mTrainCode:Ljava/lang/String;

.field public mTrainName:Ljava/lang/String;

.field protected mTrainType:Ljava/lang/String;

.field private mUseOldZwdMethod:Z

.field private mZwd:Landroid/widget/Button;

.field private mZwdSwitch:Landroid/widget/Button;

.field private mZwdType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 73
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mUseOldZwdMethod:Z

    const/4 v1, 0x0

    .line 79
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLoadingLayout:Landroid/view/View;

    const-string v1, "0"

    .line 94
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdType:Ljava/lang/String;

    const/16 v1, 0xff

    .line 96
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mFilter:I

    .line 99
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLastScrollPos:I

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)Ljava/lang/String;
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->getMessage()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private checkToolbarButtons()V
    .locals 7

    const-string v0, "QueryResultActivity"

    const-string v1, "checkToolbarButtons"

    .line 535
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 537
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    if-nez v2, :cond_1

    return-void

    :cond_1
    const-string v3, "query_type_train"

    .line 539
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v4, 0x8

    if-eqz v2, :cond_2

    if-nez v0, :cond_2

    .line 540
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwd:Landroid/widget/Button;

    if-eqz v2, :cond_3

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 542
    :cond_2
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwd:Landroid/widget/Button;

    if-eqz v2, :cond_3

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 545
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    const-string v5, "query_type_train_zwd"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    const-string v6, "query_type_station"

    .line 546
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    .line 550
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTimeView:Landroid/view/View;

    if-eqz v2, :cond_7

    if-eqz v0, :cond_5

    const/16 v6, 0x8

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 547
    :cond_6
    :goto_3
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTimeView:Landroid/view/View;

    if-eqz v2, :cond_7

    .line 548
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 553
    :cond_7
    :goto_4
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 554
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdSwitch:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    .line 556
    :cond_8
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdSwitch:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 559
    :goto_5
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v2, :cond_a

    if-eqz v0, :cond_9

    const/4 v5, 0x0

    goto :goto_6

    :cond_9
    const/16 v5, 0x8

    .line 560
    :goto_6
    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 563
    :cond_a
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 564
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBook:Landroid/widget/Button;

    if-eqz v2, :cond_d

    if-eqz v0, :cond_b

    const/16 v3, 0x8

    goto :goto_7

    :cond_b
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_8

    .line 566
    :cond_c
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBook:Landroid/widget/Button;

    if-eqz v2, :cond_d

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 569
    :cond_d
    :goto_8
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    const-string v3, "query_type_ticket"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    const-string v3, "query_type_zztime"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    goto :goto_9

    .line 573
    :cond_e
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetFilter:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetSort:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_b

    .line 570
    :cond_f
    :goto_9
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetFilter:Landroid/widget/Button;

    if-eqz v0, :cond_10

    const/16 v3, 0x8

    goto :goto_a

    :cond_10
    const/4 v3, 0x0

    :goto_a
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 571
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetSort:Landroid/widget/Button;

    if-eqz v0, :cond_11

    const/16 v1, 0x8

    :cond_11
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_b
    return-void
.end method

.method private getMessage()Ljava/lang/String;
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "getMessage"

    .line 498
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initView()V
    .locals 12

    const-string v0, "QueryResultActivity"

    const-string v1, "initView"

    .line 193
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 194
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->requestWindowFeature(I)Z

    .line 198
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getZwdQueryType()I

    move-result v1

    const/4 v2, 0x0

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mUseOldZwdMethod:Z

    .line 200
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getFilterType()I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mFilter:I

    .line 202
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 205
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    const-string v3, "ticket_start_station"

    .line 208
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ticket_arrive_station"

    .line 209
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "train_code"

    .line 210
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainCode:Ljava/lang/String;

    const-string v5, "ticket_date"

    .line 211
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v5, "ticket_type"

    .line 212
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainType:Ljava/lang/String;

    const-string v5, "station_code"

    .line 213
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStation:Ljava/lang/String;

    const-string v5, "train_name"

    .line 214
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    .line 215
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    if-nez v5, :cond_3

    .line 216
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainCode:Ljava/lang/String;

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    :cond_3
    const-string v5, "query"

    .line 218
    invoke-virtual {p0, v5, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 220
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "start_point"

    .line 221
    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v6, "end_point"

    .line 222
    invoke-interface {v5, v6, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 223
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    const-string v5, "query_method"

    .line 226
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 227
    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    return-void

    :cond_5
    const-string v6, "query_method_skbcx"

    .line 230
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x2

    if-eqz v6, :cond_6

    const/4 v5, 0x1

    goto :goto_1

    :cond_6
    const-string v6, "query_method_trainno"

    .line 232
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x2

    goto :goto_1

    :cond_7
    const/4 v5, 0x0

    :goto_1
    const-string v6, "query_type"

    .line 235
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    .line 236
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    return-void

    .line 238
    :cond_8
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQueryType:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v8

    const/4 v9, 0x5

    const/4 v10, 0x4

    const/4 v11, 0x3

    sparse-switch v8, :sswitch_data_0

    goto :goto_2

    :sswitch_0
    const-string v8, "query_type_station"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x3

    goto :goto_2

    :sswitch_1
    const-string v8, "query_type_train_baike"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x5

    goto :goto_2

    :sswitch_2
    const-string v8, "query_type_train_zwd"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x4

    goto :goto_2

    :sswitch_3
    const-string v8, "query_type_train"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x1

    goto :goto_2

    :sswitch_4
    const-string v8, "query_type_zztime"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x2

    goto :goto_2

    :sswitch_5
    const-string v8, "query_type_ticket"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v6, 0x0

    :cond_9
    :goto_2
    if-eqz v6, :cond_10

    if-eq v6, v0, :cond_f

    if-eq v6, v7, :cond_e

    if-eq v6, v11, :cond_d

    if-eq v6, v10, :cond_b

    if-eq v6, v9, :cond_a

    goto :goto_3

    .line 263
    :cond_a
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;

    invoke-direct {v1, p0, v5}, Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 254
    :cond_b
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mUseOldZwdMethod:Z

    if-eqz v1, :cond_c

    .line 256
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdType:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v5}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;ILjava/lang/String;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 258
    :cond_c
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    invoke-direct {v1, p0, v5}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 251
    :cond_d
    new-instance v1, Lcom/lltskb/lltskb/engine/online/StationQuery;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/engine/online/StationQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 247
    :cond_e
    new-instance v1, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;

    invoke-direct {v1, p0, v5}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 244
    :cond_f
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;

    invoke-direct {v1, p0, v5}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    goto :goto_3

    .line 241
    :cond_10
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    :goto_3
    const v1, 0x7f0b0090

    .line 267
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setContentView(I)V

    const v1, 0x7f09017c

    .line 269
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLoadingLayout:Landroid/view/View;

    const v1, 0x7f0900b7

    .line 270
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mErrorTxt:Landroid/widget/TextView;

    const v1, 0x7f090173

    .line 271
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 272
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 273
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 274
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    .line 275
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v1, p0}, Lcom/lltskb/lltskb/view/widget/XListView;->setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V

    .line 276
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    const v1, 0x7f09003e

    .line 278
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBottombar:Landroid/view/View;

    .line 279
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBottombar:Landroid/view/View;

    if-eqz v1, :cond_11

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_11
    const v1, 0x7f0901c2

    .line 281
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTitleView:Landroid/widget/TextView;

    .line 282
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTitleView:Landroid/widget/TextView;

    if-eqz v1, :cond_12

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setSelected(Z)V

    :cond_12
    const v1, 0x7f09006c

    .line 284
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetFilter:Landroid/widget/Button;

    const v1, 0x7f09006e

    .line 285
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetSort:Landroid/widget/Button;

    .line 287
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetSort:Landroid/widget/Button;

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$LEQcw-csGk-vFXfvgeh5xS2NKuk;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$LEQcw-csGk-vFXfvgeh5xS2NKuk;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSetFilter:Landroid/widget/Button;

    if-eqz v1, :cond_13

    .line 290
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$VTBQ66_PgjcayMsBS8PjDfwJYTE;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$VTBQ66_PgjcayMsBS8PjDfwJYTE;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_13
    const v1, 0x7f0900d5

    .line 293
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_14

    .line 294
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_14
    const v1, 0x7f0900f5

    .line 296
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_15

    .line 297
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_15
    const v0, 0x7f09015f

    .line 299
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTimeView:Landroid/view/View;

    const v0, 0x7f090078

    .line 301
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwd:Landroid/widget/Button;

    .line 303
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwd:Landroid/widget/Button;

    if-eqz v0, :cond_16

    .line 304
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$yTUfBhdzyTN6QjNKvzHL8KWC9RQ;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$yTUfBhdzyTN6QjNKvzHL8KWC9RQ;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_16
    const v0, 0x7f090075

    .line 307
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdSwitch:Landroid/widget/Button;

    .line 308
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdSwitch:Landroid/widget/Button;

    if-eqz v0, :cond_17

    .line 309
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$mp3yFfIymhPOBQkNn4tB0ySS-fM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$mp3yFfIymhPOBQkNn4tB0ySS-fM;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_17
    const v0, 0x7f090047

    .line 312
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBook:Landroid/widget/Button;

    .line 314
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBook:Landroid/widget/Button;

    if-eqz v0, :cond_18

    .line 315
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$2KQUrO_UDqsRhEvxv3uExRHTIgU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$2KQUrO_UDqsRhEvxv3uExRHTIgU;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_18
    const v0, 0x7f090069

    .line 318
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_19

    .line 321
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$OzXW4TAfGhuXw5HBVOoREyQlC2A;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$OzXW4TAfGhuXw5HBVOoREyQlC2A;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_19
    if-eqz v3, :cond_1a

    .line 324
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    :cond_1a
    if-eqz v4, :cond_1b

    .line 326
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    :cond_1b
    const v0, 0x7f09006f

    .line 328
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_1c

    .line 330
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AnrxhZlbXVAV5Ok9_JqY1Mx9s-8;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AnrxhZlbXVAV5Ok9_JqY1Mx9s-8;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1c
    const v0, 0x7f090044

    .line 334
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_1d

    .line 336
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$O6-9l0qCLcawJ7qjACTgrmV1ltk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$O6-9l0qCLcawJ7qjACTgrmV1ltk;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1d
    const v0, 0x7f0901e1

    .line 340
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    .line 341
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1e

    .line 342
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$nt8Cz2wGZ4kPPW7OfkGgx_MF2fw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$nt8Cz2wGZ4kPPW7OfkGgx_MF2fw;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_1e
    const v0, 0x7f0902bf

    .line 353
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 355
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$DLIXnyLWk7VH4n4bfkqYVnpmnyw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$DLIXnyLWk7VH4n4bfkqYVnpmnyw;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1f
    const v0, 0x7f090273

    .line 358
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_20

    .line 360
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$4ZwJrCvYWpIC-LvrUnwFYms3Eq8;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$4ZwJrCvYWpIC-LvrUnwFYms3Eq8;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_20
    const v0, 0x7f0902ab

    .line 367
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 369
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$LsRBNvlib2vPAT-hPBtyTxIZcpM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$LsRBNvlib2vPAT-hPBtyTxIZcpM;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    :cond_21
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->checkToolbarButtons()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x78b45146 -> :sswitch_5
        -0x6d7ffa45 -> :sswitch_4
        -0x66f97f66 -> :sswitch_3
        -0x4592545e -> :sswitch_2
        -0x2baac901 -> :sswitch_1
        0x3ff13346 -> :sswitch_0
    .end sparse-switch
.end method

.method public static synthetic lambda$AjYZhmhT4wHE7RmI38v-paB2PDU(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showResult()V

    return-void
.end method

.method private onBook()V
    .locals 3

    const-string v0, "QueryResultActivity"

    const-string v1, "onBook"

    .line 525
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 528
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    const-string v2, "order_from_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 529
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    const-string v2, "order_to_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "order_depart_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 531
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private onChangeDate(Z)V
    .locals 11

    const-string v0, "QueryResultActivity"

    const-string v1, "onChangeDate"

    .line 416
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 421
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 423
    :catch_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 425
    :goto_0
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    if-eqz p1, :cond_0

    sub-long/2addr v1, v3

    goto :goto_1

    :cond_0
    add-long/2addr v1, v3

    .line 431
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultMaxDays()J

    move-result-wide v5

    .line 432
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 433
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    const-wide/16 v9, 0x18

    mul-long v5, v5, v9

    const-wide/16 v9, 0xe10

    mul-long v5, v5, v9

    const-wide/16 v9, 0x3e8

    mul-long v5, v5, v9

    add-long/2addr v5, v7

    sub-long/2addr v7, v3

    const/4 p1, 0x0

    const v3, 0x7f0d0111

    cmp-long v4, v1, v7

    if-gez v4, :cond_1

    .line 437
    invoke-static {p0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_1
    cmp-long v4, v1, v5

    if-lez v4, :cond_2

    .line 440
    invoke-static {p0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 443
    :cond_2
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    .line 445
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onRetry()V

    return-void
.end method

.method private onRetry()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "onRetry"

    .line 458
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090193

    .line 459
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 460
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setTitle(Ljava/lang/String;)V

    const v0, 0x7f090273

    .line 462
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 463
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getQueryMethod()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    .line 466
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showPassCodeDlg(Landroid/os/Bundle;)V

    goto :goto_0

    .line 468
    :cond_2
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->query(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private onSetFilter()V
    .locals 3

    .line 396
    new-instance v0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mFilter:I

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$dzlSMaaOG4M_Z07d95HTl-gv_iI;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$dzlSMaaOG4M_Z07d95HTl-gv_iI;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;-><init>(Landroid/content/Context;ILcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;)V

    .line 401
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->show()V

    return-void
.end method

.method private onSetSort()V
    .locals 8

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u51fa\u53d1\u65f6\u95f4"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "\u5230\u8fbe\u65f6\u95f4"

    aput-object v4, v1, v3

    const/4 v4, 0x2

    const-string v5, "\u901f\u5ea6\u4f18\u5148"

    aput-object v5, v1, v4

    const/4 v5, 0x3

    const-string v6, "\u4f59\u7968\u4f18\u5148"

    aput-object v6, v1, v5

    .line 998
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v6, v6, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;

    if-eqz v6, :cond_0

    const-string v6, "\u7968\u4ef7\u4f18\u5148"

    aput-object v6, v1, v5

    .line 1002
    :cond_0
    sget v6, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    if-ne v6, v4, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    .line 1004
    :cond_1
    sget v6, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v7, 0x20

    if-ne v6, v7, :cond_2

    const/4 v5, 0x2

    goto :goto_0

    .line 1006
    :cond_2
    sget v4, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v6, 0x8

    if-ne v4, v6, :cond_3

    goto :goto_0

    .line 1008
    :cond_3
    sget v4, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    if-ne v4, v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    .line 1012
    :goto_0
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$n83-O-HrJvkGWPMI527tmt6Yd8I;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$n83-O-HrJvkGWPMI527tmt6Yd8I;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    .line 1013
    invoke-virtual {v0, v1, v5, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108000a

    .line 1026
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1027
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0262

    .line 1028
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1029
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 1030
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    return-void
.end method

.method private onShare()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "onShare"

    .line 579
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-nez v0, :cond_0

    return-void

    .line 583
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setSelectMode(Z)V

    .line 585
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->checkToolbarButtons()V

    return-void

    .line 589
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity$1;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    return-void
.end method

.method private onZwdSwitch()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "queryZwd"

    .line 382
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->cancel()V

    .line 388
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getZwdQueryType()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mUseOldZwdMethod:Z

    .line 389
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mUseOldZwdMethod:Z

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setZwdQueryType(I)V

    .line 390
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->queryZwd()V

    .line 391
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->finish()V

    return-void
.end method

.method private queryZwd()V
    .locals 6

    const-string v0, "QueryResultActivity"

    const-string v1, "queryZwd"

    .line 503
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 505
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const/16 v2, 0x2d

    .line 507
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_0

    .line 508
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x6

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    .line 509
    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v2, "ticket_date"

    .line 511
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ticket_type"

    const-string v2, "\u5168\u90e8"

    .line 512
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_normal"

    .line 513
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 515
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    .line 517
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "train_code"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "query_type"

    const-string v3, "query_type_train_zwd"

    .line 518
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "query_method_skbcx"

    .line 519
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    const-class v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 521
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private requestPassCode()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "requestPassCode"

    .line 700
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$9l7ii4U97UUpVYmWOTufBOmyesI;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$9l7ii4U97UUpVYmWOTufBOmyesI;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private setFilter(I)V
    .locals 1

    .line 405
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->setFilter(I)V

    .line 408
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$TEUjWRnh9zrJ0K-E3vbftg8cBhM;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$TEUjWRnh9zrJ0K-E3vbftg8cBhM;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private setPassImage(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 738
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setPassImage this"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QueryResultActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mProgBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz p1, :cond_0

    .line 742
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 743
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const-string p1, "1234"

    .line 746
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->queryZWD(Ljava/lang/String;)V

    .line 747
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    if-eqz p1, :cond_1

    .line 748
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    :cond_1
    :goto_0
    return-void
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private showDateDialog()V
    .locals 5

    const-string v0, "QueryResultActivity"

    const-string v1, "showDateDialog"

    .line 832
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 835
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->requestWindowFeature(I)Z

    .line 836
    new-instance v0, Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultMaxDays()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/lltskb/lltskb/view/CalendarView;-><init>(Landroid/content/Context;J)V

    .line 838
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v2, v0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    .line 839
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const-string v3, "\u5e74\u6708\u65e5"

    invoke-virtual {v2, v3}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 840
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v2}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_0

    const v3, 0x7f0e00bd

    .line 842
    invoke-virtual {v2, v3}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 845
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v2}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 849
    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 850
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 852
    :catch_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 854
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 855
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 856
    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/CalendarView;->setSelectedDate(III)V

    .line 858
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$HzFSwuk-gjxb0IqVMK63gjGRx90;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$HzFSwuk-gjxb0IqVMK63gjGRx90;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/CalendarView;->setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V

    return-void
.end method

.method private showResult()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "showResult"

    .line 919
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    if-nez v1, :cond_0

    goto :goto_1

    .line 923
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 924
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const v0, 0x7f090193

    .line 930
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v1, 0x8

    .line 932
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 934
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setVisibility(I)V

    .line 935
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 936
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$4yyh06HF88re9yof69VKwNeU77M;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$4yyh06HF88re9yof69VKwNeU77M;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 957
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$0Tyh_SrneGxXOfF-yn2uA4WVdsc;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$0Tyh_SrneGxXOfF-yn2uA4WVdsc;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 961
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBottombar:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 963
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLastScrollPos:I

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setSelection(I)V

    return-void

    .line 925
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setVisibility(I)V

    .line 926
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->postQueryError(Ljava/lang/String;)V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public btn_flight(Landroid/view/View;)V
    .locals 2

    .line 979
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_hotel(Landroid/view/View;)V
    .locals 1

    .line 985
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 986
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 988
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStation:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected hideProgress()V
    .locals 0

    .line 147
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    return-void
.end method

.method protected isProgressShown()Z
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLoadingLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$initView$0$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 287
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onSetSort()V

    return-void
.end method

.method public synthetic lambda$initView$1$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 290
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onSetFilter()V

    return-void
.end method

.method public synthetic lambda$initView$10$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 363
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$initView$11$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x0

    .line 369
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$initView$2$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 304
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->queryZwd()V

    return-void
.end method

.method public synthetic lambda$initView$3$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 309
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onZwdSwitch()V

    return-void
.end method

.method public synthetic lambda$initView$4$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 315
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onBook()V

    return-void
.end method

.method public synthetic lambda$initView$5$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 321
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onRetry()V

    return-void
.end method

.method public synthetic lambda$initView$6$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 330
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onShare()V

    return-void
.end method

.method public synthetic lambda$initView$7$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 336
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onBack()V

    return-void
.end method

.method public synthetic lambda$initView$8$QueryResultActivity(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 343
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 344
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->setSelect(IZ)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 347
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz p1, :cond_2

    .line 348
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public synthetic lambda$initView$9$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 355
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$null$14$QueryResultActivity()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "get pass code failed"

    .line 725
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mProgBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 727
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$null$15$QueryResultActivity(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 733
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setPassImage(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public synthetic lambda$null$21$QueryResultActivity(Ljava/lang/String;)V
    .locals 1

    .line 903
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->stopRefresh()V

    .line 904
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->stopLoadMore()V

    if-eqz p1, :cond_0

    .line 907
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->postQueryError(Ljava/lang/String;)V

    goto :goto_0

    .line 909
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showResult()V

    .line 911
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->hideProgress()V

    return-void
.end method

.method public synthetic lambda$null$22$QueryResultActivity()V
    .locals 3

    .line 900
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->doQuery()Ljava/lang/String;

    move-result-object v0

    .line 901
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 902
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$wL-KRhrtWW4_er8BMTil8f2hD18;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$wL-KRhrtWW4_er8BMTil8f2hD18;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$onSetFilter$12$QueryResultActivity(ILjava/lang/String;)V
    .locals 0

    .line 397
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setFilter(I)V

    .line 398
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mFilter:I

    return-void
.end method

.method public synthetic lambda$onSetSort$26$QueryResultActivity(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 v0, 0x1

    if-nez p2, :cond_0

    .line 1016
    sput v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_1

    :cond_0
    const/4 v1, 0x2

    if-ne p2, v0, :cond_1

    .line 1018
    sput v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_1

    :cond_1
    if-ne p2, v1, :cond_2

    const/16 p2, 0x20

    .line 1020
    sput p2, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_4

    .line 1022
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of p2, p2, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;

    if-eqz p2, :cond_3

    const/4 p2, 0x4

    goto :goto_0

    :cond_3
    const/16 p2, 0x8

    :goto_0
    sput p2, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    .line 1024
    :cond_4
    :goto_1
    iget p2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mFilter:I

    invoke-direct {p0, p2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setFilter(I)V

    .line 1025
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public synthetic lambda$query$23$QueryResultActivity()V
    .locals 3

    .line 887
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/TicketLeftQuery;

    const v2, -0xffff01

    if-nez v1, :cond_5

    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;

    if-eqz v1, :cond_0

    goto :goto_1

    .line 889
    :cond_0
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    if-nez v1, :cond_4

    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    if-eqz v1, :cond_1

    goto :goto_0

    .line 891
    :cond_1
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/StationQuery;

    if-eqz v1, :cond_2

    .line 892
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStation:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(Ljava/lang/String;I)V

    goto :goto_2

    .line 893
    :cond_2
    instance-of v0, v0, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;

    if-eqz v0, :cond_3

    .line 894
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(Ljava/lang/String;I)V

    goto :goto_2

    :cond_3
    const v0, 0x7f0d01a6

    .line 896
    invoke-virtual {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(II)V

    goto :goto_2

    .line 890
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(Ljava/lang/String;I)V

    goto :goto_2

    .line 888
    :cond_5
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(Ljava/lang/String;I)V

    .line 899
    :goto_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$KNqQ92WyV7w9DjTiF9fEw01MWIg;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$KNqQ92WyV7w9DjTiF9fEw01MWIg;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$requestPassCode$16$QueryResultActivity()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 705
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v2, v2, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    const/4 v3, 0x1

    const/4 v4, 0x3

    if-eqz v2, :cond_0

    .line 706
    new-instance v2, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;

    invoke-direct {v2, v4}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;-><init>(I)V

    .line 707
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    .line 709
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v2, v2, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    if-eqz v2, :cond_1

    goto :goto_1

    .line 714
    :cond_1
    new-instance v2, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;

    invoke-direct {v2, v4}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;-><init>(I)V

    .line 715
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 724
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$shyZ_A-ONBixuCHAlzVlb8eON58;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$shyZ_A-ONBixuCHAlzVlb8eON58;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 733
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$aD-qQj4wnf1u_mIdoto99LusEzA;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$aD-qQj4wnf1u_mIdoto99LusEzA;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$setFilter$13$QueryResultActivity()V
    .locals 2

    .line 409
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 410
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AjYZhmhT4wHE7RmI38v-paB2PDU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AjYZhmhT4wHE7RmI38v-paB2PDU;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$showDateDialog$20$QueryResultActivity(IIILjava/lang/String;)V
    .locals 2

    .line 860
    sget-object p4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    add-int/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, p1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    const-string p1, "%04d-%02d-%02d"

    invoke-static {p4, p1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    .line 866
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onRetry()V

    .line 867
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$showPassCodeDlg$17$QueryResultActivity(Landroid/view/View;)V
    .locals 1

    .line 783
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mProgBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 785
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mProgBar:Landroid/widget/ProgressBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 786
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 787
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->requestPassCode()V

    return-void
.end method

.method public synthetic lambda$showPassCodeDlg$18$QueryResultActivity(Landroid/view/View;)V
    .locals 1

    .line 801
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    .line 802
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassEdt:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 803
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 804
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->finish()V

    goto :goto_0

    .line 807
    :cond_0
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->queryZWD(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$showPassCodeDlg$19$QueryResultActivity(Landroid/view/View;)V
    .locals 0

    .line 815
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    .line 816
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$showResult$24$QueryResultActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 937
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-nez p1, :cond_0

    return-void

    :cond_0
    add-int/lit8 p3, p3, -0x1

    .line 940
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isShowFlight()Z

    move-result p1

    if-eqz p1, :cond_2

    if-nez p3, :cond_1

    const/4 p1, 0x0

    .line 942
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->btn_flight(Landroid/view/View;)V

    return-void

    :cond_1
    add-int/lit8 p3, p3, -0x1

    .line 947
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-nez p1, :cond_3

    return-void

    .line 950
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isSelectMode()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 951
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->isSelect(I)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p3, p2}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->setSelect(IZ)V

    .line 952
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->notifyDataSetChanged()V

    return-void

    .line 955
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->onItemClicked(I)V

    return-void
.end method

.method public synthetic lambda$showResult$25$QueryResultActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 0

    .line 958
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onShare()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onBack()V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "onBack"

    .line 482
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBottombar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setSelectMode(Z)V

    .line 485
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->notifyDataSetChanged()V

    .line 486
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mBottombar:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 487
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->checkToolbarButtons()V

    return-void

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_1

    .line 492
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->cancel()V

    .line 494
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCreate bundle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QueryResultActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 165
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->initView()V

    .line 167
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_2

    const/4 p1, 0x0

    .line 170
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 171
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getQueryMethod()I

    move-result v0

    if-nez v0, :cond_1

    .line 172
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showPassCodeDlg(Landroid/os/Bundle;)V

    goto :goto_0

    .line 174
    :cond_1
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->query(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 181
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const-string v2, "\u53d1\u9001\u7ed3\u679c"

    .line 108
    invoke-interface {p1, v0, v1, v0, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v1, 0x1

    const-string v2, "\u8fd4\u56de"

    .line 109
    invoke-interface {p1, v0, v1, v0, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 110
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 186
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onDestroy()V

    .line 187
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->cancel()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 475
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onBack()V

    const/4 p1, 0x1

    return p1

    .line 478
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLoadMore()V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 115
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onShare()V

    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onBack()V

    .line 123
    :goto_0
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onRefresh()V
    .locals 2

    .line 1035
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 1036
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->onRetry()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 665
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 666
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    if-eqz v0, :cond_0

    .line 667
    invoke-virtual {v0, p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->setActivity(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    if-eqz v0, :cond_1

    .line 671
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/XListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/SimpleAdapter;

    if-eqz v0, :cond_1

    .line 673
    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    :cond_1
    const-string v0, "selectmode"

    .line 677
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 678
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTimeView:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 679
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setSelectMode(Z)V

    .line 680
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTimeView:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 681
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mSelectAll:Landroid/widget/CheckBox;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 693
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    if-eqz v0, :cond_0

    .line 694
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "selectmode"

    .line 693
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 695
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected postQueryError(Ljava/lang/String;)V
    .locals 2

    .line 967
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "postQueryError ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QueryResultActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setVisibility(I)V

    const v0, 0x7f090193

    .line 970
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 971
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mErrorTxt:Landroid/widget/TextView;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 973
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 974
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mErrorTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected query(Ljava/lang/String;)V
    .locals 2

    const-string v0, "QueryResultActivity"

    const-string v1, "query"

    .line 883
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCode:Ljava/lang/String;

    .line 885
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XListView;->getFirstVisiblePosition()I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mLastScrollPos:I

    .line 886
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$d4AkmaQv7NhsdtIo_rnfISCmgoE;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$d4AkmaQv7NhsdtIo_rnfISCmgoE;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public queryZWD(Ljava/lang/String;)V
    .locals 2

    .line 753
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryZWD pass="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QueryResultActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    if-eqz v1, :cond_0

    .line 755
    check-cast v0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdType:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->queryZWD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 756
    :cond_0
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    if-eqz v1, :cond_1

    .line 757
    check-cast v0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mZwdType:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->queryZWD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 759
    :cond_1
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->query(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public showPassCodeDlg(Landroid/os/Bundle;)V
    .locals 2

    .line 764
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showPassCodeDlg saved state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "QueryResultActivity"

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    if-nez p1, :cond_6

    .line 767
    new-instance p1, Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0e0003

    invoke-direct {p1, p0, v0}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 768
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_0

    const v0, 0x7f0e00bd

    .line 770
    invoke-virtual {p1, v0}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 773
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0b0076

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(I)V

    const p1, 0x7f090193

    .line 775
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    .line 776
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 778
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0901a6

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    .line 779
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0901a5

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassEdt:Landroid/widget/EditText;

    .line 780
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0901ac

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mProgBar:Landroid/widget/ProgressBar;

    .line 782
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassImage:Landroid/widget/ImageView;

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$lrNBi0uiY8RkaWnz6pYmGsM9VKA;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$lrNBi0uiY8RkaWnz6pYmGsM9VKA;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 790
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    if-nez v0, :cond_1

    instance-of p1, p1, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    if-eqz p1, :cond_2

    .line 791
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassEdt:Landroid/widget/EditText;

    const/4 v0, 0x0

    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v0, v1}, Landroid/text/method/QwertyKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/QwertyKeyListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 794
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0902f6

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_3

    const v0, 0x7f0d020a

    .line 796
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 798
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f0902ae

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 800
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$dJDXhnmxpushlakjFK9c6Jx58ys;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$dJDXhnmxpushlakjFK9c6Jx58ys;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 812
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    const v0, 0x7f090261

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 814
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AKhmXI3mYvJw57teR7mFsPPXJXg;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$QueryResultActivity$AKhmXI3mYvJw57teR7mFsPPXJXg;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 821
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 822
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 825
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassEdt:Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 827
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mPassCodeDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 828
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->requestPassCode()V

    return-void
.end method

.method protected showProgress(II)V
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->showProgress(Ljava/lang/String;I)V

    return-void
.end method

.method protected showProgress(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 137
    invoke-static {p0, p1, p2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
