.class public abstract Lcom/lltskb/lltskb/engine/online/BaseQuery;
.super Ljava/lang/Object;
.source "BaseQuery.java"


# static fields
.field public static final QUERY_NORMAL:I = 0x0

.field public static final QUERY_SKBCX:I = 0x1

.field public static final QUERY_TRAINNO:I = 0x2


# instance fields
.field protected mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

.field protected mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

.field protected mFilter:I

.field private mQueryMethod:I


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xff

    .line 23
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mFilter:I

    .line 26
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    .line 27
    iput p2, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mQueryMethod:I

    .line 28
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->getFilterType()I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mFilter:I

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public abstract doQuery()Ljava/lang/String;
.end method

.method public abstract getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
.end method

.method public getCount()I
    .locals 2

    .line 81
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    :cond_1
    return v1
.end method

.method public abstract getDisplayResult()Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation
.end method

.method public getQueryMethod()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mQueryMethod:I

    return v0
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method isNotMatchFilter(Ljava/lang/String;)Z
    .locals 1

    if-eqz p1, :cond_1

    .line 101
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mFilter:I

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->isMatchFilter(ILjava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public isSelect(I)Z
    .locals 3

    .line 68
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 70
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-le v2, p1, :cond_3

    if-gez p1, :cond_1

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 73
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;

    if-eqz v0, :cond_2

    .line 74
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;

    iget-boolean p1, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;->isSelected:Z

    return p1

    .line 76
    :cond_2
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;

    iget-boolean p1, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;->isSelected:Z

    return p1

    :cond_3
    :goto_0
    return v1
.end method

.method public abstract onItemClicked(I)V
.end method

.method public onOrderClicked(I)V
    .locals 0

    return-void
.end method

.method public setActivity(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    return-void
.end method

.method public setFilter(I)V
    .locals 0

    .line 95
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/BaseQuery;->mFilter:I

    return-void
.end method

.method public setSelect(IZ)V
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->getDisplayResult()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 49
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 52
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;

    if-eqz v0, :cond_2

    .line 53
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;

    iput-boolean p2, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseInfo;->isSelected:Z

    goto :goto_0

    .line 55
    :cond_2
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;

    iput-boolean p2, p1, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;->isSelected:Z

    :cond_3
    :goto_0
    return-void
.end method
