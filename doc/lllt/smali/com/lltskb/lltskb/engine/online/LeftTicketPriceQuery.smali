.class Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "LeftTicketPriceQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LeftTicketPriceQuery"


# instance fields
.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mTicketPriceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 29
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method private getPrice(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    if-eqz p1, :cond_3

    const-string v0, "--"

    .line 133
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_1

    .line 135
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x30

    if-ne v0, v3, :cond_1

    .line 136
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 141
    :cond_1
    :try_start_0
    invoke-static {p1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    .line 142
    rem-int/lit8 v3, v0, 0xa

    if-nez v3, :cond_2

    .line 143
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    new-array v1, v1, [Ljava/lang/Object;

    div-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 145
    :cond_2
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d.5"

    new-array v1, v1, [Ljava/lang/Object;

    div-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v0

    .line 149
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_1
    return-object p1
.end method

.method private parseQueryResult(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "data"

    .line 156
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 161
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 164
    :cond_0
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "message"

    if-eqz v2, :cond_3

    .line 165
    :try_start_1
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 166
    instance-of v2, v2, Lorg/json/JSONArray;

    if-eqz v2, :cond_2

    .line 167
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 181
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    .line 182
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 183
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;-><init>()V

    .line 184
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "queryLeftNewDTO"

    .line 185
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "train_no"

    .line 187
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->train_no:Ljava/lang/String;

    const-string v6, "station_train_code"

    .line 188
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    const-string v6, "start_station_telecode"

    .line 189
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_station_telecode:Ljava/lang/String;

    const-string v6, "start_station_name"

    .line 190
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_station_name:Ljava/lang/String;

    const-string v6, "end_station_telecode"

    .line 191
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->end_station_telecode:Ljava/lang/String;

    const-string v6, "end_station_name"

    .line 192
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->end_station_name:Ljava/lang/String;

    const-string v6, "from_station_telecode"

    .line 193
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_telecode:Ljava/lang/String;

    const-string v6, "from_station_name"

    .line 194
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_name:Ljava/lang/String;

    const-string v6, "to_station_telecode"

    .line 195
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_telecode:Ljava/lang/String;

    const-string v6, "to_station_name"

    .line 196
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_name:Ljava/lang/String;

    const-string v6, "start_time"

    .line 197
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_time:Ljava/lang/String;

    const-string v6, "arrive_time"

    .line 198
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->arrive_time:Ljava/lang/String;

    const-string v6, "day_difference"

    .line 199
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->day_difference:Ljava/lang/String;

    const-string v6, "train_class_name"

    .line 200
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->train_class_name:Ljava/lang/String;

    const-string v6, "lishi"

    .line 201
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->lishi:Ljava/lang/String;

    const-string v6, "yp_info"

    .line 204
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yp_info:Ljava/lang/String;

    const-string v6, "control_train_day"

    .line 205
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->control_train_day:Ljava/lang/String;

    const-string v6, "start_train_date"

    .line 206
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_train_date:Ljava/lang/String;

    const-string v6, "seat_feature"

    .line 207
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->seat_feature:Ljava/lang/String;

    const-string v6, "yp_ex"

    .line 208
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yp_ex:Ljava/lang/String;

    const-string v6, "train_seat_feature"

    .line 209
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->train_seat_feature:Ljava/lang/String;

    const-string v6, "seat_types"

    .line 210
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->seat_types:Ljava/lang/String;

    const-string v6, "location_code"

    .line 211
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->location_code:Ljava/lang/String;

    const-string v6, "from_station_no"

    .line 212
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_no:Ljava/lang/String;

    const-string v6, "to_station_no"

    .line 213
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_no:Ljava/lang/String;

    const-string v6, "control_day"

    .line 214
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->control_day:Ljava/lang/String;

    const-string v6, "sale_time"

    .line 215
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->sale_time:Ljava/lang/String;

    const-string v6, "is_support_card"

    .line 216
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->is_support_card:Ljava/lang/String;

    const-string v6, "controlled_train_flag"

    .line 217
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->controlled_train_flag:Ljava/lang/String;

    const-string v6, "controlled_train_message"

    .line 218
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->controlled_train_message:Ljava/lang/String;

    const-string v6, "note"

    .line 219
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->note:Ljava/lang/String;

    const-string v6, "gg_num"

    .line 221
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->gg_num:Ljava/lang/String;

    const-string v6, "gr_num"

    .line 222
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->gr_num:Ljava/lang/String;

    const-string v6, "qt_num"

    .line 223
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->qt_num:Ljava/lang/String;

    const-string v6, "rw_num"

    .line 224
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rw_num:Ljava/lang/String;

    const-string v6, "rz_num"

    .line 225
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rz_num:Ljava/lang/String;

    const-string v6, "tz_num"

    .line 226
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->tz_num:Ljava/lang/String;

    const-string v6, "wz_num"

    .line 227
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->wz_num:Ljava/lang/String;

    const-string v6, "yb_num"

    .line 228
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yb_num:Ljava/lang/String;

    const-string v6, "yw_num"

    .line 229
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yw_num:Ljava/lang/String;

    const-string v6, "yz_num"

    .line 230
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yz_num:Ljava/lang/String;

    const-string v6, "ze_num"

    .line 231
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->ze_num:Ljava/lang/String;

    const-string v6, "zy_num"

    .line 232
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->zy_num:Ljava/lang/String;

    const-string v6, "swz_num"

    .line 233
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->swz_num:Ljava/lang/String;

    const-string v6, "yz_price"

    .line 235
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yz_price:Ljava/lang/String;

    const-string v6, "rz_price"

    .line 236
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rz_price:Ljava/lang/String;

    const-string v6, "yw_price"

    .line 237
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yw_price:Ljava/lang/String;

    const-string v6, "rw_price"

    .line 238
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rw_price:Ljava/lang/String;

    const-string v6, "gr_price"

    .line 239
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->gr_price:Ljava/lang/String;

    const-string v6, "zy_price"

    .line 240
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->zy_price:Ljava/lang/String;

    const-string v6, "ze_price"

    .line 241
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->ze_price:Ljava/lang/String;

    const-string v6, "tz_price"

    .line 242
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->tz_price:Ljava/lang/String;

    const-string v6, "gg_price"

    .line 243
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->gg_price:Ljava/lang/String;

    const-string v6, "yb_price"

    .line 244
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yb_price:Ljava/lang/String;

    const-string v6, "swz_price"

    .line 245
    invoke-virtual {p0, v5, v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getPrice(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->swz_price:Ljava/lang/String;

    const-string v5, "secretStr"

    .line 248
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->secretStr:Ljava/lang/String;

    const-string v5, "buttonTextInfo"

    .line 249
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->buttonTextInfo:Ljava/lang/String;

    .line 251
    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    return-object v1

    .line 169
    :cond_2
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 170
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    new-instance v1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 174
    :cond_3
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 175
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    new-instance v1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 178
    :cond_4
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u672a\u77e5\u9519\u8bef"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ret="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "LeftTicketPriceQuery"

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 258
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method


# virtual methods
.method getmTicketPriceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mTicketPriceList:Ljava/util/List;

    return-object v0
.end method

.method public queryTicketPrice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 101
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "\u3011\u65e0\u6cd5\u627e\u5230"

    const-string v4, "\u8f66\u7ad9\u3010"

    if-nez v2, :cond_2

    .line 108
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 112
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "leftTicketDTO.train_date="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&leftTicketDTO.from_station="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&leftTicketDTO.to_station="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&purpose_codes="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&randCode="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 118
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryTicketPrice url="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "LeftTicketPriceQuery"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p4, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p4, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 122
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "QueryLeftTicket ret="

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->parseQueryResult(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 125
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_0

    .line 127
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 128
    :cond_0
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 109
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 105
    :cond_2
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method queryTicketPricePublic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 38
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "\u3011\u65e0\u6cd5\u627e\u5230"

    const-string v4, "\u8f66\u7ad9\u3010"

    if-nez v2, :cond_4

    .line 45
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 52
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mTicketPriceList:Ljava/util/List;

    .line 53
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mTicketPriceList:Ljava/util/List;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 54
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 55
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mTicketPriceList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    .line 56
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;-><init>()V

    .line 57
    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getSTCODE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    .line 58
    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getATIME()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->arrive_time:Ljava/lang/String;

    .line 59
    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getSTIME()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_time:Ljava/lang/String;

    .line 60
    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getLISHI()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result v1

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v1, 0x3c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    rem-int/lit8 v1, v1, 0x3c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->lishi:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getEST()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->end_station_name:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getSST()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_station_name:Ljava/lang/String;

    .line 64
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getFST()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_name:Ljava/lang/String;

    .line 65
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getTST()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_name:Ljava/lang/String;

    .line 66
    invoke-virtual {p4}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getTRNO()Ljava/lang/String;

    move-result-object p4

    iput-object p4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->train_no:Ljava/lang/String;

    .line 67
    iput-object p3, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_train_date:Ljava/lang/String;

    .line 69
    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_0
    return-object p1

    .line 74
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "leftTicketDTO.train_date="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&leftTicketDTO.from_station="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&leftTicketDTO.to_station="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&purpose_codes="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 79
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryTicketPrice url="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "LeftTicketPriceQuery"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p4, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE_PUBLIC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p4, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 83
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "QueryLeftTicket ret="

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->parseQueryResult(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 86
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 87
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_2

    .line 88
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 89
    :cond_2
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 46
    :cond_3
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 42
    :cond_4
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :goto_1
    throw p2

    :goto_2
    goto :goto_1
.end method
