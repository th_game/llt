.class public Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;
.super Ljava/lang/Object;
.source "InitMy12306ApiDTO.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "InitMy12306ApiDTO"


# instance fields
.field public _email:Ljava/lang/String;

.field public _is_active:Ljava/lang/String;

.field public _is_needModifyPassword:Ljava/lang/String;

.field public id_type_code:Ljava/lang/String;

.field public if_show_ali_qr_code:Z

.field public isCanRegistMember:Z

.field public isSuperUser:Ljava/lang/String;

.field public member_level:Ljava/lang/String;

.field public member_status:Ljava/lang/String;

.field public needEdit:Z

.field public notify_TWO_2:Ljava/lang/String;

.field public notify_way:Ljava/lang/String;

.field public qr_code_url:Ljava/lang/String;

.field public resetMemberPwd:Ljava/lang/String;

.field public user_name:Ljava/lang/String;

.field public user_regard:Ljava/lang/String;

.field public user_status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;
    .locals 4

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "parseResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InitMy12306ApiDTO"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    return-object v2

    .line 44
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "data"

    .line 45
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v2

    .line 50
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;-><init>()V

    const-string v3, "_email"

    .line 51
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->_email:Ljava/lang/String;

    const-string v3, "_is_active"

    .line 52
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->_is_active:Ljava/lang/String;

    const-string v3, "_is_needModifyPassword"

    .line 53
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->_is_needModifyPassword:Ljava/lang/String;

    const-string v3, "id_type_code"

    .line 54
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->id_type_code:Ljava/lang/String;

    const-string v3, "if_show_ali_qr_code"

    .line 55
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->if_show_ali_qr_code:Z

    const-string v3, "isCanRegistMember"

    .line 56
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->isCanRegistMember:Z

    const-string v3, "isSuperUser"

    .line 57
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->isSuperUser:Ljava/lang/String;

    const-string v3, "member_level"

    .line 58
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->member_level:Ljava/lang/String;

    const-string v3, "member_status"

    .line 59
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->member_status:Ljava/lang/String;

    const-string v3, "needEdit"

    .line 60
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->needEdit:Z

    const-string v3, "notify_TWO_2"

    .line 61
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->notify_TWO_2:Ljava/lang/String;

    const-string v3, "notify_way"

    .line 62
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->notify_way:Ljava/lang/String;

    const-string v3, "qr_code_url"

    .line 63
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->qr_code_url:Ljava/lang/String;

    const-string v3, "resetMemberPwd"

    .line 64
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->resetMemberPwd:Ljava/lang/String;

    const-string v3, "user_name"

    .line 65
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->user_name:Ljava/lang/String;

    const-string v3, "user_regard"

    .line 66
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->user_regard:Ljava/lang/String;

    const-string v3, "user_status"

    .line 67
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->user_status:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    .line 70
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 71
    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method
