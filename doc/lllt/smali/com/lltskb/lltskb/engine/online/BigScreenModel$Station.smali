.class public Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;
.super Ljava/lang/Object;
.source "BigScreenModel.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/BigScreenModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Station"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;",
        ">;"
    }
.end annotation


# instance fields
.field public firstPY:Ljava/lang/String;

.field public isShowPY:Z

.field public pinyin:Ljava/lang/String;

.field public pinyinShort:Ljava/lang/String;

.field public presaleTime:Ljava/lang/String;

.field public source:Ljava/lang/String;

.field public stationCode:Ljava/lang/String;

.field public stationName:Ljava/lang/String;

.field public stationOrder:Ljava/lang/String;

.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/BigScreenModel;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/BigScreenModel;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->this$0:Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;)I
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->pinyin:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 43
    check-cast p1, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;->compareTo(Lcom/lltskb/lltskb/engine/online/BigScreenModel$Station;)I

    move-result p1

    return p1
.end method
