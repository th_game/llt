.class public Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "UserProfileActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UserProfileActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->signOut()V

    return-void
.end method

.method private refresh()V
    .locals 3

    const v0, 0x7f0d01a6

    const/high16 v1, -0x1000000

    const/4 v2, 0x0

    .line 69
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 71
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$c9JviM5tY3PB2tElJMjTmuN8n4g;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$c9JviM5tY3PB2tElJMjTmuN8n4g;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private showFailedDialog()V
    .locals 2

    .line 84
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$dUycZRBpeaf7Dv3sF6Q3jj_Wcxk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$dUycZRBpeaf7Dv3sF6Q3jj_Wcxk;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private showQueryResult(Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;)V
    .locals 2

    .line 91
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$r6nhWJMqrBuy40dr40BLregyL3U;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$r6nhWJMqrBuy40dr40BLregyL3U;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private signOut()V
    .locals 5

    const-string v0, "UserProfileActivity"

    const-string v1, "signOut"

    .line 178
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    .line 181
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreate$0$UserProfileActivity(Landroid/view/View;)V
    .locals 0

    .line 31
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$onCreate$1$UserProfileActivity(Landroid/view/View;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->refresh()V

    return-void
.end method

.method public synthetic lambda$onCreate$2$UserProfileActivity(Landroid/view/View;)V
    .locals 2

    .line 45
    new-instance p1, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$1;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    const v0, 0x7f0d0173

    const v1, 0x7f0d00b6

    invoke-static {p0, v0, v1, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;IILcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    return-void
.end method

.method public synthetic lambda$refresh$3$UserProfileActivity()V
    .locals 1

    .line 72
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->queryUserInfo()Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;

    move-result-object v0

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->showFailedDialog()V

    return-void

    .line 79
    :cond_0
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->showQueryResult(Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;)V

    return-void
.end method

.method public synthetic lambda$showFailedDialog$4$UserProfileActivity()V
    .locals 2

    .line 85
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const v0, 0x7f0d02de

    const v1, 0x7f0d0104

    .line 86
    invoke-static {p0, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method public synthetic lambda$showQueryResult$5$UserProfileActivity(Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;)V
    .locals 5

    .line 92
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const v0, 0x7f09026e

    .line 93
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->country_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f090263

    .line 98
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d01ca

    const v2, 0x7f0d01c9

    const-string v3, "Y"

    if-eqz v0, :cond_2

    .line 100
    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->isMobileCheck:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 103
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    :goto_0
    const v0, 0x7f0902bd

    .line 107
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 109
    iget-object v4, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->isMobileCheck:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 110
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 112
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_4
    :goto_1
    const v0, 0x7f09025b

    .line 116
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 118
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->bornDateString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const v0, 0x7f090303

    .line 121
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 123
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_6
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    if-eqz v0, :cond_e

    const v0, 0x7f0902be

    .line 127
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 129
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    const v0, 0x7f09027d

    .line 132
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 134
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    const v0, 0x7f09027c

    .line 137
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->is_active:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x7f0d003a

    .line 139
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_9
    const v1, 0x7f0d0039

    .line 141
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    const v0, 0x7f0902e0

    .line 144
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 145
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->sex_code:Ljava/lang/String;

    const-string v2, "M"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x7f0d01c2

    .line 146
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_a
    const v1, 0x7f0d0119

    .line 148
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 151
    :goto_3
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    if-eqz v0, :cond_e

    const v0, 0x7f090248

    .line 152
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 154
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->user_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    const v0, 0x7f0902aa

    .line 156
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_c

    .line 158
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    const v0, 0x7f090292

    .line 161
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    .line 163
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->id_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    const v0, 0x7f090290

    .line 166
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_e

    .line 168
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->id_type_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_e
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 27
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0b0099

    .line 28
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->setContentView(I)V

    const p1, 0x7f0900fb

    .line 29
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 31
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$siAA9ASBkomuIa9B-q9s6n82JTc;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$siAA9ASBkomuIa9B-q9s6n82JTc;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const p1, 0x7f090067

    .line 34
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 36
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 37
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$aWa3I8uzsGU6on5_yvXf0mBIXnI;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$aWa3I8uzsGU6on5_yvXf0mBIXnI;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const p1, 0x7f090070

    .line 42
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 44
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$OJU3mNw1H-AyHwn6Adheasj2JAM;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$UserProfileActivity$OJU3mNw1H-AyHwn6Adheasj2JAM;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const p1, 0x7f090223

    .line 61
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_3

    const v0, 0x7f0d02d0

    .line 63
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 65
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->refresh()V

    return-void
.end method
