.class public final Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;
.super Ljava/lang/Object;
.source "TicketPriceDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008|\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u00b1\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010 \u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010!J\u000b\u0010`\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010h\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010i\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010j\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010k\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010l\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010m\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010o\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010p\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010r\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010s\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010u\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010v\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010w\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010x\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010y\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010z\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010{\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010|\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010}\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00f1\u0002\u0010~\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0015\u0010\u007f\u001a\u00030\u0080\u00012\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u000b\u0010\u0082\u0001\u001a\u00030\u0083\u0001H\u00d6\u0001J\n\u0010\u0084\u0001\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010#\"\u0004\u0008$\u0010%R\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008&\u0010#\"\u0004\u0008\'\u0010%R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010#\"\u0004\u0008)\u0010%R\u001c\u0010\u0008\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008*\u0010#\"\u0004\u0008+\u0010%R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008,\u0010#\"\u0004\u0008-\u0010%R\u001c\u0010\r\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010#\"\u0004\u0008/\u0010%R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00080\u0010#\"\u0004\u00081\u0010%R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00082\u0010#\"\u0004\u00083\u0010%R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00084\u0010#\"\u0004\u00085\u0010%R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00086\u0010#\"\u0004\u00087\u0010%R\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00088\u0010#\"\u0004\u00089\u0010%R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008:\u0010#\"\u0004\u0008;\u0010%R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008<\u0010#\"\u0004\u0008=\u0010%R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008>\u0010#\"\u0004\u0008?\u0010%R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008@\u0010#\"\u0004\u0008A\u0010%R\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008B\u0010#\"\u0004\u0008C\u0010%R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008D\u0010#\"\u0004\u0008E\u0010%R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008F\u0010#\"\u0004\u0008G\u0010%R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008H\u0010#\"\u0004\u0008I\u0010%R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008J\u0010#\"\u0004\u0008K\u0010%R\u001c\u0010\u001f\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008L\u0010#\"\u0004\u0008M\u0010%R\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008N\u0010#\"\u0004\u0008O\u0010%R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008P\u0010#\"\u0004\u0008Q\u0010%R\u001c\u0010 \u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008R\u0010#\"\u0004\u0008S\u0010%R\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008T\u0010#\"\u0004\u0008U\u0010%R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008V\u0010#\"\u0004\u0008W\u0010%R\u001c\u0010\u000c\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008X\u0010#\"\u0004\u0008Y\u0010%R\u001c\u0010\t\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008Z\u0010#\"\u0004\u0008[\u0010%R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\\\u0010#\"\u0004\u0008]\u0010%R\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008^\u0010#\"\u0004\u0008_\u0010%\u00a8\u0006\u0085\u0001"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;",
        "",
        "DAYD",
        "",
        "STIME",
        "TRNO",
        "DESPRI5",
        "DESPRI2",
        "DESPRI1",
        "TIC4",
        "AC",
        "DESPRI4",
        "TIC3",
        "DESPRI3",
        "TIC2",
        "TIC1",
        "FST",
        "LISHI",
        "DESTIC4",
        "EST",
        "DESTIC3",
        "PRI4",
        "DESTIC2",
        "PRI3",
        "DESTIC1",
        "PRI5",
        "PRI2",
        "PRI1",
        "STCODE",
        "TST",
        "ATIME",
        "SST",
        "TCCODE",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getAC",
        "()Ljava/lang/String;",
        "setAC",
        "(Ljava/lang/String;)V",
        "getATIME",
        "setATIME",
        "getDAYD",
        "setDAYD",
        "getDESPRI1",
        "setDESPRI1",
        "getDESPRI2",
        "setDESPRI2",
        "getDESPRI3",
        "setDESPRI3",
        "getDESPRI4",
        "setDESPRI4",
        "getDESPRI5",
        "setDESPRI5",
        "getDESTIC1",
        "setDESTIC1",
        "getDESTIC2",
        "setDESTIC2",
        "getDESTIC3",
        "setDESTIC3",
        "getDESTIC4",
        "setDESTIC4",
        "getEST",
        "setEST",
        "getFST",
        "setFST",
        "getLISHI",
        "setLISHI",
        "getPRI1",
        "setPRI1",
        "getPRI2",
        "setPRI2",
        "getPRI3",
        "setPRI3",
        "getPRI4",
        "setPRI4",
        "getPRI5",
        "setPRI5",
        "getSST",
        "setSST",
        "getSTCODE",
        "setSTCODE",
        "getSTIME",
        "setSTIME",
        "getTCCODE",
        "setTCCODE",
        "getTIC1",
        "setTIC1",
        "getTIC2",
        "setTIC2",
        "getTIC3",
        "setTIC3",
        "getTIC4",
        "setTIC4",
        "getTRNO",
        "setTRNO",
        "getTST",
        "setTST",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component19",
        "component2",
        "component20",
        "component21",
        "component22",
        "component23",
        "component24",
        "component25",
        "component26",
        "component27",
        "component28",
        "component29",
        "component3",
        "component30",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private AC:Ljava/lang/String;

.field private ATIME:Ljava/lang/String;

.field private DAYD:Ljava/lang/String;

.field private DESPRI1:Ljava/lang/String;

.field private DESPRI2:Ljava/lang/String;

.field private DESPRI3:Ljava/lang/String;

.field private DESPRI4:Ljava/lang/String;

.field private DESPRI5:Ljava/lang/String;

.field private DESTIC1:Ljava/lang/String;

.field private DESTIC2:Ljava/lang/String;

.field private DESTIC3:Ljava/lang/String;

.field private DESTIC4:Ljava/lang/String;

.field private EST:Ljava/lang/String;

.field private FST:Ljava/lang/String;

.field private LISHI:Ljava/lang/String;

.field private PRI1:Ljava/lang/String;

.field private PRI2:Ljava/lang/String;

.field private PRI3:Ljava/lang/String;

.field private PRI4:Ljava/lang/String;

.field private PRI5:Ljava/lang/String;

.field private SST:Ljava/lang/String;

.field private STCODE:Ljava/lang/String;

.field private STIME:Ljava/lang/String;

.field private TCCODE:Ljava/lang/String;

.field private TIC1:Ljava/lang/String;

.field private TIC2:Ljava/lang/String;

.field private TIC3:Ljava/lang/String;

.field private TIC4:Ljava/lang/String;

.field private TRNO:Ljava/lang/String;

.field private TST:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    move-object v0, p0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    move-object v1, p2

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    move-object v1, p3

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    move-object v1, p4

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    move-object v1, p5

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    move-object v1, p6

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    move-object v1, p7

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    move-object v1, p8

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    move-object v1, p9

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    move-object v1, p10

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    move-object v1, p12

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    move-object v1, p13

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    move-object/from16 v1, p16

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    move-object/from16 v1, p20

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    move-object/from16 v1, p22

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    move-object/from16 v1, p24

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    move-object/from16 v1, p25

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    move-object/from16 v1, p26

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    move-object/from16 v1, p27

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    move-object/from16 v1, p28

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    move-object/from16 v1, p29

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    move-object/from16 v1, p30

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p31

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p15, v15

    if-eqz v16, :cond_f

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    goto :goto_f

    :cond_f
    move-object/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move-object/from16 p16, v15

    if-eqz v16, :cond_10

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    goto :goto_10

    :cond_10
    move-object/from16 v15, p17

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move-object/from16 p17, v15

    if-eqz v16, :cond_11

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    goto :goto_11

    :cond_11
    move-object/from16 v15, p18

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move-object/from16 p18, v15

    if-eqz v16, :cond_12

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    goto :goto_12

    :cond_12
    move-object/from16 v15, p19

    :goto_12
    const/high16 v16, 0x80000

    and-int v16, v1, v16

    move-object/from16 p19, v15

    if-eqz v16, :cond_13

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    goto :goto_13

    :cond_13
    move-object/from16 v15, p20

    :goto_13
    const/high16 v16, 0x100000

    and-int v16, v1, v16

    move-object/from16 p20, v15

    if-eqz v16, :cond_14

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    goto :goto_14

    :cond_14
    move-object/from16 v15, p21

    :goto_14
    const/high16 v16, 0x200000

    and-int v16, v1, v16

    move-object/from16 p21, v15

    if-eqz v16, :cond_15

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    goto :goto_15

    :cond_15
    move-object/from16 v15, p22

    :goto_15
    const/high16 v16, 0x400000

    and-int v16, v1, v16

    move-object/from16 p22, v15

    if-eqz v16, :cond_16

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    goto :goto_16

    :cond_16
    move-object/from16 v15, p23

    :goto_16
    const/high16 v16, 0x800000

    and-int v16, v1, v16

    move-object/from16 p23, v15

    if-eqz v16, :cond_17

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    goto :goto_17

    :cond_17
    move-object/from16 v15, p24

    :goto_17
    const/high16 v16, 0x1000000

    and-int v16, v1, v16

    move-object/from16 p24, v15

    if-eqz v16, :cond_18

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    goto :goto_18

    :cond_18
    move-object/from16 v15, p25

    :goto_18
    const/high16 v16, 0x2000000

    and-int v16, v1, v16

    move-object/from16 p25, v15

    if-eqz v16, :cond_19

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    goto :goto_19

    :cond_19
    move-object/from16 v15, p26

    :goto_19
    const/high16 v16, 0x4000000

    and-int v16, v1, v16

    move-object/from16 p26, v15

    if-eqz v16, :cond_1a

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    goto :goto_1a

    :cond_1a
    move-object/from16 v15, p27

    :goto_1a
    const/high16 v16, 0x8000000

    and-int v16, v1, v16

    move-object/from16 p27, v15

    if-eqz v16, :cond_1b

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    goto :goto_1b

    :cond_1b
    move-object/from16 v15, p28

    :goto_1b
    const/high16 v16, 0x10000000

    and-int v16, v1, v16

    move-object/from16 p28, v15

    if-eqz v16, :cond_1c

    iget-object v15, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    goto :goto_1c

    :cond_1c
    move-object/from16 v15, p29

    :goto_1c
    const/high16 v16, 0x20000000

    and-int v1, v1, v16

    if-eqz v1, :cond_1d

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    goto :goto_1d

    :cond_1d
    move-object/from16 v1, p30

    :goto_1d
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p29, v15

    move-object/from16 p30, v1

    invoke-virtual/range {p0 .. p30}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    return-object v0
.end method

.method public final component11()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    return-object v0
.end method

.method public final component12()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    return-object v0
.end method

.method public final component13()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    return-object v0
.end method

.method public final component14()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    return-object v0
.end method

.method public final component15()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    return-object v0
.end method

.method public final component16()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    return-object v0
.end method

.method public final component17()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    return-object v0
.end method

.method public final component18()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    return-object v0
.end method

.method public final component19()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    return-object v0
.end method

.method public final component20()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    return-object v0
.end method

.method public final component21()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    return-object v0
.end method

.method public final component22()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    return-object v0
.end method

.method public final component23()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    return-object v0
.end method

.method public final component24()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    return-object v0
.end method

.method public final component25()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    return-object v0
.end method

.method public final component26()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    return-object v0
.end method

.method public final component27()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    return-object v0
.end method

.method public final component28()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    return-object v0
.end method

.method public final component29()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    return-object v0
.end method

.method public final component30()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    return-object v0
.end method

.method public final component9()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;
    .locals 32

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move-object/from16 v23, p23

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    new-instance v31, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    move-object/from16 v0, v31

    invoke-direct/range {v0 .. v30}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v31
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAC()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    return-object v0
.end method

.method public final getATIME()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    return-object v0
.end method

.method public final getDAYD()Ljava/lang/String;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESPRI1()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESPRI2()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESPRI3()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESPRI4()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESPRI5()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESTIC1()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESTIC2()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESTIC3()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    return-object v0
.end method

.method public final getDESTIC4()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    return-object v0
.end method

.method public final getEST()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    return-object v0
.end method

.method public final getFST()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    return-object v0
.end method

.method public final getLISHI()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    return-object v0
.end method

.method public final getPRI1()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    return-object v0
.end method

.method public final getPRI2()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    return-object v0
.end method

.method public final getPRI3()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    return-object v0
.end method

.method public final getPRI4()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    return-object v0
.end method

.method public final getPRI5()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    return-object v0
.end method

.method public final getSST()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    return-object v0
.end method

.method public final getSTCODE()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    return-object v0
.end method

.method public final getSTIME()Ljava/lang/String;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    return-object v0
.end method

.method public final getTCCODE()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    return-object v0
.end method

.method public final getTIC1()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    return-object v0
.end method

.method public final getTIC2()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    return-object v0
.end method

.method public final getTIC3()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    return-object v0
.end method

.method public final getTIC4()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    return-object v0
.end method

.method public final getTRNO()Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    return-object v0
.end method

.method public final getTST()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_b
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_c
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_d
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_e

    :cond_e
    const/4 v2, 0x0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_f
    const/4 v2, 0x0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_10

    :cond_10
    const/4 v2, 0x0

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_11

    :cond_11
    const/4 v2, 0x0

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    if-eqz v2, :cond_12

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_12

    :cond_12
    const/4 v2, 0x0

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    if-eqz v2, :cond_13

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_13

    :cond_13
    const/4 v2, 0x0

    :goto_13
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_14

    :cond_14
    const/4 v2, 0x0

    :goto_14
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    if-eqz v2, :cond_15

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_15

    :cond_15
    const/4 v2, 0x0

    :goto_15
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    if-eqz v2, :cond_16

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_16

    :cond_16
    const/4 v2, 0x0

    :goto_16
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    if-eqz v2, :cond_17

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_17

    :cond_17
    const/4 v2, 0x0

    :goto_17
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    if-eqz v2, :cond_18

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_18

    :cond_18
    const/4 v2, 0x0

    :goto_18
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    if-eqz v2, :cond_19

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_19

    :cond_19
    const/4 v2, 0x0

    :goto_19
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    if-eqz v2, :cond_1a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1a

    :cond_1a
    const/4 v2, 0x0

    :goto_1a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1b

    :cond_1b
    const/4 v2, 0x0

    :goto_1b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1c

    :cond_1c
    const/4 v2, 0x0

    :goto_1c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    if-eqz v2, :cond_1d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1d
    add-int/2addr v0, v1

    return v0
.end method

.method public final setAC(Ljava/lang/String;)V
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    return-void
.end method

.method public final setATIME(Ljava/lang/String;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    return-void
.end method

.method public final setDAYD(Ljava/lang/String;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    return-void
.end method

.method public final setDESPRI1(Ljava/lang/String;)V
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    return-void
.end method

.method public final setDESPRI2(Ljava/lang/String;)V
    .locals 0

    .line 8
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    return-void
.end method

.method public final setDESPRI3(Ljava/lang/String;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    return-void
.end method

.method public final setDESPRI4(Ljava/lang/String;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    return-void
.end method

.method public final setDESPRI5(Ljava/lang/String;)V
    .locals 0

    .line 7
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    return-void
.end method

.method public final setDESTIC1(Ljava/lang/String;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    return-void
.end method

.method public final setDESTIC2(Ljava/lang/String;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    return-void
.end method

.method public final setDESTIC3(Ljava/lang/String;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    return-void
.end method

.method public final setDESTIC4(Ljava/lang/String;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    return-void
.end method

.method public final setEST(Ljava/lang/String;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    return-void
.end method

.method public final setFST(Ljava/lang/String;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    return-void
.end method

.method public final setLISHI(Ljava/lang/String;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    return-void
.end method

.method public final setPRI1(Ljava/lang/String;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    return-void
.end method

.method public final setPRI2(Ljava/lang/String;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    return-void
.end method

.method public final setPRI3(Ljava/lang/String;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    return-void
.end method

.method public final setPRI4(Ljava/lang/String;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    return-void
.end method

.method public final setPRI5(Ljava/lang/String;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    return-void
.end method

.method public final setSST(Ljava/lang/String;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    return-void
.end method

.method public final setSTCODE(Ljava/lang/String;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    return-void
.end method

.method public final setSTIME(Ljava/lang/String;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    return-void
.end method

.method public final setTCCODE(Ljava/lang/String;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    return-void
.end method

.method public final setTIC1(Ljava/lang/String;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    return-void
.end method

.method public final setTIC2(Ljava/lang/String;)V
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    return-void
.end method

.method public final setTIC3(Ljava/lang/String;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    return-void
.end method

.method public final setTIC4(Ljava/lang/String;)V
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    return-void
.end method

.method public final setTRNO(Ljava/lang/String;)V
    .locals 0

    .line 6
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    return-void
.end method

.method public final setTST(Ljava/lang/String;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TicketPriceDTO(DAYD="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DAYD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", STIME="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STIME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TRNO="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TRNO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESPRI5="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESPRI2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESPRI1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TIC4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", AC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->AC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESPRI4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TIC3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESPRI3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESPRI3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TIC2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TIC1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TIC1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", FST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->FST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", LISHI="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->LISHI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESTIC4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", EST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->EST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESTIC3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", PRI4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESTIC2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", PRI3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", DESTIC1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->DESTIC1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", PRI5="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", PRI2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", PRI1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->PRI1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", STCODE="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->STCODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ATIME="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->ATIME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", SST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->SST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", TCCODE="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->TCCODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
