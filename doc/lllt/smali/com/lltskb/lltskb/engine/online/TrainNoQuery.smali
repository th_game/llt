.class public Lcom/lltskb/lltskb/engine/online/TrainNoQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "TrainNoQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TrainNoQuery"


# instance fields
.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 28
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method private parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    .locals 13

    const-string v0, "isEnabled"

    const-string v1, "station_no"

    const-string v2, "stopover_time"

    const-string v3, "start_time"

    const-string v4, "station_name"

    const-string v5, "arrive_time"

    const-string v6, "data"

    .line 73
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseResult="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "TrainNoQuery"

    invoke-static {v8, v7}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    if-eqz p1, :cond_6

    .line 74
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x6

    if-ge v9, v10, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v9, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    .line 76
    invoke-virtual {p1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    return-object v7

    .line 81
    :cond_1
    :try_start_0
    new-instance v9, Lorg/json/JSONTokener;

    invoke-direct {v9, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v9}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 85
    instance-of v9, p1, Lorg/json/JSONObject;

    if-nez v9, :cond_2

    goto/16 :goto_1

    .line 89
    :cond_2
    check-cast p1, Lorg/json/JSONObject;

    const-string v9, "httpstatus"

    .line 91
    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    const/16 v10, 0xc8

    if-eq v9, v10, :cond_3

    return-object v7

    .line 94
    :cond_3
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 95
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    .line 97
    new-instance v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    invoke-direct {v6}, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;-><init>()V

    const/4 v9, 0x0

    .line 99
    invoke-virtual {p1, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "start_station_name"

    .line 100
    invoke-virtual {p0, v10, v11}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_station_name:Ljava/lang/String;

    .line 101
    invoke-virtual {p0, v10, v5}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->arrive_time:Ljava/lang/String;

    const-string v11, "station_train_code"

    .line 102
    invoke-virtual {p0, v10, v11}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_train_code:Ljava/lang/String;

    .line 103
    invoke-virtual {p0, v10, v4}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_name:Ljava/lang/String;

    const-string v11, "train_class_name"

    .line 104
    invoke-virtual {p0, v10, v11}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->train_class_name:Ljava/lang/String;

    const-string v11, "service_type"

    .line 105
    invoke-virtual {p0, v10, v11}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->service_type:Ljava/lang/String;

    .line 106
    invoke-virtual {p0, v10, v3}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_time:Ljava/lang/String;

    .line 107
    invoke-virtual {p0, v10, v2}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->stopover_time:Ljava/lang/String;

    const-string v11, "end_station_name"

    .line 108
    invoke-virtual {p0, v10, v11}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->end_station_name:Ljava/lang/String;

    .line 109
    invoke-virtual {p0, v10, v1}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_no:Ljava/lang/String;

    .line 110
    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    iput-boolean v10, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->isEnabled:Z

    .line 111
    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    iput-object v10, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_list:Ljava/util/Vector;

    .line 113
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v9, v10, :cond_4

    .line 114
    invoke-virtual {p1, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 115
    new-instance v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v11, v6}, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;-><init>(Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)V

    .line 116
    invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->arrive_time:Ljava/lang/String;

    .line 117
    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    .line 118
    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->start_time:Ljava/lang/String;

    .line 119
    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->stopover_time:Ljava/lang/String;

    .line 120
    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_no:Ljava/lang/String;

    .line 121
    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    iput-boolean v10, v11, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->isEnabled:Z

    .line 123
    iget-object v10, v6, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_list:Ljava/util/Vector;

    invoke-virtual {v10, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_4
    return-object v6

    :cond_5
    :goto_1
    return-object v7

    :catch_0
    move-exception p1

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_6
    :goto_2
    return-object v7
.end method


# virtual methods
.method public queryByTrainNo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "train_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&from_station_telecode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&to_station_telecode="

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&depart_date="

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 43
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "queryByTrainNo url="

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "TrainNoQuery"

    invoke-static {v0, p3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p3, 0x3

    :goto_0
    if-lez p3, :cond_2

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_BY_TRAIN_NO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, p2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queryByTrainNo ret ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 52
    iput-object p1, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->train_no:Ljava/lang/String;

    .line 53
    iput-object p4, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_train_date:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_0
    add-int/lit8 p3, p3, -0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 63
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 64
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 65
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 66
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method
