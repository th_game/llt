.class public Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;
.super Ljava/lang/Object;
.source "QueryTrainInfoDTO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryTrainInfoDTO"


# instance fields
.field data:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertToResultItemList(Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 120
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    const v4, -0xe7652a

    .line 121
    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setBackColor(I)V

    .line 122
    sget v4, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    const/4 v4, 0x0

    const-string v5, "  \u8f66\u7ad9\u540d\u79f0  "

    .line 124
    invoke-virtual {v1, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v5, "     \u8f66\u6b21    "

    .line 125
    invoke-virtual {v1, v3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v5, 0x2

    const-string v6, "  \u5230\u70b9   "

    .line 126
    invoke-virtual {v1, v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v6, 0x3

    const-string v7, "  \u5f00\u70b9   "

    .line 127
    invoke-virtual {v1, v6, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v7, 0x4

    const-string v8, "  \u91cc\u7a0b   "

    .line 128
    invoke-virtual {v1, v7, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v8, 0x5

    const-string v9, "        \u68c0\u7968\u53e3         "

    .line 129
    invoke-virtual {v1, v8, v9}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v9, 0x6

    const-string v10, "   \u5929\u6570   "

    .line 130
    invoke-virtual {v1, v9, v10}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v10, 0x7

    const-string v11, "     \u901f\u5ea6     "

    .line 131
    invoke-virtual {v1, v10, v11}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 132
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p0, :cond_2

    .line 134
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    move-object v11, v1

    const/4 v1, 0x0

    .line 138
    :goto_0
    iget-object v12, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    invoke-virtual {v12}, Ljava/util/Vector;->size()I

    move-result v12

    if-ge v1, v12, :cond_2

    .line 139
    iget-object v12, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    invoke-virtual {v12, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 140
    invoke-static {v11}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 141
    iget-object v11, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->train_class_name:Ljava/lang/String;

    .line 143
    :cond_1
    new-instance v13, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-direct {v13, v2, v3, v4}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    const v14, -0x231108

    .line 144
    invoke-virtual {v13, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setBackColor(I)V

    .line 145
    sget v14, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v13, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 146
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v13, v4, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 147
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v13, v3, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 148
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v13, v5, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 149
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v13, v6, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 150
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_no:Ljava/lang/String;

    invoke-virtual {v13, v7, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 151
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_day_str:Ljava/lang/String;

    invoke-virtual {v13, v8, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 152
    iget-object v14, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_day_diff:Ljava/lang/String;

    invoke-virtual {v13, v9, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 153
    invoke-virtual {v13, v10, v11}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 154
    iget-object v12, v12, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v13, v12}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public static parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;
    .locals 9

    const-string v0, "data"

    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseResult = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "QueryTrainInfoDTO"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    return-object v3

    .line 67
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v3

    .line 73
    :cond_1
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p0

    if-eqz p0, :cond_7

    .line 74
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    goto/16 :goto_2

    .line 78
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;-><init>()V

    .line 81
    new-instance v1, Ljava/util/Vector;

    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/Vector;-><init>(I)V

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    const/4 v1, 0x0

    move-object v4, v3

    .line 82
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_6

    .line 83
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    if-nez v5, :cond_3

    goto/16 :goto_1

    .line 87
    :cond_3
    new-instance v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    invoke-direct {v6}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;-><init>()V

    const-string v7, "arrive_day_str"

    .line 88
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_day_str:Ljava/lang/String;

    const-string v7, "arrive_time"

    .line 89
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const-string v7, "station_train_code"

    .line 90
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_train_code:Ljava/lang/String;

    const-string v7, "station_name"

    .line 91
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    const-string v7, "arrive_day_diff"

    .line 92
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_day_diff:Ljava/lang/String;

    const-string v7, "start_time"

    .line 93
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    const-string v7, "wz_num"

    .line 94
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->wz_num:Ljava/lang/String;

    const-string v7, "station_no"

    .line 95
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_no:Ljava/lang/String;

    const-string v7, "running_time"

    .line 96
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    const-string v7, "is_start"

    .line 97
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->is_start:Ljava/lang/String;

    .line 98
    invoke-static {v4}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v8, "train_class_name"

    if-eqz v7, :cond_4

    .line 99
    :try_start_1
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 101
    :cond_4
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->train_class_name:Ljava/lang/String;

    .line 102
    iget-object v5, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->train_class_name:Ljava/lang/String;

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 103
    iput-object v4, v6, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->train_class_name:Ljava/lang/String;

    .line 105
    :cond_5
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    invoke-virtual {v5, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_6
    :goto_1
    return-object v0

    :cond_7
    :goto_2
    return-object v3

    :catch_0
    move-exception p0

    .line 110
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    invoke-virtual {p0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method


# virtual methods
.method public getData()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->data:Ljava/util/Vector;

    return-object v0
.end method
