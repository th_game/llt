.class public Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;
.super Ljava/lang/Object;
.source "QueryUserInfoDTO.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryUserInfoDTO"


# instance fields
.field public bornDateString:Ljava/lang/String;

.field public country_name:Ljava/lang/String;

.field public isMobileCheck:Ljava/lang/String;

.field public notice:Ljava/lang/String;

.field public userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

.field public userTypeName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseQueryUserInfo(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;
    .locals 4

    .line 34
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 39
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "data"

    .line 40
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v1

    .line 46
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;-><init>()V

    const-string v2, "bornDateString"

    .line 47
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->bornDateString:Ljava/lang/String;

    const-string v2, "country_name"

    .line 48
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->country_name:Ljava/lang/String;

    const-string v2, "isMobileCheck"

    .line 49
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->isMobileCheck:Ljava/lang/String;

    const-string v2, "notice"

    .line 50
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->notice:Ljava/lang/String;

    const-string v2, "userTypeName"

    .line 51
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userTypeName:Ljava/lang/String;

    const-string v2, "userDTO"

    .line 53
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 55
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;-><init>()V

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    .line 56
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "address"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->address:Ljava/lang/String;

    .line 57
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "email"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->email:Ljava/lang/String;

    .line 58
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "flag_member"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->flag_member:Ljava/lang/String;

    .line 59
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "is_active"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->is_active:Ljava/lang/String;

    .line 60
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "is_receive"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->is_receive:Ljava/lang/String;

    .line 61
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "is_valid"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->is_valid:Ljava/lang/String;

    .line 62
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "mobile_no"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->mobile_no:Ljava/lang/String;

    .line 63
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "needModifyEmail"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->needModifyEmail:Ljava/lang/String;

    .line 64
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "revSm_code"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->revSm_code:Ljava/lang/String;

    .line 65
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    const-string v3, "sex_code"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->sex_code:Ljava/lang/String;

    const-string v2, "loginUserDTO"

    .line 67
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 69
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;-><init>()V

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    .line 70
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "agent_contact"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->agent_contact:Ljava/lang/String;

    .line 71
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "id_no"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->id_no:Ljava/lang/String;

    .line 72
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "id_type_code"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->id_type_code:Ljava/lang/String;

    .line 73
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "id_type_name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->id_type_name:Ljava/lang/String;

    .line 74
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "member_level"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->member_level:Ljava/lang/String;

    .line 75
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->name:Ljava/lang/String;

    .line 76
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "user_name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->user_name:Ljava/lang/String;

    .line 77
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "user_type"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->user_type:Ljava/lang/String;

    .line 78
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->userDTO:Lcom/lltskb/lltskb/engine/online/dto/UserDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/UserDTO;->loginUserDTO:Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;

    const-string v3, "userIpAddress"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, Lcom/lltskb/lltskb/engine/online/dto/LoginUserDTO;->userIpAddress:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-object v0

    :catch_0
    move-exception p0

    .line 85
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 86
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string v0, "QueryUserInfoDTO"

    invoke-static {v0, p0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
