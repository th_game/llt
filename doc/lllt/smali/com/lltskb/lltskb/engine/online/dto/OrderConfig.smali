.class public Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
.super Ljava/lang/Object;
.source "OrderConfig.java"


# instance fields
.field private alertType:Ljava/lang/Integer;

.field private chooseSeats:Ljava/lang/String;

.field private fromStation:Ljava/lang/String;

.field private fromStationName:Ljava/lang/String;

.field private mIsIgnoreNetworkErr:Z

.field private mQueryFreq:Ljava/lang/Integer;

.field private orderDate:Ljava/lang/String;

.field private orderPerson:Ljava/lang/String;

.field private orderSeat:Ljava/lang/String;

.field private orderTime:Ljava/lang/String;

.field private purpose:Ljava/lang/String;

.field private rangeCode:Ljava/lang/String;

.field private searchSleepTime:Ljava/lang/Integer;

.field private searchWatiTime:Ljava/lang/Integer;

.field private toStation:Ljava/lang/String;

.field private toStationName:Ljava/lang/String;

.field private tour_flag:Ljava/lang/String;

.field private trainClass:[Ljava/lang/String;

.field private trainCode:Ljava/lang/String;

.field private trainNo:Ljava/lang/String;

.field private username:Ljava/lang/String;

.field private userpass:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 50
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    const/4 v1, 0x3

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    .line 57
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mIsIgnoreNetworkErr:Z

    const-string v0, ""

    .line 114
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->chooseSeats:Ljava/lang/String;

    const-string v0, "ADULT"

    .line 116
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public from(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 1

    .line 61
    iget-boolean v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mIsIgnoreNetworkErr:Z

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mIsIgnoreNetworkErr:Z

    .line 62
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    .line 63
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStation:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStation:Ljava/lang/String;

    .line 64
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    .line 65
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    .line 67
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    .line 68
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    .line 69
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStation:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStation:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    .line 72
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    .line 73
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchSleepTime:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchSleepTime:Ljava/lang/Integer;

    .line 75
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchWatiTime:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchWatiTime:Ljava/lang/Integer;

    .line 76
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->tour_flag:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->tour_flag:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    .line 78
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->username:Ljava/lang/String;

    .line 83
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->userpass:Ljava/lang/String;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->userpass:Ljava/lang/String;

    return-object p0
.end method

.method public getAlertType()Ljava/lang/Integer;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    return-object v0
.end method

.method public getChooseSeats()Ljava/lang/String;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->chooseSeats:Ljava/lang/String;

    return-object v0
.end method

.method public getFromStation()Ljava/lang/String;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStation:Ljava/lang/String;

    return-object v0
.end method

.method public getFromStationName()Ljava/lang/String;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    return-object v0
.end method

.method public getIsIgnoreNetworkErr()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mIsIgnoreNetworkErr:Z

    return v0
.end method

.method public getOrderDate()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderPerson()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderSeat()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderTime()Ljava/lang/String;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    return-object v0
.end method

.method public getPurpose()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryFreq()Ljava/lang/Integer;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRangeCode()Ljava/lang/String;
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->rangeCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchSleepTime()Ljava/lang/Integer;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchSleepTime:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSearchWatiTime()Ljava/lang/Integer;
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchWatiTime:Ljava/lang/Integer;

    return-object v0
.end method

.method public getToStation()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStation:Ljava/lang/String;

    return-object v0
.end method

.method public getToStationName()Ljava/lang/String;
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    return-object v0
.end method

.method public getTourFlag()Ljava/lang/String;
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->tour_flag:Ljava/lang/String;

    return-object v0
.end method

.method public getTrainClass()[Ljava/lang/String;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    return-object v0
.end method

.method public getTrainCode()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    return-object v0
.end method

.method public getTrainNo()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->username:Ljava/lang/String;

    return-object v0
.end method

.method public getUserpass()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->userpass:Ljava/lang/String;

    return-object v0
.end method

.method public initConfig(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "from"

    .line 262
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    const-string v0, "to"

    .line 263
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    .line 265
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStation:Ljava/lang/String;

    .line 266
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStation:Ljava/lang/String;

    const-string v0, "order_time"

    .line 268
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    const-string v0, "order_date"

    .line 269
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    const-string v0, "train_class"

    .line 270
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ","

    .line 272
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    :cond_0
    const-string v0, "order_people"

    .line 275
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    const-string v0, "order_seat"

    .line 276
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    const-string v0, "train_no"

    .line 277
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    const-string v0, "train_code"

    .line 278
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    const-string v0, "purpose"

    .line 279
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    const-string v0, "alert_type"

    .line 281
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    const-string v0, "query_freq"

    .line 283
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    .line 284
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    .line 286
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "ADULT"

    .line 287
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public isAllStudent()Z
    .locals 5

    .line 293
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    .line 294
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;)V

    .line 299
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 300
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 304
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 305
    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    const-string v4, "3"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    return v1
.end method

.method public setAlertType(Ljava/lang/Integer;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    return-void
.end method

.method public setChooseSeats(Ljava/lang/String;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->chooseSeats:Ljava/lang/String;

    return-void
.end method

.method public setFromStation(Ljava/lang/String;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStation:Ljava/lang/String;

    return-void
.end method

.method public setFromStationName(Ljava/lang/String;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    return-void
.end method

.method public setIsIgnoreNetworkErr(Z)V
    .locals 0

    .line 89
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mIsIgnoreNetworkErr:Z

    return-void
.end method

.method public setOrderDate(Ljava/lang/String;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    return-void
.end method

.method public setOrderPerson(Ljava/lang/String;)V
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    return-void
.end method

.method public setOrderSeat(Ljava/lang/String;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    return-void
.end method

.method public setOrderTime(Ljava/lang/String;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    return-void
.end method

.method public setPurpose(Ljava/lang/String;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    return-void
.end method

.method public setQueryFreq(Ljava/lang/Integer;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    return-void
.end method

.method public setRangeCode(Ljava/lang/String;)V
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->rangeCode:Ljava/lang/String;

    return-void
.end method

.method public setSearchSleepTime(Ljava/lang/Integer;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchSleepTime:Ljava/lang/Integer;

    return-void
.end method

.method public setSearchWatiTime(Ljava/lang/Integer;)V
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->searchWatiTime:Ljava/lang/Integer;

    return-void
.end method

.method public setToStation(Ljava/lang/String;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStation:Ljava/lang/String;

    return-void
.end method

.method public setToStationName(Ljava/lang/String;)V
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    return-void
.end method

.method public setTourFlag(Ljava/lang/String;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->tour_flag:Ljava/lang/String;

    return-void
.end method

.method public setTrainClass([Ljava/lang/String;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    return-void
.end method

.method public setTrainCode(Ljava/lang/String;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    return-void
.end method

.method public setTrainNo(Ljava/lang/String;)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->username:Ljava/lang/String;

    return-void
.end method

.method public setUserpass(Ljava/lang/String;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->userpass:Ljava/lang/String;

    return-void
.end method

.method public toJSONObject()Lorg/json/JSONObject;
    .locals 7

    .line 316
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 318
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ADULT"

    .line 319
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    :cond_0
    const-string v1, "from"

    .line 321
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->fromStationName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "to"

    .line 322
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->toStationName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "order_time"

    .line 323
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderTime:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "order_date"

    .line 324
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 325
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 326
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 327
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainClass:[Ljava/lang/String;

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    .line 328
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "train_class"

    .line 330
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const-string v1, "order_people"

    .line 333
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderPerson:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "order_seat"

    .line 334
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->orderSeat:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "train_no"

    .line 335
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainNo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "train_code"

    .line 336
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->trainCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "purpose"

    .line 337
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->purpose:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "alert_type"

    .line 338
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->alertType:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "query_freq"

    .line 339
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->mQueryFreq:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 343
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method
