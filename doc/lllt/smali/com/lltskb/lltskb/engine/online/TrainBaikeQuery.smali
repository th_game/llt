.class public Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;
.super Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;
.source "TrainBaikeQuery.java"


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u6b21\u4fe1\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onItemClicked(I)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 23
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    return-void

    .line 24
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;

    .line 25
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 26
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainBaikeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showStationBaike(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
