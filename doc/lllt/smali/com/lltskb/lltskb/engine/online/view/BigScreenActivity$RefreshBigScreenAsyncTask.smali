.class Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;
.super Landroid/os/AsyncTask;
.source "BigScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RefreshBigScreenAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V
    .locals 1

    .line 310
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 311
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 307
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 315
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 320
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getInstance()Lcom/lltskb/lltskb/engine/online/BigScreenModel;

    move-result-object v1

    .line 321
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->access$000(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->access$100(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/BigScreenModel;->getSTBigScreen(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    return-object v0
.end method

.method public synthetic lambda$onPreExecute$0$BigScreenActivity$RefreshBigScreenAsyncTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 328
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 307
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0

    .line 334
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 335
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 336
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;

    if-nez p1, :cond_0

    return-void

    .line 340
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;->access$200(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 327
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 328
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$RefreshBigScreenAsyncTask$C3Lo1bAnMmkMUI2JUMZFi-iIQ4A;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$BigScreenActivity$RefreshBigScreenAsyncTask$C3Lo1bAnMmkMUI2JUMZFi-iIQ4A;-><init>(Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity$RefreshBigScreenAsyncTask;)V

    const v2, 0x7f0d02dd

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
