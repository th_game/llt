.class public Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "TrainDetailsActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TrainDetailsActivity"

.field public static mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

.field public static mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;


# instance fields
.field private houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

.field private mDate:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private mHoubuSeatType:Ljava/lang/String;

.field private mListView:Lcom/lltskb/lltskb/view/widget/XListView;

.field private mSeatData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;",
            ">;"
        }
    .end annotation
.end field

.field private mShowMoreTickets:Z

.field private mStartTrainDate:Ljava/lang/String;

.field private mStopStations:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mTo:Ljava/lang/String;

.field private mTrainCode:Ljava/lang/String;

.field private mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

.field private mTrainNo:Ljava/lang/String;

.field private mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

.field private mTrainView:Landroid/widget/LinearLayout;

.field private slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 619
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x1

    .line 64
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mShowMoreTickets:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->onBookTicket(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->handleHoubu(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStartTrainDate:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainCode:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->createSeatDetails()V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->refreshTrainView()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->refreshSeatInfo()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Lcom/lltskb/lltskb/view/widget/XListView;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainNo:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStopStations:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$702(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStopStations:Ljava/util/Vector;

    return-object p1
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    return-object p0
.end method

.method static synthetic access$802(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    return-object p1
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    return-object p0
.end method

.method private checkFaceFlag(Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 317
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->promptLogin()V

    return v0

    .line 321
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;

    move-result-object v1

    .line 323
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;->getStatus()Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v1, :cond_1

    goto :goto_0

    .line 341
    :cond_1
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->getLogin_flag()Z

    move-result p1

    if-nez p1, :cond_2

    .line 342
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->promptLogin()V

    return v0

    .line 346
    :cond_2
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->getFace_flag()Z

    move-result p1

    if-nez p1, :cond_3

    .line 347
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->getFace_check_code()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->showFaceFlagErrorDialog(Ljava/lang/String;)V

    return v0

    :cond_3
    const/4 p1, 0x1

    return p1

    .line 325
    :cond_4
    :goto_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;->getMessages()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 326
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;->getMessages()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string p1, ""

    :goto_1
    const/4 v1, 0x0

    .line 331
    :try_start_0
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 333
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 334
    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    const-string v2, "TrainDetailsActivity"

    invoke-static {v2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    .line 337
    :goto_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d02de

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return v0
.end method

.method private createSeatDetails()V
    .locals 7

    .line 429
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 430
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 435
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez v0, :cond_1

    return-void

    .line 439
    :cond_1
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    if-nez v0, :cond_2

    .line 440
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    .line 443
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->isAllowedTime(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 444
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "--"

    if-nez v1, :cond_4

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 445
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u5546\u52a1\u5ea7"

    .line 446
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 447
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 448
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->swz:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 449
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "9"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 450
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 453
    :cond_4
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 454
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u9ad8\u7ea7\u8f6f\u5367"

    .line 455
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 456
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 457
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->gr:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 458
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "6"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_1
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 459
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_6
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 463
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u7279\u7b49\u5ea7"

    .line 464
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 465
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 466
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->tz:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 467
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "P"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    :cond_7
    const/4 v4, 0x0

    :goto_2
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 468
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 471
    :cond_8
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 472
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u4e00\u7b49\u5ea7"

    .line 473
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 474
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 475
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->zy:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 476
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "M"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    :cond_9
    const/4 v4, 0x0

    :goto_3
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 477
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 480
    :cond_a
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 481
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u4e8c\u7b49\u5ea7"

    .line 482
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 483
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 484
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->ze:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 485
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "O"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    :cond_b
    const/4 v4, 0x0

    :goto_4
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 486
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 489
    :cond_c
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 490
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u52a8\u5367"

    .line 491
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 492
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 493
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->srrb:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 494
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "F"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_5

    :cond_d
    const/4 v4, 0x0

    :goto_5
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 495
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 498
    :cond_e
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 499
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u8f6f\u5367"

    .line 500
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 501
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 502
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->rw:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 503
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "4"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_6

    :cond_f
    const/4 v4, 0x0

    :goto_6
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 504
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_10
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 508
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u786c\u5367"

    .line 509
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 510
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 511
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->yw:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 512
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "3"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_7

    :cond_11
    const/4 v4, 0x0

    :goto_7
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 513
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 516
    :cond_12
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 517
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u8f6f\u5ea7"

    .line 518
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 519
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 520
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->rz:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 521
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v6, "2"

    invoke-static {v6, v4, v5}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_8

    :cond_13
    const/4 v4, 0x0

    :goto_8
    iput v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 522
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 525
    :cond_14
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 526
    new-instance v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v4}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v4, "\u786c\u5ea7"

    .line 527
    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 528
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 529
    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->yz:Ljava/lang/String;

    iput-object v4, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 530
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    sget-object v4, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    const-string v5, "1"

    invoke-static {v5, v0, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoubuStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_9

    :cond_15
    const/4 v0, 0x0

    :goto_9
    iput v0, v1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 531
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 534
    :cond_16
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 535
    new-instance v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;-><init>(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;)V

    const-string v1, "\u65e0\u5ea7"

    .line 536
    iput-object v1, v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    .line 537
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->ticket:Ljava/lang/String;

    .line 538
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->wz:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    .line 539
    iput v2, v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->houbuStatus:I

    .line 540
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_17
    return-void
.end method

.method private doSubmitHBOrderRequest()V
    .locals 3

    const v0, 0x7f0d01a6

    const/high16 v1, -0x1000000

    const/4 v2, 0x0

    .line 207
    invoke-static {p0, v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 208
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$3QLqZgsBiBqhh8W8owU0G_0XUj4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$3QLqZgsBiBqhh8W8owU0G_0XUj4;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getSuccessRate()V
    .locals 5

    const-string v0, "TrainDetailsActivity"

    const-string v1, "getSuccessRate"

    .line 355
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 357
    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mHoubuSeatType:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getSuccessRate(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 358
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;->getStatus()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 363
    :cond_0
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mHoubuSeatType:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;-><init>(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/GetSuccessRateDTO;)V

    .line 364
    invoke-interface {v0, v2}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->addHoubuTicket(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)Z

    .line 366
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$0LucCuAS_ClTjfWPqiZRUCW2dKA;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$0LucCuAS_ClTjfWPqiZRUCW2dKA;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void

    .line 359
    :cond_1
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    sget-object v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$zbQ36IdojlZu62rH7_1cHy0OaXk;->INSTANCE:Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$zbQ36IdojlZu62rH7_1cHy0OaXk;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private handleHoubu(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V
    .locals 6

    const-string v0, "TrainDetailsActivity"

    const-string v1, "handleHoubu"

    .line 246
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v1

    .line 248
    invoke-interface {v1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketCount()I

    move-result v2

    const v3, 0x7f0d02de

    const/4 v4, 0x0

    const/4 v5, 0x4

    if-lt v2, v5, :cond_0

    .line 249
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const p1, 0x7f0d00eb

    .line 250
    invoke-static {p0, v3, p1, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 254
    :cond_0
    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-eqz v2, :cond_5

    if-nez p1, :cond_1

    goto :goto_0

    .line 258
    :cond_1
    iget-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuCountInDate(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_2

    .line 259
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const p1, 0x7f0d00f3

    .line 260
    invoke-static {p0, v3, p1, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 264
    :cond_2
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->isAdjacentDate(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 265
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const p1, 0x7f0d00e5

    .line 266
    invoke-static {p0, v3, p1, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 270
    :cond_3
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->isAllowedTime(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 271
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const p1, 0x7f0d00f4

    .line 272
    invoke-static {p0, v3, p1, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 276
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 278
    iget-object p1, p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeForHoubu(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mHoubuSeatType:Ljava/lang/String;

    const p1, 0x7f0d01a6

    const/high16 v0, -0x1000000

    .line 279
    invoke-static {p0, p1, v0, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 280
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ess_4RsX7vAc2d_DZiURCNio2fM;

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ess_4RsX7vAc2d_DZiURCNio2fM;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    return-void

    :cond_5
    :goto_0
    const-string p1, "handleHoubu npe"

    .line 255
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic lambda$-qXmAJf3nAQfPUUsKt0ExbXnR84(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->updateHoubuList()V

    return-void
.end method

.method public static synthetic lambda$lW1ctOn6k9DQ7DhkPWOlzaF9L-c(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSuccessRate()V

    return-void
.end method

.method static synthetic lambda$onCreate$6(F)V
    .locals 0

    return-void
.end method

.method private moreTickets()V
    .locals 6

    .line 373
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    if-nez v0, :cond_0

    return-void

    .line 375
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 376
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 378
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    .line 379
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainNo:Ljava/lang/String;

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->train_no:Ljava/lang/String;

    .line 380
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStartTrainDate:Ljava/lang/String;

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_train_date:Ljava/lang/String;

    .line 382
    new-instance v2, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    invoke-direct {v2}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;-><init>()V

    .line 384
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 385
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    const-string v5, "from"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    const-string v5, "to"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    const-string v5, "date"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->start_train_date:Ljava/lang/String;

    const-string v5, "start_date"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 390
    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->setTrainDTO(Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)V

    const v1, 0x7f0900e1

    .line 392
    const-class v3, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    .line 393
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 392
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 396
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method private onBookTicket(Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;)V
    .locals 3

    .line 401
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez v0, :cond_0

    return-void

    .line 403
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 404
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    const-string v2, "order_from_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    const-string v2, "order_to_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    const-string v2, "order_depart_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainCode:Ljava/lang/String;

    const-string v2, "order_train_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    iget-object v1, p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->name:Ljava/lang/String;

    const-string v2, "order_seat_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    iget-object p1, p1, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$SeatDetail;->price:Ljava/lang/String;

    const-string v1, "order_seat_price"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    sget-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v1, "order_start_time"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    sget-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const-string v1, "order_arrive_time"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    sget-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v1, "order_duration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, 0x2

    const-string v1, "order_from_flag"

    .line 413
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 414
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private onChangeDate(Z)V
    .locals 11

    const-string v0, "TrainDetailsActivity"

    const-string v1, "onChangeDate"

    .line 686
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 691
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 693
    :catch_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 695
    :goto_0
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    if-eqz p1, :cond_0

    sub-long/2addr v1, v3

    goto :goto_1

    :cond_0
    add-long/2addr v1, v3

    .line 701
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultMaxDays()J

    move-result-wide v5

    .line 702
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 703
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    const-wide/16 v9, 0x18

    mul-long v5, v5, v9

    const-wide/16 v9, 0xe10

    mul-long v5, v5, v9

    const-wide/16 v9, 0x3e8

    mul-long v5, v5, v9

    add-long/2addr v5, v7

    sub-long/2addr v7, v3

    const/4 p1, 0x0

    const v3, 0x7f0d0111

    cmp-long v4, v1, v7

    if-gez v4, :cond_1

    .line 707
    invoke-static {p0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_1
    cmp-long v4, v1, v5

    if-lez v4, :cond_2

    .line 710
    invoke-static {p0, v3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 713
    :cond_2
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    const/4 p1, 0x0

    .line 715
    sput-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 716
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->prepareData()V

    return-void
.end method

.method private prepareData()V
    .locals 5

    const v0, 0x7f090273

    .line 419
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 421
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    .line 425
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private promptLogin()V
    .locals 3

    .line 293
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$NANx5yy04HWyksO6-gjb-ONrTsI;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$NANx5yy04HWyksO6-gjb-ONrTsI;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    const v1, 0x7f0d02de

    const v2, 0x7f0d010e

    invoke-static {p0, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private readIntent()Z
    .locals 4

    .line 627
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const-string v1, "ticket_start_station"

    .line 631
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    const-string v1, "ticket_arrive_station"

    .line 632
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    const-string v1, "train_code"

    .line 633
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainNo:Ljava/lang/String;

    const-string v1, "ticket_date"

    .line 634
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    const-string v1, "train_name"

    .line 635
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainCode:Ljava/lang/String;

    const-string v1, "start_train_date"

    .line 636
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStartTrainDate:Ljava/lang/String;

    .line 637
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStartTrainDate:Ljava/lang/String;

    const/4 v2, 0x1

    const-string v3, "-"

    invoke-static {v1, v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->ensureDate(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStartTrainDate:Ljava/lang/String;

    const-string v1, "show_more_ticket"

    .line 639
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mShowMoreTickets:Z

    return v2
.end method

.method private refreshSeatInfo()V
    .locals 2

    .line 603
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    if-eqz v0, :cond_0

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "seat date size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TrainDetailsActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    new-instance v0, Ljava/util/Vector;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mSeatData:Ljava/util/Vector;

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    .line 606
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->setData(Ljava/util/Vector;)V

    goto :goto_0

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 609
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;->setData(Ljava/util/Vector;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private refreshTrainView()V
    .locals 7

    .line 581
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStopStations:Ljava/util/Vector;

    if-nez v1, :cond_0

    goto :goto_1

    .line 584
    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 587
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mStopStations:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;

    .line 588
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    .line 591
    :cond_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0b008e

    .line 592
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5, v6, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 593
    invoke-direct {p0, v4, v3, v2}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->setupView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;Z)V

    .line 594
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 595
    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method private setupView(Landroid/view/View;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;Z)V
    .locals 5

    if-eqz p1, :cond_6

    if-nez p2, :cond_0

    goto/16 :goto_2

    :cond_0
    const v0, 0x7f0902e9

    .line 553
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    return-void

    .line 555
    :cond_1
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->stopover_time:Ljava/lang/String;

    const-string v2, "---"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 556
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 558
    :cond_2
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->stopover_time:Ljava/lang/String;

    const-string v3, "\u5206\u949f"

    const-string v4, "\'"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 559
    iget-object v3, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->station_name:Ljava/lang/String;

    .line 560
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 561
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 562
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    if-eqz p3, :cond_3

    const p3, 0x7f06004c

    .line 566
    invoke-static {p0, p3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_3
    const p3, 0x7f060021

    .line 568
    invoke-static {p0, p3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    const p3, 0x7f09024d

    .line 571
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_4

    return-void

    .line 573
    :cond_4
    iget-object p3, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 574
    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 576
    :cond_5
    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    :goto_2
    return-void
.end method

.method private showDateDialog()V
    .locals 6

    const-string v0, "TrainDetailsActivity"

    const-string v1, "showDateDialog"

    .line 646
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 649
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->requestWindowFeature(I)Z

    .line 650
    new-instance v2, Lcom/lltskb/lltskb/view/CalendarView;

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultMaxDays()J

    move-result-wide v3

    invoke-direct {v2, p0, v3, v4}, Lcom/lltskb/lltskb/view/CalendarView;-><init>(Landroid/content/Context;J)V

    .line 652
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    const-string v3, "\u5e74\u6708\u65e5"

    .line 653
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 654
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz v3, :cond_0

    const v4, 0x7f0e00bd

    .line 656
    invoke-virtual {v3, v4}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 659
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 663
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd"

    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 664
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 666
    :catch_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 668
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 669
    invoke-virtual {v4, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 670
    invoke-virtual {v4, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v3, 0x2

    invoke-virtual {v4, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/lltskb/lltskb/view/CalendarView;->setSelectedDate(III)V

    .line 672
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$2OBxhlr503WbVvOye0NFEOxjcK4;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$2OBxhlr503WbVvOye0NFEOxjcK4;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Landroid/support/v7/app/AppCompatDialog;)V

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/view/CalendarView;->setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V

    return-void
.end method

.method private showFaceFlagErrorDialog(Ljava/lang/String;)V
    .locals 2

    const-string v0, "01"

    .line 301
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "11"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    :cond_0
    const-string v0, "02"

    .line 303
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f0d00f0

    if-nez v0, :cond_6

    const-string v0, "12"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    const-string v0, "03"

    .line 305
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "13"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, "04"

    .line 307
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "14"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 310
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 308
    :cond_4
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00f2

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 306
    :cond_5
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00f1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 304
    :cond_6
    :goto_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 302
    :cond_7
    :goto_3
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00ef

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 312
    :goto_4
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d02de

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private updateHoubuList()V
    .locals 6

    .line 183
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 184
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketCount()I

    move-result v2

    const/4 v3, 0x0

    if-lez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->setVisibility(I)V

    .line 186
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d013a

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 187
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v2, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 188
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    const v3, 0x7f09026b

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 190
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :cond_1
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->getHoubuTicketList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->setHoubuTicketList(Ljava/util/List;)V

    .line 195
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->notifyDataSetChanged()V

    .line 196
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->requestLayout()V

    .line 197
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$fobYXhGtT0plPEZIuw4kGBjqkSA;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$fobYXhGtT0plPEZIuw4kGBjqkSA;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 199
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->hide()V

    .line 202
    :goto_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    const v1, 0x7f09005e

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 203
    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ybvU5bupZ3flQ-YYZNk-iuFMdBw;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ybvU5bupZ3flQ-YYZNk-iuFMdBw;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$doSubmitHBOrderRequest$11$TrainDetailsActivity()V
    .locals 3

    .line 209
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 210
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->submitOrderRequest()Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;

    move-result-object v0

    .line 211
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$venUwc_EONObmU15Ecc49MIDhtw;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$venUwc_EONObmU15Ecc49MIDhtw;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$getSuccessRate$15$TrainDetailsActivity()V
    .locals 0

    .line 367
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 368
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->updateHoubuList()V

    return-void
.end method

.method public synthetic lambda$handleHoubu$13$TrainDetailsActivity(Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;)V
    .locals 2

    .line 281
    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mHoubuSeatType:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->chechFace(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;

    move-result-object p1

    .line 282
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ii4K6LNWMrwtEWZmbBKn_JBcuio;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ii4K6LNWMrwtEWZmbBKn_JBcuio;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->executeOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$null$10$TrainDetailsActivity(Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;)V
    .locals 5

    .line 212
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const v0, 0x7f0d00e6

    const/4 v1, 0x0

    const v2, 0x7f0d02de

    if-nez p1, :cond_0

    .line 214
    invoke-static {p0, v2, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;IILandroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;->getMessages()Ljava/util/List;

    move-result-object v3

    .line 219
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_1

    .line 220
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v0, 0x0

    .line 221
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 223
    :cond_1
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestDTO;->getData()Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestData;

    move-result-object p1

    if-nez p1, :cond_2

    .line 225
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/SubmitOrderRequestData;->getFlag()Z

    move-result p1

    if-nez p1, :cond_3

    .line 230
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 234
    :cond_3
    new-instance p1, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-direct {p1}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;-><init>()V

    .line 235
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$-qXmAJf3nAQfPUUsKt0ExbXnR84;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$-qXmAJf3nAQfPUUsKt0ExbXnR84;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->setListener(Lcom/lltskb/lltskb/fragment/HoubuTicketFragment$Listener;)V

    .line 236
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0900e1

    .line 237
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 238
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method public synthetic lambda$null$12$TrainDetailsActivity(Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;)V
    .locals 1

    .line 283
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->checkFaceFlag(Lcom/lltskb/lltskb/engine/online/dto/CheckFaceDTO;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 284
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$lW1ctOn6k9DQ7DhkPWOlzaF9L-c;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$lW1ctOn6k9DQ7DhkPWOlzaF9L-c;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->executeOnNetworkIO(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 286
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onCreate$0$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    .line 121
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$onCreate$1$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x0

    .line 127
    sput-object p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 128
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->prepareData()V

    return-void
.end method

.method public synthetic lambda$onCreate$2$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->moreTickets()V

    return-void
.end method

.method public synthetic lambda$onCreate$3$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    .line 149
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$onCreate$4$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 155
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$onCreate$5$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x0

    .line 161
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->onChangeDate(Z)V

    return-void
.end method

.method public synthetic lambda$onCreate$7$TrainDetailsActivity(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)V
    .locals 1

    .line 174
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    move-result-object v0

    .line 175
    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;->delHoubuTicket(Lcom/lltskb/lltskb/engine/online/dto/HoubuTicket;)Z

    .line 176
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->updateHoubuList()V

    return-void
.end method

.method public synthetic lambda$promptLogin$14$TrainDetailsActivity(Landroid/view/View;)V
    .locals 1

    .line 294
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 295
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public synthetic lambda$showDateDialog$16$TrainDetailsActivity(Landroid/support/v7/app/AppCompatDialog;IIILjava/lang/String;)V
    .locals 2

    .line 674
    sget-object p5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    add-int/2addr p3, p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v0, p2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    const-string p2, "%04d-%02d-%02d"

    invoke-static {p5, p2, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    .line 679
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->prepareData()V

    .line 681
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$updateHoubuList$8$TrainDetailsActivity()V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->show()V

    return-void
.end method

.method public synthetic lambda$updateHoubuList$9$TrainDetailsActivity(Landroid/view/View;)V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->doSubmitHBOrderRequest()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 85
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 86
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0b0092

    .line 87
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->setContentView(I)V

    .line 89
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->readIntent()Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "TrainDetailsActivity"

    const-string v0, "init from intent failed"

    .line 90
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const v0, 0x7f090223

    .line 94
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090164

    .line 97
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainView:Landroid/widget/LinearLayout;

    const v0, 0x7f090183

    .line 99
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/widget/XListView;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullRefreshEnable(Z)V

    .line 101
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setPullLoadEnable(Z)V

    .line 102
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/widget/XListView;->setAutoLoadEnable(Z)V

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    invoke-virtual {v0, p0}, Lcom/lltskb/lltskb/view/widget/XListView;->setXListViewListener(Lcom/lltskb/lltskb/view/widget/XListView$IXListViewListener;)V

    .line 104
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$1;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-direct {v0, p0, v2}, Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter$Listener;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    .line 118
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTrainSeatListAdapter:Lcom/lltskb/lltskb/adapters/TrainSeatListAdapter;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/view/widget/XListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f0900fb

    .line 120
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$anoX_bW_OwSHFlphRS45lUQDnHA;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$anoX_bW_OwSHFlphRS45lUQDnHA;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090067

    .line 123
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 124
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$40kSZWFaEAGeQb38RPONF-0Ucqw;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$40kSZWFaEAGeQb38RPONF-0Ucqw;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090144

    .line 131
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 132
    new-instance v2, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$5Ity7p6Fa7q2frq7TiUID4bXnRE;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$5Ity7p6Fa7q2frq7TiUID4bXnRE;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mShowMoreTickets:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09025c

    .line 136
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 138
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d0133

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 139
    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mFrom:Ljava/lang/String;

    aput-object v5, v4, v1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mTo:Ljava/lang/String;

    aput-object v1, v4, p1

    invoke-static {v3, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->prepareData()V

    const p1, 0x7f090273

    .line 144
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_3

    .line 146
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$nPwSlPJu8bCZY9jGLJDFpt1818c;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$nPwSlPJu8bCZY9jGLJDFpt1818c;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const p1, 0x7f0902bf

    .line 153
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 155
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ROBr_CUZbLQ_TlvomAFIZl6cz-4;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ROBr_CUZbLQ_TlvomAFIZl6cz-4;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const p1, 0x7f0902ab

    .line 159
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 161
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ZhdSaz-66k2_ohQeYV8sJioWhuo;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$ZhdSaz-66k2_ohQeYV8sJioWhuo;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const p1, 0x7f0901cd

    .line 163
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    .line 165
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->slideBottomLayout:Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;

    sget-object v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$qGD5sJ7ru-a529AhVE3KsoxSbAU;->INSTANCE:Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$qGD5sJ7ru-a529AhVE3KsoxSbAU;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/widget/SlideBottomLayout;->setShortSlideListener(Lcom/lltskb/lltskb/view/widget/SlideBottomLayout$ShortSlideListener;)V

    const p1, 0x7f0901bf

    .line 168
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 169
    new-instance v0, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-direct {v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    .line 170
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 171
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 173
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->houbuListItemAdapter:Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;

    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$yr0l9yzcTTPHTb8-lLXOxuyiZec;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$yr0l9yzcTTPHTb8-lLXOxuyiZec;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter;->setLister(Lcom/lltskb/lltskb/adapters/HoubuListItemAdapter$Listener;)V

    .line 178
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->updateHoubuList()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 723
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 724
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 725
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/MoreTicketsFragment;->dismiss()V

    return v1

    .line 729
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;

    if-eqz v0, :cond_1

    .line 730
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 731
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/SelectPassengersFragment;->dismiss()V

    return v1

    .line 735
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;

    if-eqz v0, :cond_2

    .line 736
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 737
    invoke-virtual {v0}, Lcom/lltskb/lltskb/fragment/HoubuTicketFragment;->dismiss()V

    return v1

    .line 742
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLoadMore()V
    .locals 0

    return-void
.end method

.method public onRefresh()V
    .locals 2

    .line 747
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mListView:Lcom/lltskb/lltskb/view/widget/XListView;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/widget/XListView;->setRefreshTime(Ljava/lang/String;)V

    .line 748
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->prepareData()V

    return-void
.end method
