.class public Lcom/lltskb/lltskb/engine/online/PayOrderModel;
.super Ljava/lang/Object;
.source "PayOrderModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PayOrderModel"

.field private static mInst:Lcom/lltskb/lltskb/engine/online/PayOrderModel;


# instance fields
.field private mErrorMsg:Ljava/lang/String;

.field private mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/engine/online/PayOrderModel;
    .locals 1

    .line 33
    sget-object v0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mInst:Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mInst:Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    .line 36
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mInst:Lcom/lltskb/lltskb/engine/online/PayOrderModel;

    return-object v0
.end method

.method private getAlipayForm(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 652
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 653
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAlipayForm url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 659
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getAlipayForm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 662
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 663
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getWapPayGatewayParams()Ljava/lang/String;
    .locals 3

    const-string v0, "utf-8"

    .line 414
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "interfaceName=WAP_SERVLET&interfaceVersion=1.0&tranData="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 416
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTranData()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&merSignMsg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 417
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerSignMsg()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 418
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&transType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 419
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTransType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 422
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method private parseContinuePayNoCompleteMyOrder(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_7

    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto/16 :goto_2

    .line 83
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 85
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 86
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_1

    .line 91
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 92
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    const-string v2, ""

    .line 93
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    .line 95
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 96
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    const-string v1, "status"

    .line 100
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    :cond_3
    const-string v1, "data"

    .line 102
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const/4 v1, 0x1

    if-nez p1, :cond_4

    return v1

    :cond_4
    const-string v2, "existError"

    .line 106
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v3, "Y"

    .line 107
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v1, "errorMsg"

    .line 108
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    return v0

    :cond_5
    return v1

    .line 87
    :cond_6
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v1, 0x7f0d010e

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return v0

    .line 79
    :cond_7
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    return v0
.end method

.method private parsePayCheck(Ljava/lang/String;)Z
    .locals 8

    const-string v0, "exception="

    const-string v1, "PayOrderModel"

    const-string v2, ""

    .line 255
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;-><init>()V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 256
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 258
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 259
    instance-of v4, v3, Lorg/json/JSONObject;

    if-nez v4, :cond_0

    goto/16 :goto_1

    .line 264
    :cond_0
    check-cast v3, Lorg/json/JSONObject;

    const-string v4, "messages"

    .line 265
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONArray;

    .line 266
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    .line 267
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    .line 268
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 269
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    const-string v4, "status"

    .line 274
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    return p1

    :cond_2
    const-string v4, "data"

    .line 279
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-nez v3, :cond_3

    return p1

    :cond_3
    const-string v4, "flag"

    .line 281
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    return p1

    :cond_4
    const-string v4, "payForm"

    .line 283
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-nez v3, :cond_5

    return p1

    .line 286
    :cond_5
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v5, "payOrderId"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPayOrderId(Ljava/lang/String;)V

    .line 288
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v5, "interfaceName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceName(Ljava/lang/String;)V

    .line 289
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v5, "interfaceVersion"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceVersion(Ljava/lang/String;)V

    const-string v4, "tranData"

    .line 290
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "\\r\\n"

    if-eqz v4, :cond_6

    .line 292
    :try_start_1
    invoke-static {v4, v5, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 294
    :cond_6
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v6, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTranData(Ljava/lang/String;)V

    const-string v4, "merSignMsg"

    .line 296
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 298
    invoke-static {v4, v5, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 300
    :cond_7
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerSignMsg(Ljava/lang/String;)V

    .line 302
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v4, "appId"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setAppId(Ljava/lang/String;)V

    .line 303
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v4, "transType"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTransType(Ljava/lang/String;)V

    .line 304
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v4, "epayurl"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPayUrl(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    .line 260
    :cond_8
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d010e

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return p1

    :catch_0
    move-exception v2

    .line 313
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    .line 315
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    return p1

    :catch_1
    move-exception v2

    .line 308
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    .line 310
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    return p1
.end method

.method private parsePayOrderInit(Ljava/lang/String;)Z
    .locals 7

    const-string v0, ""

    const-string v1, "\\r\\n"

    const-string v2, ";"

    const/4 v3, 0x0

    .line 350
    :try_start_0
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;-><init>()V

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v4, "interfaceName"

    .line 352
    invoke-static {p1, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 353
    invoke-static {p1, v2, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    add-int/lit8 v4, v4, 0x11

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    .line 354
    invoke-static {p1, v4, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 355
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceName(Ljava/lang/String;)V

    const-string v4, "interfaceVersion"

    .line 356
    invoke-static {p1, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 357
    invoke-static {p1, v2, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    add-int/lit8 v4, v4, 0x14

    sub-int/2addr v5, v6

    .line 358
    invoke-static {p1, v4, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 359
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setInterfaceVersion(Ljava/lang/String;)V

    const-string v4, "tranData"

    .line 360
    invoke-static {p1, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 361
    invoke-static {p1, v2, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    add-int/lit8 v4, v4, 0xc

    sub-int/2addr v5, v6

    .line 362
    invoke-static {p1, v4, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 363
    invoke-static {v4, v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 364
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTranData(Ljava/lang/String;)V

    const-string v4, "merSignMsg"

    .line 365
    invoke-static {p1, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 366
    invoke-static {p1, v2, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    add-int/lit8 v4, v4, 0xe

    sub-int/2addr v5, v6

    .line 367
    invoke-static {p1, v4, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-static {v4, v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 369
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerSignMsg(Ljava/lang/String;)V

    const-string v0, "appId"

    .line 370
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 371
    invoke-static {p1, v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v0, v0, 0x9

    sub-int/2addr v1, v6

    .line 372
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setAppId(Ljava/lang/String;)V

    const-string v0, "transType"

    .line 374
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 375
    invoke-static {p1, v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v0, v0, 0xd

    sub-int/2addr v1, v6

    .line 376
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 377
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setTransType(Ljava/lang/String;)V

    const-string v0, "loseTime"

    .line 379
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    add-int/lit8 v1, v0, 0xb

    add-int/lit8 v0, v0, 0x18

    .line 381
    invoke-static {p1, v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p1

    .line 382
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->toLong(Ljava/lang/String;)J

    .line 385
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    const/4 v3, 0x1

    :cond_1
    return v3

    :catchall_0
    move-exception p1

    .line 389
    throw p1

    :catch_0
    return v3
.end method

.method private replaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "pages/web/img/logo.gif"

    .line 497
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "https://epay.12306.cn/pay/pages/web/img/logo.gif"

    .line 498
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    const-string v0, "pages/web/mb.html"

    .line 500
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    const-string v2, "https://epay.12306.cn/pay/pages/web/mb.html"

    if-eqz v1, :cond_2

    .line 501
    invoke-static {p1, v0, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_2
    const-string v1, "pages/web/css/bank.css"

    .line 503
    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "https://epay.12306.cn/pay/pages/web/css/bank.css"

    .line 504
    invoke-static {p1, v1, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 506
    :cond_3
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 507
    invoke-static {p1, v1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_4
    const-string v0, "/pay/webBusiness"

    .line 509
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "https://epay.12306.cn/pay/webBusiness"

    .line 510
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_5
    const-string v0, "/pay/wapBusiness"

    .line 512
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "https://epay.12306.cn/pay/wapBusiness"

    .line 513
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_6
    const-string v0, "pages/web/images/bank_gsyh2.gif"

    .line 515
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_gsyh2.gif"

    .line 516
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_7
    const-string v0, "pages/web/images/bank_nyyh2.gif"

    .line 519
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_nyyh2.gif"

    .line 520
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_8
    const-string v0, "pages/web/images/bank_zgyh2.gif"

    .line 522
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_zgyh2.gif"

    .line 523
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_9
    const-string v0, "pages/web/images/bank_zsyh2.gif"

    .line 525
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_zsyh2.gif"

    .line 526
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_a
    const-string v0, "pages/web/images/bank_zgyl.gif"

    .line 528
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_zgyl.gif"

    .line 529
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_b
    const-string v0, "pages/web/images/bank_ztytk.gif"

    .line 531
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_ztytk.gif"

    .line 532
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_c
    const-string v0, "pages/web/images/bank_jsyh2.gif"

    .line 534
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_jsyh2.gif"

    .line 535
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_d
    const-string v0, "pages/web/images/bank_zfb.gif"

    .line 537
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "https://epay.12306.cn/pay/pages/web/images/bank_zfb.gif"

    .line 538
    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_e
    return-object p1
.end method

.method private wapPayGateway()Z
    .locals 5

    .line 470
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getWapPayGatewayParams()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 473
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_WAPPAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v2, v3, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "PayOrderModel"

    .line 474
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wapPayGateway="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getWapPayInitParam(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getChannelId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setChannelId(Ljava/lang/String;)V

    .line 479
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerCustomIp()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerCustomIp(Ljava/lang/String;)V

    .line 480
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getOrderTimeoutDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setOrderTimeoutDate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return v1

    :catch_0
    move-exception v0

    .line 487
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v1

    :catch_1
    move-exception v0

    .line 484
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 485
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->wapPayGateway()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public continuePayNoCompleteMyOrder(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&pay_flag=pay"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONTINUE_PAY_NOCOMPLETEMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "continuePayNoCompleteMyOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PayOrderModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->parseContinuePayNoCompleteMyOrder(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 61
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_1

    .line 62
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 63
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAlipayClient(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 669
    :cond_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 671
    :cond_1
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getAlipayForm(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Ljava/lang/String;

    move-result-object p1

    .line 673
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/JSoupUtils;->parseAlipayHtml(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object p1

    if-nez p1, :cond_2

    return-object v0

    .line 676
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "qr url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " params="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getAlipayForm(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Ljava/lang/String;

    move-result-object p1

    .line 678
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAlipayClient qrpayloopcheck return="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    return-object v0

    :cond_3
    const-string v1, "https://"

    .line 685
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_4

    return-object v0

    :cond_4
    const-string v2, "\"}"

    .line 687
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-gez v2, :cond_5

    return-object v0

    .line 690
    :cond_5
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 692
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;-><init>()V

    .line 693
    iput-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    :cond_6
    :goto_0
    return-object v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getForm(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 153
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PayOrderModel"

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postForm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/JSoupUtils;->parseHtmlForm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 170
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 171
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getPayGatewayParams()Ljava/lang/String;
    .locals 3

    const-string v0, "utf-8"

    .line 399
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "interfaceName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getInterfaceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&interfaceVersion="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 400
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getInterfaceVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&tranData="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 401
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTranData()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&merSignMsg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 402
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerSignMsg()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 403
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&transType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 404
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTransType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 406
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public payCheck()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 213
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECK:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "_json_att="

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "payCheck="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->parsePayCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 215
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 216
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 217
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 218
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public payCheckNew()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "batch_nos=&coach_nos=&seat_nos=&passenger_id_types=&passenger_id_nos=&passenger_names=&insure_price_all=&insure_types=&if_buy_insure_only=N&hasBoughtIns=&_json_att="

    .line 193
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECKNEW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "payCheck="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->parsePayCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 195
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 196
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 197
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 198
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u7f51\u7edc\u6545\u969c"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public payFinish(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_json_att=&get_ticket_pass="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 585
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_FINISH:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PayOrderModel"

    .line 586
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "payFinish="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 592
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 589
    invoke-virtual {p1}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    :goto_0
    const/4 p1, -0x1

    return p1
.end method

.method public payGateway()Ljava/lang/String;
    .locals 4

    .line 433
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->getPayGatewayParams()Ljava/lang/String;

    move-result-object v0

    .line 436
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_PAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PayOrderModel"

    .line 437
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "payGateway="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/JSoupUtils;->getPayInitParam(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 441
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getChannelId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setChannelId(Ljava/lang/String;)V

    .line 442
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerCustomIp()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setMerCustomIp(Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getOrderTimeoutDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setOrderTimeoutDate(Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getPaymentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setPaymentType(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    .line 458
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0

    :catch_1
    move-exception v0

    .line 455
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 456
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->payGateway()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public payOrderInit()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 330
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYORDER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "payOrderInit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PayOrderModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->parsePayOrderInit(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 332
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 333
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 334
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 335
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string v1, "\u7f51\u7edc\u6545\u969c"

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public postForm(Lcom/lltskb/lltskb/engine/online/dto/FormInfo;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 125
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->params:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/FormInfo;->action:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PayOrderModel"

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postForm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/JSoupUtils;->parseHtmlForm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 142
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 143
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_1

    .line 144
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 145
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public selectBank(Lcom/lltskb/lltskb/engine/online/dto/PayInfo;)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "utf-8"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x0

    .line 549
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "interfaceName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&interfaceVersion="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getInterfaceVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&tranData="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTranData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&merSignMsg="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerSignMsg()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&appId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getAppId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&transType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTransType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&channelId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getChannelId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 556
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerCustomIp()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 557
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&merCustomIp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerCustomIp()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 560
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&orderTimeoutDate="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getOrderTimeoutDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&bankId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getBankId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 563
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    move-object p1, v2

    .line 567
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WAPBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v2, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PayOrderModel"

    .line 568
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectBank="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return v1

    :catch_1
    move-exception p1

    .line 574
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception p1

    .line 571
    invoke-virtual {p1}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    :goto_1
    const/4 p1, -0x1

    return p1
.end method

.method public webBussiness(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;
    .locals 4

    const-string v0, "utf-8"

    .line 612
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 614
    :cond_0
    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setBankId(Ljava/lang/String;)V

    .line 615
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    const-string v1, "1"

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->setBusinessType(Ljava/lang/String;)V

    .line 618
    :try_start_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tranData="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 620
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTranData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&transType="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 621
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getTransType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&channelId="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 622
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getChannelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&appId="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 623
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&merSignMsg="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 624
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerSignMsg()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&merCustomIp="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 625
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getMerCustomIp()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&orderTimeoutDate="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 626
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getOrderTimeoutDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&paymentType="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 627
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getPaymentType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&bankId="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 628
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getBankId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&businessType="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mPayInfo:Lcom/lltskb/lltskb/engine/online/dto/PayInfo;

    .line 629
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/PayInfo;->getBusinessType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 632
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    move-object p1, v2

    .line 635
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PayOrderModel;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WEBBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PayOrderModel"

    .line 636
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webBussiness="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/JSoupUtils;->parseHtmlForm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/FormInfo;

    move-result-object p1
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    .line 642
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception p1

    .line 639
    invoke-virtual {p1}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    :goto_1
    return-object v2
.end method
