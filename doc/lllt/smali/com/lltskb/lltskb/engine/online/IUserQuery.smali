.class public interface abstract Lcom/lltskb/lltskb/engine/online/IUserQuery;
.super Ljava/lang/Object;
.source "IUserQuery.java"


# virtual methods
.method public abstract checkUser()Z
.end method

.method public abstract getErrorMsg()Ljava/lang/String;
.end method

.method public abstract isSignedIn(Z)Z
.end method

.method public abstract loginAysnSuggest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation
.end method

.method public abstract loginInit()Z
.end method

.method public abstract queryUserInfo()Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;
.end method

.method public abstract signOut()Z
.end method
