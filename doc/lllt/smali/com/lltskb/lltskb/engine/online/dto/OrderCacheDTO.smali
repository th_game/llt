.class public Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;
.source "OrderCacheDTO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;
    }
.end annotation


# instance fields
.field public array_passer_name_page:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public fromStationCode:Ljava/lang/String;

.field public fromStationName:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public modifyTime:Ljava/lang/String;

.field public number:I

.field public queueName:Ljava/lang/String;

.field public queueOffset:Ljava/lang/String;

.field public requestId:Ljava/lang/String;

.field public requestTime:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public startTimeString:Ljava/lang/String;

.field public stationTrainCode:Ljava/lang/String;

.field public status:I

.field public ticketCount:I

.field public tickets:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;",
            ">;"
        }
    .end annotation
.end field

.field public toStationCode:Ljava/lang/String;

.field public toStationName:Ljava/lang/String;

.field public tourFlag:Ljava/lang/String;

.field public trainDate:Ljava/lang/String;

.field public userId:Ljava/lang/String;

.field public waitCount:I

.field public waitTime:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;-><init>()V

    return-void
.end method
