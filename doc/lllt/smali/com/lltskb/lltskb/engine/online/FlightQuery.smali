.class public Lcom/lltskb/lltskb/engine/online/FlightQuery;
.super Ljava/lang/Object;
.source "FlightQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "flight"

.field public static mFlights:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;


# instance fields
.field private mArrive:Ljava/lang/String;

.field private mStart:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private parseFlightResult(Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;
    .locals 11

    const-string v0, "exception="

    const-string v1, "flight"

    const/4 v2, 0x0

    .line 116
    :try_start_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "out"

    .line 118
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v2

    :cond_0
    const/4 v3, 0x2

    new-array v4, v3, [Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    .line 121
    new-instance v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;-><init>(Lcom/lltskb/lltskb/engine/online/FlightQuery;)V

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 122
    new-instance v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;-><init>(Lcom/lltskb/lltskb/engine/online/FlightQuery;)V

    const/4 v7, 0x1

    aput-object v5, v4, v7

    .line 123
    aget-object v5, v4, v6

    iget-object v8, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mStart:Ljava/lang/String;

    iput-object v8, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->start:Ljava/lang/String;

    .line 124
    aget-object v5, v4, v6

    iget-object v8, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mArrive:Ljava/lang/String;

    iput-object v8, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->arrive:Ljava/lang/String;

    .line 126
    aget-object v5, v4, v7

    iget-object v8, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mStart:Ljava/lang/String;

    iput-object v8, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->start:Ljava/lang/String;

    .line 127
    aget-object v5, v4, v7

    iget-object v7, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mArrive:Ljava/lang/String;

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->arrive:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 131
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    if-ge v6, v3, :cond_2

    .line 132
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 133
    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    if-nez v8, :cond_1

    goto :goto_0

    .line 135
    :cond_1
    aget-object v9, v4, v6

    iput-object v7, v9, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    .line 137
    aget-object v7, v4, v6

    const-string v9, "dt"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->date:Ljava/lang/String;

    .line 138
    aget-object v7, v4, v6

    const-string v9, "dis"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->dis:Ljava/lang/String;

    .line 139
    aget-object v7, v4, v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "\uffe5"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "pr"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    if-nez v6, :cond_3

    return-object v2

    :cond_3
    return-object v4

    :catch_0
    move-exception p1

    .line 150
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v2

    :catch_1
    move-exception p1

    .line 146
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return-object v2
.end method


# virtual methods
.method public getFlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;
    .locals 10

    const-string v0, "utf-8"

    .line 59
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getCity(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 60
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/ResMgr;->getCity(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    .line 62
    sput-object v1, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mFlights:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    .line 63
    new-instance v2, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v2}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    .line 65
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_2

    .line 72
    :cond_0
    :try_start_0
    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-static {p2, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 75
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v3, p1

    move-object v0, p2

    :goto_0
    const/4 v4, 0x0

    if-eqz p3, :cond_1

    const-string v5, "-"

    .line 81
    invoke-virtual {p3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x6

    if-lt v6, v7, :cond_1

    .line 82
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x4

    invoke-virtual {p3, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_1
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v4

    const/4 v3, 0x1

    aput-object v0, v5, v3

    const/4 v0, 0x2

    aput-object p3, v5, v0

    const-string p3, "http://ws.qunar.com/all_lp.jcp?from=%s&to=%s&goDate=%s&count=2&output=json"

    .line 84
    invoke-static {p3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 85
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mStart:Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mArrive:Ljava/lang/String;

    const/16 p1, 0xbb8

    .line 89
    :try_start_1
    invoke-virtual {v2, p3, v1, p1}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 91
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string p2, "flight"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    .line 94
    :goto_1
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_3

    const-string p2, "\"pr\""

    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    goto :goto_2

    .line 98
    :cond_2
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/FlightQuery;->parseFlightResult(Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mFlights:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    .line 99
    sget-object p1, Lcom/lltskb/lltskb/engine/online/FlightQuery;->mFlights:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    return-object p1

    :cond_3
    :goto_2
    return-object v1
.end method
