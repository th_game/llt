.class Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;
.super Landroid/os/AsyncTask;
.source "TrainDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryTrainAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V
    .locals 1

    .line 762
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 763
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 758
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .line 815
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 820
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$500(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010a

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 824
    :cond_1
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$600(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 825
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$600(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 828
    :cond_2
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$700(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 829
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$700(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    :cond_3
    const/4 v0, 0x0

    .line 833
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$802(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    .line 835
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;-><init>()V

    .line 837
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$900(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 839
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00ed

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 841
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v3

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1000(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 843
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d0108

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 847
    :cond_5
    :try_start_0
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$500(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1100(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/lltskb/lltskb/engine/online/TrainNoQuery;->queryByTrainNo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$802(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 849
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 853
    :goto_0
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$800(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 854
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$800(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;

    move-result-object v1

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;->station_list:Ljava/util/Vector;

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$702(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;Ljava/util/Vector;)Ljava/util/Vector;

    :cond_6
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Integer;

    .line 857
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 858
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d00fa

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 859
    sget-object v3, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez v3, :cond_a

    .line 864
    new-instance v3, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;-><init>()V

    .line 866
    :try_start_1
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$900(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1000(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1200(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "ADULT"

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->queryLeftTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 867
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getResult()Ljava/util/Vector;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v3, :cond_7

    return-object v2

    .line 880
    :cond_7
    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 881
    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 882
    iget-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1300(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    goto :goto_1

    .line 885
    :cond_8
    sput-object v5, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    goto :goto_2

    .line 869
    :cond_9
    :try_start_2
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/QueryLeftTicketProxy;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    .line 872
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 891
    :cond_a
    :goto_2
    sget-object v3, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez v3, :cond_b

    return-object v2

    .line 897
    :cond_b
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->get()Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;

    move-result-object v5

    .line 900
    :try_start_3
    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v6, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v7, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v8, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v9, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    .line 901
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1200(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Ljava/lang/String;

    move-result-object v10

    .line 900
    invoke-virtual/range {v5 .. v10}, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->queryTicketPrice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    move-result-object v2

    sput-object v2, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;
    :try_end_3
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v2

    .line 903
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TrainDetailsActivity"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    :goto_3
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$1400(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    new-array p1, v1, [Ljava/lang/Integer;

    const/4 v1, 0x2

    .line 909
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v4

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->publishProgress([Ljava/lang/Object;)V

    return-object v0
.end method

.method public synthetic lambda$onPreExecute$0$TrainDetailsActivity$QueryTrainAsyncTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 793
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 758
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 798
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 799
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 800
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    const-string v2, "\u9519\u8bef"

    .line 807
    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 808
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$300(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    .line 810
    :cond_1
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$400(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)Lcom/lltskb/lltskb/view/widget/XListView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/widget/XListView;->stopRefresh()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 784
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 786
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0d021b

    const/4 v2, -0x1

    .line 791
    new-instance v3, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$QueryTrainAsyncTask$bXmWYhNts3ikI84mrjoSX6y9SDw;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/online/view/-$$Lambda$TrainDetailsActivity$QueryTrainAsyncTask$bXmWYhNts3ikI84mrjoSX6y9SDw;-><init>(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    .line 768
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    if-nez v0, :cond_0

    return-void

    .line 772
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 773
    aget-object p1, p1, v1

    .line 775
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 776
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$200(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    goto :goto_0

    .line 777
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 778
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->access$300(Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;)V

    :cond_2
    :goto_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 758
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity$QueryTrainAsyncTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
