.class public Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
.super Landroid/widget/SimpleAdapter;
.source "ResultSimpleAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;
    }
.end annotation


# instance fields
.field mBinder:Landroid/widget/SimpleAdapter$ViewBinder;

.field private mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

.field private mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

.field private mSelectMode:Z

.field private mShowBookButton:Z

.field private mShowPrice:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILcom/lltskb/lltskb/engine/online/BaseQuery;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[I",
            "Lcom/lltskb/lltskb/engine/online/BaseQuery;",
            ")V"
        }
    .end annotation

    .line 144
    invoke-direct/range {p0 .. p5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    const/4 p1, 0x0

    .line 32
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mSelectMode:Z

    const/4 p2, 0x1

    .line 34
    iput-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowBookButton:Z

    .line 35
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowPrice:Z

    const/4 p1, 0x0

    .line 36
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    .line 149
    new-instance p1, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$1;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;)V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    .line 145
    iput-object p6, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    .line 146
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setViewBinder(Landroid/widget/SimpleAdapter$ViewBinder;)V

    return-void
.end method

.method private getFlightView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .line 65
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 68
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v1, 0x7f0b0041

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_1
    const p2, 0x7f0902f6

    .line 72
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->start:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\uff0d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->arrive:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f090274

    .line 75
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 76
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    const/16 v2, 0xa

    const/16 v3, 0x7c

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0902c1

    .line 79
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 80
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f090275

    .line 82
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->title:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0902c2

    .line 86
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;->price:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 91
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .line 25
    invoke-super {p0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v0

    .line 26
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isShowFlight()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p1, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isShowFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-direct {p0, p2, p3}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->getFlightView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->isShowFlight()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 p1, p1, -0x1

    .line 107
    :cond_1
    instance-of v0, p2, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;

    if-nez v0, :cond_2

    const/4 p2, 0x0

    .line 110
    :cond_2
    move-object v0, p2

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;

    const v1, 0x7f090062

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-nez p2, :cond_7

    .line 112
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 113
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_4

    .line 115
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowBookButton:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    const p3, 0x7f090007

    .line 118
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_6

    .line 120
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowPrice:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 122
    :cond_6
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {v0, p0, p3, p2}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;-><init>(Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;Landroid/content/Context;Landroid/view/View;)V

    goto :goto_2

    .line 124
    :cond_7
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->getView()Landroid/view/View;

    move-result-object p2

    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 127
    :goto_2
    iget-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mSelectMode:Z

    if-eqz p2, :cond_8

    .line 128
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->isSelect(I)Z

    move-result p1

    invoke-virtual {v0, v2, p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->showCheckBox(IZ)V

    goto :goto_3

    .line 131
    :cond_8
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->isSelect(I)Z

    move-result p2

    invoke-virtual {v0, v3, p2}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->showCheckBox(IZ)V

    .line 132
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_9

    .line 134
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 135
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    :goto_3
    return-object v0
.end method

.method public isSelectMode()Z
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mSelectMode:Z

    return v0
.end method

.method public isShowFlight()Z
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 204
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mQuery:Lcom/lltskb/lltskb/engine/online/BaseQuery;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/BaseQuery;->onOrderClicked(I)V

    return-void
.end method

.method public setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    return-void
.end method

.method public setSelectMode(Z)V
    .locals 0

    .line 56
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mSelectMode:Z

    .line 57
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setShowBookButton(Z)V
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowBookButton:Z

    return-void
.end method

.method public setShowPrice(Z)V
    .locals 0

    .line 43
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->mShowPrice:Z

    return-void
.end method
