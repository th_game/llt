.class public Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;
.super Ljava/lang/Object;
.source "TrainSearchImpl.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/online/ITrainSearch;


# static fields
.field private static final TAG:Ljava/lang/String; = "TrainSearchImpl"

.field private static trainSearch:Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;


# instance fields
.field private httpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->trainSearch:Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->httpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method


# virtual methods
.method public get()Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;
    .locals 1

    .line 16
    sget-object v0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->trainSearch:Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;

    return-object v0
.end method

.method public queryTrain(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;
    .locals 4

    .line 54
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const-string v0, "/"

    .line 58
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 63
    :cond_1
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v1, v0, v3

    .line 64
    invoke-virtual {p0, v1, p2}, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->search(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    .line 72
    iget-object p1, v1, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;->train_no:Ljava/lang/String;

    :cond_4
    const/4 v0, 0x1

    const-string v1, "-"

    .line 76
    invoke-static {p2, v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->ensureDate(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "leftTicketDTO.train_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&leftTicketDTO.train_date="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&rand_code="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 78
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->httpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v0, "https://kyfw.12306.cn/otn/queryTrainInfo/query"

    invoke-virtual {p2, v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 79
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;

    move-result-object p1

    return-object p1
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;
    .locals 8

    const/4 v0, 0x0

    const-string v1, "-"

    .line 27
    invoke-static {p2, v1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->ensureDate(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    .line 28
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->httpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyword="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&date="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "https://search.12306.cn/search/v1/train/search"

    invoke-virtual {v1, v5, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;->parseResult(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;

    move-result-object v1

    .line 30
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v6, 0x2

    if-ge v2, v6, :cond_0

    return-object v1

    .line 33
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v6, 0xc

    if-ne v2, v6, :cond_1

    if-nez v1, :cond_1

    .line 34
    new-instance p2, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;

    invoke-direct {p2}, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;-><init>()V

    .line 35
    iput-object p1, p2, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;->train_no:Ljava/lang/String;

    return-object p2

    .line 39
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v6, 0x1

    sub-int/2addr v2, v6

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-nez v1, :cond_2

    .line 40
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v6, :cond_2

    .line 41
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;->httpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v5, v7}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;->parseResult(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TrainSearchDTO;

    move-result-object v1

    .line 43
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v7, v6

    invoke-virtual {v2, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    return-object v1
.end method
