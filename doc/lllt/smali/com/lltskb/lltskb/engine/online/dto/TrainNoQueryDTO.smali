.class public Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
.super Ljava/lang/Object;
.source "TrainNoQueryDTO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;
    }
.end annotation


# instance fields
.field public arrive_time:Ljava/lang/String;

.field public end_station_name:Ljava/lang/String;

.field public isEnabled:Z

.field public service_type:Ljava/lang/String;

.field public start_station_name:Ljava/lang/String;

.field public start_time:Ljava/lang/String;

.field public start_train_date:Ljava/lang/String;

.field public station_list:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO$StopStationDTO;",
            ">;"
        }
    .end annotation
.end field

.field public station_name:Ljava/lang/String;

.field public station_no:Ljava/lang/String;

.field public station_train_code:Ljava/lang/String;

.field public stopover_time:Ljava/lang/String;

.field public train_class_name:Ljava/lang/String;

.field public train_no:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 36
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/TrainNoQueryDTO;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 38
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
