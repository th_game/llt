.class public Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "PassCodeImageQuery.java"


# static fields
.field public static final LOGIN_MODEL:I = 0x1

.field public static final ORDER_MODEL:I = 0x2

.field public static final OTHER_MODEL:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PassCodeImageQuery"


# instance fields
.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mModel:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 26
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 27
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    .line 28
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    return-void
.end method

.method private captchaCheck(Ljava/lang/String;)Z
    .locals 5

    .line 168
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "randp"

    goto :goto_0

    :cond_0
    const-string v0, "sjrand"

    .line 173
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/passport/captcha/captcha-check"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "answer="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&login_site=E&rand="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "captcha-check="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassCodeImageQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->parseCheckCaptchaResult(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 175
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method private getCaptchaImage()Landroid/graphics/Bitmap;
    .locals 4

    .line 86
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "passenger"

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const-string v0, "other"

    goto :goto_0

    :cond_1
    const-string v0, "login"

    .line 92
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://kyfw.12306.cn/passport/captcha/captcha-image?login_site=E&module="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&rand="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "sjrand"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getCaptcha(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 96
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private parseCaptchaImage64(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .line 124
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 128
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "image"

    .line 129
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 132
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    .line 133
    array-length v2, p1

    invoke-static {p1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 136
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v1
.end method

.method private parseCheckCaptchaResult(Ljava/lang/String;)Z
    .locals 2

    .line 186
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 189
    :cond_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 191
    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 192
    instance-of v0, p1, Lorg/json/JSONObject;

    if-nez v0, :cond_1

    goto :goto_0

    .line 195
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v0, "result_code"

    .line 196
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1

    :catch_0
    move-exception p1

    .line 200
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v1
.end method


# virtual methods
.method public captchaCheck64(Ljava/lang/String;)Z
    .locals 5

    .line 144
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "randp"

    goto :goto_0

    :cond_0
    const-string v0, "sjrand"

    .line 149
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/passport/captcha/captcha-check"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "answer="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&login_site=E&rand="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "captcha-check="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassCodeImageQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->parseCheckCaptchaResult(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 151
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public checkRandCode(Ljava/lang/String;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 32
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "uamtk"

    .line 35
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    if-ne v0, v1, :cond_1

    .line 36
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->captchaCheck(Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 41
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const-string v0, "randp"

    goto :goto_0

    :cond_2
    const-string v0, "sjrand"

    .line 46
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKRANDCODEANSYN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "randCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&rand="

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkRandCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "PassCodeImageQuery"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 53
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, "\"result\":\"1\""

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    return v1

    :catch_0
    move-exception p1

    .line 48
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 50
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCaptchaImage64()Landroid/graphics/Bitmap;
    .locals 4

    .line 106
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "passenger"

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const-string v0, "other"

    goto :goto_0

    :cond_1
    const-string v0, "login"

    .line 112
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://kyfw.12306.cn/passport/captcha/captcha-image64?login_site=E&module="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&rand="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "sjrand"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->parseCaptchaImage64(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 117
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 4

    const-string v0, "uamtk"

    .line 57
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->getCaptchaImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 64
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const-string v0, "randp"

    goto :goto_0

    :cond_1
    const-string v0, "sjrand"

    .line 68
    :goto_0
    iget v2, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mModel:I

    if-ne v2, v1, :cond_2

    const-string v1, "passenger"

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    if-ne v2, v1, :cond_3

    const-string v1, "other"

    goto :goto_1

    :cond_3
    const-string v1, "login"

    .line 73
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&rand="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassCodeImageQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getCaptcha(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 77
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method
