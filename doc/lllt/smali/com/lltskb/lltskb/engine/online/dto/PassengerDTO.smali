.class public Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;
.source "PassengerDTO.java"


# instance fields
.field public address:Ljava/lang/String;

.field public allEncStr:Ljava/lang/String;

.field public born_date:Ljava/lang/String;

.field public child_ticket_num:I

.field public country_code:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public isSelected:Z

.field public isUserSelf:Ljava/lang/String;

.field public mobile_no:Ljava/lang/String;

.field public old_passenger_id_no:Ljava/lang/String;

.field public old_passenger_id_type_code:Ljava/lang/String;

.field public old_passenger_name:Ljava/lang/String;

.field public passenger_id_no:Ljava/lang/String;

.field public passenger_id_type_code:Ljava/lang/String;

.field public passenger_id_type_name:Ljava/lang/String;

.field public passenger_name:Ljava/lang/String;

.field public passenger_type:Ljava/lang/String;

.field public passenger_type_name:Ljava/lang/String;

.field public passenger_type_name_real:Ljava/lang/String;

.field public passenger_type_real:Ljava/lang/String;

.field public phone_no:Ljava/lang/String;

.field public postalcode:Ljava/lang/String;

.field public seat_type:I

.field public sex_code:Ljava/lang/String;

.field public status:I

.field public studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

.field public total_times:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/BaseDTO;-><init>()V

    const-string v0, "CN"

    .line 30
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->country_code:Ljava/lang/String;

    const/4 v0, 0x0

    .line 70
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    return-void
.end method


# virtual methods
.method public calc_status()V
    .locals 7

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 90
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto/16 :goto_5

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v2, "95"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v3, "97"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_4

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v3, "93"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "1"

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v4, "99"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_3

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v2, "94"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v2, "96"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_2

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v1, "92"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "G"

    const-string v2, "C"

    const-string v3, "H"

    const-string v4, "B"

    const/4 v5, 0x4

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v6, "98"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v6, "91"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 114
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 116
    :cond_5
    iput v5, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 119
    :cond_6
    iput v5, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 107
    :cond_7
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_1

    :cond_8
    const/4 v0, 0x3

    .line 111
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 109
    :cond_9
    :goto_1
    iput v5, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 100
    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 101
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 103
    :cond_b
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 94
    :cond_c
    :goto_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 95
    iput v2, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    :cond_d
    const/4 v0, 0x2

    .line 97
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    goto :goto_5

    .line 92
    :cond_e
    :goto_4
    iput v2, p0, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->status:I

    :cond_f
    :goto_5
    return-void
.end method

.method public getStatusString(I)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    const-string v1, "\u5f85\u6838\u9a8c"

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    return-object v1

    :cond_0
    const-string p1, "\u8bf7\u62a5\u9a8c"

    return-object p1

    :cond_1
    return-object v1

    :cond_2
    const-string p1, "\u9884\u901a\u8fc7"

    return-object p1

    :cond_3
    const-string p1, "\u672a\u901a\u8fc7"

    return-object p1

    :cond_4
    const-string p1, "\u5df2\u901a\u8fc7"

    return-object p1
.end method
