.class public Lcom/lltskb/lltskb/engine/online/ModelFactory;
.super Ljava/lang/Object;
.source "ModelFactory.java"


# static fields
.field private static modelFactory:Lcom/lltskb/lltskb/engine/online/ModelFactory;


# instance fields
.field private houbuTicketModel:Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

.field private infoQueryModel:Lcom/lltskb/lltskb/engine/online/IInfoQueryModel;

.field private ticketPriceModel:Lcom/lltskb/lltskb/engine/online/ITicketPriceModel;

.field private trainSearch:Lcom/lltskb/lltskb/engine/online/ITrainSearch;

.field private userQuery:Lcom/lltskb/lltskb/engine/online/IUserQuery;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/ModelFactory;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/ModelFactory;

    monitor-enter v0

    .line 12
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/ModelFactory;->modelFactory:Lcom/lltskb/lltskb/engine/online/ModelFactory;

    if-nez v1, :cond_0

    .line 13
    new-instance v1, Lcom/lltskb/lltskb/engine/online/ModelFactory;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/ModelFactory;->modelFactory:Lcom/lltskb/lltskb/engine/online/ModelFactory;

    .line 15
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/ModelFactory;->modelFactory:Lcom/lltskb/lltskb/engine/online/ModelFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public declared-synchronized getHoubuTicketModel()Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;
    .locals 1

    monitor-enter p0

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->houbuTicketModel:Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/HoubuTicketImpl;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/HoubuTicketImpl;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->houbuTicketModel:Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->houbuTicketModel:Lcom/lltskb/lltskb/engine/online/IHoubuTicketModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInfoQueryModel()Lcom/lltskb/lltskb/engine/online/IInfoQueryModel;
    .locals 1

    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->infoQueryModel:Lcom/lltskb/lltskb/engine/online/IInfoQueryModel;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/InfoQueryModel;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/InfoQueryModel;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->infoQueryModel:Lcom/lltskb/lltskb/engine/online/IInfoQueryModel;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->infoQueryModel:Lcom/lltskb/lltskb/engine/online/IInfoQueryModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTicketPriceModel()Lcom/lltskb/lltskb/engine/online/ITicketPriceModel;
    .locals 1

    monitor-enter p0

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->ticketPriceModel:Lcom/lltskb/lltskb/engine/online/ITicketPriceModel;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/TicketPriceModel;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/TicketPriceModel;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->ticketPriceModel:Lcom/lltskb/lltskb/engine/online/ITicketPriceModel;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->ticketPriceModel:Lcom/lltskb/lltskb/engine/online/ITicketPriceModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTrainSearchModel()Lcom/lltskb/lltskb/engine/online/ITrainSearch;
    .locals 1

    monitor-enter p0

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->trainSearch:Lcom/lltskb/lltskb/engine/online/ITrainSearch;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/TrainSearchImpl;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->trainSearch:Lcom/lltskb/lltskb/engine/online/ITrainSearch;

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->trainSearch:Lcom/lltskb/lltskb/engine/online/ITrainSearch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;
    .locals 1

    monitor-enter p0

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->userQuery:Lcom/lltskb/lltskb/engine/online/IUserQuery;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->userQuery:Lcom/lltskb/lltskb/engine/online/IUserQuery;

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ModelFactory;->userQuery:Lcom/lltskb/lltskb/engine/online/IUserQuery;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
