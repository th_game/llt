.class Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;
.super Ljava/lang/Thread;
.source "TrainZwdQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ZwdThread"
.end annotation


# instance fields
.field mRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mStation:Ljava/lang/String;

.field mTrain:Ljava/lang/String;

.field mType:Ljava/lang/String;

.field mYzm:Ljava/lang/String;

.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 219
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 220
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    .line 221
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mTrain:Ljava/lang/String;

    .line 222
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mStation:Ljava/lang/String;

    .line 223
    iput-object p5, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mYzm:Ljava/lang/String;

    .line 224
    iput-object p6, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 229
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    const-string v1, "ticket"

    const-string v2, "\u6b63\u5728\u67e5\u8be2..."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->access$000(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;)V

    .line 231
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d00f8

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 233
    :try_start_0
    new-instance v2, Lcom/lltskb/lltskb/engine/online/ZWDQuery;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;-><init>()V

    const/4 v3, 0x3

    move-object v3, v0

    const/4 v4, 0x3

    .line 236
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-ltz v4, :cond_0

    .line 237
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mStation:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mTrain:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mYzm:Ljava/lang/String;

    iget-object v7, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3, v5, v6, v7}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;->query(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 243
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 244
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;

    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->access$000(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;)V

    return-void
.end method
