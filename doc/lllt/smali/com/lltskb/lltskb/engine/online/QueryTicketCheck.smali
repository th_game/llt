.class public Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "QueryTicketCheck.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryTicketCheck"

.field private static instance:Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;


# instance fields
.field private mErrMsg:Ljava/lang/String;

.field private mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->instance:Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 25
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;
    .locals 1

    .line 21
    sget-object v0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->instance:Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;

    return-object v0
.end method

.method private parseTicketCheckResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 58
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 63
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "data"

    .line 64
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 66
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mErrMsg:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public getErrMsg()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mErrMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketCheck(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 36
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 37
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 41
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    .line 42
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mErrMsg:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trainDate="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&station_train_code="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&from_station_telecode="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 46
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string p3, "https://www.12306.cn/index/otn/index12306/queryTicketCheck"

    invoke-virtual {p2, p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "getTicketCheck ret="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "QueryTicketCheck"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->parseTicketCheckResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 48
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 49
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/QueryTicketCheck;->mErrMsg:Ljava/lang/String;

    return-object v1
.end method
