.class public Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;
.super Ljava/lang/Object;
.source "SearchTicketQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchTicketQuery"

.field public static mCURL:Ljava/lang/String;

.field private static mLastQueryTime:J

.field public static mOtherMaxdate:Ljava/lang/String;

.field private static mOtherMindate:Ljava/lang/String;

.field public static mStudentMaxDate:Ljava/lang/String;

.field private static mStudentMinDate:Ljava/lang/String;


# instance fields
.field private mDate:Ljava/lang/String;

.field private mErrorCode:I

.field private mErrorMsg:Ljava/lang/String;

.field private mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    .line 49
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mResult:Ljava/util/Vector;

    .line 44
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method private static calcMaxDate()V
    .locals 10

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calcMaxDate stu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mStudentMaxDate:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",other="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mOtherMaxdate:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "SearchTicketQuery"

    invoke-static {v3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mStudentMaxDate:Ljava/lang/String;

    const-string v4, "yyyy-MM-dd"

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->str2Date(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 142
    sget-object v5, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mOtherMaxdate:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->str2Date(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 143
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 144
    invoke-static {v5, v0}, Lcom/lltskb/lltskb/utils/DateUtils;->getDayDiff(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v6

    .line 145
    invoke-static {v5, v4}, Lcom/lltskb/lltskb/utils/DateUtils;->getDayDiff(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    .line 147
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/lltskb/lltskb/engine/LltSettings;->setMaxStudentDays(J)V

    .line 148
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/lltskb/lltskb/engine/LltSettings;->setMaxOtherDays(J)V

    .line 150
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 58
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 61
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-lez p1, :cond_1

    add-int/lit8 p1, p1, 0x1

    const-string v0, "\'"

    .line 63
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    .line 64
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    .line 66
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    return-object v1
.end method

.method public static initCURL()Z
    .locals 8

    const-string v0, "SearchTicketQuery"

    .line 89
    sget-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 92
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v1

    const-string v3, "https://kyfw.12306.cn/otn/leftTicket/init"

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/utils/JSEngine;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 94
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :goto_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v3, 0x3

    const/4 v4, 0x0

    .line 103
    :try_start_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result v5

    if-nez v5, :cond_1

    .line 104
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v5

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->clearCookie()V

    :cond_1
    const/4 v5, 0x0

    move-object v3, v5

    const/4 v6, 0x3

    .line 106
    :goto_1
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    if-lez v6, :cond_2

    .line 107
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v3

    invoke-virtual {v3, v1, v5}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 112
    :cond_2
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 113
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-direct {v1, v3}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    const-string v1, "Y"

    const-string v5, "isSaveQueryLog"

    .line 116
    invoke-static {v3, v5}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 118
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSaveQueryLog="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "CLeftTicketUrl"

    .line 119
    invoke-static {v3, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CLeftTicketUrl="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "studentMindate"

    .line 121
    invoke-static {v3, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mStudentMinDate:Ljava/lang/String;

    const-string v1, "studentMaxdate"

    .line 122
    invoke-static {v3, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mStudentMaxDate:Ljava/lang/String;

    const-string v1, "otherMindate"

    .line 123
    invoke-static {v3, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mOtherMindate:Ljava/lang/String;

    const-string v1, "otherMaxdate"

    .line 124
    invoke-static {v3, v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mOtherMaxdate:Ljava/lang/String;

    .line 125
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->calcMaxDate()V

    .line 128
    sget-object v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    xor-int/2addr v0, v2

    return v0

    :catch_1
    move-exception v1

    .line 131
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initCURL="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return v4
.end method

.method private parseQueryResult(Ljava/lang/String;Ljava/util/Vector;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "messages"

    const-string v1, "c_url"

    const-string v2, "httpstatus"

    const-string v3, "data"

    .line 578
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseQueryResult ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SearchTicketQuery"

    invoke-static {v5, v4}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v4, 0x7f0d00f8

    const/4 v6, -0x3

    if-eqz p1, :cond_c

    .line 579
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x6

    if-ge v7, v8, :cond_0

    goto/16 :goto_5

    :cond_0
    const-string v7, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    .line 584
    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x2

    const/4 v9, 0x0

    if-lez v7, :cond_1

    .line 585
    sput-object v9, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    .line 586
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->initCURL()Z

    return v8

    .line 590
    :cond_1
    :try_start_0
    new-instance v7, Lorg/json/JSONTokener;

    invoke-direct {v7, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 594
    invoke-virtual {v7}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_b

    .line 595
    instance-of v7, p1, Lorg/json/JSONObject;

    if-nez v7, :cond_2

    goto/16 :goto_3

    .line 599
    :cond_2
    check-cast p1, Lorg/json/JSONObject;

    .line 600
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 601
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 602
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    .line 603
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "parseQueryResult new c_url = "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v5, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v8

    .line 607
    :cond_3
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_4

    .line 609
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    .line 612
    :cond_4
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 613
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 614
    instance-of v0, v0, Lorg/json/JSONArray;

    if-eqz v0, :cond_5

    .line 615
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    goto :goto_1

    .line 617
    :cond_5
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "message"

    .line 618
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    .line 621
    :cond_6
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 622
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    const-string v0, ""

    .line 623
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 625
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 626
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 628
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    .line 631
    :cond_8
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010b

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    :goto_1
    if-nez v9, :cond_9

    return v6

    :cond_9
    const/4 p1, 0x0

    .line 637
    :goto_2
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_a

    .line 638
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 639
    invoke-virtual {v9, p1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v3, "queryLeftNewDTO"

    .line 640
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 641
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mDate:Ljava/lang/String;

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    .line 642
    invoke-static {}, Lcom/lltskb/lltskb/utils/StringUtils;->getToday()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->back_take_date:Ljava/lang/String;

    const-string v4, "train_no"

    .line 643
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const-string v4, "station_train_code"

    .line 644
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const-string v4, "start_station_telecode"

    .line 646
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const-string v4, "start_station_name"

    .line 647
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    const-string v4, "end_station_telecode"

    .line 649
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    const-string v4, "end_station_name"

    .line 650
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    const-string v4, "from_station_telecode"

    .line 652
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const-string v4, "from_station_name"

    .line 653
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    const-string v4, "to_station_telecode"

    .line 655
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const-string v4, "to_station_name"

    .line 656
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    const-string v4, "start_time"

    .line 657
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v4, "arrive_time"

    .line 658
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const-string v4, "day_difference"

    .line 659
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->day_difference:Ljava/lang/String;

    const-string v4, "train_class_name"

    .line 660
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_class_name:Ljava/lang/String;

    const-string v4, "lishi"

    .line 661
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const-string v4, "canWebBuy"

    .line 662
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const-string v4, "lishiValue"

    .line 663
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishiValue:Ljava/lang/String;

    const-string v4, "yp_info"

    .line 664
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v4, "control_train_day"

    .line 665
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_train_day:Ljava/lang/String;

    const-string v4, "start_train_date"

    .line 666
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const-string v4, "seat_feature"

    .line 667
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_feature:Ljava/lang/String;

    const-string v4, "yp_ex"

    .line 668
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const-string v4, "train_seat_feature"

    .line 669
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const-string v4, "seat_types"

    .line 670
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    const-string v4, "location_code"

    .line 671
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const-string v4, "from_station_no"

    .line 672
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const-string v4, "to_station_no"

    .line 673
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const-string v4, "control_day"

    .line 674
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->control_day:Ljava/lang/String;

    const-string v4, "sale_time"

    .line 675
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->sale_time:Ljava/lang/String;

    const-string v4, "is_support_card"

    .line 676
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const-string v4, "controlled_train_flag"

    .line 677
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const-string v4, "controlled_train_message"

    .line 678
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    const-string v4, "note"

    .line 679
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->note:Ljava/lang/String;

    const-string v4, "gg_num"

    .line 682
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const-string v4, "gr_num"

    .line 683
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const-string v4, "qt_num"

    .line 684
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const-string v4, "rw_num"

    .line 685
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const-string v4, "rz_num"

    .line 686
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const-string v4, "tz_num"

    .line 687
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const-string v4, "wz_num"

    .line 688
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const-string v4, "yb_num"

    .line 689
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const-string v4, "yw_num"

    .line 690
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const-string v4, "yz_num"

    .line 691
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v4, "ze_num"

    .line 692
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const-string v4, "zy_num"

    .line 693
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const-string v4, "swz_num"

    .line 694
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const-string v3, "secretStr"

    .line 696
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const-string v3, "buttonTextInfo"

    .line 697
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    .line 704
    invoke-virtual {p2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_2

    :cond_a
    return v2

    :cond_b
    :goto_3
    const/4 p1, -0x1

    return p1

    :catch_0
    move-exception p1

    .line 712
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :catch_1
    move-exception p1

    .line 709
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "exception="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v5, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_4
    return v6

    .line 580
    :cond_c
    :goto_5
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v6
.end method

.method private parseQueryResultNew(Ljava/lang/String;Ljava/util/Vector;)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "error_msg"

    const-string v1, "messages"

    const-string v2, "data"

    const-string v3, "c_url"

    const-string v4, "httpstatus"

    .line 306
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseQueryResultNew ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "SearchTicketQuery"

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v5, 0x7f0d00f8

    const/4 v7, -0x2

    if-eqz p1, :cond_22

    .line 307
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x6

    if-ge v8, v9, :cond_0

    goto/16 :goto_14

    :cond_0
    const-string v8, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    .line 312
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-gtz v8, :cond_21

    const-string v8, "<"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    goto/16 :goto_13

    :cond_1
    const/4 v8, -0x3

    .line 318
    :try_start_0
    new-instance v10, Lorg/json/JSONTokener;

    invoke-direct {v10, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 322
    invoke-virtual {v10}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    const/4 v10, -0x1

    if-eqz p1, :cond_20

    .line 323
    instance-of v11, p1, Lorg/json/JSONObject;

    if-nez v11, :cond_2

    goto/16 :goto_11

    .line 327
    :cond_2
    check-cast p1, Lorg/json/JSONObject;

    .line 328
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 329
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 330
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    .line 331
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "parseQueryResult new c_url = "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v6, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v7

    .line 335
    :cond_3
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_4

    .line 337
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    :cond_4
    const/4 v3, 0x0

    .line 340
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_6

    .line 341
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "result"

    if-eqz v0, :cond_5

    .line 343
    :try_start_1
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    if-nez v3, :cond_a

    const-string p1, "\u683c\u5f0f\u9519\u8bef"

    .line 345
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v8

    .line 349
    :cond_5
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "message"

    .line 350
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    .line 353
    :cond_6
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 354
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    const-string v0, ""

    .line 355
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 357
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 358
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 360
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    .line 362
    :cond_8
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 363
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    goto :goto_1

    .line 366
    :cond_9
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010b

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    :cond_a
    :goto_1
    if-nez v3, :cond_b

    return v8

    :cond_b
    const/4 p1, 0x0

    .line 372
    :goto_2
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_1f

    .line 373
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    .line 374
    invoke-virtual {v3, p1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_c

    goto/16 :goto_10

    :cond_c
    const-string v2, "\\|"

    .line 377
    invoke-static {v1, v2, v10}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 379
    array-length v2, v1

    const/16 v4, 0x24

    if-ge v2, v4, :cond_d

    goto/16 :goto_10

    .line 433
    :cond_d
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mDate:Ljava/lang/String;

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    .line 434
    invoke-static {}, Lcom/lltskb/lltskb/utils/StringUtils;->getToday()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->back_take_date:Ljava/lang/String;

    .line 442
    aget-object v2, v1, v5

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    const/4 v2, 0x1

    .line 443
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    const/4 v2, 0x2

    .line 444
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    const/4 v2, 0x3

    .line 445
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    const/4 v2, 0x4

    .line 446
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    const/4 v2, 0x5

    .line 447
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    .line 448
    aget-object v2, v1, v9

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    const/4 v2, 0x7

    .line 449
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    const/16 v2, 0x8

    .line 450
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const/16 v2, 0x9

    .line 451
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    const/16 v2, 0xa

    .line 452
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    const/16 v2, 0xb

    .line 453
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->canWebBuy:Ljava/lang/String;

    const/16 v2, 0xc

    .line 454
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const/16 v2, 0xd

    .line 455
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_train_date:Ljava/lang/String;

    const/16 v2, 0xe

    .line 456
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_seat_feature:Ljava/lang/String;

    const/16 v2, 0xf

    .line 457
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->location_code:Ljava/lang/String;

    const/16 v2, 0x10

    .line 458
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_no:Ljava/lang/String;

    const/16 v2, 0x11

    .line 459
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_no:Ljava/lang/String;

    const/16 v2, 0x12

    .line 460
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->is_support_card:Ljava/lang/String;

    const/16 v2, 0x13

    .line 461
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_flag:Ljava/lang/String;

    const/16 v2, 0x14

    .line 462
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v4, "--"

    if-eqz v2, :cond_e

    const/16 v2, 0x14

    :try_start_2
    aget-object v2, v1, v2

    goto :goto_3

    :cond_e
    move-object v2, v4

    :goto_3
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gg_num:Ljava/lang/String;

    const/16 v2, 0x15

    .line 463
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x15

    aget-object v2, v1, v2

    goto :goto_4

    :cond_f
    move-object v2, v4

    :goto_4
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    const/16 v2, 0x16

    .line 464
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x16

    aget-object v2, v1, v2

    goto :goto_5

    :cond_10
    move-object v2, v4

    :goto_5
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->qt_num:Ljava/lang/String;

    const/16 v2, 0x17

    .line 465
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x17

    aget-object v2, v1, v2

    goto :goto_6

    :cond_11
    move-object v2, v4

    :goto_6
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    const/16 v2, 0x18

    .line 466
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x18

    aget-object v2, v1, v2

    goto :goto_7

    :cond_12
    move-object v2, v4

    :goto_7
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    const/16 v2, 0x19

    .line 467
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x19

    aget-object v2, v1, v2

    goto :goto_8

    :cond_13
    move-object v2, v4

    :goto_8
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    const/16 v2, 0x1a

    .line 468
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x1a

    aget-object v2, v1, v2

    goto :goto_9

    :cond_14
    move-object v2, v4

    :goto_9
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->wz_num:Ljava/lang/String;

    const/16 v2, 0x1b

    .line 469
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x1b

    aget-object v2, v1, v2

    goto :goto_a

    :cond_15
    move-object v2, v4

    :goto_a
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yb_num:Ljava/lang/String;

    const/16 v2, 0x1c

    .line 470
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x1c

    aget-object v2, v1, v2

    goto :goto_b

    :cond_16
    move-object v2, v4

    :goto_b
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    const/16 v2, 0x1d

    .line 471
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x1d

    aget-object v2, v1, v2

    goto :goto_c

    :cond_17
    move-object v2, v4

    :goto_c
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const/16 v2, 0x1e

    .line 472
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x1e

    aget-object v2, v1, v2

    goto :goto_d

    :cond_18
    move-object v2, v4

    :goto_d
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    const/16 v2, 0x1f

    .line 473
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x1f

    aget-object v2, v1, v2

    goto :goto_e

    :cond_19
    move-object v2, v4

    :goto_e
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    const/16 v2, 0x20

    .line 474
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x20

    aget-object v2, v1, v2

    goto :goto_f

    :cond_1a
    move-object v2, v4

    :goto_f
    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    const/16 v2, 0x21

    .line 475
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v2, 0x21

    aget-object v4, v1, v2

    :cond_1b
    iput-object v4, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->srrb_num:Ljava/lang/String;

    const/16 v2, 0x22

    .line 476
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_ex:Ljava/lang/String;

    const/16 v2, 0x23

    .line 478
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    .line 480
    array-length v2, v1

    const/16 v4, 0x26

    if-le v2, v4, :cond_1c

    const/16 v2, 0x24

    .line 481
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->exchange_train_flag:Ljava/lang/String;

    const/16 v2, 0x25

    .line 482
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_train_flag:Ljava/lang/String;

    const/16 v2, 0x26

    .line 483
    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->houbu_seat_limit:Ljava/lang/String;

    .line 486
    :cond_1c
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    aget-object v4, v1, v9

    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    .line 487
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    const/4 v4, 0x7

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    .line 488
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_telecode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    .line 489
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_telecode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->end_station_name:Ljava/lang/String;

    .line 492
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    const-string v1, "\u9884\u8ba2"

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 493
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->controlled_train_message:Ljava/lang/String;

    .line 495
    :cond_1d
    invoke-virtual {p2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1e
    :goto_10
    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_2

    :cond_1f
    return v5

    :cond_20
    :goto_11
    return v10

    :catch_0
    move-exception p1

    .line 503
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    :catch_1
    move-exception p1

    .line 500
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "exception="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v6, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_12
    return v8

    :cond_21
    :goto_13
    return v7

    .line 308
    :cond_22
    :goto_14
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    return v7
.end method


# virtual methods
.method public QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "utf-8"

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mLastQueryTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xbb8

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    sub-long v1, v3, v1

    .line 174
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 176
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 179
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sput-wide v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mLastQueryTime:J

    .line 180
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mResult:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    const/4 v1, -0x3

    .line 181
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorCode:I

    .line 182
    invoke-static {p3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v5, -0x7

    const/4 v6, 0x0

    if-nez v2, :cond_1

    .line 183
    invoke-static {}, Lcom/lltskb/lltskb/utils/StringUtils;->getToday()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    .line 184
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const p2, 0x7f0d00e9

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    .line 185
    iput v5, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorCode:I

    return v6

    .line 190
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->initCURL()Z

    .line 192
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 195
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    const v9, 0x7f0d0130

    const/4 v10, 0x1

    if-nez v8, :cond_c

    .line 201
    invoke-static {v7}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_b

    .line 213
    :try_start_1
    invoke-static {p3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    .line 214
    :try_start_2
    invoke-static {p4, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p2

    goto :goto_1

    :catch_2
    move-exception p2

    move-object p1, p3

    .line 216
    :goto_1
    invoke-virtual {p2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 219
    :goto_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "leftTicketDTO.train_date="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&leftTicketDTO.from_station="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&leftTicketDTO.to_station="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&purpose_codes="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 224
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "QueryLeftTicket url="

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p4, "SearchTicketQuery"

    invoke-static {p4, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mDate:Ljava/lang/String;

    const/4 p2, 0x0

    :goto_3
    const/4 p3, 0x2

    if-ge p2, p3, :cond_a

    .line 231
    sget-object p3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    if-nez p3, :cond_2

    .line 232
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->initCURL()Z

    .line 235
    :cond_2
    sget-object p3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    if-nez p3, :cond_3

    const-string p3, "get mCURL failed, reset to default"

    .line 236
    invoke-static {p4, p3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string p3, "leftTicket/queryT"

    .line 237
    sput-object p3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    .line 241
    :cond_3
    sget-object p3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_ORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 242
    sget-object v0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->setPath(Ljava/lang/String;)V

    .line 244
    :try_start_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mHttpClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    invoke-virtual {v0, p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 246
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mResult:Ljava/util/Vector;

    invoke-direct {p0, p3, v0}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->parseQueryResultNew(Ljava/lang/String;Ljava/util/Vector;)I

    move-result v0

    .line 247
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorCode:I

    if-eqz v0, :cond_4

    .line 249
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "QueryTicket response="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p4, p3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 p3, -0x2

    if-ne p3, v0, :cond_5

    add-int/lit8 p2, p2, 0x1

    .line 254
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    const/4 p3, 0x0

    .line 255
    sput-object p3, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_7

    if-eq v1, v0, :cond_7

    if-ne v5, v0, :cond_6

    goto :goto_4

    :cond_6
    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    :cond_7
    :goto_4
    if-nez v0, :cond_8

    const/4 v6, 0x1

    :cond_8
    return v6

    :catch_3
    move-exception p1

    .line 262
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 264
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "QueryTicket exception="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p4, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iput v1, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorCode:I

    .line 266
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_9

    .line 267
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 268
    :cond_9
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    return v6

    .line 202
    :cond_b
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 203
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    new-array p4, v10, [Ljava/lang/Object;

    aput-object p2, p4, v6

    invoke-static {p3, p1, p4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 204
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 196
    :cond_c
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    invoke-virtual {p2, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 197
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    new-array p4, v10, [Ljava/lang/Object;

    aput-object p1, p4, v6

    invoke-static {p3, p2, p4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 198
    new-instance p2, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {p2, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :goto_5
    throw p2

    :goto_6
    goto :goto_5
.end method

.method public getErrorCode()I
    .locals 1

    .line 280
    iget v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorCode:I

    return v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mResult:Ljava/util/Vector;

    return-object v0
.end method
