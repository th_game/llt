.class public Lcom/lltskb/lltskb/engine/online/StZwdQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "StZwdQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "StZwdQuery"

.field private static inst:Lcom/lltskb/lltskb/engine/online/StZwdQuery;


# instance fields
.field private mData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mTrainCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 123
    new-instance v0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->inst:Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 155
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    .line 156
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/engine/online/StZwdQuery;
    .locals 1

    .line 138
    sget-object v0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->inst:Lcom/lltskb/lltskb/engine/online/StZwdQuery;

    return-object v0
.end method

.method private getUser()Lorg/json/JSONObject;
    .locals 4

    const-string v0, ""

    .line 227
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "mac"

    .line 229
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUtils;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "version"

    const-string v3, "1.4.4"

    .line 230
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "phone"

    .line 231
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "device"

    .line 232
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "system"

    .line 233
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 237
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 235
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-object v1
.end method

.method private parseQueryResult(Ljava/lang/String;)Z
    .locals 5

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "zwd result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StZwdQuery"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    .line 190
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-ge v1, v2, :cond_0

    goto :goto_3

    .line 193
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 199
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONArray;

    if-eqz p1, :cond_4

    .line 200
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    .line 204
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 205
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_2

    goto :goto_1

    .line 210
    :cond_2
    new-instance v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;-><init>(Lcom/lltskb/lltskb/engine/online/StZwdQuery;)V

    const-string v4, "arrive"

    .line 211
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->arrive:Ljava/lang/String;

    const-string v4, "expectArriveTime"

    .line 212
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->expectArriveTime:Ljava/lang/String;

    const-string v4, "message"

    .line 213
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->message:Ljava/lang/String;

    const-string v4, "station"

    .line 214
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->station:Ljava/lang/String;

    const-string v4, "order"

    .line 215
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->order:Ljava/lang/String;

    .line 216
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;->station:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 219
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez p1, :cond_4

    const/4 v0, 0x1

    :cond_4
    :goto_2
    return v0

    :catch_0
    move-exception p1

    .line 221
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_5
    :goto_3
    return v0
.end method


# virtual methods
.method public getZwdItem(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;
    .locals 1

    if-nez p1, :cond_0

    .line 147
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;

    return-object p1

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mTrainCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 150
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/StZwdQuery$ZwdItem;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public isZwdReady(Ljava/lang/String;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mTrainCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public queryDelayInfo(Ljava/lang/String;)Z
    .locals 4

    .line 160
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 165
    :cond_0
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mTrainCode:Ljava/lang/String;

    .line 167
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 168
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v3, "trainCode"

    .line 170
    invoke-virtual {v2, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "stationName"

    const-string v3, ""

    .line 171
    invoke-virtual {v2, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "info"

    .line 172
    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "user"

    .line 173
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->getUser()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 175
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://www.st12306.cn/AppService/queryTrainDelay"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v1, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 177
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/StZwdQuery;->parseQueryResult(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 182
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 180
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_1
    :goto_0
    return v0
.end method
