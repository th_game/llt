.class public Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;
.super Ljava/lang/Object;
.source "StudentInfoDTO.java"


# instance fields
.field public department:Ljava/lang/String;

.field public enter_year:Ljava/lang/String;

.field public perference_card_no:Ljava/lang/String;

.field public preference_from_station_code:Ljava/lang/String;

.field public preference_from_station_name:Ljava/lang/String;

.field public preference_to_station_code:Ljava/lang/String;

.field public preference_to_station_name:Ljava/lang/String;

.field public province_code:Ljava/lang/String;

.field public school_class:Ljava/lang/String;

.field public school_code:Ljava/lang/String;

.field public school_name:Ljava/lang/String;

.field public school_system:Ljava/lang/String;

.field public student_no:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 7
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_code:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->department:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_class:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->perference_card_no:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_code:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    .line 55
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_code:Ljava/lang/String;

    return-void
.end method
