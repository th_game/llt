.class public Lcom/lltskb/lltskb/engine/online/LoginModel;
.super Ljava/lang/Object;
.source "LoginModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LoginModel"

.field private static mInst:Lcom/lltskb/lltskb/engine/online/LoginModel;


# instance fields
.field private isstudentDate:Z

.field mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

.field private mErrorMsg:Ljava/lang/String;

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mIsSignIn:Z

.field private userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    .line 22
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->isstudentDate:Z

    .line 44
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/LoginModel;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/LoginModel;

    monitor-enter v0

    .line 27
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/LoginModel;->mInst:Lcom/lltskb/lltskb/engine/online/LoginModel;

    if-nez v1, :cond_0

    .line 28
    new-instance v1, Lcom/lltskb/lltskb/engine/online/LoginModel;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/LoginModel;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/LoginModel;->mInst:Lcom/lltskb/lltskb/engine/online/LoginModel;

    .line 30
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/LoginModel;->mInst:Lcom/lltskb/lltskb/engine/online/LoginModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private parseConf(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 75
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 78
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 80
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    .line 81
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    return v0

    .line 84
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "data"

    .line 85
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_2

    return v0

    :cond_2
    const-string v1, "isstudentDate"

    .line 90
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->isstudentDate:Z

    const-string v1, "name"

    .line 92
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->userName:Ljava/lang/String;

    const-string v1, "is_login"

    .line 93
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "Y"

    .line 94
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 97
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    :goto_0
    return v0
.end method

.method private parserLoginASynSuggestResult(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "data"

    const-string v1, ""

    const/4 v2, 0x0

    if-eqz p1, :cond_9

    .line 140
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 143
    :cond_0
    :try_start_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 145
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_1

    .line 149
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "httpstatus"

    .line 151
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_2

    return v2

    :cond_2
    const-string v3, "messages"

    .line 153
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONArray;

    .line 155
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mErrorMsg:Ljava/lang/String;

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    .line 157
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 158
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 160
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mErrorMsg:Ljava/lang/String;

    .line 162
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    return v2

    .line 165
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_5

    return v2

    :cond_5
    const-string v0, "loginCheck"

    .line 167
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_6

    return v2

    :cond_6
    const-string v0, "Y"

    .line 170
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    .line 171
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    if-eqz p1, :cond_7

    .line 172
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setSignKey(Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V

    .line 174
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    .line 176
    :cond_7
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :cond_8
    :goto_1
    return v2

    :catch_0
    move-exception p1

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LoginModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_9
    :goto_2
    return v2
.end method


# virtual methods
.method public conf()Z
    .locals 4

    const-string v0, "LoginModel"

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/otn/login/conf"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "conf="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->parseConf(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v1

    .line 55
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "userLogin failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public isSignin(Z)Z
    .locals 0

    if-nez p1, :cond_0

    .line 34
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    return p1

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/LoginModel;->conf()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    .line 36
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    return p1
.end method

.method public loginAysnSuggest(Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setAccount(Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 109
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    .line 112
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberPass(Z)V

    .line 113
    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p4, p3}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberName(Z)V

    .line 115
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "loginUserDTO.user_name="

    .line 116
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&userDTO.password="

    .line 117
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string p2, "https://kyfw.12306.cn/otn/login/loginAysnSuggest"

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "login="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "LoginModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->parserLoginASynSuggestResult(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 124
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 126
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 127
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public signOut()V
    .locals 1

    const/4 v0, 0x0

    .line 190
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/LoginModel;->mIsSignIn:Z

    return-void
.end method
