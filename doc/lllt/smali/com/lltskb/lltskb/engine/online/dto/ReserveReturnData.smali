.class public final Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J0\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001aJ\u0013\u0010\u001b\u001a\u00020\u00032\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0007H\u00d6\u0001R\"\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006 "
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;",
        "",
        "flag",
        "",
        "data",
        "",
        "msg",
        "",
        "(ZLjava/lang/Float;Ljava/lang/String;)V",
        "getData",
        "()Ljava/lang/Float;",
        "setData",
        "(Ljava/lang/Float;)V",
        "Ljava/lang/Float;",
        "getFlag",
        "()Z",
        "setFlag",
        "(Z)V",
        "getMsg",
        "()Ljava/lang/String;",
        "setMsg",
        "(Ljava/lang/String;)V",
        "component1",
        "component2",
        "component3",
        "copy",
        "(ZLjava/lang/Float;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private data:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field private flag:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "flag"
    .end annotation
.end field

.field private msg:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "msg"
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/lang/Float;Ljava/lang/String;)V
    .locals 0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;ZLjava/lang/Float;Ljava/lang/String;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->copy(ZLjava/lang/Float;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    return v0
.end method

.method public final component2()Ljava/lang/Float;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZLjava/lang/Float;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;
    .locals 1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;

    invoke-direct {v0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;-><init>(ZLjava/lang/Float;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    iget-boolean v3, p1, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public final getData()Ljava/lang/Float;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    return-object v0
.end method

.method public final getFlag()Z
    .locals 1

    .line 234
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    return v0
.end method

.method public final getMsg()Ljava/lang/String;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public final setData(Ljava/lang/Float;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    return-void
.end method

.method public final setFlag(Z)V
    .locals 0

    .line 234
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    return-void
.end method

.method public final setMsg(Ljava/lang/String;)V
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReserveReturnData(flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->flag:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->data:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/ReserveReturnData;->msg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
