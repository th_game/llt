.class public final enum Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;
.super Ljava/lang/Enum;
.source "UrlEnums.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum ALLCITYS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum BINDTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CANCEL_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CANCEL_QUEUE_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CHECKORDERINFO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CHECKRANDCODEANSYN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CHECKUSER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CHECK_MOBILE_CODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CHECK_USERNAME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CONFIRMSINGLE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CONFIRMSINGLEFORQUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum CONTINUE_PAY_NOCOMPLETEMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum DO_MAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum EDITTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum EMPTY_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GETPASSENGERDTOS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GETQUEUECOUNT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GET_ORDER_PERSON:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GET_ORDER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GET_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum GET_STATION_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum INITDC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum INITGC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum INIT_SUC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFTTICKET_LOG:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFT_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFT_TICKET_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFT_TICKET_ORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFT_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LEFT_TICKET_PRICE_PUBLIC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LOGIN_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LOGIN_KEY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LOGIN_LOGOUT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LOGIN_RANGCODE_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LOGIN_USER_ASYNC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum LONGIN_CONFIM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_ADD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_DEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_EDIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PASSENGER_SHOW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAYCHECK:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAYCHECKNEW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAYORDER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAY_FINISH:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAY_WAPBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum PAY_WEBBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERYMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_BY_TRAIN_NO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_MYORDER_NOCOMPLETE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_ORDER_WAITTIME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_STATION_TRAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum QUERY_TICKET_PRICEFL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum RESGIN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum RESULT_ORDER_FOR_DC_QUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum RETURN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum RETURN_TICKET_AFFIRM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum SCHOOL_NAMES:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum SEARCH_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum SUBDETAIL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum SUBMITINIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum SUBMITORDERREQUEST:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum TO_PAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum TO_WAPPAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum USER_LOGIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

.field public static final enum ZWD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;


# instance fields
.field private accept:Ljava/lang/String;

.field private contentType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private refer:Ljava/lang/String;

.field private xRequestWith:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 9
    new-instance v8, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v1, "DO_MAIN"

    const/4 v2, 0x0

    const-string v3, "https://kyfw.12306.cn/otn/"

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v8, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->DO_MAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 10
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LOGIN_INIT"

    const/4 v11, 0x1

    const-string v12, "login/init"

    const-string v13, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"

    const-string v14, "https://kyfw.12306.cn/otn/leftTicket/init"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 11
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LOGIN_KEY"

    const/4 v3, 0x2

    const-string v4, "dynamicJs/lwgotnx"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/login/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_KEY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 12
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LOGIN_RANGCODE_URL"

    const/4 v11, 0x3

    const-string v12, "passcodeNew/getPassCodeNew.do"

    const-string v13, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_RANGCODE_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 13
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LONGIN_CONFIM"

    const/4 v3, 0x4

    const-string v4, "login/loginAysnSuggest"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/login/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LONGIN_CONFIM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 16
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LOGIN_USER_ASYNC"

    const/4 v11, 0x5

    const-string v12, "login/loginUserAsyn"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/login/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_USER_ASYNC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 20
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "USER_LOGIN"

    const/4 v3, 0x6

    const-string v4, "login/userLogin"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/login/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->USER_LOGIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 22
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LOGIN_LOGOUT"

    const/4 v11, 0x7

    const-string v12, "login/loginOut"

    const-string v13, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"

    const-string v14, "https://kyfw.12306.cn/otn/index/init"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_LOGOUT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 24
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LEFT_TICKET_INIT"

    const/16 v3, 0x8

    const-string v4, "leftTicket/init"

    const-string v5, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"

    const-string v6, "https://kyfw.12306.cn/otn/index/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 25
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LEFT_TICKET_ORDER"

    const/16 v11, 0x9

    const-string v12, "leftTicket/query"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/login/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_ORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 26
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LEFT_TICKET"

    const/16 v3, 0xa

    const-string v4, "lcxxcx/query"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/login/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 27
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "LEFT_TICKET_PRICE"

    const/16 v11, 0xb

    const-string v12, "leftTicketPrice/query"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/login/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 28
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LEFT_TICKET_PRICE_PUBLIC"

    const/16 v3, 0xc

    const-string v4, "leftTicketPrice/queryAllPublicPrice"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/login/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE_PUBLIC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 30
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "QUERY_TICKET_PRICEFL"

    const/16 v11, 0xd

    const-string v12, "leftTicket/queryTicketPriceFL"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/leftTicket/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICEFL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 31
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "QUERY_TICKET_PRICE"

    const/16 v3, 0xe

    const-string v4, "leftTicket/queryTicketPrice"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/leftTicket/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 32
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "QUERY_BY_TRAIN_NO"

    const/16 v11, 0xf

    const-string v12, "czxx/queryByTrainNo"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/lcxxcx/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_BY_TRAIN_NO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 33
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "QUERY_STATION_TRAIN"

    const/16 v3, 0x10

    const-string v4, "czxx/query"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/czxx/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_STATION_TRAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 35
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PASSENGER_INIT"

    const/16 v11, 0x11

    const-string v12, "passengers/init"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/passengers/init"

    const-string v15, "application/x-www-form-urlencoded"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 36
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PASSENGER_ADD"

    const/16 v3, 0x12

    const-string v4, "passengers/add"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/passengers/addInit"

    const-string v7, "application/x-www-form-urlencoded"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_ADD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 37
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PASSENGER_DEL"

    const/16 v11, 0x13

    const-string v12, "passengers/delete"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/passengers/init"

    const-string v15, "application/x-www-form-urlencoded"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_DEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 38
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PASSENGER_EDIT"

    const/16 v3, 0x14

    const-string v4, "passengers/edit"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/passengers/show"

    const-string v7, "application/x-www-form-urlencoded"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_EDIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 39
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PASSENGER_SHOW"

    const/16 v11, 0x15

    const-string v12, "passengers/show"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/passengers/show"

    const-string v15, "application/x-www-form-urlencoded"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_SHOW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 40
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "GET_ORDER_PERSON"

    const/16 v3, 0x16

    const-string v4, "passengers/query"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/passengers/init"

    const-string v7, "application/x-www-form-urlencoded"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_PERSON:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 41
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "SEARCH_TICKET"

    const/16 v11, 0x17

    const-string v12, "leftTicket/queryT"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v15, "application/x-www-form-urlencoded"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SEARCH_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 42
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "LEFTTICKET_LOG"

    const/16 v3, 0x18

    const-string v4, "leftTicket/log"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v7, "application/x-www-form-urlencoded"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFTTICKET_LOG:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 43
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "GET_ORDER_RANGCODE"

    const/16 v11, 0x19

    const-string v12, "passcodeNew/getPassCodeNew"

    const-string v13, "image/webp,*/*;q=0.8"

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 44
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "QUERY_MYORDER_NOCOMPLETE"

    const/16 v3, 0x1a

    const-string v4, "queryOrder/queryMyOrderNoComplete"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/queryOrder/initNoComplete"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_MYORDER_NOCOMPLETE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 45
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CANCEL_QUEUE_NOCOMPLETE_MYORDER"

    const/16 v11, 0x1b

    const-string v12, "queryOrder/cancelQueueNoCompleteMyOrder"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/queryOrder/initNoComplete"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_QUEUE_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 46
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "SUBMITORDERREQUEST"

    const/16 v3, 0x1c

    const-string v4, "leftTicket/submitOrderRequest"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITORDERREQUEST:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 47
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CHECKUSER"

    const/16 v11, 0x1d

    const-string v12, "login/checkUser"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKUSER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 48
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "SUBMITINIT"

    const/16 v3, 0x1e

    const-string v4, "leftTicket/init"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/index/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITINIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 49
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "INITGC"

    const/16 v11, 0x1f

    const-string v12, "confirmPassenger/initGc"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITGC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 50
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "INITDC"

    const/16 v3, 0x20

    const-string v4, "confirmPassenger/initDc"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/leftTicket/init?random=1387009135876"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITDC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 51
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "GETPASSENGERDTOS"

    const/16 v11, 0x21

    const-string v12, "confirmPassenger/getPassengerDTOs"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETPASSENGERDTOS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 52
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "CHECKORDERINFO"

    const/16 v3, 0x22

    const-string v4, "confirmPassenger/checkOrderInfo"

    const-string v5, "application/json, text/javascript, */*; q=0.01"

    const-string v6, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKORDERINFO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 53
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "GETQUEUECOUNT"

    const/16 v11, 0x23

    const-string v12, "confirmPassenger/getQueueCount"

    const-string v13, ""

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETQUEUECOUNT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 54
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "QUERY_ORDER_WAITTIME"

    const/16 v3, 0x24

    const-string v4, "confirmPassenger/queryOrderWaitTime"

    const-string v5, "application/json, text/javascript, */*; q=0.01"

    const-string v6, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v7, ""

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_ORDER_WAITTIME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 55
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PASSENGER_RANGCODE"

    const/16 v11, 0x25

    const-string v12, "passcodeNew/getPassCodeNew.do?module=passenger&rand=randp&0.15299957408569753"

    const-string v13, "image/webp,*/*;q=0.8"

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 57
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "CONFIRMSINGLE"

    const/16 v3, 0x26

    const-string v4, "confirmPassenger/confirmSingle"

    const-string v5, "application/json, text/javascript, */*; q=0.01"

    const-string v6, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 58
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CONFIRMSINGLEFORQUEUE"

    const/16 v11, 0x27

    const-string v12, "confirmPassenger/confirmSingleForQueue"

    const-string v13, "application/json, text/javascript, */*; q=0.01"

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLEFORQUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 59
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "RESULT_ORDER_FOR_DC_QUEUE"

    const/16 v3, 0x28

    const-string v4, "confirmPassenger/resultOrderForDcQueue"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESULT_ORDER_FOR_DC_QUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 60
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CONTINUE_PAY_NOCOMPLETEMYORDER"

    const/16 v11, 0x29

    const-string v12, "queryOrder/continuePayNoCompleteMyOrder"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/payfinish/init"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONTINUE_PAY_NOCOMPLETEMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 61
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PAYORDER_INIT"

    const/16 v3, 0x2a

    const-string v4, "payOrder/init"

    const-string v5, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"

    const-string v6, "https://kyfw.12306.cn/otn/payfinish/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYORDER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 62
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PAYCHECK"

    const/16 v11, 0x2b

    const-string v12, "payOrder/paycheck"

    const-string v13, "application/json, text/javascript, */*; q=0.01"

    const-string v14, "https://kyfw.12306.cn/otn/payOrder/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECK:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 63
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PAYCHECKNEW"

    const/16 v3, 0x2c

    const-string v4, "payOrder/paycheckNew"

    const-string v5, "application/json, text/javascript, */*; q=0.01"

    const-string v6, "https://kyfw.12306.cn/otn/payOrder/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECKNEW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 64
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CANCEL_NOCOMPLETE_MYORDER"

    const/16 v11, 0x2d

    const-string v12, "queryOrder/cancelNoCompleteMyOrder"

    const-string v13, "*/*"

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 65
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "QUERYMYORDER"

    const/16 v3, 0x2e

    const-string v4, "queryOrder/queryMyOrder"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/queryOrder/init"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERYMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 66
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CHECKRANDCODEANSYN"

    const/16 v11, 0x2f

    const-string v12, "passcodeNew/checkRandCodeAnsyn"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/confirmPassenger/initDc"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKRANDCODEANSYN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 67
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PAY_FINISH"

    const/16 v3, 0x30

    const-string v4, "payfinish/init"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/payOrder/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_FINISH:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 69
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "RETURN_TICKET_AFFIRM"

    const/16 v11, 0x31

    const-string v12, "queryOrder/returnTicketAffirm"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/queryOrder/init"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET_AFFIRM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 70
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "RETURN_TICKET"

    const/16 v3, 0x32

    const-string v4, "queryOrder/returnTicket"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/queryOrder.init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 73
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "RESGIN_TICKET"

    const/16 v11, 0x33

    const-string v12, "queryOrder/resginTicket"

    const-string v13, "application/json, text/javascript, */*; q=0.01"

    const-string v14, "https://kyfw.12306.cn/otn/queryOrder/init"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESGIN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 76
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "GET_STATION_RANDCODE"

    const/16 v3, 0x34

    const-string v4, "passcodeNew/getPassCodeNew"

    const-string v5, "image/webp,*/*;q=0.8"

    const-string v6, "https://kyfw.12306.cn/otn/czxx/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_STATION_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 80
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "SUBDETAIL"

    const/16 v11, 0x35

    const-string v12, "regist/subDetail"

    const-string v13, ""

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBDETAIL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 81
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "CHECK_USERNAME"

    const/16 v3, 0x36

    const-string v4, "regist/checkUserName"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/regist/init"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECK_USERNAME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 82
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "GET_RANDCODE"

    const/16 v11, 0x37

    const-string v12, "regist/getRandCode"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/regist/init"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 83
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "INIT_SUC"

    const/16 v3, 0x38

    const-string v4, "regist/initSuc"

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INIT_SUC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 86
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "BINDTEL"

    const/16 v11, 0x39

    const-string v12, "userSecurity/bindTel"

    const-string v13, ""

    const-string v14, "https://kyfw.12306.cn/otn/index/initMy12306"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->BINDTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 87
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "EDITTEL"

    const/16 v3, 0x3a

    const-string v4, "userSecurity/doEditTel"

    const-string v5, ""

    const-string v6, "https://kyfw.12306.cn/otn/index/initMy12306"

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EDITTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 88
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "CHECK_MOBILE_CODE"

    const/16 v11, 0x3b

    const-string v12, "userSecurity/checkMobileCode"

    const-string v13, ""

    const-string v14, "https://kyfw.12306.cn/otn/index/initMy12306"

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECK_MOBILE_CODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 90
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "TO_PAY"

    const/16 v3, 0x3c

    const-string v4, "https://epay.12306.cn/pay/payGateway"

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_PAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 91
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "TO_WAPPAY"

    const/16 v11, 0x3d

    const-string v12, "https://epay.12306.cn/pay/wapPayGateway"

    const-string v13, ""

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_WAPPAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 92
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "PAY_WEBBUSINESS"

    const/16 v3, 0x3e

    const-string v4, "https://epay.12306.cn/pay/webBusiness"

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WEBBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 93
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "PAY_WAPBUSINESS"

    const/16 v11, 0x3f

    const-string v12, "https://epay.12306.cn/pay/wapBusiness"

    const-string v13, ""

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WAPBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 95
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "ZWD"

    const/16 v3, 0x40

    const-string v4, "zwdch/query"

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->ZWD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 96
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "ALLCITYS"

    const/16 v11, 0x41

    const-string v12, "userCommon/allCitys"

    const-string v13, "*/*"

    const-string v14, "https://kyfw.12306.cn/otn/passengers/show"

    const-string v15, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v16, "XMLHttpRequest"

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->ALLCITYS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 97
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, "SCHOOL_NAMES"

    const/16 v3, 0x42

    const-string v4, "userCommon/schoolNames"

    const-string v5, "*/*"

    const-string v6, "https://kyfw.12306.cn/otn/passengers/show"

    const-string v7, "application/x-www-form-urlencoded; charset=UTF-8"

    const-string v8, "XMLHttpRequest"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SCHOOL_NAMES:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 98
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v10, "EMPTY_URL"

    const/16 v11, 0x43

    const-string v12, ""

    const-string v13, ""

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EMPTY_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v0, 0x44

    new-array v0, v0, [Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 8
    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->DO_MAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_KEY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_RANGCODE_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LONGIN_CONFIM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_USER_ASYNC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->USER_LOGIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_LOGOUT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_ORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFT_TICKET_PRICE_PUBLIC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICEFL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_BY_TRAIN_NO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_STATION_TRAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_ADD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_DEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_EDIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_SHOW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_PERSON:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SEARCH_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LEFTTICKET_LOG:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_MYORDER_NOCOMPLETE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_QUEUE_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITORDERREQUEST:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKUSER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITINIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITGC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITDC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETPASSENGERDTOS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKORDERINFO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETQUEUECOUNT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_ORDER_WAITTIME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLEFORQUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESULT_ORDER_FOR_DC_QUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONTINUE_PAY_NOCOMPLETEMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYORDER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECK:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYCHECKNEW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERYMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKRANDCODEANSYN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_FINISH:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET_AFFIRM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESGIN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_STATION_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBDETAIL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECK_USERNAME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_RANDCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INIT_SUC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->BINDTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EDITTEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECK_MOBILE_CODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_PAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->TO_WAPPAY:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WEBBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAY_WAPBUSINESS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->ZWD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->ALLCITYS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SCHOOL_NAMES:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->EMPTY_URL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sput-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->$VALUES:[Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->path:Ljava/lang/String;

    .line 113
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->accept:Ljava/lang/String;

    .line 114
    iput-object p5, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->refer:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->contentType:Ljava/lang/String;

    .line 116
    iput-object p7, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->xRequestWith:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;
    .locals 1

    .line 8
    const-class v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    return-object p0
.end method

.method public static values()[Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;
    .locals 1

    .line 8
    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->$VALUES:[Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0}, [Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    return-object v0
.end method


# virtual methods
.method public getAccept()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->accept:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getRefer()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->refer:Ljava/lang/String;

    return-object v0
.end method

.method public getxRequestWith()Ljava/lang/String;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->xRequestWith:Ljava/lang/String;

    return-object v0
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->path:Ljava/lang/String;

    return-void
.end method
