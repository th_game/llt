.class Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;
.super Landroid/os/AsyncTask;
.source "UserProfileActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SignOutAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field thisView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;)V
    .locals 1

    .line 187
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 188
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 185
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 231
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object p1

    .line 232
    invoke-interface {p1}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->signOut()Z

    .line 234
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object p1

    const-string v0, ""

    .line 235
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->setSessionId(Ljava/lang/String;)V

    .line 236
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 185
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 213
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 214
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;

    .line 215
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 219
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const v1, 0x7f0d0274

    .line 222
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    const v1, 0x7f090245

    .line 223
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    const v2, 0x7f0d01e7

    .line 224
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 225
    :cond_2
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 226
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;->finish()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 192
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;

    .line 193
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 197
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const v1, 0x7f0d0273

    const/4 v2, -0x1

    .line 200
    new-instance v3, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask$1;

    invoke-direct {v3, p0, p0}, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask$1;-><init>(Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity$SignOutAsyncTask;Landroid/os/AsyncTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 208
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
