.class public Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;
.super Ljava/lang/Object;
.source "TicketInfo.java"


# instance fields
.field public amount_char:I

.field public batch_no:Ljava/lang/String;

.field public cancel_flag:Ljava/lang/String;

.field public coach_name:Ljava/lang/String;

.field public coach_no:Ljava/lang/String;

.field public come_go_traveller_ticket_page:Ljava/lang/String;

.field public confirm_flag:Ljava/lang/String;

.field public limit_time:Ljava/lang/String;

.field public lose_time:Ljava/lang/String;

.field public mTag:Ljava/lang/Object;

.field public passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

.field public pay_limit_time:Ljava/lang/String;

.field public pay_mode_code:Ljava/lang/String;

.field public print_eticket_flag:Ljava/lang/String;

.field public reserve_time:Ljava/lang/String;

.field public resign_flag:Ljava/lang/String;

.field public return_flag:Ljava/lang/String;

.field public seat_flag:Ljava/lang/String;

.field public seat_name:Ljava/lang/String;

.field public seat_no:Ljava/lang/String;

.field public seat_type_code:Ljava/lang/String;

.field public seat_type_name:Ljava/lang/String;

.field public sequence_no:Ljava/lang/String;

.field public start_train_date_page:Ljava/lang/String;

.field public stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

.field public str_ticket_price_page:Ljava/lang/String;

.field public ticket_no:Ljava/lang/String;

.field public ticket_price:Ljava/lang/String;

.field public ticket_status_code:Ljava/lang/String;

.field public ticket_status_name:Ljava/lang/String;

.field public ticket_type_code:Ljava/lang/String;

.field public ticket_type_name:Ljava/lang/String;

.field public trade_mode:Ljava/lang/String;

.field public train_date:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u8ba2\u5355\u53f7:"

    .line 82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->sequence_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u4e58\u5ba2:"

    .line 83
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u8f66\u6b21:"

    .line 84
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u533a\u95f4:"

    .line 85
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u53d1\u8f66\u65f6\u95f4:"

    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u5ea7\u5e2d:"

    .line 87
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_type_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\u8f66 "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\u7968\u4ef7:"

    .line 88
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->str_ticket_price_page:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\u5143"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_status_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
