.class public Lcom/lltskb/lltskb/engine/online/StationTrainQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;
.source "StationTrainQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "StationQuery"


# instance fields
.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/BaseHttpsQuery;-><init>()V

    .line 31
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method private parseQueryResult(Ljava/lang/String;)Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "data"

    const/4 v1, 0x0

    .line 80
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 84
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 88
    :cond_0
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 90
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_1

    return-object v1

    .line 93
    :cond_1
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "message"

    if-eqz v2, :cond_5

    .line 94
    :try_start_1
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    if-eqz p1, :cond_3

    .line 96
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 100
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object p1, v1

    .line 110
    :goto_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    :goto_1
    if-eqz p1, :cond_4

    .line 111
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 112
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;-><init>()V

    .line 113
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "train_no"

    .line 115
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->train_no:Ljava/lang/String;

    const-string v5, "station_train_code"

    .line 116
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->station_train_code:Ljava/lang/String;

    const-string v5, "start_station_telecode"

    .line 117
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->start_station_telecode:Ljava/lang/String;

    const-string v5, "start_station_name"

    .line 118
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->start_station_name:Ljava/lang/String;

    const-string v5, "end_station_telecode"

    .line 119
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->end_station_telecode:Ljava/lang/String;

    const-string v5, "end_station_name"

    .line 120
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->end_station_name:Ljava/lang/String;

    const-string v5, "station_name"

    .line 121
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->station_name:Ljava/lang/String;

    const-string v5, "station_telecode"

    .line 122
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->station_telecode:Ljava/lang/String;

    const-string v5, "train_class_name"

    .line 123
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->train_class_name:Ljava/lang/String;

    const-string v5, "train_type_name"

    .line 124
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->train_type_name:Ljava/lang/String;

    const-string v5, "start_time"

    .line 125
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->start_time:Ljava/lang/String;

    const-string v5, "arrive_time"

    .line 126
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->arrive_time:Ljava/lang/String;

    const-string v5, "start_start_time"

    .line 127
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->start_start_time:Ljava/lang/String;

    const-string v5, "end_arrive_time"

    .line 128
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->end_arrive_time:Ljava/lang/String;

    const-string v5, "running_time"

    .line 129
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->running_time:Ljava/lang/String;

    const-string v5, "start_train_date"

    .line 131
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->start_train_date:Ljava/lang/String;

    const-string v5, "seat_types"

    .line 133
    invoke-virtual {p0, v4, v5}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->safeGetString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;->seat_types:Ljava/lang/String;

    .line 135
    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_4
    return-object v0

    .line 103
    :cond_5
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 104
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 105
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-direct {v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_6
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d010b

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_7
    :goto_2
    return-object v1

    :catch_0
    move-exception p1

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "StationQuery"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return-object v1
.end method


# virtual methods
.method public queryStation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/StationQueryDTO;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 36
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    const-string v1, "UTF-8"

    .line 44
    invoke-static {p2, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 47
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 50
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "train_start_date="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_station_name="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_station_code="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&randCode="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 55
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryStation url="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "StationQuery"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :try_start_1
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_STATION_TRAIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 59
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "queryStation ret="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/StationTrainQuery;->parseQueryResult(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    .line 62
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 63
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_0

    .line 64
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 65
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 38
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const p3, 0x7f0d0130

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 39
    new-instance p3, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p3
.end method
