.class public Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;
.super Ljava/lang/Object;
.source "OrderDBDTO.java"


# instance fields
.field public array_passer_name_page:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public arrive_time_page:Ljava/lang/String;

.field public canOffLinePay:Ljava/lang/String;

.field public cancel_flag:Ljava/lang/String;

.field public com_go_traveller_order_page:Ljava/lang/String;

.field public confirm_flag:Ljava/lang/String;

.field public from_station_name_page:Ljava/lang/String;

.field public if_show_resigning_info:Ljava/lang/String;

.field public isNeedSendMailAndMsg:Ljava/lang/String;

.field public order_date:Ljava/lang/String;

.field public order_status:Ljava/lang/String;

.field public pay_flag:Ljava/lang/String;

.field public pay_resign_flag:Ljava/lang/String;

.field public print_eticket_flag:Ljava/lang/String;

.field public recordCount:Ljava/lang/String;

.field public reserve_flag_query:Ljava/lang/String;

.field public resign_flag:Ljava/lang/String;

.field public return_flag:Ljava/lang/String;

.field public sequence_no:Ljava/lang/String;

.field public start_time_page:Ljava/lang/String;

.field public start_train_date_page:Ljava/lang/String;

.field public ticket_price_all:D

.field public ticket_total_price_page:Ljava/lang/String;

.field public ticket_totalnum:I

.field public tickets:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;",
            ">;"
        }
    .end annotation
.end field

.field public to_station_name_page:Ljava/lang/String;

.field public train_code_page:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
