.class public Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;
.super Ljava/lang/Object;
.source "UserQueryImpl.java"

# interfaces
.implements Lcom/lltskb/lltskb/engine/online/IUserQuery;


# static fields
.field private static final TAG:Ljava/lang/String; = "SignModel"


# instance fields
.field private initMy12306ApiDTO:Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;

.field private mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

.field private mDynamicJs:Lcom/lltskb/lltskb/engine/online/DynamicJs;

.field private mDynamicStr:Ljava/lang/String;

.field private mErrorMsg:Ljava/lang/String;

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mIsSignIn:Z

.field private uamtk:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    .line 27
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    .line 90
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->updatePassenger()V

    return-void
.end method

.method private initMy12306()Z
    .locals 4

    const-string v0, "SignModel"

    const-string v1, "initMy12306"

    .line 397
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/otn/index/initMy12306"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initMy12306 ret="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    .line 404
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return v0
.end method

.method private initMy12306Api()Z
    .locals 4

    const/4 v0, 0x0

    .line 299
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/otn/index/initMy12306Api"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 300
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->parseResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/dto/InitMy12306ApiDTO;->user_name:Ljava/lang/String;

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->username:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :catch_0
    move-exception v1

    .line 306
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 307
    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SignModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method private login(Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "login user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SignModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setAccount(Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 240
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberPass(Z)V

    .line 244
    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p4, p3}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberName(Z)V

    .line 246
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "username="

    .line 247
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&password="

    .line 248
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&appid=otn"

    .line 249
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string p2, "https://kyfw.12306.cn/passport/web/login"

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "login="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 262
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 263
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->parseLoginResult(Ljava/lang/String;)Z

    move-result p2

    :cond_1
    if-eqz p2, :cond_2

    .line 269
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk()Z

    move-result p2

    .line 270
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/LoginModel;->get()Lcom/lltskb/lltskb/engine/online/LoginModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->conf()Z

    .line 273
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->initMy12306Api()Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "initMy12306 return false"

    .line 274
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_2
    iput-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    .line 278
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    if-eqz p1, :cond_3

    .line 279
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->username:Ljava/lang/String;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setUserName(Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/online/UserMgr;->addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V

    .line 281
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    :cond_3
    const/4 p1, 0x0

    .line 283
    sput-object p1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->mCURL:Ljava/lang/String;

    if-eqz p2, :cond_4

    .line 285
    new-instance p1, Ljava/lang/Thread;

    new-instance p3, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl$1;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl$1;-><init>(Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;)V

    invoke-direct {p1, p3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    :cond_4
    return p2

    :catch_0
    move-exception p1

    .line 255
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 256
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_5

    .line 257
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 258
    :cond_5
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string p2, "\u7f51\u7edc\u95ee\u9898"

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private parseLoginResult(Ljava/lang/String;)Z
    .locals 3

    .line 324
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 327
    :cond_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 329
    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 330
    instance-of v0, p1, Lorg/json/JSONObject;

    if-nez v0, :cond_1

    goto :goto_0

    .line 333
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v0, "result_code"

    .line 334
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const-string v2, "uamtk"

    .line 335
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk:Ljava/lang/String;

    const-string v2, "result_message"

    .line 336
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1

    :catch_0
    move-exception p1

    .line 340
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 341
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    return v1
.end method

.method private parseUamtk(Ljava/lang/String;)Z
    .locals 4

    const-string v0, "newapptk"

    .line 125
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return v2

    .line 128
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 130
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 131
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_0

    .line 134
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "result_code"

    .line 135
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 136
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk:Ljava/lang/String;

    :cond_2
    const-string v0, "result_message"

    .line 139
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    :goto_0
    return v2

    :catch_0
    move-exception p1

    .line 143
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v2
.end method

.method private parserLoginASynSuggestResult(Ljava/lang/String;)Z
    .locals 6

    const-string v0, "data"

    const/4 v1, 0x0

    if-eqz p1, :cond_9

    .line 416
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    goto/16 :goto_2

    .line 419
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 420
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 421
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_1

    goto/16 :goto_1

    .line 425
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 427
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    const-string v2, "messages"

    .line 429
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONArray;

    const-string v3, ""

    .line 431
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    .line 432
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    .line 433
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 434
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 436
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    .line 438
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 441
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_5

    return v1

    :cond_5
    const-string v0, "loginCheck"

    .line 443
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_6

    return v1

    :cond_6
    const-string v0, "Y"

    .line 446
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    .line 447
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    if-eqz p1, :cond_7

    .line 448
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setSignKey(Ljava/lang/String;)V

    .line 449
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V

    .line 450
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V

    .line 452
    :cond_7
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :cond_8
    :goto_1
    return v1

    :catch_0
    move-exception p1

    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SignModel"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_9
    :goto_2
    return v1
.end method

.method private parserloginUserAsynResult(Ljava/lang/String;)Z
    .locals 6

    const-string v0, "data"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return v1

    .line 500
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 501
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 502
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_1

    goto :goto_1

    .line 506
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "httpstatus"

    .line 508
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    const-string v2, "messages"

    .line 511
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONArray;

    const-string v3, ""

    .line 513
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    const/4 v3, 0x0

    .line 514
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 515
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 518
    :cond_3
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 521
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "status"

    .line 522
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    .line 524
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    if-eqz v0, :cond_5

    const-string v0, "username"

    .line 525
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 526
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setUserName(Ljava/lang/String;)V

    .line 527
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->addUser(Lcom/lltskb/lltskb/engine/online/dto/UserInfo;)V

    .line 528
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object p1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->setLastUser(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :cond_5
    :goto_1
    return v1

    :catch_0
    move-exception p1

    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SignModel"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v1
.end method

.method private parseuamauthClientResult(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "apptk"

    .line 167
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 170
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 172
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 173
    instance-of v3, v1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_0

    .line 176
    :cond_1
    check-cast v1, Lorg/json/JSONObject;

    const-string v3, "result_code"

    .line 177
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 178
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 179
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk:Ljava/lang/String;

    :cond_2
    const-string v0, "username"

    .line 181
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->username:Ljava/lang/String;

    const-string v0, "result_message"

    .line 182
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_3

    const/4 p1, 0x1

    :cond_3
    :goto_0
    return p1

    :catch_0
    move-exception v0

    .line 186
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    return p1
.end method

.method private uamauthclient()Z
    .locals 6

    const-string v0, "\u767b\u5f55\u5931\u8d25"

    .line 151
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uamauthclient uamtk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SignModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk:Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v3, "https://kyfw.12306.cn/otn/uamauthclient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tk="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uamauthclient failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    .line 161
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uamauthclient="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->parseuamauthClientResult(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private uamtk()Z
    .locals 4

    const-string v0, "SignModel"

    const-string v1, "uamtk"

    .line 109
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/passport/web/auth/uamtk"

    const-string v3, "appid=otn"

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 114
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uamtk failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, ""

    .line 117
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uamtk="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->parseUamtk(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamauthclient()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private updatePassenger()V
    .locals 3

    .line 314
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengers(Z)Z
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updatePassenger exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SignModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private userLogin()Z
    .locals 4

    const-string v0, "SignModel"

    const-string v1, "userLogin"

    .line 42
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->USER_LOGIN:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v3, "_json_att="

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 48
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 49
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "userLogin failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public checkUser()Z
    .locals 7

    const-string v0, "SignModel"

    const-string v1, "doCheckSign"

    .line 196
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/LoginModel;->get()Lcom/lltskb/lltskb/engine/online/LoginModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->conf()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    :cond_0
    const-string v1, "uamtk"

    .line 201
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->uamtk()Z

    move-result v0

    return v0

    :cond_1
    const/4 v1, 0x3

    const-string v3, ""

    :goto_0
    if-lez v1, :cond_2

    .line 209
    :try_start_0
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v1, v1, -0x1

    .line 211
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v5, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKUSER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v6, "_json_att="

    invoke-virtual {v4, v5, v6}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 215
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 216
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doCheckSign failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSign="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "{\"flag\":true}"

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public isSignedIn(Z)Z
    .locals 2

    const-string v0, "SignModel"

    const-string v1, "isSign"

    .line 95
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/LoginModel;->get()Lcom/lltskb/lltskb/engine/online/LoginModel;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->isSignin(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-nez p1, :cond_1

    .line 100
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    return p1

    .line 101
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->checkUser()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    .line 102
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isSign = "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    return p1
.end method

.method public loginAysnSuggest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "uamtk"

    .line 352
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->login(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result p1

    return p1

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setAccount(Ljava/lang/String;)V

    if-eqz p5, :cond_1

    .line 357
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    .line 360
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {v0, p5}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberPass(Z)V

    .line 361
    iget-object p5, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p5, p4}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setRemeberName(Z)V

    .line 363
    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicJs:Lcom/lltskb/lltskb/engine/online/DynamicJs;

    if-nez p4, :cond_2

    const-string p4, "\u767b\u5f55\u5931\u8d25\uff0c\u8bf7\u91cd\u8bd5"

    .line 364
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mErrorMsg:Ljava/lang/String;

    .line 368
    :cond_2
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "loginUserDTO.user_name="

    .line 369
    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&userDTO.password="

    .line 370
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&randCode="

    .line 371
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicStr:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, "&"

    .line 375
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicStr:Ljava/lang/String;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :cond_3
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LONGIN_CONFIM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "login="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "SignModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->parserLoginASynSuggestResult(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 385
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 386
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_4

    .line 387
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 388
    :cond_4
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public loginInit()Z
    .locals 6

    const/4 v0, 0x1

    .line 58
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/JSEngine;->get()Lcom/lltskb/lltskb/utils/JSEngine;

    move-result-object v1

    const-string v2, "https://kyfw.12306.cn/otn/login/init"

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/utils/JSEngine;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v1

    .line 61
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SignModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 66
    :try_start_1
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v4, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 72
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return v1

    .line 76
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loginInit="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-direct {v1, v3}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicJs:Lcom/lltskb/lltskb/engine/online/DynamicJs;

    .line 81
    :try_start_2
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicJs:Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mDynamicStr:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    return v0

    :catch_2
    move-exception v0

    .line 68
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v1
.end method

.method public loginUserAsyn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "random="

    .line 464
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "&loginUserDTO.user_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&userDTO.password="

    .line 465
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&randCode="

    .line 466
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p3, p1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setAccount(Ljava/lang/String;)V

    .line 469
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mCurUser:Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->setPassword(Ljava/lang/String;)V

    .line 474
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_USER_ASYNC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "login="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "SignModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 483
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_0

    .line 484
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->parserloginUserAsynResult(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 476
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 477
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 478
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 479
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public queryUserInfo()Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;
    .locals 4

    const-string v0, ""

    .line 564
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v2, "https://kyfw.12306.cn/otn/modifyUser/initQueryUserInfoApi"

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 566
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;->parseQueryUserInfo(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryUserInfoDTO;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    .line 568
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 569
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SignModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "singOut = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public signOut()Z
    .locals 3

    const-string v0, ""

    .line 545
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/LoginModel;->get()Lcom/lltskb/lltskb/engine/online/LoginModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/LoginModel;->signOut()V

    .line 548
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->LOGIN_LOGOUT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 550
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->clearCookie()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 553
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 555
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "singOut = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SignModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 556
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/impl/UserQueryImpl;->mIsSignIn:Z

    const/4 v0, 0x1

    return v0
.end method
