.class Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;
.super Landroid/widget/LinearLayout;
.source "ResultSimpleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResultLayout"
.end annotation


# instance fields
.field mItemChbox:Landroid/widget/CheckBox;

.field mView:Landroid/view/View;

.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .line 169
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->this$0:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 170
    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 171
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->setOrientation(I)V

    .line 173
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mView:Landroid/view/View;

    .line 174
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mView:Landroid/view/View;

    const v0, 0x7f09008c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/CheckBox;

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    .line 175
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 177
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {p2, p1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 178
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {p2, p1}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 179
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {p2, p1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 187
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mView:Landroid/view/View;

    return-object v0
.end method

.method public showCheckBox(IZ)V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 192
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter$ResultLayout;->mItemChbox:Landroid/widget/CheckBox;

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method
