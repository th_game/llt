.class public Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;
.super Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;
.source "TrainZwdQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;
    }
.end annotation


# instance fields
.field private mSelectedRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStation:Ljava/lang/String;

.field private mTicketList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTrain:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 1

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/TrainCodeQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    .line 35
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    .line 36
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    const/16 p2, 0x2f

    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    if-ltz p1, :cond_0

    .line 38
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->refreshList()V

    return-void
.end method

.method private refreshList()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    new-instance v1, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$1;-><init>(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public doQuery()Ljava/lang/String;
    .locals 8

    .line 112
    :try_start_0
    new-instance v0, Lcom/lltskb/lltskb/engine/QueryCC;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    .line 113
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 115
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit16 v4, v4, 0x2710

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/2addr v6, v2

    mul-int/lit8 v6, v6, 0x64

    add-int/2addr v4, v6

    const/4 v6, 0x5

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v4, v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    .line 118
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mTrainName:Ljava/lang/String;

    sget v4, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v0, v3, v4}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 119
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v2, :cond_1

    .line 121
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 123
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 124
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;-><init>()V

    .line 125
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 126
    invoke-virtual {v6, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    .line 127
    invoke-virtual {v6, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const/4 v7, 0x3

    .line 128
    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    const/4 v7, 0x4

    .line 129
    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    .line 130
    invoke-virtual {v3, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iput-object v3, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    .line 137
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
    .locals 11

    .line 148
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v2}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    .line 150
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    if-nez v0, :cond_1

    return-object v1

    .line 152
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    const v7, 0x7f0b0094

    const/4 v1, 0x4

    new-array v8, v1, [Ljava/lang/String;

    const-string v4, "station"

    aput-object v4, v8, v3

    const-string v3, "stoptime"

    aput-object v3, v8, v2

    const/4 v2, 0x2

    const-string v3, "time"

    aput-object v3, v8, v2

    const/4 v2, 0x3

    const-string v3, "ticket"

    aput-object v3, v8, v2

    new-array v9, v1, [I

    fill-array-data v9, :array_0

    move-object v4, v0

    move-object v10, p0

    invoke-direct/range {v4 .. v10}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILcom/lltskb/lltskb/engine/online/BaseQuery;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 157
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    return-object v0

    :array_0
    .array-data 4
        0x7f090009
        0x7f09000a
        0x7f09000c
        0x7f09000b
    .end array-data
.end method

.method public getDisplayResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mDisplayResult:Ljava/util/Vector;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u6b21\u6b63\u665a\u70b9\u4fe1\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onItemClicked(I)V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 258
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_1

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 259
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    .line 260
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mSelectedRow:Ljava/util/Map;

    .line 261
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mStation:Ljava/lang/String;

    .line 264
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-string v0, "1234"

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->queryZWD(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "*>;ZZ)",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 167
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 168
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;

    if-eqz p2, :cond_1

    .line 169
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->isSelected:Z

    if-nez v3, :cond_1

    goto/16 :goto_4

    .line 170
    :cond_1
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 171
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 172
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->station_name:Ljava/lang/String;

    const-string v5, "station"

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    const-string v5, "--"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v6, ""

    if-nez v4, :cond_2

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " \u5230"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    move-object v4, v6

    .line 178
    :goto_1
    iget-object v7, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 179
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 180
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_3

    const-string v4, "<br/>"

    goto :goto_2

    :cond_3
    const-string v4, "\n"

    :goto_2
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 181
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " \u5f00"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_5
    const-string v7, "stoptime"

    .line 183
    invoke-interface {v3, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 186
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 187
    array-length v5, v4

    const/4 v7, 0x1

    if-le v5, v7, :cond_6

    .line 188
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v4, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u65f6"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v7

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u5206"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 190
    :cond_6
    iget-object v6, v2, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO$StationDTO;->running_time:Ljava/lang/String;

    :cond_7
    :goto_3
    const-string v2, "time"

    .line 194
    invoke-interface {v3, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ticket"

    const-string v4, "\u8bf7\u70b9\u51fb\u67e5\u8be2"

    .line 196
    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 199
    :cond_8
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTicketList:Ljava/util/List;

    return-object p1
.end method

.method public queryZWD(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 252
    new-instance v7, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mSelectedRow:Ljava/util/Map;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mStation:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;->mTrain:Ljava/lang/String;

    move-object v0, v7

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;-><init>(Lcom/lltskb/lltskb/engine/online/TrainZwdQuery;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/online/TrainZwdQuery$ZwdThread;->start()V

    return-void
.end method
