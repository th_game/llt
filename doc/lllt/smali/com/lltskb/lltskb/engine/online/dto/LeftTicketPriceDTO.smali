.class public Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;
.super Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;
.source "LeftTicketPriceDTO.java"


# instance fields
.field public gg_price:Ljava/lang/String;

.field public gr_price:Ljava/lang/String;

.field public rw_price:Ljava/lang/String;

.field public rz_price:Ljava/lang/String;

.field public swz_price:Ljava/lang/String;

.field public tz_price:Ljava/lang/String;

.field public yb_price:Ljava/lang/String;

.field public yw_price:Ljava/lang/String;

.field public yz_price:Ljava/lang/String;

.field public ze_price:Ljava/lang/String;

.field public zy_price:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)I
    .locals 3

    .line 34
    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    .line 35
    sget v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 36
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yz_price:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result p1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->ze_price:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v2

    add-int/2addr p1, v2

    .line 37
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yz_price:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->ze_price:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v2, v0

    if-ne p1, v2, :cond_0

    return v1

    :cond_0
    if-ge p1, v2, :cond_1

    const/4 p1, -0x1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1

    .line 42
    :cond_2
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->compareTo(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 8
    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->compareTo(Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;)I

    move-result p1

    return p1
.end method
