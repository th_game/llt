.class public Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;
.super Lcom/lltskb/lltskb/engine/online/BaseQuery;
.source "ZZTimeQuery.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ZZTimeQuery"


# instance fields
.field private mDisplayResult:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

.field private mTicketPriceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mTicketPriceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/BaseQuery;-><init>(Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;I)V

    .line 47
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    return-void
.end method

.method private formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    if-eqz p2, :cond_2

    const-string v0, "--"

    .line 122
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "-:-"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    if-nez p3, :cond_1

    .line 127
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 130
    :cond_1
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "<font color=\"#5c9f00\">\u00a5"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "</font>"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const-string p1, ""

    return-object p1
.end method

.method private getPrice(Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;Z)Ljava/lang/String;
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 240
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI5()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 242
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI5()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getPRI5()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    :cond_1
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI4()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 245
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI4()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getPRI4()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI3()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 248
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI3()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getPRI3()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_3
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI2()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 251
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getPRI2()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_4
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI1()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 254
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getDESPRI1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getPRI1()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPriceString(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceMap:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 265
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    .line 266
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->getPrice(Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "*>;ZZ)",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 140
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 141
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 142
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    if-eqz p2, :cond_1

    .line 143
    iget-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->isSelected:Z

    if-nez v4, :cond_1

    goto :goto_1

    .line 144
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 147
    :cond_2
    :try_start_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sort:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "ZZTimeQuery"

    invoke-static {v2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :goto_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 154
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 155
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    .line 158
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->isNotMatchFilter(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto/16 :goto_8

    .line 160
    :cond_3
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_4

    .line 162
    iget-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->isSelected:Z

    if-nez v4, :cond_4

    goto/16 :goto_8

    .line 164
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-"

    if-eqz p3, :cond_5

    const-string v6, "</b>"

    .line 167
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "<br/><small>("

    .line 168
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_station_name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->end_station_name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ")</small>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 171
    :cond_5
    iget-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_station_name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->end_station_name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :goto_4
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 176
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "train"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<br/>"

    const-string v7, " \u5230"

    const-string v8, " "

    if-eqz p3, :cond_6

    .line 186
    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "<b><font color=\"#0000ff\">&nbsp;"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_time:Ljava/lang/String;

    .line 187
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "</font></b>&nbsp;\u5f00"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_name:Ljava/lang/String;

    .line 189
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 192
    :cond_6
    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " \u5f00"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\n"

    .line 194
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_name:Ljava/lang/String;

    .line 195
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :goto_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v7, "station"

    invoke-interface {v5, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "type"

    const-string v7, ""

    .line 200
    invoke-interface {v5, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->lishi:Ljava/lang/String;

    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 203
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v8, v4, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\u65f6"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x1

    aget-object v4, v4, v8

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u5206"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v7, "time"

    invoke-interface {v5, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    invoke-direct {p0, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->getPriceString(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 207
    invoke-static {v7}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 208
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    invoke-virtual {v4, v1, v8}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 209
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 211
    :cond_7
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->swz_price:Ljava/lang/String;

    const-string v8, " \u5546\u52a1:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->tz_price:Ljava/lang/String;

    const-string v8, " \u7279\u7b49:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->zy_price:Ljava/lang/String;

    const-string v8, " \u4e00\u7b49:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->ze_price:Ljava/lang/String;

    const-string v8, " \u4e8c\u7b49:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->gr_price:Ljava/lang/String;

    const-string v8, " \u9ad8\u7ea7\u8f6f\u5367:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rw_price:Ljava/lang/String;

    const-string v8, " \u8f6f\u5367:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yw_price:Ljava/lang/String;

    const-string v8, " \u786c\u5367:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->rz_price:Ljava/lang/String;

    const-string v8, " \u8f6f\u5ea7:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    iget-object v7, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->yz_price:Ljava/lang/String;

    const-string v8, " \u786c\u5ea7:"

    invoke-direct {p0, v8, v7, p3}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->formatPrice(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :goto_6
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->getNote()Ljava/lang/String;

    move-result-object v3

    .line 224
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v7

    const-string v8, "ticket"

    if-eqz v7, :cond_8

    .line 225
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 228
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    :goto_7
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_9
    return-object p1
.end method


# virtual methods
.method public doQuery()Ljava/lang/String;
    .locals 8

    .line 61
    :try_start_0
    new-instance v6, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;

    invoke-direct {v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;-><init>()V

    .line 62
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v4, "ADULT"

    const-string v5, ""

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->queryTicketPrice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    iput-object v0, v7, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 63
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v4, "ADULT"

    invoke-virtual {v6, v1, v2, v3, v4}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->queryTicketPricePublic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    .line 68
    :cond_1
    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/online/LeftTicketPriceQuery;->getmTicketPriceList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceList:Ljava/util/List;

    .line 69
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceMap:Ljava/util/Map;

    .line 71
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;

    .line 72
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mTicketPriceMap:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceDTO;->getSTCODE()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 75
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/FlightQuery;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/FlightQuery;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/lltskb/lltskb/engine/online/FlightQuery;->getFlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;
    .locals 10

    .line 90
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 93
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const v6, 0x7f0b0086

    const/4 v3, 0x4

    new-array v7, v3, [Ljava/lang/String;

    const-string v8, "train"

    aput-object v8, v7, v2

    const-string v8, "station"

    aput-object v8, v7, v1

    const/4 v1, 0x2

    const-string v8, "time"

    aput-object v8, v7, v1

    const/4 v1, 0x3

    const-string v8, "ticket"

    aput-object v8, v7, v1

    new-array v8, v3, [I

    fill-array-data v8, :array_0

    move-object v3, v0

    move-object v9, p0

    invoke-direct/range {v3 .. v9}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILcom/lltskb/lltskb/engine/online/BaseQuery;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mFlight:[Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setFlight([Lcom/lltskb/lltskb/engine/online/FlightQuery$Flight;)V

    .line 99
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;->setShowBookButton(Z)V

    .line 100
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mAdapter:Lcom/lltskb/lltskb/engine/online/view/ResultSimpleAdapter;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x7f09000d
        0x7f090008
        0x7f09000c
        0x7f09000b
    .end array-data
.end method

.method public getDisplayResult()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "*>;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " \u2192 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8f66\u6b21"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onItemClicked(I)V
    .locals 3

    .line 356
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    .line 357
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_1

    if-gez p1, :cond_0

    goto :goto_0

    .line 358
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;

    const/4 v0, 0x0

    .line 360
    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mLeftDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 361
    sput-object v0, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;->mPriceDTO:Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    .line 363
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 374
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->from_station_name:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->to_station_name:Ljava/lang/String;

    const-string v2, "ticket_arrive_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->train_no:Ljava/lang/String;

    const-string v2, "train_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->station_train_code:Ljava/lang/String;

    const-string v2, "train_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "ticket_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketPriceDTO;->start_train_date:Ljava/lang/String;

    const-string v1, "start_train_date"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/TrainDetailsActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 381
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onOrderClicked(I)V
    .locals 3

    .line 341
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mDisplayResult:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 342
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    goto :goto_0

    .line 343
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u2192"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 346
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    const-class v2, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 347
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mStartStation:Ljava/lang/String;

    const-string v2, "order_from_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mArriveStation:Ljava/lang/String;

    const-string v2, "order_to_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mDate:Ljava/lang/String;

    const-string v2, "order_depart_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "order_train_code"

    .line 350
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 105
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->mActivity:Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;->mResult:Ljava/util/Vector;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/lltskb/lltskb/engine/online/ZZTimeQuery;->prepareResult(Ljava/util/Vector;ZZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 109
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 111
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string v4, "train"

    .line 112
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "station"

    .line 113
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u7968\u4ef7\uff1a"

    .line 114
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "ticket"

    .line 115
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n\n"

    .line 116
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
