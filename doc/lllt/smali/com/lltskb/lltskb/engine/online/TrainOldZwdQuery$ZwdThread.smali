.class Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;
.super Ljava/lang/Thread;
.source "TrainOldZwdQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ZwdThread"
.end annotation


# instance fields
.field mRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mStation:Ljava/lang/String;

.field mTrain:Ljava/lang/String;

.field mType:Ljava/lang/String;

.field mYzm:Ljava/lang/String;

.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 179
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 180
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    .line 181
    iput-object p4, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mTrain:Ljava/lang/String;

    .line 182
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mStation:Ljava/lang/String;

    .line 183
    iput-object p5, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mYzm:Ljava/lang/String;

    .line 184
    iput-object p6, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 188
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-static {v0}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->access$000(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "TrainOldZwdQuery"

    const-string v1, "ticket list is null"

    .line 189
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 193
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/ZWDQuery;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;-><init>()V

    const/4 v1, 0x0

    .line 194
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->access$000(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 195
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->access$100(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 198
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->access$000(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    .line 199
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    if-nez v2, :cond_2

    goto :goto_5

    :cond_2
    const-string v3, "station"

    .line 201
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mStation:Ljava/lang/String;

    const/4 v2, 0x2

    const-string v8, ""

    move-object v2, v8

    const/4 v9, 0x2

    :cond_3
    const-string v10, "retry"

    if-lez v9, :cond_4

    .line 206
    :try_start_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mTrain:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mStation:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mYzm:Ljava/lang/String;

    const/16 v6, 0x2ee0

    iget-object v7, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mType:Ljava/lang/String;

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, Lcom/lltskb/lltskb/engine/online/ZWDQuery;->requestZwd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v9, v9, -0x1

    .line 208
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_3

    .line 213
    :cond_4
    :goto_1
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_5

    move-object v2, v8

    goto :goto_4

    .line 221
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 218
    :goto_3
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 219
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    move-object v2, v3

    .line 224
    :cond_5
    :goto_4
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->mRow:Ljava/util/Map;

    const-string v4, "ticket"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery$ZwdThread;->this$0:Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;

    invoke-static {v2}, Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;->access$200(Lcom/lltskb/lltskb/engine/online/TrainOldZwdQuery;)V

    const-wide/16 v2, 0x7d0

    .line 228
    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_5

    :catch_2
    move-exception v2

    .line 230
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_6
    return-void
.end method
