.class public Lcom/lltskb/lltskb/engine/online/OrderTicketModel;
.super Ljava/lang/Object;
.source "OrderTicketModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OrderTicketModel"

.field private static instance:Lcom/lltskb/lltskb/engine/online/OrderTicketModel; = null

.field private static final item_history:Ljava/lang/String; = "history"

.field private static final pref_name:Ljava/lang/String; = "order_histroy"


# instance fields
.field private REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

.field private canChooseBeds:Ljava/lang/String;

.field private canChooseSeats:Ljava/lang/String;

.field private choose_Seats:Ljava/lang/String;

.field private ifShowPassCode:Ljava/lang/String;

.field private ifShowPassCodeTime:Ljava/lang/String;

.field private isCanChooseMid:Ljava/lang/String;

.field private key_check_isChange:Ljava/lang/String;

.field private mCompleteOrderList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mDynamicJS:Ljava/lang/String;

.field private mErrorMsg:Ljava/lang/String;

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mInitDCStr:Ljava/lang/String;

.field private mIsAsync:Z

.field private mNextRequestTime:J

.field private mNoCompleteOrderList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderId:Ljava/lang/String;

.field private mOrderMgrs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

.field private mPref:Landroid/content/SharedPreferences;

.field private mSubmitInitStr:Ljava/lang/String;

.field private mWaitTime:J

.field private orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

.field private train_location:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "N"

    .line 60
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseBeds:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseSeats:Ljava/lang/String;

    const-string v1, ""

    .line 62
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->choose_Seats:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->isCanChooseMid:Ljava/lang/String;

    const-string v0, "Y"

    .line 64
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->ifShowPassCode:Ljava/lang/String;

    const-string v0, "1"

    .line 65
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->ifShowPassCodeTime:Ljava/lang/String;

    const/4 v0, 0x1

    .line 67
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mIsAsync:Z

    const-wide/16 v0, 0x0

    .line 688
    iput-wide v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    .line 689
    iput-wide v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNextRequestTime:J

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    .line 85
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    monitor-enter v0

    .line 77
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->instance:Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    if-nez v1, :cond_0

    .line 78
    new-instance v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->instance:Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    .line 80
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->instance:Lcom/lltskb/lltskb/engine/online/OrderTicketModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getOldPassengerStr(Ljava/util/Vector;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 1308
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1309
    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-boolean v2, v2, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-eqz v2, :cond_0

    const-string v2, "_ "

    .line 1310
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1313
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    .line 1315
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1316
    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1317
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1319
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1322
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPassengerTicketStr(Ljava/util/Vector;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1343
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canBuyStudentTicket()Z

    move-result v0

    .line 1353
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->exchange_train_flag:Ljava/lang/String;

    .line 1354
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1355
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ge v3, v4, :cond_5

    .line 1356
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    invoke-direct {p0, v4, v6}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getRealSeatTypeFromName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1359
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    .line 1362
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v7, v7, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v7, v7, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_real:Ljava/lang/String;

    .line 1363
    invoke-static {v7}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    const-string v9, "1"

    if-nez v8, :cond_0

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v0, :cond_1

    if-eqz v5, :cond_2

    :cond_1
    const-string v5, "3"

    .line 1365
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v6, v9

    .line 1369
    :cond_2
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-boolean v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-eqz v5, :cond_3

    const-string v6, "2"

    .line 1371
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ",0,"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1372
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v6, v6, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1373
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1374
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1376
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1378
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->phone_no:Ljava/lang/String;

    if-nez v5, :cond_4

    const-string v5, ""

    goto :goto_2

    .line 1379
    :cond_4
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v5, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->phone_no:Ljava/lang/String;

    :goto_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ",N"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1380
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->allEncStr:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1381
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1383
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1384
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    return-object p1

    .line 1385
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v5

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getRealSeatTypeFromName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 836
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p2, :cond_1

    .line 837
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "4"

    .line 838
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p1, "I"

    goto :goto_0

    :cond_0
    const-string p2, "3"

    .line 840
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p1, "J"

    :cond_1
    :goto_0
    return-object p1
.end method

.method private getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 848
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "WZ"

    .line 849
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 850
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    const-string v0, "--"

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yz_num:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "yz"

    .line 851
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 854
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->ze_num:Ljava/lang/String;

    .line 855
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "ze"

    .line 856
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 859
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yw_num:Ljava/lang/String;

    .line 860
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "yw"

    .line 861
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 864
    :cond_2
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->zy_num:Ljava/lang/String;

    .line 865
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, "zy"

    .line 866
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 869
    :cond_3
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rz_num:Ljava/lang/String;

    .line 870
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "rz"

    .line 871
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 874
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->rw_num:Ljava/lang/String;

    .line 875
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_5

    const-string p1, "rw"

    .line 876
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 879
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->tz_num:Ljava/lang/String;

    .line 880
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    const-string p1, "tz"

    .line 881
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 884
    :cond_6
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->gr_num:Ljava/lang/String;

    .line 885
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    const-string p1, "gr"

    .line 886
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 889
    :cond_7
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->swz_num:Ljava/lang/String;

    .line 890
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_8

    const-string p1, "swz"

    .line 891
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 894
    :cond_8
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    :cond_9
    return-object p1
.end method

.method private initOrderConfigMgr(Lorg/json/JSONObject;)V
    .locals 5

    .line 157
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    if-nez p1, :cond_0

    return-void

    .line 159
    :cond_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 161
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 164
    :try_start_0
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    .line 166
    :cond_2
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-nez v3, :cond_3

    return-void

    .line 168
    :cond_3
    new-instance v4, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;-><init>()V

    .line 169
    invoke-virtual {v4, v3}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->initOrderConfigMgr(Lorg/json/JSONObject;)V

    .line 170
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 172
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method private parseCancelOrderResult(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 1903
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto :goto_2

    .line 1907
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 1909
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 1910
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_1

    .line 1913
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 1914
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    const-string v2, ""

    .line 1915
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 1917
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1918
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "data"

    .line 1922
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_3

    const-string v1, "existError"

    .line 1924
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "N"

    .line 1925
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :cond_3
    :goto_1
    return v0

    :catch_0
    move-exception p1

    .line 1930
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1931
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 1932
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_4
    :goto_2
    return v0
.end method

.method private parseCancelQueueMyOrderNoComplete(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "exception="

    const-string v1, "OrderTicketModel"

    const/4 v2, 0x0

    if-eqz p1, :cond_7

    .line 2260
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 2264
    :cond_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 2266
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 2267
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_1

    .line 2270
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "messages"

    .line 2271
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const-string v4, ""

    .line 2272
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2273
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2274
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "status"

    .line 2277
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    return v2

    :cond_3
    const-string v3, "message"

    .line 2282
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2283
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2284
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v2

    :cond_4
    const-string v3, "data"

    .line 2288
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_5

    return v2

    :cond_5
    const-string v3, "existError"

    .line 2293
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v3, "Y"

    .line 2294
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_6
    :goto_1
    return v2

    :catch_0
    move-exception p1

    .line 2304
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2305
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2306
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return v2

    :catch_1
    move-exception p1

    .line 2298
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2300
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_7
    :goto_2
    return v2
.end method

.method private parseCheckOrderInfo(Ljava/lang/String;)I
    .locals 13

    const-string v0, "errMsg"

    const-string v1, "submitStatus"

    const-string v2, "isCanChooseMid"

    const-string v3, "choose_Seats"

    const-string v4, "canChooseSeats"

    const-string v5, "canChooseBeds"

    const-string v6, "data"

    const/4 v7, -0x1

    if-eqz p1, :cond_c

    .line 613
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0xa

    if-ge v8, v9, :cond_0

    goto/16 :goto_2

    .line 618
    :cond_0
    new-instance v8, Lorg/json/JSONTokener;

    invoke-direct {v8, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 620
    :try_start_0
    invoke-virtual {v8}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_b

    .line 621
    instance-of v8, p1, Lorg/json/JSONObject;

    if-nez v8, :cond_1

    goto/16 :goto_1

    .line 626
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v8, "messages"

    .line 627
    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONArray;

    const-string v9, ""

    .line 628
    iput-object v9, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 629
    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v10, v11, :cond_2

    .line 630
    iget-object v11, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v8, v10}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_2
    const-string v8, "status"

    .line 633
    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    return v7

    .line 638
    :cond_3
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    return v7

    .line 642
    :cond_4
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 645
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 646
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseBeds:Ljava/lang/String;

    .line 648
    :cond_5
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 649
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseSeats:Ljava/lang/String;

    .line 652
    :cond_6
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 653
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->choose_Seats:Ljava/lang/String;

    .line 656
    :cond_7
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 657
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->isCanChooseMid:Ljava/lang/String;

    :cond_8
    const-string v2, "ifShowPassCode"

    .line 660
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->ifShowPassCode:Ljava/lang/String;

    const-string v2, "ifShowPassCodeTime"

    .line 661
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->ifShowPassCodeTime:Ljava/lang/String;

    .line 663
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 664
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 666
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 667
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 668
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const-string v0, "\u9a8c\u8bc1\u7801"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_9

    const/4 p1, -0x5

    return p1

    :cond_9
    return v7

    :cond_a
    return v9

    .line 622
    :cond_b
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v7

    :catch_0
    move-exception p1

    .line 676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 678
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    const/4 p1, -0x3

    return p1

    .line 614
    :cond_c
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v7
.end method

.method private parseConfirmSingleForQueue(Ljava/lang/String;)Z
    .locals 6

    const-string v0, "data"

    const/4 v1, 0x0

    if-eqz p1, :cond_8

    .line 1223
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    goto/16 :goto_2

    .line 1228
    :cond_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 1230
    :try_start_0
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 1231
    instance-of v2, p1, Lorg/json/JSONObject;

    if-nez v2, :cond_1

    goto :goto_1

    .line 1236
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v2, "messages"

    .line 1237
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONArray;

    const-string v3, ""

    .line 1238
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v3, 0x0

    .line 1239
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 1240
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "status"

    .line 1243
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    :cond_3
    const-string v2, "repeatSubmitToken"

    .line 1248
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1249
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1250
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    .line 1253
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    return v1

    .line 1257
    :cond_5
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "submitStatus"

    .line 1258
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "errMsg"

    .line 1261
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v1

    :cond_6
    const/4 p1, 0x1

    return p1

    .line 1232
    :cond_7
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception p1

    .line 1269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 1271
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v1

    .line 1224
    :cond_8
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v1
.end method

.method private parseGetPassengerDTOSRequest(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    .line 428
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto :goto_2

    .line 433
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 435
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 436
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_1

    .line 440
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 441
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    const-string v2, ""

    .line 442
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v2, 0x0

    .line 443
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 444
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "status"

    .line 447
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x1

    return p1

    .line 437
    :cond_4
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v1, 0x7f0d010e

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    .line 454
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 456
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v0

    .line 429
    :cond_5
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v0
.end method

.method private parseGetQueueCount(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "isRelogin"

    const-string v1, "data"

    const/4 v2, 0x0

    if-eqz p1, :cond_8

    .line 1022
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 1027
    :cond_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 1029
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 1030
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_1

    .line 1035
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "messages"

    .line 1036
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONArray;

    const-string v4, ""

    .line 1037
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v4, 0x0

    .line 1038
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 1039
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "status"

    .line 1042
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    return v2

    .line 1047
    :cond_3
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    return v2

    .line 1051
    :cond_4
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 1052
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_5

    .line 1053
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Y"

    .line 1054
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v3

    return p1

    .line 1058
    :cond_5
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const-string v1, "ticket"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    const-string v0, "op_2"

    .line 1062
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "true"

    .line 1067
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 1068
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d0101

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v2

    :cond_6
    return v3

    .line 1031
    :cond_7
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception p1

    .line 1074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 1076
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v2

    .line 1023
    :cond_8
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v2
.end method

.method private parseInitDc(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_9

    .line 307
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto/16 :goto_1

    .line 313
    :cond_0
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "/otn/dynamicJs/"

    .line 314
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v2, "\""

    .line 315
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-lez v1, :cond_1

    if-lez v2, :cond_1

    add-int/lit8 v1, v1, 0x5

    .line 318
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mDynamicJS:Ljava/lang/String;

    .line 320
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mDynamicJS:Ljava/lang/String;

    const-string v2, "OrderTicketModel"

    if-nez v1, :cond_2

    .line 321
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initDC ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "InitDC  dynmaic js ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mDynamicJS:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "var globalRepeatSubmitToken = \'"

    .line 331
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_8

    add-int/lit8 v1, v1, 0x1f

    const/16 v3, 0x27

    .line 333
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    if-lez v4, :cond_3

    .line 335
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    :cond_3
    const-string v1, "\'isAsync\':\'"

    .line 340
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v4, 0x1

    if-lez v1, :cond_5

    add-int/lit8 v1, v1, 0xb

    .line 342
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    if-lez v5, :cond_5

    .line 344
    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mIsAsync:Z

    :cond_5
    const-string v1, "\'key_check_isChange\':\'"

    .line 348
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_6

    add-int/lit8 v1, v1, 0x16

    .line 350
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    if-lez v5, :cond_6

    .line 352
    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->key_check_isChange:Ljava/lang/String;

    :cond_6
    const-string v1, "\'train_location\':\'"

    .line 359
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_7

    add-int/lit8 v1, v1, 0x12

    .line 361
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    if-lez v3, :cond_7

    .line 363
    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    .line 366
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "repeat_submit_token="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " key_check_isChange="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->key_check_isChange:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " train_location="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    if-eqz p1, :cond_8

    .line 370
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_8

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->key_check_isChange:Ljava/lang/String;

    if-eqz p1, :cond_8

    .line 372
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_8

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    if-eqz p1, :cond_8

    .line 373
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_8

    const/4 v0, 0x1

    :cond_8
    return v0

    .line 308
    :cond_9
    :goto_1
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v0
.end method

.method private parseMyOrder(Ljava/lang/String;)Ljava/util/Vector;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v3, "confirm_flag"

    const-string v4, "print_eticket_flag"

    const-string v5, "return_flag"

    const-string v6, "cancel_flag"

    const-string v7, "sequence_no"

    const-string v8, "data"

    const-string v9, "exception="

    const-string v10, "OrderTicketModel"

    const/4 v11, 0x0

    if-eqz v2, :cond_f

    .line 1499
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v12

    const/16 v13, 0xa

    if-ge v12, v13, :cond_0

    goto/16 :goto_d

    .line 1504
    :cond_0
    new-instance v12, Lorg/json/JSONTokener;

    invoke-direct {v12, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 1506
    :try_start_0
    invoke-virtual {v12}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a

    if-eqz v2, :cond_e

    .line 1507
    :try_start_1
    instance-of v12, v2, Lorg/json/JSONObject;

    if-nez v12, :cond_1

    goto/16 :goto_8

    .line 1511
    :cond_1
    check-cast v2, Lorg/json/JSONObject;

    const-string v12, "messages"

    .line 1512
    invoke-virtual {v2, v12}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/json/JSONArray;

    const-string v13, ""

    .line 1513
    iput-object v13, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v14, 0x0

    .line 1514
    :goto_0
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v15
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a

    if-ge v14, v15, :cond_2

    .line 1515
    :try_start_2
    iget-object v15, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v12, v14}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    move-object/from16 v18, v9

    move-object/from16 v17, v10

    goto/16 :goto_b

    :cond_2
    :try_start_3
    const-string v12, "status"

    .line 1518
    invoke-virtual {v2, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    return-object v11

    .line 1523
    :cond_3
    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a

    const v13, 0x7f0d0100

    if-nez v12, :cond_6

    .line 1524
    :try_start_4
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 1525
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_5
    return-object v11

    .line 1530
    :cond_6
    :try_start_5
    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    const-string v12, "orderDBList"

    .line 1531
    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a

    if-nez v12, :cond_7

    :try_start_6
    const-string v12, "OrderDTODataList"

    .line 1533
    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :cond_7
    if-eqz v12, :cond_d

    .line 1536
    :try_start_7
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-nez v8, :cond_8

    move-object/from16 v18, v9

    move-object/from16 v17, v10

    goto/16 :goto_6

    .line 1542
    :cond_8
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    const/4 v8, 0x0

    .line 1543
    :goto_1
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-ge v8, v13, :cond_c

    .line 1544
    invoke-virtual {v12, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 1545
    new-instance v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;

    invoke-direct {v14}, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;-><init>()V

    .line 1546
    invoke-virtual {v13, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    const-string v15, "order_date"

    .line 1547
    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_date:Ljava/lang/String;

    const-string v15, "ticket_totalnum"

    .line 1548
    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_totalnum:I

    const-string v15, "ticket_price_all"

    move-object/from16 v16, v12

    .line 1549
    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v11

    iput-wide v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_price_all:D

    .line 1550
    invoke-virtual {v13, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->cancel_flag:Ljava/lang/String;

    const-string v11, "resign_flat"

    .line 1551
    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->resign_flag:Ljava/lang/String;

    .line 1552
    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->return_flag:Ljava/lang/String;

    .line 1553
    invoke-virtual {v13, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->print_eticket_flag:Ljava/lang/String;

    const-string v11, "pay_flag"

    .line 1554
    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->pay_flag:Ljava/lang/String;

    const-string v11, "pay_resign_flag"

    .line 1555
    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->pay_resign_flag:Ljava/lang/String;

    .line 1556
    invoke-virtual {v13, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->confirm_flag:Ljava/lang/String;

    const-string v11, "tickets"

    .line 1558
    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 1559
    new-instance v12, Ljava/util/Vector;

    invoke-direct {v12}, Ljava/util/Vector;-><init>()V

    iput-object v12, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    const-string v12, "start_train_date_page"

    if-eqz v11, :cond_a

    move-object/from16 v17, v10

    const/4 v15, 0x0

    .line 1561
    :goto_2
    :try_start_8
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v15, v10, :cond_9

    .line 1562
    new-instance v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    invoke-direct {v10}, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;-><init>()V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move-object/from16 v18, v9

    .line 1563
    :try_start_9
    invoke-virtual {v11, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    move-object/from16 v19, v11

    .line 1564
    new-instance v11, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    invoke-direct {v11}, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;-><init>()V

    iput-object v11, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v11, "stationTrainDTO"

    .line 1565
    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 1566
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    move/from16 v20, v8

    const-string v8, "station_train_code"

    .line 1567
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    .line 1568
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "from_station_telecode"

    .line 1569
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_telecode:Ljava/lang/String;

    .line 1570
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "from_station_name"

    .line 1571
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    .line 1572
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "start_time"

    .line 1573
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->start_time:Ljava/lang/String;

    .line 1574
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "to_station_telecode"

    .line 1575
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_telecode:Ljava/lang/String;

    .line 1576
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "to_station_name"

    .line 1577
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    .line 1578
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "arrive_time"

    .line 1579
    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->arrive_time:Ljava/lang/String;

    .line 1580
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v8, "distance"

    invoke-virtual {v11, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->distance:Ljava/lang/String;

    .line 1584
    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;-><init>()V

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v1, "passengerDTO"

    .line 1585
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1586
    iget-object v8, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v11, "passenger_name"

    .line 1587
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    .line 1588
    iget-object v8, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v11, "passenger_id_type_code"

    .line 1589
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    .line 1590
    iget-object v8, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v11, "passenger_id_type_name"

    .line 1591
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_name:Ljava/lang/String;

    .line 1592
    iget-object v8, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v11, "passenger_id_no"

    .line 1593
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 1594
    iget-object v8, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v11, "total_times"

    .line 1595
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v1, "ticket_no"

    .line 1605
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_no:Ljava/lang/String;

    .line 1606
    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->sequence_no:Ljava/lang/String;

    const-string v1, "batch_no"

    .line 1607
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->batch_no:Ljava/lang/String;

    const-string v1, "train_date"

    .line 1608
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->train_date:Ljava/lang/String;

    const-string v1, "coach_no"

    .line 1609
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_no:Ljava/lang/String;

    const-string v1, "coach_name"

    .line 1610
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_name:Ljava/lang/String;

    const-string v1, "seat_no"

    .line 1611
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_no:Ljava/lang/String;

    const-string v1, "seat_name"

    .line 1612
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_name:Ljava/lang/String;

    const-string v1, "seat_flag"

    .line 1613
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_flag:Ljava/lang/String;

    const-string v1, "seat_type_code"

    .line 1614
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_type_code:Ljava/lang/String;

    const-string v1, "seat_type_name"

    .line 1615
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_type_name:Ljava/lang/String;

    const-string v1, "ticket_type_code"

    .line 1617
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_type_code:Ljava/lang/String;

    const-string v1, "ticket_type_name"

    .line 1619
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_type_name:Ljava/lang/String;

    const-string v1, "reserve_time"

    .line 1620
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->reserve_time:Ljava/lang/String;

    const-string v1, "limit_time"

    .line 1621
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->limit_time:Ljava/lang/String;

    const-string v1, "lost_time"

    .line 1622
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->lose_time:Ljava/lang/String;

    const-string v1, "pay_limit_time"

    .line 1623
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->pay_limit_time:Ljava/lang/String;

    const-string v1, "ticket_price"

    .line 1624
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_price:Ljava/lang/String;

    .line 1626
    invoke-virtual {v9, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->print_eticket_flag:Ljava/lang/String;

    const-string v1, "resign_flag"

    .line 1627
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->resign_flag:Ljava/lang/String;

    .line 1628
    invoke-virtual {v9, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->return_flag:Ljava/lang/String;

    .line 1629
    invoke-virtual {v9, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->confirm_flag:Ljava/lang/String;

    const-string v1, "pay_mode_code"

    .line 1630
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->pay_mode_code:Ljava/lang/String;

    const-string v1, "ticket_status_code"

    .line 1632
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_status_code:Ljava/lang/String;

    const-string v1, "ticket_status_name"

    .line 1634
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_status_name:Ljava/lang/String;

    .line 1635
    iget-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->ticket_status_name:Ljava/lang/String;

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->order_status:Ljava/lang/String;

    .line 1636
    invoke-virtual {v9, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->cancel_flag:Ljava/lang/String;

    const-string v1, "amount_char"

    .line 1637
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->amount_char:I

    const-string v1, "trade_mode"

    .line 1638
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->trade_mode:Ljava/lang/String;

    .line 1640
    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    const-string v1, "str_ticket_price_page"

    .line 1642
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->str_ticket_price_page:Ljava/lang/String;

    const-string v1, "come_go_traveller_ticket_page"

    .line 1645
    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->come_go_traveller_ticket_page:Ljava/lang/String;

    .line 1646
    iget-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v1, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v1, p0

    move-object/from16 v9, v18

    move-object/from16 v11, v19

    move/from16 v8, v20

    goto/16 :goto_2

    :cond_9
    move/from16 v20, v8

    move-object/from16 v18, v9

    goto :goto_3

    :catch_1
    move-exception v0

    move-object/from16 v18, v9

    goto/16 :goto_5

    :catch_2
    move-exception v0

    move-object/from16 v1, p0

    move-object v2, v0

    move-object v4, v9

    move-object/from16 v5, v17

    goto/16 :goto_c

    :cond_a
    move/from16 v20, v8

    move-object/from16 v18, v9

    move-object/from16 v17, v10

    :goto_3
    const-string v1, "reserve_flag_query"

    .line 1656
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->reserve_flag_query:Ljava/lang/String;

    const-string v1, "if_show_resigning_info"

    .line 1658
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->if_show_resigning_info:Ljava/lang/String;

    const-string v1, "recordCount"

    .line 1659
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->recordCount:Ljava/lang/String;

    const-string v1, "isNeedSendMailAndMsg"

    .line 1661
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->isNeedSendMailAndMsg:Ljava/lang/String;

    const-string v1, "array_passser_name_page"

    .line 1662
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1664
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    iput-object v8, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    const/4 v8, 0x0

    .line 1665
    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v8, v9, :cond_b

    .line 1666
    iget-object v9, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->array_passer_name_page:Ljava/util/Vector;

    invoke-virtual {v1, v8}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_b
    const-string v1, "from_station_name_page"

    .line 1670
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->from_station_name_page:Ljava/lang/String;

    const-string v1, "to_station_name_page"

    .line 1672
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->to_station_name_page:Ljava/lang/String;

    .line 1674
    invoke-virtual {v13, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->start_train_date_page:Ljava/lang/String;

    const-string v1, "start_time_page"

    .line 1675
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->start_time_page:Ljava/lang/String;

    const-string v1, "arrive_time_page"

    .line 1676
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->arrive_time_page:Ljava/lang/String;

    const-string v1, "train_code_page"

    .line 1677
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->train_code_page:Ljava/lang/String;

    const-string v1, "ticket_total_price_page"

    .line 1679
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->ticket_total_price_page:Ljava/lang/String;

    const-string v1, "come_go_traveller_order_page"

    .line 1681
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->com_go_traveller_order_page:Ljava/lang/String;

    const-string v1, "canOffLinePay"

    .line 1682
    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->canOffLinePay:Ljava/lang/String;

    .line 1684
    invoke-virtual {v2, v14}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    add-int/lit8 v8, v20, 0x1

    const/4 v11, 0x0

    move-object/from16 v1, p0

    move-object/from16 v12, v16

    move-object/from16 v10, v17

    move-object/from16 v9, v18

    goto/16 :goto_1

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    move-object/from16 v1, p0

    goto :goto_9

    :cond_c
    return-object v2

    :catch_5
    move-exception v0

    move-object/from16 v18, v9

    move-object/from16 v17, v10

    :goto_5
    move-object/from16 v1, p0

    goto :goto_a

    :catch_6
    move-exception v0

    move-object/from16 v1, p0

    goto :goto_7

    :cond_d
    move-object/from16 v18, v9

    move-object/from16 v17, v10

    move-object/from16 v1, p0

    .line 1537
    :goto_6
    :try_start_a
    invoke-direct {v1, v2}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseOrderCache(Lorg/json/JSONObject;)Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    .line 1538
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v2, 0x0

    return-object v2

    :catch_7
    move-exception v0

    :goto_7
    move-object v2, v0

    move-object v4, v9

    move-object v5, v10

    goto :goto_c

    :cond_e
    :goto_8
    move-object/from16 v18, v9

    move-object/from16 v17, v10

    .line 1508
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d010e

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_9
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    const/4 v2, 0x0

    return-object v2

    :catch_8
    move-exception v0

    goto :goto_a

    :catch_9
    move-exception v0

    :goto_9
    move-object v2, v0

    move-object/from16 v5, v17

    move-object/from16 v4, v18

    goto :goto_c

    :catch_a
    move-exception v0

    move-object/from16 v18, v9

    move-object/from16 v17, v10

    :goto_a
    move-object v2, v0

    .line 1696
    :goto_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v4, v18

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v5, v17

    invoke-static {v5, v3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 1698
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v2, 0x0

    return-object v2

    :catch_b
    move-exception v0

    move-object v4, v9

    move-object v5, v10

    move-object v2, v0

    .line 1691
    :goto_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 1693
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    const/4 v3, 0x0

    return-object v3

    :cond_f
    :goto_d
    move-object v3, v11

    .line 1500
    iput-object v2, v1, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return-object v3
.end method

.method private parseMyOrderComplete(Ljava/lang/String;)Z
    .locals 0

    .line 1842
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseMyOrder(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mCompleteOrderList:Ljava/util/Vector;

    .line 1843
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mCompleteOrderList:Ljava/util/Vector;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private parseMyOrderNoComplete(Ljava/lang/String;)Z
    .locals 0

    .line 1847
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseMyOrder(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNoCompleteOrderList:Ljava/util/Vector;

    .line 1848
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNoCompleteOrderList:Ljava/util/Vector;

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private parseOrderCache(Lorg/json/JSONObject;)Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;
    .locals 8

    const-string v0, "message"

    const-string v1, "orderCacheDTO"

    const/4 v2, 0x0

    if-eqz p1, :cond_7

    const-string v3, "data"

    .line 1752
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_3

    .line 1756
    :cond_0
    :try_start_0
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 1757
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    goto/16 :goto_2

    .line 1761
    :cond_1
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 1762
    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;-><init>()V

    const-string v3, "requestId"

    .line 1763
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->requestId:Ljava/lang/String;

    const-string v3, "userId"

    .line 1764
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->userId:Ljava/lang/String;

    const-string v3, "number"

    .line 1765
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->number:I

    const-string v3, "tourFlag"

    .line 1766
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->tourFlag:Ljava/lang/String;

    const-string v3, "requestTime"

    .line 1767
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->requestTime:Ljava/lang/String;

    const-string v3, "queueOffset"

    .line 1768
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->queueOffset:Ljava/lang/String;

    const-string v3, "queueName"

    .line 1769
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->queueName:Ljava/lang/String;

    const-string v3, "trainDate"

    .line 1770
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->trainDate:Ljava/lang/String;

    const-string v3, "startTime"

    .line 1771
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->startTime:Ljava/lang/String;

    const-string v3, "stationTrainCode"

    .line 1772
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->stationTrainCode:Ljava/lang/String;

    const-string v3, "fromStationCode"

    .line 1773
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->fromStationCode:Ljava/lang/String;

    const-string v3, "fromStationName"

    .line 1774
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->fromStationName:Ljava/lang/String;

    const-string v3, "toStationCode"

    .line 1775
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->toStationCode:Ljava/lang/String;

    const-string v3, "toStationName"

    .line 1776
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->toStationName:Ljava/lang/String;

    const-string v3, "status"

    .line 1777
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->status:I

    .line 1779
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1781
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->message:Ljava/lang/String;

    :cond_2
    const-string v0, "modifyTime"

    .line 1783
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->modifyTime:Ljava/lang/String;

    .line 1784
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->tickets:Ljava/util/Vector;

    const-string v0, "tickets"

    .line 1785
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_4

    const/4 v4, 0x0

    .line 1787
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 1788
    new-instance v5, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;

    invoke-direct {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;-><init>()V

    .line 1789
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_3

    const-string v7, "passengerIdTypeName"

    .line 1791
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;->passengerIdTypeName:Ljava/lang/String;

    const-string v7, "passengerName"

    .line 1792
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;->passengerName:Ljava/lang/String;

    const-string v7, "seatTypeCode"

    .line 1793
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;->seatTypeCode:Ljava/lang/String;

    const-string v7, "ticketTypeName"

    .line 1794
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO$Ticket;->ticketTypeName:Ljava/lang/String;

    .line 1795
    iget-object v6, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v6, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    const-string v0, "waitTime"

    .line 1799
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->waitTime:I

    const-string v0, "waitCount"

    .line 1800
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->waitCount:I

    const-string v0, "ticketCount"

    .line 1801
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->ticketCount:I

    const-string v0, "startTimeString"

    .line 1802
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->startTimeString:Ljava/lang/String;

    .line 1803
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->array_passer_name_page:Ljava/util/Vector;

    const-string v0, "array_passser_name_page"

    .line 1804
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1806
    :goto_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 1807
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;->array_passer_name_page:Ljava/util/Vector;

    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1811
    :cond_5
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_6
    :goto_2
    return-object v2

    :catch_0
    move-exception p1

    .line 1814
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_7
    :goto_3
    return-object v2
.end method

.method private parseQueryOrderWaitTime(Ljava/lang/String;)I
    .locals 11

    const-string v0, "orderId"

    const-string v1, "waitTime"

    const-string v2, "data"

    const/4 v3, -0x1

    if-eqz p1, :cond_10

    .line 739
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_0

    goto/16 :goto_2

    :cond_0
    const/4 v4, 0x0

    .line 744
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderId:Ljava/lang/String;

    .line 745
    new-instance v4, Lorg/json/JSONTokener;

    invoke-direct {v4, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 747
    :try_start_0
    invoke-virtual {v4}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_f

    .line 748
    instance-of v4, p1, Lorg/json/JSONObject;

    if-nez v4, :cond_1

    goto/16 :goto_1

    .line 753
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v4, "messages"

    .line 754
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONArray;

    const-string v5, ""

    .line 755
    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 756
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 757
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    const-string v4, "status"

    .line 760
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    return v3

    .line 765
    :cond_3
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    return v3

    .line 770
    :cond_4
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v2, "queryOrderWaitTimeStatus"

    .line 772
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    return v3

    .line 777
    :cond_5
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    return v3

    .line 788
    :cond_6
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    .line 790
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    const-wide/16 v6, -0x64

    const-wide/16 v8, 0x1

    cmp-long v4, v1, v6

    if-nez v4, :cond_7

    .line 791
    iput-wide v8, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    .line 794
    :cond_7
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    const-wide/16 v6, 0x3

    mul-long v1, v1, v6

    const-wide/16 v6, 0x2

    div-long/2addr v1, v6

    const-wide/16 v6, 0x3c

    cmp-long v4, v1, v6

    if-lez v4, :cond_8

    move-wide v1, v6

    .line 796
    :cond_8
    iget-wide v6, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    sub-long v1, v6, v1

    const-wide/16 v6, 0x0

    cmp-long v4, v1, v6

    if-gtz v4, :cond_9

    move-wide v1, v8

    .line 797
    :cond_9
    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNextRequestTime:J

    .line 799
    iget-wide v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, -0x2

    const-string v8, "waitCount"

    const/4 v9, 0x1

    cmp-long v10, v1, v6

    if-lez v10, :cond_a

    .line 800
    :try_start_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d014e

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 801
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v5

    iget-wide v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v2, v9

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v4

    .line 805
    :cond_a
    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_b

    .line 807
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d014d

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 808
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v0, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v4

    :cond_b
    const-string v1, "msg"

    .line 812
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const-string v1, "errorcode"

    .line 814
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    return v3

    .line 817
    :cond_c
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    return v3

    .line 821
    :cond_d
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderId:Ljava/lang/String;

    .line 822
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderId:Ljava/lang/String;

    if-eqz p1, :cond_e

    :cond_e
    return v5

    .line 749
    :cond_f
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    return v3

    :catch_0
    move-exception p1

    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 830
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v3

    .line 740
    :cond_10
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v3
.end method

.method private parseResginTicket(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "exception="

    const-string v1, "OrderTicketModel"

    const/4 v2, 0x0

    if-eqz p1, :cond_8

    .line 2374
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 2378
    :cond_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 2380
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 2381
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_1

    .line 2384
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "messages"

    .line 2385
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const-string v4, ""

    .line 2386
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2387
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2388
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "status"

    .line 2391
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    return v2

    :cond_3
    const-string v3, "message"

    .line 2396
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2397
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2398
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v2

    :cond_4
    const-string v3, "data"

    .line 2402
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_5

    return v2

    :cond_5
    const-string v3, "existError"

    .line 2407
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Y"

    .line 2409
    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v4, "errorMsg"

    .line 2411
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    xor-int/lit8 p1, v3, 0x1

    return p1

    :cond_7
    :goto_1
    return v2

    :catch_0
    move-exception p1

    .line 2424
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2426
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return v2

    :catch_1
    move-exception p1

    .line 2418
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2419
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2420
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_8
    :goto_2
    return v2
.end method

.method private parseReturnTicket(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "exception="

    const-string v1, "OrderTicketModel"

    const/4 v2, 0x0

    if-eqz p1, :cond_8

    .line 2157
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 2161
    :cond_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 2163
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 2164
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto :goto_1

    .line 2167
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "messages"

    .line 2168
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONArray;

    const-string v4, ""

    .line 2169
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2170
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2171
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "status"

    .line 2174
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    return v2

    :cond_3
    const-string v3, "message"

    .line 2179
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2180
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2181
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v2

    :cond_4
    const-string v3, "data"

    .line 2185
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const/4 v3, 0x1

    if-nez p1, :cond_5

    return v3

    :cond_5
    const-string v4, "errMsg"

    .line 2190
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2191
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2192
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :cond_6
    return v3

    :cond_7
    :goto_1
    return v2

    :catch_0
    move-exception p1

    .line 2206
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2207
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2208
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return v2

    :catch_1
    move-exception p1

    .line 2200
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2202
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_8
    :goto_2
    return v2
.end method

.method private parseReturnTicketAffirm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;
    .locals 7

    const-string v0, "exception="

    const-string v1, "OrderTicketModel"

    const/4 v2, 0x0

    if-eqz p1, :cond_9

    .line 2033
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    goto/16 :goto_2

    .line 2037
    :cond_0
    new-instance v3, Lorg/json/JSONTokener;

    invoke-direct {v3, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 2039
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 2040
    instance-of v3, p1, Lorg/json/JSONObject;

    if-nez v3, :cond_1

    goto/16 :goto_1

    .line 2044
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v3, "messages"

    .line 2045
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONArray;

    const-string v4, ""

    .line 2046
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2047
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2048
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "status"

    .line 2051
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    return-object v2

    :cond_3
    const-string v3, "data"

    .line 2056
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_4

    return-object v2

    :cond_4
    const-string v3, "errMsg"

    .line 2061
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2062
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2063
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return-object v2

    :cond_5
    const-string v3, "stationTrainDTO"

    .line 2067
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-nez v3, :cond_6

    return-object v2

    .line 2072
    :cond_6
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;-><init>()V

    .line 2074
    new-instance v5, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    invoke-direct {v5}, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;-><init>()V

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    .line 2075
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v6, "station_train_code"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    .line 2076
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v6, "from_station_name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    .line 2077
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v6, "start_time"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->start_time:Ljava/lang/String;

    .line 2078
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    const-string v6, "to_station_name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    const-string v3, "passengerDTO"

    .line 2081
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-nez v3, :cond_7

    return-object v2

    .line 2086
    :cond_7
    new-instance v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {v5}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;-><init>()V

    iput-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 2087
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v6, "passenger_name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    .line 2088
    iget-object v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const-string v6, "total_times"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v3, "ticket_no"

    .line 2090
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->ticket_no:Ljava/lang/String;

    const-string v3, "sequence_no"

    .line 2091
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->sequence_no:Ljava/lang/String;

    const-string v3, "batch_no"

    .line 2092
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->batch_no:Ljava/lang/String;

    const-string v3, "train_date"

    .line 2094
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->train_date:Ljava/lang/String;

    const-string v3, "coach_no"

    .line 2095
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->coach_no:Ljava/lang/String;

    const-string v3, "coach_name"

    .line 2096
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->coach_name:Ljava/lang/String;

    const-string v3, "seat_no"

    .line 2098
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->seat_no:Ljava/lang/String;

    const-string v3, "seat_name"

    .line 2099
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->seat_name:Ljava/lang/String;

    const-string v3, "seat_type_name"

    .line 2100
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->seat_type_name:Ljava/lang/String;

    const-string v3, "ticket_price"

    .line 2102
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v5

    iput-wide v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->ticket_price:D

    const-string v3, "return_price"

    .line 2103
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v5

    iput-wide v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->return_price:D

    const-string v3, "return_cost"

    .line 2104
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v5

    iput-wide v5, v4, Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;->return_cost:D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    :cond_8
    :goto_1
    return-object v2

    :catch_0
    move-exception p1

    .line 2116
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2117
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2118
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v2

    :catch_1
    move-exception p1

    .line 2110
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 2112
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_9
    :goto_2
    return-object v2
.end method

.method private parseSubmitOrderRequest(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 522
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto :goto_2

    .line 527
    :cond_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 529
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 530
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_1

    .line 535
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 536
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    const-string v2, ""

    .line 537
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    const/4 v2, 0x0

    .line 538
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 539
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "status"

    .line 542
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 531
    :cond_3
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v1, 0x7f0d010e

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    .line 545
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    .line 547
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v0

    .line 523
    :cond_4
    :goto_2
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return v0
.end method

.method private toJSONObject()Lorg/json/JSONObject;
    .locals 4

    .line 179
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 180
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 181
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    .line 182
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v3

    .line 184
    :try_start_0
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 187
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public canBuyStudentTicket()Z
    .locals 3

    .line 1330
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1331
    :cond_0
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x5

    .line 1332
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "06-01"

    .line 1333
    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    const-string v2, "09-30"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    :cond_1
    const-string v2, "12-01"

    .line 1334
    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_2

    const-string v2, "03-31"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method public canChooseBeds()Z
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseBeds:Ljava/lang/String;

    const-string v1, "Y"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public canChooseMid()Z
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->isCanChooseMid:Ljava/lang/String;

    const-string v1, "Y"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public canChooseSeats()Z
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->canChooseSeats:Ljava/lang/String;

    const-string v1, "Y"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public cancelNoCompleteMyOrder(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1879
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&cancel_flag=cancel_order"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1884
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cancelNoCompleteMyOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1893
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseCancelOrderResult(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 1886
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1887
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_1

    .line 1888
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 1889
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public cancelQueueNoCompleteMyOrder()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 2231
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const-string v1, "dc"

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    if-eqz v0, :cond_0

    .line 2232
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTourFlag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 2235
    :goto_0
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 2239
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tourFlag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2244
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CANCEL_QUEUE_NOCOMPLETE_MYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelQueueNoCompleteMyOrder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseCancelQueueMyOrderNoComplete(Ljava/lang/String;)Z

    move-result v0

    return v0

    :catch_0
    move-exception v0

    .line 2246
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2247
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_2

    .line 2248
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 2249
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public checkOrderInfo(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 567
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mInitDCStr:Ljava/lang/String;

    .line 570
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 571
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v1

    .line 573
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancel_flag=2&bed_level_order_num=000000000000000000000000000000"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&passengerTicketStr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getPassengerTicketStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 574
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&oldPassengerStr="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOldPassengerStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 575
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&tour_flag="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&randCode="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 577
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&whatsSelect=1"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_0

    .line 578
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 579
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&_json_att="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 580
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 585
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CHECKORDERINFO:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 592
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "checkOrderInfo="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "OrderTicketModel"

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseCheckOrderInfo(Ljava/lang/String;)I

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 587
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 588
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 589
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 590
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const v0, 0x7f0d00f8

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public confirmSingle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1100
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    .line 1103
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1105
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 1108
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 1109
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v1

    .line 1110
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "passengerTicketStr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getPassengerTicketStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1111
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&oldPassengerStr="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOldPassengerStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&randCode="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "&purpose_codes="

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1114
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&key_check_isChange="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->key_check_isChange:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1115
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&leftTicketStr="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1116
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_location="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1117
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&choose_seats="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1118
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&seatDetailType=000"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1119
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&roomType=00"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1120
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&dwAll=N"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1121
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1126
    :try_start_1
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1133
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "confirmSingleForQueue="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "OrderTicketModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseConfirmSingleForQueue(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_1
    move-exception p1

    .line 1128
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1129
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 1130
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 1131
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public confirmSingleForQueue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1175
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    .line 1178
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1180
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 1183
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 1184
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v1

    .line 1185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "passengerTicketStr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getPassengerTicketStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1186
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&oldPassengerStr="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOldPassengerStr(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&randCode="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "&purpose_codes="

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1189
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&key_check_isChange="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->key_check_isChange:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1190
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&leftTicketStr="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1191
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_location="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1192
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&choose_seats="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1193
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&seatDetailType=000"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1194
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&whatsSelect=1"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1195
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&roomType=00"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1196
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&dwAll=N"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1197
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1202
    :try_start_1
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->CONFIRMSINGLEFORQUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1209
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "confirmSingleForQueue="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "OrderTicketModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseConfirmSingleForQueue(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_1
    move-exception p1

    .line 1204
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1205
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 1206
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 1207
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getDisplayTime()J
    .locals 2

    .line 685
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mWaitTime:J

    return-wide v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 2

    .line 1938
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00ae

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1941
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getIfShowPassCode()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->ifShowPassCode:Ljava/lang/String;

    return-object v0
.end method

.method public getMyOrderComplete()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation

    .line 1393
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mCompleteOrderList:Ljava/util/Vector;

    return-object v0
.end method

.method public getMyOrderNoComplete()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;",
            ">;"
        }
    .end annotation

    .line 1435
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNoCompleteOrderList:Ljava/util/Vector;

    return-object v0
.end method

.method public getNextRequestTime()J
    .locals 2

    .line 692
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNextRequestTime:J

    return-wide v0
.end method

.method public getOrderCacheDTO()Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;
    .locals 1

    .line 1704
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    return-object v0
.end method

.method public getOrderConfigMgr(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/OrderConfigMgr;-><init>()V

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderMgrs:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getOrderId()Ljava/lang/String;
    .locals 1

    .line 1389
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderId:Ljava/lang/String;

    return-object v0
.end method

.method public getPassCodeNew()I
    .locals 3

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "module=passenger&rand=randp&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 385
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_RANGCODE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 387
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getPassengerDTOs()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 394
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 399
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_json_att="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETPASSENGERDTOS:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v2, v3, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPassengerDTOs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OrderTicketModel"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseGetPassengerDTOSRequest(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 405
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 406
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_2

    .line 407
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 408
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getQueueCount(Ljava/lang/String;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 919
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 923
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 927
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    const/4 v0, 0x0

    .line 931
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 932
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v1

    .line 934
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_3

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 948
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    .line 951
    :try_start_1
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->yp_info:Ljava/lang/String;

    const-string v4, "utf-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    .line 953
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 965
    :goto_1
    new-instance v3, Ljava/text/SimpleDateFormat;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "EEE MMM dd yyyy HH:mm:ss"

    invoke-direct {v3, v5, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 967
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "+GMT%2B0800+(CST)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 968
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "train_date="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 969
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&train_no="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&stationTrainCode="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&seatType="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->seat_types:Ljava/lang/String;

    .line 971
    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getRealSeatTypeFromName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 973
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&fromStationTelecode="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_telecode:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&toStationTelecode="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_telecode:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 975
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&leftTicket="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&train_location="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->train_location:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&purpose_codes="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 984
    :try_start_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GETQUEUECOUNT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 991
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getQueueCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseGetQueueCount(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    return v2

    :cond_1
    const/4 p1, 0x0

    return p1

    :catch_2
    move-exception p1

    .line 986
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 987
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_2

    .line 988
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 989
    :cond_2
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_2
    return v2
.end method

.method public getToken()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "_json_att"

    .line 1290
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITDC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    .line 1292
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1293
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_0

    .line 1294
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 1295
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 2

    const-string v0, "OrderTicketModel"

    const-string v1, "init"

    .line 109
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "order_histroy"

    const/4 v1, 0x0

    .line 110
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mPref:Landroid/content/SharedPreferences;

    .line 111
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mPref:Landroid/content/SharedPreferences;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "history"

    const-string v1, ""

    .line 113
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 114
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 116
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 118
    instance-of v0, p1, Lorg/json/JSONObject;

    if-nez v0, :cond_1

    goto :goto_0

    .line 122
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->initOrderConfigMgr(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception p1

    .line 125
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    :goto_1
    return-void
.end method

.method public initDc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "_json_att="

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITDC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseInitDc(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, -0x1

    return v0

    .line 293
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mInitDCStr:Ljava/lang/String;

    .line 296
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mInitDCStr:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :cond_1
    return v1

    :catch_0
    move-exception v0

    .line 283
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 284
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_2

    .line 285
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 286
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public initGc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, "_json_att="

    .line 247
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->INITGC:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseInitDc(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, -0x1

    return v0

    .line 259
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    .line 260
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mInitDCStr:Ljava/lang/String;

    .line 262
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mInitDCStr:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :cond_1
    return v1

    :catch_0
    move-exception v0

    .line 249
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 250
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_2

    .line 251
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 252
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isAsync()Z
    .locals 1

    .line 105
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mIsAsync:Z

    return v0
.end method

.method public queryMyOrder(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1407
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mCompleteOrderList:Ljava/util/Vector;

    .line 1410
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "&queryStartDate="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&queryEndDate="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&come_from_flag=my_order&pageSize=100&pageIndex=0&query_where="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&sequeue_train_name="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1417
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p3, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERYMYORDER:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p3, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1425
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryMyOrder="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "OrderTicketModel"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseMyOrderComplete(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 1419
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1420
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 1421
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 1422
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const p3, 0x7f0d00f8

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public queryMyOrderNoComplete()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1445
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mNoCompleteOrderList:Ljava/util/Vector;

    .line 1446
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->orderCacheDTO:Lcom/lltskb/lltskb/engine/online/dto/OrderCacheDTO;

    const-string v0, "_json_att="

    .line 1451
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_MYORDER_NOCOMPLETE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    .line 1452
    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1460
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryMyOrderNoComplete="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseMyOrderNoComplete(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 1454
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1455
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 1456
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 1457
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public queryOrderWaitTime(Ljava/lang/String;)I
    .locals 3

    .line 703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "random="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "&tourFlag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 709
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_ORDER_WAITTIME:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 711
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const-string p1, ""

    .line 714
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryOrderWaitTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseQueryOrderWaitTime(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public queryPayOrder()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1821
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 1822
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "random="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1828
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PAYORDER_INIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1836
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryPayOrder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 1830
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1831
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_0

    .line 1832
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 1833
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public resginTicket(Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;Ljava/lang/String;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 2330
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2331
    :goto_0
    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2332
    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->tickets:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;

    .line 2333
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->sequence_no:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2334
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->batch_no:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2335
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_no:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2336
    iget-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_no:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2337
    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2341
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2343
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2345
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ticketKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2346
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&sequenceNo="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderDBDTO;->sequence_no:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&changeTSFlag="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2352
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESGIN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2360
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "cancelQueueNoCompleteMyOrder="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "OrderTicketModel"

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2363
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseResginTicket(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 2354
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2355
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_2

    .line 2356
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 2357
    :cond_2
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const v0, 0x7f0d00f8

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method public resultOrderForDcQueue()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1857
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "orderSequence_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&_json_att=&REPEAT_SUBMIT_TOKEN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->REPEAT_SUBMIT_TOKEN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1863
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RESULT_ORDER_FOR_DC_QUEUE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resultOrderForDcQueue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 1865
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1866
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_0

    .line 1867
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 1868
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public returnTicket()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 2133
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnTicket="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OrderTicketModel"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2142
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseReturnTicket(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    .line 2135
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2136
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_1

    .line 2137
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 2138
    :cond_1
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public returnTicketAffirm(Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;)Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 1964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->sequence_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&batch_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->batch_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&coach_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&seat_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&start_train_date_page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->start_train_date_page:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&train_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&coach_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&seat_name="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&seat_type_name="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->seat_type_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&train_date="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->train_date:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&from_station_name="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&to_station_name="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&start_time="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->stationTrainDTO:Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/StationTrainDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->coach_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/TicketInfo;->passengerDTO:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1980
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "returnTicketAffirm param="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderTicketModel"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->RETURN_TICKET_AFFIRM:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1993
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseReturnTicketAffirm(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/ReturnTicketDTO;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 1987
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1988
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_0

    .line 1989
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 1990
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public save()V
    .locals 3

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mPref:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    return-void

    .line 133
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 138
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "history"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 140
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setOrderParams(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    return-void
.end method

.method public submitInit()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const-string v0, ""

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITINIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x3

    return v0

    .line 222
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/DynamicJs;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/online/DynamicJs;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/DynamicJs;->getDynamicParam()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mSubmitInitStr:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mSubmitInitStr:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :cond_1
    return v1

    :catch_0
    move-exception v0

    .line 211
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 212
    instance-of v1, v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v1, :cond_2

    .line 213
    check-cast v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw v0

    .line 214
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00f8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public submitOrderRequest(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 478
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const/4 v1, -0x1

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mSubmitInitStr:Ljava/lang/String;

    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "secretStr="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->secretStr:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&train_date="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&back_train_date="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->back_take_date:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&tour_flag="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "&purpose_codes="

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&query_from_station_name="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 490
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&query_to_station_name="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&undefined"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 496
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v0, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->SUBMITORDERREQUEST:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, v0, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "submitOrderRequest="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "OrderTicketModel"

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->parseSubmitOrderRequest(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 498
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 499
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_2

    .line 500
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 501
    :cond_2
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    const v0, 0x7f0d00f8

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    return v1
.end method
