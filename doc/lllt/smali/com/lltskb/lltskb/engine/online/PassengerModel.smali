.class public Lcom/lltskb/lltskb/engine/online/PassengerModel;
.super Ljava/lang/Object;
.source "PassengerModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PassengerModel"

.field private static instance:Lcom/lltskb/lltskb/engine/online/PassengerModel;


# instance fields
.field private mErrorMsg:Ljava/lang/String;

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

.field private mPageTotal:I

.field private mPassengers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedPassengers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 28
    iput v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    .line 39
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    .line 40
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    .line 41
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/PassengerModel;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/PassengerModel;

    monitor-enter v0

    .line 32
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/PassengerModel;->instance:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Lcom/lltskb/lltskb/engine/online/PassengerModel;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/PassengerModel;->instance:Lcom/lltskb/lltskb/engine/online/PassengerModel;

    .line 35
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/PassengerModel;->instance:Lcom/lltskb/lltskb/engine/online/PassengerModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getBornDate(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;
    .locals 5

    const-string v0, "2008-02-12"

    if-nez p1, :cond_0

    return-object v0

    .line 495
    :cond_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->born_date:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/16 v2, 0xa

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->born_date:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 496
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->born_date:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 499
    :cond_1
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x12

    if-ge v1, v3, :cond_2

    goto :goto_0

    .line 503
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const/16 v4, 0xc

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const/16 v1, 0xe

    .line 504
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    return-object v0
.end method

.method private getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .line 201
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 203
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p2

    if-gez p2, :cond_1

    return-object v1

    :cond_1
    if-eqz p3, :cond_2

    const-string p3, "\"selected\""

    .line 207
    invoke-virtual {p1, p3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result p2

    if-gez p2, :cond_2

    return-object v1

    :cond_2
    const-string p3, "value=\""

    .line 210
    invoke-virtual {p1, p3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result p2

    if-gez p2, :cond_3

    return-object v1

    :cond_3
    add-int/lit8 p2, p2, 0x7

    const-string p3, "\""

    .line 212
    invoke-virtual {p1, p3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result p3

    if-gez p3, :cond_4

    return-object v1

    .line 214
    :cond_4
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private init()V
    .locals 3

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    const-string v1, "https://kyfw.12306.cn/otn/passengers/init"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 56
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private parseAddDelPassengerResult(Ljava/lang/String;)Z
    .locals 6

    const-string v0, "data"

    .line 544
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    return v2

    .line 555
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "response="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PassengerModel"

    invoke-static {v3, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 558
    :try_start_0
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 559
    instance-of v1, p1, Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_1

    .line 564
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v1, "messages"

    .line 565
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    if-nez v1, :cond_2

    return v2

    :cond_2
    const-string v3, ""

    .line 567
    iput-object v3, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    .line 568
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    .line 569
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 570
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 572
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    const-string v1, "status"

    .line 574
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    .line 579
    :cond_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    return v2

    .line 582
    :cond_5
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_6

    return v2

    :cond_6
    const-string v0, "flag"

    .line 584
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "message"

    .line 587
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    return v2

    :cond_7
    const/4 p1, 0x1

    return p1

    .line 560
    :cond_8
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception p1

    .line 593
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    .line 594
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    return v2
.end method

.method private parsePassengerNew(Ljava/lang/String;Ljava/util/Vector;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "pageTotal"

    const-string v1, "datas"

    const-string v2, "data"

    const/4 v3, 0x0

    if-eqz p1, :cond_d

    .line 340
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_0

    goto/16 :goto_3

    .line 345
    :cond_0
    new-instance v4, Lorg/json/JSONTokener;

    invoke-direct {v4, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 347
    :try_start_0
    invoke-virtual {v4}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 348
    instance-of v4, p1, Lorg/json/JSONObject;

    if-nez v4, :cond_1

    goto/16 :goto_2

    .line 353
    :cond_1
    check-cast p1, Lorg/json/JSONObject;

    const-string v4, "messages"

    .line 354
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONArray;

    if-nez v4, :cond_2

    return v3

    :cond_2
    const-string v5, ""

    .line 356
    iput-object v5, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    .line 357
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    .line 358
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v6, v7, :cond_3

    .line 359
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 361
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    const-string v4, "status"

    .line 363
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    return v3

    .line 368
    :cond_4
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    return v3

    .line 372
    :cond_5
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_6

    return v3

    :cond_6
    const-string v2, "flag"

    .line 375
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string p2, "message"

    .line 378
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    return v3

    .line 382
    :cond_7
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    return v3

    .line 386
    :cond_8
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-nez v1, :cond_9

    return v3

    .line 389
    :cond_9
    iget v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    if-gez v2, :cond_a

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 390
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    :cond_a
    const/4 p1, 0x0

    .line 393
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_b

    .line 394
    invoke-virtual {v1, p1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 395
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;-><init>()V

    const-string v4, "passenger_id_type_name"

    .line 396
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_name:Ljava/lang/String;

    const-string v4, "isUserSelf"

    .line 397
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isUserSelf:Ljava/lang/String;

    const-string v4, "mobile_no"

    .line 398
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    const-string v4, "phone_no"

    .line 399
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->phone_no:Ljava/lang/String;

    const-string v4, "email"

    .line 400
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->email:Ljava/lang/String;

    const-string v4, "address"

    .line 401
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->address:Ljava/lang/String;

    const-string v4, "born_date"

    .line 402
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->born_date:Ljava/lang/String;

    const-string v4, "postalcode"

    .line 404
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->postalcode:Ljava/lang/String;

    const-string v4, "passenger_id_no"

    .line 406
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    const-string v4, "passenger_id_type_code"

    .line 407
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    const-string v4, "passenger_name"

    .line 408
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    const-string v4, "passenger_type"

    .line 409
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v4, "passenger_type_name"

    .line 410
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type_name:Ljava/lang/String;

    const-string v4, "sex_code"

    .line 411
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    const-string v4, "total_times"

    .line 412
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->total_times:Ljava/lang/String;

    const-string v4, "allEncStr"

    .line 413
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->allEncStr:Ljava/lang/String;

    .line 415
    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->calc_status()V

    const-string v0, "PassengerModel"

    .line 416
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "passenger="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_1

    .line 424
    :cond_b
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->updateSelectedStatus()V

    const/4 p1, 0x1

    return p1

    .line 349
    :cond_c
    :goto_2
    :try_start_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const p2, 0x7f0d010e

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    return v3

    :catch_0
    move-exception p1

    .line 420
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 421
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    return v3

    .line 341
    :cond_d
    :goto_3
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    return v3
.end method

.method private parsePassengerShow(Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 218
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 220
    :cond_0
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    const-string v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    return v2

    .line 222
    :cond_1
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    if-nez v1, :cond_2

    .line 223
    new-instance v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;-><init>()V

    iput-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    .line 225
    :cond_2
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.province_code"

    invoke-direct {p0, p1, v3, v2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    .line 226
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.school_name"

    invoke-direct {p0, p1, v3, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    .line 227
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.student_no"

    invoke-direct {p0, p1, v3, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    .line 228
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.school_system"

    invoke-direct {p0, p1, v3, v2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    .line 229
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.enter_year"

    invoke-direct {p0, p1, v3, v2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    .line 230
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v3, "studentInfoDTO.preference_from_station_name"

    invoke-direct {p0, p1, v3, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    .line 231
    iget-object p2, p2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    const-string v1, "studentInfoDTO.preference_to_station_name"

    invoke-direct {p0, p1, v1, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    return v2

    :cond_3
    :goto_0
    return v0
.end method

.method private queryPassengersNew(I)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pageIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "&pageSize=10"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x3

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    if-lez v1, :cond_2

    .line 311
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->GET_ORDER_PERSON:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v2, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "PassengerModel"

    .line 312
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-direct {p0, v0, v2}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->parsePassengerNew(Ljava/lang/String;Ljava/util/Vector;)Z

    move-result v0

    add-int/lit8 v1, v1, -0x1

    if-nez v0, :cond_0

    const-wide/16 v2, 0x1f4

    .line 316
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 321
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 322
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_1

    .line 323
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 324
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return v0
.end method

.method private declared-synchronized selectPassenger(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v0, "[\u7ae5]"

    .line 113
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "[\u7ae5]"

    const-string v2, ""

    .line 115
    invoke-static {p1, v1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 118
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    .line 120
    iget v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    add-int/2addr v4, v3

    iput v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    goto :goto_0

    .line 122
    :cond_2
    iput-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 126
    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method private declared-synchronized updateSelectedStatus()V
    .locals 6

    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 156
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 157
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 158
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    if-nez v3, :cond_1

    iget v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    if-lez v3, :cond_0

    goto :goto_1

    .line 170
    :cond_0
    iput v0, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    goto :goto_3

    .line 159
    :cond_1
    :goto_1
    new-instance v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {v3}, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;-><init>()V

    .line 160
    iput-object v2, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 161
    iput-boolean v0, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    .line 162
    iget-boolean v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_2
    const/4 v3, 0x0

    .line 163
    :goto_2
    iget v4, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    if-ge v3, v4, :cond_3

    .line 164
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;-><init>()V

    .line 165
    iput-object v2, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v5, 0x1

    .line 166
    iput-boolean v5, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    .line 167
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 173
    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    goto :goto_5

    :goto_4
    throw v0

    :goto_5
    goto :goto_4
.end method


# virtual methods
.method public addPassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 509
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&sex_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&_birthDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getBornDate(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&country_code=CN&passenger_id_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_id_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 514
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&mobile_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&phone_no=&email=&address=&postalcode=&passenger_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.province_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.department=&studentInfoDTO.school_class=&studentInfoDTO.student_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_system="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.enter_year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_card_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->perference_card_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_from_station_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_from_station_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_to_station_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_to_station_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_code:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 532
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_ADD:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 533
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->parseAddDelPassengerResult(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 535
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 536
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_0

    .line 537
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 538
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized clearSelectedStatus()V
    .locals 3

    monitor-enter p0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v2, 0x0

    .line 102
    iput-boolean v2, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isSelected:Z

    .line 103
    iput v2, v1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :cond_1
    monitor-exit p0

    return-void

    .line 99
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    goto :goto_3

    :goto_2
    throw v0

    :goto_3
    goto :goto_2
.end method

.method public deletePassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "#&passenger_id_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_id_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "#&isUserSelf="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isUserSelf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&allEncStr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->allEncStr:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 480
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_DEL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 481
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->parseAddDelPassengerResult(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 483
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 484
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_0

    .line 485
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 486
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public editPassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&old_passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&sex_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->sex_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&_birthDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getBornDate(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&country_code=CN&passenger_id_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&old_passenger_id_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_id_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    .line 438
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&old_passenger_id_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->old_passenger_id_no:Ljava/lang/String;

    .line 439
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&mobile_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&phone_no=&email=&address=&postalcode=&passenger_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.province_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->province_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.department=&studentInfoDTO.school_class=&studentInfoDTO.student_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->student_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.school_system="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->school_system:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.enter_year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->enter_year:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_card_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->perference_card_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_from_station_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_from_station_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_from_station_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_to_station_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&studentInfoDTO.preference_to_station_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->studentInfo:Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/StudentInfoDTO;->preference_to_station_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&allEncStr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->allEncStr:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v1, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_EDIT:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 459
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->parseAddDelPassengerResult(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 461
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 462
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_0

    .line 463
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 464
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getMobileNo(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    const-string v1, ""

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 250
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 251
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 252
    iget-object p1, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->mobile_no:Ljava/lang/String;

    return-object p1

    :cond_2
    :goto_0
    return-object v1
.end method

.method public getPassengers()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    return-object v0
.end method

.method public declared-synchronized getPassengersIncludeChild()Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 78
    :try_start_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 79
    :goto_0
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 80
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 81
    new-instance v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {v4}, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;-><init>()V

    .line 82
    iput-object v3, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 83
    iput-boolean v1, v4, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    .line 84
    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    .line 86
    :goto_1
    iget v5, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->child_ticket_num:I

    if-ge v4, v5, :cond_0

    .line 87
    new-instance v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    invoke-direct {v5}, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;-><init>()V

    .line 88
    iput-object v3, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    const/4 v6, 0x1

    .line 89
    iput-boolean v6, v5, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    .line 90
    invoke-virtual {v0, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    :cond_1
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    goto :goto_3

    :goto_2
    throw v0

    :goto_3
    goto :goto_2
.end method

.method public getSelectedPassenger()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;",
            ">;"
        }
    .end annotation

    .line 148
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->updateSelectedStatus()V

    .line 149
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    return-object v0
.end method

.method public getSelf()Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;
    .locals 5

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    .line 69
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isUserSelf:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->isUserSelf:Ljava/lang/String;

    const-string v4, "Y"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    :goto_0
    return-object v1
.end method

.method public declared-synchronized queryPassengers(Z)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    monitor-enter p0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    if-lez v0, :cond_0

    if-nez p1, :cond_0

    .line 266
    monitor-exit p0

    return v1

    .line 268
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->init()V

    .line 269
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->clear()V

    const/4 p1, -0x1

    .line 270
    iput p1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    const/4 p1, 0x1

    .line 274
    :cond_1
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengersNew(I)Z

    move-result v0

    .line 275
    iget v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I

    add-int/lit8 p1, p1, 0x1

    .line 277
    iget v2, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPageTotal:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-lez v2, :cond_2

    if-nez v0, :cond_1

    .line 287
    :cond_2
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    goto :goto_1

    :goto_0
    throw p1

    :goto_1
    goto :goto_0
.end method

.method public declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mSelectedPassengers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized selectPassenger(Ljava/lang/String;)V
    .locals 7

    monitor-enter p0

    .line 129
    :try_start_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->clearSelectedStatus()V

    .line 130
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, ","

    .line 131
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    .line 132
    monitor-exit p0

    return-void

    .line 133
    :cond_0
    :try_start_1
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v3, p1, v2

    const-string v4, "-"

    .line 134
    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 135
    array-length v4, v3

    const/4 v5, 0x2

    if-lt v4, v5, :cond_1

    .line 136
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v4

    aget-object v5, v3, v1

    const/4 v6, 0x1

    aget-object v3, v3, v6

    invoke-direct {v4, v5, v3}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method public showPassenger(Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_json_att=&passenger_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_id_type_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_type_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_id_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&passenger_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/PassengerModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object v2, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->PASSENGER_SHOW:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->post(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PassengerModel"

    .line 190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contact="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-direct {p0, v0, p1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->parsePassengerShow(Ljava/lang/String;Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 193
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 194
    instance-of v0, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz v0, :cond_0

    .line 195
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 196
    :cond_0
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
