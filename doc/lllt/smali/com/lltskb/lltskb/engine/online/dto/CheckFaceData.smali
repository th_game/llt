.class public final Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;
.super Ljava/lang/Object;
.source "HoubuTicketDTO.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J)\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00032\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001R \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u001e\u0010\u0004\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\r\"\u0004\u0008\u0011\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;",
        "",
        "login_flag",
        "",
        "face_flag",
        "face_check_code",
        "",
        "(ZZLjava/lang/String;)V",
        "getFace_check_code",
        "()Ljava/lang/String;",
        "setFace_check_code",
        "(Ljava/lang/String;)V",
        "getFace_flag",
        "()Z",
        "setFace_flag",
        "(Z)V",
        "getLogin_flag",
        "setLogin_flag",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "lltskb_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private face_check_code:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "face_check_code"
    .end annotation
.end field

.field private face_flag:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "face_flag"
    .end annotation
.end field

.field private login_flag:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "login_flag"
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZLjava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    iput-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;ZZLjava/lang/String;ILjava/lang/Object;)Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->copy(ZZLjava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZZLjava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;
    .locals 1

    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;

    invoke-direct {v0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;-><init>(ZZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    iget-boolean v3, p1, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    iget-boolean v3, p1, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final getFace_check_code()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    return-object v0
.end method

.method public final getFace_flag()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    return v0
.end method

.method public final getLogin_flag()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final setFace_check_code(Ljava/lang/String;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    return-void
.end method

.method public final setFace_flag(Z)V
    .locals 0

    .line 25
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    return-void
.end method

.method public final setLogin_flag(Z)V
    .locals 0

    .line 24
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckFaceData(login_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->login_flag:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", face_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_flag:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", face_check_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/dto/CheckFaceData;->face_check_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
