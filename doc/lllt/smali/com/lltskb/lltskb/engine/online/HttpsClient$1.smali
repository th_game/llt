.class final Lcom/lltskb/lltskb/engine/online/HttpsClient$1;
.super Ljava/lang/Object;
.source "HttpsClient.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/engine/online/HttpsClient;->disableSSLCertificateChecking()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1

    .line 157
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "checkClientTrusted "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "HttpClientModel"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkServerTrusted "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "HttpClientModel"

    invoke-static {v0, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 165
    array-length p2, p1

    if-lez p2, :cond_3

    const/4 p2, 0x0

    .line 169
    aget-object v0, p1, p2

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/Principal;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u975e\u6cd5\u8bc1\u4e66\u9519\u8bef"

    if-eqz v0, :cond_1

    const-string v2, "kyfw.12306.cn"

    .line 170
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    aget-object p1, p1, p2

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "bc0b1973f95ff82a4524f184f1571ce28bbc69da064f5aeb95062c10ea2c0bf7c8adef958d1a260251ab035f2dcef3063e3ed645be010a9291ea43553ab9e9a21d2b6d8544b5c5306c53f4ee5c5e801dcfa876e3facc218a7149c744092c45bf01192833040fd7dc1f4250a9d86bd600d8404861c72bcc887a6910230c76ef61"

    .line 173
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 174
    :cond_0
    new-instance p1, Ljava/security/cert/CertificateException;

    invoke-direct {p1, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-eqz v0, :cond_3

    const-string v2, "www.st12306.cn"

    .line 177
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 178
    aget-object p1, p1, p2

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "b963f4ccf401792a6c21f7aa61499c8ea90da7ec39bb3a9f7d70b6fab8480b36e3a1034e95ef41c0ba0b4dd3ce377c696de7d9bb03ae8acc76b2658208785cbdce014f02d2ff66d0da2644d493296373df6000c0a48a626cb973873bf938af43244f538eb260d1f524afce16f1a506ddaad1102ff9ed82890697eda343198ad738f8e9935cb699ecef1bd14c661c056e83f134f305c7ecf123db6b20b0b89d69c9e15a3b31e975f368174a1b1ad366d3fb118c4cb2f570536cc7920c25d929b8fe8cf423d0e7e7869627a7b944c1409e8ce27a5c84ce6f62c8da24dc36f05a7e33903e6e02ce6db8474f8c37db59a283a11de0f8219bada35fcf501781062f9d"

    .line 180
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 181
    :cond_2
    new-instance p1, Ljava/security/cert/CertificateException;

    invoke-direct {p1, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    return-void
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
