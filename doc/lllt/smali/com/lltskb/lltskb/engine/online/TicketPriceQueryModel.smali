.class public Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;
.super Ljava/lang/Object;
.source "TicketPriceQueryModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TicketPriceQuery"

.field private static instance:Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;


# instance fields
.field private mErrorMsg:Ljava/lang/String;

.field private mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    return-void
.end method

.method public static declared-synchronized get()Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;

    monitor-enter v0

    .line 42
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->instance:Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->instance:Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;

    .line 45
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->instance:Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private parseQueryResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    const-string v2, "WZ"

    const-string v3, "A1"

    const-string v4, "A2"

    const-string v5, "A3"

    const-string v6, "A4"

    const-string v7, "A6"

    const-string v8, "O"

    const-string v9, "M"

    const-string v10, "P"

    const-string v11, "A9"

    const-string v12, "F"

    const-string v13, "message"

    const-string v14, "messages"

    if-eqz v0, :cond_15

    .line 119
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    move-object/from16 v16, v2

    const/16 v2, 0xa

    if-ge v15, v2, :cond_0

    goto/16 :goto_5

    :cond_0
    const-string v2, "\u7f51\u7edc\u53ef\u80fd\u5b58\u5728\u95ee\u9898"

    .line 124
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    return-object v2

    .line 129
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 133
    instance-of v2, v0, Lorg/json/JSONObject;

    if-nez v2, :cond_2

    goto/16 :goto_4

    .line 137
    :cond_2
    move-object v2, v0

    check-cast v2, Lorg/json/JSONObject;

    const-string v0, "httpstatus"

    .line 139
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const/16 v15, 0xc8

    if-eq v0, v15, :cond_3

    const/4 v15, 0x0

    return-object v15

    .line 142
    :cond_3
    invoke-virtual {v2, v14}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_6

    .line 144
    :try_start_1
    invoke-virtual {v2, v14}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 145
    instance-of v14, v0, Ljava/lang/String;

    if-eqz v14, :cond_4

    .line 146
    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    goto :goto_2

    .line 147
    :cond_4
    instance-of v14, v0, Lorg/json/JSONArray;

    if-eqz v14, :cond_6

    const-string v14, ""

    .line 148
    iput-object v14, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    .line 149
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v15, 0x0

    .line 150
    :goto_0
    move-object/from16 v17, v0

    check-cast v17, Lorg/json/JSONArray;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v18, v3

    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v15, v3, :cond_5

    .line 151
    move-object v3, v0

    check-cast v3, Lorg/json/JSONArray;

    invoke-virtual {v3, v15}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v3, v18

    goto :goto_0

    .line 153
    :cond_5
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object/from16 v18, v3

    .line 157
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    goto :goto_3

    :cond_6
    :goto_2
    move-object/from16 v18, v3

    :goto_3
    const-string v0, "data"

    .line 161
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_7

    .line 162
    invoke-virtual {v2, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 163
    invoke-virtual {v2, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    const/4 v2, 0x0

    return-object v2

    :cond_7
    const/4 v2, 0x0

    if-nez v0, :cond_8

    return-object v2

    .line 170
    :cond_8
    new-instance v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    invoke-direct {v2}, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;-><init>()V

    .line 172
    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 173
    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->srrb:Ljava/lang/String;

    .line 176
    :cond_9
    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 177
    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->swz:Ljava/lang/String;

    .line 180
    :cond_a
    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 181
    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->tz:Ljava/lang/String;

    .line 184
    :cond_b
    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 185
    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->zy:Ljava/lang/String;

    .line 188
    :cond_c
    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 189
    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->ze:Ljava/lang/String;

    .line 192
    :cond_d
    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 193
    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->gr:Ljava/lang/String;

    .line 196
    :cond_e
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 197
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->rw:Ljava/lang/String;

    .line 200
    :cond_f
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 201
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->yw:Ljava/lang/String;

    .line 204
    :cond_10
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 205
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->rz:Ljava/lang/String;

    :cond_11
    move-object/from16 v3, v18

    .line 208
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 209
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->yz:Ljava/lang/String;

    :cond_12
    move-object/from16 v3, v16

    .line 212
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 213
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->wz:Ljava/lang/String;

    :cond_13
    const-string v3, "train_no"

    .line 216
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;->train_no:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    return-object v2

    :cond_14
    :goto_4
    const/4 v2, 0x0

    return-object v2

    :catch_2
    move-exception v0

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TicketPriceQuery"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 222
    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    const/4 v2, 0x0

    return-object v2

    :cond_15
    :goto_5
    const/4 v2, 0x0

    return-object v2
.end method

.method private queryTicketPriceFL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "train_no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&from_station_no="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&to_station_no="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&seat_types="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_date="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 65
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryTicketPrice url = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "TicketPriceQuery"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :try_start_0
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p4, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICEFL:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p2, p4, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 69
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "queryTicketPriceFL ret="

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 72
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public queryTicketPrice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lltskb/lltskb/engine/online/HttpParseException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 81
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "train_no="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&from_station_no="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&to_station_no="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&seat_types="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&train_date="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "utf-8"

    .line 85
    invoke-static {p5, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 87
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object p1, v0

    .line 90
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryTicketPrice url = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "TicketPriceQuery"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x3

    :goto_1
    if-lez p2, :cond_2

    .line 95
    :try_start_1
    iget-object p4, p0, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->mHttpsClient:Lcom/lltskb/lltskb/engine/online/HttpsClient;

    sget-object p5, Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;->QUERY_TICKET_PRICE:Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;

    invoke-virtual {p4, p5, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get(Lcom/lltskb/lltskb/engine/online/dto/UrlEnums;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 96
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryTicketPrice ret="

    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-static {p3, p5}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0, p4}, Lcom/lltskb/lltskb/engine/online/TicketPriceQueryModel;->parseQueryResult(Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/TicketPriceQueryDTO;

    move-result-object p4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez p4, :cond_0

    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    :cond_0
    return-object p4

    :catch_1
    move-exception p1

    .line 110
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    instance-of p2, p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    if-eqz p2, :cond_1

    .line 112
    check-cast p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    throw p1

    .line 113
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/engine/online/HttpParseException;

    const-string p2, "\u7f51\u7edc\u95ee\u9898"

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/engine/online/HttpParseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-object v0
.end method
