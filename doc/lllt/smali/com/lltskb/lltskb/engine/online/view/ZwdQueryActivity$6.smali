.class Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;
.super Ljava/lang/Object;
.source "ZwdQueryActivity.java"

# interfaces
.implements Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->onShare()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDenied()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onGranted()V
    .locals 4

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$400(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u6b63\u665a\u70b9\u4fe1\u606f:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$500(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/util/Vector;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->access$500(Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;

    .line 176
    iget-boolean v3, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->isSelected:Z

    if-eqz v3, :cond_0

    .line 177
    iget-object v3, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->station_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    .line 178
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    iget-object v2, v2, Lcom/lltskb/lltskb/engine/online/dto/ZwdDTO;->zwd_info:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    .line 180
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity$6;->this$0:Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-static {v1, v0, v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->startShareIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method
