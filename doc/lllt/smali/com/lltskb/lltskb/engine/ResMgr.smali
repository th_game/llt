.class public Lcom/lltskb/lltskb/engine/ResMgr;
.super Ljava/lang/Object;
.source "ResMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ResMgr"

.field private static instance:Lcom/lltskb/lltskb/engine/ResMgr; = null

.field public static mPath:Ljava/lang/String; = "/data/data/com.lltskb.lltskb/files/"

.field private static mVer:Ljava/lang/String; = "1.9.10"


# instance fields
.field private ads_txt:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ads_url:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

.field private extraData:Lcom/lltskb/lltskb/engine/ExtraDataMgr;

.field private fuzzyStationMgr:Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

.field private isReady:Z

.field private jlbDataMgr:Lcom/lltskb/lltskb/engine/JlbDataMgr;

.field private mContext:Landroid/content/Context;

.field private mLastStation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSoft:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;",
            ">;"
        }
    .end annotation
.end field

.field private pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

.field private priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

.field private rwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

.field private scheduleData:Lcom/lltskb/lltskb/engine/DataMgr;

.field private station:Lcom/lltskb/lltskb/engine/IndexMgr;

.field private stationData:[Lcom/lltskb/lltskb/engine/DataMgr;

.field private train:Lcom/lltskb/lltskb/engine/IndexMgr;

.field private trainData:[Lcom/lltskb/lltskb/engine/DataMgr;

.field private tsData:Lcom/lltskb/lltskb/engine/DataMgr;

.field private ver:Ljava/lang/String;

.field private xwData:Lcom/lltskb/lltskb/engine/DataMgr;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->get()Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->fuzzyStationMgr:Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    const/4 v0, 0x0

    .line 102
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->isReady:Z

    const/4 v0, 0x0

    .line 104
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    .line 106
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ads_txt:Ljava/util/Vector;

    .line 107
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ads_url:Ljava/util/Vector;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    return-void
.end method

.method private base64ToBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x0

    .line 575
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    .line 576
    array-length v1, p1

    invoke-static {p1, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private copyFile(ILjava/lang/String;)Z
    .locals 5

    const-string v0, "ResMgr"

    const/4 v1, 0x0

    .line 500
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 506
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 508
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_0

    .line 509
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p2

    invoke-virtual {p2}, Ljava/io/File;->mkdirs()Z

    .line 510
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 512
    :cond_0
    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 515
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    :goto_0
    if-lez v3, :cond_1

    .line 517
    invoke-virtual {p2, v2, v1, v3}, Ljava/io/FileOutputStream;->write([BII)V

    .line 518
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    goto :goto_0

    .line 520
    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 521
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 528
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 529
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :catch_1
    move-exception p1

    .line 524
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 525
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :catch_2
    move-exception p1

    .line 502
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return v1
.end method

.method private copyFiles()Z
    .locals 5

    .line 291
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "ResMgr"

    if-nez v1, :cond_0

    .line 293
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mkdirs failed"

    .line 294
    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const v0, 0x7f0c0020

    const-string v1, "s.i"

    .line 298
    invoke-direct {p0, v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "copy s.i failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1
    const v0, 0x7f0c0038

    const-string v3, "t.i"

    .line 299
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "copy t.i failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2
    const v0, 0x7f0c0039

    const-string v3, "t.rule"

    .line 300
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "copy t.rule failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_3
    const v0, 0x7f0c0023

    const-string v3, "station_name.js"

    .line 302
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "copy station_name.js failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_4
    const v0, 0x7f0c003b

    const-string v3, "ver.txt"

    .line 304
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "copy ver.txt failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_5
    const v0, 0x7f0c0016

    const-string v3, "s0.dat"

    .line 305
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "copy s0.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_6
    const v0, 0x7f0c0017

    const-string v3, "s1.dat"

    .line 306
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "copy s1.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_7
    const v0, 0x7f0c0018

    const-string v3, "s2.dat"

    .line 307
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "copy s2.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_8
    const v0, 0x7f0c0019

    const-string v3, "s3.dat"

    .line 308
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "copy s3.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_9
    const v0, 0x7f0c001a

    const-string v3, "s4.dat"

    .line 309
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "copy s4.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_a
    const v0, 0x7f0c001b

    const-string v3, "s5.dat"

    .line 310
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "copy s5.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_b
    const v0, 0x7f0c001c

    const-string v3, "s6.dat"

    .line 311
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "copy s6.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_c
    const v0, 0x7f0c001d

    const-string v3, "s7.dat"

    .line 312
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "copy s7.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_d
    const v0, 0x7f0c001e

    const-string v3, "s8.dat"

    .line 313
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "copy s8.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_e
    const v0, 0x7f0c001f

    const-string v3, "s9.dat"

    .line 314
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "copy s9.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_f
    const v0, 0x7f0c0024

    const-string v3, "t0.dat"

    .line 316
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "copy t0.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_10
    const v0, 0x7f0c0025

    const-string v3, "t1.dat"

    .line 317
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "copy t1.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_11
    const v0, 0x7f0c0030

    const-string v3, "t2.dat"

    .line 318
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "copy t2.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_12
    const v0, 0x7f0c0031

    const-string v3, "t3.dat"

    .line 319
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "copy t3.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_13
    const v0, 0x7f0c0032

    const-string v3, "t4.dat"

    .line 320
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "copy t4.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_14
    const v0, 0x7f0c0033

    const-string v3, "t5.dat"

    .line 321
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    const-string v0, "copy t5.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_15
    const v0, 0x7f0c0034

    const-string v3, "t6.dat"

    .line 322
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "copy t6.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_16
    const v0, 0x7f0c0035

    const-string v3, "t7.dat"

    .line 323
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    const-string v0, "copy t7.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_17
    const v0, 0x7f0c0036

    const-string v3, "t8.dat"

    .line 324
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_18

    const-string v0, "copy t8.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_18
    const v0, 0x7f0c0037

    const-string v3, "t9.dat"

    .line 325
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    const-string v0, "copy t9.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_19
    const v0, 0x7f0c0026

    const-string v3, "t10.dat"

    .line 326
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "copy t10.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1a
    const v0, 0x7f0c0027

    const-string v3, "t11.dat"

    .line 327
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "copy t11.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1b
    const v0, 0x7f0c0028

    const-string v3, "t12.dat"

    .line 328
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    const-string v0, "copy t12.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1c
    const v0, 0x7f0c0029

    const-string v3, "t13.dat"

    .line 329
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    const-string v0, "copy t13.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1d
    const v0, 0x7f0c002a

    const-string v3, "t14.dat"

    .line 330
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    const-string v0, "copy t14.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1e
    const v0, 0x7f0c002b

    const-string v3, "t15.dat"

    .line 331
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    const-string v0, "copy t15.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1f
    const v0, 0x7f0c002c

    const-string v3, "t16.dat"

    .line 332
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "copy t16.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_20
    const v0, 0x7f0c002d

    const-string v3, "t17.dat"

    .line 333
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_21

    const-string v0, "copy t17.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_21
    const v0, 0x7f0c002e

    const-string v3, "t18.dat"

    .line 334
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    const-string v0, "copy t18.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_22
    const v0, 0x7f0c002f

    const-string v3, "t19.dat"

    .line 335
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_23

    const-string v0, "copy t19.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_23
    const v0, 0x7f0c0005

    const-string v3, "p0.dat"

    .line 336
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    const-string v0, "copy p0.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_24
    const v0, 0x7f0c0006

    const-string v3, "p1.dat"

    .line 337
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_25

    const-string v0, "copy p1.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_25
    const v0, 0x7f0c0007

    const-string v3, "p2.dat"

    .line 338
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_26

    const-string v0, "copy p2.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_26
    const v0, 0x7f0c0008

    const-string v3, "p3.dat"

    .line 339
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_27

    const-string v0, "copy p3.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_27
    const v0, 0x7f0c0009

    const-string v3, "p4.dat"

    .line 340
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_28

    const-string v0, "copy p4.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_28
    const v0, 0x7f0c000a

    const-string v3, "pk0.dat"

    .line 342
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_29

    const-string v0, "copy pk0.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_29
    const v0, 0x7f0c000b

    const-string v3, "pk1.dat"

    .line 343
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2a

    const-string v0, "copy pk1.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2a
    const v0, 0x7f0c000c

    const-string v3, "pk2.dat"

    .line 344
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    const-string v0, "copy pk2.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2b
    const v0, 0x7f0c000d

    const-string v3, "pk3.dat"

    .line 345
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    const-string v0, "copy pk3.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2c
    const v0, 0x7f0c000e

    const-string v3, "pk4.dat"

    .line 346
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2d

    const-string v0, "copy pk4.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2d
    const v0, 0x7f0c000f

    const-string v3, "pk5.dat"

    .line 347
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2e

    const-string v0, "copy pk5.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2e
    const v0, 0x7f0c0010

    const-string v3, "pk6.dat"

    .line 348
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2f

    const-string v0, "copy pk6.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2f
    const v0, 0x7f0c0011

    const-string v3, "pk7.dat"

    .line 349
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_30

    const-string v0, "copy pk7.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_30
    const v0, 0x7f0c0012

    const-string v3, "pk8.dat"

    .line 350
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_31

    const-string v0, "copy pk8.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_31
    const v0, 0x7f0c0013

    const-string v3, "pk9.dat"

    .line 351
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_32

    const-string v0, "copy pk9.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_32
    const v0, 0x7f0c0021

    const-string v3, "sch.dat"

    .line 353
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_33

    const-string v0, "copy sch.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_33
    const v0, 0x7f0c0015

    const-string v3, "rwp.dat"

    .line 354
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    const-string v0, "copy rwp.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_34
    const v0, 0x7f0c003c

    const-string v3, "xw.dat"

    .line 355
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_35

    const-string v0, "copy xw.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_35
    const v0, 0x7f0c0003

    const-string v3, "extra.dat"

    .line 356
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_36

    const-string v0, "copy extra.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_36
    const v0, 0x7f0c0022

    const-string v3, "sn.dat"

    .line 357
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_37

    const-string v0, "copy sn.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_37
    const v0, 0x7f0c0002

    const-string v3, "dwp.dat"

    .line 359
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    const-string v0, "copy dwp.dat failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_38
    const v0, 0x7f0c003a

    const-string v3, "ts.dat"

    .line 360
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    const-string v3, "copy ts.dat failed"

    if-nez v0, :cond_39

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_39
    const v0, 0x7f0c0004

    const-string v4, "jlb.dat"

    .line 362
    invoke-direct {p0, v0, v4}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_3a
    const v0, 0x7f0c0001

    const-string v3, "config.json"

    .line 364
    invoke-direct {p0, v0, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3b

    const-string v0, "copy config.json failed"

    invoke-static {v2, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_3b
    const/4 v0, 0x1

    return v0
.end method

.method public static declared-synchronized getInstance()Lcom/lltskb/lltskb/engine/ResMgr;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/ResMgr;

    monitor-enter v0

    .line 133
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->instance:Lcom/lltskb/lltskb/engine/ResMgr;

    if-nez v1, :cond_0

    .line 134
    new-instance v1, Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/ResMgr;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->instance:Lcom/lltskb/lltskb/engine/ResMgr;

    .line 136
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->instance:Lcom/lltskb/lltskb/engine/ResMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private initMRI()V
    .locals 5

    .line 841
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    goto :goto_2

    .line 842
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 843
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "query"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v2, "StationMRI"

    const-string v3, ""

    .line 846
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-void

    :cond_2
    const-string v2, "\\|"

    .line 848
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 849
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 850
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 851
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 854
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xf

    if-le v0, v1, :cond_5

    .line 855
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    :goto_2
    return-void
.end method

.method private preBuild()Z
    .locals 2

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ver.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->readVer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "20170308"

    .line 275
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f0c003b

    .line 279
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->readVer(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "ResMgr"

    const-string v1, "read ver failed"

    .line 281
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    .line 284
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    .line 285
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFiles()Z

    move-result v0

    return v0

    :cond_2
    const/4 v0, 0x1

    return v0

    .line 276
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFiles()Z

    move-result v0

    return v0
.end method

.method private readVer(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 478
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object p1

    .line 480
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 481
    new-instance p1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 482
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 483
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V

    .line 484
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 486
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readVer failed :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "ResMgr"

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private readVer(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 543
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 544
    new-instance p1, Ljava/io/FileInputStream;

    invoke-direct {p1, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 546
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 547
    new-instance p1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 548
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 549
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V

    .line 550
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 552
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readVer failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "ResMgr"

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public doParse(Ljava/io/InputStream;)Z
    .locals 7

    .line 583
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "utf-8"

    .line 585
    invoke-interface {v0, p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 587
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result p1
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    move-object v3, v2

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-eq p1, v5, :cond_c

    if-eqz p1, :cond_b

    const/4 v5, 0x2

    const-string v6, "app"

    if-eq p1, v5, :cond_1

    const/4 v5, 0x3

    if-eq p1, v5, :cond_0

    goto/16 :goto_2

    .line 621
    :cond_0
    :try_start_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    if-eqz v3, :cond_b

    .line 622
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->tag:Ljava/lang/String;

    add-int/lit8 v4, v4, 0x1

    .line 624
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mSoft:Ljava/util/Vector;

    invoke-virtual {p1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 626
    new-instance p1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->tag:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".apk"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 627
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 628
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 595
    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v5, "software"

    .line 596
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 597
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mSoft:Ljava/util/Vector;

    goto/16 :goto_2

    .line 598
    :cond_2
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 599
    new-instance p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;-><init>(Lcom/lltskb/lltskb/engine/ResMgr;)V

    move-object v3, p1

    goto/16 :goto_2

    :cond_3
    const-string v5, "name"

    .line 600
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz v3, :cond_b

    .line 601
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->name:Ljava/lang/String;

    goto/16 :goto_2

    :cond_4
    const-string v5, "size"

    .line 602
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    if-eqz v3, :cond_b

    .line 603
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->size:Ljava/lang/String;

    goto :goto_2

    :cond_5
    const-string v5, "desc"

    .line 604
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    if-eqz v3, :cond_b

    .line 605
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->desc:Ljava/lang/String;

    goto :goto_2

    :cond_6
    const-string v5, "link"

    .line 606
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v3, :cond_b

    .line 607
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    goto :goto_2

    :cond_7
    const-string v5, "icon"

    .line 608
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    if-eqz v3, :cond_8

    .line 609
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->icon:Ljava/lang/String;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_8
    if-eqz v3, :cond_9

    .line 611
    :try_start_2
    iget-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->icon:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->base64ToBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->bmp:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 613
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_9
    :goto_1
    if-eqz v3, :cond_b

    .line 615
    iput-object v2, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->icon:Ljava/lang/String;

    goto :goto_2

    :cond_a
    const-string v5, "detail"

    .line 616
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    if-eqz v3, :cond_b

    .line 617
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    .line 632
    :cond_b
    :goto_2
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result p1
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :cond_c
    return v5

    :catch_1
    move-exception p1

    .line 641
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return v1

    :catch_2
    move-exception p1

    .line 637
    invoke-virtual {p1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    return v1
.end method

.method public getAdsTxt()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 558
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ads_txt:Ljava/util/Vector;

    return-object v0
.end method

.method public getAdsUrl()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 562
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ads_url:Ljava/util/Vector;

    return-object v0
.end method

.method getCancelTrainData(II)[B
    .locals 1

    .line 666
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->scheduleData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 667
    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getCancelTrainData(II)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCity(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 896
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    return-object p1

    .line 898
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->get()Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->getCity(I)I

    move-result v0

    .line 900
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    .line 901
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object p1

    :cond_1
    return-object v0
.end method

.method getDwPriceUnit(II)[B
    .locals 1

    .line 742
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->dwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 743
    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getPriceUnit(II)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;
    .locals 1

    .line 908
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->extraData:Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    return-object v0
.end method

.method public getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;
    .locals 1

    .line 912
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->jlbDataMgr:Lcom/lltskb/lltskb/engine/JlbDataMgr;

    return-object v0
.end method

.method public getMri()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    return-object v0
.end method

.method getPkPriceUnit(II)[B
    .locals 2

    .line 690
    :try_start_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    rem-int/lit8 v1, p1, 0xa

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    rem-int/lit8 v1, p1, 0xa

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getPkPriceUnit(II)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 693
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 694
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "getPkPriceUnit="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "ResMgr"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method getPriceUnit(II)[B
    .locals 4

    const/4 v0, 0x0

    const-string v1, "ResMgr"

    if-gez p1, :cond_0

    .line 707
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPriceUnit start="

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 711
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    rem-int/lit8 v3, p1, 0x5

    aget-object v2, v2, v3

    if-eqz v2, :cond_1

    .line 712
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    rem-int/lit8 v3, p1, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v2, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getPriceUnit(II)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 714
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 715
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPriceUnit="

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public getProgVer()Ljava/lang/String;
    .locals 1

    .line 393
    sget-object v0, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    return-object v0
.end method

.method getRwPriceUnit(II)[B
    .locals 1

    .line 729
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->rwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 730
    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getPriceUnit(II)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method getScheduleUnit(II)[B
    .locals 1

    .line 660
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->scheduleData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 661
    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/engine/DataMgr;->getScheduleData(II)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getSoftItems()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;",
            ">;"
        }
    .end annotation

    .line 566
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mSoft:Ljava/util/Vector;

    return-object v0
.end method

.method getStation(I)[B
    .locals 2

    add-int/lit8 v0, p1, 0x1

    .line 370
    rem-int/lit8 v0, v0, 0xa

    .line 371
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->stationData:[Lcom/lltskb/lltskb/engine/DataMgr;

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 372
    :cond_0
    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/engine/DataMgr;->getUnit(IZ)[B

    move-result-object p1

    return-object p1
.end method

.method getStationCount()I
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 434
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v0

    return v0
.end method

.method public getStationIndex(Ljava/lang/String;)I
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 422
    :cond_0
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method getStationName(I)Ljava/lang/String;
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public getSx(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz p1, :cond_3

    .line 251
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v1, :cond_0

    goto :goto_1

    .line 253
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 254
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5a

    if-gt v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x41

    if-lt v2, v3, :cond_1

    .line 255
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getSxLike(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 257
    move-object v0, p1

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_1

    .line 260
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 261
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 262
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v0
.end method

.method public getTrainCount()I
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 428
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v0

    return v0
.end method

.method public getTrainIndex(Ljava/lang/String;)I
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 405
    :cond_0
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method getTrainIndexFuzzy(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 410
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 411
    :cond_0
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getIndexFuzzy(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getTrainInfo(I)[B
    .locals 2

    .line 398
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->trainData:[Lcom/lltskb/lltskb/engine/DataMgr;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    add-int/lit8 v1, p1, 0x1

    .line 399
    rem-int/lit8 v1, v1, 0x14

    .line 400
    aget-object v0, v0, v1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/lltskb/lltskb/engine/DataMgr;->getUnit(IZ)[B

    move-result-object p1

    return-object p1
.end method

# 城际高速
.method getTrainLX(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 441
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit16 v2, p2, 0xf0

    and-int/lit8 p2, p2, 0xf

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_2

    :cond_1
    const-string v4, "\u65b0"

    .line 445
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v2, :cond_4

    const/4 v4, 0x2

    if-ne v2, v4, :cond_3

    goto :goto_0

    .line 449
    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    :goto_0
    const-string v0, "\u7a7a\u8c03"

    .line 447
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v0, 0x7

    if-ne p2, v0, :cond_5

    const-string p1, "\u5e02\u90ca"

    return-object p1

    :cond_5
    const/4 v0, 0x4

    if-ne p2, v0, :cond_6

    const-string p1, "\u57ce\u9645\u9ad8\u901f"

    return-object p1

    :cond_6
    const/4 v0, 0x6

    if-ne p2, v0, :cond_7

    const-string p1, "\u9ad8\u94c1"

    return-object p1

    :cond_7
    const/4 v0, 0x3

    if-ne p2, v0, :cond_8

    const-string p1, "\u52a8\u8f66"

    return-object p1

    :cond_8
    if-ne p2, v3, :cond_d

    const/4 p2, 0x0

    .line 459
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x4b

    if-eq v0, v2, :cond_c

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x4e

    if-ne v0, v2, :cond_9

    goto :goto_2

    .line 461
    :cond_9
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x54

    const-string v3, "\u7279\u5feb"

    if-ne v0, v2, :cond_a

    .line 462
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 463
    :cond_a
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 p2, 0x5a

    if-ne p1, p2, :cond_b

    const-string p1, "\u76f4\u5feb"

    .line 464
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 466
    :cond_b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_c
    :goto_2
    const-string p1, "\u5feb\u901f"

    .line 460
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_d
    if-nez p2, :cond_e

    const-string p1, "\u666e\u5feb"

    .line 468
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_e
    const-string p1, "\u666e\u5ba2"

    .line 470
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    :goto_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getTrainName(I)Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-eqz v0, :cond_0

    .line 378
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public getTrainSx(Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 168
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v2, p1

    invoke-virtual {v2, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 169
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 170
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    if-nez v3, :cond_0

    goto/16 :goto_a

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 173
    :goto_0
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v5

    const-string v6, "/"

    if-ge v4, v5, :cond_5

    .line 174
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object v5

    .line 175
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 176
    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    goto :goto_2

    .line 178
    :cond_1
    array-length v7, v6

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_4

    aget-object v9, v6, v8

    .line 179
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 180
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 185
    :cond_3
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 186
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    .line 192
    :goto_3
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_b

    .line 193
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    const/16 v8, 0x5a

    const/16 v9, 0x41

    const/4 v10, 0x1

    if-eqz v7, :cond_9

    .line 195
    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_6

    goto :goto_6

    .line 198
    :cond_6
    array-length v11, v7

    const/4 v12, 0x0

    :goto_4
    if-ge v12, v11, :cond_8

    aget-object v13, v7, v12

    .line 199
    invoke-virtual {v13, v3}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 200
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v3, v16, 0x1

    if-ne v15, v3, :cond_7

    invoke-virtual {v13, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v10, :cond_7

    if-lt v14, v9, :cond_7

    if-gt v14, v8, :cond_7

    .line 201
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    add-int/lit8 v12, v12, 0x1

    const/4 v3, 0x0

    goto :goto_4

    :cond_8
    :goto_5
    const/4 v3, 0x0

    goto :goto_6

    .line 206
    :cond_9
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 207
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v12, v10

    if-ne v11, v12, :cond_a

    invoke-virtual {v5, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-ne v11, v10, :cond_a

    if-lt v7, v9, :cond_a

    if-gt v7, v8, :cond_a

    .line 208
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_b
    const/4 v4, 0x0

    .line 224
    :goto_7
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/IndexMgr;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_10

    .line 225
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    invoke-virtual {v5, v4}, Lcom/lltskb/lltskb/engine/IndexMgr;->getName(I)Ljava/lang/String;

    move-result-object v5

    .line 226
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 227
    invoke-static {v5, v6}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_c

    goto :goto_9

    .line 229
    :cond_c
    array-length v8, v7

    const/4 v9, 0x0

    :goto_8
    if-ge v9, v8, :cond_f

    aget-object v10, v7, v9

    .line 230
    invoke-virtual {v10, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_d

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    if-eq v10, v11, :cond_d

    .line 231
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_d
    add-int/lit8 v9, v9, 0x1

    goto :goto_8

    .line 236
    :cond_e
    invoke-virtual {v5, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-eq v7, v8, :cond_f

    .line 237
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_f
    :goto_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_10
    :goto_a
    return-object v2
.end method

.method getTsPriceNo(III)I
    .locals 1

    .line 748
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->tsData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 749
    invoke-virtual {v0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/DataMgr;->getTsPriceNo(III)I

    move-result p1

    return p1

    :cond_0
    return p3
.end method

.method public getVer()Ljava/lang/String;
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ver:Ljava/lang/String;

    return-object v0
.end method

.method getXwUnit(I)[B
    .locals 1

    .line 677
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->xwData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;->getXwData(I)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method hasScheduleData(I)Z
    .locals 1

    .line 648
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->scheduleData:Lcom/lltskb/lltskb/engine/DataMgr;

    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;->hasScheduleData(I)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public init()Z
    .locals 11

    .line 759
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResMgr;->preBuild()Z

    move-result v0

    const-string v1, "ResMgr"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string v0, "init preBuild return false"

    .line 760
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    .line 764
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "station_name.js"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/QuickStation;->init(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 765
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->stationData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/16 v3, 0xa

    new-array v4, v3, [Lcom/lltskb/lltskb/engine/DataMgr;

    .line 766
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/ResMgr;->stationData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/4 v4, 0x0

    :goto_0
    const-string v5, ".dat"

    if-ge v4, v3, :cond_1

    .line 769
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/ResMgr;->stationData:[Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v7, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "s"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v7, v5}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    aput-object v7, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 771
    :cond_1
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->trainData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/16 v4, 0x14

    new-array v6, v4, [Lcom/lltskb/lltskb/engine/DataMgr;

    .line 772
    iput-object v6, p0, Lcom/lltskb/lltskb/engine/ResMgr;->trainData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_2

    .line 774
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/ResMgr;->trainData:[Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v8, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "t"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    aput-object v8, v7, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x5

    new-array v6, v4, [Lcom/lltskb/lltskb/engine/DataMgr;

    .line 777
    iput-object v6, p0, Lcom/lltskb/lltskb/engine/ResMgr;->priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v4, :cond_3

    .line 779
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/ResMgr;->priceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v8, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "p"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    aput-object v8, v7, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_3
    new-array v4, v3, [Lcom/lltskb/lltskb/engine/DataMgr;

    .line 782
    iput-object v4, p0, Lcom/lltskb/lltskb/engine/ResMgr;->pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v3, :cond_4

    .line 784
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/ResMgr;->pkPriceData:[Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v7, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "pk"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    aput-object v7, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 787
    :cond_4
    new-instance v3, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "rwp.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->rwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

    .line 789
    new-instance v3, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "dwp.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->dwPriceData:Lcom/lltskb/lltskb/engine/DataMgr;

    .line 791
    new-instance v3, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "ts.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->tsData:Lcom/lltskb/lltskb/engine/DataMgr;

    .line 793
    new-instance v3, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "xw.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->xwData:Lcom/lltskb/lltskb/engine/DataMgr;

    .line 795
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->fuzzyStationMgr:Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "sn.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->init(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const v3, 0x7f0c0022

    .line 796
    invoke-direct {p0, v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    .line 797
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->fuzzyStationMgr:Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->init(Ljava/lang/String;)Z

    .line 803
    :cond_5
    new-instance v3, Lcom/lltskb/lltskb/engine/DataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "sch.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->scheduleData:Lcom/lltskb/lltskb/engine/DataMgr;

    .line 805
    new-instance v3, Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "extra.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->extraData:Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    .line 807
    new-instance v3, Lcom/lltskb/lltskb/engine/JlbDataMgr;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "jlb.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/ResMgr;->jlbDataMgr:Lcom/lltskb/lltskb/engine/JlbDataMgr;

    .line 809
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "config.json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/engine/ConfigMgr;->init(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const v3, 0x7f0c0001

    .line 810
    invoke-direct {p0, v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->copyFile(ILjava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "copy config.json failed"

    invoke-static {v1, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :cond_6
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    .line 814
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    .line 815
    new-instance v0, Lcom/lltskb/lltskb/engine/IndexMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "s.i"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/IndexMgr;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->station:Lcom/lltskb/lltskb/engine/IndexMgr;

    .line 816
    new-instance v0, Lcom/lltskb/lltskb/engine/IndexMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "t.i"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/lltskb/lltskb/engine/IndexMgr;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->train:Lcom/lltskb/lltskb/engine/IndexMgr;

    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ver.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->readVer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ver:Ljava/lang/String;

    .line 820
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->ver:Ljava/lang/String;

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    :cond_7
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->isReady:Z

    .line 821
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResMgr;->initMRI()V

    .line 822
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->isReady:Z

    return v0
.end method

.method public isReady()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->isReady:Z

    return v0
.end method

.method public removeMri(I)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-gez p1, :cond_1

    .line 154
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 156
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 159
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->saveMRI(Ljava/lang/String;)V

    return-void
.end method

.method saveMRI(Ljava/lang/String;)V
    .locals 3

    .line 864
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_3

    .line 866
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 867
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 868
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "|"

    .line 869
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 872
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    const-string v2, "query"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 874
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 875
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "StationMRI"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 876
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    .line 880
    :cond_3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_4

    .line 881
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 882
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 883
    :goto_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/16 v0, 0xf

    if-le p1, v0, :cond_5

    .line 884
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mLastStation:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    const/4 p1, 0x0

    .line 887
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->saveMRI(Ljava/lang/String;)V

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 3

    const-string v0, "ResMgr"

    if-nez p1, :cond_0

    return-void

    .line 827
    :cond_0
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    .line 829
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/ResMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    .line 830
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    .line 831
    sget-object p1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    sget-object p1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0xa

    if-lt p1, v1, :cond_1

    .line 832
    sget-object p1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x9

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 835
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    :cond_1
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "version="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/lltskb/lltskb/engine/ResMgr;->mVer:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
