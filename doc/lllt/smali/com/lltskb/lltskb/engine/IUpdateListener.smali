.class public interface abstract Lcom/lltskb/lltskb/engine/IUpdateListener;
.super Ljava/lang/Object;
.source "IUpdateListener.java"


# static fields
.field public static final DATA_UPDATE:I = 0x2

.field public static final ERROR_UPDATE:I = -0x1

.field public static final INFO_UPDATE:I = 0x3

.field public static final NO_UPDATE:I = 0x0

.field public static final PROG_UPDATE:I = 0x1

.field public static final STATUS_ERROR:I = -0x1

.field public static final STATUS_PROCESS:I = 0x1

.field public static final STATUS_SUCCESS:I


# virtual methods
.method public abstract onCheckResult(ILjava/lang/String;)Z
.end method

.method public abstract onDownload(IIILjava/lang/String;)Z
.end method
