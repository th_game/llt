.class public Lcom/lltskb/lltskb/engine/UpdateMgr;
.super Ljava/lang/Object;
.source "UpdateMgr.java"


# static fields
.field private static final BUFF_SIZE:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "UpdateMgr"

.field private static final mConfigUrl:Ljava/lang/String; = "http://down.lltskb.com/config.json"

.field private static final mUrl:Ljava/lang/String; = "http://down.lltskb.com/an.ver"


# instance fields
.field private mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

.field private mRemoteVer:[Ljava/lang/String;

.field private mServers:[Ljava/lang/String;

.field private mStopFlag:Z


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/IUpdateListener;)V
    .locals 3

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "down.lltskb.com"

    aput-object v2, v1, v0

    const/4 v0, 0x1

    const-string v2, "223.113.64.14:8011"

    aput-object v2, v1, v0

    const/4 v0, 0x2

    const-string v2, "down2.lltskb.com"

    aput-object v2, v1, v0

    .line 40
    iput-object v1, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    .line 47
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    return-void
.end method

.method private downloadFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 16

    move-object/from16 v1, p0

    const-string v0, "UpdateMgr"

    const-string v2, "downloadFile"

    .line 251
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x100

    .line 254
    :try_start_0
    new-instance v3, Ljava/net/URL;

    move-object/from16 v4, p1

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    const/4 v4, 0x0

    .line 256
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 257
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    const/16 v6, 0x7530

    .line 258
    invoke-virtual {v3, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 259
    invoke-virtual {v3, v6}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 260
    new-instance v6, Ljava/io/File;

    move-object/from16 v7, p2

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 262
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "create new file failed"

    .line 263
    invoke-static {v0, v7}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v0

    .line 267
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v6, 0x400

    new-array v6, v6, [B

    const/4 v8, 0x0

    .line 272
    :goto_0
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v9

    const/4 v10, -0x1

    const/16 v11, 0x101

    if-eq v9, v10, :cond_3

    iget-boolean v10, v1, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    if-nez v10, :cond_3

    .line 273
    invoke-virtual {v7, v6, v4, v9}, Ljava/io/FileOutputStream;->write([BII)V

    mul-int/lit8 v10, v8, 0x64

    int-to-float v10, v10

    int-to-float v12, v0

    div-float/2addr v10, v12

    float-to-int v10, v10

    .line 275
    iget-boolean v12, v1, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    if-eqz v12, :cond_1

    const/16 v0, 0x101

    goto :goto_1

    .line 279
    :cond_1
    iget-object v12, v1, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    const/4 v13, 0x3

    const/4 v14, 0x1

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    div-int/lit16 v4, v8, 0x400

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    div-int/lit16 v4, v0, 0x400

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "KB"

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v12, v13, v14, v10, v4}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const/16 v0, 0x100

    goto :goto_1

    :cond_2
    add-int/2addr v8, v9

    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    .line 285
    :goto_1
    iget-boolean v4, v1, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    if-eqz v4, :cond_4

    const/16 v0, 0x101

    .line 288
    :cond_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 289
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 290
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 293
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v2
.end method

.method private downloadFileSafe(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 85
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 86
    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadStringSafe selected="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UpdateMgr"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v3, v3

    if-ne v1, v3, :cond_2

    return v2

    :cond_2
    :goto_2
    if-eqz v2, :cond_4

    .line 98
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v4, v3

    if-ge v0, v4, :cond_4

    if-ne v0, v1, :cond_3

    goto :goto_3

    .line 103
    :cond_3
    aget-object v2, v3, v1

    aget-object v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-direct {p0, v2, p2}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    return v2
.end method

.method private downloadString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const-string v0, "UpdateMgr"

    const-string v1, "downloadString"

    .line 221
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 225
    :cond_0
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;

    const/16 v1, 0x7530

    .line 227
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 228
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/4 v1, 0x0

    .line 229
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 230
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 235
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 237
    new-instance v6, Ljava/lang/String;

    const-string v7, "GB2312"

    invoke-direct {v6, v3, v1, v5, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 240
    :cond_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 241
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 243
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 245
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v0
.end method

.method private downloadStringSafe(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 112
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 113
    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadStringSafe selected="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UpdateMgr"

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 120
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v3, v3

    if-ne v1, v3, :cond_2

    const/4 p1, 0x0

    return-object p1

    :cond_2
    :goto_2
    if-nez v2, :cond_4

    .line 125
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mServers:[Ljava/lang/String;

    array-length v4, v3

    if-ge v0, v4, :cond_4

    if-ne v0, v1, :cond_3

    goto :goto_3

    .line 130
    :cond_3
    aget-object v2, v3, v1

    aget-object v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    return-object v2
.end method

.method private static upZipFile(Ljava/io/File;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "UpdateMgr"

    const-string v1, "upZipFile"

    .line 306
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 308
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "err mkdirs"

    if-nez v2, :cond_0

    .line 309
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 310
    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    new-instance v1, Ljava/util/zip/ZipFile;

    invoke-direct {v1, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 314
    invoke-virtual {v1}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 315
    invoke-interface {p0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/zip/ZipEntry;

    .line 316
    invoke-virtual {v1, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v4

    .line 317
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 318
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 319
    new-instance v5, Ljava/lang/String;

    const-string v6, "8859_1"

    invoke-virtual {v2, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const-string v6, "GB2312"

    invoke-direct {v5, v2, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 320
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 322
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    .line 323
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 324
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_1

    .line 325
    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "err create new file"

    .line 329
    invoke-static {v0, v5}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_2
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/high16 v2, 0x100000

    new-array v2, v2, [B

    .line 335
    :goto_1
    invoke-virtual {v4, v2}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_3

    const/4 v7, 0x0

    .line 336
    invoke-virtual {v5, v2, v7, v6}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1

    .line 338
    :cond_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 339
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    goto :goto_0

    .line 341
    :cond_4
    invoke-virtual {v1}, Ljava/util/zip/ZipFile;->close()V

    return-void
.end method

.method private updateConfig()V
    .locals 2

    const-string v0, "UpdateMgr"

    const-string v1, "updateConfig"

    .line 52
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "http://down.lltskb.com/config.json"

    .line 53
    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadStringSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "download config.json failed"

    .line 55
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 58
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->saveConfig(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public checkUpdate()Z
    .locals 8

    const-string v0, "UpdateMgr"

    const-string v1, "checkUpdate"

    .line 176
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 177
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    .line 178
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/UpdateMgr;->updateConfig()V

    const-string v2, "http://down.lltskb.com/an.ver"

    .line 179
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadStringSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0d01d6

    const/4 v4, -0x1

    if-nez v2, :cond_1

    .line 182
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    if-eqz v2, :cond_0

    .line 183
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onCheckResult(ILjava/lang/String;)Z

    :cond_0
    const-string v2, "checkUpdate failed"

    .line 185
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 188
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkUpdate ver="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "\\|"

    .line 190
    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    .line 191
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    array-length v2, v2

    const/4 v5, 0x6

    if-ge v2, v5, :cond_3

    .line 192
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    if-eqz v0, :cond_2

    .line 193
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onCheckResult(ILjava/lang/String;)Z

    :cond_2
    return v1

    .line 198
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v2, :cond_4

    .line 199
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v2, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->compareVersion(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_5

    .line 200
    :cond_4
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    if-eqz v2, :cond_5

    .line 201
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d0145

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    aget-object v6, v5, v1

    aput-object v6, v3, v1

    aget-object v1, v5, v4

    aput-object v1, v3, v4

    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    invoke-interface {v1, v4, v0}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onCheckResult(ILjava/lang/String;)Z

    move-result v0

    return v0

    .line 207
    :cond_5
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v6

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_7

    .line 208
    :cond_6
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    if-eqz v2, :cond_7

    .line 209
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d0144

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 210
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    aget-object v5, v7, v5

    aput-object v5, v6, v1

    const/4 v1, 0x4

    aget-object v1, v7, v1

    aput-object v1, v6, v4

    invoke-static {v2, v0, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    invoke-interface {v1, v3, v0}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onCheckResult(ILjava/lang/String;)Z

    move-result v0

    return v0

    .line 214
    :cond_7
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    if-eqz v2, :cond_8

    .line 215
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v3

    const v4, 0x7f0d01f2

    invoke-virtual {v3, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onCheckResult(ILjava/lang/String;)Z

    :cond_8
    const-string v2, "checkUpdate end"

    .line 216
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mStopFlag:Z

    return-void
.end method

.method public updateData()Z
    .locals 12

    const-string v0, "failed to delete"

    const-string v1, "UpdateMgr"

    .line 138
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_5

    array-length v4, v2

    const/4 v5, 0x6

    if-ge v4, v5, :cond_0

    goto/16 :goto_0

    :cond_0
    const/4 v4, 0x5

    .line 142
    aget-object v2, v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "lltskb.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadFileSafe(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v4, 0x2

    const/4 v6, -0x1

    if-eqz v2, :cond_2

    const/16 v0, 0x100

    if-ne v2, v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d00ea

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v6, v6, v1}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    :cond_1
    return v3

    .line 148
    :cond_2
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v7

    const v8, 0x7f0d01ac

    invoke-virtual {v7, v8}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x3

    const/16 v10, 0x64

    invoke-interface {v2, v9, v8, v10, v7}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    .line 149
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0d010c

    .line 151
    :try_start_0
    sget-object v7, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/lltskb/lltskb/engine/UpdateMgr;->upZipFile(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_3

    .line 168
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->init()Z

    .line 171
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d02c8

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v3, v10, v1}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    return v8

    :catch_0
    move-exception v4

    .line 160
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 161
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v11

    invoke-virtual {v11, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v9, v6, v10, v4}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    .line 162
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_4

    .line 163
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return v3

    :catch_1
    move-exception v4

    .line 153
    invoke-virtual {v4}, Ljava/util/zip/ZipException;->printStackTrace()V

    .line 154
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v11

    invoke-virtual {v11, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/util/zip/ZipException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v9, v6, v10, v4}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    .line 155
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_5

    .line 156
    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_0
    return v3
.end method

.method public updateProgram()Z
    .locals 6

    .line 66
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mRemoteVer:[Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    array-length v2, v0

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    .line 70
    aget-object v0, v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "lltskb.apk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/lltskb/lltskb/engine/UpdateMgr;->downloadFileSafe(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    const/16 v3, 0x100

    if-ne v0, v3, :cond_1

    .line 73
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v3, 0x7f0d00ea

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 74
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    const/4 v4, -0x1

    invoke-interface {v3, v2, v4, v4, v0}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    :cond_1
    return v1

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/UpdateMgr;->mListener:Lcom/lltskb/lltskb/engine/IUpdateListener;

    const/16 v3, 0x64

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d01ab

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v1, v3, v4}, Lcom/lltskb/lltskb/engine/IUpdateListener;->onDownload(IIILjava/lang/String;)Z

    return v2

    :cond_3
    :goto_0
    return v1
.end method
