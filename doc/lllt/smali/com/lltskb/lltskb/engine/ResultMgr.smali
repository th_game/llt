.class public Lcom/lltskb/lltskb/engine/ResultMgr;
.super Ljava/lang/Object;
.source "ResultMgr.java"


# static fields
.field private static instance:Lcom/lltskb/lltskb/engine/ResultMgr;


# instance fields
.field private mJlb:Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

.field private mStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mStackPos:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mStackTitle:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTitleFmt:Ljava/lang/String;

.field private pos:Ljava/lang/Object;

.field private result:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    .line 14
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    .line 29
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStack:Ljava/util/Stack;

    .line 30
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackTitle:Ljava/util/Stack;

    .line 31
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackPos:Ljava/util/Stack;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/ResultMgr;

    monitor-enter v0

    .line 23
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ResultMgr;->instance:Lcom/lltskb/lltskb/engine/ResultMgr;

    if-nez v1, :cond_0

    .line 24
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultMgr;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/ResultMgr;->instance:Lcom/lltskb/lltskb/engine/ResultMgr;

    .line 25
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ResultMgr;->instance:Lcom/lltskb/lltskb/engine/ResultMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getJlb()Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mJlb:Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    return-object v0
.end method

.method public getPos()Ljava/lang/Object;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    return-object v0
.end method

.method public getResult()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    return-object v0
.end method

.method public getStackSize()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleFmt()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mTitleFmt:Ljava/lang/String;

    return-object v0
.end method

.method public popResult()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    .line 50
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackTitle:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackPos:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    return-void
.end method

.method public pushResult()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStack:Ljava/util/Stack;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackTitle:Ljava/util/Stack;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mStackPos:Ljava/util/Stack;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mJlb:Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    return-void
.end method

.method public setPos(Ljava/lang/Object;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->pos:Ljava/lang/Object;

    return-void
.end method

.method public setResult(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    .line 75
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    return-void
.end method

.method public setSelection(Z)V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->result:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    .line 63
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 64
    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setSelected(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->title:Ljava/lang/String;

    return-void
.end method

.method public setTitleFmt(Ljava/lang/String;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultMgr;->mTitleFmt:Ljava/lang/String;

    return-void
.end method
