.class public Lcom/lltskb/lltskb/engine/Rule;
.super Ljava/lang/Object;
.source "Rule.java"


# static fields
.field public static final OUT_OF_DATE_BY_RULE:B = 0x3t

.field public static final OUT_OF_DATE_ENDSTART:B = 0x2t

.field public static final OUT_OF_DATE_NOTSTART:B = 0x1t

.field public static final UP_TO_DATE:B

.field private static instance:Lcom/lltskb/lltskb/engine/Rule;

.field private static nCount:I

.field private static ruleArr:[[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    .line 73
    check-cast v0, [[J

    sput-object v0, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    const/4 v0, 0x0

    .line 74
    sput v0, Lcom/lltskb/lltskb/engine/Rule;->nCount:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/Rule;->init()V

    return-void
.end method

.method public static get()Lcom/lltskb/lltskb/engine/Rule;
    .locals 1

    .line 25
    sget-object v0, Lcom/lltskb/lltskb/engine/Rule;->instance:Lcom/lltskb/lltskb/engine/Rule;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/lltskb/lltskb/engine/Rule;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/Rule;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/engine/Rule;->instance:Lcom/lltskb/lltskb/engine/Rule;

    .line 28
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/engine/Rule;->instance:Lcom/lltskb/lltskb/engine/Rule;

    return-object v0
.end method

.method private getCalendar(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 5

    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 84
    :try_start_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x6

    .line 85
    :try_start_1
    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v4, 0x8

    .line 86
    :try_start_2
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 88
    :catch_2
    :goto_0
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "setDate error."

    invoke-virtual {p1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 p1, 0x0

    :goto_1
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 93
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v3, 0x1

    .line 94
    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    const/4 v2, 0x2

    .line 95
    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->set(II)V

    const/4 v0, 0x5

    .line 96
    invoke-virtual {v1, v0, p1}, Ljava/util/Calendar;->set(II)V

    return-object v1
.end method

.method private init()V
    .locals 1

    .line 36
    sget-object v0, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    if-eqz v0, :cond_0

    return-void

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/Rule;->readFile()V

    return-void
.end method

.method private readFile()V
    .locals 9

    .line 43
    :try_start_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 44
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/lltskb/lltskb/engine/ResMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "t.rule"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 47
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 48
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 49
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    .line 51
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 52
    :cond_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 55
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    sput v1, Lcom/lltskb/lltskb/engine/Rule;->nCount:I

    .line 56
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, J

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[J

    sput-object v1, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 57
    :goto_1
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 58
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, " "

    .line 59
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :try_start_1
    sget-object v5, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    aget-object v5, v5, v2

    aget-object v6, v4, v1

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v5, v1

    .line 62
    sget-object v5, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    aget-object v5, v5, v2

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    aput-wide v7, v5, v6
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 64
    :catch_0
    :try_start_2
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_1
    :cond_2
    return-void
.end method


# virtual methods
.method public outOfDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)B
    .locals 8

    :goto_0
    if-gez p4, :cond_0

    add-int/lit16 p4, p4, 0x100

    goto :goto_0

    .line 105
    :cond_0
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/Rule;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 106
    invoke-direct {p0, p3}, Lcom/lltskb/lltskb/engine/Rule;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object p3

    const/4 v1, 0x5

    neg-int p5, p5

    .line 109
    invoke-virtual {p3, v1, p5}, Ljava/util/Calendar;->add(II)V

    .line 110
    new-instance p5, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyyMMdd"

    invoke-direct {p5, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 111
    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p5, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p5

    .line 113
    invoke-virtual {p1, p5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    const/4 v1, 0x1

    if-lez p1, :cond_1

    return v1

    .line 115
    :cond_1
    invoke-virtual {p2, p5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_2

    const/4 p1, 0x2

    return p1

    .line 118
    :cond_2
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    .line 120
    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    .line 122
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide p2

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr p2, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr p2, v2

    const-wide/16 v2, 0x3c

    div-long/2addr p2, v2

    div-long/2addr p2, v2

    const-wide/16 v2, 0x18

    div-long/2addr p2, v2

    .line 125
    sget p1, Lcom/lltskb/lltskb/engine/Rule;->nCount:I

    const/4 p5, 0x0

    if-ge p4, p1, :cond_4

    .line 127
    sget-object p1, Lcom/lltskb/lltskb/engine/Rule;->ruleArr:[[J

    aget-object v0, p1, p4

    aget-wide v2, v0, p5

    .line 128
    aget-object p1, p1, p4

    aget-wide v0, p1, v1

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x1

    cmp-long p1, v2, v4

    if-nez p1, :cond_3

    move-wide v2, v6

    :cond_3
    const-wide/16 v4, 0x2

    .line 130
    rem-long/2addr p2, v2

    invoke-virtual {p0, v4, v5, p2, p3}, Lcom/lltskb/lltskb/engine/Rule;->pow(JJ)J

    move-result-wide p1

    and-long/2addr p1, v0

    cmp-long p3, v2, v6

    if-eqz p3, :cond_4

    cmp-long p3, p1, v6

    if-gez p3, :cond_4

    const/4 p1, 0x3

    return p1

    :cond_4
    return p5
.end method

.method public pow(JJ)J
    .locals 10

    const/4 v0, 0x0

    move-wide v1, p1

    :goto_0
    int-to-long v3, v0

    const-wide/16 v5, 0x1

    sub-long v7, p3, v5

    cmp-long v9, v3, v7

    if-gez v9, :cond_0

    mul-long v1, v1, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    cmp-long v0, p3, p1

    if-nez v0, :cond_1

    move-wide v1, v5

    :cond_1
    return-wide v1
.end method
