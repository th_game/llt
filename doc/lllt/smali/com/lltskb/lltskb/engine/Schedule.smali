.class public Lcom/lltskb/lltskb/engine/Schedule;
.super Ljava/lang/Object;
.source "Schedule.java"


# static fields
.field public static final SCH_TYPE_FZ:I = 0x1

.field public static final SCH_TYPE_JZ:I = 0x2

.field public static final SCH_TYPE_NONE:I = -0x1

.field public static final SCH_TYPE_SCHEDULE:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get_schedule(III[B)I
    .locals 17

    move/from16 v0, p3

    move-object/from16 v1, p4

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 79
    aput-byte v2, v1, v3

    .line 80
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v4

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-virtual {v4, v5, v6}, Lcom/lltskb/lltskb/engine/ResMgr;->getScheduleUnit(II)[B

    move-result-object v4

    if-nez v4, :cond_0

    return v3

    .line 83
    :cond_0
    aget-byte v5, v4, v3

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    :goto_0
    if-ge v7, v5, :cond_7

    add-int/lit8 v9, v8, 0x1

    .line 88
    aget-byte v10, v4, v8

    add-int/lit8 v11, v9, 0x1

    .line 89
    aget-byte v9, v4, v9

    add-int/lit8 v12, v11, 0x1

    .line 90
    aget-byte v11, v4, v11

    add-int/lit8 v13, v12, 0x1

    .line 91
    aget-byte v12, v4, v12

    add-int/lit8 v14, v13, 0x1

    .line 92
    aget-byte v13, v4, v13

    mul-int/lit16 v11, v11, 0x80

    add-int/2addr v9, v11

    mul-int/lit16 v12, v12, 0x4000

    add-int/2addr v9, v12

    const/high16 v11, 0x200000

    mul-int v13, v13, v11

    add-int/2addr v9, v13

    add-int/lit8 v12, v14, 0x1

    .line 95
    aget-byte v13, v4, v14

    add-int/lit8 v14, v12, 0x1

    .line 96
    aget-byte v12, v4, v12

    add-int/lit8 v15, v14, 0x1

    .line 97
    aget-byte v14, v4, v14

    add-int/lit8 v16, v15, 0x1

    .line 98
    aget-byte v15, v4, v15

    mul-int/lit16 v12, v12, 0x80

    add-int/2addr v13, v12

    mul-int/lit16 v14, v14, 0x4000

    add-int/2addr v13, v14

    mul-int v15, v15, v11

    add-int/2addr v13, v15

    if-nez v10, :cond_1

    add-int/lit8 v16, v16, 0x3

    :cond_1
    if-lt v0, v9, :cond_6

    if-gt v0, v13, :cond_6

    .line 106
    aget-byte v9, v1, v3

    const/16 v11, 0xc

    if-ne v9, v2, :cond_3

    if-ne v10, v6, :cond_2

    const/16 v9, 0x9

    .line 108
    invoke-static {v4, v8, v1, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :cond_2
    if-nez v10, :cond_6

    .line 110
    invoke-static {v4, v8, v1, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 112
    :cond_3
    aget-byte v9, v1, v3

    if-nez v9, :cond_4

    if-ne v10, v6, :cond_6

    .line 114
    aput-byte v6, v1, v3

    goto :goto_1

    .line 116
    :cond_4
    aget-byte v9, v1, v3

    const/4 v12, 0x2

    if-ne v9, v6, :cond_5

    if-ne v10, v12, :cond_6

    .line 118
    aput-byte v12, v1, v3

    goto :goto_1

    .line 120
    :cond_5
    aget-byte v9, v1, v3

    if-ne v9, v12, :cond_6

    if-nez v10, :cond_6

    .line 122
    invoke-static {v4, v8, v1, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_1
    add-int/lit8 v7, v7, 0x1

    move/from16 v8, v16

    goto :goto_0

    :cond_7
    return v3
.end method

.method public hasScheduleData(I)Z
    .locals 1

    .line 21
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->hasScheduleData(I)Z

    move-result p1

    return p1
.end method

.method public isCancelTrain(II)Z
    .locals 16

    move/from16 v0, p2

    .line 25
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getCancelTrainData(II)[B

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 28
    :cond_0
    aget-byte v3, v1, v2

    const/4 v4, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x1

    :goto_0
    if-ge v6, v3, :cond_7

    add-int/lit8 v9, v8, 0x1

    .line 33
    aget-byte v8, v1, v8

    add-int/lit8 v10, v9, 0x1

    .line 34
    aget-byte v9, v1, v9

    add-int/lit8 v11, v10, 0x1

    .line 35
    aget-byte v10, v1, v10

    add-int/lit8 v12, v11, 0x1

    .line 36
    aget-byte v11, v1, v11

    add-int/lit8 v13, v12, 0x1

    .line 37
    aget-byte v12, v1, v12

    mul-int/lit16 v10, v10, 0x80

    add-int/2addr v9, v10

    mul-int/lit16 v11, v11, 0x4000

    add-int/2addr v9, v11

    const/high16 v10, 0x200000

    mul-int v12, v12, v10

    add-int/2addr v9, v12

    add-int/lit8 v11, v13, 0x1

    .line 40
    aget-byte v12, v1, v13

    add-int/lit8 v13, v11, 0x1

    .line 41
    aget-byte v11, v1, v11

    add-int/lit8 v14, v13, 0x1

    .line 42
    aget-byte v13, v1, v13

    add-int/lit8 v15, v14, 0x1

    .line 43
    aget-byte v14, v1, v14

    mul-int/lit16 v11, v11, 0x80

    add-int/2addr v12, v11

    mul-int/lit16 v13, v13, 0x4000

    add-int/2addr v12, v13

    mul-int v14, v14, v10

    add-int/2addr v12, v14

    if-nez v8, :cond_1

    add-int/lit8 v15, v15, 0x3

    :cond_1
    const/4 v10, 0x2

    if-lt v0, v9, :cond_6

    if-gt v0, v12, :cond_6

    if-ne v7, v4, :cond_3

    if-ne v8, v5, :cond_2

    :goto_1
    const/4 v7, 0x1

    goto :goto_3

    :cond_2
    if-nez v8, :cond_6

    :goto_2
    const/4 v7, 0x0

    goto :goto_3

    :cond_3
    if-nez v7, :cond_4

    if-ne v8, v5, :cond_6

    goto :goto_1

    :cond_4
    if-ne v7, v5, :cond_5

    if-ne v8, v10, :cond_6

    const/4 v7, 0x2

    goto :goto_3

    :cond_5
    if-ne v7, v10, :cond_6

    if-nez v8, :cond_6

    goto :goto_2

    :cond_6
    :goto_3
    add-int/lit8 v6, v6, 0x1

    move v8, v15

    goto :goto_0

    :cond_7
    if-ne v7, v5, :cond_8

    const/4 v2, 0x1

    :cond_8
    return v2
.end method
