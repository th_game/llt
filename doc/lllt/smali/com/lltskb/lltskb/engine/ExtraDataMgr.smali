.class public Lcom/lltskb/lltskb/engine/ExtraDataMgr;
.super Lcom/lltskb/lltskb/engine/DataMgr;
.source "ExtraDataMgr.java"


# instance fields
.field private mExtraMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/lltskb/lltskb/engine/ExtraData;",
            ">;"
        }
    .end annotation
.end field

.field private mFuXingHaoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mQiYe:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    .line 33
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mFuXingHaoMap:Ljava/util/Map;

    .line 41
    :try_start_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->initData()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 43
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private initData()V
    .locals 20

    move-object/from16 v1, p0

    .line 57
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mExtraMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 62
    :cond_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mQiYe:Ljava/util/Vector;

    .line 64
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v3

    const/4 v4, 0x2

    const/4 v0, 0x2

    const/4 v5, 0x0

    :goto_0
    const-string v6, "UTF-8"

    if-ge v5, v3, :cond_1

    .line 68
    iget-object v7, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v8, v0, 0x1

    aget-byte v0, v7, v0

    and-int/lit16 v7, v0, 0xff

    .line 70
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v9, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-direct {v0, v9, v8, v7, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 71
    iget-object v6, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mQiYe:Ljava/util/Vector;

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 73
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :goto_1
    add-int v0, v8, v7

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 79
    :cond_1
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-virtual {v1, v3, v0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v3

    add-int/2addr v0, v4

    .line 81
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mExtraMap:Ljava/util/Map;

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v3, :cond_6

    .line 83
    new-instance v7, Lcom/lltskb/lltskb/engine/ExtraData;

    invoke-direct {v7}, Lcom/lltskb/lltskb/engine/ExtraData;-><init>()V

    .line 84
    iget-object v8, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-virtual {v1, v8, v0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/ExtraData;->trainIdx:I

    add-int/lit8 v0, v0, 0x2

    .line 87
    iget-object v8, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v9, v0, 0x1

    aget-byte v0, v8, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, v7, Lcom/lltskb/lltskb/engine/ExtraData;->nQiYe:I

    .line 88
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v8, v9, 0x1

    aget-byte v0, v0, v9

    and-int/lit16 v9, v0, 0xff

    if-nez v9, :cond_2

    .line 90
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mExtraMap:Ljava/util/Map;

    iget v9, v7, Lcom/lltskb/lltskb/engine/ExtraData;->trainIdx:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v0, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v8

    goto/16 :goto_6

    .line 94
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v7, Lcom/lltskb/lltskb/engine/ExtraData;->mStations:Ljava/util/Map;

    move v0, v8

    const/4 v8, 0x0

    # v9 is the size?
    :goto_3
    if-ge v8, v9, :cond_5

    .line 97
    iget-object v10, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-virtual {v1, v10, v0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v10

    add-int/lit8 v0, v0, 0x2

    .line 99
    new-instance v11, Lcom/lltskb/lltskb/engine/JPK;

    invoke-direct {v11}, Lcom/lltskb/lltskb/engine/JPK;-><init>()V

    .line 100
    new-instance v12, Ljava/util/Vector;

    invoke-direct {v12}, Ljava/util/Vector;-><init>()V

    iput-object v12, v11, Lcom/lltskb/lltskb/engine/JPK;->vect:Ljava/util/Vector;

    .line 101
    iput v10, v11, Lcom/lltskb/lltskb/engine/JPK;->station:I

    .line 104
    iget-object v10, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v12, v0, 0x1

    aget-byte v0, v10, v0

    and-int/lit16 v10, v0, 0xff

    move v0, v12

    const/4 v12, 0x0

    :goto_4
    if-ge v12, v10, :cond_4

    # v10 is the loop
    .line 106
    new-instance v13, Lcom/lltskb/lltskb/engine/JpkUnit;

    invoke-direct {v13}, Lcom/lltskb/lltskb/engine/JpkUnit;-><init>()V

    .line 108
    iget-object v14, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    # v0 is the index
    # v15 is the index
    add-int/lit8 v15, v0, 0x1

    # v0 byte0
    aget-byte v0, v14, v0

    .line 109
    iget-object v14, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v16, v15, 0x1

    # v14 byte1
    aget-byte v14, v14, v15

    .line 110
    iget-object v15, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v17, v16, 0x1

    # v15 byte2
    aget-byte v15, v15, v16

    .line 111
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v18, v17, 0x1

    # v2 byte3
    aget-byte v2, v2, v17

    mul-int/lit16 v14, v14, 0x80

    # byte 0 += byte1 * 0x80
    add-int/2addr v0, v14

    # byte2 *= 0x80
    mul-int/lit16 v15, v15, 0x4000

    # byte0 += byte2 * 0x400
    add-int/2addr v0, v15

    const/high16 v14, 0x200000

    mul-int v2, v2, v14

    # byte3 * 0x200000 + byte2 * 4000 + byte1 * 80 + byte0
    add-int/2addr v0, v2

    .line 112
    iput v0, v13, Lcom/lltskb/lltskb/engine/JpkUnit;->start:I

    .line 114
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v2, v18, 0x1

    # v0
    aget-byte v0, v0, v18

    .line 115
    iget-object v15, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v17, v2, 0x1

    # v2
    aget-byte v2, v15, v2

    .line 116
    iget-object v15, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v18, v17, 0x1

    # v15
    aget-byte v15, v15, v17

    .line 117
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v19, v18, 0x1

    # v4
    aget-byte v4, v4, v18

    mul-int/lit16 v2, v2, 0x80

    add-int/2addr v0, v2

    mul-int/lit16 v15, v15, 0x4000

    add-int/2addr v0, v15

    mul-int v4, v4, v14

    add-int/2addr v0, v4

    .line 118
    iput v0, v13, Lcom/lltskb/lltskb/engine/JpkUnit;->end:I

    .line 120
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    add-int/lit8 v2, v19, 0x1

    aget-byte v0, v0, v19

    and-int/lit16 v4, v0, 0xff

    if-lez v4, :cond_3

    .line 123
    :try_start_1
    new-instance v0, Ljava/lang/String;

    iget-object v14, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-direct {v0, v14, v2, v4, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/JpkUnit;->jpk:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    .line 125
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :goto_5
    add-int/2addr v2, v4

    :cond_3
    move v0, v2

    .line 129
    iget-object v2, v11, Lcom/lltskb/lltskb/engine/JPK;->vect:Ljava/util/Vector;

    invoke-virtual {v2, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    goto :goto_4

    .line 131
    :cond_4
    iget-object v2, v7, Lcom/lltskb/lltskb/engine/ExtraData;->mStations:Ljava/util/Map;

    iget v4, v11, Lcom/lltskb/lltskb/engine/JPK;->station:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    goto/16 :goto_3

    .line 133
    :cond_5
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mExtraMap:Ljava/util/Map;

    iget v4, v7, Lcom/lltskb/lltskb/engine/ExtraData;->trainIdx:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_6
    add-int/lit8 v5, v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    goto/16 :goto_2

    .line 136
    :cond_6
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mFuXingHaoMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 137
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    array-length v2, v2

    const/4 v3, 0x2

    sub-int/2addr v2, v3

    if-ge v0, v2, :cond_7

    .line 138
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-virtual {v1, v2, v0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v2

    add-int/2addr v0, v3

    move v4, v0

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_7

    .line 141
    iget-object v5, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->data:[B

    invoke-virtual {v1, v5, v4}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->readShort([BI)I

    move-result v5

    add-int/2addr v4, v3

    .line 143
    iget-object v6, v1, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mFuXingHaoMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    return-void
.end method


# virtual methods
.method public getExtraData(I)Lcom/lltskb/lltskb/engine/ExtraData;
    .locals 1

    .line 159
    :try_start_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->initData()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 161
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mExtraMap:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 164
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ExtraData;

    return-object p1
.end method

.method public getHcdd(Lcom/lltskb/lltskb/engine/ExtraData;II)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 190
    :cond_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/ExtraData;->mStations:Ljava/util/Map;

    if-nez v1, :cond_1

    return-object v0

    .line 191
    :cond_1
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/ExtraData;->mStations:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/JPK;

    if-eqz p1, :cond_4

    .line 192
    iget-object p2, p1, Lcom/lltskb/lltskb/engine/JPK;->vect:Ljava/util/Vector;

    if-nez p2, :cond_2

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    .line 195
    :goto_0
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/JPK;->vect:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p2, v1, :cond_4

    .line 196
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/JPK;->vect:Ljava/util/Vector;

    invoke-virtual {v0, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/JpkUnit;

    .line 197
    iget v1, v0, Lcom/lltskb/lltskb/engine/JpkUnit;->start:I

    if-lt p3, v1, :cond_3

    iget v1, v0, Lcom/lltskb/lltskb/engine/JpkUnit;->end:I

    if-gt p3, v1, :cond_3

    .line 198
    iget-object p1, v0, Lcom/lltskb/lltskb/engine/JpkUnit;->jpk:Ljava/lang/String;

    return-object p1

    .line 200
    :cond_3
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/JpkUnit;->jpk:Ljava/lang/String;

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    return-object v0
.end method

.method public getQiYe(I)Ljava/lang/String;
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mQiYe:Ljava/util/Vector;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    if-ltz p1, :cond_1

    .line 175
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mQiYe:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_1
    return-object v1
.end method

.method public isFuXingHao(I)Z
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->mFuXingHaoMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected readShort([BI)I
    .locals 1

    if-ltz p2, :cond_1

    .line 48
    array-length v0, p1

    if-lt p2, v0, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    mul-int/lit16 v0, v0, 0x80

    add-int/lit8 p2, p2, 0x1

    .line 51
    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    add-int/2addr v0, p1

    return v0

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method
