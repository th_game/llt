.class public abstract Lcom/lltskb/lltskb/engine/QueryBase;
.super Ljava/lang/Object;
.source "QueryBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/QueryBase$StationNode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryBase"


# instance fields
.field bend:[B

.field bstart:[B

.field db:Lcom/lltskb/lltskb/engine/ResMgr;

.field protected fuzzy:Z

.field hiden:Z

.field lastErr:I

.field mAuxDay:I

.field protected mFilter:I

.field mMaxDate:Ljava/lang/String;

.field mMinDate:Ljava/lang/String;

.field mRuleIndex:I

.field private runRule:Lcom/lltskb/lltskb/engine/Rule;

.field protected strDate:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZ)V
    .locals 7

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    const/4 v0, 0x1

    .line 21
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->fuzzy:Z

    const/4 v1, 0x0

    .line 22
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->hiden:Z

    .line 23
    iput v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->lastErr:I

    const/16 v1, 0xff

    .line 31
    iput v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mFilter:I

    const/16 v1, 0xa

    new-array v2, v1, [B

    .line 59
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bstart:[B

    new-array v2, v1, [B

    .line 60
    iput-object v2, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bend:[B

    .line 61
    invoke-static {}, Lcom/lltskb/lltskb/engine/Rule;->get()Lcom/lltskb/lltskb/engine/Rule;

    move-result-object v2

    iput-object v2, p0, Lcom/lltskb/lltskb/engine/QueryBase;->runRule:Lcom/lltskb/lltskb/engine/Rule;

    .line 62
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 63
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    const/4 v3, 0x2

    .line 64
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const-string v5, "0"

    const/16 v6, 0x9

    if-ge v4, v6, :cond_0

    .line 65
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    .line 67
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    const/4 v0, 0x5

    .line 68
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge v3, v1, :cond_1

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    .line 70
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    .line 71
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->fuzzy:Z

    .line 72
    iput-boolean p2, p0, Lcom/lltskb/lltskb/engine/QueryBase;->hiden:Z

    return-void
.end method

.method private isEqualInFuzzy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 958
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u4e1c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u5357"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u897f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 959
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u5317"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u65b0\u533a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 960
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u8679\u6865"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u56ed\u533a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 961
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u9f99\u95e8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u9ad8\u5d0e"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 962
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\u79e6\u90fd"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private isFuzzyEqualStation(II[BZ)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p4, :cond_1

    .line 116
    aget-byte p3, p3, v1

    add-int/2addr p2, p3

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 p4, 0x0

    .line 120
    :goto_1
    array-length v2, p3

    if-ge p4, v2, :cond_4

    .line 121
    aget-byte v2, p3, p4

    const/16 v3, 0xa

    if-le v2, v3, :cond_2

    return v1

    .line 123
    :cond_2
    aget-byte v2, p3, p4

    add-int/2addr v2, p2

    if-ne p1, v2, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    .line 127
    :cond_4
    :goto_2
    array-length p1, p3

    if-eq p4, p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    return v0
.end method


# virtual methods
.method formatTime(II)Ljava/lang/String;
    .locals 3

    .line 102
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v1, p2

    const-string p1, "%02d:%02d"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAuxDay()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    return v0
.end method

.method public getLastErr()I
    .locals 1

    .line 1008
    iget v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->lastErr:I

    return v0
.end method

.method public getMaxDate()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    return-object v0
.end method

.method getMaxSpeed(C)I
    .locals 1

    const/16 v0, 0x47

    if-ne p1, v0, :cond_0

    const/16 p1, 0x190

    goto :goto_2

    :cond_0
    const/16 v0, 0x44

    if-eq p1, v0, :cond_4

    const/16 v0, 0x43

    if-eq p1, v0, :cond_4

    const/16 v0, 0x53

    if-ne p1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x4b

    if-eq p1, v0, :cond_3

    const/16 v0, 0x5a

    if-eq p1, v0, :cond_3

    const/16 v0, 0x54

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/16 p1, 0x96

    goto :goto_2

    :cond_3
    :goto_0
    const/16 p1, 0xaa

    goto :goto_2

    :cond_4
    :goto_1
    const/16 p1, 0x15e

    :goto_2
    return p1
.end method

.method public getMinDate()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    return-object v0
.end method

.method abstract getQueryType()I
.end method

.method public getRuleIndex()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    return v0
.end method

.method getSimilarStation(Ljava/lang/String;)Ljava/util/Vector;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1012
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 1013
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/QueryBase;->isWuhan(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "\u6b66\u6c49"

    .line 1014
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "\u6c49\u53e3"

    const-string v4, "\u6b66\u660c"

    if-eqz v2, :cond_0

    .line 1015
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1016
    invoke-virtual {v0, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1017
    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 1018
    :cond_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1019
    invoke-virtual {v0, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1020
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1021
    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 1023
    :cond_1
    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1024
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1025
    invoke-virtual {v0, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    .line 1029
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method getStationIndex(Ljava/lang/String;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1043
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    .line 1044
    iget-boolean v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->fuzzy:Z

    if-nez v1, :cond_1

    if-gez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1048
    :cond_0
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 1049
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object p1

    :cond_1
    if-gez v0, :cond_2

    .line 1054
    invoke-static {}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->get()Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->getStationIdx(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    return-object p1

    .line 1057
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->get()Lcom/lltskb/lltskb/engine/FuzzyStationMgr;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/FuzzyStationMgr;->getStationIdx(Ljava/lang/Integer;)Ljava/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method getStationIndexFuzzy(Ljava/lang/String;)I
    .locals 3

    .line 1061
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    return v0

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u4e1c"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    return v0

    .line 1065
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u897f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    return v0

    .line 1067
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u5357"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    return v0

    .line 1069
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u5317"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationIndex(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_4

    :cond_4
    return p1
.end method

.method getTrainStatusString(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const-string p1, "[*]"

    return-object p1

    :cond_1
    const-string p1, "[+]"

    return-object p1

    :cond_2
    const-string p1, "[-]"

    return-object p1
.end method

.method getTrainTime(III)[B
    .locals 33

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    .line 668
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v4, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v4

    if-nez v4, :cond_0

    const-string v1, "QueryBase"

    const-string v2, "train is null"

    .line 670
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    return-object v1

    :cond_0
    const/16 v5, 0x16

    new-array v5, v5, [B

    const/16 v6, 0x12

    .line 700
    div-int/lit16 v7, v2, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    const/16 v6, 0x13

    .line 701
    rem-int/lit16 v7, v2, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 703
    div-int/lit16 v6, v3, 0x80

    int-to-byte v6, v6

    const/16 v7, 0x14

    aput-byte v6, v5, v7

    const/16 v6, 0x15

    .line 704
    rem-int/lit16 v8, v3, 0x80

    int-to-byte v8, v8

    aput-byte v8, v5, v6

    const/16 v6, 0xc

    .line 706
    aget-byte v8, v4, v6

    aput-byte v8, v5, v6

    const/16 v8, 0xd

    const/16 v9, 0xd

    .line 707
    aget-byte v9, v4, v9

    aput-byte v9, v5, v8

    const/16 v8, 0xe

    .line 708
    array-length v9, v4

    const/4 v10, 0x7

    sub-int/2addr v9, v10

    aget-byte v9, v4, v9

    aput-byte v9, v5, v8

    const/16 v8, 0xf

    .line 709
    array-length v9, v4

    const/4 v11, 0x6

    sub-int/2addr v9, v11

    aget-byte v9, v4, v9

    aput-byte v9, v5, v8

    .line 711
    array-length v8, v4

    const/4 v9, 0x0

    .line 712
    aget-byte v12, v4, v9

    const/4 v13, 0x4

    aput-byte v12, v5, v13

    const/4 v12, 0x5

    const/4 v14, 0x1

    .line 713
    aget-byte v15, v4, v14

    aput-byte v15, v5, v12

    const/4 v12, 0x2

    .line 714
    aget-byte v15, v4, v12

    aput-byte v15, v5, v11

    const/4 v15, 0x3

    .line 715
    aget-byte v16, v4, v15

    const/16 v17, 0x5

    .line 716
    aget-byte v6, v4, v13

    .line 717
    aget-byte v14, v4, v17

    .line 718
    aget-byte v11, v4, v11

    mul-int/lit16 v6, v6, 0x80

    add-int v16, v16, v6

    mul-int/lit16 v14, v14, 0x4000

    add-int v16, v16, v14

    const/high16 v6, 0x200000

    mul-int v11, v11, v6

    add-int v16, v16, v11

    const/16 v6, 0x8

    .line 722
    aget-byte v11, v4, v10

    const/16 v14, 0x9

    .line 723
    aget-byte v6, v4, v6

    const/16 v17, 0xa

    .line 724
    aget-byte v14, v4, v14

    .line 725
    aget-byte v17, v4, v17

    mul-int/lit16 v6, v6, 0x80

    add-int/2addr v11, v6

    mul-int/lit16 v14, v14, 0x4000

    add-int/2addr v11, v14

    const/high16 v6, 0x200000

    mul-int v17, v17, v6

    add-int v11, v11, v17

    const/16 v6, 0xb

    .line 727
    aget-byte v14, v4, v6

    iput v14, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    .line 729
    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    .line 730
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    .line 731
    iput v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    new-array v13, v13, [B

    .line 738
    new-instance v14, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {v14}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    .line 739
    invoke-virtual {v14, v1}, Lcom/lltskb/lltskb/engine/Schedule;->hasScheduleData(I)Z

    move-result v16

    new-array v11, v7, [B

    .line 745
    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    const/16 v7, 0xc

    const/16 v18, 0x0

    const/16 v19, 0x0

    :goto_0
    if-ge v7, v8, :cond_3

    .line 749
    new-instance v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    invoke-direct {v6, v0}, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;-><init>(Lcom/lltskb/lltskb/engine/QueryBase;)V

    add-int/lit8 v21, v7, 0x1

    .line 750
    aget-byte v7, v4, v7

    and-int/lit16 v7, v7, 0xff

    mul-int/lit16 v7, v7, 0x80

    add-int/lit8 v22, v21, 0x1

    aget-byte v15, v4, v21

    and-int/lit16 v15, v15, 0xff

    add-int/2addr v7, v15

    iput v7, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    add-int/lit8 v7, v22, 0x1

    .line 751
    aget-byte v15, v4, v22

    iput-byte v15, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    add-int/lit8 v15, v7, 0x1

    .line 752
    aget-byte v7, v4, v7

    iput-byte v7, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    add-int/lit8 v7, v15, 0x1

    .line 753
    aget-byte v15, v4, v15

    iput-byte v15, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    add-int/lit8 v15, v7, 0x1

    .line 754
    aget-byte v7, v4, v7

    iput-byte v7, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    add-int/lit8 v7, v15, 0x1

    .line 755
    aget-byte v15, v4, v15

    iput-byte v15, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    .line 756
    iget v15, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v15, v2, :cond_1

    const/16 v18, 0x1

    .line 759
    :cond_1
    iget v15, v6, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v15, v3, :cond_2

    const/16 v19, 0x1

    .line 763
    :cond_2
    invoke-virtual {v10, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/16 v6, 0xb

    const/4 v15, 0x3

    goto :goto_0

    :cond_3
    if-eqz v18, :cond_21

    if-nez v19, :cond_4

    goto/16 :goto_f

    :cond_4
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v23, -0x1

    const/16 v24, 0x0

    .line 781
    :goto_1
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v9

    const/16 v22, 0x11

    const/16 v25, 0x10

    move/from16 v26, v8

    if-ge v4, v9, :cond_1d

    if-eq v6, v12, :cond_1d

    .line 782
    invoke-virtual {v10, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    .line 783
    iget v8, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    .line 784
    iget-byte v12, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    move/from16 v27, v8

    .line 785
    iget-byte v8, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    move/from16 v28, v8

    .line 786
    iget-byte v8, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    move/from16 v29, v8

    .line 787
    iget-byte v8, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    .line 788
    iget-byte v9, v9, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    const/16 v30, -0x1

    const/16 v21, 0x0

    aput-byte v30, v11, v21

    if-eqz v16, :cond_c

    move-object/from16 v30, v10

    .line 793
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v10

    move/from16 v31, v4

    const/4 v4, 0x2

    if-ne v10, v4, :cond_5

    const/16 v10, 0x4000

    goto :goto_2

    :cond_5
    move/from16 v10, v27

    :goto_2
    move/from16 v32, v7

    if-nez v15, :cond_6

    .line 797
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v7

    if-ne v7, v4, :cond_8

    :cond_6
    if-nez v17, :cond_8

    .line 799
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    iget v7, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    neg-int v7, v7

    invoke-static {v4, v7}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v14, v1, v4}, Lcom/lltskb/lltskb/engine/Schedule;->isCancelTrain(II)Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0xb

    const/4 v7, 0x3

    aput-byte v7, v5, v4

    :cond_7
    const/16 v17, 0x1

    .line 805
    :cond_8
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    move/from16 v7, v24

    invoke-static {v4, v7}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v14, v1, v10, v4, v11}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    const/4 v4, 0x0

    .line 807
    aget-byte v1, v11, v4

    const/4 v4, 0x1

    if-ne v1, v4, :cond_b

    move/from16 v1, v23

    if-le v1, v12, :cond_9

    if-nez v15, :cond_9

    .line 809
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_9

    iget v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    add-int/2addr v8, v4

    iput v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    :cond_9
    if-le v1, v12, :cond_a

    if-eqz v15, :cond_a

    add-int/lit8 v24, v7, 0x1

    goto :goto_3

    :cond_a
    move/from16 v24, v7

    :goto_3
    move-object/from16 v23, v14

    move/from16 v8, v26

    move/from16 v7, v32

    const/16 v4, 0x14

    goto/16 :goto_d

    :cond_b
    move/from16 v1, v23

    const/4 v4, 0x0

    .line 815
    aget-byte v23, v11, v4

    if-nez v23, :cond_d

    const/16 v4, 0x9

    .line 816
    aget-byte v12, v11, v4

    const/16 v4, 0xa

    .line 817
    aget-byte v4, v11, v4

    const/16 v20, 0xb

    .line 818
    aget-byte v23, v11, v20

    move/from16 v28, v4

    move/from16 v29, v23

    goto :goto_4

    :cond_c
    move/from16 v31, v4

    move/from16 v32, v7

    move-object/from16 v30, v10

    move/from16 v1, v23

    move/from16 v7, v24

    move/from16 v10, v27

    :cond_d
    :goto_4
    if-le v1, v12, :cond_e

    if-nez v15, :cond_e

    .line 825
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v4

    move-object/from16 v23, v14

    const/4 v14, 0x2

    if-eq v4, v14, :cond_f

    iget v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    const/4 v14, 0x1

    add-int/2addr v4, v14

    iput v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    goto :goto_5

    :cond_e
    move-object/from16 v23, v14

    :cond_f
    :goto_5
    if-le v1, v12, :cond_10

    if-eqz v15, :cond_10

    add-int/lit8 v24, v7, 0x1

    goto :goto_6

    :cond_10
    move/from16 v24, v7

    :goto_6
    const/4 v1, 0x1

    if-eq v6, v1, :cond_17

    if-eqz v18, :cond_17

    if-ne v2, v10, :cond_17

    const/4 v4, 0x0

    .line 833
    aget-byte v7, v11, v4

    if-ne v7, v1, :cond_11

    .line 834
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_17

    :cond_11
    const/16 v1, 0x12

    .line 836
    div-int/lit16 v4, v10, 0x80

    int-to-byte v4, v4

    aput-byte v4, v5, v1

    const/16 v1, 0x13

    .line 837
    rem-int/lit16 v10, v10, 0x80

    int-to-byte v4, v10

    aput-byte v4, v5, v1

    const/16 v1, 0x18

    if-ne v2, v3, :cond_14

    .line 839
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v4

    const/4 v6, 0x3

    if-ne v4, v6, :cond_14

    const/4 v4, 0x0

    aput-byte v12, v5, v4

    const/4 v4, 0x1

    aput-byte v28, v5, v4

    add-int v4, v28, v29

    int-to-byte v4, v4

    :cond_12
    :goto_7
    const/16 v6, 0x3c

    if-lt v4, v6, :cond_13

    add-int/lit8 v12, v12, 0x1

    int-to-byte v12, v12

    add-int/lit8 v4, v4, -0x3c

    int-to-byte v4, v4

    if-lt v12, v1, :cond_12

    add-int/lit8 v12, v12, -0x18

    int-to-byte v12, v12

    goto :goto_7

    :cond_13
    const/4 v6, 0x2

    aput-byte v12, v5, v6

    const/4 v1, 0x3

    aput-byte v4, v5, v1

    const/4 v7, 0x0

    const/4 v10, 0x1

    aput-byte v7, v13, v10

    aput-byte v7, v13, v7

    aput-byte v8, v13, v6

    aput-byte v9, v13, v1

    move/from16 v28, v4

    const/4 v1, 0x2

    goto :goto_9

    :cond_14
    add-int v4, v28, v29

    int-to-byte v4, v4

    :cond_15
    :goto_8
    const/16 v6, 0x3c

    if-lt v4, v6, :cond_16

    add-int/lit8 v12, v12, 0x1

    int-to-byte v12, v12

    add-int/lit8 v4, v4, -0x3c

    int-to-byte v4, v4

    if-lt v12, v1, :cond_15

    add-int/lit8 v12, v12, -0x18

    int-to-byte v12, v12

    goto :goto_8

    :cond_16
    const/4 v6, 0x0

    aput-byte v12, v5, v6

    const/4 v1, 0x1

    aput-byte v4, v5, v1

    aput-byte v8, v13, v6

    aput-byte v9, v13, v1

    move/from16 v28, v4

    const/4 v1, 0x1

    :goto_9
    aput-byte v12, v5, v25

    aput-byte v28, v5, v22

    const/16 v4, 0x14

    const/4 v15, 0x1

    goto :goto_a

    :cond_17
    if-eqz v6, :cond_19

    if-eqz v19, :cond_19

    if-ne v10, v3, :cond_19

    const/4 v1, 0x0

    .line 875
    aget-byte v4, v11, v1

    const/4 v1, 0x1

    if-ne v4, v1, :cond_18

    .line 876
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_19

    .line 879
    :cond_18
    div-int/lit16 v1, v10, 0x80

    int-to-byte v1, v1

    const/16 v4, 0x14

    aput-byte v1, v5, v4

    const/16 v1, 0x15

    .line 880
    rem-int/lit16 v10, v10, 0x80

    int-to-byte v6, v10

    aput-byte v6, v5, v1

    const/4 v1, 0x2

    aput-byte v12, v5, v1

    const/4 v6, 0x3

    aput-byte v28, v5, v6

    aput-byte v8, v13, v1

    aput-byte v9, v13, v6

    const/4 v1, 0x2

    goto :goto_a

    :cond_19
    const/16 v4, 0x14

    move v1, v6

    :goto_a
    if-eqz v15, :cond_1c

    .line 895
    aget-byte v6, v5, v25

    sub-int v6, v12, v6

    .line 896
    aget-byte v7, v5, v22

    sub-int v7, v28, v7

    :goto_b
    if-gez v7, :cond_1a

    add-int/lit8 v7, v7, 0x3c

    add-int/lit8 v6, v6, -0x1

    goto :goto_b

    :cond_1a
    :goto_c
    if-gez v6, :cond_1b

    add-int/lit8 v6, v6, 0x18

    goto :goto_c

    :cond_1b
    add-int v6, v32, v6

    add-int v8, v26, v7

    aput-byte v12, v5, v25

    aput-byte v28, v5, v22

    move/from16 v32, v6

    move/from16 v26, v8

    :cond_1c
    move v6, v1

    move v1, v12

    move/from16 v8, v26

    move/from16 v7, v32

    :goto_d
    add-int/lit8 v9, v31, 0x1

    move v4, v9

    move-object/from16 v14, v23

    move-object/from16 v10, v30

    const/4 v12, 0x2

    move/from16 v23, v1

    move/from16 v1, p1

    goto/16 :goto_1

    :cond_1d
    move/from16 v32, v7

    const/4 v1, 0x2

    if-eq v6, v1, :cond_1e

    .line 915
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v2

    if-eq v2, v1, :cond_1e

    const/4 v1, 0x0

    return-object v1

    :cond_1e
    const/4 v2, 0x0

    .line 918
    aget-byte v2, v13, v2

    const/4 v3, 0x7

    aput-byte v2, v5, v3

    const/16 v2, 0x8

    const/4 v3, 0x1

    .line 919
    aget-byte v3, v13, v3

    aput-byte v3, v5, v2

    const/16 v2, 0x9

    .line 920
    aget-byte v1, v13, v1

    aput-byte v1, v5, v2

    const/16 v1, 0xa

    const/4 v2, 0x3

    .line 921
    aget-byte v3, v13, v2

    aput-byte v3, v5, v1

    move/from16 v1, v26

    move/from16 v2, v32

    const/16 v3, 0x3c

    :goto_e
    if-lt v1, v3, :cond_1f

    add-int/lit8 v1, v1, -0x3c

    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    :cond_1f
    int-to-byte v2, v2

    aput-byte v2, v5, v25

    int-to-byte v1, v1

    aput-byte v1, v5, v22

    const/16 v1, 0xb

    .line 932
    aget-byte v2, v5, v1

    const/4 v1, 0x3

    if-ne v2, v1, :cond_20

    return-object v5

    .line 936
    :cond_20
    iget-object v6, v0, Lcom/lltskb/lltskb/engine/QueryBase;->runRule:Lcom/lltskb/lltskb/engine/Rule;

    iget-object v7, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    iget-object v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    iget-object v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    iget v10, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    iget v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    invoke-virtual/range {v6 .. v11}, Lcom/lltskb/lltskb/engine/Rule;->outOfDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)B

    move-result v1

    const/16 v2, 0xb

    aput-byte v1, v5, v2

    return-object v5

    :cond_21
    :goto_f
    const/4 v1, 0x0

    return-object v1
.end method

.method getTrainTimeDTO(III)Lcom/lltskb/lltskb/engine/TrainTimeDTO;
    .locals 32

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    .line 395
    iget-object v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v4, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    const-string v1, "QueryBase"

    const-string v2, "train is null"

    .line 397
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 426
    :cond_0
    new-instance v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;

    invoke-direct {v6}, Lcom/lltskb/lltskb/engine/TrainTimeDTO;-><init>()V

    .line 429
    iput v2, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->fromStation:I

    .line 430
    iput v3, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->toStation:I

    const/16 v7, 0xc

    .line 432
    aget-byte v8, v4, v7

    mul-int/lit16 v8, v8, 0x80

    const/16 v9, 0xd

    aget-byte v9, v4, v9

    add-int/2addr v8, v9

    iput v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->beginStation:I

    .line 433
    array-length v8, v4

    const/4 v9, 0x7

    sub-int/2addr v8, v9

    aget-byte v8, v4, v8

    mul-int/lit16 v8, v8, 0x80

    array-length v10, v4

    const/4 v11, 0x6

    sub-int/2addr v10, v11

    aget-byte v10, v4, v10

    add-int/2addr v8, v10

    iput v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endStation:I

    const/4 v8, 0x0

    .line 435
    aget-byte v10, v4, v8

    iput v10, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->type:I

    const/4 v10, 0x1

    .line 436
    aget-byte v12, v4, v10

    and-int/lit16 v12, v12, 0xff

    mul-int/lit16 v12, v12, 0xff

    const/4 v13, 0x2

    aget-byte v14, v4, v13

    and-int/lit16 v14, v14, 0xff

    add-int/2addr v12, v14

    iput v12, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->priceNo:I

    const/4 v12, 0x3

    .line 439
    aget-byte v14, v4, v12

    const/4 v15, 0x5

    const/4 v7, 0x4

    .line 440
    aget-byte v5, v4, v7

    .line 441
    aget-byte v15, v4, v15

    .line 442
    aget-byte v11, v4, v11

    mul-int/lit16 v5, v5, 0x80

    add-int/2addr v14, v5

    const/16 v5, 0x4000

    mul-int/lit16 v15, v15, 0x4000

    add-int/2addr v14, v15

    const/high16 v15, 0x200000

    mul-int v11, v11, v15

    add-int/2addr v14, v11

    .line 444
    iput v14, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDate:I

    const/16 v11, 0x8

    .line 446
    aget-byte v9, v4, v9

    const/16 v14, 0x9

    .line 447
    aget-byte v11, v4, v11

    const/16 v15, 0xa

    .line 448
    aget-byte v14, v4, v14

    const/16 v17, 0xb

    .line 449
    aget-byte v15, v4, v15

    mul-int/lit16 v11, v11, 0x80

    add-int/2addr v9, v11

    mul-int/lit16 v14, v14, 0x4000

    add-int/2addr v9, v14

    const/high16 v11, 0x200000

    mul-int v15, v15, v11

    add-int/2addr v9, v15

    .line 450
    iput v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endDate:I

    .line 451
    aget-byte v9, v4, v17

    iput v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    .line 453
    iget v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDate:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    .line 454
    iget v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endDate:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    .line 455
    iput v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    new-array v7, v7, [B

    .line 463
    new-instance v9, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {v9}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    .line 464
    invoke-virtual {v9, v1}, Lcom/lltskb/lltskb/engine/Schedule;->hasScheduleData(I)Z

    move-result v11

    const/16 v14, 0x14

    new-array v14, v14, [B

    .line 470
    new-instance v15, Ljava/util/Vector;

    invoke-direct {v15}, Ljava/util/Vector;-><init>()V

    .line 472
    array-length v5, v4

    const/16 v10, 0xc

    const/16 v16, 0x0

    const/16 v18, 0x0

    :goto_0
    if-ge v10, v5, :cond_3

    .line 475
    new-instance v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    invoke-direct {v12, v0}, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;-><init>(Lcom/lltskb/lltskb/engine/QueryBase;)V

    add-int/lit8 v19, v10, 0x1

    .line 476
    aget-byte v10, v4, v10

    and-int/lit16 v10, v10, 0xff

    mul-int/lit16 v10, v10, 0x80

    add-int/lit8 v20, v19, 0x1

    aget-byte v8, v4, v19

    and-int/lit16 v8, v8, 0xff

    add-int/2addr v10, v8

    iput v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    add-int/lit8 v8, v20, 0x1

    .line 477
    aget-byte v10, v4, v20

    iput-byte v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    add-int/lit8 v10, v8, 0x1

    .line 478
    aget-byte v8, v4, v8

    iput-byte v8, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    add-int/lit8 v8, v10, 0x1

    .line 479
    aget-byte v10, v4, v10

    iput-byte v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    add-int/lit8 v10, v8, 0x1

    .line 480
    aget-byte v8, v4, v8

    iput-byte v8, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    add-int/lit8 v8, v10, 0x1

    .line 481
    aget-byte v10, v4, v10

    iput-byte v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    .line 482
    iget v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v10, v2, :cond_1

    const/16 v16, 0x1

    .line 485
    :cond_1
    iget v10, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v10, v3, :cond_2

    const/16 v18, 0x1

    .line 489
    :cond_2
    invoke-virtual {v15, v12}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move v10, v8

    const/4 v8, 0x0

    const/4 v12, 0x3

    goto :goto_0

    :cond_3
    if-eqz v16, :cond_21

    if-nez v18, :cond_4

    goto/16 :goto_e

    :cond_4
    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0x0

    const/16 v23, -0x1

    .line 507
    :goto_1
    invoke-virtual {v15}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v25, v12

    if-ge v5, v4, :cond_1d

    if-eq v8, v13, :cond_1d

    .line 508
    invoke-virtual {v15, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    .line 509
    iget v12, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    .line 510
    iget-byte v13, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    move/from16 v26, v12

    .line 511
    iget-byte v12, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    move/from16 v27, v12

    .line 512
    iget-byte v12, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    move/from16 v28, v12

    .line 513
    iget-byte v12, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    .line 514
    iget-byte v4, v4, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    const/16 v21, 0x0

    const/16 v24, -0x1

    aput-byte v24, v14, v21

    if-eqz v11, :cond_c

    move/from16 v29, v11

    .line 519
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v11

    move-object/from16 v30, v15

    const/4 v15, 0x2

    if-ne v11, v15, :cond_5

    const/16 v11, 0x4000

    goto :goto_2

    :cond_5
    move/from16 v11, v26

    :goto_2
    move/from16 v31, v5

    if-nez v19, :cond_6

    .line 523
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v5

    if-ne v5, v15, :cond_8

    :cond_6
    if-nez v20, :cond_8

    .line 525
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    iget v15, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    neg-int v15, v15

    invoke-static {v5, v15}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v9, v1, v5}, Lcom/lltskb/lltskb/engine/Schedule;->isCancelTrain(II)Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x3

    .line 526
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    :cond_7
    const/16 v20, 0x1

    .line 531
    :cond_8
    iget-object v5, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    move/from16 v15, v22

    invoke-static {v5, v15}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v9, v1, v11, v5, v14}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    const/4 v5, 0x0

    .line 533
    aget-byte v1, v14, v5

    const/4 v5, 0x1

    if-ne v1, v5, :cond_b

    move/from16 v1, v23

    if-le v1, v13, :cond_9

    if-nez v19, :cond_9

    .line 535
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v4

    const/4 v11, 0x2

    if-eq v4, v11, :cond_9

    iget v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    add-int/2addr v4, v5

    iput v4, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    :cond_9
    if-le v1, v13, :cond_a

    if-eqz v19, :cond_a

    add-int/lit8 v22, v15, 0x1

    move/from16 v23, v1

    move/from16 v15, v22

    move/from16 v12, v25

    move-object/from16 v22, v9

    goto/16 :goto_c

    :cond_a
    move/from16 v23, v1

    move-object/from16 v22, v9

    move/from16 v12, v25

    goto/16 :goto_c

    :cond_b
    move/from16 v1, v23

    const/4 v5, 0x0

    .line 541
    aget-byte v22, v14, v5

    if-nez v22, :cond_d

    const/16 v5, 0x9

    .line 542
    aget-byte v13, v14, v5

    const/16 v5, 0xa

    .line 543
    aget-byte v5, v14, v5

    const/16 v22, 0xb

    .line 544
    aget-byte v22, v14, v22

    move/from16 v28, v22

    goto :goto_3

    :cond_c
    move/from16 v31, v5

    move/from16 v29, v11

    move-object/from16 v30, v15

    move/from16 v15, v22

    move/from16 v1, v23

    move/from16 v11, v26

    :cond_d
    move/from16 v5, v27

    :goto_3
    if-le v1, v13, :cond_e

    if-nez v19, :cond_e

    move-object/from16 v22, v9

    .line 551
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v9

    move/from16 v23, v10

    const/4 v10, 0x2

    if-eq v9, v10, :cond_f

    iget v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    const/4 v10, 0x1

    add-int/2addr v9, v10

    iput v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    goto :goto_4

    :cond_e
    move-object/from16 v22, v9

    move/from16 v23, v10

    :cond_f
    :goto_4
    if-le v1, v13, :cond_10

    if-eqz v19, :cond_10

    add-int/lit8 v1, v15, 0x1

    move v15, v1

    :cond_10
    const/4 v1, 0x1

    if-eq v8, v1, :cond_17

    if-eqz v16, :cond_17

    if-ne v2, v11, :cond_17

    const/4 v9, 0x0

    .line 559
    aget-byte v10, v14, v9

    if-ne v10, v1, :cond_11

    .line 560
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v9, 0x2

    if-ne v1, v9, :cond_17

    .line 562
    :cond_11
    iput v11, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->fromStation:I

    const/16 v1, 0x18

    if-ne v2, v3, :cond_14

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_14

    .line 564
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startHour:I

    .line 565
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startMin:I

    add-int v5, v5, v28

    int-to-byte v5, v5

    :cond_12
    :goto_5
    const/16 v8, 0x3c

    if-lt v5, v8, :cond_13

    add-int/lit8 v13, v13, 0x1

    int-to-byte v13, v13

    add-int/lit8 v5, v5, -0x3c

    int-to-byte v5, v5

    if-lt v13, v1, :cond_12

    add-int/lit8 v13, v13, -0x18

    int-to-byte v13, v13

    goto :goto_5

    .line 574
    :cond_13
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endHour:I

    .line 575
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endMin:I

    const/4 v1, 0x0

    const/4 v8, 0x1

    aput-byte v1, v7, v8

    aput-byte v1, v7, v1

    const/4 v1, 0x2

    aput-byte v12, v7, v1

    const/4 v1, 0x3

    aput-byte v4, v7, v1

    const/4 v1, 0x2

    goto :goto_7

    :cond_14
    add-int v5, v5, v28

    int-to-byte v5, v5

    :cond_15
    :goto_6
    const/16 v8, 0x3c

    if-lt v5, v8, :cond_16

    add-int/lit8 v13, v13, 0x1

    int-to-byte v13, v13

    add-int/lit8 v5, v5, -0x3c

    int-to-byte v5, v5

    if-lt v13, v1, :cond_15

    add-int/lit8 v13, v13, -0x18

    int-to-byte v13, v13

    goto :goto_6

    .line 588
    :cond_16
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startHour:I

    .line 589
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startMin:I

    const/4 v1, 0x0

    aput-byte v12, v7, v1

    const/4 v1, 0x1

    aput-byte v4, v7, v1

    const/4 v1, 0x1

    .line 596
    :goto_7
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    .line 597
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    const/16 v19, 0x1

    goto :goto_9

    :cond_17
    if-eqz v8, :cond_19

    if-eqz v18, :cond_19

    if-ne v11, v3, :cond_19

    const/4 v1, 0x0

    .line 599
    aget-byte v9, v14, v1

    const/4 v1, 0x1

    if-ne v9, v1, :cond_18

    .line 600
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v9, 0x2

    if-ne v1, v9, :cond_19

    goto :goto_8

    :cond_18
    const/4 v9, 0x2

    .line 603
    :goto_8
    iput v11, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->toStation:I

    .line 605
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endHour:I

    .line 606
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endMin:I

    aput-byte v12, v7, v9

    const/4 v1, 0x3

    aput-byte v4, v7, v1

    const/4 v1, 0x2

    goto :goto_9

    :cond_19
    move v1, v8

    :goto_9
    if-eqz v19, :cond_1c

    .line 618
    iget v4, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    sub-int v4, v13, v4

    .line 619
    iget v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    sub-int v8, v5, v8

    :goto_a
    if-gez v8, :cond_1a

    add-int/lit8 v8, v8, 0x3c

    add-int/lit8 v4, v4, -0x1

    goto :goto_a

    :cond_1a
    :goto_b
    if-gez v4, :cond_1b

    add-int/lit8 v4, v4, 0x18

    goto :goto_b

    :cond_1b
    add-int v10, v23, v4

    add-int v12, v25, v8

    .line 631
    iput v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    .line 632
    iput v5, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    move/from16 v23, v10

    move/from16 v25, v12

    :cond_1c
    move v8, v1

    move/from16 v10, v23

    move/from16 v12, v25

    move/from16 v23, v13

    :goto_c
    add-int/lit8 v5, v31, 0x1

    move/from16 v1, p1

    move-object/from16 v9, v22

    move/from16 v11, v29

    const/4 v13, 0x2

    move/from16 v22, v15

    move-object/from16 v15, v30

    goto/16 :goto_1

    :cond_1d
    move/from16 v23, v10

    const/4 v1, 0x2

    if-eq v8, v1, :cond_1e

    .line 638
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v2

    if-eq v2, v1, :cond_1e

    const/4 v2, 0x0

    return-object v2

    :cond_1e
    const/4 v2, 0x0

    .line 640
    aget-byte v2, v7, v2

    mul-int/lit16 v2, v2, 0x80

    const/4 v3, 0x1

    aget-byte v3, v7, v3

    add-int/2addr v2, v3

    iput v2, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDist:I

    .line 641
    aget-byte v1, v7, v1

    mul-int/lit16 v1, v1, 0x80

    const/4 v2, 0x3

    aget-byte v3, v7, v2

    add-int/2addr v1, v3

    iput v1, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endDist:I

    move/from16 v2, v23

    move/from16 v1, v25

    const/16 v3, 0x3c

    :goto_d
    if-lt v1, v3, :cond_1f

    add-int/lit8 v1, v1, -0x3c

    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 648
    :cond_1f
    iput v2, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    .line 649
    iput v1, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    .line 651
    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->init()V

    .line 653
    iget v1, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_20

    return-object v6

    .line 657
    :cond_20
    iget-object v7, v0, Lcom/lltskb/lltskb/engine/QueryBase;->runRule:Lcom/lltskb/lltskb/engine/Rule;

    iget-object v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    iget-object v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    iget-object v10, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    iget v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    iget v12, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    invoke-virtual/range {v7 .. v12}, Lcom/lltskb/lltskb/engine/Rule;->outOfDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)B

    move-result v1

    .line 661
    iput v1, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    return-object v6

    :cond_21
    :goto_e
    const/4 v1, 0x0

    return-object v1
.end method

.method public getTrainTimeNew(I[BIIZ)[B
    .locals 34

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    if-nez v2, :cond_0

    const-string v1, "QueryBase"

    const-string v2, "train is null"

    .line 141
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    return-object v1

    :cond_0
    const/16 v6, 0x16

    new-array v6, v6, [B

    const/16 v7, 0x12

    .line 171
    div-int/lit16 v8, v3, 0x80

    int-to-byte v8, v8

    aput-byte v8, v6, v7

    const/16 v7, 0x13

    .line 172
    rem-int/lit16 v8, v3, 0x80

    int-to-byte v8, v8

    aput-byte v8, v6, v7

    .line 174
    div-int/lit16 v7, v4, 0x80

    int-to-byte v7, v7

    const/16 v8, 0x14

    aput-byte v7, v6, v8

    const/16 v7, 0x15

    .line 175
    rem-int/lit16 v9, v4, 0x80

    int-to-byte v9, v9

    aput-byte v9, v6, v7

    const/16 v7, 0xc

    .line 177
    aget-byte v9, v2, v7

    aput-byte v9, v6, v7

    const/16 v9, 0xd

    const/16 v10, 0xd

    .line 178
    aget-byte v10, v2, v10

    aput-byte v10, v6, v9

    const/16 v9, 0xe

    .line 179
    array-length v10, v2

    const/4 v11, 0x7

    sub-int/2addr v10, v11

    aget-byte v10, v2, v10

    aput-byte v10, v6, v9

    const/16 v9, 0xf

    .line 180
    array-length v10, v2

    const/4 v12, 0x6

    sub-int/2addr v10, v12

    aget-byte v10, v2, v10

    aput-byte v10, v6, v9

    .line 182
    array-length v9, v2

    const/4 v10, 0x0

    .line 183
    aget-byte v13, v2, v10

    const/4 v14, 0x4

    aput-byte v13, v6, v14

    const/4 v13, 0x5

    const/4 v15, 0x1

    .line 184
    aget-byte v16, v2, v15

    aput-byte v16, v6, v13

    const/4 v13, 0x2

    .line 185
    aget-byte v16, v2, v13

    aput-byte v16, v6, v12

    const/4 v7, 0x3

    .line 187
    aget-byte v17, v2, v7

    const/16 v18, 0x5

    .line 188
    aget-byte v7, v2, v14

    .line 189
    aget-byte v15, v2, v18

    .line 190
    aget-byte v12, v2, v12

    mul-int/lit16 v7, v7, 0x80

    add-int v17, v17, v7

    const/16 v7, 0x4000

    mul-int/lit16 v15, v15, 0x4000

    add-int v17, v17, v15

    const/high16 v15, 0x200000

    mul-int v12, v12, v15

    add-int v17, v17, v12

    const/16 v12, 0x8

    .line 194
    aget-byte v15, v2, v11

    const/16 v18, 0x9

    .line 195
    aget-byte v12, v2, v12

    const/16 v19, 0xa

    .line 196
    aget-byte v11, v2, v18

    const/16 v18, 0xb

    .line 197
    aget-byte v19, v2, v19

    mul-int/lit16 v12, v12, 0x80

    add-int/2addr v15, v12

    mul-int/lit16 v11, v11, 0x4000

    add-int/2addr v15, v11

    const/high16 v11, 0x200000

    mul-int v19, v19, v11

    add-int v15, v15, v19

    .line 199
    aget-byte v11, v2, v18

    iput v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    .line 201
    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    .line 202
    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    .line 203
    iput v10, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    new-array v12, v14, [B

    .line 210
    new-instance v14, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {v14}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    .line 211
    invoke-virtual {v14, v1}, Lcom/lltskb/lltskb/engine/Schedule;->hasScheduleData(I)Z

    move-result v15

    new-array v7, v8, [B

    .line 217
    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    const/16 v8, 0xc

    const/16 v16, 0x0

    const/16 v20, 0x0

    :goto_0
    if-ge v8, v9, :cond_3

    .line 221
    new-instance v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    invoke-direct {v10, v0}, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;-><init>(Lcom/lltskb/lltskb/engine/QueryBase;)V

    add-int/lit8 v22, v8, 0x1

    .line 222
    aget-byte v8, v2, v8

    and-int/lit16 v8, v8, 0xff

    mul-int/lit16 v8, v8, 0x80

    add-int/lit8 v23, v22, 0x1

    aget-byte v13, v2, v22

    and-int/lit16 v13, v13, 0xff

    add-int/2addr v8, v13

    iput v8, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    add-int/lit8 v8, v23, 0x1

    .line 223
    aget-byte v13, v2, v23

    iput-byte v13, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    add-int/lit8 v13, v8, 0x1

    .line 224
    aget-byte v8, v2, v8

    iput-byte v8, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    add-int/lit8 v8, v13, 0x1

    .line 225
    aget-byte v13, v2, v13

    iput-byte v13, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    add-int/lit8 v13, v8, 0x1

    .line 226
    aget-byte v8, v2, v8

    iput-byte v8, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    add-int/lit8 v8, v13, 0x1

    .line 227
    aget-byte v13, v2, v13

    iput-byte v13, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    .line 228
    iget v13, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v13, v3, :cond_1

    const/16 v16, 0x1

    .line 231
    :cond_1
    iget v13, v10, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    if-ne v13, v4, :cond_2

    const/16 v20, 0x1

    .line 235
    :cond_2
    invoke-virtual {v11, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    const/4 v13, 0x2

    goto :goto_0

    :cond_3
    move-object/from16 v23, v12

    const/16 p2, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/16 v18, 0x0

    const/16 v22, 0x0

    .line 238
    :goto_1
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v12

    const/16 v24, 0x11

    const/16 v25, 0x10

    if-ge v2, v12, :cond_1c

    const/4 v12, 0x2

    if-eq v13, v12, :cond_1c

    .line 239
    invoke-virtual {v11, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;

    .line 240
    iget v4, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stationIdx:I

    move/from16 v26, v4

    .line 241
    iget-byte v4, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveH:B

    move/from16 v27, v4

    .line 242
    iget-byte v4, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->arriveM:B

    move/from16 v28, v4

    .line 243
    iget-byte v4, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->stay:B

    move/from16 v29, v4

    .line 244
    iget-byte v4, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist1:B

    .line 245
    iget-byte v12, v12, Lcom/lltskb/lltskb/engine/QueryBase$StationNode;->dist2:B

    const/16 v30, -0x1

    const/16 v21, 0x0

    aput-byte v30, v7, v21

    if-eqz v15, :cond_8

    move-object/from16 v30, v11

    .line 250
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v11

    move/from16 v31, v15

    const/4 v15, 0x2

    if-ne v11, v15, :cond_4

    const/16 v11, 0x4000

    goto :goto_2

    :cond_4
    move/from16 v11, v26

    :goto_2
    if-eqz v10, :cond_6

    if-nez v18, :cond_6

    .line 256
    iget-object v15, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    move/from16 v32, v2

    iget v2, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    neg-int v2, v2

    invoke-static {v15, v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v14, v1, v2}, Lcom/lltskb/lltskb/engine/Schedule;->isCancelTrain(II)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v1, 0x0

    return-object v1

    :cond_5
    const/16 v18, 0x1

    goto :goto_3

    :cond_6
    move/from16 v32, v2

    .line 262
    :goto_3
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    invoke-static {v2, v8}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v14, v1, v11, v2, v7}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    const/4 v2, 0x0

    .line 264
    aget-byte v15, v7, v2

    const/4 v2, 0x1

    if-ne v15, v2, :cond_7

    move/from16 v1, p2

    move v2, v9

    const/16 v15, 0x14

    move/from16 v9, p4

    goto/16 :goto_11

    :cond_7
    const/4 v2, 0x0

    .line 266
    aget-byte v15, v7, v2

    if-nez v15, :cond_9

    const/16 v2, 0x9

    .line 267
    aget-byte v2, v7, v2

    const/16 v15, 0xa

    .line 268
    aget-byte v15, v7, v15

    const/16 v26, 0xb

    .line 269
    aget-byte v26, v7, v26

    move/from16 v28, v15

    move/from16 v29, v26

    goto :goto_4

    :cond_8
    move/from16 v32, v2

    move-object/from16 v30, v11

    move/from16 v31, v15

    move/from16 v11, v26

    :cond_9
    move/from16 v2, v27

    :goto_4
    if-le v9, v2, :cond_a

    if-nez v10, :cond_a

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v15

    const/4 v1, 0x2

    if-eq v15, v1, :cond_a

    iget v1, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    const/4 v15, 0x1

    add-int/2addr v1, v15

    iput v1, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    :cond_a
    if-le v9, v2, :cond_b

    if-eqz v10, :cond_b

    add-int/lit8 v8, v8, 0x1

    :cond_b
    const/4 v1, 0x1

    if-eq v13, v1, :cond_15

    if-eqz v16, :cond_d

    if-eq v3, v11, :cond_c

    goto :goto_6

    :cond_c
    :goto_5
    const/4 v1, 0x0

    goto :goto_7

    :cond_d
    :goto_6
    if-nez v16, :cond_15

    if-nez v13, :cond_15

    .line 284
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/QueryBase;->bstart:[B

    .line 285
    invoke-direct {v0, v11, v3, v1, v5}, Lcom/lltskb/lltskb/engine/QueryBase;->isFuzzyEqualStation(II[BZ)Z

    move-result v1

    if-eqz v1, :cond_15

    goto :goto_5

    :goto_7
    aget-byte v9, v7, v1

    const/4 v1, 0x1

    if-ne v9, v1, :cond_e

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v9, 0x2

    if-ne v1, v9, :cond_15

    :cond_e
    const/16 v1, 0x12

    .line 288
    div-int/lit16 v9, v11, 0x80

    int-to-byte v9, v9

    aput-byte v9, v6, v1

    const/16 v1, 0x13

    .line 289
    rem-int/lit16 v11, v11, 0x80

    int-to-byte v9, v11

    aput-byte v9, v6, v1

    const/16 v1, 0x18

    move/from16 v9, p4

    const/16 v10, 0x3c

    if-ne v3, v9, :cond_11

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v11

    const/4 v13, 0x3

    if-ne v11, v13, :cond_11

    const/4 v11, 0x0

    aput-byte v2, v6, v11

    const/4 v11, 0x1

    aput-byte v28, v6, v11

    add-int v11, v28, v29

    int-to-byte v11, v11

    :cond_f
    :goto_8
    if-lt v11, v10, :cond_10

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    add-int/lit8 v11, v11, -0x3c

    int-to-byte v11, v11

    if-lt v2, v1, :cond_f

    add-int/lit8 v2, v2, -0x18

    int-to-byte v2, v2

    goto :goto_8

    :cond_10
    const/4 v13, 0x2

    aput-byte v2, v6, v13

    const/4 v1, 0x3

    aput-byte v11, v6, v1

    const/4 v10, 0x0

    const/4 v15, 0x1

    aput-byte v10, v23, v15

    aput-byte v10, v23, v10

    aput-byte v4, v23, v13

    aput-byte v12, v23, v1

    move/from16 v28, v11

    const/4 v13, 0x2

    goto :goto_a

    :cond_11
    add-int v11, v28, v29

    int-to-byte v11, v11

    :cond_12
    :goto_9
    if-lt v11, v10, :cond_13

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    add-int/lit8 v11, v11, -0x3c

    int-to-byte v11, v11

    if-lt v2, v1, :cond_12

    add-int/lit8 v2, v2, -0x18

    int-to-byte v2, v2

    goto :goto_9

    :cond_13
    const/4 v13, 0x0

    aput-byte v2, v6, v13

    const/4 v1, 0x1

    aput-byte v11, v6, v1

    aput-byte v4, v23, v13

    aput-byte v12, v23, v1

    move/from16 v28, v11

    const/4 v13, 0x1

    :goto_a
    aput-byte v2, v6, v25

    aput-byte v28, v6, v24

    const/4 v10, 0x1

    :cond_14
    const/16 v15, 0x14

    goto :goto_e

    :cond_15
    move/from16 v9, p4

    if-eqz v13, :cond_14

    if-eqz v20, :cond_17

    if-eq v11, v9, :cond_16

    goto :goto_c

    :cond_16
    :goto_b
    const/4 v1, 0x0

    goto :goto_d

    :cond_17
    :goto_c
    if-nez v20, :cond_14

    .line 327
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/QueryBase;->bend:[B

    invoke-direct {v0, v11, v9, v1, v5}, Lcom/lltskb/lltskb/engine/QueryBase;->isFuzzyEqualStation(II[BZ)Z

    move-result v1

    if-eqz v1, :cond_14

    goto :goto_b

    :goto_d
    aget-byte v15, v7, v1

    const/4 v1, 0x1

    if-ne v15, v1, :cond_18

    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v1

    const/4 v15, 0x2

    if-ne v1, v15, :cond_14

    .line 331
    :cond_18
    div-int/lit16 v1, v11, 0x80

    int-to-byte v1, v1

    const/16 v15, 0x14

    aput-byte v1, v6, v15

    const/16 v1, 0x15

    .line 332
    rem-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v1

    const/4 v1, 0x2

    aput-byte v2, v6, v1

    const/4 v11, 0x3

    aput-byte v28, v6, v11

    aput-byte v4, v23, v1

    aput-byte v12, v23, v11

    const/4 v13, 0x2

    :goto_e
    if-eqz v10, :cond_1b

    .line 347
    aget-byte v1, v6, v25

    sub-int v1, v2, v1

    .line 348
    aget-byte v4, v6, v24

    sub-int v4, v28, v4

    :goto_f
    if-gez v4, :cond_19

    add-int/lit8 v4, v4, 0x3c

    add-int/lit8 v1, v1, -0x1

    goto :goto_f

    :cond_19
    :goto_10
    if-gez v1, :cond_1a

    add-int/lit8 v1, v1, 0x18

    goto :goto_10

    :cond_1a
    add-int v22, v22, v1

    add-int v1, p2, v4

    aput-byte v2, v6, v25

    aput-byte v28, v6, v24

    goto :goto_11

    :cond_1b
    move/from16 v1, p2

    :goto_11
    add-int/lit8 v4, v32, 0x1

    move/from16 p2, v1

    move-object/from16 v11, v30

    move/from16 v15, v31

    move/from16 v1, p1

    move/from16 v33, v9

    move v9, v2

    move v2, v4

    move/from16 v4, v33

    goto/16 :goto_1

    :cond_1c
    const/16 v10, 0x3c

    const/4 v1, 0x2

    if-eq v13, v1, :cond_1d

    .line 367
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/QueryBase;->getQueryType()I

    move-result v2

    if-eq v2, v1, :cond_1d

    const/4 v1, 0x0

    return-object v1

    :cond_1d
    const/4 v2, 0x0

    .line 370
    aget-byte v2, v23, v2

    const/4 v3, 0x7

    aput-byte v2, v6, v3

    const/16 v2, 0x8

    const/4 v3, 0x1

    .line 371
    aget-byte v3, v23, v3

    aput-byte v3, v6, v2

    const/16 v2, 0x9

    .line 372
    aget-byte v1, v23, v1

    aput-byte v1, v6, v2

    const/16 v1, 0xa

    const/4 v2, 0x3

    .line 373
    aget-byte v2, v23, v2

    aput-byte v2, v6, v1

    move/from16 v1, p2

    move/from16 v2, v22

    :goto_12
    if-lt v1, v10, :cond_1e

    add-int/lit8 v1, v1, -0x3c

    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    :cond_1e
    int-to-byte v2, v2

    aput-byte v2, v6, v25

    int-to-byte v1, v1

    aput-byte v1, v6, v24

    .line 384
    iget-object v7, v0, Lcom/lltskb/lltskb/engine/QueryBase;->runRule:Lcom/lltskb/lltskb/engine/Rule;

    iget-object v8, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMinDate:Ljava/lang/String;

    iget-object v9, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mMaxDate:Ljava/lang/String;

    iget-object v10, v0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    iget v11, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mRuleIndex:I

    iget v12, v0, Lcom/lltskb/lltskb/engine/QueryBase;->mAuxDay:I

    invoke-virtual/range {v7 .. v12}, Lcom/lltskb/lltskb/engine/Rule;->outOfDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)B

    move-result v1

    const/16 v2, 0xb

    aput-byte v1, v6, v2

    return-object v6
.end method

.method get_exact_train_name([BI[Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_1

    if-eqz p3, :cond_0

    .line 1085
    array-length p1, p3

    if-lez p1, :cond_0

    .line 1086
    aget-object p1, p3, v1

    return-object p1

    :cond_0
    return-object v0

    :cond_1
    if-nez p3, :cond_2

    return-object v0

    .line 1093
    :cond_2
    array-length v2, p3

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    aget-object p1, p3, v1

    return-object p1

    :cond_3
    const/4 v2, 0x3

    const/4 v3, 0x2

    .line 1099
    aget-byte v4, p1, v3

    if-lez v4, :cond_7

    const/4 v2, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    :goto_0
    if-ge v2, v4, :cond_5

    .line 1103
    aget-byte v7, p1, v5

    and-int/lit16 v7, v7, 0xff

    mul-int/lit16 v7, v7, 0x80

    add-int/lit8 v8, v5, 0x1

    aget-byte v8, p1, v8

    and-int/lit16 v8, v8, 0xff

    add-int/2addr v7, v8

    add-int/2addr v5, v3

    if-le v7, p2, :cond_4

    goto :goto_1

    :cond_4
    add-int/lit8 v6, v5, 0x1

    .line 1109
    aget-byte v5, p1, v5

    add-int/lit8 v2, v2, 0x1

    move v9, v6

    move v6, v5

    move v5, v9

    goto :goto_0

    :cond_5
    :goto_1
    if-gez v6, :cond_6

    .line 1114
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "get_exact_train_name name_pos <0 "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p2, p3, v1

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "QueryBase"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    :cond_6
    array-length p1, p3

    if-ge v6, p1, :cond_7

    if-ltz v6, :cond_7

    .line 1117
    aget-object p1, p3, v6

    return-object p1

    .line 1120
    :cond_7
    array-length p1, p3

    if-lez p1, :cond_8

    aget-object p1, p3, v1

    return-object p1

    :cond_8
    return-object v0
.end method

.method isMatchFilter(Lcom/lltskb/lltskb/engine/ResultItem;)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 97
    :cond_0
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 98
    iget v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mFilter:I

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->isMatchFilter(ILjava/lang/String;)Z

    move-result p1

    return p1
.end method

.method isWuhan(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "\u6b66\u6c49"

    .line 1034
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "\u6c49\u53e3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "\u6b66\u660c"

    .line 1035
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0

    .line 1004
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->strDate:Ljava/lang/String;

    return-void
.end method

.method public setFilter(I)V
    .locals 0

    .line 92
    iput p1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->mFilter:I

    return-void
.end method

.method updateStartEnd(II)V
    .locals 8

    .line 966
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    .line 967
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v1

    .line 968
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bstart:[B

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bend:[B

    const/4 v4, 0x0

    aput-byte v4, v3, v4

    aput-byte v4, v2, v4

    const/4 v2, 0x1

    const/4 v3, 0x1

    :goto_0
    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    .line 970
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bstart:[B

    const/16 v5, 0x14

    aput-byte v5, v4, v3

    .line 971
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bend:[B

    aput-byte v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 973
    :cond_0
    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->fuzzy:Z

    if-nez v3, :cond_1

    return-void

    :cond_1
    const/4 v3, 0x1

    const/4 v5, 0x1

    :goto_1
    add-int v6, p1, v3

    .line 977
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationCount()I

    move-result v7

    if-ge v6, v7, :cond_4

    if-ge v3, v4, :cond_4

    .line 978
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v7, v6}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    goto :goto_1

    .line 983
    :cond_3
    invoke-direct {p0, v6, v0}, Lcom/lltskb/lltskb/engine/QueryBase;->isEqualInFuzzy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 984
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bstart:[B

    add-int/lit8 v7, v5, 0x1

    aput-byte v3, v6, v5

    move v5, v7

    goto :goto_2

    :cond_4
    const/4 p1, 0x1

    :goto_3
    add-int v0, p2, v2

    .line 990
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationCount()I

    move-result v3

    if-ge v0, v3, :cond_7

    if-ge v2, v4, :cond_7

    .line 991
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryBase;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v3, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    :goto_4
    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    goto :goto_3

    .line 996
    :cond_6
    invoke-direct {p0, v0, v1}, Lcom/lltskb/lltskb/engine/QueryBase;->isEqualInFuzzy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 997
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryBase;->bend:[B

    add-int/lit8 v3, p1, 0x1

    aput-byte v2, v0, p1

    move p1, v3

    goto :goto_4

    :cond_7
    return-void
.end method
