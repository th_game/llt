.class public Lcom/lltskb/lltskb/engine/Consts;
.super Ljava/lang/Object;
.source "Consts.java"


# static fields
.field public static final BACK_COLOR1:I = -0x231108

.field public static final BACK_COLOR2:I = -0xd0803

.field public static final CANCEL_ORDER_MANY:I = 0xd

.field public static final COLOR_DIANLAN:I = -0xf9ad87

.field public static final COLOR_HEADER:I = -0xe7652a

.field public static final COLOR_HUI:I = -0x7f7f80

.field public static final COLOR_LVSHEN:I = -0xf376e8

.field public static final COLOR_ZHUHONG:I = -0xb400

.field public static final ERR_CANCELLED:I = -0x4

.field public static final ERR_EXIST_NO_COMPLETE_ORDER:I = -0x6

.field public static final ERR_FAILED:I = -0x3

.field public static final ERR_INVALID_DATE:I = -0x7

.field public static final ERR_INVALID_PASSCODE:I = -0x5

.field public static final ERR_NOT_SIGNIN:I = -0x8

.field public static final ERR_SUCCESS:I = 0x0

.field public static final ERR_UNKNOWN:I = -0x1

.field public static final ERR_WANT_RETRY:I = -0x2

.field public static final FILTER_ALL:I = 0xff

.field public static final FILTER_DC:I = 0x1

.field public static final FILTER_G:I = 0x80

.field public static final FILTER_K:I = 0x8

.field public static final FILTER_PK:I = 0x10

.field public static final FILTER_PKE:I = 0x20

.field public static final FILTER_T:I = 0x4

.field public static final FILTER_Z:I = 0x2

.field public static final GR_SEAT:Ljava/lang/String; = "6"

.field public static final GUARD:I = 0x80

.field public static final HAVA_NO_DETAIL_ORDER:I = 0x4

.field public static final HAVE_TICKET:I = 0x2

.field public static final INPUT_ORDERCODE:I = 0xc

.field public static final MAX_RETRY_COUNT:I = 0x5

.field public static final NET_ERROR:I = 0x0

.field public static final NOT_HAVE_PERSON:I = 0xe

.field public static final NO_ALLOW_ORDER:I = 0xa

.field public static final NO_ORDER:I = 0x10

.field public static final NO_TRAINTIME_FOUND:I = 0x14

.field public static final NO_TRAINTYPE_FOUND:I = 0x13

.field public static final NO_TRAIN_FOUND:I = 0x12

.field public static final NO_TRAIN_SEAT:I = 0xf

.field public static final ORDER_CODE_ERROR:I = 0x8

.field public static final ORDER_NUM_ERROR:I = 0x7

.field public static final ORDER_PERSON_ERROR:I = 0x6

.field public static final ORDER_SEAT_ERROR:I = 0x5

.field public static final ORDER_SUCCESS:I = 0xb

.field public static final PASSENGER_INDEX:Ljava/lang/String; = "passenger_index"

.field public static PAY_HTML:Ljava/lang/String; = null

.field public static final PURPOSE_CODE_ADULT:Ljava/lang/String; = "ADULT"

.field public static final PURPOSE_CODE_STUDENT:Ljava/lang/String; = "0X00"

.field public static final QUERY_TYPE_CC:I = 0x2

.field public static final QUERY_TYPE_CZ:I = 0x3

.field public static final QUERY_TYPE_ZZ:I = 0x1

.field public static RESULT_SORT_TYPE:I = 0x1

.field public static final RW_SEAT:Ljava/lang/String; = "4"

.field public static final RZ_SEAT:Ljava/lang/String; = "2"

.field public static final SAFE_SLEEP_TIME:I = 0x64

.field public static final SEAT_NAME_GR:Ljava/lang/String; = "\u9ad8\u7ea7\u8f6f\u5367"

.field public static final SEAT_NAME_OTHER:Ljava/lang/String; = "\u5176\u4ed6"

.field public static final SEAT_NAME_RW:Ljava/lang/String; = "\u8f6f\u5367"

.field public static final SEAT_NAME_RZ:Ljava/lang/String; = "\u8f6f\u5ea7"

.field public static final SEAT_NAME_SRRB:Ljava/lang/String; = "\u52a8\u5367"

.field public static final SEAT_NAME_SWZ:Ljava/lang/String; = "\u5546\u52a1\u5ea7"

.field public static final SEAT_NAME_TZ:Ljava/lang/String; = "\u7279\u7b49\u5ea7"

.field public static final SEAT_NAME_WZ:Ljava/lang/String; = "\u65e0\u5ea7"

.field public static final SEAT_NAME_YW:Ljava/lang/String; = "\u786c\u5367"

.field public static final SEAT_NAME_YZ:Ljava/lang/String; = "\u786c\u5ea7"

.field public static final SEAT_NAME_ZE:Ljava/lang/String; = "\u4e8c\u7b49\u5ea7"

.field public static final SEAT_NAME_ZY:Ljava/lang/String; = "\u4e00\u7b49\u5ea7"

.field public static final SORT_TYPE_ARRIVETIME:I = 0x2

.field public static final SORT_TYPE_DISTANCE:I = 0x10

.field public static final SORT_TYPE_DURATION:I = 0x20

.field public static final SORT_TYPE_STARTTIME:I = 0x1

.field public static final SORT_TYPE_TICKET_COUNT:I = 0x8

.field public static final SORT_TYPE_TICKET_PRICE:I = 0x4

.field public static final SRRB_SEAT:Ljava/lang/String; = "F"

.field public static final SWZ_SEAT:Ljava/lang/String; = "9"

.field public static final SYSTEM_ERROR:I = 0x11

.field public static final SYSTEM_MAINTENANCE:I = 0x1

.field public static final TEXT_COLOR1:I = -0xe98105

.field public static final TEXT_COLOR2:I = -0x30cedb

.field public static final TICKET_IS_NOT_ENOUGH:I = 0x9

.field public static final TOUR_FLAG_DC:Ljava/lang/String; = "dc"

.field public static final TOUR_FLAG_FC:Ljava/lang/String; = "fc"

.field public static final TOUR_FLAG_GC:Ljava/lang/String; = "gc"

.field public static final TOUR_FLAG_WC:Ljava/lang/String; = "wc"

.field public static final TRAINTYPE_ALL:Ljava/lang/String; = "\u5168\u90e8"

.field public static final TRAINTYPE_DC:Ljava/lang/String; = "\u52a8\u8f66"

.field public static final TRAINTYPE_G:Ljava/lang/String; = "\u9ad8\u94c1"

.field public static final TRAINTYPE_K:Ljava/lang/String; = "\u5feb\u901f"

.field public static final TRAINTYPE_PK:Ljava/lang/String; = "\u666e\u5feb"

.field public static final TRAINTYPE_PKE:Ljava/lang/String; = "\u666e\u5ba2"

.field public static final TRAINTYPE_T:Ljava/lang/String; = "\u7279\u5feb"

.field public static final TRAINTYPE_Z:Ljava/lang/String; = "\u76f4\u5feb"

.field public static final TRAIN_NO_ERROR:I = 0x3

.field public static final TZ_SEAT:Ljava/lang/String; = "P"

.field public static final USER_STATUS_NOT_PASS:I = 0x1

.field public static final USER_STATUS_PASS:I = 0x0

.field public static final USER_STATUS_PLEASE_REPORT:I = 0x4

.field public static final USER_STATUS_PRE_PASS:I = 0x2

.field public static final USER_STATUS_WAIT_VALIDATE:I = 0x3

.field public static final XW_TYPE_DW:I = 0x40

.field public static final XW_TYPE_RW:I = 0x8

.field public static final XW_TYPE_RZ:I = 0x2

.field public static final XW_TYPE_YW:I = 0x4

.field public static final XW_TYPE_YZ:I = 0x1

.field public static final XW_TYPE_ZE:I = 0x20

.field public static final XW_TYPE_ZY:I = 0x10

.field public static final YW_SEAT:Ljava/lang/String; = "3"

.field public static final YZ_SEAT:Ljava/lang/String; = "1"

.field public static final ZE_SEAT:Ljava/lang/String; = "O"

.field public static final ZY_SEAT:Ljava/lang/String; = "M"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
