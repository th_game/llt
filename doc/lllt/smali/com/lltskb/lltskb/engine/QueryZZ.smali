.class public Lcom/lltskb/lltskb/engine/QueryZZ;
.super Lcom/lltskb/lltskb/engine/QueryBase;
.source "QueryZZ.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryZZ"


# instance fields
.field private mTrainSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/QueryBase;-><init>(ZZ)V

    .line 15
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->mTrainSet:Ljava/util/Set;

    return-void
.end method

# from, to, new Array(new ResultItem)
.method private doWork(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    # p1 = StationIndex(p1)
    .line 317
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->getStationIndex(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    .line 318
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getStationIndex(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p2

    if-nez p1, :cond_0

    const/4 p1, 0x2

    .line 321
    iput p1, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->lastErr:I

    return-void

    :cond_0
    if-nez p2, :cond_1

    const/4 p1, 0x3

    .line 325
    iput p1, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->lastErr:I

    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 330
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 331
    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 332
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    # v2 = idOfStatation(froM)
    invoke-virtual {v3, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getStation(I)[B

    move-result-object v9

    # v9 = station
    if-nez v9, :cond_2

    goto :goto_3

    :cond_2
    const/4 v10, 0x0

    .line 335
    :goto_1
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v10, v3, :cond_4

    .line 336
    invoke-virtual {p2, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 337
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v3, v5}, Lcom/lltskb/lltskb/engine/ResMgr;->getStation(I)[B

    move-result-object v7

    if-nez v7, :cond_3

    goto :goto_2

    :cond_3
    move-object v3, p0

    move v4, v2

    move-object v6, v9

    move-object v8, p3

    .line 340
    invoke-direct/range {v3 .. v8}, Lcom/lltskb/lltskb/engine/QueryZZ;->getResult(II[B[BLjava/util/List;)V

    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public static getIndex(I)I
    .locals 1

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    return v0

    :pswitch_0
    const/16 p0, 0xa

    return p0

    :pswitch_1
    const/16 p0, 0xd

    return p0

    :pswitch_2
    const/16 p0, 0xc

    return p0

    :pswitch_3
    const/16 p0, 0xb

    return p0

    :pswitch_4
    const/16 p0, 0x9

    return p0

    :pswitch_5
    const/16 p0, 0x8

    return p0

    :pswitch_6
    const/4 p0, 0x7

    return p0

    :pswitch_7
    const/4 p0, 0x6

    return p0

    :pswitch_8
    const/4 p0, 0x5

    return p0

    :pswitch_9
    const/4 p0, 0x4

    return p0

    :pswitch_a
    const/4 p0, 0x2

    return p0

    :pswitch_b
    const/4 p0, 0x3

    return p0

    :pswitch_c
    const/4 p0, 0x1

    return p0

    :pswitch_d
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getLinkStation(II[BLjava/util/Vector;Ljava/util/Vector;Z)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[B",
            "Ljava/util/Vector<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    if-ltz v1, :cond_10

    if-gez v2, :cond_0

    goto/16 :goto_8

    .line 45
    :cond_0
    iget-object v6, v0, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v6, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v6

    if-eqz v6, :cond_f

    .line 47
    array-length v7, v6

    const/16 v8, 0xb

    if-ge v7, v8, :cond_1

    goto/16 :goto_7

    .line 73
    :cond_1
    aget-byte v1, v6, v8

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lcom/lltskb/lltskb/engine/QueryZZ;->hiden:Z

    if-eqz v1, :cond_2

    return-void

    .line 76
    :cond_2
    array-length v1, v6

    const/16 v7, 0xc

    const/16 v8, 0xc

    :goto_0
    const/4 v10, 0x1

    if-ge v8, v1, :cond_4

    add-int/lit8 v11, v8, 0x1

    .line 94
    aget-byte v8, v6, v8

    mul-int/lit16 v8, v8, 0x80

    add-int/lit8 v12, v11, 0x1

    aget-byte v11, v6, v11

    add-int/2addr v8, v11

    add-int/lit8 v12, v12, 0x3

    add-int/lit8 v11, v12, 0x1

    .line 97
    aget-byte v12, v6, v12

    add-int/lit8 v13, v11, 0x1

    .line 98
    aget-byte v11, v6, v11

    .line 101
    invoke-direct {v0, v8, v2, v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->isEqualStation(II[B)Z

    move-result v8

    if-eqz v8, :cond_3

    mul-int/lit16 v12, v12, 0x80

    add-int v8, v12, v11

    move v11, v8

    const/4 v8, 0x1

    goto :goto_1

    :cond_3
    move v8, v13

    goto :goto_0

    :cond_4
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_1
    if-nez v8, :cond_5

    return-void

    :cond_5
    const/4 v8, 0x0

    :goto_2
    if-ge v7, v1, :cond_e

    add-int/lit8 v12, v7, 0x1

    .line 113
    aget-byte v7, v6, v7

    mul-int/lit16 v7, v7, 0x80

    add-int/lit8 v13, v12, 0x1

    aget-byte v12, v6, v12

    add-int/2addr v7, v12

    add-int/lit8 v13, v13, 0x3

    add-int/lit8 v12, v13, 0x1

    .line 116
    aget-byte v13, v6, v13

    add-int/lit8 v14, v12, 0x1

    .line 117
    aget-byte v12, v6, v12

    .line 119
    invoke-direct {v0, v7, v2, v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->isEqualStation(II[B)Z

    move-result v15

    if-eqz v15, :cond_7

    if-eqz p6, :cond_6

    goto/16 :goto_6

    :cond_6
    move v7, v14

    const/4 v8, 0x1

    goto :goto_2

    :cond_7
    mul-int/lit16 v13, v13, 0x80

    add-int/2addr v13, v12

    if-nez p6, :cond_8

    if-eqz v8, :cond_d

    :cond_8
    const/4 v12, 0x0

    .line 131
    :goto_3
    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v15

    if-ge v12, v15, :cond_b

    .line 132
    invoke-virtual {v4, v12}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    .line 133
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-ne v7, v15, :cond_a

    .line 134
    invoke-virtual {v5, v12}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    if-eqz p6, :cond_9

    sub-int v9, v11, v13

    .line 136
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-ge v9, v15, :cond_b

    .line 137
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9, v12}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_4

    :cond_9
    sub-int v9, v13, v11

    .line 140
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-ge v9, v15, :cond_b

    .line 141
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9, v12}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_4

    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 148
    :cond_b
    :goto_4
    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v9

    if-ne v12, v9, :cond_d

    .line 150
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    .line 151
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {v9}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 154
    invoke-virtual {v4, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    if-eqz p6, :cond_c

    sub-int v9, v11, v13

    .line 156
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_5

    :cond_c
    sub-int v9, v13, v11

    .line 158
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_5

    :cond_d
    move v7, v14

    goto/16 :goto_2

    :cond_e
    :goto_6
    return-void

    .line 48
    :cond_f
    :goto_7
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u4fe1\u606f\u4e0d\u5168\uff01\u65e0\u6cd5\u67e5\u8be2"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "QueryZZ"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    :goto_8
    return-void
.end method

.method private getResult(II[B[BLjava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[B[B",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    .line 357
    array-length v0, p3

    array-length v1, p4

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_2

    .line 360
    aget-byte v5, p3, v3

    aget-byte v6, p4, v4

    if-ne v5, v6, :cond_1

    add-int/lit8 v5, v3, 0x1

    aget-byte v6, p3, v5

    add-int/lit8 v7, v4, 0x1

    aget-byte v7, p4, v7

    if-ne v6, v7, :cond_1

    .line 361
    aget-byte v6, p3, v3

    mul-int/lit16 v6, v6, 0x80

    aget-byte v5, p3, v5

    add-int/2addr v6, v5

    .line 363
    invoke-direct {p0, v6, p1, p2}, Lcom/lltskb/lltskb/engine/QueryZZ;->loadTrain(III)Lcom/lltskb/lltskb/engine/ResultItem;

    move-result-object v5

    if-nez v5, :cond_0

    goto :goto_2

    .line 367
    :cond_0
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->isMatchFilter(Lcom/lltskb/lltskb/engine/ResultItem;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->mTrainSet:Ljava/util/Set;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 368
    invoke-interface {p5, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->mTrainSet:Ljava/util/Set;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x2

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    :cond_3
    return-void
.end method

.method private isEqualStation(II[B)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p1, p2, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 25
    :goto_0
    array-length v3, p3

    if-ge v2, v3, :cond_3

    .line 26
    aget-byte v3, p3, v2

    const/16 v4, 0x14

    if-lt v3, v4, :cond_1

    goto :goto_1

    .line 27
    :cond_1
    aget-byte v3, p3, v2

    add-int/2addr v3, p2

    if-ne p1, v3, :cond_2

    return v0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return v1
.end method

.method static synthetic lambda$queryMid$0(Lcom/lltskb/lltskb/engine/ResultItem;Lcom/lltskb/lltskb/engine/ResultItem;)I
    .locals 1

    const/4 v0, 0x1

    .line 204
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result p0

    .line 205
    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/Object;)I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method private loadTrain(III)Lcom/lltskb/lltskb/engine/ResultItem;
    .locals 19

    move-object/from16 v1, p0

    move/from16 v2, p1

    .line 444
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "KKK"

    :cond_0
    move-object v3, v0

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "laodTrain="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "QueryZZ"

    invoke-static {v4, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    .line 456
    :try_start_0
    invoke-virtual/range {p0 .. p3}, Lcom/lltskb/lltskb/engine/QueryZZ;->getTrainTimeDTO(III)Lcom/lltskb/lltskb/engine/TrainTimeDTO;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v6, v0

    .line 459
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v5

    :goto_0
    if-nez v6, :cond_1

    return-object v5

    .line 468
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 470
    iget v7, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-eqz v7, :cond_2

    iget-boolean v7, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->hiden:Z

    if-eqz v7, :cond_2

    return-object v5

    .line 472
    :cond_2
    new-instance v5, Lcom/lltskb/lltskb/engine/ResultItem;

    const/16 v7, 0xe

    const/4 v8, 0x0

    invoke-direct {v5, v7, v8, v8}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 473
    iget-object v7, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v7}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->isFuXingHao(I)Z

    move-result v7

    invoke-virtual {v5, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->setFuxing(Z)V

    .line 474
    sget v7, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_DISABLE_TRAIN:I

    invoke-virtual {v5, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 475
    invoke-virtual {v5, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    .line 476
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 477
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    iget v7, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v11, 0x1

    if-eq v7, v11, :cond_5

    if-eq v7, v10, :cond_4

    if-eq v7, v9, :cond_3

    .line 489
    sget v7, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v5, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    goto :goto_1

    :cond_3
    const-string v7, " [*]"

    .line 486
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v7, " [+]"

    .line 483
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    const-string v7, " [-]"

    .line 480
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v8, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 493
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v0, v8, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 496
    iget v7, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->fromStation:I

    .line 497
    iget-object v12, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v12, v7}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v12

    .line 498
    invoke-virtual {v5, v11, v12}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 507
    iget-object v12, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->arriveTime:Ljava/lang/String;

    invoke-virtual {v5, v10, v12}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 508
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    invoke-virtual {v0, v8, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 510
    iget v12, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->toStation:I

    .line 511
    iget-object v13, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v13, v12}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v13

    .line 512
    invoke-virtual {v5, v9, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    if-ne v7, v12, :cond_6

    const-string v13, "from is same as to"

    .line 517
    invoke-static {v4, v13}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_6
    iget-object v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->departTime:Ljava/lang/String;

    const/4 v14, 0x4

    invoke-virtual {v5, v14, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 528
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    invoke-virtual {v0, v8, v13}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 531
    iget v13, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endDist:I

    .line 532
    iget v15, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDist:I

    sub-int/2addr v13, v15

    .line 535
    iget v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDist:I

    if-nez v9, :cond_7

    iget v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-nez v9, :cond_7

    .line 536
    sget v9, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_START_TRAIN:I

    invoke-virtual {v5, v9}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    :cond_7
    if-nez v13, :cond_8

    .line 538
    iget v9, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-nez v9, :cond_8

    .line 539
    sget v9, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_START_TRAIN:I

    invoke-virtual {v5, v9}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    :cond_8
    if-gez v13, :cond_9

    neg-int v13, v13

    :cond_9
    const/high16 v9, 0x42c80000    # 100.0f

    .line 547
    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_a

    .line 548
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 549
    invoke-virtual {v1, v9}, Lcom/lltskb/lltskb/engine/QueryZZ;->getMaxSpeed(C)I

    move-result v9

    int-to-float v9, v9

    .line 552
    :cond_a
    iget v14, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    int-to-float v14, v14

    iget v10, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    int-to-float v10, v10

    const/high16 v17, 0x42700000    # 60.0f

    div-float v10, v10, v17

    add-float/2addr v14, v10

    int-to-float v10, v13

    div-float v14, v10, v14

    :goto_2
    cmpl-float v18, v14, v9

    if-lez v18, :cond_b

    .line 556
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "duration ="

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, " "

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    iget v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    add-int/lit8 v8, v8, 0x18

    iput v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    .line 558
    iget v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    int-to-float v8, v8

    iget v11, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    int-to-float v11, v11

    div-float v11, v11, v17

    add-float/2addr v8, v11

    div-float v14, v10, v8

    const/4 v8, 0x0

    const/4 v11, 0x1

    goto :goto_2

    .line 563
    :cond_b
    iget v4, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durHour:I

    .line 564
    iget v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->durMin:I

    const-string v9, "0"

    const/16 v10, 0xa

    if-ge v4, v10, :cond_c

    .line 566
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    :cond_c
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge v8, v10, :cond_d

    .line 569
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    :cond_d
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v4, 0x6

    .line 571
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x5

    invoke-virtual {v5, v9, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 572
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    const/4 v11, 0x0

    invoke-virtual {v0, v11, v8}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    const-string v0, "/"

    .line 577
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 578
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_3

    .line 580
    :cond_e
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 588
    :goto_3
    iget-object v8, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v8, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getXwUnit(I)[B

    move-result-object v8

    const v9, 0xffff

    if-eqz v8, :cond_f

    const/4 v11, 0x0

    .line 591
    aget-byte v9, v8, v11

    and-int/lit16 v9, v9, 0xff

    mul-int/lit16 v9, v9, 0x80

    const/4 v11, 0x1

    aget-byte v14, v8, v11

    and-int/lit16 v11, v14, 0xff

    add-int/2addr v9, v11

    .line 594
    :cond_f
    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 596
    invoke-virtual {v1, v8, v15, v11}, Lcom/lltskb/lltskb/engine/QueryZZ;->get_exact_train_name([BI[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_10

    .line 600
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v11, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    invoke-virtual {v1, v11}, Lcom/lltskb/lltskb/engine/QueryZZ;->getTrainStatusString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v11, 0x0

    invoke-virtual {v5, v11, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    move-object v3, v8

    goto :goto_4

    :cond_10
    const/4 v11, 0x0

    .line 603
    :goto_4
    iget v8, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->priceNo:I

    .line 611
    invoke-virtual {v3, v11}, Ljava/lang/String;->charAt(I)C

    move-result v14

    const/16 v11, 0x8

    const-string v15, "-"

    if-eqz v8, :cond_19

    const/16 v10, 0x44

    if-eq v14, v10, :cond_11

    const/16 v10, 0x47

    if-eq v14, v10, :cond_11

    const/16 v10, 0x43

    if-eq v14, v10, :cond_11

    const/16 v10, 0x53

    if-eq v14, v10, :cond_11

    goto :goto_9

    .line 640
    :cond_11
    iget-object v10, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->strDate:Ljava/lang/String;

    iget v14, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->mAuxDay:I

    neg-int v14, v14

    invoke-static {v10, v14}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v10

    .line 641
    invoke-static {v2, v10, v8}, Lcom/lltskb/lltskb/engine/PriceQuery;->getTsPriceNo(III)I

    move-result v2

    .line 643
    invoke-static {v2, v7, v12}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_gt(III)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_12

    const-string v8, " - "

    .line 648
    :cond_12
    invoke-virtual {v8, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 649
    array-length v10, v8

    const/4 v14, 0x2

    if-lt v10, v14, :cond_15

    and-int/lit8 v10, v9, 0x20

    if-eqz v10, :cond_13

    const/4 v10, 0x0

    .line 651
    aget-object v10, v8, v10

    goto :goto_5

    :cond_13
    move-object v10, v15

    :goto_5
    and-int/lit8 v14, v9, 0x10

    if-eqz v14, :cond_14

    const/4 v14, 0x1

    .line 654
    aget-object v8, v8, v14

    goto :goto_6

    :cond_14
    move-object v8, v15

    goto :goto_6

    :cond_15
    move-object v8, v15

    move-object v10, v8

    :goto_6
    and-int/lit8 v14, v9, 0x8

    if-eqz v14, :cond_17

    .line 659
    invoke-static {v2, v7, v12}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_gtrw(III)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_16

    goto :goto_7

    .line 663
    :cond_16
    invoke-virtual {v14, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    goto :goto_8

    :cond_17
    :goto_7
    move-object v14, v15

    :goto_8
    and-int/lit8 v9, v9, 0x40

    if-eqz v9, :cond_21

    .line 669
    invoke-static {v2, v7, v12}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_gtdw(III)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_18

    goto :goto_e

    .line 673
    :cond_18
    invoke-virtual {v2, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    goto :goto_e

    .line 614
    :cond_19
    :goto_9
    :try_start_1
    invoke-static {v2, v7, v12}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_pk(III)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_a

    :catch_1
    move-exception v0

    move-object v2, v0

    .line 616
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, ""

    :goto_a
    if-eqz v0, :cond_1a

    .line 619
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1b

    .line 620
    :cond_1a
    iget v0, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->type:I

    invoke-static {v0, v13}, Lcom/lltskb/lltskb/engine/PriceQuery;->get_price_normal(II)Ljava/lang/String;

    move-result-object v0

    .line 623
    :cond_1b
    invoke-virtual {v0, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 624
    array-length v2, v0

    const/4 v7, 0x4

    if-lt v2, v7, :cond_20

    and-int/lit8 v2, v9, 0x1

    if-eqz v2, :cond_1c

    const/4 v2, 0x0

    .line 626
    aget-object v2, v0, v2

    goto :goto_b

    :cond_1c
    move-object v2, v15

    :goto_b
    and-int/lit8 v7, v9, 0x2

    if-eqz v7, :cond_1d

    const/4 v7, 0x1

    .line 629
    aget-object v7, v0, v7

    goto :goto_c

    :cond_1d
    move-object v7, v15

    :goto_c
    and-int/lit8 v8, v9, 0x4

    if-eqz v8, :cond_1e

    const/4 v8, 0x2

    .line 632
    aget-object v8, v0, v8

    goto :goto_d

    :cond_1e
    move-object v8, v15

    :goto_d
    and-int/2addr v9, v11

    if-eqz v9, :cond_1f

    const/4 v9, 0x3

    .line 635
    aget-object v15, v0, v9

    :cond_1f
    move-object v10, v2

    move-object v14, v15

    move-object v15, v8

    move-object v8, v7

    goto :goto_e

    :cond_20
    move-object v8, v15

    move-object v10, v8

    move-object v14, v10

    :cond_21
    :goto_e
    const/4 v0, 0x7

    .line 678
    invoke-virtual {v5, v4, v10}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 679
    invoke-virtual {v5, v0, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0x9

    .line 680
    invoke-virtual {v5, v11, v15}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 681
    invoke-virtual {v5, v0, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0xb

    .line 700
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xa

    invoke-virtual {v5, v4, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v2, 0xc

    .line 702
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    iget v7, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->type:I

    invoke-virtual {v4, v3, v7}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainLX(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 704
    iget v0, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->beginStation:I

    const/16 v3, 0xd

    .line 705
    iget-object v4, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v4, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 707
    iget v0, v6, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endStation:I

    .line 708
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    return-object v5
.end method

.method private searchMid(IILjava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v7, p0

    .line 235
    iget-object v0, v7, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    move/from16 v8, p1

    invoke-virtual {v0, v8}, Lcom/lltskb/lltskb/engine/ResMgr;->getStation(I)[B

    move-result-object v9

    .line 236
    iget-object v0, v7, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    move/from16 v10, p2

    invoke-virtual {v0, v10}, Lcom/lltskb/lltskb/engine/ResMgr;->getStation(I)[B

    move-result-object v11

    if-eqz v9, :cond_8

    if-nez v11, :cond_0

    goto/16 :goto_8

    .line 243
    :cond_0
    new-instance v12, Ljava/util/Vector;

    invoke-direct {v12}, Ljava/util/Vector;-><init>()V

    .line 244
    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 245
    :goto_0
    array-length v0, v9

    if-ge v15, v0, :cond_1

    .line 246
    aget-byte v0, v9, v15

    mul-int/lit16 v0, v0, 0x80

    add-int/lit8 v1, v15, 0x1

    aget-byte v1, v9, v1

    add-int/2addr v1, v0

    .line 247
    iget-object v3, v7, Lcom/lltskb/lltskb/engine/QueryZZ;->bstart:[B

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v2, p1

    move-object v4, v12

    move-object v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/lltskb/lltskb/engine/QueryZZ;->getLinkStation(II[BLjava/util/Vector;Ljava/util/Vector;Z)V

    add-int/lit8 v15, v15, 0x2

    goto :goto_0

    .line 251
    :cond_1
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    .line 252
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    const/4 v15, 0x0

    .line 253
    :goto_1
    array-length v0, v11

    if-ge v15, v0, :cond_2

    .line 254
    aget-byte v0, v11, v15

    mul-int/lit16 v0, v0, 0x80

    add-int/lit8 v1, v15, 0x1

    aget-byte v1, v11, v1

    add-int/2addr v1, v0

    .line 255
    iget-object v3, v7, Lcom/lltskb/lltskb/engine/QueryZZ;->bend:[B

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v2, p2

    move-object v4, v8

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/lltskb/lltskb/engine/QueryZZ;->getLinkStation(II[BLjava/util/Vector;Ljava/util/Vector;Z)V

    add-int/lit8 v15, v15, 0x2

    goto :goto_1

    .line 267
    :cond_2
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 268
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    .line 269
    :goto_2
    invoke-virtual {v12}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 270
    invoke-virtual {v12, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, 0x0

    .line 271
    :goto_3
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_6

    .line 272
    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 274
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v6, v5, :cond_5

    .line 275
    invoke-virtual {v13, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 276
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 277
    invoke-virtual {v9, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 278
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x0

    .line 280
    :goto_4
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 281
    invoke-virtual {v1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 282
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 283
    invoke-virtual {v12, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 284
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_5

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 288
    :cond_4
    :goto_5
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ne v4, v5, :cond_6

    .line 289
    invoke-virtual {v12, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 290
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    .line 297
    :goto_7
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_8

    const/16 v3, 0x28

    if-ge v2, v3, :cond_8

    .line 298
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 299
    iget-object v4, v7, Lcom/lltskb/lltskb/engine/QueryZZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v3

    .line 300
    new-instance v4, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v5, 0x2

    invoke-direct {v4, v5, v14, v14}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 301
    sget v5, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 302
    invoke-virtual {v4, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    .line 303
    invoke-virtual {v4, v14, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 304
    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 305
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    move-object/from16 v3, p3

    .line 306
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_8
    :goto_8
    return-void
.end method


# virtual methods
.method getQueryType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public query(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 403
    iput v0, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->lastErr:I

    .line 404
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 405
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 407
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->saveMRI(Ljava/lang/String;)V

    .line 408
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/lltskb/lltskb/engine/ResMgr;->saveMRI(Ljava/lang/String;)V

    .line 410
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 411
    new-instance v2, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v3, 0x1

    const/16 v4, 0xe

    invoke-direct {v2, v4, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    # 车次名称
    const-string v4, " \u8f66\u6b21\u540d\u79f0 "

    .line 413
    invoke-virtual {v2, v0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    #    出发车站
    const-string v0, "   \u51fa\u53d1\u8f66\u7ad9  "

    .line 414
    invoke-virtual {v2, v3, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x2

    #   开点
    const-string v3, "  \u5f00\u70b9  "

    .line 415
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x3

    const-string v3, "  \u5230\u8fbe\u8f66\u7ad9  "

    .line 416
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x4

    const-string v3, "  \u5230\u70b9  "

    .line 417
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x5

    #   \u5386\u65f6
    const-string v3, "  \u5386\u65f6  "

    .line 418
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x6

    const-string v3, "  \u786c\u5ea7  "

    .line 419
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v0, 0x7

    const-string v3, "  \u8f6f\u5ea7  "

    .line 420
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0x8

    #       硬卧/上/中/下
    const-string v3, "      \u786c\u5367/\u4e0a/\u4e2d/\u4e0b      "

    .line 421
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0x9

    const-string v3, "     \u8f6f\u5367/\u4e0a/\u4e0b     "

    .line 422
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0xa

    const-string v3, " \u91cc\u7a0b "

    .line 423
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0xb

    const-string v3, "  \u8f66\u6b21\u7c7b\u578b  "

    .line 424
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0xc

    const-string v3, "    \u8d77\u59cb\u7ad9    "

    .line 425
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/16 v0, 0xd

    const-string v3, "    \u7ec8\u70b9\u7ad9    "

    .line 426
    invoke-virtual {v2, v0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    # new Array(ResultItem())
    .line 427
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryZZ;->mTrainSet:Ljava/util/Set;

    # mTrainSet.clear()
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    # (from, to, newArray)
    .line 430
    invoke-direct {p0, p1, p2, v1}, Lcom/lltskb/lltskb/engine/QueryZZ;->doWork(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v1
.end method

.method public queryMid(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 175
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v4, v3, v2}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    const-string v4, "  \u8f66\u7ad9\u540d\u79f0  "

    .line 176
    invoke-virtual {v1, v3, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v4, "  \u4e2d\u8f6c\u91cc\u7a0b  "

    .line 177
    invoke-virtual {v1, v2, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 178
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->getStationIndex(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    .line 181
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getStationIndex(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p2

    if-eqz p1, :cond_7

    if-nez p2, :cond_0

    goto/16 :goto_5

    .line 188
    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p2}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_4

    .line 192
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    .line 194
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 195
    invoke-virtual {p1, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    .line 196
    :goto_1
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 197
    invoke-virtual {p2, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 199
    invoke-direct {p0, v5, v7, v1}, Lcom/lltskb/lltskb/engine/QueryZZ;->searchMid(IILjava/util/List;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 203
    :cond_3
    sget-object p1, Lcom/lltskb/lltskb/engine/-$$Lambda$QueryZZ$lZwtuRLLWZ7g2vMV-OtkHwUglTs;->INSTANCE:Lcom/lltskb/lltskb/engine/-$$Lambda$QueryZZ$lZwtuRLLWZ7g2vMV-OtkHwUglTs;

    invoke-static {v1, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 210
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 211
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 212
    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_2

    .line 215
    :cond_4
    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 219
    :cond_5
    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/16 p2, 0x28

    if-le p1, p2, :cond_6

    .line 220
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    sub-int/2addr p1, v2

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    :goto_4
    return-object v0

    .line 184
    :cond_7
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryMid fromIndex=null || toIndex==null, from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ",toIndexs="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "QueryZZ"

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method
