.class public Lcom/lltskb/lltskb/engine/QueryConst;
.super Ljava/lang/Object;
.source "QueryConst.java"


# static fields
.field public static COLOR_DISABLE_TRAIN:I = -0x7f7f80

.field public static COLOR_PASSBY_TRAIN:I = -0xe98105

.field public static COLOR_START_TRAIN:I = -0x30cedb

.field public static final QUERY_TYPE:Ljava/lang/String; = "query_type"

.field public static final QUERY_TYPE_CC:I = 0x1

.field public static final QUERY_TYPE_CZ:I = 0x2

.field public static final QUERY_TYPE_ZZ:I = 0x0

.field public static final RESULT_CAN_SORT:Ljava/lang/String; = "result_can_sort"

.field public static final RESULT_INDEX_ARRIVESTATION:I = 0x2

.field public static final RESULT_INDEX_ARRIVE_TIME:I = 0x4

.field public static final RESULT_INDEX_BEGINSTATION:I = 0xb

.field public static final RESULT_INDEX_DAY:I = 0x11

.field public static final RESULT_INDEX_DISTANCE:I = 0xd

.field public static final RESULT_INDEX_DURATION:I = 0x5

.field public static final RESULT_INDEX_ENDSTATION:I = 0xc

.field public static final RESULT_INDEX_HARDBED:I = 0x8

.field public static final RESULT_INDEX_HARDSEAT:I = 0x6

.field public static final RESULT_INDEX_JIANPIAOKOU:I = 0xf

.field public static final RESULT_INDEX_SOFTBED:I = 0x9

.field public static final RESULT_INDEX_SOFTSEAT:I = 0x7

.field public static final RESULT_INDEX_SPEED:I = 0x10

.field public static final RESULT_INDEX_STARTSTATION:I = 0x1

.field public static final RESULT_INDEX_START_TIME:I = 0x3

.field public static final RESULT_INDEX_STATIONNAME:I = 0xe

.field public static final RESULT_INDEX_TRAINNAME:I = 0x0

.field public static final RESULT_INDEX_TYPE:I = 0xa


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
