.class public Lcom/lltskb/lltskb/engine/ConfigMgr;
.super Ljava/lang/Object;
.source "ConfigMgr.java"


# static fields
.field public static final AD_TYPE_BAIDU:I = 0x0

.field public static final AD_TYPE_GDT:I = 0x2

.field public static final AD_TYPE_INMOBI:I = 0x1

.field public static final CONFIG_FILE:Ljava/lang/String; = "config.json"

.field private static final TAG:Ljava/lang/String; = "ConfigMgr"

.field private static instance:Lcom/lltskb/lltskb/engine/ConfigMgr;


# instance fields
.field private gdtWeitht:I

.field private final inMobiAccountId:Ljava/lang/String;

.field private final inMobiPlaceId:J

.field private inmobiWeight:I

.field private isEnableGdt:Z

.field private isEnableInMobi:Z

.field private isEnableSSP:Z

.field private isEnablelltskb:Z

.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private final mLLTAppSid:Ljava/lang/String;

.field private final mLLTPlaceId:Ljava/lang/String;

.field private mLastUpdateTime:J

.field private mPath:Ljava/lang/String;

.field private mTimer:I

.field private final mYMAppSid:Ljava/lang/String;

.field private final mYMPlaceId:Ljava/lang/String;

.field private sspWeight:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 56
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableSSP:Z

    const/4 v1, 0x0

    .line 57
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnablelltskb:Z

    .line 58
    iput-boolean v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableInMobi:Z

    .line 59
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableGdt:Z

    .line 60
    iput v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->sspWeight:I

    .line 61
    iput v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inmobiWeight:I

    .line 62
    iput v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->gdtWeitht:I

    const/4 v0, 0x6

    .line 64
    iput v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    .line 66
    iput v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mCount:I

    const-wide/16 v0, 0x0

    .line 68
    iput-wide v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    const-string v0, "/data/com.lltskb.lltskb/files"

    .line 71
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mPath:Ljava/lang/String;

    const-string v0, "bad6831f"

    .line 73
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLLTAppSid:Ljava/lang/String;

    const-string v0, "5928025"

    .line 74
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLLTPlaceId:Ljava/lang/String;

    const-string v0, "f307bfa1"

    .line 76
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mYMAppSid:Ljava/lang/String;

    const-string v0, "2679007"

    .line 77
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mYMPlaceId:Ljava/lang/String;

    const-string v0, "dc597fb80c894356842b684a42c284a3"

    .line 79
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inMobiAccountId:Ljava/lang/String;

    const-wide v0, 0x15f622dfaa6L

    .line 80
    iput-wide v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inMobiPlaceId:J

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;
    .locals 2

    const-class v0, Lcom/lltskb/lltskb/engine/ConfigMgr;

    monitor-enter v0

    .line 84
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ConfigMgr;->instance:Lcom/lltskb/lltskb/engine/ConfigMgr;

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Lcom/lltskb/lltskb/engine/ConfigMgr;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;-><init>()V

    sput-object v1, Lcom/lltskb/lltskb/engine/ConfigMgr;->instance:Lcom/lltskb/lltskb/engine/ConfigMgr;

    .line 87
    :cond_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ConfigMgr;->instance:Lcom/lltskb/lltskb/engine/ConfigMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private setOpenCount(I)V
    .locals 2

    .line 158
    iput p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mCount:I

    .line 160
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    const-string v1, "config"

    .line 161
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 163
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 165
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mCount:I

    const-string v1, "open_count"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    return-void
.end method


# virtual methods
.method public getAppSid()Ljava/lang/String;
    .locals 2

    .line 150
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnablelltskb:Z

    const-string v1, "bad6831f"

    if-eqz v0, :cond_0

    :cond_0
    return-object v1
.end method

.method public getInMobiAccountId()Ljava/lang/String;
    .locals 1

    const-string v0, "dc597fb80c894356842b684a42c284a3"

    return-object v0
.end method

.method public getInMobiPlaceId()J
    .locals 2

    const-wide v0, 0x15f622dfaa6L

    return-wide v0
.end method

.method public getNextAdType()I
    .locals 6

    .line 92
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableInMobi:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 93
    iput v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inmobiWeight:I

    .line 96
    :cond_0
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableSSP:Z

    if-nez v0, :cond_1

    .line 97
    iput v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->sspWeight:I

    .line 100
    :cond_1
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableGdt:Z

    if-nez v0, :cond_2

    .line 101
    iput v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->gdtWeitht:I

    .line 103
    :cond_2
    iget v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inmobiWeight:I

    iget v2, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->sspWeight:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->gdtWeitht:I

    add-int/2addr v0, v2

    if-gtz v0, :cond_3

    const/4 v0, -0x1

    return v0

    .line 108
    :cond_3
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v2, v2, v4

    double-to-int v2, v2

    rem-int/2addr v2, v0

    .line 110
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CURR="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ",total="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ConfigMgr"

    invoke-static {v3, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->gdtWeitht:I

    const/4 v3, 0x2

    if-ge v2, v0, :cond_4

    return v3

    .line 117
    :cond_4
    iget v4, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inmobiWeight:I

    add-int v5, v4, v0

    if-ge v2, v5, :cond_5

    const/4 v0, 0x1

    return v0

    .line 120
    :cond_5
    iget v5, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->sspWeight:I

    add-int/2addr v4, v5

    add-int/2addr v4, v0

    if-ge v2, v4, :cond_6

    return v1

    :cond_6
    return v3
.end method

.method public getPlaceId()Ljava/lang/String;
    .locals 2

    .line 201
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnablelltskb:Z

    const-string v1, "5928025"

    if-eqz v0, :cond_0

    :cond_0
    return-object v1
.end method

.method public init(Ljava/lang/String;)Z
    .locals 10

    const-string v0, "kuntao_h5"

    const-string v1, "ConfigMgr"

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    .line 215
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    new-instance p1, Ljava/io/FileInputStream;

    invoke-direct {p1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 218
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 219
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 220
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    :goto_0
    if-eqz v4, :cond_0

    .line 222
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 225
    :cond_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 226
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 232
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "config json ="

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "config json is empty"

    .line 235
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 240
    :cond_1
    :try_start_1
    new-instance p1, Lorg/json/JSONTokener;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/json/JSONObject;

    if-nez p1, :cond_2

    return v3

    :cond_2
    const-string v2, "android"

    .line 243
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_3

    return v3

    :cond_3
    const-string v2, "ssp"

    .line 245
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v4, "timer"

    const/4 v5, 0x6

    const-string v6, "weight"

    const-string v7, "enable"

    if-eqz v2, :cond_4

    .line 247
    :try_start_2
    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableSSP:Z

    const-string v8, "lltskb"

    .line 248
    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnablelltskb:Z

    .line 249
    invoke-virtual {v2, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->sspWeight:I

    .line 251
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    .line 252
    iget v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    if-gtz v8, :cond_4

    .line 253
    iput v5, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    :cond_4
    const-string v8, "inmobi"

    .line 257
    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 259
    invoke-virtual {v8, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->inmobiWeight:I

    .line 260
    invoke-virtual {v8, v7, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableInMobi:Z

    if-eqz v2, :cond_5

    .line 261
    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    :cond_5
    iput v5, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    :cond_6
    const-string v2, "gdt"

    .line 264
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const/4 v4, 0x1

    if-eqz v2, :cond_7

    .line 266
    invoke-virtual {v2, v6, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->gdtWeitht:I

    .line 267
    invoke-virtual {v2, v7, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableGdt:Z

    :cond_7
    const-string v2, "baoxian"

    .line 270
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 272
    invoke-virtual {p1, v7, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string v5, "hint"

    .line 273
    invoke-virtual {p1, v5, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 274
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-nez v2, :cond_8

    const/4 p1, 0x0

    const/4 v5, 0x0

    :cond_8
    const-string v2, "baoxian_hint"

    .line 281
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/lltskb/lltskb/utils/FeatureToggle;->setFeatureEnabled(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 282
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/FeatureToggle;->setFeatureEnabled(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_9
    return v4

    :catch_0
    move-exception p1

    .line 289
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_1
    move-exception p1

    .line 286
    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_2
    move-exception p1

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read config.json failed: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method public isEnableSSP()Z
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableSSP:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableInMobi:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableGdt:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method saveConfig(Ljava/lang/String;)V
    .locals 5

    const-string v0, "save config error "

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveConfig="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ConfigMgr"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "config.json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 300
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 303
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 305
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 310
    :cond_0
    :goto_0
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 311
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 312
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 313
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->init(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 318
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception p1

    .line 315
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 4

    .line 128
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mContext:Landroid/content/Context;

    .line 129
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mContext:Landroid/content/Context;

    if-nez p1, :cond_0

    return-void

    .line 131
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 133
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mPath:Ljava/lang/String;

    .line 135
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    const-string v1, "config"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    const-wide/16 v1, 0x0

    const-string v3, "last_update_time"

    .line 137
    invoke-interface {p1, v3, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    const-string v1, "open_count"

    .line 138
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mCount:I

    return-void
.end method

.method public setLastUpdateTime(J)V
    .locals 2

    .line 171
    iput-wide p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    .line 172
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_2

    const/4 p2, 0x0

    const-string v0, "config"

    .line 173
    invoke-virtual {p1, v0, p2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 175
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 177
    :cond_1
    iget-wide v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    const-string p2, "last_update_time"

    invoke-interface {p1, p2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 178
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    return-void
.end method

.method public shouldShowAd()Z
    .locals 10

    .line 188
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getBaiduCrashed()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-gt v0, v2, :cond_0

    const-string v0, "ConfigMgr"

    const-string v2, "baidu crashed"

    .line 189
    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 192
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mCount:I

    const/4 v2, 0x3

    const/4 v3, 0x1

    if-ge v0, v2, :cond_1

    add-int/2addr v0, v3

    .line 193
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setOpenCount(I)V

    return v1

    .line 197
    :cond_1
    iget-wide v4, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mLastUpdateTime:J

    sub-long/2addr v4, v6

    iget v0, p0, Lcom/lltskb/lltskb/engine/ConfigMgr;->mTimer:I

    mul-int/lit16 v0, v0, 0xe10

    int-to-long v6, v0

    const-wide/16 v8, 0x3e8

    mul-long v6, v6, v8

    cmp-long v0, v4, v6

    if-ltz v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method
