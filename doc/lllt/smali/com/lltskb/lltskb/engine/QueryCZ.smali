.class public Lcom/lltskb/lltskb/engine/QueryCZ;
.super Lcom/lltskb/lltskb/engine/QueryBase;
.source "QueryCZ.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QueryCZ"


# instance fields
.field private mTrainSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/QueryBase;-><init>(ZZ)V

    .line 16
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->mTrainSet:Ljava/util/Set;

    return-void
.end method

.method public static getIndex(I)I
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    const/16 v1, 0xe

    if-eq p0, v1, :cond_2

    const/4 v1, 0x3

    if-eq p0, v1, :cond_1

    const/4 v2, 0x4

    if-eq p0, v2, :cond_0

    packed-switch p0, :pswitch_data_0

    return v0

    :pswitch_0
    const/4 p0, 0x5

    return p0

    :pswitch_1
    return v2

    :pswitch_2
    const/4 p0, 0x6

    return p0

    :cond_0
    return v1

    :cond_1
    const/4 p0, 0x2

    return p0

    :cond_2
    const/4 p0, 0x1

    return p0

    :cond_3
    return v0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getResult(ILjava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)Z"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStation(I)[B

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 111
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v2

    .line 112
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_3

    .line 116
    aget-byte v4, v0, v1

    and-int/lit16 v4, v4, 0xff

    mul-int/lit16 v4, v4, 0x80

    add-int/lit8 v5, v1, 0x1

    aget-byte v5, v0, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v4, v5

    .line 118
    invoke-direct {p0, v4, p1, p1}, Lcom/lltskb/lltskb/engine/QueryCZ;->loadTrain(III)Lcom/lltskb/lltskb/engine/ResultItem;

    move-result-object v5

    if-nez v5, :cond_1

    goto :goto_1

    :cond_1
    const/16 v6, 0xe

    .line 121
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v6

    invoke-virtual {v5, v6, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 123
    iget-object v6, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->mTrainSet:Ljava/util/Set;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 124
    invoke-interface {p2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->mTrainSet:Ljava/util/Set;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic lambda$query$0(Lcom/lltskb/lltskb/engine/ResultItem;Lcom/lltskb/lltskb/engine/ResultItem;)I
    .locals 5

    const/4 v0, 0x3

    .line 81
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "--"

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 83
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 88
    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    if-eqz v1, :cond_3

    if-nez p0, :cond_2

    goto :goto_0

    .line 95
    :cond_2
    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0

    :cond_3
    :goto_0
    const/4 p0, -0x1

    return p0
.end method

.method private loadTrain(III)Lcom/lltskb/lltskb/engine/ResultItem;
    .locals 9

    const/4 v0, 0x0

    .line 143
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/lltskb/lltskb/engine/QueryCZ;->getTrainTimeDTO(III)Lcom/lltskb/lltskb/engine/TrainTimeDTO;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 147
    sget-object p3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    move-object p2, v0

    :goto_0
    if-nez p2, :cond_0

    const-string p1, "QueryCZ"

    const-string p2, " train data is null"

    .line 152
    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 156
    :cond_0
    iget-object p3, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {p3, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_1

    goto :goto_1

    :cond_1
    const-string p3, "kkk"

    .line 167
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget v2, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->hiden:Z

    if-eqz v2, :cond_2

    return-object v0

    .line 171
    :cond_2
    new-instance v0, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v2, 0x7

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 172
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->isFuXingHao(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setFuxing(Z)V

    .line 173
    sget v2, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 174
    invoke-virtual {v0, p3}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    .line 175
    invoke-static {p3}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 176
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget v2, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getTrainStatusString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    .line 178
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 179
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 182
    iget-object v1, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->arriveTime:Ljava/lang/String;

    .line 183
    iget-object v5, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->departTime:Ljava/lang/String;

    .line 185
    iget v6, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->priceNo:I

    .line 187
    iget v6, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->startDist:I

    .line 188
    iget v7, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endDist:I

    sub-int v6, v7, v6

    if-nez v6, :cond_3

    .line 191
    iget v8, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-nez v8, :cond_3

    .line 192
    sget v8, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_START_TRAIN:I

    invoke-virtual {v0, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    goto :goto_2

    .line 193
    :cond_3
    iget v8, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-eqz v8, :cond_4

    .line 194
    sget v8, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_DISABLE_TRAIN:I

    invoke-virtual {v0, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 203
    :cond_4
    :goto_2
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v8, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getXwUnit(I)[B

    move-result-object p1

    .line 204
    invoke-static {p3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "/"

    .line 205
    invoke-virtual {p3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 206
    invoke-virtual {p0, p1, v7, v8}, Lcom/lltskb/lltskb/engine/QueryCZ;->get_exact_train_name([BI[Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 209
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    invoke-virtual {p0, v7}, Lcom/lltskb/lltskb/engine/QueryCZ;->getTrainStatusString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, v4, p3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    goto :goto_3

    :cond_5
    move-object p1, p3

    :goto_3
    const-string p3, "placeholder"

    .line 213
    invoke-virtual {v0, v2, p3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    if-gez v6, :cond_6

    neg-int v6, v6

    .line 218
    :cond_6
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    const-string v2, "--:--"

    if-eqz p3, :cond_8

    if-nez v6, :cond_7

    move-object v1, v2

    goto :goto_4

    :cond_7
    move-object v5, v2

    :cond_8
    :goto_4
    const/4 p3, 0x3

    .line 224
    invoke-virtual {v0, v3, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v1, 0x4

    .line 225
    invoke-virtual {v0, p3, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 227
    iget p3, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->beginStation:I

    .line 228
    iget v2, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->endStation:I

    .line 230
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object p3

    const/4 v3, 0x5

    .line 231
    invoke-virtual {v0, v1, p3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p3

    invoke-virtual {p3, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object p3

    const/4 v1, 0x6

    .line 233
    invoke-virtual {v0, v3, p3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 235
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    iget p2, p2, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->type:I

    invoke-virtual {v2, p1, p2}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainLX(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method getQueryType()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public query(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query cz="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QueryCZ"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    .line 53
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 54
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->saveMRI(Ljava/lang/String;)V

    .line 56
    new-instance v2, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v3, 0x7

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 57
    sget v3, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    const/4 v3, 0x0

    const-string v6, "\u8f66\u6b21\u540d\u79f0"

    .line 58
    invoke-virtual {v2, v3, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v6, "      \u8f66\u7ad9     "

    .line 60
    invoke-virtual {v2, v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v5, "  \u5230\u65f6  "

    .line 61
    invoke-virtual {v2, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x3

    const-string v5, "  \u5f00\u65f6  "

    .line 62
    invoke-virtual {v2, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x4

    const-string v5, "    \u59cb\u53d1\u7ad9    "

    .line 63
    invoke-virtual {v2, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x5

    const-string v5, "    \u7ec8\u5230\u7ad9    "

    .line 64
    invoke-virtual {v2, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x6

    const-string v5, "   \u8f66\u6b21\u7c7b\u578b   "

    .line 65
    invoke-virtual {v2, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 66
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCZ;->mTrainSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 69
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getStationIndex(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, "query v is null"

    .line 71
    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 74
    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 75
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->getResult(ILjava/util/List;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "getResult failed"

    .line 76
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 80
    :cond_3
    sget-object p1, Lcom/lltskb/lltskb/engine/-$$Lambda$QueryCZ$v7kyGUqpPVMopPiSiq3ahn3Clf4;->INSTANCE:Lcom/lltskb/lltskb/engine/-$$Lambda$QueryCZ$v7kyGUqpPVMopPiSiq3ahn3Clf4;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method
