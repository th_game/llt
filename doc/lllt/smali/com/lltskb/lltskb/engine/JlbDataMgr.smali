.class public Lcom/lltskb/lltskb/engine/JlbDataMgr;
.super Lcom/lltskb/lltskb/engine/DataMgr;
.source "JlbDataMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;,
        Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "JlbDataMgr"


# instance fields
.field private foodCoachesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;",
            ">;"
        }
    .end annotation
.end field

.field private jlbList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/DataMgr;-><init>(Ljava/lang/String;)V

    .line 33
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->foodCoachesList:Ljava/util/List;

    .line 34
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->jlbList:Ljava/util/List;

    .line 38
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->initDataList()V

    return-void
.end method

.method private convertTrains(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .line 126
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 130
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    .line 131
    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    if-nez v1, :cond_1

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 135
    :cond_1
    array-length v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_7

    aget-object v6, v1, v5

    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    const-string v7, "#/"

    .line 137
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 139
    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :goto_1
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 142
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 147
    :cond_3
    invoke-static {v6, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    goto :goto_4

    .line 151
    :cond_4
    aget-object v7, v6, v4

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    aget-object v7, v6, v4

    const/4 v8, 0x1

    .line 153
    :goto_2
    array-length v9, v6

    if-ge v8, v9, :cond_6

    .line 154
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    aget-object v10, v6, v8

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    sub-int/2addr v9, v10

    if-gtz v9, :cond_5

    .line 156
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "convertTrains error train="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "JlbDataMgr"

    invoke-static {v10, v9}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 160
    :cond_5
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    aget-object v10, v6, v8

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {v0, v7, v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    aget-object v9, v6, v8

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 163
    :cond_6
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 166
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private initDataList()V
    .locals 11

    .line 46
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    array-length v0, v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    goto/16 :goto_5

    :cond_0
    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    :goto_0
    const-string v5, "utf-8"

    const-string v6, "JlbDataMgr"

    if-ge v3, v2, :cond_1

    .line 53
    new-instance v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    invoke-direct {v7, p0}, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;-><init>(Lcom/lltskb/lltskb/engine/JlbDataMgr;)V

    .line 54
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->start:I

    add-int/lit8 v4, v4, 0x4

    .line 56
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->end:I

    add-int/2addr v4, v1

    .line 58
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->rule1:I

    add-int/2addr v4, v1

    .line 60
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v8

    iput v8, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->rule2:I

    add-int/2addr v4, v1

    .line 62
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    aget-byte v8, v8, v4

    and-int/lit16 v8, v8, 0xff

    iput v8, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->qiye:I

    add-int/lit8 v4, v4, 0x1

    .line 65
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    aget-byte v8, v8, v4

    and-int/lit16 v8, v8, 0xff

    add-int/lit8 v4, v4, 0x1

    .line 68
    :try_start_0
    new-instance v9, Ljava/lang/String;

    iget-object v10, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    invoke-direct {v9, v10, v4, v8, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iput-object v9, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->bianzu:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v9

    .line 70
    invoke-virtual {v9}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 71
    invoke-virtual {v9}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/2addr v4, v8

    .line 75
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    aget-byte v8, v8, v4

    and-int/lit16 v8, v8, 0xff

    add-int/lit8 v4, v4, 0x1

    .line 78
    :try_start_1
    new-instance v9, Ljava/lang/String;

    iget-object v10, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    invoke-direct {v9, v10, v4, v8, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-direct {p0, v9}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->convertTrains(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->trains:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v5

    .line 80
    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 81
    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    add-int/2addr v4, v8

    .line 84
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->jlbList:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 87
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initDataList jlb size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->jlbList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->foodCoachesList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 89
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x8

    # len <= size + 8
    if-le v4, v2, :cond_2

    return-void

    .line 93
    :cond_2
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v2

    add-int/2addr v4, v1

    :goto_3
    if-ge v0, v2, :cond_3

    .line 96
    new-instance v3, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;-><init>(Lcom/lltskb/lltskb/engine/JlbDataMgr;)V

    .line 97
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v7

    iput v7, v3, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->index:I

    add-int/2addr v4, v1

    .line 99
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v7

    iput v7, v3, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->start:I

    add-int/2addr v4, v1

    .line 101
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->readInt(I)I

    move-result v7

    iput v7, v3, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->end:I

    add-int/2addr v4, v1

    .line 104
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    aget-byte v7, v7, v4

    and-int/lit16 v7, v7, 0xff

    add-int/lit8 v4, v4, 0x1

    .line 108
    :try_start_2
    new-instance v8, Ljava/lang/String;

    iget-object v9, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->data:[B

    invoke-direct {v8, v9, v4, v7, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iput-object v8, v3, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->coach:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    :catch_2
    move-exception v8

    .line 110
    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 111
    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    add-int/2addr v4, v7

    .line 114
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->foodCoachesList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 116
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initDataList food coach size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->foodCoachesList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_5
    return-void
.end method


# virtual methods
.method public buildJlbMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 16

    .line 243
    invoke-static/range {p1 .. p1}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getJlb()Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    move-result-object v1

    const-string v2, ""

    if-eqz v1, :cond_c

    .line 245
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->trains:Ljava/lang/String;

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_5

    .line 249
    :cond_0
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->trains:Ljava/lang/String;

    const-string v4, "#"

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    return-object v2

    :cond_1
    const-string v4, "/"

    .line 254
    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return-object v2

    .line 259
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v2

    iget v5, v1, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->qiye:I

    invoke-virtual {v2, v5}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->getQiYe(I)Ljava/lang/String;

    move-result-object v2

    .line 260
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v6

    const v7, 0x7f0d01b0

    invoke-virtual {v6, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    const-string v8, "<br/>"

    if-nez v7, :cond_3

    .line 263
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v7

    const v9, 0x7f0d00c9

    invoke-virtual {v7, v9}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v7, 0x7f0d0070

    invoke-virtual {v2, v7}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->bianzu:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    array-length v1, v3

    const/4 v2, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v1, :cond_b

    aget-object v7, v3, v6

    .line 271
    array-length v9, v0

    const/4 v10, 0x0

    :goto_1
    const/4 v11, 0x1

    if-ge v10, v9, :cond_5

    aget-object v12, v0, v10

    .line 272
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v9, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_5
    const/4 v9, 0x0

    :goto_2
    if-eqz v9, :cond_6

    const-string v10, "<font color=\"#5fc534\"><b>"

    .line 279
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    :cond_6
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    sub-int/2addr v10, v11

    invoke-virtual {v7, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 284
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-static {v7, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const-string v10, "</b></font>"

    if-nez v7, :cond_8

    if-eqz v9, :cond_7

    .line 288
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :cond_7
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 295
    :cond_8
    new-instance v12, Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-direct {v12, v2, v11}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    .line 296
    aget-object v7, v7, v2

    const/4 v13, -0x1

    invoke-virtual {v12, v7, v13}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 297
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x2

    if-le v12, v13, :cond_9

    .line 299
    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 300
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v13

    sub-int/2addr v13, v11

    invoke-interface {v7, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v11, 0x3

    .line 302
    invoke-static {v11}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v11

    const/4 v13, 0x4

    .line 303
    invoke-static {v13}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v13

    const/16 v14, 0xe

    .line 305
    invoke-static {v14}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v14

    const-string v15, " ("

    .line 306
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, " "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, " - "

    .line 307
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v14}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v13}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ")"

    .line 308
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 311
    :cond_9
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    if-eqz v9, :cond_a

    .line 315
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 319
    :cond_b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_c
    :goto_5
    return-object v2
.end method

.method public getFoodCoach(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;
    .locals 4

    .line 176
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_5

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    :goto_0
    const-string v0, "-"

    .line 180
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, ""

    .line 181
    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 184
    :cond_1
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 186
    invoke-static {p2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result p2

    .line 187
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndex(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    return-object v1

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->foodCoachesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;

    if-eqz p2, :cond_4

    .line 193
    iget v3, v2, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->start:I

    if-gt v3, p2, :cond_3

    iget v3, v2, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->end:I

    if-ge v3, p2, :cond_4

    goto :goto_1

    .line 196
    :cond_4
    iget v3, v2, Lcom/lltskb/lltskb/engine/JlbDataMgr$FoodCoach;->index:I

    if-ne v3, p1, :cond_3

    return-object v2

    :cond_5
    :goto_2
    return-object v1
.end method

.method public getJlb(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;
    .locals 9

    .line 210
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    invoke-static {p2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    :cond_0
    :goto_0
    const-string v0, "-"

    .line 214
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, ""

    .line 215
    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 218
    :cond_1
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 220
    invoke-static {p2, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result p2

    const-string v2, "/"

    .line 221
    invoke-static {p1, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    return-object v1

    .line 225
    :cond_2
    array-length v3, p1

    :goto_1
    if-ge v0, v3, :cond_6

    aget-object v4, p1, v0

    .line 226
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/JlbDataMgr;->jlbList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    if-eqz p2, :cond_4

    .line 227
    iget v7, v6, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->start:I

    if-gt v7, p2, :cond_3

    iget v7, v6, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->end:I

    if-ge v7, p2, :cond_4

    goto :goto_2

    .line 230
    :cond_4
    iget-object v7, v6, Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;->trains:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    return-object v6

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    :goto_3
    return-object v1
.end method
