.class public Lcom/lltskb/lltskb/engine/ResultItem;
.super Ljava/lang/Object;
.source "ResultItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/lltskb/lltskb/engine/ResultItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final FIRST_SECTION:I = 0x0

.field public static final SECOND_SECTION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ResultItem"

.field private static houCheTime:Ljava/util/Date;

.field private static simpleDateFormat:Ljava/text/SimpleDateFormat;


# instance fields
.field private isFuxing:Z

.field private mAuxDay:I

.field private mBackColor:I

.field private mHead:Z

.field private mIsHtml:[B

.field private mIsNextStation:Z

.field private mName:Ljava/lang/String;

.field private mQueryType:I

.field private mSectionType:I

.field private mSelected:Z

.field private mText:[Ljava/lang/String;

.field private mTextColor:I

.field private mWidths:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 315
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const-string v2, "HH:mm"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/lltskb/lltskb/engine/ResultItem;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 2

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing:Z

    .line 62
    iput v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mQueryType:I

    .line 63
    iput v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mAuxDay:I

    .line 64
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSelected:Z

    .line 65
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mHead:Z

    .line 71
    iput v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    .line 74
    new-array v1, p1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mText:[Ljava/lang/String;

    .line 75
    new-array v1, p1, [I

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mWidths:[I

    .line 76
    new-array p1, p1, [B

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mIsHtml:[B

    const/4 p1, -0x1

    if-eqz p3, :cond_0

    const/4 v1, -0x1

    goto :goto_0

    :cond_0
    const v1, -0xe98105    # -2.0004352E38f

    .line 77
    :goto_0
    iput v1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mTextColor:I

    if-eqz p3, :cond_1

    const p1, -0xe7652a

    .line 78
    :cond_1
    iput p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mBackColor:I

    .line 79
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSelected:Z

    .line 80
    iput p2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mQueryType:I

    .line 81
    iput-boolean p3, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mHead:Z

    .line 82
    iput v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    .line 83
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing:Z

    return-void
.end method

.method private compareToCZ(Lcom/lltskb/lltskb/engine/ResultItem;)I
    .locals 5

    const/4 v0, 0x3

    .line 346
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "--"

    .line 347
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x4

    if-eqz v3, :cond_0

    .line 348
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 350
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 352
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private getTimeToHouChe(Ljava/lang/String;)J
    .locals 5

    .line 333
    :try_start_0
    sget-object v0, Lcom/lltskb/lltskb/engine/ResultItem;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    .line 334
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sget-object v2, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const-wide/32 v0, 0x5265c00

    .line 335
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    add-long/2addr v2, v0

    sget-object p1, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long/2addr v2, v0

    return-wide v2

    .line 337
    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sget-object p1, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    sub-long/2addr v0, v2

    return-wide v0

    :catch_0
    move-exception p1

    .line 339
    invoke-virtual {p1}, Ljava/text/ParseException;->printStackTrace()V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private toStringCC()Ljava/lang/String;
    .locals 7

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/ResultItem;->getName()Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    .line 212
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    .line 213
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x1

    const/16 v6, 0x30

    if-lt v4, v6, :cond_0

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v6, 0x39

    if-le v4, v6, :cond_1

    .line 214
    :cond_0
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v6, 0x41

    if-lt v4, v6, :cond_2

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v4, 0x5a

    if-gt v1, v4, :cond_2

    :cond_1
    const/4 v3, 0x1

    :cond_2
    const/4 v1, 0x3

    const/4 v4, 0x4

    if-eqz v3, :cond_3

    .line 217
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v4

    sub-int/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " \u59cb\u53d1\n"

    .line 218
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v1

    sub-int/2addr v1, v5

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " \u7ec8\u5230 "

    .line 220
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 222
    :cond_3
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " \u5230\n"

    .line 223
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " \u5f00 "

    .line 225
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0xd

    if-eqz v3, :cond_4

    .line 232
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v1

    sub-int/2addr v1, v5

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 234
    :cond_4
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " \u516c\u91cc"

    .line 235
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toStringCZ()Ljava/lang/String;
    .locals 3

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 191
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    .line 192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0xb

    .line 193
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0xc

    .line 195
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") "

    .line 196
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    .line 197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    .line 198
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u5230\n"

    .line 199
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    .line 200
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u5f00"

    .line 201
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    .line 204
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryCZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toStringHead()Ljava/lang/String;
    .locals 5

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mText:[Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 183
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 185
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toStringZZ()Ljava/lang/String;
    .locals 12

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 243
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ("

    .line 244
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    .line 245
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    .line 246
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0xc

    .line 247
    invoke-static {v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ") "

    .line 248
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    .line 249
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    .line 251
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    .line 253
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    .line 254
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x3

    .line 255
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u5f00\n"

    .line 256
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x2

    .line 257
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v7, 0x4

    .line 259
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\u5230 "

    .line 260
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v7, 0x5

    .line 262
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 263
    array-length v9, v8

    if-ge v9, v6, :cond_0

    goto :goto_0

    :cond_0
    const-string v6, "\u5386\u65f6"

    .line 266
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, v8, v1

    .line 267
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u65f6"

    .line 268
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v8, v4

    .line 269
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\u5206"

    .line 270
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 264
    :cond_1
    :goto_0
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-static {v1}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    .line 275
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x7

    .line 276
    invoke-static {v6}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x9

    .line 277
    invoke-static {v7}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x8

    .line 278
    invoke-static {v8}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v8

    .line 280
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x44

    const-string v11, "\u8f6f\u5367\uff1a"

    if-eq v9, v10, :cond_6

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x47

    if-eq v9, v10, :cond_6

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x43

    if-eq v9, v10, :cond_6

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x53

    if-ne v1, v3, :cond_2

    goto :goto_2

    .line 292
    :cond_2
    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "\u786c\u5ea7\uff1a"

    .line 293
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    :cond_3
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "\u8f6f\u5ea7\uff1a"

    .line 296
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_4
    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "\u786c\u5367\uff1a"

    .line 299
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_5
    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 302
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 282
    :cond_6
    :goto_2
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "\u4e00\u7b49\u5ea7\uff1a"

    .line 283
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_7
    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "\u4e8c\u7b49\u5ea7\uff1a"

    .line 286
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    :cond_8
    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 289
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_9
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static updateHouCheTime()V
    .locals 4

    .line 320
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x1b7740

    sub-long/2addr v0, v2

    .line 322
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    sput-object v2, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;

    .line 323
    sget-object v0, Lcom/lltskb/lltskb/engine/ResultItem;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 325
    :try_start_0
    sget-object v1, Lcom/lltskb/lltskb/engine/ResultItem;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/engine/ResultItem;->houCheTime:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 327
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/lltskb/lltskb/engine/ResultItem;)I
    .locals 6

    .line 366
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mQueryType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 367
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->compareToCZ(Lcom/lltskb/lltskb/engine/ResultItem;)I

    move-result p1

    return p1

    .line 370
    :cond_0
    sget v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/4 v2, 0x4

    const-string v3, "--"

    const/4 v4, 0x1

    const/4 v5, 0x3

    if-ne v0, v4, :cond_6

    .line 371
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 373
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    :cond_1
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 376
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 377
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 379
    :cond_2
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 380
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 381
    :cond_3
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    if-nez v2, :cond_4

    .line 382
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    if-ne v2, v4, :cond_5

    .line 384
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 386
    :cond_5
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 387
    :cond_6
    sget v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    if-ne v0, v1, :cond_d

    .line 388
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 389
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 390
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 392
    :cond_7
    invoke-static {v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 393
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 394
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 396
    :cond_8
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v3

    if-ne v2, v3, :cond_a

    .line 397
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    if-ne v2, v4, :cond_9

    .line 398
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 399
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 401
    :cond_9
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    if-nez v2, :cond_a

    .line 402
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 404
    :cond_a
    iget v2, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    if-nez v2, :cond_b

    .line 405
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_b
    if-ne v2, v4, :cond_c

    .line 407
    invoke-static {v5}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v0

    .line 409
    :cond_c
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 410
    :cond_d
    sget v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_e

    const/4 v0, 0x5

    .line 411
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 412
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 414
    invoke-virtual {v1, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 415
    :cond_e
    sget v0, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_f

    const/16 v0, 0xd

    .line 416
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 417
    invoke-static {v0}, Lcom/lltskb/lltskb/engine/QueryZZ;->getIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 419
    invoke-virtual {v1, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_f
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 15
    check-cast p1, Lcom/lltskb/lltskb/engine/ResultItem;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->compareTo(Lcom/lltskb/lltskb/engine/ResultItem;)I

    move-result p1

    return p1
.end method

.method public getAuxDay()I
    .locals 1

    .line 106
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mAuxDay:I

    return v0
.end method

.method public getBackColor()I
    .locals 1

    .line 149
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mBackColor:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mText:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getIsHtml(I)Z
    .locals 3

    const/4 v0, 0x0

    if-ltz p1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mIsHtml:[B

    array-length v2, v1

    if-lt p1, v2, :cond_0

    goto :goto_0

    .line 127
    :cond_0
    aget-byte p1, v1, p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryType()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mQueryType:I

    return v0
.end method

.method public getSectionType()I
    .locals 1

    .line 95
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    return v0
.end method

.method public getText(I)Ljava/lang/String;
    .locals 2

    if-ltz p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mText:[Ljava/lang/String;

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 133
    :cond_0
    aget-object p1, v0, p1

    return-object p1

    :cond_1
    :goto_0
    const-string p1, ""

    return-object p1
.end method

.method public getTextColor()I
    .locals 1

    .line 141
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mTextColor:I

    return v0
.end method

.method public getWidth(I)I
    .locals 2

    if-ltz p1, :cond_1

    .line 153
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mWidths:[I

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 155
    :cond_0
    aget p1, v0, p1

    return p1

    :cond_1
    :goto_0
    const/16 p1, 0x1e

    return p1
.end method

.method public isFuxing()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing:Z

    return v0
.end method

.method public isIsNextStation()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mIsNextStation:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSelected:Z

    return v0
.end method

.method public setAuxDay(I)V
    .locals 0

    .line 102
    iput p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mAuxDay:I

    return-void
.end method

.method public setBackColor(I)V
    .locals 0

    .line 145
    iput p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mBackColor:I

    return-void
.end method

.method public setFuxing(Z)V
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->isFuxing:Z

    return-void
.end method

.method public setIsHtml(IB)V
    .locals 2

    if-ltz p1, :cond_1

    .line 119
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mIsHtml:[B

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 121
    :cond_0
    aput-byte p2, v0, p1

    :cond_1
    :goto_0
    return-void
.end method

.method public setIsNextStation(Z)V
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mIsNextStation:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mName:Ljava/lang/String;

    return-void
.end method

.method public setSectionType(I)V
    .locals 0

    .line 91
    iput p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSectionType:I

    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .line 55
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mSelected:Z

    return-void
.end method

.method public setText(ILjava/lang/String;)V
    .locals 2

    if-ltz p1, :cond_1

    .line 113
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mText:[Ljava/lang/String;

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 115
    :cond_0
    aput-object p2, v0, p1

    :cond_1
    :goto_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    .line 137
    iput p1, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mTextColor:I

    return-void
.end method

.method public setWidth(II)V
    .locals 2

    if-ltz p1, :cond_1

    .line 158
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mWidths:[I

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    aput p2, v0, p1

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 165
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mHead:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 168
    :cond_0
    iget v0, p0, Lcom/lltskb/lltskb/engine/ResultItem;->mQueryType:I

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 174
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResultItem;->toStringCZ()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 172
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResultItem;->toStringCC()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 170
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/ResultItem;->toStringZZ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
