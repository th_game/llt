.class public Lcom/lltskb/lltskb/engine/QueryCC;
.super Lcom/lltskb/lltskb/engine/QueryBase;
.source "QueryCC.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private db:Lcom/lltskb/lltskb/engine/ResMgr;

.field private isFuzzy:Z

.field private lx:Ljava/lang/String;

.field private mQiYe:Ljava/lang/String;

.field private mTrain:Ljava/lang/String;

.field private speed_audx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    const-class v0, Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/engine/QueryBase;-><init>(ZZ)V

    const-string p1, ""

    .line 21
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->mTrain:Ljava/lang/String;

    const/4 p1, 0x0

    .line 481
    iput p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    .line 534
    iput-boolean p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy:Z

    .line 536
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    return-void
.end method

.method public static getIndex(I)I
    .locals 2

    if-eqz p0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v1, 0x0

    packed-switch p0, :pswitch_data_0

    return v1

    :pswitch_0
    const/4 p0, 0x6

    return p0

    :pswitch_1
    const/4 p0, 0x7

    return p0

    :pswitch_2
    const/4 p0, 0x5

    return p0

    :pswitch_3
    return v1

    :pswitch_4
    return v0

    :cond_0
    const/4 p0, 0x2

    return p0

    :cond_1
    return v0

    :cond_2
    const/4 p0, 0x1

    return p0

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getSpeed(CIIIII)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 494
    iput v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    .line 495
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/QueryCC;->getMaxSpeed(C)I

    move-result p1

    if-nez p6, :cond_0

    const-string p1, "-- km/h"

    return-object p1

    :cond_0
    sub-int/2addr p5, p3

    :goto_0
    if-gez p5, :cond_1

    add-int/lit8 p4, p4, -0x1

    add-int/lit8 p5, p5, 0x3c

    goto :goto_0

    :cond_1
    sub-int/2addr p4, p2

    :goto_1
    if-gez p4, :cond_2

    add-int/lit8 p4, p4, 0x18

    goto :goto_1

    :cond_2
    int-to-float p2, p4

    int-to-float p3, p5

    const/high16 p4, 0x42700000    # 60.0f

    div-float/2addr p3, p4

    add-float/2addr p2, p3

    int-to-float p3, p6

    :goto_2
    div-float p4, p3, p2

    int-to-float p5, p1

    const/high16 p6, 0x41c00000    # 24.0f

    cmpl-float p5, p4, p5

    if-lez p5, :cond_3

    add-float/2addr p2, p6

    .line 516
    iget p4, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    add-int/lit8 p4, p4, 0x1

    iput p4, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    goto :goto_2

    :cond_3
    const/high16 p1, 0x41a00000    # 20.0f

    cmpg-float p1, p4, p1

    if-gez p1, :cond_4

    cmpl-float p1, p2, p6

    if-ltz p1, :cond_4

    sub-float/2addr p2, p6

    .line 522
    iget p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    div-float p4, p3, p2

    .line 526
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int p2, p4

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " km/h"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTimeDiff(IIII)I
    .locals 0

    sub-int/2addr p4, p2

    :goto_0
    if-gez p4, :cond_0

    add-int/lit8 p3, p3, -0x1

    add-int/lit8 p4, p4, 0x3c

    goto :goto_0

    :cond_0
    sub-int/2addr p3, p1

    :goto_1
    if-gez p3, :cond_1

    add-int/lit8 p3, p3, 0x18

    goto :goto_1

    :cond_1
    mul-int/lit8 p3, p3, 0x3c

    add-int/2addr p3, p4

    return p3
.end method

.method private loadTrain(I)Lcom/lltskb/lltskb/engine/ResultItem;
    .locals 9

    .line 42
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 44
    sget-object p1, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    const-string v0, "loadTrain info is null"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/16 v2, 0xc

    .line 47
    aget-byte v2, v0, v2

    mul-int/lit16 v2, v2, 0x80

    const/16 v3, 0xd

    aget-byte v3, v0, v3

    add-int/2addr v2, v3

    .line 48
    array-length v3, v0

    add-int/lit8 v3, v3, -0x7

    aget-byte v3, v0, v3

    mul-int/lit16 v3, v3, 0x80

    array-length v4, v0

    add-int/lit8 v4, v4, -0x6

    aget-byte v4, v0, v4

    add-int/2addr v3, v4

    .line 55
    :try_start_0
    invoke-virtual {p0, p1, v2, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->getTrainTimeDTO(III)Lcom/lltskb/lltskb/engine/TrainTimeDTO;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 58
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    move-object v4, v1

    :goto_0
    if-nez v4, :cond_1

    return-object v1

    .line 65
    :cond_1
    iget v5, v4, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/lltskb/lltskb/engine/QueryCC;->hiden:Z

    if-eqz v5, :cond_2

    return-object v1

    .line 69
    :cond_2
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v5, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {v1, v5, v7, v6}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 70
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->isFuXingHao(I)Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setFuxing(Z)V

    .line 71
    sget v5, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_DISABLE_TRAIN:I

    invoke-virtual {v1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    .line 72
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v5, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object v5

    .line 73
    invoke-virtual {v1, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    .line 74
    iget-object v8, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->isFuXingHao(I)Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setFuxing(Z)V

    .line 75
    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 77
    iget v4, v4, Lcom/lltskb/lltskb/engine/TrainTimeDTO;->outOfDateFlag:I

    const/4 v5, 0x3

    const/4 v8, 0x2

    if-eq v4, v7, :cond_5

    if-eq v4, v8, :cond_4

    if-eq v4, v5, :cond_3

    .line 88
    sget v4, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    const/4 v4, -0x1

    .line 89
    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setBackColor(I)V

    goto :goto_1

    .line 85
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " [*]"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 82
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " [+]"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 79
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " [-]"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 93
    :goto_1
    invoke-virtual {v1, v6, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 94
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 96
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 97
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    aget-byte v0, v0, v6

    invoke-virtual {v2, p1, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainLX(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v5, p1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public doAction(Ljava/lang/String;I)Ljava/util/List;
    .locals 45
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v7, p0

    .line 201
    sget-object v0, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doAction name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 206
    sget v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_DISABLE_TRAIN:I

    move/from16 v2, p2

    if-eq v2, v1, :cond_0

    .line 207
    sget v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    move v8, v1

    goto :goto_0

    :cond_0
    move v8, v2

    .line 210
    :goto_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 211
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/16 v10, 0x8

    const/4 v11, 0x1

    invoke-direct {v1, v10, v11, v11}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    const v2, -0xe7652a

    .line 212
    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setBackColor(I)V

    .line 213
    invoke-virtual {v1, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    const/4 v12, 0x0

    const-string v2, "  \u8f66\u7ad9\u540d\u79f0  "

    .line 215
    invoke-virtual {v1, v12, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v2, "     \u8f66\u6b21    "

    .line 216
    invoke-virtual {v1, v11, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v13, 0x2

    const-string v2, "  \u5230\u70b9   "

    .line 217
    invoke-virtual {v1, v13, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v14, 0x3

    const-string v2, "  \u5f00\u70b9   "

    .line 218
    invoke-virtual {v1, v14, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v15, 0x4

    const-string v2, "  \u91cc\u7a0b   "

    .line 219
    invoke-virtual {v1, v15, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v6, 0x5

    const-string v2, "        \u68c0\u7968\u53e3         "

    .line 220
    invoke-virtual {v1, v6, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v5, 0x6

    const-string v2, "   \u5929\u6570   "

    .line 221
    invoke-virtual {v1, v5, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x7

    const-string v2, "     \u901f\u5ea6     "

    .line 222
    invoke-virtual {v1, v4, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 223
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v9

    :cond_1
    const-string v3, ""

    .line 229
    iput-object v3, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mQiYe:Ljava/lang/String;

    .line 230
    iput-boolean v12, v7, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy:Z

    .line 231
    iput-object v0, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mTrain:Ljava/lang/String;

    .line 232
    iput-object v3, v7, Lcom/lltskb/lltskb/engine/QueryCC;->lx:Ljava/lang/String;

    .line 234
    iget-object v1, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndex(Ljava/lang/String;)I

    move-result v2

    .line 239
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v11

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v13, 0x42

    if-lt v1, v13, :cond_2

    .line 240
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v11

    invoke-virtual {v0, v12, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v13, v1

    goto :goto_1

    :cond_2
    move-object v13, v0

    .line 244
    :goto_1
    iget-object v1, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object v1

    .line 245
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_3

    move-object v1, v13

    .line 248
    :cond_3
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v11, "/"

    .line 249
    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 250
    iget-object v1, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getXwUnit(I)[B

    move-result-object v1

    if-gez v2, :cond_4

    .line 256
    sget-object v1, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doAction train is not found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v9

    .line 260
    :cond_4
    iget-object v10, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v10, v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainInfo(I)[B

    move-result-object v10

    if-nez v10, :cond_5

    .line 262
    sget-object v1, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doAction train info is not found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v9

    .line 266
    :cond_5
    iget-object v4, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/ResMgr;->getExtraData()Lcom/lltskb/lltskb/engine/ExtraDataMgr;

    move-result-object v4

    const/16 v18, 0x0

    if-eqz v4, :cond_7

    .line 269
    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->getExtraData(I)Lcom/lltskb/lltskb/engine/ExtraData;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 271
    iget v6, v5, Lcom/lltskb/lltskb/engine/ExtraData;->nQiYe:I

    invoke-virtual {v4, v6}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->getQiYe(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mQiYe:Ljava/lang/String;

    :cond_6
    move-object v6, v5

    goto :goto_2

    :cond_7
    move-object/from16 v6, v18

    :goto_2
    if-eqz v4, :cond_8

    .line 274
    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->isFuXingHao(I)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    goto :goto_3

    :cond_8
    const/4 v5, 0x0

    .line 277
    :goto_3
    array-length v15, v10

    .line 278
    aget-byte v14, v10, v12

    .line 279
    iget-object v12, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v12, v0, v14}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainLX(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/lltskb/lltskb/engine/QueryCC;->lx:Ljava/lang/String;

    const/4 v0, 0x3

    .line 284
    aget-byte v12, v10, v0

    and-int/lit16 v0, v12, 0xff

    const/4 v12, 0x4

    .line 285
    aget-byte v14, v10, v12

    and-int/lit16 v12, v14, 0xff

    move-object/from16 v22, v9

    const/4 v14, 0x5

    .line 286
    aget-byte v9, v10, v14

    and-int/lit16 v9, v9, 0xff

    move-object/from16 v23, v3

    const/4 v14, 0x6

    .line 287
    aget-byte v3, v10, v14

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v12, v12, 0x80

    add-int/2addr v0, v12

    mul-int/lit16 v9, v9, 0x4000

    add-int/2addr v0, v9

    move-object v9, v13

    int-to-long v12, v0

    move-object v14, v4

    int-to-long v3, v3

    const-wide/32 v24, 0x200000

    mul-long v3, v3, v24

    add-long/2addr v12, v3

    .line 289
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mMinDate:Ljava/lang/String;

    const/4 v4, 0x7

    .line 292
    aget-byte v0, v10, v4

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0x9

    const/16 v12, 0x8

    .line 293
    aget-byte v13, v10, v12

    and-int/lit16 v12, v13, 0xff

    const/16 v13, 0xa

    .line 294
    aget-byte v3, v10, v3

    and-int/lit16 v3, v3, 0xff

    .line 295
    aget-byte v13, v10, v13

    and-int/lit16 v13, v13, 0xff

    mul-int/lit16 v12, v12, 0x80

    add-int/2addr v0, v12

    mul-int/lit16 v3, v3, 0x4000

    add-int/2addr v0, v3

    move v12, v5

    int-to-long v4, v0

    move-object v0, v14

    int-to-long v13, v13

    mul-long v13, v13, v24

    add-long/2addr v4, v13

    .line 297
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mMaxDate:Ljava/lang/String;

    const/16 v13, 0xb

    .line 300
    aget-byte v4, v10, v13

    iput v4, v7, Lcom/lltskb/lltskb/engine/QueryCC;->mRuleIndex:I

    .line 306
    new-instance v14, Lcom/lltskb/lltskb/engine/Schedule;

    invoke-direct {v14}, Lcom/lltskb/lltskb/engine/Schedule;-><init>()V

    const/16 v4, 0x14

    new-array v5, v4, [B

    .line 313
    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v4

    .line 315
    invoke-virtual {v4, v13}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v13, 0xc

    .line 316
    invoke-virtual {v4, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    move/from16 v24, v3

    move/from16 v27, v13

    const/4 v3, 0x0

    const/16 v4, 0xc

    const/4 v13, 0x1

    const/16 v26, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    :goto_4
    if-ge v4, v15, :cond_1b

    move/from16 v31, v15

    .line 321
    new-instance v15, Lcom/lltskb/lltskb/engine/ResultItem;

    move-object/from16 v16, v0

    move-object/from16 v32, v6

    move/from16 v33, v13

    const/4 v0, 0x0

    const/16 v6, 0x8

    const/4 v13, 0x1

    invoke-direct {v15, v6, v13, v0}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 322
    invoke-virtual {v15, v12}, Lcom/lltskb/lltskb/engine/ResultItem;->setFuxing(Z)V

    .line 323
    invoke-virtual {v15, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    add-int/lit8 v0, v4, 0x1

    .line 324
    aget-byte v4, v10, v4

    mul-int/lit16 v4, v4, 0x80

    add-int/lit8 v13, v0, 0x1

    aget-byte v0, v10, v0

    add-int/2addr v4, v0

    .line 326
    iget-object v0, v7, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v0, v4}, Lcom/lltskb/lltskb/engine/ResMgr;->getStationName(I)Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-virtual {v15, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setName(Ljava/lang/String;)V

    add-int/lit8 v34, v13, 0x1

    .line 331
    aget-byte v13, v10, v13

    and-int/lit16 v13, v13, 0xff

    add-int/lit8 v35, v34, 0x1

    .line 332
    aget-byte v6, v10, v34

    and-int/lit16 v6, v6, 0xff

    add-int/lit8 v34, v35, 0x1

    move/from16 v37, v6

    .line 333
    aget-byte v6, v10, v35

    and-int/lit16 v6, v6, 0xff

    add-int/lit8 v35, v34, 0x1

    move/from16 v38, v6

    .line 335
    aget-byte v6, v10, v34

    mul-int/lit16 v6, v6, 0x80

    add-int/lit8 v34, v35, 0x1

    aget-byte v35, v10, v35

    add-int v6, v6, v35

    move/from16 v35, v8

    const/4 v8, 0x0

    .line 338
    invoke-virtual {v15, v8, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 340
    invoke-virtual {v7, v1, v6, v11}, Lcom/lltskb/lltskb/engine/QueryCC;->get_exact_train_name([BI[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    const/4 v8, 0x1

    .line 342
    invoke-virtual {v15, v8, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    goto :goto_5

    :cond_9
    const/4 v8, 0x1

    .line 344
    invoke-virtual {v15, v8, v9}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    move-object v0, v9

    :goto_5
    if-ge v13, v3, :cond_a

    add-int/lit8 v26, v26, 0x1

    :cond_a
    move/from16 v8, v26

    .line 353
    invoke-virtual {v15, v8}, Lcom/lltskb/lltskb/engine/ResultItem;->setAuxDay(I)V

    .line 355
    iget-object v3, v7, Lcom/lltskb/lltskb/engine/QueryCC;->strDate:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/lltskb/lltskb/utils/LLTUtils;->getDateAfter(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v14, v2, v4, v3, v5}, Lcom/lltskb/lltskb/engine/Schedule;->get_schedule(III[B)I

    const/4 v3, 0x0

    .line 356
    aget-byte v26, v5, v3

    if-nez v26, :cond_b

    const/16 v3, 0x9

    .line 357
    aget-byte v3, v5, v3

    and-int/lit16 v3, v3, 0xff

    const/16 v13, 0xa

    .line 358
    aget-byte v13, v5, v13

    and-int/lit16 v13, v13, 0xff

    move-object/from16 v26, v1

    const/16 v25, 0xb

    .line 359
    aget-byte v1, v5, v25

    and-int/lit16 v1, v1, 0xff

    move/from16 v38, v1

    move/from16 v1, v33

    move/from16 v33, v2

    const/4 v2, 0x1

    move/from16 v44, v13

    move v13, v3

    move/from16 v3, v44

    goto :goto_6

    :cond_b
    move-object/from16 v26, v1

    const/16 v25, 0xb

    move/from16 v1, v33

    move/from16 v3, v37

    move/from16 v33, v2

    const/4 v2, 0x1

    :goto_6
    if-ne v1, v2, :cond_c

    const-string v2, "--:--"

    move-object/from16 v37, v5

    const/4 v5, 0x2

    .line 368
    invoke-virtual {v15, v5, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    goto :goto_7

    :cond_c
    move-object/from16 v37, v5

    const/4 v5, 0x2

    .line 370
    invoke-virtual {v7, v13, v3}, Lcom/lltskb/lltskb/engine/QueryCC;->formatTime(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v5, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    :goto_7
    add-int v2, v38, v3

    move v5, v2

    move/from16 v39, v3

    move v2, v13

    :goto_8
    const/16 v3, 0x3c

    if-lt v5, v3, :cond_d

    add-int/lit8 v5, v5, -0x3c

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_d
    move v3, v2

    :goto_9
    const/16 v2, 0x18

    if-lt v3, v2, :cond_e

    add-int/lit8 v3, v3, -0x18

    goto :goto_9

    :cond_e
    if-gtz v38, :cond_10

    const/4 v2, 0x1

    if-ne v1, v2, :cond_f

    goto :goto_a

    :cond_f
    const-string v2, "--:--"

    move-object/from16 v40, v9

    const/4 v9, 0x3

    .line 388
    invoke-virtual {v15, v9, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    goto :goto_b

    :cond_10
    :goto_a
    move-object/from16 v40, v9

    const/4 v9, 0x3

    .line 386
    invoke-virtual {v7, v3, v5}, Lcom/lltskb/lltskb/engine/QueryCC;->formatTime(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v9, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 391
    :goto_b
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x4

    invoke-virtual {v15, v9, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    if-eqz v16, :cond_13

    .line 396
    iget-object v2, v7, Lcom/lltskb/lltskb/engine/QueryCC;->strDate:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v2, v9}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move-object/from16 v10, v32

    invoke-virtual {v9, v10, v4, v2}, Lcom/lltskb/lltskb/engine/ExtraDataMgr;->getHcdd(Lcom/lltskb/lltskb/engine/ExtraData;II)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x5

    .line 398
    invoke-virtual {v15, v4, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    if-eqz v2, :cond_11

    .line 399
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v20, v3

    const/4 v3, 0x6

    if-le v2, v3, :cond_12

    const/4 v2, 0x1

    .line 400
    invoke-virtual {v15, v4, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setIsHtml(IB)V

    goto :goto_c

    :cond_11
    move/from16 v20, v3

    :cond_12
    :goto_c
    move-object/from16 v3, v23

    goto :goto_d

    :cond_13
    move/from16 v20, v3

    move-object/from16 v9, v16

    move-object/from16 v3, v23

    const/4 v4, 0x5

    move-object/from16 v16, v10

    move-object/from16 v10, v32

    .line 403
    invoke-virtual {v15, v4, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    :goto_d
    add-int/lit8 v23, v1, 0x1

    if-nez v8, :cond_14

    const-string v1, "\u5f53\u5929"

    const/4 v2, 0x6

    .line 409
    invoke-virtual {v15, v2, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    move-object/from16 v41, v3

    const/4 v3, 0x0

    goto :goto_e

    :cond_14
    const/4 v2, 0x6

    .line 411
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    add-int/lit8 v4, v8, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v41, v3

    const/4 v3, 0x0

    aput-object v4, v2, v3

    const-string v4, "\u7b2c%d\u5929"

    invoke-static {v1, v4, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    .line 412
    invoke-virtual {v15, v2, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 416
    :goto_e
    iput v3, v7, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    .line 417
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v0, 0x4b

    const/16 v1, 0x4b

    goto :goto_f

    :cond_15
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v1, v0

    :goto_f
    sub-int v32, v6, v30

    move-object/from16 v0, p0

    const/16 v42, 0x6

    move/from16 v2, v28

    move/from16 v4, v24

    move-object/from16 v24, v9

    move/from16 v9, v20

    move/from16 v20, v39

    move-object/from16 v39, v41

    move/from16 v3, v29

    move-object/from16 v19, v10

    move-object/from16 v17, v24

    const/16 v41, 0x5

    move v10, v4

    move-object/from16 v24, v11

    const/4 v11, 0x7

    move v4, v13

    move/from16 v43, v5

    move/from16 v5, v20

    move-object/from16 v20, v19

    const/16 v36, 0x8

    move/from16 v19, v6

    move/from16 v6, v32

    .line 418
    invoke-direct/range {v0 .. v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getSpeed(CIIIII)Ljava/lang/String;

    move-result-object v0

    .line 419
    iget v1, v7, Lcom/lltskb/lltskb/engine/QueryCC;->speed_audx:I

    add-int/2addr v1, v8

    .line 420
    invoke-virtual {v15, v11, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    if-lez v19, :cond_19

    move/from16 v0, v28

    move/from16 v3, v29

    move/from16 v2, v43

    .line 424
    invoke-direct {v7, v0, v3, v9, v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getTimeDiff(IIII)I

    move-result v4

    move/from16 v5, v27

    .line 425
    invoke-direct {v7, v10, v5, v9, v2}, Lcom/lltskb/lltskb/engine/QueryCC;->getTimeDiff(IIII)I

    move-result v6

    if-ge v6, v4, :cond_16

    const/4 v4, 0x1

    goto :goto_10

    :cond_16
    const/4 v4, 0x0

    .line 427
    :goto_10
    invoke-virtual {v15, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setIsNextStation(Z)V

    .line 428
    invoke-virtual {v15}, Lcom/lltskb/lltskb/engine/ResultItem;->isIsNextStation()Z

    move-result v4

    if-eqz v4, :cond_18

    sub-int v6, v6, v38

    const/16 v4, 0xd

    .line 431
    invoke-static {v4}, Lcom/lltskb/lltskb/engine/QueryCC;->getIndex(I)I

    move-result v4

    if-gtz v6, :cond_17

    .line 433
    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v8, 0x1

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/16 v21, 0x0

    aput-object v8, v11, v21

    const-string v8, "\u5df2\u5230\u7ad9<br/>%d \u516c\u91cc"

    invoke-static {v6, v8, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v4, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    move/from16 v28, v0

    const/4 v11, 0x1

    goto :goto_11

    :cond_17
    const/16 v21, 0x0

    .line 435
    sget-object v8, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    move/from16 v28, v0

    const/4 v11, 0x2

    new-array v0, v11, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v21

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v11, 0x1

    aput-object v6, v0, v11

    const-string v6, "%d\u5206\u949f<br/>%d \u516c\u91cc"

    invoke-static {v8, v6, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v4, v0}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 437
    :goto_11
    invoke-virtual {v15, v4, v11}, Lcom/lltskb/lltskb/engine/ResultItem;->setIsHtml(IB)V

    goto :goto_13

    :cond_18
    move/from16 v28, v0

    goto :goto_12

    :cond_19
    move/from16 v5, v27

    move/from16 v3, v29

    move/from16 v2, v43

    :goto_12
    const/4 v11, 0x1

    const/16 v21, 0x0

    .line 445
    :goto_13
    aget-byte v0, v37, v21

    if-eq v0, v11, :cond_1a

    move-object/from16 v0, v22

    .line 446
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v29, v2

    move/from16 v28, v9

    move/from16 v30, v19

    goto :goto_14

    :cond_1a
    move-object/from16 v0, v22

    add-int/lit8 v23, v23, -0x1

    move/from16 v29, v3

    :goto_14
    move-object/from16 v22, v0

    move/from16 v27, v5

    move v3, v13

    move-object/from16 v0, v17

    move-object/from16 v6, v20

    move/from16 v13, v23

    move-object/from16 v11, v24

    move/from16 v15, v31

    move/from16 v2, v33

    move/from16 v4, v34

    move/from16 v8, v35

    move-object/from16 v5, v37

    move-object/from16 v23, v39

    move-object/from16 v9, v40

    move/from16 v24, v10

    move-object/from16 v10, v16

    move-object/from16 v44, v26

    move/from16 v26, v1

    move-object/from16 v1, v44

    goto/16 :goto_4

    :cond_1b
    move-object/from16 v0, v22

    return-object v0
.end method

.method public doActionFuzzy(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation

    const-string v0, ""

    .line 107
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->mQiYe:Ljava/lang/String;

    .line 108
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->mTrain:Ljava/lang/String;

    .line 109
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->lx:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 111
    new-instance v1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-direct {v1, v3, v2, v2}, Lcom/lltskb/lltskb/engine/ResultItem;-><init>(IIZ)V

    .line 112
    sget v3, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/engine/ResultItem;->setTextColor(I)V

    const/4 v3, 0x0

    const-string v4, "  \u8f66\u6b21\u540d\u79f0    "

    .line 113
    invoke-virtual {v1, v3, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const-string v4, "  \u59cb\u53d1\u7ad9   "

    .line 114
    invoke-virtual {v1, v2, v4}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v4, 0x2

    const-string v5, "  \u7ec8\u5230\u7ad9   "

    .line 115
    invoke-virtual {v1, v4, v5}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    const/4 v5, 0x3

    const-string v6, " \u8f66\u6b21\u7c7b\u578b     "

    .line 116
    invoke-virtual {v1, v5, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->setText(ILjava/lang/String;)V

    .line 117
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 120
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 124
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v5, 0x41

    if-lt v1, v5, :cond_1

    const/16 v5, 0x5a

    if-gt v1, v5, :cond_1

    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v5, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndex(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_3

    .line 129
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v1, "/"

    .line 132
    invoke-static {p1, v1}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 133
    array-length v5, v1

    if-lez v5, :cond_2

    .line 134
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    aget-object v1, v1, v3

    invoke-virtual {v5, v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainIndexFuzzy(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    if-eqz v1, :cond_8

    .line 140
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_4

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    .line 147
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_6

    .line 148
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 149
    invoke-direct {p0, v5}, Lcom/lltskb/lltskb/engine/QueryCC;->loadTrain(I)Lcom/lltskb/lltskb/engine/ResultItem;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 151
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move p1, v3

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 156
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_7

    .line 157
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 158
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 159
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/QueryCC;->db:Lcom/lltskb/lltskb/engine/ResMgr;

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ResMgr;->getTrainName(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultItem;->getTextColor()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/engine/QueryCC;->doAction(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 162
    :cond_7
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy:Z

    return-object v0

    .line 141
    :cond_8
    :goto_2
    sget-object v1, Lcom/lltskb/lltskb/engine/QueryCC;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doActionFuzzy train is not found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getLX()Ljava/lang/String;
    .locals 1

    .line 531
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->lx:Ljava/lang/String;

    return-object v0
.end method

.method public getQiYe()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->mQiYe:Ljava/lang/String;

    return-object v0
.end method

.method getQueryType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getTrainName()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->mTrain:Ljava/lang/String;

    return-object v0
.end method

.method public isFuzzy()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy:Z

    return v0
.end method
