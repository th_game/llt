.class public Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;
.super Landroid/os/AsyncTask;
.source "SearchTicketTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchTicketTask"


# instance fields
.field private mBeginTime:I

.field private mEndTime:I

.field private mIsAllTime:Z

.field private mIsAllTrainType:Z

.field private mIsCheckTrainNo:Z

.field private mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

.field private mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

.field private mOrderSeats:[Ljava/lang/String;

.field private mPaused:Z

.field private mProgress:I

.field private mSink:Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;)V
    .locals 1

    .line 63
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mProgress:I

    .line 25
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    .line 60
    new-instance v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-direct {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    .line 64
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;

    return-void
.end method

.method private checkTicketAndOrder(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;Ljava/util/Vector;)I
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;",
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;",
            ">;)I"
        }
    .end annotation

    move-object/from16 v1, p0

    if-eqz p2, :cond_19

    .line 222
    invoke-virtual/range {p2 .. p2}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_9

    .line 233
    :cond_0
    iget-boolean v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsCheckTrainNo:Z

    const-string v2, ""

    const-string v3, ","

    if-eqz v0, :cond_2

    .line 234
    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainNo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainNo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v2, v0

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    :cond_2
    move-object v4, v2

    .line 243
    :goto_1
    iget-boolean v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsCheckTrainNo:Z

    .line 247
    invoke-virtual/range {p2 .. p2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    move v8, v0

    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x1

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_14

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 250
    iget-object v12, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v13

    const v14, 0x7f0d0298

    invoke-virtual {v13, v14}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 251
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v2, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->buttonTextInfo:Ljava/lang/String;

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    return v6

    .line 255
    :cond_4
    iget-boolean v12, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsCheckTrainNo:Z

    if-eqz v12, :cond_6

    if-eqz v2, :cond_5

    .line 256
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 257
    invoke-virtual {v4, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_6

    goto :goto_2

    .line 265
    :cond_6
    iget-boolean v8, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsAllTrainType:Z

    if-nez v8, :cond_9

    .line 267
    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v8

    array-length v12, v8

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_3
    if-ge v13, v12, :cond_8

    aget-object v15, v8, v13

    .line 268
    iget-object v6, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-static {v6, v15}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v14, 0x1

    :cond_7
    add-int/lit8 v13, v13, 0x1

    const/4 v6, 0x1

    goto :goto_3

    :cond_8
    if-nez v14, :cond_9

    const/4 v6, 0x1

    const/4 v8, 0x0

    goto :goto_2

    .line 279
    :cond_9
    iget-boolean v6, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsAllTime:Z

    if-nez v6, :cond_b

    .line 280
    iget-object v6, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v8, ":"

    invoke-static {v6, v8}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 281
    array-length v8, v6

    if-lez v8, :cond_b

    .line 282
    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 283
    iget v8, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mBeginTime:I

    if-lt v6, v8, :cond_a

    iget v8, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mEndTime:I

    if-le v6, v8, :cond_b

    :cond_a
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    goto/16 :goto_2

    .line 291
    :cond_b
    iget-object v6, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderSeats:[Ljava/lang/String;

    array-length v8, v6

    const/4 v9, 0x0

    :goto_4
    if-ge v9, v8, :cond_13

    aget-object v10, v6, v9

    .line 293
    invoke-virtual {v11, v10}, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->getSeats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "--"

    .line 295
    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    goto/16 :goto_8

    :cond_c
    const-string v0, "\u65e0"

    .line 297
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_5
    const/4 v0, 0x1

    goto/16 :goto_8

    :cond_d
    const-string v0, "*"

    .line 299
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-wide/16 v12, 0x1f4

    .line 303
    :try_start_0
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    move-object v10, v0

    .line 305
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_5

    .line 309
    :cond_e
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "\u4e58\u5750\u533a\u95f4:&nbsp;<font color=\"#303f9f\"><big><b>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</b></big></font> \u81f3 <font color=\"#303f9f\"><big><b>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</b></big></font><br/>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v15, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "\u4e58\u5750\u8f66\u6b21:&nbsp;<font color=\"#303f9f\"><big><b>"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v15, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "</b></big></font> \u6b21<br/>\u4e58\u5750\u65e5\u671f:&nbsp;<font color=\"#ff5722\"><big><b>"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v15, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->take_date:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v14, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "\u51fa\u53d1\u65f6\u95f4:&nbsp;<font color=\"#303f9f\"><b>"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</b></font><br/>"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v15, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "\u5230\u8fbe\u65f6\u95f4:&nbsp;<font color=\"#303f9f\"><b>"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v15, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v15, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "\u5386\u65f6:&nbsp;<font color=\"#303f9f\"><b>"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v15, v11, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->lishi:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 319
    invoke-static {v12}, Lcom/lltskb/lltskb/utils/StringUtils;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v14, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "\u6709&nbsp;<font color=\"#303f9f\"><b>"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v10}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</b></font>&nbsp;\u7968 <font color=\"#4caf50\">"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</font>&nbsp;\u5f20!\n"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 321
    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderPerson()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 325
    array-length v13, v0

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_6
    if-ge v14, v13, :cond_10

    aget-object v16, v0, v14

    .line 326
    invoke-static/range {v16 .. v16}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_f

    add-int/lit8 v15, v15, 0x1

    :cond_f
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 330
    :cond_10
    invoke-static {v12, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    if-ge v0, v15, :cond_12

    const-wide/16 v13, 0xc8

    .line 333
    :try_start_1
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    :catch_1
    move-exception v0

    move-object v10, v0

    .line 336
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 339
    :goto_7
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v10, 0x7f0d01fd

    invoke-virtual {v0, v10}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 341
    iget-object v10, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    aput-object v12, v14, v7

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v15, 0x1

    aput-object v12, v14, v15

    invoke-static {v13, v0, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    goto/16 :goto_5

    :goto_8
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 346
    :cond_11
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d01fa

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v10}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getSeatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v0, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 350
    :cond_12
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iput-object v11, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicketDTO:Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 351
    iput-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mSeat:Ljava/lang/String;

    const/16 v0, 0xc

    return v0

    :cond_13
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_14
    if-nez v0, :cond_15

    .line 358
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d00fe

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    const/16 v0, 0xf

    return v0

    :cond_15
    if-eqz v8, :cond_16

    .line 363
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d00ec

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 364
    iget-object v2, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    const/16 v0, 0x12

    return v0

    :cond_16
    if-eqz v9, :cond_17

    .line 369
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d00fd

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    const/16 v0, 0x13

    return v0

    :cond_17
    if-eqz v10, :cond_18

    .line 375
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d00fc

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    const/16 v0, 0x14

    return v0

    :cond_18
    return v7

    .line 223
    :cond_19
    :goto_9
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 224
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d01ee

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    :cond_1a
    const/16 v0, 0x10

    return v0
.end method

.method private init(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V
    .locals 6

    .line 83
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 84
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iput-object p1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 85
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    const-string v3, "QB"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsAllTrainType:Z

    .line 88
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    const-string v3, "00:00--24:00"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsAllTime:Z

    .line 89
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    const-string v3, "--"

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 90
    array-length v3, v0

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    .line 91
    aget-object v3, v0, v2

    const-string v4, ":"

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 92
    array-length v5, v3

    if-le v5, v1, :cond_1

    .line 93
    aget-object v3, v3, v2

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mBeginTime:I

    .line 96
    :cond_1
    aget-object v0, v0, v1

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 97
    array-length v3, v0

    if-le v3, v1, :cond_2

    .line 98
    aget-object v0, v0, v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mEndTime:I

    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainNo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mIsCheckTrainNo:Z

    .line 104
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderSeat()Ljava/lang/String;

    move-result-object p1

    const-string v0, ","

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 106
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderSeats:[Ljava/lang/String;

    .line 107
    array-length v0, p1

    sub-int/2addr v0, v1

    :goto_3
    if-ltz v0, :cond_5

    .line 108
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderSeats:[Ljava/lang/String;

    array-length v4, p1

    sub-int/2addr v4, v0

    sub-int/2addr v4, v1

    aget-object v5, p1, v0

    aput-object v5, v3, v4

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 112
    :cond_5
    iput-boolean v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)Ljava/lang/Object;
    .locals 10

    const/4 v0, 0x0

    .line 132
    aget-object v1, p1, v0

    invoke-direct {p0, v1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->init(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V

    const-string v1, "SearchTicketTask"

    const-string v2, "doInBackGround"

    .line 133
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengers(Z)Z
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 139
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 144
    :goto_0
    new-instance v1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;

    invoke-direct {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;-><init>()V

    .line 145
    aget-object p1, p1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 147
    :goto_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->isCancelled()Z

    move-result v5

    const/4 v6, 0x0

    if-nez v5, :cond_7

    .line 149
    iget-boolean v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    if-eqz v5, :cond_0

    .line 151
    :try_start_1
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v5

    .line 153
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 159
    :cond_0
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const-string v7, ""

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 160
    iput v0, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    .line 162
    :try_start_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v8

    .line 163
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v9

    .line 162
    invoke-virtual {v1, v5, v7, v8, v9}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 164
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorMsg()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    .line 165
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorCode()I

    move-result v5

    const/4 v7, -0x7

    if-ne v5, v7, :cond_1

    .line 166
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iput v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    goto :goto_2

    .line 169
    :cond_1
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getErrorCode()I

    move-result v7

    iput v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    const/4 v5, 0x1

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v5, 0x0

    .line 174
    :goto_3
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object v7
    :try_end_2
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    :catch_2
    move-exception v5

    .line 177
    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 178
    iget-object v7, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const/4 v8, -0x3

    iput v8, v7, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    .line 179
    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    move-object v7, v6

    const/4 v5, 0x1

    :goto_4
    add-int/lit8 v3, v3, 0x1

    if-eqz v5, :cond_3

    .line 184
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getIsIgnoreNetworkErr()Z

    move-result v5

    if-eqz v5, :cond_3

    new-array v5, v2, [Ljava/lang/Integer;

    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->publishProgress([Ljava/lang/Object;)V

    .line 187
    :try_start_3
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v5

    .line 189
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    .line 194
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_4

    return-object v6

    .line 195
    :cond_4
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-direct {p0, p1, v7}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->checkTicketAndOrder(Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;Ljava/util/Vector;)I

    move-result v7

    iput v7, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    .line 196
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget v5, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    if-eq v5, v2, :cond_7

    const/16 v7, 0xc

    if-eq v5, v7, :cond_7

    const/16 v7, 0xf

    if-eq v5, v7, :cond_7

    packed-switch v5, :pswitch_data_0

    .line 205
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget v5, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    const/16 v7, 0x10

    if-eq v5, v7, :cond_5

    const/4 v4, 0x0

    .line 207
    :cond_5
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    iget v5, v5, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mStatus:I

    if-ne v5, v7, :cond_6

    const/4 v5, 0x2

    if-le v3, v5, :cond_6

    if-eqz v4, :cond_6

    return-object v6

    :cond_6
    new-array v5, v2, [Ljava/lang/Integer;

    .line 211
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->publishProgress([Ljava/lang/Object;)V

    .line 213
    :try_start_4
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getQueryFreq()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v5

    .line 215
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    :pswitch_0
    return-object v6

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, [Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->doInBackground([Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getOrderConfig()Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-object v0
.end method

.method public getOrderParams()Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .line 33
    iget v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mProgress:I

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .line 127
    iget-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    return v0
.end method

.method protected onCancelled(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;->onTicketSearched(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V

    .line 56
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 39
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    .line 44
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 46
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/lltskb/lltskb/engine/tasks/ISearchTicketSink;->onSearchTicketProgress(I)V

    .line 48
    :cond_0
    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mProgress:I

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public pause()V
    .locals 2

    const-string v0, "SearchTicketTask"

    const-string v1, "pause"

    .line 116
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 117
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    .line 118
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mOrderParams:Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    const-string v1, "\u5df2\u6682\u505c"

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mMsg:Ljava/lang/String;

    return-void
.end method

.method public resume()V
    .locals 2

    const-string v0, "SearchTicketTask"

    const-string v1, "resume"

    .line 122
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 123
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTicketTask;->mPaused:Z

    return-void
.end method
