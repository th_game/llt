.class public Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;
.super Landroid/os/AsyncTask;
.source "CheckLoginStatusTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

.field private mNoLogin:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 67
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object p1

    const/4 v2, 0x1

    .line 68
    invoke-interface {p1, v2}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result p1

    if-nez p1, :cond_3

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v0, 0x3e8

    cmp-long p1, v2, v0

    if-gez p1, :cond_0

    sub-long/2addr v0, v2

    .line 72
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 74
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 78
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_1

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mNoLogin:Ljava/lang/String;

    return-object p1

    :cond_2
    :goto_1
    const-string p1, ""

    return-object p1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    if-nez v0, :cond_0

    return-void

    .line 53
    :cond_0
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 56
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 57
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 58
    :goto_0
    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->onSignStatus(Z)V

    .line 61
    :cond_3
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    if-nez v0, :cond_0

    return-void

    .line 33
    :cond_0
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d009d

    const/high16 v2, -0x1000000

    new-instance v3, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$1;-><init>(Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 43
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d01e7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckLoginStatusTask;->mNoLogin:Ljava/lang/String;

    .line 45
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
