.class public Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;
.super Landroid/os/AsyncTask;
.source "SubmitOrderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SubmitOrderTask"


# instance fields
.field private mErrorMsg:Ljava/lang/String;

.field private mSink:Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;

.field private mStatus:I


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;)V
    .locals 1

    .line 56
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, -0x1

    .line 32
    iput v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 57
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;

    return-void
.end method

.method private doOrderTicket(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    const-string v2, "0X00"

    .line 91
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v3

    .line 93
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->isAsync()Z

    move-result v4

    const/4 v5, -0x1

    .line 97
    :try_start_0
    iget-object v6, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getRangeCode()Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v7, ""

    if-nez v6, :cond_0

    move-object v6, v7

    .line 101
    :cond_0
    :try_start_1
    iget-object v8, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v8}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getChooseSeats()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    move-object v8, v7

    .line 105
    :cond_1
    iget-object v9, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v9}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTourFlag()Ljava/lang/String;

    move-result-object v9

    const-string v10, "00"

    .line 107
    iget-object v11, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v11}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getPurpose()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    goto :goto_0

    :cond_2
    move-object v2, v10

    .line 155
    :goto_0
    iget-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    invoke-static {v10}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    if-nez v10, :cond_5

    const v7, 0x7f0d02a6

    .line 156
    invoke-virtual {v1, v7}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 157
    iget-object v10, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    const/16 v13, 0x2c

    invoke-virtual {v10, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    if-lez v10, :cond_3

    .line 159
    sget-object v13, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v14, v11, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    invoke-virtual {v0, v12, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v14, v12

    invoke-static {v13, v7, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v10, "0"

    .line 161
    iget-object v13, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 162
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v10, v11, [Ljava/lang/Object;

    const-string v13, "\u5f88\u591a"

    aput-object v13, v10, v12

    invoke-static {v0, v7, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 164
    :cond_4
    sget-object v10, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    new-array v13, v11, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;->mTicket:Ljava/lang/String;

    aput-object v0, v13, v12

    invoke-static {v10, v7, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 167
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_5
    new-array v0, v11, [Ljava/lang/String;

    .line 170
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v13, 0x7f0d00b7

    invoke-virtual {v1, v13}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v12

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->publishProgress([Ljava/lang/Object;)V

    const/4 v0, -0x1

    const/4 v10, 0x0

    .line 174
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isCancelled()Z

    move-result v13

    const/4 v14, -0x3

    const/4 v15, 0x5

    if-nez v13, :cond_9

    if-eqz v0, :cond_9

    if-ge v10, v15, :cond_9

    if-nez v4, :cond_6

    .line 176
    invoke-virtual {v3, v2, v8, v6}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->confirmSingle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 178
    :cond_6
    invoke-virtual {v3, v2, v8, v6}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->confirmSingleForQueue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :goto_3
    const/4 v13, -0x5

    if-ne v0, v13, :cond_7

    .line 182
    iput v13, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 183
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    return-void

    :cond_7
    if-eqz v0, :cond_8

    .line 186
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    .line 187
    iget-object v13, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    invoke-direct {v1, v13}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isBookFailure(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 188
    iput v14, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    return-void

    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 195
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isCancelled()Z

    move-result v0

    const/4 v2, 0x0

    const/4 v6, -0x4

    if-eqz v0, :cond_a

    .line 196
    iput v6, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 197
    iput-object v2, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    return-void

    :cond_a
    const/4 v8, -0x2

    if-ne v10, v15, :cond_b

    .line 202
    iput v8, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 203
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    return-void

    :cond_b
    if-eqz v4, :cond_11

    new-array v0, v11, [Ljava/lang/String;

    .line 207
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v13, 0x7f0d0165

    invoke-virtual {v1, v13}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v12

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->publishProgress([Ljava/lang/Object;)V

    .line 209
    invoke-virtual {v3, v9}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->queryOrderWaitTime(Ljava/lang/String;)I

    move-result v0

    .line 210
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isCancelled()Z

    move-result v10

    if-nez v10, :cond_f

    if-ne v0, v8, :cond_f

    new-array v0, v11, [Ljava/lang/String;

    .line 211
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v12

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->publishProgress([Ljava/lang/Object;)V

    .line 213
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    const-string v0, "SubmitOrderTask"

    .line 214
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "queryOrderWaitTime ="

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isBookFailure(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 216
    iput v14, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I
    :try_end_1
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    .line 220
    :cond_c
    :try_start_2
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getDisplayTime()J

    move-result-wide v15

    const-wide/16 v17, 0xa

    cmp-long v0, v15, v17

    if-lez v0, :cond_d

    move-wide/from16 v15, v17

    :cond_d
    const-wide/16 v17, 0x0

    cmp-long v0, v15, v17

    if-gtz v0, :cond_e

    const-wide/16 v15, 0x1

    :cond_e
    const-wide/16 v17, 0x3e8

    mul-long v15, v15, v17

    .line 223
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_5

    :catch_0
    move-exception v0

    .line 225
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 228
    :goto_5
    invoke-virtual {v3, v9}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->queryOrderWaitTime(Ljava/lang/String;)I

    move-result v0

    goto :goto_4

    .line 231
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->isCancelled()Z

    move-result v9

    if-eqz v9, :cond_10

    .line 232
    iput v6, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 233
    iput-object v2, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    return-void

    :cond_10
    if-eqz v0, :cond_11

    .line 238
    iput v8, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 239
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    return-void

    :cond_11
    new-array v0, v11, [Ljava/lang/String;

    .line 243
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v6, 0x7f0d0243

    invoke-virtual {v1, v6}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v12

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->publishProgress([Ljava/lang/Object;)V

    if-eqz v4, :cond_12

    .line 246
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->resultOrderForDcQueue()I

    .line 249
    :cond_12
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->queryPayOrder()I

    .line 250
    iput v12, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 251
    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderId()Ljava/lang/String;

    move-result-object v0

    if-eqz v4, :cond_13

    .line 252
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    const-string v2, "null"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 253
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f0d0200

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v11, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getOrderId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v12

    invoke-static {v0, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    goto :goto_6

    :cond_13
    const v0, 0x7f0d0201

    .line 255
    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;
    :try_end_3
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    .line 259
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 260
    iput v5, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    .line 261
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    :goto_6
    return-void
.end method

.method private isBookFailure(Ljava/lang/String;)Z
    .locals 6

    .line 71
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 76
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d01b2

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 77
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d01b3

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const/4 v2, 0x2

    .line 78
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d01b4

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x3

    .line 79
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v4

    const v5, 0x7f0d01b5

    invoke-virtual {v4, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    .line 82
    array-length v2, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, v0, v4

    .line 83
    invoke-static {p1, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    return v3

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, [Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->doInBackground([Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 66
    aget-object p1, p1, v0

    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->doOrderTicket(Lcom/lltskb/lltskb/engine/online/dto/OrderParameters;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 61
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3

    .line 42
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;

    if-eqz v0, :cond_0

    .line 43
    iget v1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mStatus:I

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mErrorMsg:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;->onSubmitOrderResult(ILjava/lang/String;)V

    .line 45
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 37
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .locals 2

    .line 25
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderTask;->mSink:Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 27
    aget-object p1, p1, v1

    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/engine/tasks/ISubmitOrderSink;->publishProgress(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
