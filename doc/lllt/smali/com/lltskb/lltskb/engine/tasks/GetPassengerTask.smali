.class public Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;
.super Landroid/os/AsyncTask;
.source "GetPassengerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

.field private selectedString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->doInBackground([Ljava/lang/Boolean;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Boolean;)[Ljava/lang/String;
    .locals 3

    .line 83
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getPassengers()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    .line 84
    aget-object v2, p1, v1

    .line 86
    :try_start_0
    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengers(Z)Z

    goto :goto_1

    .line 87
    :cond_1
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->queryPassengers(Z)Z
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 92
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 2

    .line 64
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->selectedString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->selectPassenger(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    if-nez v0, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 70
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->onQueryCompleted(I)V

    .line 73
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .line 29
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->get()Lcom/lltskb/lltskb/engine/online/PassengerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/PassengerModel;->getSelectedPassenger()Ljava/util/Vector;

    move-result-object v0

    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 31
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 32
    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;

    .line 33
    iget-object v4, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v4, v4, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    iget-boolean v4, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->is_child_ticket:Z

    if-eqz v4, :cond_0

    const-string v4, "[\u7ae5]"

    .line 35
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v4, "-"

    .line 36
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/SelectedPassengerDTO;->passenger:Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/online/dto/PassengerDTO;->passenger_id_no:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    .line 37
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->selectedString:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    if-nez v0, :cond_2

    return-void

    .line 45
    :cond_2
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    .line 48
    :cond_3
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 49
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;->mListener:Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;

    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$Listener;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0228

    const/high16 v2, -0x1000000

    new-instance v3, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$1;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask$1;-><init>(Lcom/lltskb/lltskb/engine/tasks/GetPassengerTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 58
    :cond_4
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
