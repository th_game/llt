.class public Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;
.super Landroid/os/AsyncTask;
.source "GetPassCodeTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private imageView:Landroid/widget/ImageView;

.field private progBar:Landroid/widget/ProgressBar;

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    .line 27
    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    .line 32
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    .line 33
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    .line 34
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->progBar:Landroid/widget/ProgressBar;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .line 39
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "module=passenger&rand=randp&"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew.do?"

    .line 43
    :try_start_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->get()Lcom/lltskb/lltskb/engine/online/HttpsClient;

    move-result-object v1

    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/online/HttpsClient;->getCaptcha(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 46
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 54
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 58
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->progBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez p1, :cond_2

    .line 61
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    const v0, 0x7f0800c8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    .line 64
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    const/4 v0, 0x0

    const-string v1, "\u63d0\u793a"

    const-string v2, "\u83b7\u53d6\u9a8c\u8bc1\u7801\u5931\u8d25\uff0c\u8bf7\u68c0\u6d4b\u7f51\u7edc\u662f\u5426\u6b63\u5e38\uff01"

    invoke-static {p1, v1, v2, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .line 72
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->activity:Landroid/app/Activity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "\u83b7\u53d6\u9a8c\u8bc1\u7801"

    const-string v4, "\u6b63\u5728\u83b7\u53d6\u9a8c\u8bc1\u7801..."

    invoke-static {v0, v3, v4, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->progressDialog:Landroid/app/ProgressDialog;

    const-string v0, "GetRandCodeTask.onPreExecute"

    const-string v1, "\u5f00\u59cb\u83b7\u53d6\u9a8c\u8bc1\u7801..."

    .line 75
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->progBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1

    const-string p1, "GetRandCodeTask.onProgressUpdate"

    const-string v0, "\u6b63\u5728\u83b7\u53d6\u9a8c\u8bc1\u7801..."

    .line 82
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/GetPassCodeTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
