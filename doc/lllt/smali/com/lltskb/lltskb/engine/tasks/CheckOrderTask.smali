.class public Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;
.super Landroid/os/AsyncTask;
.source "CheckOrderTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private listener:Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;

.field private purpose:Ljava/lang/String;

.field private tourFlag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->purpose:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->tourFlag:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->listener:Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string p1, "0X00"

    .line 59
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v0

    .line 62
    :try_start_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->initDc()I

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 66
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getPassengerDTOs()I

    .line 67
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getPassCodeNew()I
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_2

    const-wide/16 v1, 0x3e8

    .line 70
    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v3

    .line 72
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_0
    const-string v3, "00"

    .line 76
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->purpose:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v3

    :goto_1
    const-string v3, ""

    .line 80
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->tourFlag:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->checkOrderInfo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    .line 82
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 85
    :cond_2
    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getQueueCount(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_3

    .line 86
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_2

    return-object p1

    .line 89
    :cond_3
    :try_start_3
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_1
    move-exception p1

    .line 91
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_2
    const/4 p1, 0x0

    return-object p1

    :catch_2
    move-exception p1

    .line 94
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    .line 95
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1

    .line 49
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 50
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 51
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;->listener:Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;

    if-eqz v0, :cond_0

    .line 52
    invoke-interface {v0, p1}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$Listener;->onResult(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 35
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 36
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask$1;-><init>(Lcom/lltskb/lltskb/engine/tasks/CheckOrderTask;)V

    const v2, 0x7f0d0296

    const/4 v3, -0x1

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
