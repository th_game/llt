.class Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;
.super Ljava/lang/Object;
.source "SearchTrainTask.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->showTrainDialog([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

.field final synthetic val$editText:Landroid/widget/TextView;

.field final synthetic val$result:[Ljava/lang/String;

.field final synthetic val$selectTrains:[Z


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;[ZLandroid/widget/TextView;[Ljava/lang/String;)V
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    iput-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$selectTrains:[Z

    iput-object p3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$editText:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$result:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .line 198
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$selectTrains:[Z

    const/4 p2, 0x0

    aget-boolean p1, p1, p2

    if-eqz p1, :cond_0

    .line 199
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$editText:Landroid/widget/TextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$editText:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 201
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->access$100(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 202
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->access$100(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    goto :goto_1

    .line 204
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    .line 206
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$selectTrains:[Z

    array-length v3, v2

    if-ge v1, v3, :cond_3

    .line 207
    aget-boolean v2, v2, v1

    if-eqz v2, :cond_2

    .line 208
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$result:[Ljava/lang/String;

    aget-object v2, v2, v1

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 209
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$result:[Ljava/lang/String;

    aget-object v4, v3, v1

    if-lez v2, :cond_1

    .line 211
    aget-object v3, v3, v1

    invoke-virtual {v3, p2, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 212
    :cond_1
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    invoke-static {v3}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->access$200(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :cond_3
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$editText:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->val$editText:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->clearFocus()V

    .line 220
    iget-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    invoke-static {p2}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->access$100(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainCode(Ljava/lang/String;)V

    .line 221
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;->this$0:Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;

    invoke-static {p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->access$100(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    move-result-object p1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->setTrainNo(Ljava/lang/String;)V

    :goto_1
    return-void
.end method
