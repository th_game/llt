.class public Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;
.super Landroid/os/AsyncTask;
.source "SubmitOrderRequestTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SubmitOrderRequestTask"


# instance fields
.field private errorMsg:Ljava/lang/String;

.field private listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

.field private purpose:Ljava/lang/String;

.field private status:I

.field private strErrHasNoComplete:Ljava/lang/String;

.field private strNoCompleteOrder:Ljava/lang/String;

.field private tourFlag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    iput-object p3, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

    .line 39
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->purpose:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->tourFlag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 p1, -0x3

    .line 100
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 102
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v0

    .line 104
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x5

    if-ge v3, v4, :cond_4

    .line 108
    :try_start_0
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->submitInit()I

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 110
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 111
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_2

    return-object p1

    :cond_0
    const-wide/16 v5, 0x64

    .line 115
    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_0
    move-exception v5

    .line 118
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 121
    :goto_1
    invoke-interface {v0}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->checkUser()Z

    move-result v5

    if-nez v5, :cond_1

    .line 122
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 123
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d010e

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 124
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    return-object p1

    .line 127
    :cond_1
    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->purpose:Ljava/lang/String;

    iget-object v6, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->tourFlag:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->submitOrderRequest(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    .line 128
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 130
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    iget-object v5, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->strNoCompleteOrder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 131
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->strErrHasNoComplete:Ljava/lang/String;

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    const/4 p1, -0x6

    .line 132
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 133
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    return-object p1

    .line 134
    :cond_2
    iget-object v4, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    invoke-static {v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->is12306Crack(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    add-int/lit8 v3, v3, 0x1

    const-string v4, "SubmitOrderRequestTask"

    .line 136
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try submit order request count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_2 .. :try_end_2} :catch_2

    const-wide/16 v4, 0xc8

    .line 139
    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 141
    :try_start_4
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 146
    :cond_3
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 149
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    return-object p1

    :catch_2
    move-exception p1

    goto :goto_2

    :cond_4
    if-ne v3, v4, :cond_5

    .line 162
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 163
    iput p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 164
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;
    :try_end_4
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_4 .. :try_end_4} :catch_2

    return-object p1

    .line 167
    :goto_2
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    const/4 v0, -0x1

    .line 168
    iput v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    .line 169
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 170
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    return-object p1

    .line 173
    :cond_5
    iput v2, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 63
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 64
    iget p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    if-eqz p1, :cond_7

    .line 65
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    const-string v0, "\u672a\u767b\u5f55"

    if-eqz p1, :cond_2

    const-string v1, "\u5df2\u8fc7\u671f"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 66
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->is12306Crack(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "12306.cn\u7f51\u7edc\u7e41\u5fd9\uff0c\u63d0\u4ea4\u8ba2\u5355\u5931\u8d25\uff0c\u518d\u8bd5\u4e00\u6b21\u5417?"

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

    if-eqz v0, :cond_1

    .line 72
    iget v1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->status:I

    invoke-interface {v0, v1, p1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;->onResult(ILjava/lang/String;)V

    :cond_1
    return-void

    .line 76
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 77
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

    if-eqz p1, :cond_3

    const/4 v0, -0x8

    .line 78
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;->onResult(ILjava/lang/String;)V

    :cond_3
    return-void

    .line 83
    :cond_4
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    if-nez p1, :cond_5

    .line 84
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d02c5

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    .line 86
    :cond_5
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

    if-eqz p1, :cond_6

    const/4 v0, -0x1

    .line 87
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->errorMsg:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;->onResult(ILjava/lang/String;)V

    :cond_6
    return-void

    .line 93
    :cond_7
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->listener:Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;

    if-eqz v0, :cond_8

    const-string v1, ""

    .line 94
    invoke-interface {v0, p1, v1}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$Listener;->onResult(ILjava/lang/String;)V

    :cond_8
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 45
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask$1;-><init>(Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;)V

    const v2, 0x7f0d0296

    const/4 v3, -0x1

    invoke-static {v0, v2, v3, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 56
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d01f3

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->strNoCompleteOrder:Ljava/lang/String;

    .line 57
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v1, 0x7f0d00ee

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SubmitOrderRequestTask;->strErrHasNoComplete:Ljava/lang/String;

    .line 58
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
