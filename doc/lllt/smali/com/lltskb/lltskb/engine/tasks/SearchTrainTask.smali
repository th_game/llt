.class public Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;
.super Landroid/os/AsyncTask;
.source "SearchTrainTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBeginTime:I

.field private mDontSelectTrain:Ljava/lang/String;

.field private mEndTime:I

.field private mIsAllTime:Z

.field private mIsAllTrainType:Z

.field private mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

.field private mSelectTrainDialog:Landroid/support/v7/app/AlertDialog;

.field private mTrainCodeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    .line 49
    iput-object p2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 50
    invoke-direct {p0}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Landroid/support/v7/app/AlertDialog;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mSelectTrainDialog:Landroid/support/v7/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)Ljava/util/Map;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mTrainCodeMap:Ljava/util/Map;

    return-object p0
.end method

.method private init()V
    .locals 6

    .line 55
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    .line 56
    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    const-string v3, "QB"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mIsAllTrainType:Z

    .line 58
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    const-string v3, "00:00--24:00"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mIsAllTime:Z

    .line 59
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderTime()Ljava/lang/String;

    move-result-object v0

    const-string v3, "--"

    invoke-static {v0, v3}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 60
    array-length v3, v0

    const/4 v4, 0x2

    if-lt v3, v4, :cond_3

    .line 61
    aget-object v3, v0, v2

    const-string v4, ":"

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 62
    array-length v5, v3

    if-lez v5, :cond_1

    .line 63
    aget-object v3, v3, v2

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mBeginTime:I

    goto :goto_1

    .line 65
    :cond_1
    iput v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mBeginTime:I

    .line 68
    :goto_1
    aget-object v0, v0, v1

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 69
    array-length v1, v0

    if-lez v1, :cond_2

    .line 70
    aget-object v0, v0, v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mEndTime:I

    goto :goto_2

    .line 72
    :cond_2
    iput v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mEndTime:I

    :cond_3
    :goto_2
    return-void
.end method

.method private showTrainDialog([Ljava/lang/String;)V
    .locals 11

    .line 140
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f090242

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 146
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputType(I)V

    if-eqz p1, :cond_8

    .line 147
    array-length v2, p1

    if-nez v2, :cond_2

    goto/16 :goto_4

    .line 153
    :cond_2
    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mSelectTrainDialog:Landroid/support/v7/app/AlertDialog;

    if-nez v2, :cond_7

    .line 154
    array-length v2, p1

    new-array v2, v2, [Z

    .line 155
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_6

    .line 156
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 157
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, ","

    .line 156
    invoke-static {v3, v5}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 159
    array-length v5, v3

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_6

    aget-object v7, v3, v6

    const/4 v8, 0x0

    .line 160
    :goto_1
    array-length v9, p1

    if-ge v8, v9, :cond_5

    .line 161
    aget-object v9, p1, v8

    const/16 v10, 0x28

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 162
    aget-object v10, p1, v8

    if-ltz v9, :cond_3

    .line 164
    aget-object v10, p1, v8

    invoke-virtual {v10, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 165
    :cond_3
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 166
    aput-boolean v4, v2, v8

    goto :goto_2

    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 175
    :cond_6
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$1;

    invoke-direct {v3, p0, v2}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$1;-><init>(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;[Z)V

    .line 176
    invoke-virtual {v1, p1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x108000a

    .line 192
    invoke-virtual {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0d0260

    .line 193
    invoke-virtual {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f0d01f4

    new-instance v6, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;

    invoke-direct {v6, p0, v2, v0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$3;-><init>(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;[ZLandroid/widget/TextView;[Ljava/lang/String;)V

    .line 194
    invoke-virtual {v3, v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0d008a

    new-instance v3, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$2;

    invoke-direct {v3, p0, v0}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$2;-><init>(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;Landroid/widget/TextView;)V

    .line 225
    invoke-virtual {p1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 232
    invoke-virtual {p1, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 233
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mSelectTrainDialog:Landroid/support/v7/app/AlertDialog;

    .line 234
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mSelectTrainDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    goto :goto_3

    .line 236
    :cond_7
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog;->show()V

    :goto_3
    return-void

    .line 148
    :cond_8
    :goto_4
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0d0173

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0d01ee

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 149
    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 9

    .line 85
    new-instance p1, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;

    invoke-direct {p1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;-><init>()V

    const/4 v0, 0x0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getFromStationName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getToStationName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getOrderDate()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ADULT"

    invoke-virtual {p1, v1, v2, v3, v4}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->QueryTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 91
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/SearchTicketQuery;->getResult()Ljava/util/Vector;

    move-result-object p1
    :try_end_0
    .catch Lcom/lltskb/lltskb/engine/online/HttpParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_9

    .line 96
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_2

    .line 100
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mTrainCodeMap:Ljava/util/Map;

    .line 103
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;

    .line 105
    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mIsAllTrainType:Z

    if-nez v3, :cond_5

    .line 106
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mOrderConfig:Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/online/dto/OrderConfig;->getTrainClass()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_1
    if-ge v5, v4, :cond_4

    aget-object v7, v3, v5

    .line 107
    iget-object v8, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-static {v8, v7}, Lcom/lltskb/lltskb/utils/StringUtils;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v6, 0x1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    if-nez v6, :cond_5

    goto :goto_0

    .line 115
    :cond_5
    iget-boolean v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mIsAllTime:Z

    if-nez v3, :cond_6

    .line 116
    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    const-string v4, ":"

    invoke-static {v3, v4}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 117
    array-length v4, v3

    if-lez v4, :cond_6

    .line 118
    aget-object v3, v3, v2

    invoke-static {v3, v2}, Lcom/lltskb/lltskb/utils/StringUtils;->toInt(Ljava/lang/String;I)I

    move-result v2

    .line 119
    iget v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mBeginTime:I

    if-lt v2, v3, :cond_2

    iget v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mEndTime:I

    if-le v2, v3, :cond_6

    goto :goto_0

    .line 125
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->from_station_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->start_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\u2192"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->to_station_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->arrive_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 126
    iget-object v3, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mTrainCodeMap:Ljava/util/Map;

    iget-object v4, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->station_train_code:Ljava/lang/String;

    iget-object v1, v1, Lcom/lltskb/lltskb/engine/online/dto/LeftTicketDTO;->train_no:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 129
    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_8

    .line 130
    iget-object p1, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mDontSelectTrain:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 132
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    .line 133
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-object p1

    :cond_9
    :goto_2
    return-object v0

    :catch_0
    move-exception p1

    .line 93
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/HttpParseException;->printStackTrace()V

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 0

    .line 261
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 262
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->showTrainDialog([Ljava/lang/String;)V

    .line 263
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0d00d9

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mDontSelectTrain:Ljava/lang/String;

    .line 247
    iget-object v0, p0, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0d0231

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, -0x1000000

    new-instance v3, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$4;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask$4;-><init>(Lcom/lltskb/lltskb/engine/tasks/SearchTrainTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 256
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
