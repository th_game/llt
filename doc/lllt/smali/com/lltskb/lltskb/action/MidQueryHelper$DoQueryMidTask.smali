.class Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;
.super Landroid/os/AsyncTask;
.source "MidQueryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/MidQueryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DoQueryMidTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field _list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private date:Ljava/lang/String;

.field private from:Ljava/lang/String;

.field private helperWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/MidQueryHelper;",
            ">;"
        }
    .end annotation
.end field

.field private mid:Ljava/lang/String;

.field private to:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/MidQueryHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 162
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 163
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->helperWeakReference:Ljava/lang/ref/WeakReference;

    .line 164
    iput-object p2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->from:Ljava/lang/String;

    .line 165
    iput-object p3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->to:Ljava/lang/String;

    .line 166
    iput-object p4, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->mid:Ljava/lang/String;

    .line 167
    iput-object p5, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->date:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPreExecute$0(Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 178
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->cancel(Z)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 158
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 212
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->helperWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/MidQueryHelper;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 217
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/QueryZZ;

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$400(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z

    move-result v2

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$500(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z

    move-result p1

    invoke-direct {v1, v2, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;-><init>(ZZ)V

    .line 218
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->date:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->setDate(Ljava/lang/String;)V

    .line 222
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->from:Ljava/lang/String;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->mid:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->query(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 223
    :try_start_1
    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->mid:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->to:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->query(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    move-object p1, v0

    .line 225
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v0

    :goto_1
    if-eqz p1, :cond_4

    if-nez v1, :cond_1

    goto/16 :goto_6

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 232
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 233
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 234
    invoke-virtual {v4, v2}, Lcom/lltskb/lltskb/engine/ResultItem;->setSectionType(I)V

    .line 235
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    const-string v1, "java.util.Arrays.useLegacyMergeSort"

    const-string v3, "true"

    .line 237
    invoke-static {v1, v3}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 238
    sget v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/4 v3, 0x2

    .line 239
    sput v3, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    .line 241
    :try_start_2
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_2
    move-exception v3

    .line 243
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sort list eception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MidQueryHelper"

    invoke-static {v4, v3}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :goto_3
    sput v1, Lcom/lltskb/lltskb/engine/Consts;->RESULT_SORT_TYPE:I

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 248
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 249
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lltskb/lltskb/engine/ResultItem;

    .line 250
    invoke-virtual {v4}, Lcom/lltskb/lltskb/engine/ResultItem;->getSectionType()I

    move-result v5

    if-ne v5, v2, :cond_3

    .line 251
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 252
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 257
    :cond_3
    iput-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->_list:Ljava/util/List;

    .line 258
    :goto_5
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result p1

    if-lez p1, :cond_4

    .line 259
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_5

    :cond_4
    :goto_6
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 158
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7

    .line 183
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 184
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 185
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->helperWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/MidQueryHelper;

    if-nez p1, :cond_0

    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->_list:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 191
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->_list:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 192
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->from:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u2192 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->mid:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->to:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$700(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0169

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->_list:Ljava/util/List;

    .line 195
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$700(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f0d01b7

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 198
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->from:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->mid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->to:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$700(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "%d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$700(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitleFmt(Ljava/lang/String;)V

    .line 205
    :cond_1
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$800(Lcom/lltskb/lltskb/action/MidQueryHelper;)Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 206
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$800(Lcom/lltskb/lltskb/action/MidQueryHelper;)Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;

    move-result-object v1

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->_list:Ljava/util/List;

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$100(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$200(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$600(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;->onQueryEnd(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 171
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 172
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->helperWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/MidQueryHelper;

    if-nez v0, :cond_0

    return-void

    .line 178
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$000(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->from:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->to:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, -0xffff01

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$DoQueryMidTask$JNiB7sPD3rtxqcxuej-ovNyqXaA;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$DoQueryMidTask$JNiB7sPD3rtxqcxuej-ovNyqXaA;-><init>(Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
