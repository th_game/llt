.class public Lcom/lltskb/lltskb/action/SettingsTabView;
.super Landroid/widget/LinearLayout;
.source "SettingsTabView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private isInited:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 32
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SettingsTabView;->isInited:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 32
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SettingsTabView;->isInited:Z

    return-void
.end method


# virtual methods
.method public initView()V
    .locals 4

    .line 51
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/SettingsTabView;->isInited:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 52
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/SettingsTabView;->isInited:Z

    .line 54
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0058

    .line 55
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f090208

    .line 57
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/SwitchButton;

    .line 58
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isClassicStyle()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    const v1, 0x7f090209

    .line 60
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/SwitchButton;

    .line 61
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    const v1, 0x7f09020a

    .line 63
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/SwitchButton;

    .line 64
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    const v1, 0x7f09020b

    .line 66
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/SwitchButton;

    .line 67
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isShowRunchart()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    .line 69
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v1

    const v2, 0x7f0900a2

    .line 71
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    if-eq v1, v0, :cond_1

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d01dc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d00c4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getZwdQueryType()I

    move-result v1

    const v2, 0x7f09030b

    .line 83
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v1, :cond_4

    if-eq v1, v0, :cond_3

    goto :goto_1

    .line 89
    :cond_3
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 86
    :cond_4
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v0, 0x7f09005a

    .line 94
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 95
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090058

    .line 97
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 98
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090056

    .line 100
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 101
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090065

    .line 103
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 104
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09010f

    .line 106
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v1, "58tongcheng"

    .line 108
    invoke-static {v1}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0x8

    .line 109
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .line 117
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLatitude()D

    move-result-wide v0

    .line 118
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLongitude()D

    move-result-wide v2

    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const-string v4, "tongcheng"

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x2

    const-string v8, "settings"

    sparse-switch p1, :sswitch_data_0

    const-string p1, "http://www.17u.cn/scenery/#refid=17983686"

    goto/16 :goto_0

    .line 137
    :sswitch_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "xingyuan"

    const-string v1, "\u70b9\u51fb"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "\u6c7d\u8f66"

    invoke-static {p1, v8, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "https://h5.qichedaquan.com/?utm_source=lulutong"

    goto :goto_0

    .line 121
    :sswitch_1
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v5

    const-string v0, "http://i.58.com/HIh?lat=%f&lon=%f"

    invoke-static {p1, v0, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 122
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u5de5\u4f5c"

    invoke-static {v0, v4, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :sswitch_2
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v5

    const-string v0, "http://i.58.com/HIk?lat=%f&lon=%f"

    invoke-static {p1, v0, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 127
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u623f\u5b50"

    invoke-static {v0, v4, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :sswitch_3
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v7, v5

    const-string v0, "http://i.58.com/HIq?lat=%f&lon=%f"

    invoke-static {p1, v0, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 132
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u5bb6\u653f"

    invoke-static {v0, v4, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-static {v0, v8, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SettingsTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090056 -> :sswitch_3
        0x7f090058 -> :sswitch_2
        0x7f09005a -> :sswitch_1
        0x7f090065 -> :sswitch_0
    .end sparse-switch
.end method
