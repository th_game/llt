.class public Lcom/lltskb/lltskb/action/CCQueryTabView;
.super Lcom/lltskb/lltskb/action/BaseQueryTabView;
.source "CCQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CCQueryTabView"


# instance fields
.field private isInited:Z

.field private mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

.field private mQueryTask:Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;

.field private mTrain:Landroid/widget/AutoCompleteTextView;

.field private trainTextWatch:Lcom/lltskb/lltskb/action/TrainTextWatch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 42
    iput-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    const/4 p1, 0x0

    .line 47
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->isInited:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 42
    iput-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    const/4 p1, 0x0

    .line 47
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->isInited:Z

    return-void
.end method

.method private doQueryCCBK()V
    .locals 3

    const-string v0, "CCQueryTabView"

    const-string v1, "doQueryCCBK"

    .line 403
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 406
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 408
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 409
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->prepareIntent()Landroid/content/Intent;

    move-result-object v1

    .line 410
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "train_code"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "query_type"

    const-string v2, "query_type_train_baike"

    .line 411
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "query_method"

    const-string v2, "query_method_skbcx"

    .line 412
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 414
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private doQueryCCJpk()V
    .locals 5

    .line 355
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const-string v2, "%04d-%02d-%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 356
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 357
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 360
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 362
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v4, "train_name"

    .line 363
    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_jpk"

    .line 364
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "query_date"

    .line 365
    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 367
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private doQueryCCOnline()V
    .locals 3

    const-string v0, "CCQueryTabView"

    const-string v1, "doQUeryCCOnline"

    .line 418
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 422
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 423
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 424
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->prepareIntent()Landroid/content/Intent;

    move-result-object v1

    .line 425
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "train_code"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "query_type"

    const-string v2, "query_type_train"

    .line 426
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "query_method"

    const-string v2, "query_method_skbcx"

    .line 427
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 429
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private doQueryCCOnlineZWD()V
    .locals 3

    const-string v0, "CCQueryTabView"

    const-string v1, "doQueryCCOnlineZWD"

    .line 371
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 373
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 376
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 378
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "train_name"

    .line 379
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/ZwdQueryActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 381
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private doQueryTrainInfo()V
    .locals 5

    .line 289
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d010f

    const v2, 0x7f0d01a9

    invoke-static {v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    .line 293
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    return-void

    .line 297
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 300
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d021b

    const/4 v3, 0x0

    sget-object v4, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$6b8fjrt3Oy4FM8CfOnW307ch-VM;->INSTANCE:Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$6b8fjrt3Oy4FM8CfOnW307ch-VM;

    invoke-static {v1, v2, v3, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 304
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$TXmw2dwC9G2TjZqRB0F_cdUi40o;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$TXmw2dwC9G2TjZqRB0F_cdUi40o;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$doQueryTrainInfo$13(Landroid/content/DialogInterface;)V
    .locals 0

    return-void
.end method

.method static synthetic lambda$initView$10(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 205
    invoke-virtual {p0, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 206
    invoke-virtual {p1, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 207
    invoke-virtual {p2, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 208
    invoke-virtual {p3, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$initView$6(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 169
    invoke-virtual {p0, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 170
    invoke-virtual {p1, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 171
    invoke-virtual {p2, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 172
    invoke-virtual {p3, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$initView$7(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 177
    invoke-virtual {p0, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 178
    invoke-virtual {p1, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 179
    invoke-virtual {p2, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 180
    invoke-virtual {p3, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$initView$8(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 186
    invoke-virtual {p0, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 187
    invoke-virtual {p1, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 188
    invoke-virtual {p2, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 189
    invoke-virtual {p3, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$initView$9(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 195
    invoke-virtual {p0, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 196
    invoke-virtual {p1, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 197
    invoke-virtual {p2, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 198
    invoke-virtual {p3, p4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method private onQuery()V
    .locals 6

    .line 250
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, -0xffff01

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 251
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->loadSetting()V

    .line 252
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 254
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "%04d%02d%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 258
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastTakeDate(J)V

    const v0, 0x7f090088

    .line 260
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f090090

    .line 261
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const v2, 0x7f090082

    .line 262
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const v3, 0x7f090085

    .line 263
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const v4, 0x7f090086

    .line 264
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 266
    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    const-string v5, "ccquery"

    if-eqz v4, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryCCJpk()V

    .line 268
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u68c0\u7968\u53e3"

    invoke-static {v0, v5, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryCCOnlineZWD()V

    .line 271
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u6b63\u665a\u70b9"

    invoke-static {v0, v5, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryCCOnline()V

    goto :goto_0

    .line 274
    :cond_2
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 275
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryCCBK()V

    .line 276
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u7ad9\u540d\u767e\u79d1"

    invoke-static {v0, v5, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_3
    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 279
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryTrainInfo()V

    .line 280
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u505c\u8fd0\u589e\u5f00"

    invoke-static {v0, v5, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_4
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->doQueryCC()V

    .line 283
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u79bb\u7ebf\u67e5\u8be2"

    invoke-static {v0, v5, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private prepareIntent()Landroid/content/Intent;
    .locals 5

    const-string v0, "CCQueryTabView"

    const-string v1, "prepareIntent"

    .line 437
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 439
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "%04d-%02d-%02d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_date"

    .line 440
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ticket_type"

    const-string v2, "\u5168\u90e8"

    .line 441
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_normal"

    .line 442
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private updateRecords()V
    .locals 2

    .line 392
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getTrains()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 394
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 395
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 398
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void
.end method


# virtual methods
.method protected clearRecord(I)V
    .locals 1

    .line 76
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->delTrain(I)V

    .line 77
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    return-void
.end method

.method protected doQueryCC()V
    .locals 5

    const-string v0, "CCQueryTabView"

    const-string v1, "doQueryCC"

    .line 333
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mQueryTask:Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    const-string v1, "doQueryCC task not finished"

    .line 335
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 342
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d010f

    const v2, 0x7f0d01a9

    invoke-static {v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    .line 343
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    return-void

    .line 347
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addTrain(Ljava/lang/String;)V

    .line 348
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateRecords()V

    .line 350
    new-instance v1, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mQueryTask:Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;

    .line 352
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mQueryTask:Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public initView()V
    .locals 8

    const-string v0, "CCQueryTabView"

    const-string v1, "initView"

    .line 81
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->isInited:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->isInited:Z

    .line 85
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0055

    .line 86
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 88
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$99ckisoWPgoxDA56RlAUOu-OMpM;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$99ckisoWPgoxDA56RlAUOu-OMpM;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f09017f

    .line 90
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 92
    new-instance v2, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    .line 93
    iget-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$o2KbQcsBSY4pSkQdobgjnFz4-24;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$o2KbQcsBSY4pSkQdobgjnFz4-24;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 105
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->setListViewLongClick(Landroid/widget/ListView;)V

    .line 107
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result v1

    if-lez v1, :cond_1

    .line 108
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->loadSetting()V

    const v1, 0x7f0900b0

    .line 113
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    .line 114
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    .line 115
    new-instance v1, Lcom/lltskb/lltskb/action/TrainTextWatch;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-direct {v1, v2, v3}, Lcom/lltskb/lltskb/action/TrainTextWatch;-><init>(Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;Landroid/widget/AutoCompleteTextView;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->trainTextWatch:Lcom/lltskb/lltskb/action/TrainTextWatch;

    .line 116
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->trainTextWatch:Lcom/lltskb/lltskb/action/TrainTextWatch;

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getTrains()Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 119
    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 120
    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 121
    iget-object v4, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v3}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 123
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cB6uxWaiD5on9xMr7OCgTozewbI;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cB6uxWaiD5on9xMr7OCgTozewbI;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v1, v3}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 135
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ZVl5YFci4Jk79NQnMD07x5Enec0;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ZVl5YFci4Jk79NQnMD07x5Enec0;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v1, v3}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v1, 0x7f0901b0

    .line 137
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 138
    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ItS-NtI_AMzQbvvcSJ71e7is4Wg;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ItS-NtI_AMzQbvvcSJ71e7is4Wg;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0900ab

    .line 140
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDate:Landroid/widget/TextView;

    .line 142
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getLastTakeDate()J

    move-result-wide v3

    .line 143
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v1

    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-gez v7, :cond_3

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    mul-int/lit8 v1, v1, 0x18

    mul-int/lit16 v1, v1, 0xe10

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v5, v1

    add-long/2addr v3, v5

    .line 147
    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 148
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 150
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    const/4 v0, 0x2

    .line 151
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    const/4 v0, 0x5

    .line 152
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    .line 154
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    iget v4, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v0, v1, v3, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoliday(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->updateDisplay(Ljava/lang/String;)V

    const v0, 0x7f09012b

    .line 157
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ni8CYyJ_ByX0ng2QOK6CwHx19iU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$ni8CYyJ_ByX0ng2QOK6CwHx19iU;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090086

    .line 161
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f090090

    .line 162
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const v3, 0x7f090082

    .line 163
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    const v5, 0x7f090088

    .line 164
    invoke-virtual {p0, v5}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    const v6, 0x7f090085

    .line 165
    invoke-virtual {p0, v6}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 167
    new-instance v7, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$WaStLxrv9e4GfvxwEr6BLUvgbrc;

    invoke-direct {v7, v1, v4, v5, v6}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$WaStLxrv9e4GfvxwEr6BLUvgbrc;-><init>(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 175
    new-instance v7, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$KWdGQyaPFA-gy56jxNGKJag13uU;

    invoke-direct {v7, v1, v4, v5, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$KWdGQyaPFA-gy56jxNGKJag13uU;-><init>(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 184
    new-instance v7, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$rCkes36y-YmBCyRRUjMS7mMfi_c;

    invoke-direct {v7, v1, v4, v6, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$rCkes36y-YmBCyRRUjMS7mMfi_c;-><init>(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 193
    new-instance v7, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cWr5biZ4EoUqqIx532rKIcOiQxQ;

    invoke-direct {v7, v5, v4, v6, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cWr5biZ4EoUqqIx532rKIcOiQxQ;-><init>(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 202
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 203
    new-instance v4, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$108IGbi6GmlG0AzjwMOrO1YJ1Ug;

    invoke-direct {v4, v5, v1, v6, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$108IGbi6GmlG0AzjwMOrO1YJ1Ug;-><init>(Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f090100

    .line 212
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 213
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$eBp6zdTZ6jWllU13TZoxSl0pYCo;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$eBp6zdTZ6jWllU13TZoxSl0pYCo;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "58tongcheng"

    .line 215
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f0900f5

    .line 216
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 218
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    const v0, 0x7f09016d

    .line 220
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    const/16 v1, 0x8

    .line 222
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    const v0, 0x7f0901af

    .line 226
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 227
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cbnEvwgEbA-cJMg4NoQuXLaUE2o;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$cbnEvwgEbA-cJMg4NoQuXLaUE2o;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$doQueryTrainInfo$15$CCQueryTabView(Ljava/lang/String;)V
    .locals 3

    const-string v0, "/"

    .line 305
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/StringUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 308
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 309
    aget-object v0, p1, v1

    iget-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/engine/online/StTrainInfo;->getTrainInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 316
    :cond_1
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object p1

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$Q65hrgqZp1vvL125byfa4UDVDhs;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CCQueryTabView$Q65hrgqZp1vvL125byfa4UDVDhs;-><init>(Lcom/lltskb/lltskb/action/CCQueryTabView;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$initView$0$CCQueryTabView(Landroid/view/View;)V
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V

    return-void
.end method

.method public synthetic lambda$initView$1$CCQueryTabView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 95
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 96
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    if-eqz p2, :cond_0

    .line 97
    iget-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->trainTextWatch:Lcom/lltskb/lltskb/action/TrainTextWatch;

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Lcom/lltskb/lltskb/action/TrainTextWatch;->disableDropdown(Z)V

    .line 98
    iget-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->trainTextWatch:Lcom/lltskb/lltskb/action/TrainTextWatch;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/action/TrainTextWatch;->disableDropdown(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    iget-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 101
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string p2, "ccquery"

    const-string p3, "\u5386\u53f2\u67e5\u8be2"

    invoke-static {p1, p2, p3}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$initView$11$CCQueryTabView(Landroid/view/View;)V
    .locals 1

    .line 213
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    const-string v0, "http://4g.9188.com/activity/xrdl/index.html?in=llt"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->browserUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$12$CCQueryTabView(Landroid/view/View;)V
    .locals 2

    .line 229
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    const-string v0, "https://h5.qichedaquan.com/?utm_source=lulutong"

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "xingyuan"

    const-string v1, "\u70b9\u51fb"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$2$CCQueryTabView(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-eq p2, p1, :cond_1

    const/4 p1, 0x3

    if-ne p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    .line 129
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->onQuery()V

    const/4 p1, 0x1

    return p1
.end method

.method public synthetic lambda$initView$3$CCQueryTabView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 135
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    iget-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mTrain:Landroid/widget/AutoCompleteTextView;

    invoke-static {p1, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->closeSoftInput(Landroid/app/Activity;Landroid/widget/EditText;)V

    return-void
.end method

.method public synthetic lambda$initView$4$CCQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->onQuery()V

    return-void
.end method

.method public synthetic lambda$initView$5$CCQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 159
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$null$14$CCQueryTabView(Ljava/lang/String;)V
    .locals 3

    .line 318
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const v0, 0x7f0d00fb

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 321
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "[\u4ee5\u5217\u8f66\u59cb\u53d1\u7ad9\u65e5\u671f\u8ba1\u7b97]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 324
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 325
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d0173

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method protected updateDisplay(Ljava/lang/String;)V
    .locals 7

    .line 238
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    .line 239
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v0, v1, [Ljava/lang/Object;

    iget v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    iget v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    add-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "%04d-%02d-%02d"

    invoke-static {p1, v1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 240
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    aput-object p1, v5, v1

    const-string p1, "%04d-%02d-%02d %s"

    invoke-static {v0, p1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
