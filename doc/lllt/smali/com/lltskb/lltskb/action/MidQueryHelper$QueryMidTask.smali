.class Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;
.super Landroid/os/AsyncTask;
.source "MidQueryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/MidQueryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryMidTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private _list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private thisHelper:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/MidQueryHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/MidQueryHelper;)V
    .locals 1

    .line 104
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 105
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->thisHelper:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPreExecute$0(Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 117
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->cancel(Z)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 100
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 141
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->thisHelper:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/MidQueryHelper;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 145
    :cond_0
    new-instance v1, Lcom/lltskb/lltskb/engine/QueryZZ;

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$400(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z

    move-result v2

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$500(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/lltskb/lltskb/engine/QueryZZ;-><init>(ZZ)V

    .line 146
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$600(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/QueryZZ;->setDate(Ljava/lang/String;)V

    .line 148
    :try_start_0
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$100(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$200(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->queryMid(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->_list:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 150
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 100
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 123
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 125
    iget-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->thisHelper:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/MidQueryHelper;

    if-nez p1, :cond_0

    return-void

    .line 130
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 132
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->_list:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->_list:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$300(Lcom/lltskb/lltskb/action/MidQueryHelper;Ljava/util/List;)V

    goto :goto_1

    .line 133
    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$000(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0d010f

    const v1, 0x7f0d01e5

    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    :goto_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 109
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 111
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->thisHelper:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/MidQueryHelper;

    if-nez v0, :cond_0

    return-void

    .line 117
    :cond_0
    invoke-static {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$000(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$100(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->access$200(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, -0xffff01

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$QueryMidTask$l72K4VQX8UNi2FAX1g9L2hRQz5k;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$QueryMidTask$l72K4VQX8UNi2FAX1g9L2hRQz5k;-><init>(Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;)V

    invoke-static {v1, v0, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
