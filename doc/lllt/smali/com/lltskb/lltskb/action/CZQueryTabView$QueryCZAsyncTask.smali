.class Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;
.super Landroid/os/AsyncTask;
.source "CZQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/CZQueryTabView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "QueryCZAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private _list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private station:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/CZQueryTabView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/CZQueryTabView;Ljava/lang/String;)V
    .locals 1

    .line 330
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 384
    iput-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->_list:Ljava/util/List;

    .line 331
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    .line 332
    iput-object p2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->station:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$onPreExecute$0(Landroid/os/AsyncTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 347
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->cancel(Z)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 326
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 388
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/CZQueryTabView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v2, 0x0

    .line 393
    aget-object v2, p1, v2

    const/4 v3, 0x1

    .line 394
    aget-object p1, p1, v3

    .line 395
    new-instance v3, Lcom/lltskb/lltskb/engine/QueryCZ;

    iget-boolean v4, v0, Lcom/lltskb/lltskb/action/CZQueryTabView;->fuzzy:Z

    iget-boolean v0, v0, Lcom/lltskb/lltskb/action/CZQueryTabView;->hiden:Z

    invoke-direct {v3, v4, v0}, Lcom/lltskb/lltskb/engine/QueryCZ;-><init>(ZZ)V

    .line 396
    invoke-virtual {v3, p1}, Lcom/lltskb/lltskb/engine/QueryCZ;->setDate(Ljava/lang/String;)V

    .line 397
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result p1

    if-lez p1, :cond_1

    .line 398
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    .line 400
    :cond_1
    :try_start_0
    invoke-virtual {v3, v2}, Lcom/lltskb/lltskb/engine/QueryCZ;->query(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->_list:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 402
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-object v1
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 326
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 0

    .line 336
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 326
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7

    .line 355
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 356
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 358
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/CZQueryTabView;

    if-nez p1, :cond_0

    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->_list:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    goto/16 :goto_1

    .line 370
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    iget-object v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->_list:Ljava/util/List;

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 371
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->station:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0169

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->_list:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d01b7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitleFmt(Ljava/lang/String;)V

    .line 374
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v3, p1, Lcom/lltskb/lltskb/action/CZQueryTabView;->mClassicUI:Z

    if-eqz v3, :cond_2

    const-class v3, Lcom/lltskb/lltskb/result/ResultActivity;

    goto :goto_0

    :cond_2
    const-class v3, Lcom/lltskb/lltskb/result/ViewShowResult;

    :goto_0
    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    const-string v3, "query_type"

    .line 375
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 376
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    iget v5, p1, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    add-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget v2, p1, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    const-string v1, "%04d-%02d-%02d"

    invoke-static {v3, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_date"

    .line 377
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->station:Ljava/lang/String;

    const-string v2, "ticket_start_station"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_2

    .line 364
    :cond_3
    :goto_1
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0d01e5

    .line 365
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0d01f4

    .line 366
    invoke-virtual {p1, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 367
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 368
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    :goto_2
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 341
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/CZQueryTabView;

    if-nez v0, :cond_0

    return-void

    .line 347
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->station:Ljava/lang/String;

    const v2, -0xffff01

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$QueryCZAsyncTask$dZm0B_N97YMwNIo1bj34EnRK5aM;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$QueryCZAsyncTask$dZm0B_N97YMwNIo1bj34EnRK5aM;-><init>(Landroid/os/AsyncTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    .line 349
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
