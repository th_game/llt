.class public Lcom/lltskb/lltskb/action/SoftTabView;
.super Landroid/widget/LinearLayout;
.source "SoftTabView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SoftTabView"


# instance fields
.field private mIsDownloading:Z

.field private mSoftItems:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;",
            ">;"
        }
    .end annotation
.end field

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 48
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mIsDownloading:Z

    .line 55
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 48
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mIsDownloading:Z

    .line 64
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/action/SoftTabView;)Ljava/util/Vector;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mSoftItems:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/action/SoftTabView;)Z
    .locals 0

    .line 40
    iget-boolean p0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mIsDownloading:Z

    return p0
.end method

.method static synthetic access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z
    .locals 0

    .line 40
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mIsDownloading:Z

    return p1
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/action/SoftTabView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/action/SoftTabView;I)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/SoftTabView;->showSoftDetails(I)V

    return-void
.end method

.method private initView()V
    .locals 2

    .line 94
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0b0054

    .line 95
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 97
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResMgr;->getSoftItems()Ljava/util/Vector;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mSoftItems:Ljava/util/Vector;

    .line 98
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mSoftItems:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mSoftItems:Ljava/util/Vector;

    :cond_0
    const v0, 0x7f090030

    .line 100
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/SoftTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 102
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 103
    new-instance v1, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;-><init>(Lcom/lltskb/lltskb/action/SoftTabView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    return-void
.end method

.method private showSoftDetails(I)V
    .locals 3

    const-string v0, "SoftTabView"

    const-string v1, "showSoftDetails"

    .line 68
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView;->mSoftItems:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 70
    iget-object v0, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const-string v1, ".htm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 75
    :cond_0
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    .line 76
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v1, Lcom/lltskb/lltskb/action/SoftDetailsView;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;-><init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    .line 79
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 81
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    .line 83
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    const/4 v2, -0x1

    .line 84
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 85
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 86
    invoke-virtual {p1, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 90
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDialog;->show()V

    return-void

    .line 71
    :cond_2
    :goto_0
    new-instance v0, Landroid/content/Intent;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 72
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
