.class public Lcom/lltskb/lltskb/action/ZZQueryTabView;
.super Lcom/lltskb/lltskb/action/BaseQueryTabView;
.source "ZZQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ZZQueryTabView"


# instance fields
.field private mEnd:Landroid/widget/TextView;

.field private mFilterType:I

.field private mHistoryAlert:Landroid/support/v7/app/AlertDialog;

.field private mStart:Landroid/widget/TextView;

.field private mTrainType:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 43
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    .line 44
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    .line 46
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    const/16 p1, 0xff

    .line 47
    iput p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    .line 56
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 43
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    .line 44
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    .line 46
    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    const/16 p1, 0xff

    .line 47
    iput p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    .line 65
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->doSearchMidStation()V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/action/ZZQueryTabView;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 41
    invoke-direct/range {p0 .. p5}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->processResult(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/action/ZZQueryTabView;)Landroid/widget/TextView;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/action/ZZQueryTabView;)Landroid/widget/TextView;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    return-object p0
.end method

.method private checkInput()Z
    .locals 2

    const-string v0, "ZZQueryTabView"

    const-string v1, "checkInput"

    .line 435
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 440
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$-_a1y3engo4SS0nzbHAUDSZQekk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$-_a1y3engo4SS0nzbHAUDSZQekk;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    return v0
.end method

.method private doQuery()V
    .locals 9

    const-string v0, "ZZQueryTabView"

    const-string v1, "doQuery"

    .line 504
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->checkInput()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f090084

    .line 508
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    # 模糊查询
    check-cast v0, Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    return-void

    .line 510
    :cond_1
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    # fuzzy = false
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->fuzzy:Z

    .line 512
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    iget-boolean v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->fuzzy:Z

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setFuzzyQuery(Z)V

    .line 514
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->hiden:Z

    .line 516
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 517
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->string2Type(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    .line 519
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/Object;

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    const/4 v5, 0x1

    add-int/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x2

    aput-object v3, v2, v6

    const-string v3, "%04d%02d%02d"

    invoke-static {v0, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 521
    new-instance v2, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    .line 523
    iget-object v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 524
    iget-object v7, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 526
    sget-object v8, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/String;

    aput-object v3, v1, v4

    aput-object v7, v1, v5

    aput-object v0, v1, v6

    invoke-virtual {v2, v8, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private doSearchMidStation()V
    .locals 11

    const-string v0, "ZZQueryTabView"

    const-string v1, "doSearchMidStation"

    .line 392
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "%04d%02d%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 394
    new-instance v0, Lcom/lltskb/lltskb/action/MidQueryHelper;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    .line 395
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iget-boolean v8, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->fuzzy:Z

    iget-boolean v9, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->hiden:Z

    new-instance v10, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$EBacyLixqQD3xXFzzqJVrr_br4Q;

    invoke-direct {v10, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$EBacyLixqQD3xXFzzqJVrr_br4Q;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    move-object v3, v0

    invoke-direct/range {v3 .. v10}, Lcom/lltskb/lltskb/action/MidQueryHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/lltskb/lltskb/action/MidQueryHelper$Listener;)V

    .line 397
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->searchMidStation()V

    .line 399
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "zzquery"

    const-string v2, "\u4e2d\u8f6c\u67e5\u8be2"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private initView()V
    .locals 6

    const-string v0, "ZZQueryTabView"

    const-string v1, "initView"

    .line 77
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0b0059

    .line 79
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 81
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result v0

    if-lez v0, :cond_0

    .line 82
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    :cond_0
    const v0, 0x7f0900ae

    .line 85
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    const v0, 0x7f0900aa

    .line 86
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    const v0, 0x7f0900ab

    .line 87
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDate:Landroid/widget/TextView;

    .line 89
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getStartStation()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getEndStation()Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090158

    .line 95
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$zhIFeixKTUK732ap02RGtzEw2F8;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$zhIFeixKTUK732ap02RGtzEw2F8;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090113

    .line 105
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$oYW8jslVC56wjyfwfjFNy_-yf6o;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$oYW8jslVC56wjyfwfjFNy_-yf6o;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getLastTakeDate()J

    move-result-wide v0

    .line 115
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v2

    .line 116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v5, v0, v3

    if-gez v5, :cond_1

    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    mul-int/lit8 v2, v2, 0x18

    mul-int/lit16 v2, v2, 0xe10

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 119
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 120
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x1

    .line 121
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    const/4 v1, 0x2

    .line 122
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    const/4 v1, 0x5

    .line 123
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    .line 125
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    iget v4, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v1, v2, v3, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoliday(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->updateDisplay(Ljava/lang/String;)V

    const v1, 0x7f09012b

    .line 128
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 129
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$vnh6GbXZgRgqSS6q-PWdIC5ujVk;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$vnh6GbXZgRgqSS6q-PWdIC5ujVk;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0900b1

    .line 131
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    const v1, 0x7f090166

    .line 132
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$4NPBM_pX_13MI5Wdvqtdlvc3Tac;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$4NPBM_pX_13MI5Wdvqtdlvc3Tac;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090066

    .line 135
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 136
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$HntGipIxVVwOFsFaK33CCsY_9I8;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$HntGipIxVVwOFsFaK33CCsY_9I8;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f09020c

    .line 138
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 139
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$4K9y0stIVqLeZZxnuLYili4Ho0Q;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$4K9y0stIVqLeZZxnuLYili4Ho0Q;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    invoke-virtual {v1}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 147
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0600b9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 146
    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 149
    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/DialogUtils;->setBackgroundCompat(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 151
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->loadSetting()V

    const v1, 0x7f090084

    .line 153
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-eqz v1, :cond_2

    .line 155
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 156
    sget-object v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$ZH9Cn-DmnB0Bucm41m0pJluZbMM;->INSTANCE:Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$ZH9Cn-DmnB0Bucm41m0pJluZbMM;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    const v1, 0x7f090088

    .line 159
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-eqz v1, :cond_3

    .line 161
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$FkUMUUoBNkj73L_qc6I4NXc14ew;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$FkUMUUoBNkj73L_qc6I4NXc14ew;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_3
    const v1, 0x7f09008d

    .line 170
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-eqz v1, :cond_4

    .line 172
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$2bzfiE8PBGo8nE1019qsOT1WpV4;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$2bzfiE8PBGo8nE1019qsOT1WpV4;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_4
    const v1, 0x7f0900f2

    .line 185
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 187
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 188
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$7YwAu5clSyoWm9X13s4BxEYbxug;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$7YwAu5clSyoWm9X13s4BxEYbxug;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v1, 0x7f0900d5

    .line 191
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_6

    .line 192
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_6
    const v1, 0x7f0900f5

    .line 193
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_7

    .line 194
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_7
    const v1, 0x7f0901af

    .line 195
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_8

    .line 196
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 198
    :cond_8
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getPurpose()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->setPurpose(Ljava/lang/String;)V

    const v0, 0x7f0901b7

    .line 200
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->setPurposeChangeListener(I)V

    const v0, 0x7f0901b8

    .line 201
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->setPurposeChangeListener(I)V

    return-void
.end method

.method public static synthetic lambda$EBacyLixqQD3xXFzzqJVrr_br4Q(Lcom/lltskb/lltskb/action/ZZQueryTabView;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->processResult(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic lambda$initView$6(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 156
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->setFuzzyQuery(Z)V

    return-void
.end method

.method static synthetic lambda$null$13(Lcom/lltskb/lltskb/view/LLTProgressDialog;Landroid/view/View;)V
    .locals 0

    .line 452
    invoke-virtual {p0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method private onHistroy()V
    .locals 6

    const-string v0, "ZZQueryTabView"

    const-string v1, "onHistroy"

    .line 290
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getHistroy()Ljava/util/Vector;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 293
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 297
    :cond_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    .line 298
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 299
    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    if-nez v3, :cond_1

    goto :goto_1

    .line 301
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v3, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u2192"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "\u6ca1\u6709\u5386\u53f2\u8bb0\u5f55"

    aput-object v3, v2, v1

    .line 305
    :cond_3
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0d025e

    .line 306
    invoke-virtual {v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 307
    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$opLr_Wx2JpZjAEWJpp8mmKQPaU0;

    invoke-direct {v3, p0, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$opLr_Wx2JpZjAEWJpp8mmKQPaU0;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;Ljava/util/Vector;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 315
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mHistoryAlert:Landroid/support/v7/app/AlertDialog;

    .line 317
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mHistoryAlert:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 318
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mHistoryAlert:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_4

    const v1, 0x7f0e00bd

    .line 320
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 323
    :cond_4
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mHistoryAlert:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 325
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->setListViewLongClick(Landroid/widget/ListView;)V

    .line 327
    :cond_5
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "zzquery"

    const-string v2, "\u5386\u53f2\u67e5\u8be2"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onQuery()V
    .locals 3

    const-string v0, "ZZQueryTabView"

    const-string v1, "onQuery"

    .line 248
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->loadSetting()V

    const v0, 0x7f090088

    .line 250
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    # v0 = 联网查询
    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f09008d

    .line 251
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    # v1 = 查询余票
    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    if-nez v1, :cond_0

    goto :goto_0

    .line 254
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->saveHistroy()V

    .line 255
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const-string v2, "zzquery"

    if-eqz v0, :cond_1

    .line 256
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->searchTime()V

    .line 257
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u8054\u7f51\u67e5\u8be2"

    invoke-static {v0, v2, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 261
    :cond_1
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 262
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->queryTicket()V

    .line 263
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "\u4f59\u7968\u67e5\u8be2"

    invoke-static {v0, v2, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 267
    :cond_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->doQuery()V

    .line 268
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    # v1 余票查询
    const-string v1, "\u79bb\u7ebf\u67e5\u8be2"

    invoke-static {v0, v2, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private prepareIntent()Landroid/content/Intent;
    .locals 5

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "prepareIntent from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZZQueryTabView"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 353
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "%04d-%02d-%02d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_date"

    .line 355
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 357
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_start_station"

    .line 356
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 359
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_arrive_station"

    .line 358
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_normal"

    .line 361
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private processResult(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const-string v0, "ZZQueryTabView"

    const-string v1, "processResult"

    .line 465
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 468
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 469
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d02ce

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 470
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ";"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    invoke-static {p3}, Lcom/lltskb/lltskb/utils/LLTUtils;->type2String(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/16 p3, 0x3b

    const/16 p4, 0x20

    .line 471
    invoke-virtual {p2, p3, p4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    new-array p3, v1, [Ljava/lang/Object;

    aput-object p2, p3, v0

    .line 472
    invoke-static {p1, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 473
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    .line 474
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f0d010f

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    new-instance p4, Lcom/lltskb/lltskb/action/ZZQueryTabView$1;

    invoke-direct {p4, p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView$1;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-static {p2, p3, p1, p4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showConfirmDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/lltskb/lltskb/utils/LLTUIUtils$IConfirmSink;)V

    goto :goto_1

    .line 490
    :cond_1
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mClassicUI:Z

    if-eqz v2, :cond_2

    const-class v2, Lcom/lltskb/lltskb/result/ResultActivity;

    goto :goto_0

    :cond_2
    const-class v2, Lcom/lltskb/lltskb/result/ViewShowResult;

    :goto_0
    invoke-direct {p1, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "query_type"

    .line 491
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "result_can_sort"

    .line 492
    invoke-virtual {p1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p5, "ticket_start_station"

    .line 493
    invoke-virtual {p1, p5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "ticket_arrive_station"

    .line 494
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "ticket_date"

    .line 495
    invoke-virtual {p1, p2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    :goto_1
    return-void
.end method

.method private queryTicket()V
    .locals 3

    const-string v0, "ZZQueryTabView"

    const-string v1, "queryTicket"

    .line 369
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query_type"

    const-string v2, "query_type_ticket"

    .line 371
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 373
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private saveHistroy()V
    .locals 8

    const-string v0, "ZZQueryTabView"

    const-string v1, "saveHistory"

    .line 407
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f090088

    .line 410
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-nez v1, :cond_0

    return-void

    .line 412
    :cond_0
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    const v1, 0x7f09008d

    .line 414
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 415
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    .line 417
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    iget v6, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    move-object v2, v0

    move-object v3, v7

    invoke-virtual/range {v1 .. v6}, Lcom/lltskb/lltskb/engine/LltSettings;->addHistroy(Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 419
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setStartStation(Ljava/lang/String;)V

    .line 420
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/lltskb/lltskb/engine/LltSettings;->setEndStation(Ljava/lang/String;)V

    .line 421
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    iget v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setFilterType(I)V

    .line 423
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "%04d%02d%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 425
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 426
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastTakeDate(J)V

    return-void
.end method

.method private searchTime()V
    .locals 3

    const-string v0, "ZZQueryTabView"

    const-string v1, "searchTime"

    .line 380
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query_type"

    const-string v2, "query_type_zztime"

    .line 382
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_skbcx"

    .line 383
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 385
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private setPurpose(Ljava/lang/String;)V
    .locals 4

    const v0, 0x7f0901b7

    .line 205
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v1, 0x7f0901b8

    .line 206
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 207
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_1

    .line 208
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->setPurpose(Ljava/lang/String;)V

    const-string v2, "ADULT"

    .line 210
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz v0, :cond_2

    .line 212
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_2

    .line 216
    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 221
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method private setPurposeChangeListener(I)V
    .locals 1

    .line 227
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    if-eqz p1, :cond_0

    .line 230
    new-instance v0, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$-068RbBTuDwMzhuZZLPQiJw6a9k;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$-068RbBTuDwMzhuZZLPQiJw6a9k;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-void
.end method

.method private setTrainType()V
    .locals 4

    const-string v0, "ZZQueryTabView"

    const-string v1, "setTrainType"

    .line 335
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v0, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$xlVSl78UXSuR801H_INT-X1A-Yc;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$xlVSl78UXSuR801H_INT-X1A-Yc;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;-><init>(Landroid/content/Context;ILcom/lltskb/lltskb/view/SelectTrainTypeDialog$Listener;)V

    .line 343
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SelectTrainTypeDialog;->show()V

    return-void
.end method

.method private updatePurpose()V
    .locals 2

    const v0, 0x7f0901b8

    .line 235
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    const-string v1, "0X00"

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setPurpose(Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    const-string v1, "ADULT"

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setPurpose(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected clearRecord(I)V
    .locals 1

    .line 277
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->deleteHistroy(I)V

    .line 278
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mHistoryAlert:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    :cond_0
    if-ltz p1, :cond_1

    .line 282
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->onHistroy()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$checkInput$14$ZZQueryTabView()V
    .locals 3

    .line 442
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 444
    new-instance v0, Lcom/lltskb/lltskb/view/LLTProgressDialog;

    .line 445
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0143

    invoke-direct {v0, v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x10

    .line 447
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    const v1, 0x7f0d010f

    .line 448
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(I)V

    const v1, 0x7f0d01a9

    .line 449
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(I)V

    .line 450
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V

    .line 452
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$HWWwt6vD0WXiAgCYq7t37Pti2xI;

    invoke-direct {v1, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$HWWwt6vD0WXiAgCYq7t37Pti2xI;-><init>(Lcom/lltskb/lltskb/view/LLTProgressDialog;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setOkBtnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$initView$0$ZZQueryTabView(Landroid/view/View;)V
    .locals 2

    .line 101
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/MainLLTSkb;

    const v0, 0x7f0900ae

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->selectStation(Landroid/widget/TextView;I)V

    return-void
.end method

.method public synthetic lambda$initView$1$ZZQueryTabView(Landroid/view/View;)V
    .locals 2

    .line 111
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/MainLLTSkb;

    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->selectStation(Landroid/widget/TextView;I)V

    return-void
.end method

.method public synthetic lambda$initView$2$ZZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 129
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$initView$3$ZZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->setTrainType()V

    return-void
.end method

.method public synthetic lambda$initView$4$ZZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 136
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->onQuery()V

    return-void
.end method

.method public synthetic lambda$initView$5$ZZQueryTabView(Landroid/view/View;)V
    .locals 2

    .line 140
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 141
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "zzquery"

    const-string v1, "\u8fd4\u7a0b"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$initView$7$ZZQueryTabView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 162
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f09008d

    .line 163
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    const/4 p2, 0x0

    .line 164
    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$initView$8$ZZQueryTabView(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 173
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_0

    const p2, 0x7f090088

    .line 174
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/CheckBox;

    const/4 v0, 0x0

    .line 175
    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    const p2, 0x7f090084

    .line 178
    invoke-virtual {p0, p2}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/CheckBox;

    if-eqz p2, :cond_1

    .line 180
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p2, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$initView$9$ZZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 188
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->onHistroy()V

    return-void
.end method

.method public synthetic lambda$onHistroy$11$ZZQueryTabView(Ljava/util/Vector;Landroid/content/DialogInterface;I)V
    .locals 0

    if-eqz p1, :cond_1

    .line 308
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p2

    if-lt p3, p2, :cond_0

    goto :goto_0

    .line 309
    :cond_0
    invoke-virtual {p1, p3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;

    .line 310
    iget-object p2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mStart:Landroid/widget/TextView;

    iget-object p3, p1, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mStart:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object p2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mEnd:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/LltSettings$HistroyItem;->mEnd:Ljava/lang/String;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$setPurposeChangeListener$10$ZZQueryTabView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 230
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->updatePurpose()V

    return-void
.end method

.method public synthetic lambda$setTrainType$12$ZZQueryTabView(ILjava/lang/String;)V
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    iput p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    .line 340
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->setFilterType(I)V

    return-void
.end method

.method protected loadSetting()V
    .locals 2

    const-string v0, "ZZQueryTabView"

    const-string v1, "loadSetting"

    .line 543
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    invoke-super {p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView;->loadSetting()V

    const v0, 0x7f090084

    .line 546
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 547
    iget-boolean v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->fuzzy:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 549
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isClassicStyle()Z

    move-result v0

    const v1, -0xe98105    # -2.0004352E38f

    if-eqz v0, :cond_1

    .line 550
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    goto :goto_0

    .line 552
    :cond_1
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    .line 554
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getFilterType()I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    .line 555
    iget v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mFilterType:I

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->type2String(I)Ljava/lang/String;

    move-result-object v0

    .line 556
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mTrainType:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method protected updateDisplay(Ljava/lang/String;)V
    .locals 7

    .line 565
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    .line 566
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v0, v1, [Ljava/lang/Object;

    iget v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    iget v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    add-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "%04d-%02d-%02d"

    invoke-static {p1, v1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 567
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 569
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mYear:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mMonth:I

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    aput-object p1, v5, v1

    const-string p1, "%04d-%02d-%02d %s"

    invoke-static {v0, p1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 570
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
