.class public Lcom/lltskb/lltskb/action/CZQueryTabView;
.super Lcom/lltskb/lltskb/action/BaseQueryTabView;
.source "CZQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CZQueryTabView"


# instance fields
.field private chk_baike:Landroid/widget/CheckBox;

.field private chk_online:Landroid/widget/CheckBox;

.field private chk_qss:Landroid/widget/CheckBox;

.field private isInited:Z

.field private mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

.field private mStation:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 41
    iput-object p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->isInited:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/action/BaseQueryTabView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 41
    iput-object p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->isInited:Z

    return-void
.end method

.method private doQueryCZBK()V
    .locals 2

    const-string v0, "CZQueryTabView"

    const-string v1, "doQueryCZBK"

    .line 294
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 296
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showStationBaike(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private doQueryCZOnline()V
    .locals 4

    const-string v0, "CZQueryTabView"

    const-string v1, "doQueryCZOnline"

    .line 301
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->addStation(Ljava/lang/String;)V

    .line 306
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->updateRecords()V

    .line 308
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->prepareIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "query_type"

    const-string v3, "query_type_station"

    .line 309
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "station_code"

    .line 310
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "query_method"

    const-string v2, "query_method_skbcx"

    .line 311
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 312
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/QueryResultActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 313
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private doQueryQss()V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/AppExecutors;->diskIO()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$eLBsKYkPYI6B9_PsNBM0uddXq1A;

    invoke-direct {v2, p0, v0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$eLBsKYkPYI6B9_PsNBM0uddXq1A;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private onQuery()V
    .locals 4

    .line 202
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->loadSetting()V

    .line 204
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->addStation(Ljava/lang/String;)V

    .line 205
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "%04d%02d%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 208
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastTakeDate(J)V

    .line 211
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->updateRecords()V

    .line 213
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_qss:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const-string v1, "czquery"

    if-eqz v0, :cond_0

    .line 214
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->doQueryQss()V

    .line 215
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v2, "\u8d77\u552e\u65f6\u95f4"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_online:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->doQueryCZOnline()V

    .line 218
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v2, "\u8054\u7f51\u67e5\u8be2"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_baike:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->doQueryCZBK()V

    .line 221
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v2, "\u7ad9\u540d\u767e\u79d1"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_2
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->doQueryCZ()V

    .line 225
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v2, "\u79bb\u7ebf\u67e5\u8be2"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private prepareIntent()Landroid/content/Intent;
    .locals 5

    const-string v0, "CZQueryTabView"

    const-string v1, "prepareIntent"

    .line 317
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 319
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "%04d-%02d-%02d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_date"

    .line 320
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ticket_type"

    const-string v2, "\u5168\u90e8"

    .line 321
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query_method"

    const-string v2, "query_method_normal"

    .line 322
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private showQss(Ljava/lang/String;)V
    .locals 4

    .line 240
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const v2, 0x7f0d02de

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d0284

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-static {p1, v0, v2, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v3, 0x7f0d0250

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p1, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method private updateRecords()V
    .locals 2

    .line 79
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->getStations()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 82
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 85
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void
.end method


# virtual methods
.method protected clearRecord(I)V
    .locals 1

    .line 91
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/LltSettings;->delStation(I)V

    .line 93
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->updateRecords()V

    return-void
.end method

.method protected doQueryCZ()V
    .locals 7

    const-string v0, "CZQueryTabView"

    const-string v1, "doQueryCZ"

    .line 277
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$Ao8T2i21QR_z-G-2PkmCrLz2R8w;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$Ao8T2i21QR_z-G-2PkmCrLz2R8w;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    .line 283
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    const/4 v4, 0x1

    add-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-string v2, "%04d%02d%02d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 285
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 287
    new-instance v2, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;

    invoke-direct {v2, p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;Ljava/lang/String;)V

    .line 289
    sget-object v6, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v3

    aput-object v0, v5, v4

    invoke-virtual {v2, v6, v5}, Lcom/lltskb/lltskb/action/CZQueryTabView$QueryCZAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public initView()V
    .locals 8

    const-string v0, "CZQueryTabView"

    const-string v1, "initView"

    .line 97
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->isInited:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 99
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->isInited:Z

    .line 100
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0056

    .line 101
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 103
    new-instance v1, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    const v1, 0x7f09017f

    .line 104
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 105
    iget-object v2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$mOyGFUY9JYAy9_E1UsR-6XLD8_8;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$mOyGFUY9JYAy9_E1UsR-6XLD8_8;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 115
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->setListViewLongClick(Landroid/widget/ListView;)V

    .line 117
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result v1

    if-lez v1, :cond_1

    .line 118
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->loadSetting()V

    const v1, 0x7f0900af

    .line 122
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    .line 124
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getStations()Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 125
    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 126
    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 127
    iget-object v4, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->setData(Ljava/util/Vector;)V

    .line 129
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->notifyDataSetChanged()V

    :cond_2
    const v1, 0x7f09015a

    .line 134
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 135
    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$Y6tZ78QVtkUnhyT9sGv30q5lYbw;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$Y6tZ78QVtkUnhyT9sGv30q5lYbw;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const v1, 0x7f0901b1

    .line 137
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 138
    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$hljfunsAKI0SK2O0ANokEKD6nvg;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$hljfunsAKI0SK2O0ANokEKD6nvg;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0900ab

    .line 140
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDate:Landroid/widget/TextView;

    .line 142
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getLastTakeDate()J

    move-result-wide v3

    .line 143
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v1

    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-gez v7, :cond_4

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    mul-int/lit8 v1, v1, 0x18

    mul-int/lit16 v1, v1, 0xe10

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v5, v1

    add-long/2addr v3, v5

    .line 147
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 148
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 150
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    const/4 v0, 0x2

    .line 151
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    const/4 v0, 0x5

    .line 152
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    .line 154
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    iget v4, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v0, v1, v3, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getHoliday(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->updateDisplay(Ljava/lang/String;)V

    const v0, 0x7f09012b

    .line 157
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$1mbxYSShWDqZHkEixW5i3r8l-bk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$1mbxYSShWDqZHkEixW5i3r8l-bk;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090088

    .line 163
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_online:Landroid/widget/CheckBox;

    const v0, 0x7f090082

    .line 164
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_baike:Landroid/widget/CheckBox;

    const v0, 0x7f090089

    .line 165
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_qss:Landroid/widget/CheckBox;

    .line 167
    new-instance v0, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$q5ik7o6npSIvTSOylMeg3YwV71I;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$q5ik7o6npSIvTSOylMeg3YwV71I;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;)V

    .line 181
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_qss:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 182
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_baike:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 183
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_online:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const-string v0, "58tongcheng"

    .line 185
    invoke-static {v0}, Lcom/lltskb/lltskb/utils/FeatureToggle;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const v0, 0x7f0900f5

    .line 186
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 188
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    const v0, 0x7f09016d

    .line 190
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    const/16 v1, 0x8

    .line 192
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    return-void
.end method

.method public synthetic lambda$doQueryCZ$7$CZQueryTabView()V
    .locals 3

    .line 279
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d010f

    const v2, 0x7f0d01a9

    invoke-static {v0, v1, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method

.method public synthetic lambda$doQueryQss$6$CZQueryTabView(Ljava/lang/String;)V
    .locals 2

    .line 234
    invoke-static {}, Lcom/lltskb/lltskb/engine/QuickStation;->get()Lcom/lltskb/lltskb/engine/QuickStation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/engine/QuickStation;->getStationQss(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 235
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->mainThread()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$mFvKpzaCS8_vS6rPqh0QCl4yrno;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/action/-$$Lambda$CZQueryTabView$mFvKpzaCS8_vS6rPqh0QCl4yrno;-><init>(Lcom/lltskb/lltskb/action/CZQueryTabView;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$initView$0$CZQueryTabView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 108
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mAdapter:Lcom/lltskb/lltskb/adapters/HistoryListAdapter;

    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/adapters/HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 109
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 110
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string p2, "czquery"

    const-string p3, "\u5386\u53f2\u67e5\u8be2"

    invoke-static {p1, p2, p3}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$initView$1$CZQueryTabView(Landroid/view/View;)V
    .locals 2

    .line 135
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/MainLLTSkb;

    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mStation:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->selectStation(Landroid/widget/TextView;I)V

    return-void
.end method

.method public synthetic lambda$initView$2$CZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->onQuery()V

    return-void
.end method

.method public synthetic lambda$initView$3$CZQueryTabView(Landroid/view/View;)V
    .locals 0

    .line 159
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->showDateDialog()V

    return-void
.end method

.method public synthetic lambda$initView$4$CZQueryTabView(Landroid/widget/CompoundButton;Z)V
    .locals 1

    if-eqz p2, :cond_2

    .line 169
    iget-object p2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_online:Landroid/widget/CheckBox;

    const/4 v0, 0x0

    if-eq p2, p1, :cond_0

    .line 170
    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 172
    :cond_0
    iget-object p2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_baike:Landroid/widget/CheckBox;

    if-eq p2, p1, :cond_1

    .line 173
    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 175
    :cond_1
    iget-object p2, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->chk_qss:Landroid/widget/CheckBox;

    if-eq p2, p1, :cond_2

    .line 176
    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_2
    return-void
.end method

.method public synthetic lambda$null$5$CZQueryTabView(Ljava/lang/String;)V
    .locals 0

    .line 235
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->showQss(Ljava/lang/String;)V

    return-void
.end method

.method protected loadSetting()V
    .locals 2

    const-string v0, "CZQueryTabView"

    const-string v1, "loadSetting"

    .line 264
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-super {p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView;->loadSetting()V

    .line 266
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mClassicUI:Z

    const v1, -0xe98105    # -2.0004352E38f

    if-eqz v0, :cond_0

    .line 267
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    goto :goto_0

    .line 269
    :cond_0
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    :goto_0
    return-void
.end method

.method protected updateDisplay(Ljava/lang/String;)V
    .locals 7

    .line 253
    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    .line 254
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v0, v1, [Ljava/lang/Object;

    iget v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    iget v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    add-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "%04d-%02d-%02d"

    invoke-static {p1, v1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 255
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 257
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mYear:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mMonth:I

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v4

    iget v3, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    aput-object p1, v5, v1

    const-string p1, "%04d-%02d-%02d %s"

    invoke-static {v0, p1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 258
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CZQueryTabView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
