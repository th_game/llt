.class Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SoftTabView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/SoftTabView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdListAdapter"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/lltskb/lltskb/action/SoftTabView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/SoftTabView;)V
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 138
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private download(Landroid/view/View;)V
    .locals 8

    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 263
    iget-object v1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 264
    iget-object v2, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const-string v3, ".htm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_3

    iget-object v2, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_0

    .line 270
    :cond_0
    iget-object v2, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v2}, Lcom/lltskb/lltskb/action/SoftTabView;->access$100(Lcom/lltskb/lltskb/action/SoftTabView;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 273
    :cond_1
    iget-object v2, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/action/SoftTabView;->access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z

    .line 275
    iget-object v2, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$CXAdtpmWOouiPOyVo6AAQgDx-7Q;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$CXAdtpmWOouiPOyVo6AAQgDx-7Q;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 281
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 282
    move-object v2, p1

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 283
    new-instance v5, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v5}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    .line 284
    iget-object v0, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0x2710

    invoke-virtual {v5, v0, v6, v7}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    if-nez v0, :cond_2

    .line 287
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$ScHgrDjvyifRWBbAsOLtZPaQSaA;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$ScHgrDjvyifRWBbAsOLtZPaQSaA;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 292
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p1, v6}, Lcom/lltskb/lltskb/action/SoftTabView;->access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z

    return-void

    :cond_2
    const/16 v7, 0x2f

    .line 296
    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 298
    new-instance v7, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$NV10iyodHmx3mzAiUQkiAAJKAl8;

    invoke-direct {v7, p0, v1, v3, p1}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$NV10iyodHmx3mzAiUQkiAAJKAl8;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v5, v0, v1, v3, v7}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;)Z

    .line 314
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p1, v6}, Lcom/lltskb/lltskb/action/SoftTabView;->access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z

    .line 315
    invoke-direct {p0, v2, v4}, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V

    return-void

    .line 265
    :cond_3
    :goto_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {p1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 266
    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic lambda$updateButtonText$2(Landroid/widget/Button;Ljava/lang/String;)V
    .locals 1

    .line 254
    invoke-virtual {p0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 255
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$jeg_cHx9TzEP9OiwOcgKoP5ifgI;

    invoke-direct {v1, p1, p2}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$jeg_cHx9TzEP9OiwOcgKoP5ifgI;-><init>(Landroid/widget/Button;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method base64ToBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x0

    .line 181
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    .line 182
    array-length v1, p1

    invoke-static {p1, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public getCount()I
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$000(Lcom/lltskb/lltskb/action/SoftTabView;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    .line 162
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 193
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$000(Lcom/lltskb/lltskb/action/SoftTabView;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    if-nez p2, :cond_0

    .line 196
    iget-object p2, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {p2}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0b0089

    const/4 v2, 0x0

    .line 197
    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f0901ef

    .line 199
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 200
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->bmp:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 202
    :try_start_0
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->icon:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->base64ToBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->bmp:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    .line 203
    iput-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->icon:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 205
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const p3, 0x7f0901f1

    .line 210
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 211
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->name:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0901f2

    .line 213
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 214
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->size:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0901ee

    .line 216
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 217
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->desc:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 221
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f09004f

    .line 223
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 224
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const p3, 0x7f0901f0

    .line 227
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 229
    new-instance v0, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$1iat6dA1lan38GbMWxzW89Jvcns;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$1iat6dA1lan38GbMWxzW89Jvcns;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;I)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method

.method public synthetic lambda$download$3$SoftTabView$AdListAdapter()V
    .locals 4

    .line 276
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d021b

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/action/SoftTabView;->access$202(Lcom/lltskb/lltskb/action/SoftTabView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 278
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public synthetic lambda$download$4$SoftTabView$AdListAdapter()V
    .locals 4

    .line 288
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00db

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/action/SoftTabView;->access$202(Lcom/lltskb/lltskb/action/SoftTabView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 290
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public synthetic lambda$download$6$SoftTabView$AdListAdapter(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;IILjava/lang/String;)Z
    .locals 2

    const/4 p5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x3

    if-ne p4, v1, :cond_0

    .line 300
    iget-object p3, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {p3}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object p3

    check-cast p3, Landroid/app/Activity;

    invoke-static {p3, p1, p2}, Lcom/lltskb/lltskb/utils/LLTUtils;->installApk(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p1, p5}, Lcom/lltskb/lltskb/action/SoftTabView;->access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z

    goto :goto_0

    :cond_0
    if-ne p4, v0, :cond_1

    .line 303
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->mHandler:Landroid/os/Handler;

    new-instance p2, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$1DSIGh_IUqVdM2NMsNcOxwLBx5w;

    invoke-direct {p2, p0, p6}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$1DSIGh_IUqVdM2NMsNcOxwLBx5w;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 308
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p1, p5}, Lcom/lltskb/lltskb/action/SoftTabView;->access$102(Lcom/lltskb/lltskb/action/SoftTabView;Z)Z

    goto :goto_0

    .line 310
    :cond_1
    check-cast p3, Landroid/widget/Button;

    invoke-direct {p0, p3, p6}, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V

    :goto_0
    return v0
.end method

.method public synthetic lambda$getView$0$SoftTabView$AdListAdapter(ILandroid/view/View;)V
    .locals 0

    .line 229
    iget-object p2, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p2, p1}, Lcom/lltskb/lltskb/action/SoftTabView;->access$300(Lcom/lltskb/lltskb/action/SoftTabView;I)V

    return-void
.end method

.method public synthetic lambda$null$5$SoftTabView$AdListAdapter(Ljava/lang/String;)V
    .locals 3

    .line 304
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/action/SoftTabView;->access$202(Lcom/lltskb/lltskb/action/SoftTabView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 306
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-static {p1}, Lcom/lltskb/lltskb/action/SoftTabView;->access$200(Lcom/lltskb/lltskb/action/SoftTabView;)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public synthetic lambda$onClick$1$SoftTabView$AdListAdapter(Landroid/view/View;)V
    .locals 0

    .line 249
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->download(Landroid/view/View;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    const-string v0, "SoftTabView"

    const-string v1, "onClick"

    .line 236
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 241
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const-string v2, ".htm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$d2RPsb8hJ-u2SQp4_XaoqUweby4;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/action/-$$Lambda$SoftTabView$AdListAdapter$d2RPsb8hJ-u2SQp4_XaoqUweby4;-><init>(Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;Landroid/view/View;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    .line 242
    :cond_1
    :goto_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {p1, v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 243
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftTabView$AdListAdapter;->this$0:Lcom/lltskb/lltskb/action/SoftTabView;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/SoftTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
