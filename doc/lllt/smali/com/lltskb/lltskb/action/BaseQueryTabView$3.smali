.class Lcom/lltskb/lltskb/action/BaseQueryTabView$3;
.super Ljava/lang/Object;
.source "BaseQueryTabView.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/action/BaseQueryTabView;->setListViewLongClick(Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/action/BaseQueryTabView;

.field final synthetic val$lv:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/BaseQueryTabView;Landroid/widget/ListView;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView$3;->this$0:Lcom/lltskb/lltskb/action/BaseQueryTabView;

    iput-object p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView$3;->val$lv:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    const p2, 0x7f0d021a

    .line 148
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 150
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, " pos="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView$3;->val$lv:Landroid/widget/ListView;

    invoke-virtual {p3}, Landroid/widget/ListView;->getTag()Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "BaseQueryTabView"

    invoke-static {p3, p2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p2, 0x7f0d00cf

    .line 152
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object p2

    new-instance p3, Lcom/lltskb/lltskb/action/BaseQueryTabView$3$1;

    invoke-direct {p3, p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView$3$1;-><init>(Lcom/lltskb/lltskb/action/BaseQueryTabView$3;)V

    invoke-interface {p2, p3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const p2, 0x7f0d00a9

    .line 163
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object p1

    new-instance p2, Lcom/lltskb/lltskb/action/BaseQueryTabView$3$2;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView$3$2;-><init>(Lcom/lltskb/lltskb/action/BaseQueryTabView$3;)V

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method
