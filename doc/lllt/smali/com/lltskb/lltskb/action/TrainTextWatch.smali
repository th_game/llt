.class public Lcom/lltskb/lltskb/action/TrainTextWatch;
.super Ljava/lang/Object;
.source "TrainTextWatch.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;,
        Lcom/lltskb/lltskb/action/TrainTextWatch$InputLowerToUpper;
    }
.end annotation


# instance fields
.field private mDisableDropdown:Z

.field private mListener:Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;

.field private mTextView:Landroid/widget/AutoCompleteTextView;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;Landroid/widget/AutoCompleteTextView;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mDisableDropdown:Z

    .line 43
    iput-object p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mListener:Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;

    .line 44
    iput-object p2, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    .line 45
    iget-object p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    new-instance p2, Lcom/lltskb/lltskb/action/TrainTextWatch$InputLowerToUpper;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/action/TrainTextWatch$InputLowerToUpper;-><init>(Lcom/lltskb/lltskb/action/TrainTextWatch;)V

    invoke-virtual {p1, p2}, Landroid/widget/AutoCompleteTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 46
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mDisableDropdown:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 81
    iget-object p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mListener:Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;

    if-eqz p1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/action/TrainTextWatch$Listener;->onTextChanged(Landroid/widget/AutoCompleteTextView;)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public disableDropdown(Z)V
    .locals 0

    .line 87
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mDisableDropdown:Z

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 63
    iget-object p2, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 68
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p1}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mDisableDropdown:Z

    if-eqz p1, :cond_1

    goto :goto_0

    .line 71
    :cond_1
    new-instance p1, Lcom/lltskb/lltskb/adapters/TrainAdapter;

    iget-object p2, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/lltskb/lltskb/adapters/TrainAdapter;-><init>(Landroid/content/Context;)V

    .line 72
    iget-object p2, p0, Lcom/lltskb/lltskb/action/TrainTextWatch;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p2, p1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2
    :goto_0
    return-void
.end method
