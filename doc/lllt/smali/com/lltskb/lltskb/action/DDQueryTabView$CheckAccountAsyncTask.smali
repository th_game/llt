.class Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;
.super Landroid/os/AsyncTask;
.source "DDQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/DDQueryTabView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CheckAccountAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private thisView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/DDQueryTabView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/DDQueryTabView;)V
    .locals 1

    .line 396
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 397
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPreExecute$0(Landroid/os/AsyncTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 415
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->cancel(Z)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 393
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 438
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "1"

    return-object p1

    :cond_0
    const-string p1, "0"

    return-object p1
.end method

.method public synthetic lambda$onPreExecute$1$DDQueryTabView$CheckAccountAsyncTask(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 419
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->cancel(Z)Z

    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 393
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/String;)V
    .locals 2

    .line 402
    iget-object p1, p0, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/DDQueryTabView;

    if-eqz p1, :cond_1

    .line 403
    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 405
    :cond_0
    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f0d0093

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 393
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .line 426
    iget-object v0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/DDQueryTabView;

    if-eqz v0, :cond_1

    .line 427
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 429
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 430
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    const/4 p1, 0x0

    .line 431
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->access$000(Lcom/lltskb/lltskb/action/DDQueryTabView;Z)V

    .line 432
    invoke-static {v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->access$200(Lcom/lltskb/lltskb/action/DDQueryTabView;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 410
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 411
    iget-object v0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->thisView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/DDQueryTabView;

    if-eqz v0, :cond_1

    .line 412
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 415
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d009c

    const/4 v2, -0x1

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$DDQueryTabView$CheckAccountAsyncTask$hfq7HEvd3Uqo8iaz8qr5zsEVmPc;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$DDQueryTabView$CheckAccountAsyncTask$hfq7HEvd3Uqo8iaz8qr5zsEVmPc;-><init>(Landroid/os/AsyncTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 418
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setCancelable(Z)V

    .line 419
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$DDQueryTabView$CheckAccountAsyncTask$3wj132-wTGWDl-UkWpfBHOJX2R4;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$DDQueryTabView$CheckAccountAsyncTask$3wj132-wTGWDl-UkWpfBHOJX2R4;-><init>(Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_1
    :goto_0
    return-void
.end method
