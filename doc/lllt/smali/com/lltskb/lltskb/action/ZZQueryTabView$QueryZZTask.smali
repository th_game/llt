.class Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;
.super Landroid/os/AsyncTask;
.source "ZZQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/ZZQueryTabView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryZZTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private _date:Ljava/lang/String;

.field private _from:Ljava/lang/String;

.field private _list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private _to:Ljava/lang/String;

.field private viewWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/ZZQueryTabView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/ZZQueryTabView;)V
    .locals 1

    .line 582
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 583
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->viewWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic lambda$onPreExecute$0(Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x1

    .line 616
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->cancel(Z)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 574
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "ZZQueryTabView"

    const-string v1, "doQuery doInBackground begin"

    .line 621
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 622
    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    const/4 v1, 0x1

    .line 623
    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    const/4 v1, 0x2

    .line 624
    aget-object p1, p1, v1

    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_date:Ljava/lang/String;

    .line 625
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doQuery from="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " date="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_date:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->viewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/ZZQueryTabView;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return-object v1

    .line 639
    :cond_0
    new-instance v2, Lcom/lltskb/lltskb/engine/QueryZZ;

    iget-boolean v3, p1, Lcom/lltskb/lltskb/action/ZZQueryTabView;->fuzzy:Z

    iget-boolean p1, p1, Lcom/lltskb/lltskb/action/ZZQueryTabView;->hiden:Z

    invoke-direct {v2, v3, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;-><init>(ZZ)V

    const/16 p1, 0xff

    .line 640
    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->setFilter(I)V

    .line 641
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_date:Ljava/lang/String;

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/QueryZZ;->setDate(Ljava/lang/String;)V

    .line 643
    :try_start_0
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, Lcom/lltskb/lltskb/engine/QueryZZ;->query(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 645
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 646
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string p1, "doQuery doInBackground end"

    .line 663
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;

    if-nez p1, :cond_1

    :cond_1
    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 574
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7

    .line 587
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 589
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->viewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/action/ZZQueryTabView;

    if-nez v0, :cond_0

    return-void

    .line 593
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;

    if-eqz p1, :cond_2

    .line 594
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result p1

    if-lez p1, :cond_1

    .line 595
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    .line 596
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 597
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \u2192 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0169

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "%d"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f0d01b7

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitleFmt(Ljava/lang/String;)V

    .line 598
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 601
    :cond_2
    iget-object v1, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_list:Ljava/util/List;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_from:Ljava/lang/String;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_to:Ljava/lang/String;

    iget-object v4, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->_date:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->access$100(Lcom/lltskb/lltskb/action/ZZQueryTabView;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 603
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 608
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 610
    iget-object v0, p0, Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;->viewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/action/ZZQueryTabView;

    if-nez v0, :cond_0

    return-void

    .line 616
    :cond_0
    invoke-virtual {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->access$200(Lcom/lltskb/lltskb/action/ZZQueryTabView;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->access$300(Lcom/lltskb/lltskb/action/ZZQueryTabView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, -0xffff01

    new-instance v3, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$QueryZZTask$OCL1ENBWWfVuO8-1IKVuFiljkKM;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$ZZQueryTabView$QueryZZTask$OCL1ENBWWfVuO8-1IKVuFiljkKM;-><init>(Lcom/lltskb/lltskb/action/ZZQueryTabView$QueryZZTask;)V

    invoke-static {v1, v0, v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showLoadingDialog(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AppCompatDialog;

    return-void
.end method
