.class Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;
.super Landroid/os/AsyncTask;
.source "CCQueryTabView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/action/CCQueryTabView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryCCAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private _query:Lcom/lltskb/lltskb/engine/QueryCC;

.field private forceOnline:Z

.field private trainName:Ljava/lang/String;

.field private weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/lltskb/lltskb/action/CCQueryTabView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/action/CCQueryTabView;Ljava/lang/String;Z)V
    .locals 1

    .line 452
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 453
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    .line 454
    iput-object p2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->trainName:Ljava/lang/String;

    .line 455
    iput-boolean p3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->forceOnline:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 446
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 511
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/CCQueryTabView;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 516
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    const/4 v5, 0x1

    add-int/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x2

    aput-object v3, v2, v5

    const-string v3, "%04d%02d%02d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 517
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    .line 518
    new-instance v2, Lcom/lltskb/lltskb/engine/QueryCC;

    iget-boolean p1, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->fuzzy:Z

    invoke-direct {v2, p1, v4}, Lcom/lltskb/lltskb/engine/QueryCC;-><init>(ZZ)V

    iput-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    .line 519
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/QueryCC;->setDate(Ljava/lang/String;)V

    .line 520
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->getStackSize()I

    move-result p1

    if-lez p1, :cond_1

    .line 521
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->popResult()V

    goto :goto_0

    .line 522
    :cond_1
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    iget-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->trainName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/engine/QueryCC;->doActionFuzzy(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 524
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v5, :cond_2

    iget-boolean v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->forceOnline:Z

    if-eqz v2, :cond_3

    .line 525
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getTrainSearchModel()Lcom/lltskb/lltskb/engine/online/ITrainSearch;

    move-result-object p1

    .line 526
    iget-object v2, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->trainName:Ljava/lang/String;

    invoke-interface {p1, v2, v1}, Lcom/lltskb/lltskb/engine/online/ITrainSearch;->queryTrain(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;

    move-result-object p1

    .line 527
    invoke-static {p1}, Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;->convertToResultItemList(Lcom/lltskb/lltskb/engine/online/dto/QueryTrainInfoDTO;)Ljava/util/List;

    move-result-object p1

    .line 530
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setResult(Ljava/util/List;)V

    .line 531
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object p1

    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ResMgr;->getJlbDataMgr()Lcom/lltskb/lltskb/engine/JlbDataMgr;

    move-result-object v2

    iget-object v3, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->trainName:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/lltskb/lltskb/engine/JlbDataMgr;->getJlb(Ljava/lang/String;Ljava/lang/String;)Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 446
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7

    .line 464
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 465
    iget-object p1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/action/CCQueryTabView;

    if-nez p1, :cond_0

    return-void

    .line 470
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->hideLoadingDialog()V

    .line 472
    iget-object v0, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    if-nez v0, :cond_1

    return-void

    .line 474
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ResultMgr;->getResult()Ljava/util/List;

    move-result-object v0

    .line 475
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy()Z

    move-result v1

    const v2, 0x7f0d0169

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    .line 476
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v4, 0x7f0d0094

    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 477
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v3

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v1

    const v2, 0x7f0d01b7

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 478
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 479
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultMgr;->setJlb(Lcom/lltskb/lltskb/engine/JlbDataMgr$JLB;)V

    goto :goto_0

    .line 481
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResultMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResultMgr;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    .line 482
    invoke-virtual {v5}, Lcom/lltskb/lltskb/engine/QueryCC;->getTrainName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/lltskb/lltskb/utils/StringUtils;->getNormalTrainName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v6}, Lcom/lltskb/lltskb/engine/QueryCC;->getLX()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 484
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v2

    const v5, 0x7f0d02e7

    invoke-virtual {v2, v5}, Lcom/lltskb/lltskb/AppContext;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 481
    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/engine/ResultMgr;->setTitle(Ljava/lang/String;)V

    .line 488
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v3, :cond_3

    .line 489
    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0d010f

    const v1, 0x7f0d01e5

    invoke-static {p1, v0, v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showAlertDialog(Landroid/content/Context;II)Landroid/support/v7/app/AppCompatDialog;

    goto/16 :goto_2

    .line 492
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mClassicUI:Z

    if-eqz v2, :cond_4

    const-class v2, Lcom/lltskb/lltskb/result/ResultActivity;

    goto :goto_1

    :cond_4
    const-class v2, Lcom/lltskb/lltskb/result/ViewShowResult;

    :goto_1
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "query_type"

    .line 493
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 494
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getTrainName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "train_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mYear:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    iget v4, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mMonth:I

    add-int/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p1, Lcom/lltskb/lltskb/action/CCQueryTabView;->mDay:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "%04d-%02d-%02d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ticket_date"

    .line 496
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->isFuzzy()Z

    move-result v1

    const-string v2, "query_result_cc_fuzzy"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 498
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getQiYe()Ljava/lang/String;

    move-result-object v1

    const-string v2, "query_result_qiye"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "run_chart_station"

    const-string v2, ""

    .line 500
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 501
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getRuleIndex()I

    move-result v1

    const-string v2, "run_chart_runindex"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 502
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getMinDate()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run_chart_mindate"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    iget-object v1, p0, Lcom/lltskb/lltskb/action/CCQueryTabView$QueryCCAsyncTask;->_query:Lcom/lltskb/lltskb/engine/QueryCC;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/QueryCC;->getMaxDate()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run_chart_maxdate"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/CCQueryTabView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    :goto_2
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 459
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method
