.class public Lcom/lltskb/lltskb/action/SoftDetailsView;
.super Landroid/widget/LinearLayout;
.source "SoftDetailsView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SoftDetailsView"


# instance fields
.field private mButtonDown:Landroid/widget/Button;

.field private mHandler:Landroid/os/Handler;

.field private mIsDownloading:Z

.field private mLoadingView:Landroid/view/View;

.field private mSoft:Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

.field private mToast:Landroid/widget/Toast;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 41
    iput-boolean p2, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    .line 56
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 41
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    .line 45
    iput-object p2, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mSoft:Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 46
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/action/SoftDetailsView;Landroid/view/View;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->download(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/action/SoftDetailsView;)Landroid/widget/Toast;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method static synthetic access$102(Lcom/lltskb/lltskb/action/SoftDetailsView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$202(Lcom/lltskb/lltskb/action/SoftDetailsView;Z)Z
    .locals 0

    .line 33
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/action/SoftDetailsView;)Landroid/os/Handler;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/action/SoftDetailsView;)Landroid/widget/Button;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/action/SoftDetailsView;Landroid/widget/Button;Ljava/lang/String;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/lltskb/lltskb/action/SoftDetailsView;->updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/action/SoftDetailsView;)Landroid/webkit/WebView;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mWebView:Landroid/webkit/WebView;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/action/SoftDetailsView;)Landroid/view/View;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mLoadingView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/action/SoftDetailsView;Ljava/lang/String;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->installSoft(Ljava/lang/String;)V

    return-void
.end method

.method private download(Landroid/view/View;)V
    .locals 3

    .line 147
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 148
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 149
    iget-object v1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const-string v2, ".htm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 156
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v0}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    .line 158
    iget-object p1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x2710

    invoke-virtual {v0, p1, v1, v2}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->download(Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x0

    .line 161
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    .line 162
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/lltskb/lltskb/action/SoftDetailsView$5;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/SoftDetailsView$5;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    .line 175
    :cond_1
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->installSoft(Ljava/lang/String;)V

    return-void

    .line 150
    :cond_2
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object p1, p1, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 151
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private installSoft(Ljava/lang/String;)V
    .locals 6

    .line 98
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 101
    new-instance v2, Lcom/lltskb/lltskb/utils/LLTHttpDownload;

    invoke-direct {v2}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;-><init>()V

    const/16 v3, 0x2f

    .line 102
    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    iget-boolean v5, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    if-eqz v5, :cond_0

    return-void

    .line 107
    :cond_0
    iput-boolean v4, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    .line 109
    iget-object v4, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/lltskb/lltskb/action/SoftDetailsView$3;

    invoke-direct {v5, p0}, Lcom/lltskb/lltskb/action/SoftDetailsView$3;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    new-instance v4, Lcom/lltskb/lltskb/action/SoftDetailsView$4;

    invoke-direct {v4, p0, v0, v3}, Lcom/lltskb/lltskb/action/SoftDetailsView$4;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p1, v0, v3, v4}, Lcom/lltskb/lltskb/utils/LLTHttpDownload;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/lltskb/lltskb/utils/IDownloadListener;)Z

    const/4 p1, 0x0

    .line 143
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mIsDownloading:Z

    .line 144
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    invoke-direct {p0, p1, v1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V

    return-void
.end method

.method private updateButtonText(Landroid/widget/Button;Ljava/lang/String;)V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/lltskb/lltskb/action/SoftDetailsView$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/lltskb/lltskb/action/SoftDetailsView$2;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;Landroid/widget/Button;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public initView(Landroid/content/Context;)V
    .locals 1

    .line 180
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftDetailsView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0b0088

    .line 181
    invoke-virtual {p1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 183
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mHandler:Landroid/os/Handler;

    const p1, 0x7f09031a

    .line 185
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/webkit/WebView;

    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mWebView:Landroid/webkit/WebView;

    const p1, 0x7f09017c

    .line 186
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mLoadingView:Landroid/view/View;

    const p1, 0x7f090050

    .line 189
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    .line 190
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mSoft:Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 191
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mButtonDown:Landroid/widget/Button;

    new-instance v0, Lcom/lltskb/lltskb/action/SoftDetailsView$6;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/SoftDetailsView$6;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object p1

    const/4 v0, 0x1

    .line 200
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 202
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 205
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mWebView:Landroid/webkit/WebView;

    if-eqz p1, :cond_0

    .line 206
    new-instance v0, Lcom/lltskb/lltskb/action/SoftDetailsView$7;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/SoftDetailsView$7;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 232
    iget-object p1, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mWebView:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/lltskb/lltskb/action/SoftDetailsView;->mSoft:Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDownLoad(Landroid/view/View;)V
    .locals 3

    const-string v0, "SoftDetailsView"

    const-string v1, "onClick"

    .line 60
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;

    .line 66
    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    const-string v2, ".htm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v1, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->detail:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/lltskb/lltskb/action/SoftDetailsView$1;

    invoke-direct {v1, p0, p1}, Lcom/lltskb/lltskb/action/SoftDetailsView$1;-><init>(Lcom/lltskb/lltskb/action/SoftDetailsView;Landroid/view/View;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 80
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    .line 67
    :cond_1
    :goto_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, v0, Lcom/lltskb/lltskb/engine/ResMgr$SoftItem;->link:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {p1, v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 68
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/SoftDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
