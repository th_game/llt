.class public abstract Lcom/lltskb/lltskb/action/BaseQueryTabView;
.super Landroid/widget/LinearLayout;
.source "BaseQueryTabView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BaseQueryTabView"


# instance fields
.field protected fuzzy:Z

.field protected hiden:Z

.field protected mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

.field protected mClassicUI:Z

.field protected mDate:Landroid/widget/TextView;

.field protected mDay:I

.field protected mMonth:I

.field protected mYear:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 35
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->fuzzy:Z

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->hiden:Z

    .line 37
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mClassicUI:Z

    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mDate:Landroid/widget/TextView;

    const/16 v0, 0x7da

    .line 40
    iput v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mYear:I

    const/16 v0, 0x8

    .line 41
    iput v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mMonth:I

    .line 42
    iput p1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mDay:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 35
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->fuzzy:Z

    const/4 p2, 0x0

    .line 36
    iput-boolean p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->hiden:Z

    .line 37
    iput-boolean p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mClassicUI:Z

    const/4 p2, 0x0

    .line 39
    iput-object p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mDate:Landroid/widget/TextView;

    const/16 p2, 0x7da

    .line 40
    iput p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mYear:I

    const/16 p2, 0x8

    .line 41
    iput p2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mMonth:I

    .line 42
    iput p1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mDay:I

    return-void
.end method


# virtual methods
.method protected abstract clearRecord(I)V
.end method

.method protected loadSetting()V
    .locals 2

    .line 82
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isHideTrain()Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->hiden:Z

    .line 83
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isFuzzyQuery()Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->fuzzy:Z

    .line 84
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->isClassicStyle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mClassicUI:Z

    .line 85
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mClassicUI:Z

    const v1, -0xe98105    # -2.0004352E38f

    if-eqz v0, :cond_0

    .line 86
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    goto :goto_0

    .line 88
    :cond_0
    sput v1, Lcom/lltskb/lltskb/engine/QueryConst;->COLOR_PASSBY_TRAIN:I

    :goto_0
    return-void
.end method

.method protected setListViewLongClick(Landroid/widget/ListView;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 131
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setLongClickable(Z)V

    .line 132
    new-instance v0, Lcom/lltskb/lltskb/action/BaseQueryTabView$2;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/action/BaseQueryTabView$2;-><init>(Lcom/lltskb/lltskb/action/BaseQueryTabView;Landroid/widget/ListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 143
    new-instance v0, Lcom/lltskb/lltskb/action/BaseQueryTabView$3;

    invoke-direct {v0, p0, p1}, Lcom/lltskb/lltskb/action/BaseQueryTabView$3;-><init>(Lcom/lltskb/lltskb/action/BaseQueryTabView;Landroid/widget/ListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    :cond_0
    return-void
.end method

.method protected showDateDialog()V
    .locals 4

    const-string v0, "BaseQueryTabView"

    const-string v1, "showDateDialog"

    .line 93
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v0, Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AppCompatDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    .line 95
    iget-object v0, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatDialog;->requestWindowFeature(I)Z

    .line 97
    new-instance v0, Lcom/lltskb/lltskb/view/CalendarView;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultMaxDays()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/CalendarView;-><init>(Landroid/content/Context;J)V

    .line 99
    iget-object v1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    .line 100
    iget-object v1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    const-string v2, "\u5e74\u6708\u65e5"

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 103
    iget-object v1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mCalendarDlg:Landroid/support/v7/app/AppCompatDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    const v2, 0x7f0e00bd

    .line 105
    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 107
    :cond_0
    iget v1, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mYear:I

    iget v2, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mMonth:I

    iget v3, p0, Lcom/lltskb/lltskb/action/BaseQueryTabView;->mDay:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lltskb/lltskb/view/CalendarView;->setSelectedDate(III)V

    .line 108
    new-instance v1, Lcom/lltskb/lltskb/action/BaseQueryTabView$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/BaseQueryTabView$1;-><init>(Lcom/lltskb/lltskb/action/BaseQueryTabView;)V

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/CalendarView;->setOnDateSetListener(Lcom/lltskb/lltskb/view/CalendarView$OnDateSetListener;)V

    .line 121
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "zzquery"

    const-string v2, "\u8f66\u578b"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract updateDisplay(Ljava/lang/String;)V
.end method
